﻿using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Moq
{
    public abstract class SqlExpressionExecutor<TExpression> : IExecutor<TExpression, ExecutionState>
        where TExpression : ISqlExpression
    {
        public virtual int Priority { get; } = 0;

        public virtual bool CanHandle(TExpression expression)
        {
            return true;
        }

        public abstract ExecutionState Handle(TExpression expression, IMockSourceExecutionContext transformer);
    }
}