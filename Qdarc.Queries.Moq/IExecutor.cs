﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Moq
{
    public interface IExecutor<TExpression, TResult> : ITransformer<TExpression, TResult, IMockSourceExecutionContext>
    {
        int Priority { get; }

        bool CanHandle(TExpression part);
    }
}
