﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Moq
{
    public class MockSourceExecutionContext : IMockSourceExecutionContext
    {
        private readonly Dictionary<ISource, ParameterExpression> _parameterMapping
            = new Dictionary<ISource, ParameterExpression>();

        private readonly IServiceProvider _resolver;

        public MockSourceExecutionContext(
            IServiceProvider resolver) => _resolver = resolver;

        public void AddParameterMapping(ISource source, ParameterExpression parameter)
        {
            _parameterMapping.Add(source, parameter);
        }

        public IList Execute(ISqlExpression part)
        {
            var result = part.Accept<ExecutionState>(this);
            return Execute(result);
        }

        public ParameterExpression GetParameterMapping(ISource source) => _parameterMapping[source];

        public TResult Handle<TExpression, TResult>(TExpression expression)
        {
            var handlers = _resolver.GetServices<IExecutor<TExpression, TResult>>();
            var handler = handlers.OrderBy(x => x.Priority).FirstOrDefault(x => x.CanHandle(expression));
            if (handler == null)
            {
                throw new InvalidOperationException($"There is no formatting handler of {typeof(TExpression).FullName}");
            }

            return handler.Handle(expression, this);
        }

        public void RemoveParameterMapping(ISource source)
        {
            _parameterMapping.Remove(source);
        }

        private IList Execute(ExecutionState result)
        {
            var lambda = Expression.Lambda<Func<IList>>(result.Expression);
            var method = lambda.Compile();
            return method();
        }
    }
}