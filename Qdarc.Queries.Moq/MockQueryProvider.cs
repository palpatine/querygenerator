﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Finalizers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Moq
{
    internal sealed class MockQueryProvider : ITableQueryProvider, IQueryProvider
    {
        private readonly Func<IVisitationContext> _linqQueryVisitationContextFactory;
        private readonly Func<IMockSourceExecutionContext> _mockSourceExecutionContextFactory;
        private readonly IEnumerable<IResultFinalizer> _finalizers;

        public MockQueryProvider(
            Func<IVisitationContext> linqQueryVisitationContextFactory,
            Func<IMockSourceExecutionContext> mockSourceExecutionContextFactory,
            IEnumerable<IResultFinalizer> finalizers)
        {
            _linqQueryVisitationContextFactory = linqQueryVisitationContextFactory;
            _mockSourceExecutionContextFactory = mockSourceExecutionContextFactory;
            _finalizers = finalizers.OrderBy(x => x.Priority).ToArray();
        }

        public IQueryProvider AsQueryProvider()
        {
            return this;
        }

        public IOrderedTableQuery<TElement> CreateOrderedQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        public ITableQuery<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        public object Execute(Expression expression)
        {
            return Execute(TypeSystem.GetElementType(expression.Type), expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            var elementType = TypeSystem.GetElementType<TResult>();

            var result = Execute(elementType, expression);

            if (result == null)
            {
                return default(TResult);
            }

            if (result is TResult)
            {
                return (TResult)result;
            }

            return (TResult)Convert.ChangeType(result, typeof(TResult));
        }

        public TResult Execute<TResult>(ISqlExpression expression)
        {
            var elementType = TypeSystem.GetElementType<TResult>();
            return (TResult)Execute(elementType, expression);
        }

        public int ExecuteNonQuery(Expression expression)
        {
            throw new NotImplementedException();
        }

        public int ExecuteNonQuery(ISqlExpression expression)
        {
            throw new NotImplementedException();
        }

        public ISqlExpression Visit(Expression expression)
        {
            var visitor = _linqQueryVisitationContextFactory();
            var visitExpression = visitor.VisitExpression(expression);
            return visitExpression;
        }

        IQueryable<TElement> IQueryProvider.CreateQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            var type = TypeSystem.GetElementType(expression.Type);
            var queryType = typeof(TableQuery<>).MakeGenericType(type);

            var constructor = queryType
                .GetTypeInfo()
                .DeclaredConstructors
                .Single(x => x.GetParameters().Length == 2);

            return (IQueryable)constructor.Invoke(
                new object[] { this, expression });
        }

        internal object Execute(Type elementType, Expression expression)
        {
            var visitor = _linqQueryVisitationContextFactory();
            var part = visitor.FinalizeSelection(visitor.VisitExpression(expression));

            var result = Execute(elementType, part);

            if (TypeSystem.IsCollection(expression.Type))
            {
                return result;
            }

            foreach (var finalizer in _finalizers)
            {
                object finalizedResult;
                if (finalizer.TryFinalizeResult(result, expression, out finalizedResult))
                {
                    return finalizedResult;
                }
            }

            var list = result.Cast<object>();
            return list.SingleOrDefault();
        }

        private IList Execute(Type elementType, ISqlExpression part)
        {
            var executor = _mockSourceExecutionContextFactory();
            var result = executor.Execute(part);
            return result;
        }
    }
}
