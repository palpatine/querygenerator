﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Moq
{
    public interface IMockDatabase : IDisposable
    {
        IDatabase Database { get; }

        IList GetData(ElementName name);

        void Setup<TSource>(ElementName name, IEnumerable<TSource> source);

        void Setup<TSource>(IEnumerable<TSource> data);
    }
}
