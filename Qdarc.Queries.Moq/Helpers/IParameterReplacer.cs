using System.Collections.Generic;
using System.Linq.Expressions;

namespace Qdarc.Queries.Moq.Helpers
{
    public interface IParameterReplacer
    {
        IEnumerable<ParameterExpression> Parameters { get; }

        Expression Replace(Expression value);
    }
}