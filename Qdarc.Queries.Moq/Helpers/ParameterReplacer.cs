﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Moq.Helpers;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class ParameterReplacer : ExpressionVisitor, IParameterReplacer
    {
        private Dictionary<ParameterExpression, ParameterExpression> _parameters;

        public IEnumerable<ParameterExpression> Parameters => _parameters.Values.ToList();

        public Expression Replace(Expression value)
        {
            _parameters = new Dictionary<ParameterExpression, ParameterExpression>();
            return Visit(value);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (_parameters.ContainsKey(node))
            {
                return _parameters[node];
            }

            var parameter = Expression.Parameter(node.Type);
            _parameters.Add(node, parameter);

            return parameter;
        }
    }
}
