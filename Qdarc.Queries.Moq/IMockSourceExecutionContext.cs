﻿using System.Collections;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Moq
{
    public interface IMockSourceExecutionContext : ITransformer
    {
        IList Execute(ISqlExpression part);

        void AddParameterMapping(ISource source, ParameterExpression parameter);

        ParameterExpression GetParameterMapping(ISource source);

        void RemoveParameterMapping(ISource source);
    }
}
