﻿using System;
using System.Linq.Expressions;

namespace Qdarc.Queries.Moq
{
    public class ExecutionState
    {
        public Expression Expression { get; set; }

        public string Name { get; set; }

        public bool IsAggregate { get; set; }

        public Func<Expression, Expression> ExpressionFactory { get; set; }
    }
}
