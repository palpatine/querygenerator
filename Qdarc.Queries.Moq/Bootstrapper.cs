﻿using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Queries.Moq.Helpers;

namespace Qdarc.Queries.Moq
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IServiceCollection registrar)
        {
            registrar.AddTransient<IMockSourceExecutionContext, MockSourceExecutionContext>();
            registrar.AddTransient<ITableQueryProvider, MockQueryProvider>();
            registrar.AddTransient<IMockDatabase, MockDatabase>();
            registrar.AddTransient<IParameterReplacer, ParameterReplacer>();

            registrar.AddTransient<IExecutor<SelectCommand, ExecutionState>, SelectCommandExecutor>();
            registrar.AddTransient<IExecutor<AliasedSourceItem, ExecutionState>, AliasedSourceItemExecutor>();
            registrar.AddTransient<IExecutor<TableReference, ExecutionState>, TableReferenceExecutor>();
            registrar.AddTransient<IExecutor<SqlSelection, ExecutionState>, SelectionExecutor>();
            registrar.AddTransient<IExecutor<SqlCastExpression, ExecutionState>, CastExpressionExecutor>();
            registrar.AddTransient<IExecutor<SqlInlineIfExpression, ExecutionState>, InlineIfExpressionExecutor>();
            registrar.AddTransient<IExecutor<ExistsExpression, ExecutionState>, ExistsExpressionExecutor>();
            registrar.AddTransient<IExecutor<SqlConstantExpression, ExecutionState>, ConstantExpressionExecutor>();
            registrar.AddTransient<IExecutor<ProjectionByConstructor, ExecutionState>, ProjectionByConstructorExecutor>();
            registrar.AddTransient<IExecutor<AliasedSelectionItem, ExecutionState>, AliasedSelectionItemExecutor>();
            registrar.AddTransient<IExecutor<LinqColumnReference, ExecutionState>, LinqColumnReferenceExecutor>();
            registrar.AddTransient<IExecutor<SqlCountExpression, ExecutionState>, CountExpressionExecutor>();
            registrar.AddTransient<IExecutor<SqlAsteriskCountExpression, ExecutionState>, AsteriskCountExpressionExecutor>();
        }
    }
}