﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class AliasedSourceItemExecutor : SqlExpressionExecutor<AliasedSourceItem>
    {
        public override ExecutionState Handle(AliasedSourceItem expression, IMockSourceExecutionContext transformer)
        {
            var source = expression.Source.Accept<ExecutionState>(transformer);
            return source;
        }
    }
}
