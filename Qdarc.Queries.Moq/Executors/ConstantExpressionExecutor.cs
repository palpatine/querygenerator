﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class ConstantExpressionExecutor : SqlExpressionExecutor<SqlConstantExpression>
    {
        public override ExecutionState Handle(SqlConstantExpression expression, IMockSourceExecutionContext transformer)
        {
            return new ExecutionState
            {
                Expression = Expression.Constant(expression.Value)
            };
        }
    }
}
