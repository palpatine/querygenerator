﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Moq.Helpers;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class CountExpressionExecutor : SqlExpressionExecutor<SqlCountExpression>
    {
        private static readonly MethodInfo Count =
            ExpressionExtensions.GetMethod(() => Enumerable.Count<int>(null))
            .GetGenericMethodDefinition();

        private static readonly MethodInfo CountWhere =
            ExpressionExtensions.GetMethod(() => Enumerable.Count<int>(null, x => x > 0))
            .GetGenericMethodDefinition();

        private readonly IParameterReplacer _replacer;

        public CountExpressionExecutor(IParameterReplacer replacer)
        {
            _replacer = replacer;
        }

        public override ExecutionState Handle(SqlCountExpression expression, IMockSourceExecutionContext transformer)
        {
            Func<Expression, Expression> expressionFactory;
            if (expression.Value != null)
            {
                expressionFactory = CreateCountWhereExpressionFactory(expression.Value, transformer);
            }
            else
            {
                throw new InvalidModelException("Count expression must refer to value", expression);
            }

            return new ExecutionState
            {
                IsAggregate = true,
                ExpressionFactory = expressionFactory
            };
        }

        private Func<Expression, Expression> CreateCountWhereExpressionFactory(ISqlValueExpression valueExpression, IMockSourceExecutionContext transformer)
        {
            var value = valueExpression.Accept<ExecutionState>(transformer);
            if (value.Expression.Type.GetTypeInfo().IsClass)
            {
                var query = _replacer.Replace(value.Expression);
                var body = Expression.NotEqual(query, Expression.Constant(null, query.Type));
                return x =>
                {
                    var countMethod = CountWhere.MakeGenericMethod(x.Type.GenericTypeArguments[0]);
                    var filterType = typeof(Func<,>).MakeGenericType(x.Type.GenericTypeArguments[0], typeof(bool));
                    var lambda = Expression.Lambda(filterType, body, _replacer.Parameters.ToArray());
                    return Expression.Call(null, countMethod, x, lambda);
                };
            }
            else
            {
                return CreateCountExpressionFactory();
            }
        }

        private Func<Expression, Expression> CreateCountExpressionFactory()
        {
            return x =>
            {
                var countMethod = Count.MakeGenericMethod(x.Type.GenericTypeArguments[0]);
                return Expression.Call(null, countMethod, x);
            };
        }
    }
}
