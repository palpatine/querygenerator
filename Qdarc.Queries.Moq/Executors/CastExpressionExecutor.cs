﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class CastExpressionExecutor : SqlExpressionExecutor<SqlCastExpression>
    {
        public override ExecutionState Handle(SqlCastExpression expression, IMockSourceExecutionContext transformer)
        {
            var casted = expression.Expression.Accept<ExecutionState>(transformer);

            if (expression.ClrType == typeof(bool))
            {
                return new ExecutionState
                {
                    Expression = Expression.Equal(casted.Expression, Expression.Constant(1))
                };
            }

            return new ExecutionState
            {
                Expression = Expression.Convert(casted.Expression, expression.ClrType)
            };
        }
    }
}
