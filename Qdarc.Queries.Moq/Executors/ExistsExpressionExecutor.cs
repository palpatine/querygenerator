﻿using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class ExistsExpressionExecutor : SqlExpressionExecutor<ExistsExpression>
    {
        private static readonly MethodInfo AnyMethod = ExpressionExtensions
             .GetMethod(() => Enumerable.Any<int>(null))
             .GetGenericMethodDefinition();

        public override ExecutionState Handle(ExistsExpression expression, IMockSourceExecutionContext transformer)
        {
            var innerSelection = expression.Select.Accept<ExecutionState>(transformer);
            var method = AnyMethod.MakeGenericMethod(innerSelection.Expression.Type.GenericTypeArguments.Single());
            Expression resultExpression = Expression.Call(null, method, innerSelection.Expression);
            if (expression.IsNegated)
            {
                resultExpression = Expression.Not(resultExpression);
            }

            return new ExecutionState
            {
                Expression = resultExpression
            };
        }
    }
}
