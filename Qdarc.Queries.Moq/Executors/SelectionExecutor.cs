﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class SelectionExecutor : SqlExpressionExecutor<SqlSelection>
    {
        public override ExecutionState Handle(SqlSelection expression, IMockSourceExecutionContext transformer)
        {
            bool isSingleSelection = IsSingleSelection(expression);
            if (isSingleSelection && expression.Elements.Count() != 1)
            {
                throw new InvalidModelException($"Expected single {expression.ClrType} selection.", expression);
            }

            var selections = expression.Elements.Select(x => x.Accept<ExecutionState>(transformer)).ToArray();
            if (isSingleSelection)
            {
                return HandleSingleSelection(expression, selections);
            }
            else
            {
                return HandleBlockSelection(expression, selections);
            }
        }

        private static ExecutionState HandleBlockSelection(SqlSelection expression, ExecutionState[] selections)
        {
            var blockContent = new List<Expression>();
            var local = Expression.Variable(expression.ClrType);
            var newExpression = Expression.New(
                expression.ClrType);
            var assignment = Expression.Assign(local, newExpression);
            blockContent.Add(assignment);

            foreach (var item in selections)
            {
                var property = Expression.Property(local, item.Name);
                var assignProperty = Expression.Assign(property, item.Expression);
                blockContent.Add(assignProperty);
            }

            blockContent.Add(local);

            return new ExecutionState
            {
                Expression = Expression.Block(new[] { local }, blockContent)
            };
        }

        private static ExecutionState HandleSingleSelection(SqlSelection expression, ExecutionState[] selections)
        {
            var item = selections.Single();
            if (item.IsAggregate)
            {
                return item;
            }
            else
            {
                if (item.Expression.Type == expression.ClrType)
                {
                    return item;
                }
            }

            return new ExecutionState
            {
                Expression = Expression.Convert(item.Expression, expression.ClrType)
            };
        }

        private static bool IsSingleSelection(SqlSelection expression)
        {
            return expression.ClrType == typeof(string)
                || expression.ClrType == typeof(ushort)
                || expression.ClrType == typeof(short)
                || expression.ClrType == typeof(ulong)
                || expression.ClrType == typeof(long)
                || expression.ClrType == typeof(uint)
                || expression.ClrType == typeof(int)
                || expression.ClrType == typeof(float)
                || expression.ClrType == typeof(double)
                || expression.ClrType == typeof(decimal)
                || expression.ClrType == typeof(char)
                || expression.ClrType == typeof(sbyte)
                || expression.ClrType == typeof(byte)
                || expression.ClrType == typeof(bool)
                || expression.ClrType == typeof(ushort?)
                || expression.ClrType == typeof(short?)
                || expression.ClrType == typeof(ulong?)
                || expression.ClrType == typeof(long?)
                || expression.ClrType == typeof(uint?)
                || expression.ClrType == typeof(int?)
                || expression.ClrType == typeof(float?)
                || expression.ClrType == typeof(double?)
                || expression.ClrType == typeof(decimal?)
                || expression.ClrType == typeof(char?)
                || expression.ClrType == typeof(sbyte?)
                || expression.ClrType == typeof(byte?)
                || expression.ClrType == typeof(bool?);
        }
    }
}
