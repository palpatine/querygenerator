﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class LinqColumnReferenceExecutor : SqlExpressionExecutor<LinqColumnReference>
    {
        public override ExecutionState Handle(LinqColumnReference expression, IMockSourceExecutionContext transformer)
        {
            return new ExecutionState
            {
                Expression = Expression.Property(transformer.GetParameterMapping(expression.Source), expression.Property)
            };
        }
    }
}
