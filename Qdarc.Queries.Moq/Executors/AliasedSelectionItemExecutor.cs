﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class AliasedSelectionItemExecutor : SqlExpressionExecutor<AliasedSelectionItem>
    {
        public override ExecutionState Handle(AliasedSelectionItem expression, IMockSourceExecutionContext transformer)
        {
            var inner = expression.Item.Accept<ExecutionState>(transformer);
            return new ExecutionState
            {
                Expression = inner.Expression,
                Name = expression.Alias
            };
        }
    }
}
