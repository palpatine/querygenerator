﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Moq.Executors
{
    public class InlineIfExpressionExecutor : SqlExpressionExecutor<SqlInlineIfExpression>
    {
        public override ExecutionState Handle(SqlInlineIfExpression expression, IMockSourceExecutionContext transformer)
        {
            var condition = expression.Condition.Accept<ExecutionState>(transformer);
            var ifTrue = expression.IfTrue.Accept<ExecutionState>(transformer);
            var ifFalse = expression.IfFalse.Accept<ExecutionState>(transformer);

            return new ExecutionState
            {
                Expression = Expression.Condition(condition.Expression, ifTrue.Expression, ifFalse.Expression)
            };
        }
    }
}
