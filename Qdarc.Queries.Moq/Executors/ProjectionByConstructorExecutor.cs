﻿using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;

namespace Qdarc.Queries.Moq.Executors
{
    public sealed class ProjectionByConstructorExecutor : SqlExpressionExecutor<ProjectionByConstructor>
    {
        public override ExecutionState Handle(ProjectionByConstructor expression, IMockSourceExecutionContext transformer)
        {
            var parameters = expression.Elements
                .Select(x => x.Accept<ExecutionState>(transformer).Expression)
                .ToArray();

            return new ExecutionState
            {
                Expression = Expression.New(expression.Constructor, parameters)
            };
        }
    }
}
