﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class AsteriskCountExpressionExecutor : SqlExpressionExecutor<SqlAsteriskCountExpression>
    {
        private static readonly MethodInfo Count =
            ExpressionExtensions.GetMethod(() => Enumerable.Count<int>(null))
            .GetGenericMethodDefinition();

        public override ExecutionState Handle(SqlAsteriskCountExpression expression, IMockSourceExecutionContext transformer)
        {
            var expressionFactory = CreateCountExpressionFactory();

            return new ExecutionState
            {
                IsAggregate = true,
                ExpressionFactory = expressionFactory
            };
        }

        private Func<Expression, Expression> CreateCountExpressionFactory()
        {
            return x =>
            {
                var countMethod = Count.MakeGenericMethod(x.Type.GenericTypeArguments[0]);
                return Expression.Call(null, countMethod, x);
            };
        }
    }
}
