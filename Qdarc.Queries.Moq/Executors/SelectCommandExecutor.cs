﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class SelectCommandExecutor : SqlExpressionExecutor<SelectCommand>
    {
        private static readonly MethodInfo Select = ExpressionExtensions
            .GetMethod(() => Enumerable.Select<int, int>(null, x => x))
            .GetGenericMethodDefinition();

        private static readonly MethodInfo ToList = ExpressionExtensions
            .GetMethod(() => Enumerable.ToList<int>(null))
            .GetGenericMethodDefinition();

        public override ExecutionState Handle(SelectCommand expression, IMockSourceExecutionContext transformer)
        {
            ExecutionState source = null;

            if (expression.Source != null)
            {
                source = expression.Source.Accept<ExecutionState>(transformer);

                var sourceType = source.Expression.Type.GenericTypeArguments.Single();

                var parameter = Expression.Parameter(sourceType);
                transformer.AddParameterMapping(expression.Source, parameter);
                var selection = expression.Selection.Accept<ExecutionState>(transformer);
                transformer.RemoveParameterMapping(expression.Source);

                if (selection.IsAggregate)
                {
                    var resultExpression = selection.ExpressionFactory(source.Expression);
                    return new ExecutionState
                    {
                        Expression = Expression.NewArrayInit(resultExpression.Type, resultExpression)
                    };
                }
                else
                {
                    var selectMethod = Select.MakeGenericMethod(sourceType, selection.Expression.Type);
                    var selectorType = typeof(Func<,>).MakeGenericType(sourceType, selection.Expression.Type);
                    var selectionLambda = Expression.Lambda(selectorType, selection.Expression, parameter);
                    var toListMethod = ToList.MakeGenericMethod(selection.Expression.Type);
                    var selectCall = Expression.Call(null, selectMethod, source.Expression, selectionLambda);
                    var toListCall = Expression.Call(null, toListMethod, selectCall);

                    return new ExecutionState
                    {
                        Expression = toListCall
                    };
                }
            }
            else
            {
                var selection = expression.Selection.Accept<ExecutionState>(transformer);
                return new ExecutionState
                {
                    Expression = Expression.NewArrayInit(selection.Expression.Type, selection.Expression)
                };
            }
        }
    }
}
