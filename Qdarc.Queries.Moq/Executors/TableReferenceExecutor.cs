﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Moq.Executors
{
    internal sealed class TableReferenceExecutor : SqlExpressionExecutor<TableReference>
    {
        private readonly IMockDatabase _database;

        public TableReferenceExecutor(IMockDatabase database)
        {
            _database = database;
        }

        public override ExecutionState Handle(TableReference expression, IMockSourceExecutionContext transformer)
        {
            var collectionType = typeof(List<>).MakeGenericType(expression.ClrType);
            var data = _database.GetData(expression.Name);

            return new ExecutionState
            {
                Expression = Expression.Constant(data, collectionType)
            };
        }
    }
}
