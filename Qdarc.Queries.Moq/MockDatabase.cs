﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Names;
using Unity;

namespace Qdarc.Queries.Moq
{
    public sealed class MockDatabase : IMockDatabase
    {
        private readonly IUnityContainer _container;
        private readonly IModelToSqlConvention _convention;
        private readonly Dictionary<ElementName, IList> _data = new Dictionary<ElementName, IList>();
        private bool _isDisposed;

        public MockDatabase(
            IUnityContainer container,
            IModelToSqlConvention convention)
        {
            _container = container.CreateChildContainer();
            _container.RegisterInstance<IMockDatabase>(this);
            _container.RegisterInstance<IServiceProvider>(new UnityServiceProvider(_container));
            _convention = convention;
            Database = _container.Resolve<IDatabase>();
        }

        public IDatabase Database { get; }

        public void Dispose()
        {
            if (!_isDisposed)
            {
                _isDisposed = true;
                _container.Dispose();
            }
        }

        public void Setup<TSource>(IEnumerable<TSource> data)
        {
            var name = _convention.GetTableName(typeof(TSource));
            Setup(name, data.ToList());
        }

        public IList GetData(ElementName name)
        {
            if (!_data.ContainsKey(name))
            {
                throw new InvalidOperationException($"No data setup for {name}");
            }

            return _data[name];
        }

        public void Setup<TSource>(ElementName name, IEnumerable<TSource> source)
        {
            if (_data.ContainsKey(name))
            {
                throw new InvalidOperationException($"Element name already present. {name}");
            }

            _data.Add(name, source.ToList());
        }
    }
}
