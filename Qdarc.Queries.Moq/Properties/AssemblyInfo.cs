﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

#if DEBUG
[assembly: InternalsVisibleTo("Qdarc.Queries.Moq.Tests")]
#endif