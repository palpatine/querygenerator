﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Formatter.Tsql.Tests
{
    [TestClass]
    public class AliasManagerTests
    {
        [TestMethod]
        public void ShouldGenerateUniqueParameterAlias()
        {
            var parameter1 = new Mock<IParameter>();
            parameter1.SetupProperty(x => x.Name);
            var parameter2 = new Mock<IParameter>();
            parameter2.SetupProperty(x => x.Name);
            var aliasManger = new AliasManager();

            var alias1 = aliasManger.GetAlias(parameter1.Object);
            var alias2 = aliasManger.GetAlias(parameter2.Object);

            alias1.ShouldBeEqual("@param1");
            alias2.ShouldBeEqual("@param2");
            parameter1.VerifySet(x => x.Name = "@param1");
            parameter2.VerifySet(x => x.Name = "@param2");
        }

        [TestMethod]
        public void ShouldNotSetParameterAliasIfItIsAlreadySet()
        {
            var parameter1 = new Mock<IParameter>();
            parameter1.SetupProperty(x => x.Name, "alias");
            var aliasManger = new AliasManager();

            var alias1 = aliasManger.GetAlias(parameter1.Object);

            alias1.ShouldBeEqual("alias");
            parameter1.VerifySet(x => x.Name = It.IsAny<string>(), Times.Never);
        }

        [TestMethod]
        public void ShouldGenerateUniqueQueryAlias()
        {
            var aliasManger = new AliasManager();

            var alias1 = aliasManger.GetAlias();
            var alias2 = aliasManger.GetAlias();

            alias1.ShouldBeEqual("q1");
            alias2.ShouldBeEqual("q2");
        }
    }
}
