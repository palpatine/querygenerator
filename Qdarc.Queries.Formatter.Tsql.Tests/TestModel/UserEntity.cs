﻿using System;

namespace Qdarc.Queries.Formatter.Tsql.Tests.TestModel
{
    internal class UserEntity : IUser
    {
        public Context Context { get; set; }

        public string FirstName { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string LastName { get; set; }

        public IUser Parent { get; set; }

        public int? ParentId { get; set; }

        public DateTime WorkEnd { get; set; }
    }
}