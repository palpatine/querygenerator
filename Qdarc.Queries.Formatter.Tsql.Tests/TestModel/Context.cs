﻿using System;

namespace Qdarc.Queries.Formatter.Tsql.Tests.TestModel
{
    [Flags]
    internal enum Context
    {
        Value1,
        Value2,
        Value3,
    }
}