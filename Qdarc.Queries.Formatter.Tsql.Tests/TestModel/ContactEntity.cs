﻿using System;

namespace Qdarc.Queries.Formatter.Tsql.Tests.TestModel
{
    internal class ContactEntity
    {
        public int? Account { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public int ParentId { get; set; }

        public DateTime? VersionEndDate { get; set; }

        public DateTime VersionStartDate { get; set; }
    }
}