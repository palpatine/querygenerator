﻿namespace Qdarc.Queries.Formatter.Tsql.Tests.TestModel
{
    internal class UserAccountRelation
    {
        ////  [SqlType(SqlNativeType.Int, true, 10)]
        public int? Account { get; set; }

        ////  [SqlType(SqlNativeType.Int, false, 10)]
        public int Id { get; set; }

        //// [SqlType(SqlNativeType.Int, true, 10)]
        public int? User { get; set; }
    }
}