﻿namespace Qdarc.Queries.Formatter.Tsql.Tests.TestModel
{
    internal interface IMarker
    {
        int Id { get; set; }
    }
}