﻿namespace Qdarc.Queries.Formatter.Tsql.Tests.TestModel
{
    internal class UserContactRelation
    {
        ////  [SqlType(SqlNativeType.Int, false, 10)]
        public int Contact { get; set; }

        ////  [SqlType(SqlNativeType.Int, false, 10)]
        public int Id { get; set; }

        ////   [SqlType(SqlNativeType.Int, false, 10)]
        public int User { get; set; }
    }
}