﻿using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Formatter.Tsql.Tests.TestModel;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Utilities;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class BoundSelectionItemTsqlFormatterTests
    {
        private static readonly PropertyInfo Property = ExpressionExtensions.GetProperty((UserEntity e) => e.Context);
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldVisitItemUnderAlias()
        {
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var selectioItem = new Mock<ISelectionItem>();
            selectioItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });

            var formatter = new BoundSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new BoundSelectionItem(Property, selectioItem.Object);

            formatter.Handle(query, _visitor.Object);

            selectioItem.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignAliasIfNotAssigned()
        {
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var selectioItem = new Mock<ISelectionItem>();
            selectioItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });

            var formatter = new BoundSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new BoundSelectionItem(Property, selectioItem.Object);
            query.Alias = null;

            var result = formatter.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("query AS [q1]");
        }

        [TestMethod]
        public void ShouldPreserveAliasIfAssigned()
        {
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var selectioItem = new Mock<ISelectionItem>();
            selectioItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });
            var formatter = new BoundSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new BoundSelectionItem(Property, selectioItem.Object);

            var result = formatter.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("query AS [Context]");
        }

        [TestMethod]
        public void ShouldUsePrefixesFormContext()
        {
            var context = new Mock<IFormattingContext>();
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            selectionProcessingContext.Setup(x => x.AliasPrefixes)
                .Returns(new[] { "first", "second" });
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var selectioItem = new Mock<ISelectionItem>();
            selectioItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });
            var formatter = new BoundSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new BoundSelectionItem(Property, selectioItem.Object);

            var result = formatter.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("query AS [first.second.Context]");
        }
    }
}