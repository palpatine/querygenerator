﻿using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ParameterFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldFormatParameterAsAlias()
        {
            var query = new LinqParameterReference(
                Expression.Constant(3),
                typeof(int),
                new TsqlType(TsqlNativeTypesNames.Int));
            var context = new Mock<IFormattingContext>();
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias(query)).Returns("@param1");
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            var handler = new LinqParameterReferenceTsqFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("@param1");
        }

        [TestMethod]
        public void ShouldSetParameterInResult()
        {
            var query = new LinqParameterReference(
                Expression.Constant(3),
                typeof(int),
                new TsqlType(TsqlNativeTypesNames.Int));
            var context = new Mock<IFormattingContext>();
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias(query)).Returns("@param1");
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            var handler = new LinqParameterReferenceTsqFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Parameters.Count().ShouldBeEqual(1);
            result.Parameters.Single().ShouldBeTheSameInstance(query);
        }
    }
}