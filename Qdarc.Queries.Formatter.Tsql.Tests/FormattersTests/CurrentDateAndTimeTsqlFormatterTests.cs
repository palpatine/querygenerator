﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class CurrentDateAndTimeTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldFormatUtcDate()
        {
            var handler = new CurrentDateAndTimeTsqlFormatter();
            var expression = new CurrentDateAndTimeExpression(isUtc: true, type: new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, _visitor.Object);

            result.Query.ShouldBeEqual("GETUTCDATE()");
        }

        [TestMethod]
        public void ShouldFormatLocalDate()
        {
            var handler = new CurrentDateAndTimeTsqlFormatter();
            var expression = new CurrentDateAndTimeExpression(isUtc: false, type: new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, _visitor.Object);

            result.Query.ShouldBeEqual("GETDATE()");
        }
    }
}
