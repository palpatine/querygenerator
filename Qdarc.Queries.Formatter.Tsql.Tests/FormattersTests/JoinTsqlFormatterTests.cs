﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class JoinTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldCombineParametersForConditionalJoin()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new InnerJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var leftParameter = new Mock<IParameter>().Object;
            var rightParameter = new Mock<IParameter>().Object;
            var onParameter = new Mock<IParameter>().Object;
            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Parameters = new[]
                        {
                            leftParameter
                        }
                    });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Parameters = new[]
                        {
                            rightParameter
                        }
                    });
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Parameters = new[]
                        {
                            onParameter
                        }
                    });
            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Parameters.Count().ShouldBeEqual(3);
            result.Parameters.First().ShouldBeTheSameInstance(leftParameter);
            result.Parameters.ElementAt(1).ShouldBeTheSameInstance(rightParameter);
            result.Parameters.Last().ShouldBeTheSameInstance(onParameter);
        }

        [TestMethod]
        public void ShouldCombineParametersForCrossJoin()
        {
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new CrossJoinSource(
                left.Object,
                right.Object);

            var leftParameter = new Mock<IParameter>().Object;
            var rightParameter = new Mock<IParameter>().Object;
            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Parameters = new[]
                        {
                            leftParameter
                        }
                    });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Parameters = new[]
                        {
                            rightParameter
                        }
                    });
            var handler = new CrossJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Parameters.Count().ShouldBeEqual(2);
            result.Parameters.First().ShouldBeTheSameInstance(leftParameter);
            result.Parameters.Last().ShouldBeTheSameInstance(rightParameter);
        }

        [TestMethod]
        public void ShouldFormatCrossJoin()
        {
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new CrossJoinSource(
                left.Object,
                right.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Left"
                    });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Right"
                    });
            var handler = new CrossJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("Left CROSS JOIN Right");
        }

        [TestMethod]
        public void ShouldFormatFullJoin()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new FullJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Left"
                    });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Right"
                    });
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Condition"
                    });

            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("Left FULL JOIN Right ON (Condition)");
        }

        [TestMethod]
        public void ShouldFormatInnerJoin()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new InnerJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Left"
                    });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Right"
                    });
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Condition"
                    });

            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("Left INNER JOIN Right ON (Condition)");
        }

        [TestMethod]
        public void ShouldFormatLeftJoin()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new LeftJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Left"
                    });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Right"
                    });
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Condition"
                    });

            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("Left LEFT JOIN Right ON (Condition)");
        }

        [TestMethod]
        public void ShouldFormatRightJoin()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new RightJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Left"
                    });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Right"
                    });
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Condition"
                    });

            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("Left RIGHT JOIN Right ON (Condition)");
        }

        [TestMethod]
        public void ShouldVisitLeftForConditionalJoin()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new InnerJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            left.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitLeftForCrossJoin()
        {
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new CrossJoinSource(
                left.Object,
                right.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            var handler = new CrossJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            left.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitOn()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new InnerJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            sqlLogicalExpression.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitRightForConditionalJoin()
        {
            var sqlLogicalExpression = new Mock<ISqlLogicalExpression>();
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new InnerJoinSource(
                left.Object,
                right.Object,
                sqlLogicalExpression.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            sqlLogicalExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            var handler = new ConditionalJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            sqlLogicalExpression.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitRightForCrossJoin()
        {
            var left = new Mock<INamedSource>();
            var right = new Mock<INamedSource>();
            var query = new CrossJoinSource(
                left.Object,
                right.Object);

            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            var handler = new CrossJoinSourceTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            right.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }
    }
}