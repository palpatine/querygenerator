﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class AliasedSelectionItemFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldVisitItemUnderAlias()
        {
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            var context = new Mock<IFormattingContext>();
            var modelConvention = new Mock<IModelToSqlConvention>();
            modelConvention.Setup(x => x.CompoundColumnSeparator).Returns(".");
            context.Setup(x => x.ModelToSqlConvention).Returns(modelConvention.Object);
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var selectionItem = new Mock<ISelectionItem>();
            selectionItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });

            var formatter = new AliasedSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new AliasedSelectionItem(selectionItem.Object);

            formatter.Handle(query, _visitor.Object);

            selectionItem.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignAliasIfNotAssigned()
        {
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            var context = new Mock<IFormattingContext>();
            var modelConvention = new Mock<IModelToSqlConvention>();
            modelConvention.Setup(x => x.CompoundColumnSeparator).Returns(".");
            context.Setup(x => x.ModelToSqlConvention).Returns(modelConvention.Object);
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var selectionItem = new Mock<ISelectionItem>();
            selectionItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });

            var formatter = new AliasedSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new AliasedSelectionItem(selectionItem.Object);

            var result = formatter.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("query AS [q1]");
        }

        [TestMethod]
        public void ShouldPreserveAliasIfAssigned()
        {
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            var context = new Mock<IFormattingContext>();
            var modelConvention = new Mock<IModelToSqlConvention>();
            modelConvention.Setup(x => x.CompoundColumnSeparator).Returns(".");
            context.Setup(x => x.ModelToSqlConvention).Returns(modelConvention.Object);
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var selectionItem = new Mock<ISelectionItem>();
            selectionItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });
            var formatter = new AliasedSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new AliasedSelectionItem(selectionItem.Object)
            {
                Alias = "a"
            };

            var result = formatter.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("query AS [a]");
        }

        [TestMethod]
        public void ShouldUsePrefixesFormContext()
        {
            var context = new Mock<IFormattingContext>();
            var modelConvention = new Mock<IModelToSqlConvention>();
            modelConvention.Setup(x => x.CompoundColumnSeparator).Returns(".");
            context.Setup(x => x.ModelToSqlConvention).Returns(modelConvention.Object);
            var selectionProcessingContext = new Mock<ISelectionProcessingContext>();
            selectionProcessingContext.Setup(x => x.AliasPrefixes)
                .Returns(new[] { "first", "second" });
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(selectionProcessingContext.Object);
            var aliasManager = new Mock<IAliasManager>();
            aliasManager.Setup(x => x.GetAlias())
                .Returns("q1");
            context.Setup(x => x.AliasManager)
                .Returns(aliasManager.Object);
            var aliasedItem = new Mock<ISelectionItem>();
            aliasedItem.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "query"
                });
            var formatter = new AliasedSelectionItemTsqlFormatter();
            formatter.AttachContext(context.Object);
            var query = new AliasedSelectionItem(aliasedItem.Object)
            {
                Alias = "a"
            };

            var result = formatter.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("query AS [first.second.a]");
        }
    }
}