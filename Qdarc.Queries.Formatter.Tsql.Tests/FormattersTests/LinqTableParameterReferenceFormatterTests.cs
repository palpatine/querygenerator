﻿using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class LinqTableParameterReferenceFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldGetAliasForTableParameter()
        {
            var context = new Mock<IFormattingContext>();
            var aliasManager = new Mock<IAliasManager>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            var expression = new LinqTableParameterReference(
                Expression.Constant(5),
                typeof(int),
                new CustomTypeReference(new ElementName("type")));
            var formater = new LinqTableParameterReferenceFormatter();
            formater.AttachContext(context.Object);

            formater.Handle(expression, _visitor.Object);

            aliasManager.Verify(x => x.GetAlias(expression), Times.Once);
        }

        [TestMethod]
        public void ShouldGenerateQueryAsParameterName()
        {
            var context = new Mock<IFormattingContext>();
            var aliasManager = new Mock<IAliasManager>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            var expression = new LinqTableParameterReference(
                Expression.Constant(5),
                typeof(int),
                new CustomTypeReference(new ElementName("type")));
            aliasManager.Setup(x => x.GetAlias(expression)).Returns("alias");
            var formater = new LinqTableParameterReferenceFormatter();
            formater.AttachContext(context.Object);

            var result = formater.Handle(expression, _visitor.Object);
            result.Query.ShouldBeEqual("alias");
        }

        [TestMethod]
        public void ShouldResolveCustomTypeReference()
        {
            var context = new Mock<IFormattingContext>();
            var aliasManager = new Mock<IAliasManager>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            var expression = new LinqTableParameterReference(
                Expression.Constant(5),
                typeof(int),
                new CustomTypeReference(new ElementName("custom", "type")));
            aliasManager.Setup(x => x.GetAlias(expression)).Returns("alias");
            var formater = new LinqTableParameterReferenceFormatter();
            formater.AttachContext(context.Object);

            var result = formater.Handle(expression, _visitor.Object);
            var parameterType = result.Parameters.Single()
                .ShouldBeInstanceOf<LinqTableParameterReference>()
                .SqlType
                .ShouldBeInstanceOf<TypeReference>();
            parameterType.Name.ShouldBeEqual("[custom].[type]");
        }
    }
}
