using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class OrLogicalConjunctionExpressionTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldFormat()
        {
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var query = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Left" });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Right" });

            var handler = new OrLogicalConjunctionExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("(Left) OR (Right)");
        }

        [TestMethod]
        public void ShouldCombineParameters()
        {
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var query = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var leftParameter = new Mock<IParameter>().Object;
            var rightParameter = new Mock<IParameter>().Object;
            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Left", Parameters = new[] { leftParameter } });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Right", Parameters = new[] { rightParameter } });

            var handler = new OrLogicalConjunctionExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Parameters.Count().ShouldBeEqual(2);
            result.Parameters.First().ShouldBeTheSameInstance(leftParameter);
            result.Parameters.Last().ShouldBeTheSameInstance(rightParameter);
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var query = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Left" });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Right" });

            var handler = new OrLogicalConjunctionExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            left.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void AndShouldVisitRight()
        {
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var query = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var context = new Mock<IFormattingContext>();
            left.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Left" });
            right.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Right" });

            var handler = new OrLogicalConjunctionExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            right.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }
    }
}