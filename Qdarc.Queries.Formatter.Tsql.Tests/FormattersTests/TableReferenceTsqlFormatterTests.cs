using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Formatter.Tsql.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class TableReferenceTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void TableReference()
        {
            var query = new TableReference(new ElementName("Name"), typeof(UserEntity));
            var context = new Mock<IFormattingContext>();
            var model = new Mock<IModelToSqlConvention>();
            model.Setup(x => x.GetTableName(It.IsAny<Type>())).Returns(new ElementName("Common", "User"));
            context.Setup(x => x.ModelToSqlConvention).Returns(model.Object);
            var handler = new TableReferenceTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("[Common].[User]");
        }
    }
}