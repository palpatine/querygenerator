﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ValuesSourceFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldFormatSimpleTypeSingleValue()
        {
            var value = new Mock<ISqlValueExpression>();
            var rows = new List<ValueSourceRow>
            {
                new ValueSourceRow(new[]
                    {
                        value.Object
                    })
            };
            var source = new ValuesSource(rows, typeof(int));
            var context = new Mock<IFormattingContext>();
            value.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery { Query = "1" });
            var formatter = new ValuesSourceTsqlFormatter();
            formatter.AttachContext(context.Object);
            var result = formatter.Handle(source, _visitor.Object);
            result.Query.ShouldBeEqual("VALUES (1)");
        }

        [TestMethod]
        public void ShouldFormatSimpleTypeMultipleValues()
        {
            var valueA = new Mock<ISqlValueExpression>();
            var valueB = new Mock<ISqlValueExpression>();
            var rows = new List<ValueSourceRow>
            {
                new ValueSourceRow(new[]
                    {
                        valueA.Object,
                    }),
                new ValueSourceRow(new[]
                    {
                        valueB.Object
                    })
            };
            var source = new ValuesSource(rows, typeof(int));

            var context = new Mock<IFormattingContext>();
            valueA.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery { Query = "N'a'" });
            valueB.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "N'b'" });
            var formatter = new ValuesSourceTsqlFormatter();
            formatter.AttachContext(context.Object);
            var result = formatter.Handle(source, _visitor.Object);
            result.Query.ShouldBeEqual("VALUES (N'a'), (N'b')");
        }

        [TestMethod]
        public void ShouldFormatComplexTypeSingleValue()
        {
            var valueA = new Mock<ISqlValueExpression>();
            var valueB = new Mock<ISqlValueExpression>();
            var valueC = new Mock<ISqlValueExpression>();
            var rows = new List<ValueSourceRow>
            {
                new ValueSourceRow(new[]
                    {
                        valueA.Object,
                        valueB.Object,
                        valueC.Object,
                    })
            };
            var source = new ValuesSource(rows, typeof(int));
            var context = new Mock<IFormattingContext>();
            valueA.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery { Query = "200" });
            valueB.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "1" });
            valueC.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                                .Returns(new FormattedQuery { Query = "10" });
            var formatter = new ValuesSourceTsqlFormatter();
            formatter.AttachContext(context.Object);
            var result = formatter.Handle(source, _visitor.Object);
            result.Query.ShouldBeEqual("VALUES (200, 1, 10)");
        }

        [TestMethod]
        public void ShouldFormatComplexTypeMultipleValues()
        {
            var valueA1 = new Mock<ISqlValueExpression>();
            var valueB1 = new Mock<ISqlValueExpression>();
            var valueC1 = new Mock<ISqlValueExpression>();
            var valueA2 = new Mock<ISqlValueExpression>();
            var valueB2 = new Mock<ISqlValueExpression>();
            var valueC2 = new Mock<ISqlValueExpression>();
            var rows = new List<ValueSourceRow>
            {
                new ValueSourceRow(new[]
                    {
                        valueA1.Object,
                        valueB1.Object,
                        valueC1.Object,
                    }),
                new ValueSourceRow(new[]
                    {
                        valueA2.Object,
                        valueB2.Object,
                        valueC2.Object,
                    }),
            };
            var source = new ValuesSource(rows, typeof(int));

            var context = new Mock<IFormattingContext>();
            valueA1.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery { Query = "200" });
            valueB1.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "1" });
            valueC1.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                                .Returns(new FormattedQuery { Query = "10" });

            valueA2.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery { Query = "201" });
            valueB2.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "5" });
            valueC2.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                                .Returns(new FormattedQuery { Query = "50" });

            var formatter = new ValuesSourceTsqlFormatter();
            formatter.AttachContext(context.Object);
            var result = formatter.Handle(source, _visitor.Object);
            result.Query.ShouldBeEqual("VALUES (200, 1, 10), (201, 5, 50)");
        }

        [TestMethod]
        public void ShouldHandleValuesSource()
        {
            var rows = new List<ValueSourceRow>
            {
                new ValueSourceRow(new[] { new Mock<ISqlValueExpression>().Object })
            };
            var source = new ValuesSource(rows, typeof(int));
            var context = new Mock<IFormattingContext>();
            var formatter = new ValuesSourceTsqlFormatter();
            formatter.AttachContext(context.Object);
            formatter.CanHandle(source).ShouldBeTrue();
        }
    }
}
