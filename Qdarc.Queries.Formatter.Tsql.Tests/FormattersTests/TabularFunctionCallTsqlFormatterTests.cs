﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Formatter.Tsql.Tests.TestModel;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class TabularFunctionCallTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldBeAbleToFormatTabularFunctionCall()
        {
            var item = new TabularFunctionCall(new ElementName("Name"), new[] { new Mock<ISqlValueExpression>().Object }, typeof(UserEntity));
            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            var handler = new TabularFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            handler.CanHandle(item).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldFormatTabularFunctionCallWithOneArgument()
        {
            var argument = new Mock<ISqlValueExpression>();
            var call = new TabularFunctionCall(
                new ElementName("name"),
                new[] { argument.Object },
                typeof(UserEntity));

            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            argument.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "first"
                });

            var handler = new TabularFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("[name](first)");
        }

        [TestMethod]
        public void ShouldFormatTabularFunctionCallWithOutArguments()
        {
            var call = new TabularFunctionCall(
                new ElementName("name"),
                Enumerable.Empty<ISqlValueExpression>(),
                typeof(UserEntity));

            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            var handler = new TabularFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("[name]()");
        }

        [TestMethod]
        public void ShouldFormatTabularFunctionCallWithTwoArgument()
        {
            var argument1 = new Mock<ISqlValueExpression>();
            var argument2 = new Mock<ISqlValueExpression>();
            var call = new TabularFunctionCall(
                new ElementName("name"),
                new[] { argument1.Object, argument2.Object },
                typeof(UserEntity));

            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            argument1.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "first"
                    });
            argument2.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "second"
                    });

            var handler = new TabularFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("[name](first, second)");
        }

        [TestMethod]
        public void ShouldGatherParametersWhileFormatting()
        {
            var argument1 = new Mock<ISqlValueExpression>();
            var argument2 = new Mock<ISqlValueExpression>();
            var call = new TabularFunctionCall(
                new ElementName("name"),
                new[]
                {
                    argument1.Object,
                    argument2.Object
                },
                typeof(UserEntity));

            var firstParameters = new[] { new Mock<IParameter>().Object };
            var secondParameters = new[] { new Mock<IParameter>().Object };
            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            argument1.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "first",
                        Parameters = firstParameters
                    });
            argument2.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "second",
                        Parameters = secondParameters
                    });

            var handler = new TabularFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Parameters.AllCorrespondingElementsShouldBeEqual(firstParameters.Concat(secondParameters));
        }
    }
}