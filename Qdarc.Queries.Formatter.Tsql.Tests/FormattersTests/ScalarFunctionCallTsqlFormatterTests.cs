﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ScalarFunctionCallTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldBeAbleToFormatScalarFunctionCall()
        {
            var argument = new Mock<ISqlValueExpression>();
            var call = new ScalarFunctionCall(
                new ElementName("name"),
                new[] { argument.Object },
                null,
                null);
            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            var handler = new ScalarFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            handler.CanHandle(call).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldFormatScalarFunctionCallWithOneArgument()
        {
            var argument = new Mock<ISqlValueExpression>();
            var call = new ScalarFunctionCall(
                new ElementName("name"),
                new[] { argument.Object },
                null,
                null);

            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            argument.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "first"
                });

            var handler = new ScalarFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("[name](first)");
        }

        [TestMethod]
        public void ShouldFormatScalarFunctionCallWithOutArguments()
        {
            var call = new ScalarFunctionCall(
                new ElementName("name"),
                Enumerable.Empty<ISqlValueExpression>(),
                null,
                null);

            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            var handler = new ScalarFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("[name]()");
        }

        [TestMethod]
        public void ShouldFormatScalarFunctionCallWithTwoArgument()
        {
            var argument1 = new Mock<ISqlValueExpression>();
            var argument2 = new Mock<ISqlValueExpression>();
            var call = new ScalarFunctionCall(
                new ElementName("name"),
                new[] { argument1.Object, argument2.Object },
                null,
                null);

            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            argument1.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "first"
                    });
            argument2.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "second"
                    });

            var handler = new ScalarFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("[name](first, second)");
        }

        [TestMethod]
        public void ShouldGatherParametersWhileFormatting()
        {
            var argument1 = new Mock<ISqlValueExpression>();
            var argument2 = new Mock<ISqlValueExpression>();
            var call = new ScalarFunctionCall(
                new ElementName("name"),
                new[] { argument1.Object, argument2.Object },
                null,
                null);

            var firstParameters = new[] { new Mock<IParameter>().Object };
            var secondParameters = new[] { new Mock<IParameter>().Object };
            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            argument1.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "first",
                        Parameters = firstParameters
                    });
            argument2.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                    .Returns(new FormattedQuery
                    {
                        Query = "second",
                        Parameters = secondParameters
                    });

            var handler = new ScalarFunctionCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Parameters.AllCorrespondingElementsShouldBeEqual(firstParameters.Concat(secondParameters));
        }
    }
}