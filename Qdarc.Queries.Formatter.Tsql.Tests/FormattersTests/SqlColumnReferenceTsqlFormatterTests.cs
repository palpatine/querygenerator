﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class SqlColumnReferenceTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldFormatSqlColumnReference()
        {
            var item = new SqlColumnReference(new ElementName("A", "B", "C"), null, null, null);
            var handler = new SqlColumnReferenceTsqlFormatter();
            var result = handler.Handle(item, _visitor.Object);
            result.Query.ShouldBeEqual("[A].[B].[C]");
        }

        [TestMethod]
        public void ShouldHandleSqlColumnReference()
        {
            var item = new SqlColumnReference(new ElementName("Name"), null, null, null);
            var handler = new SqlColumnReferenceTsqlFormatter();
            handler.CanHandle(item).ShouldBeTrue();
        }
    }
}