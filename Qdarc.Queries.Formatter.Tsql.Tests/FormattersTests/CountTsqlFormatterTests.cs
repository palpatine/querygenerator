﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class CountTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldBeAbleToFormatCountExpression()
        {
            var context = new Mock<IFormattingContext>();
            var handler = new CountTsqlFormatter();
            handler.AttachContext(context.Object);
            handler.CanHandle(new SqlCountExpression(new Mock<ISqlValueExpression>().Object, false, null, null)).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldFormatCountValue()
        {
            var value = new Mock<ISqlValueExpression>();
            var count = new SqlCountExpression(
                value.Object,
                false,
                typeof(int),
                TsqlTypeProvider.Int);

            var context = new Mock<IFormattingContext>();
            value.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "<INNER QUERY>"
                });
            var handler = new CountTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(count, _visitor.Object);
            result.Query.ShouldBeEqual("COUNT(<INNER QUERY>)");
        }

        [TestMethod]
        public void ShouldFormatCountDistinctValue()
        {
            var value = new Mock<ISqlValueExpression>();
            var count = new SqlCountExpression(
                value.Object,
                true,
                typeof(int),
                TsqlTypeProvider.Int);

            var context = new Mock<IFormattingContext>();
            value.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery
                {
                    Query = "<INNER QUERY>"
                });
            var handler = new CountTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(count, _visitor.Object);
            result.Query.ShouldBeEqual("COUNT(DISTINCT <INNER QUERY>)");
        }
    }
}