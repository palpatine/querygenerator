﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class AsteriskSelectionTsqlFormatterTests
    {
        [TestMethod]
        public void ShouldReturnAsteriskIfNoSourceProvided()
        {
            var formatter = new AsteriskSelectionTsqlFormatter();
            var selection = formatter.Handle(
                new AsteriskSelection(null, null, null),
                new Mock<ITransformer>().Object);
            selection.Query.ShouldBeEqual("*");
        }

        [TestMethod]
        public void ShouldReturnAsteriskPrefixedWithSourceIfSourceProvided()
        {
            var formatter = new AsteriskSelectionTsqlFormatter();
            var selection = formatter.Handle(
                new AsteriskSelection(new AliasedSourceItem(new Mock<ISource>().Object) { Alias = "alias" }, null, null),
                new Mock<ITransformer>().Object);
            selection.Query.ShouldBeEqual("[alias].*");
        }
    }
}
