﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class PrefixedSelectionTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldRegisterPrefixInContext()
        {
            var context = new Mock<IFormattingContext>();
            var currentSelectionProcessingContext = new Mock<ISelectionProcessingContext>();
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(currentSelectionProcessingContext.Object);
            currentSelectionProcessingContext.Setup(x => x.AppendPrefix("prefix"))
                .Returns(new Mock<IDisposable>().Object);
            var formatter = new PrefixedSelectionTsqlFormatter();
            formatter.AttachContext(context.Object);
            var selection = new PrefixedSelection("prefix", new Mock<ISqlSelection>().Object);
            formatter.Handle(selection, _visitor.Object);

            currentSelectionProcessingContext.Verify(x => x.AppendPrefix("prefix"), Times.Once);
        }

        [TestMethod]
        public void ShouldProcessInnerSelectionWithinThePrefixContext()
        {
            var context = new Mock<IFormattingContext>();
            var currentSelectionProcessingContext = new Mock<ISelectionProcessingContext>();
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(currentSelectionProcessingContext.Object);
            var prefixContextDisposer = new Mock<IDisposable>();
            currentSelectionProcessingContext.Setup(x => x.AppendPrefix("prefix"))
                .Returns(prefixContextDisposer.Object);
            var formatter = new PrefixedSelectionTsqlFormatter();
            formatter.AttachContext(context.Object);
            var sqlSelection = new Mock<ISqlSelection>();
            var selection = new PrefixedSelection("prefix", sqlSelection.Object);

            sqlSelection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Callback((ITransformer z) => prefixContextDisposer.Verify(x => x.Dispose(), Times.Never));

            formatter.Handle(selection, _visitor.Object);

            sqlSelection.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldClosePrefixContext()
        {
            var context = new Mock<IFormattingContext>();
            var currentSelectionProcessingContext = new Mock<ISelectionProcessingContext>();
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(currentSelectionProcessingContext.Object);
            var prefixContextDisposer = new Mock<IDisposable>();
            currentSelectionProcessingContext.Setup(x => x.AppendPrefix("prefix"))
                .Returns(prefixContextDisposer.Object);
            var formatter = new PrefixedSelectionTsqlFormatter();
            formatter.AttachContext(context.Object);
            var sqlSelection = new Mock<ISqlSelection>();
            var selection = new PrefixedSelection("prefix", sqlSelection.Object);

            formatter.Handle(selection, _visitor.Object);

            prefixContextDisposer.Verify(x => x.Dispose(), Times.Once);
        }

        [TestMethod]
        public void ShouldReturnInnerSelectionProcessingResult()
        {
            var context = new Mock<IFormattingContext>();
            var currentSelectionProcessingContext = new Mock<ISelectionProcessingContext>();
            context.Setup(x => x.CurrentSelectionProcessingContext)
                .Returns(currentSelectionProcessingContext.Object);
            var prefixContextDisposer = new Mock<IDisposable>();
            currentSelectionProcessingContext.Setup(x => x.AppendPrefix("prefix"))
                .Returns(prefixContextDisposer.Object);
            var formatter = new PrefixedSelectionTsqlFormatter();
            formatter.AttachContext(context.Object);
            var sqlSelection = new Mock<ISqlSelection>();
            var selection = new PrefixedSelection("prefix", sqlSelection.Object);
            var formatingResult = new FormattedQuery();
            sqlSelection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(formatingResult);

            var result = formatter.Handle(selection, _visitor.Object);

            result.ShouldBeTheSameInstance(formatingResult);
        }
    }
}
