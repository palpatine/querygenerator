﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ConstantExpressionTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldNotAllowNullValue()
        {
            var query = new SqlConstantExpression(
                null,
                typeof(bool),
                new TsqlType(TsqlNativeTypesNames.Bit));
            var context = new Mock<IFormattingContext>();
            var handler = new ConstantExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            QAssert.ShouldThrow<InvalidOperationException>(() => handler.Handle(query, _visitor.Object));
        }

        [TestMethod]
        public void Bit()
        {
            var query = new SqlConstantExpression(
                true,
                typeof(bool),
                new TsqlType(TsqlNativeTypesNames.Bit));

            var context = new Mock<IFormattingContext>();
            var handler = new ConstantExpressionTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("CAST(1 AS BIT)");
        }

        [TestMethod]
        public void Char()
        {
            var query = new SqlConstantExpression(
                'a',
                typeof(char),
                new TsqlType(TsqlNativeTypesNames.Char));

            var context = new Mock<IFormattingContext>();
            var handler = new ConstantExpressionTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("'a'");
        }

        [TestMethod]
        public void Int()
        {
            var query = new SqlConstantExpression(
                3,
                typeof(int),
                new TsqlType(TsqlNativeTypesNames.Int));

            var context = new Mock<IFormattingContext>();
            var handler = new ConstantExpressionTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("3");
        }

        [TestMethod]
        public void NVarChar()
        {
            var query = new SqlConstantExpression(
                "abc",
                typeof(string),
                new TsqlType(TsqlNativeTypesNames.NVarChar, 20));

            var context = new Mock<IFormattingContext>();
            var handler = new ConstantExpressionTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("N'abc'");
        }

        [TestMethod]
        public void VarChar()
        {
            var query = new SqlConstantExpression(
                "abc",
                typeof(string),
                new TsqlType(TsqlNativeTypesNames.VarChar, 20));

            var context = new Mock<IFormattingContext>();
            var handler = new ConstantExpressionTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("'abc'");
        }
    }
}