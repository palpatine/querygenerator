﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class CastExpressionTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldVisitBaseExpression()
        {
            var castedExpression = new Mock<ISqlValueExpression>();
            var query = new SqlCastExpression(
                castedExpression.Object,
                typeof(bool),
                new TsqlType(TsqlNativeTypesNames.Bit));

            var context = new Mock<IFormattingContext>();
            castedExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            var handler = new CastExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            castedExpression.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldForwardParameters()
        {
            var castedExpression = new Mock<ISqlValueExpression>();
            var query = new SqlCastExpression(
                castedExpression.Object,
                typeof(bool),
                new TsqlType(TsqlNativeTypesNames.Bit));

            var context = new Mock<IFormattingContext>();
            var linqParameterReferences = new List<LinqParameterReference>();
            castedExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Parameters = linqParameterReferences });
            var handler = new CastExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Parameters.ShouldBeTheSameInstance(linqParameterReferences);
        }

        [TestMethod]
        public void ShouldFormatQuery()
        {
            var castedExpression = new Mock<ISqlValueExpression>();
            var query = new SqlCastExpression(
                castedExpression.Object,
                typeof(bool),
                new TsqlType(TsqlNativeTypesNames.Bit));
            var context = new Mock<IFormattingContext>();
            castedExpression.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "base" });
            var handler = new CastExpressionTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("CAST(base AS BIT)");
        }
    }
}