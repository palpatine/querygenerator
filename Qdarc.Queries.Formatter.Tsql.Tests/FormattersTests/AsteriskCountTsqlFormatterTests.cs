﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class AsteriskCountTsqlFormatterTests
    {
        [TestMethod]
        public void ShouldFormatCountStar()
        {
            var count = new SqlAsteriskCountExpression(
                typeof(int),
                TsqlTypeProvider.Int);

            var context = new Mock<IFormattingContext>();
            var handler = new AsteriskCountTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(count, new Mock<ITransformer>().Object);
            result.Query.ShouldBeEqual("COUNT(*)");
        }
    }
}