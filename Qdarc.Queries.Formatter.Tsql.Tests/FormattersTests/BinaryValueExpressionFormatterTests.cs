﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class BinaryValueExpressionFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleBinaryValueExpression()
        {
            var item = new AddValueExpression(
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object,
                null,
                null);
            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);

            handler.CanHandle(item)
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldDelegateProcessingOfLeftOperand()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new SubtractValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(item, _visitor.Object);

            leftValue.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldDelegateProcessingOfRightOperand()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new SubtractValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });

            handler.Handle(item, _visitor.Object);

            rightValue.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldFormatAddExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new AddValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 + 2");
        }

        [TestMethod]
        public void ShouldFormatBitwiseAndExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new BitwiseAndValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 & 2");
        }

        [TestMethod]
        public void ShouldFormatBitwiseExclusiveOrExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new BitwiseExclusiveOrValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 ^ 2");
        }

        [TestMethod]
        public void ShouldFormatBitwiseOrExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new BitwiseOrValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 | 2");
        }

        [TestMethod]
        public void ShouldFormatDivideExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new DivideValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 / 2");
        }

        [TestMethod]
        public void ShouldFormatModuloExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new ModuloValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 % 2");
        }

        [TestMethod]
        public void ShouldFormatMultiplyExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new MultiplyValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 * 2");
        }

        [TestMethod]
        public void ShouldFormatSubtractExpression()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new SubtractValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1" });
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2" });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(item, _visitor.Object);

            result.Query.ShouldBeEqual("1 - 2");
        }

        [TestMethod]
        public void ShouldGatherParameters()
        {
            var leftValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var rightValue = new Mock<ISqlValueExpression>(MockBehavior.Strict);
            var item = new SubtractValueExpression(
                leftValue.Object,
                rightValue.Object,
                null,
                null);
            var context = new Mock<IFormattingContext>();
            var leftParameter = new Mock<IParameter>().Object;
            leftValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "1", Parameters = new[] { leftParameter } });
            var rightParameter = new Mock<IParameter>().Object;
            rightValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "2", Parameters = new[] { rightParameter } });
            var handler = new BinaryValueExpressionFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(item, _visitor.Object);

            result.Parameters.AllCorrespondingElementsShouldBeEqual(new[] { leftParameter, rightParameter });
        }
    }
}