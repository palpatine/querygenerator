﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Formatter.Tsql.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Utilities;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class LinqColumnReferenceTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldGetColumnNameFromModelToSqlConvention()
        {
            var context = new Mock<IFormattingContext>();
            var modelToSqlConvention = new Mock<IModelToSqlConvention>(MockBehavior.Strict);
            context.Setup(x => x.ModelToSqlConvention).Returns(modelToSqlConvention.Object);
            var formatter = new LinqColumnReferenceTsqlFormatter();
            formatter.AttachContext(context.Object);
            var source = new Mock<INamedSource>().Object;
            var property = ExpressionExtensions.GetProperty((UserEntity e) => e.Id);
            modelToSqlConvention.Setup(x => x.GetColumnName(source, property))
                .Returns(new ElementName("name"));
            var columnReference = new LinqColumnReference(
                source,
                property,
                typeof(int),
                new Mock<ISqlType>().Object);

            formatter.Handle(columnReference, _visitor.Object);

            modelToSqlConvention.Verify(x => x.GetColumnName(source, property), Times.Once);
        }

        [TestMethod]
        public void ShouldFormatColumnName()
        {
            var context = new Mock<IFormattingContext>();
            var modelToSqlConvention = new Mock<IModelToSqlConvention>();
            context.Setup(x => x.ModelToSqlConvention).Returns(modelToSqlConvention.Object);
            var formatter = new LinqColumnReferenceTsqlFormatter();
            formatter.AttachContext(context.Object);
            var source = new Mock<INamedSource>().Object;
            var property = ExpressionExtensions.GetProperty((UserEntity e) => e.Id);
            modelToSqlConvention.Setup(x => x.GetColumnName(source, property))
                .Returns(new ElementName("schema", "table", "name"));
            var columnReference = new LinqColumnReference(
                source,
                property,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = formatter.Handle(columnReference, _visitor.Object);
            result.Query.ShouldBeEqual("[schema].[table].[name]");
        }
    }
}
