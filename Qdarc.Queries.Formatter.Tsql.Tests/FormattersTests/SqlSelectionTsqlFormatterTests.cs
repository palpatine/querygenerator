﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class SqlSelectionTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldVisitEachProjectionElement()
        {
            var context = new Mock<IFormattingContext>();
            var intConstant = new Mock<ISqlValueExpression>();
            var stringConstant = new Mock<ISqlValueExpression>();
            var projection = new SqlSelection(new[] { intConstant.Object, stringConstant.Object });
            intConstant.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            stringConstant.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());

            var handler = new SqlSelectionTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(projection, _visitor.Object);

            intConstant.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
            stringConstant.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitAggregateItemsQueries()
        {
            var context = new Mock<IFormattingContext>();
            var intConstant = new Mock<ISqlValueExpression>();
            var stringConstant = new Mock<ISqlValueExpression>();
            var projection = new SqlSelection(new[] { intConstant.Object, stringConstant.Object });
            intConstant.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "INT" });
            stringConstant.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "STRING" });

            var handler = new SqlSelectionTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(projection, _visitor.Object);
            result.Query.ShouldBeEqual("INT,STRING");
        }

        [TestMethod]
        public void ShouldVisitAggregateParameters()
        {
            var context = new Mock<IFormattingContext>();
            var intConstant = new Mock<ISqlValueExpression>();
            var stringConstant = new Mock<ISqlValueExpression>();
            var projection = new SqlSelection(new[] { intConstant.Object, stringConstant.Object });
            var firstParameter = new Mock<IParameter>();
            var secondParameter = new Mock<IParameter>();
            intConstant.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Parameters = new[] { firstParameter.Object } });
            stringConstant.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Parameters = new[] { secondParameter.Object } });

            var handler = new SqlSelectionTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(projection, _visitor.Object);
            result.Parameters.AllCorrespondingElementsShouldBeEqual(new[] { firstParameter.Object, secondParameter.Object });
        }
    }
}
