﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ScriptTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldVisitEachScriptElement()
        {
            var context = new Mock<IFormattingContext>();
            var firstCommand = new Mock<ISqlCommand>();
            var secondCommand = new Mock<ISqlCommand>();
            var script = new SqlScript(new[] { firstCommand.Object, secondCommand.Object }, null);
            firstCommand.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());
            secondCommand.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery());

            var handler = new ScriptTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(script, _visitor.Object);

            firstCommand.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
            secondCommand.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitAggregateItemsQueries()
        {
            var context = new Mock<IFormattingContext>();
            var firstCommand = new Mock<ISqlCommand>();
            var secondCommand = new Mock<ISqlCommand>();
            var script = new SqlScript(new[] { firstCommand.Object, secondCommand.Object }, null);
            firstCommand.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "FIRST" });
            secondCommand.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "SECOND" });

            var handler = new ScriptTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(script, _visitor.Object);
            result.Query.ShouldBeEqual($"FIRST;{Environment.NewLine}SECOND;");
        }

        [TestMethod]
        public void ShouldVisitAggregateParameters()
        {
            var context = new Mock<IFormattingContext>();
            var firstCommand = new Mock<ISqlCommand>();
            var secondCommand = new Mock<ISqlCommand>();
            var firstParameter = new Mock<IParameter>();
            var secondParameter = new Mock<IParameter>();
            var script = new SqlScript(new[] { firstCommand.Object, secondCommand.Object }, null);
            firstCommand.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Parameters = new[] { firstParameter.Object } });
            secondCommand.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Parameters = new[] { secondParameter.Object } });

            var handler = new ScriptTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(script, _visitor.Object);
            result.Parameters.AllCorrespondingElementsShouldBeEqual(new[] { firstParameter.Object, secondParameter.Object });
        }
    }
}
