﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Utilities;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class SelectCommandTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposed in called method")]
        public void ShouldFormatSelectionWithFilter()
        {
            var selection = new Mock<ISqlSelection>();
            var filter = new Mock<ISqlLogicalExpression>();
            var query = new SelectCommand(
                selection: selection.Object,
                filter: filter.Object);

            var context = new Mock<IFormattingContext>();
            selection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Selection" });
            filter.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Filter" });
            context.Setup(x => x.OpenSelectionProcessingContext())
               .Returns(new Disposer(() => { }));
            var hanlder = new SelectCommandTsqlFormatter();
            hanlder.AttachContext(context.Object);

            var result = hanlder.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("SELECT Selection WHERE (Filter)");
        }

        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposed in called method")]
        public void ShouldFormatSelectionWithSource()
        {
            var selection = new Mock<ISqlSelection>();
            var source = new Mock<ISource>();
            var query = new SelectCommand(
                selection: selection.Object,
                source: source.Object);

            var context = new Mock<IFormattingContext>();
            selection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Selection" });
            source.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Source" });
            context.Setup(x => x.OpenSelectionProcessingContext())
               .Returns(new Disposer(() => { }));
            var hanlder = new SelectCommandTsqlFormatter();
            hanlder.AttachContext(context.Object);

            var result = hanlder.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("SELECT Selection FROM Source");
        }

        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposed in called method")]
        public void ShouldFormatSelectionWithTop()
        {
            var selection = new Mock<ISqlSelection>();
            var topValue = new Mock<ISqlValueExpression>();
            var query = new SelectCommand(
                selection: selection.Object,
                top: new TopExpression(topValue.Object, false, false));

            var context = new Mock<IFormattingContext>();
            selection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Selection" });
            topValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "TopValue" });
            context.Setup(x => x.OpenSelectionProcessingContext())
               .Returns(new Disposer(() => { }));
            var hanlder = new SelectCommandTsqlFormatter();
            hanlder.AttachContext(context.Object);

            var result = hanlder.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("SELECT TOP (TopValue) Selection");
        }

        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposed in called method")]
        public void ShouldFormatSelectionWithTopPercent()
        {
            var selection = new Mock<ISqlSelection>();
            var topValue = new Mock<ISqlValueExpression>();
            var query = new SelectCommand(
                selection: selection.Object,
                top: new TopExpression(
                    topValue.Object,
                    true,
                    false));

            var context = new Mock<IFormattingContext>();
            selection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Selection" });
            topValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "TopValue" });
            context.Setup(x => x.OpenSelectionProcessingContext())
               .Returns(new Disposer(() => { }));
            var hanlder = new SelectCommandTsqlFormatter();
            hanlder.AttachContext(context.Object);

            var result = hanlder.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("SELECT TOP (TopValue) PERCENT Selection");
        }

        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposed in called method")]
        public void ShouldFormatSelectionWithTopPercentWithTies()
        {
            var selection = new Mock<ISqlSelection>();
            var topValue = new Mock<ISqlValueExpression>();
            var query = new SelectCommand(
                selection: selection.Object,
                top: new TopExpression(topValue.Object, true, true));

            var context = new Mock<IFormattingContext>();
            selection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Selection" });
            topValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "TopValue" });
            context.Setup(x => x.OpenSelectionProcessingContext())
               .Returns(new Disposer(() => { }));
            var hanlder = new SelectCommandTsqlFormatter();
            hanlder.AttachContext(context.Object);

            var result = hanlder.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("SELECT TOP (TopValue) PERCENT WITH TIES Selection");
        }

        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposed in called method")]
        public void ShouldFormatSelectionWithTopWithTies()
        {
            var selection = new Mock<ISqlSelection>();
            var topValue = new Mock<ISqlValueExpression>();
            var query = new SelectCommand(
                selection: selection.Object,
                top: new TopExpression(
                    topValue.Object,
                    false,
                    true));

            var context = new Mock<IFormattingContext>();
            selection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Selection" });
            topValue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "TopValue" });
            context.Setup(x => x.OpenSelectionProcessingContext())
               .Returns(new Disposer(() => { }));
            var hanlder = new SelectCommandTsqlFormatter();
            hanlder.AttachContext(context.Object);

            var result = hanlder.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("SELECT TOP (TopValue) WITH TIES Selection");
        }

        [TestMethod]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Disposed in called method")]
        public void ShouldFormatSimpleSelection()
        {
            var selection = new Mock<ISqlSelection>();
            var query = new SelectCommand
            {
                Selection = selection.Object,
              };

            var context = new Mock<IFormattingContext>();
            selection.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "Selection" });
            context.Setup(x => x.OpenSelectionProcessingContext())
                .Returns(new Disposer(() => { }));
            var hanlder = new SelectCommandTsqlFormatter();
            hanlder.AttachContext(context.Object);

            var result = hanlder.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("SELECT Selection");
        }
    }
}