﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ProcedureCallTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldBeAbleToFormatProcedureCall()
        {
            var call = new ProcedureCall(new ElementName("Name"), new[] { new Mock<ISqlValueExpression>().Object }, null);
            var context = new Mock<IFormattingContext>();
            var handler = new ProcedureCallTsqlFormatter();
            handler.AttachContext(context.Object);
            handler.CanHandle(call).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldFormatManyProcedureParameters()
        {
            var sqlType = new Mock<ISqlType>();
            sqlType.Setup(x => x.ToString()).Returns("INT");
            var firstArgument = new Mock<ISqlValueExpression>();
            firstArgument.Setup(x => x.SqlType).Returns(sqlType.Object);
            var secondArgument = new Mock<ISqlValueExpression>();
            secondArgument.Setup(x => x.SqlType).Returns(sqlType.Object);
            var call = new ProcedureCall(
                new ElementName("procedure"),
                new[]
                {
                    firstArgument.Object,
                    secondArgument.Object
                },
                null);
            var context = new Mock<IFormattingContext>();
            firstArgument.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "4" });
            secondArgument.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "5" });
            var handler = new ProcedureCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("DECLARE @arg0 INT = (4), @arg1 INT = (5); EXEC [procedure] @arg0, @arg1");
        }

        [TestMethod]
        public void ShouldFormatParameterCall()
        {
            var sqlType = new Mock<ISqlType>();
            sqlType.Setup(x => x.ToString()).Returns("INT");
            var firstArgument = new Mock<ISqlValueExpression>();
            firstArgument.Setup(x => x.SqlType).Returns(sqlType.Object);
            var call = new ProcedureCall(
                new ElementName("procedure"),
                new[]
                {
                    firstArgument.Object,
                },
                null);
            var context = new Mock<IFormattingContext>();
            firstArgument.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "4" });
            var handler = new ProcedureCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("DECLARE @arg0 INT = (4); EXEC [procedure] @arg0");
        }

        [TestMethod]
        public void ShouldFormatProcedureCall()
        {
            var call = new ProcedureCall(
                new ElementName("procedure"),
                Enumerable.Empty<ISqlValueExpression>(),
                null);
            var context = new Mock<IFormattingContext>();
            var handler = new ProcedureCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("EXEC [procedure]");
        }

        [TestMethod]
        public void ShouldFormatSchemaProcedureCall()
        {
            var call = new ProcedureCall(
                new ElementName("schema", "procedure"),
                Enumerable.Empty<ISqlValueExpression>(),
                null);
            var context = new Mock<IFormattingContext>();
            var handler = new ProcedureCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Query.ShouldBeEqual("EXEC [schema].[procedure]");
        }

        [TestMethod]
        public void ShouldGatherScriptParameters()
        {
            var sqlType = new Mock<ISqlType>();
            sqlType.Setup(x => x.ToString()).Returns("INT");
            var firstArgument = new Mock<ISqlValueExpression>();
            firstArgument.Setup(x => x.SqlType).Returns(sqlType.Object);
            var secondArgument = new Mock<ISqlValueExpression>();
            secondArgument.Setup(x => x.SqlType).Returns(sqlType.Object);
            var call = new ProcedureCall(
                new ElementName("procedure"),
                new[]
                {
                    firstArgument.Object,
                    secondArgument.Object
                },
                null);
            var context = new Mock<IFormattingContext>();
            var parameter1 = new Mock<IParameter>().Object;
            var parameter2 = new Mock<IParameter>().Object;
            firstArgument.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "4", Parameters = new[] { parameter1 } });
            secondArgument.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "5", Parameters = new[] { parameter2 } });
            var handler = new ProcedureCallTsqlFormatter();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call, _visitor.Object);
            result.Parameters.AllCorrespondingElementsShouldBeEqual(new[] { parameter1, parameter2 });
        }
    }
}