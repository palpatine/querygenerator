﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class DeleteCommandFormatterTests
    {
        [TestMethod]
        public void ShouldFormatDeleteCommandWithTarget()
        {
            var formatter = new DeleteCommandFormatter();
            var conetxt = new Mock<IFormattingContext>();
            formatter.AttachContext(conetxt.Object);
            var tableReference = new TableReference(new ElementName("Name"));
            var expression = new DeleteCommand(tableReference);
            var transformer = new Mock<ITransformer>();
            transformer.Setup(x => x.Handle<TableReference, FormattedQuery>(tableReference))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "[Target]"
                    });

            var result = formatter.Handle(expression, transformer.Object);

            result.Query.ShouldBeEqual("DELETE FROM [Target]");
        }

        [TestMethod]
        public void ShouldFormatDeleteCommandWithFilter()
        {
            var formatter = new DeleteCommandFormatter();
            var conetxt = new Mock<IFormattingContext>();
            formatter.AttachContext(conetxt.Object);
            var tableReference = new TableReference(new ElementName("Name"));
            var filter = new Mock<ISqlLogicalExpression>();
            var expression = new DeleteCommand(tableReference, filter.Object);
            var transformer = new Mock<ITransformer>();
            filter.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Filter"
                    });
            transformer.Setup(x => x.Handle<TableReference, FormattedQuery>(tableReference))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "[Target]"
                    });

            var result = formatter.Handle(expression, transformer.Object);

            result.Query.ShouldBeEqual("DELETE FROM [Target] WHERE (Filter)");
        }

        [TestMethod]
        public void ShouldFormatDeleteCommandWithSource()
        {
            var formatter = new DeleteCommandFormatter();
            var conetxt = new Mock<IFormattingContext>();
            formatter.AttachContext(conetxt.Object);
            var tableReference = new AliasedSourceItem(
                new TableReference(new ElementName("Name")),
                alias: "[a]");
            var source = new Mock<ISource>();
            var expression = new DeleteCommand(tableReference, source: source.Object);
            var transformer = new Mock<ITransformer>();
            source.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Source"
                    });
            transformer.Setup(x => x.Handle<AliasedSourceItem, FormattedQuery>(tableReference))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "[Target]"
                    });

            var result = formatter.Handle(expression, transformer.Object);

            result.Query.ShouldBeEqual("DELETE [a] FROM Source");
        }

        [TestMethod]
        public void ShouldFormatDeleteCommandWithSourceAndFilter()
        {
            var formatter = new DeleteCommandFormatter();
            var conetxt = new Mock<IFormattingContext>();
            formatter.AttachContext(conetxt.Object);
            var tableReference = new AliasedSourceItem(
                new TableReference(new ElementName("Name")),
                alias: "[a]");
            var filter = new Mock<ISqlLogicalExpression>();
            var source = new Mock<ISource>();
            var expression = new DeleteCommand(tableReference, filter.Object, source.Object);
            var transformer = new Mock<ITransformer>();
            filter.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Filter"
                    });
            source.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "Source"
                    });
            transformer.Setup(x => x.Handle<AliasedSourceItem, FormattedQuery>(tableReference))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "[Target]"
                    });

            var result = formatter.Handle(expression, transformer.Object);

            result.Query.ShouldBeEqual("DELETE [a] FROM Source WHERE (Filter)");
        }
    }
}
