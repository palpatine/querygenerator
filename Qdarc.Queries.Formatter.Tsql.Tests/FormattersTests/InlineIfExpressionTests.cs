﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class InlineIfExpressionTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void InlineIf()
        {
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var query = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(string),
                new TsqlType(TsqlNativeTypesNames.VarChar, 1));

            var context = new Mock<IFormattingContext>();
            condition.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "condition" });
            ifTrue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "true" });
            ifFalse.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "false" });

            var handler = new InlineIfTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("IIF(condition, true, false)");
        }

        [TestMethod]
        public void ShouldCombineParameters()
        {
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var query = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(string),
                new TsqlType(TsqlNativeTypesNames.VarChar, 1));
            var conditionParameter = new Mock<IParameter>().Object;
            var truePathParameter = new Mock<IParameter>().Object;
            var falsePathParameter = new Mock<IParameter>().Object;

            var context = new Mock<IFormattingContext>();
            condition.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "condition", Parameters = new[] { conditionParameter } });
            ifTrue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "true", Parameters = new[] { truePathParameter } });
            ifFalse.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "false", Parameters = new[] { falsePathParameter } });

            var handler = new InlineIfTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Parameters.Count().ShouldBeEqual(3);
            result.Parameters.First().ShouldBeTheSameInstance(conditionParameter);
            result.Parameters.ElementAt(1).ShouldBeTheSameInstance(truePathParameter);
            result.Parameters.Last().ShouldBeTheSameInstance(falsePathParameter);
        }

        [TestMethod]
        public void ShouldVisitCondition()
        {
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var query = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(string),
                new TsqlType(TsqlNativeTypesNames.VarChar, 1));

            var context = new Mock<IFormattingContext>();
            condition.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "condition" });
            ifTrue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "true" });
            ifFalse.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "false" });

            var handler = new InlineIfTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);
            condition.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitIfFalse()
        {
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var query = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(string),
                new TsqlType(TsqlNativeTypesNames.VarChar, 1));

            var context = new Mock<IFormattingContext>();
            condition.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "condition" });
            ifTrue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "true" });
            ifFalse.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "false" });

            var handler = new InlineIfTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);
            ifFalse.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitIfTrue()
        {
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var query = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(string),
                new TsqlType(TsqlNativeTypesNames.VarChar, 1));

            var context = new Mock<IFormattingContext>();
            condition.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "condition" });
            ifTrue.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "true" });
            ifFalse.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
               .Returns(new FormattedQuery { Query = "false" });

            var handler = new InlineIfTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);
            ifTrue.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }
    }
}