﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class UpdateCommandFormatterTests
    {
        [TestMethod]
        public void ShouldFormatDirectUpdate()
        {
            var transformer = new Mock<ITransformer>();
            var source = new Mock<ISource>();
            source.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "[Table]"
                    });
            var value = new Mock<ISqlValueExpression>();
            value.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "value"
                    });
            var model = new UpdateCommand(
                source.Object,
                new[] { new ElementNameValueAssociation(new ElementName("Name"), value.Object) });
            var context = new Mock<IFormattingContext>();
            var handler = new UpdateCommandFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(model, transformer.Object);

            result.Query.ShouldBeEqual("UPDATE [Table] SET [Name] = value");
        }

        [TestMethod]
        public void ShouldFormatFilter()
        {
            var transformer = new Mock<ITransformer>();
            var source = new Mock<ISource>();
            source.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "[Table]"
                    });
            var value = new Mock<ISqlValueExpression>();
            value.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(
                    new FormattedQuery
                    {
                        Query = "value"
                    });
            var filter = new Mock<ISqlLogicalExpression>();
            filter.Setup(x => x.Accept<FormattedQuery>(transformer.Object))
                .Returns(new FormattedQuery
                {
                    Query = "[Condition]"
                });
            var model = new UpdateCommand(
                source.Object,
                new[] { new ElementNameValueAssociation(new ElementName("Name"), value.Object) },
                filter: filter.Object);
            var context = new Mock<IFormattingContext>();
            var handler = new UpdateCommandFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(model, transformer.Object);

            result.Query.ShouldBeEqual("UPDATE [Table] SET [Name] = value WHERE [Condition]");
        }
    }
}
