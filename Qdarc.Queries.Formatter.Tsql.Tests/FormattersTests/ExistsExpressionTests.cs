﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ExistsExpressionTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void Exists()
        {
            var selectCommand = new SelectCommand();
            var query = new ExistsExpression(selectCommand, false);
            var context = new Mock<IFormattingContext>();
            _visitor.Setup(x => x.Handle<SelectCommand, FormattedQuery>(selectCommand))
               .Returns(new FormattedQuery { Query = "base" });
            var handler = new ExistsTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("EXISTS(base)");
        }

        [TestMethod]
        public void ExistsNegated()
        {
            var selectCommand = new SelectCommand();
            var query = new ExistsExpression(
                selectCommand,
                true);

            var context = new Mock<IFormattingContext>();
            _visitor.Setup(x => x.Handle<SelectCommand, FormattedQuery>(selectCommand))
               .Returns(new FormattedQuery { Query = "base" });
            var handler = new ExistsTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Query.ShouldBeEqual("NOT EXISTS(base)");
        }

        [TestMethod]
        public void ShouldForwardParameters()
        {
            var selectCommand = new SelectCommand();
            var query = new ExistsExpression(selectCommand, false);
            var context = new Mock<IFormattingContext>();
            var linqParameterReferences = new List<LinqParameterReference>();
            _visitor.Setup(x => x.Handle<SelectCommand, FormattedQuery>(selectCommand))
                .Returns(new FormattedQuery { Parameters = linqParameterReferences });
            var handler = new ExistsTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);
            result.Parameters.ShouldBeTheSameInstance(linqParameterReferences);
        }

        [TestMethod]
        public void ShouldVisitBaseQuery()
        {
            var selectCommand = new SelectCommand();
            var query = new ExistsExpression(selectCommand, false);
            var context = new Mock<IFormattingContext>();
            _visitor.Setup(x => x.Handle<SelectCommand, FormattedQuery>(selectCommand))
                .Returns(new FormattedQuery());
            var handler = new ExistsTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(query, _visitor.Object);

            _visitor.Verify(x => x.Handle<SelectCommand, FormattedQuery>(selectCommand), Times.Once);
        }
    }
}