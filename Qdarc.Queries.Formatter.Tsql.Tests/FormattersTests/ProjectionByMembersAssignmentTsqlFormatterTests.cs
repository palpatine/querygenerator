﻿using System;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Formatter.Tsql.Tests.TestModel;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Utilities;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class ProjectionByMembersAssignmentTsqlFormatterTests
    {
        private static readonly PropertyInfo Property = ExpressionExtensions.GetProperty((UserEntity e) => e.Context);
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldVisitEachProjectionElement()
        {
            var context = new Mock<IFormattingContext>();
            var constructor = ExpressionExtensions.GetConstructor(() => new UserEntity());
            var boundSelectionItem = new BoundSelectionItem(Property, new Mock<ISelectionItem>().Object);
            var projection = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(constructor, new ISelectionItem[0]),
                new[] { boundSelectionItem });
            _visitor.Setup(x => x.Handle<BoundSelectionItem, FormattedQuery>(boundSelectionItem))
                .Returns(new FormattedQuery());

            var handler = new ProjectionByMembersAssignmentTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(projection, _visitor.Object);

            _visitor.Verify(x => x.Handle<BoundSelectionItem, FormattedQuery>(boundSelectionItem), Times.Once);
        }

        [TestMethod]
        public void ShouldAggregateItemsQueries()
        {
            var context = new Mock<IFormattingContext>();
            var constructor = ExpressionExtensions.GetConstructor(() => new UserEntity());
            var selectionItem1 = new BoundSelectionItem(Property, new Mock<ISelectionItem>().Object);
            var selectionItem2 = new BoundSelectionItem(Property, new Mock<ISelectionItem>().Object);
            var projection = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(constructor, new ISelectionItem[0]),
                new[] { selectionItem1, selectionItem2 });
            _visitor.Setup(x => x.Handle<BoundSelectionItem, FormattedQuery>(selectionItem1))
                .Returns(new FormattedQuery { Query = "ITEM1" });
            _visitor.Setup(x => x.Handle<BoundSelectionItem, FormattedQuery>(selectionItem2))
                .Returns(new FormattedQuery { Query = "ITEM2" });

            var handler = new ProjectionByMembersAssignmentTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(projection, _visitor.Object);
            result.Query.ShouldBeEqual("ITEM1,ITEM2");
        }

        [TestMethod]
        public void ShouldVisitAggregateParameters()
        {
            var context = new Mock<IFormattingContext>();
            var constructor = ExpressionExtensions.GetConstructor(() => new UserEntity());
            var firstParameter = new Mock<IParameter>();
            var secondParameter = new Mock<IParameter>();
            var selectionItem1 = new BoundSelectionItem(Property, new Mock<ISelectionItem>().Object);
            var selectionItem2 = new BoundSelectionItem(Property, new Mock<ISelectionItem>().Object);
            var projection = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(constructor, new ISelectionItem[0]),
                new[] { selectionItem1, selectionItem2 });

            _visitor.Setup(x => x.Handle<BoundSelectionItem, FormattedQuery>(selectionItem1))
                .Returns(new FormattedQuery { Parameters = new[] { firstParameter.Object } });
            _visitor.Setup(x => x.Handle<BoundSelectionItem, FormattedQuery>(selectionItem2))
                .Returns(new FormattedQuery { Parameters = new[] { secondParameter.Object } });

            var handler = new ProjectionByMembersAssignmentTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(projection, _visitor.Object);
            result.Parameters.AllCorrespondingElementsShouldBeEqual(new[] { firstParameter.Object, secondParameter.Object });
        }
    }
}
