﻿using System.Collections.Generic;
using Castle.DynamicProxy.Tokens;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class AliasedSourceItemTsqlFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldBeAbleToFormatAliasedSourceItem()
        {
            var item = new AliasedSourceItem(new Mock<ISource>().Object);
            var context = new Mock<IFormattingContext>(MockBehavior.Strict);
            var handler = new AliasedSourceItemTsqlFormatter();
            handler.AttachContext(context.Object);

            handler.CanHandle(item)
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldFormatColumnList()
        {
            var innerSource = new Mock<IValuesSource>();
            var query = new AliasedSourceItem(
                innerSource.Object,
                new[] { new ColumnAlias("A", null, null), new ColumnAlias("B", null, null) });

            var aliasManager = new Mock<IAliasManager>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            aliasManager.Setup(x => x.GetAlias()).Returns("q1");
            innerSource.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "inner" });
            var handler = new AliasedSourceItemTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("(inner) AS [q1](A, B)");
        }

        [TestMethod]
        public void ShouldFormatInnerCompositeSelectionCommandInBraces()
        {
            var innerSource = new UnionSelectionCommand(
                new Mock<ISelectCommand>().Object,
                new Mock<ISelectCommand>().Object);
            var query = new AliasedSourceItem(innerSource);
            var aliasManager = new Mock<IAliasManager>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            aliasManager.Setup(x => x.GetAlias()).Returns("q1");
            _visitor.Setup(x => x.Handle<UnionSelectionCommand, FormattedQuery>(innerSource))
                .Returns(new FormattedQuery { Query = "inner" });
            var handler = new AliasedSourceItemTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("(inner) AS [q1]");
        }

        [TestMethod]
        public void ShouldFormatInnerTableReferenceWithoutBraces()
        {
            var innerSource = new Mock<ITableReference>();
            var query = new AliasedSourceItem(innerSource.Object);
            var aliasManager = new Mock<IAliasManager>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            aliasManager.Setup(x => x.GetAlias()).Returns("q1");
            innerSource.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "inner" });
            var handler = new AliasedSourceItemTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("inner AS [q1]");
        }

        [TestMethod]
        public void ShouldFormatInnerSelectCommandInBraces()
        {
            var innerSource = new Mock<ISelectCommand>();
            var query = new AliasedSourceItem(innerSource.Object);
            var aliasManager = new Mock<IAliasManager>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            aliasManager.Setup(x => x.GetAlias()).Returns("q1");
            innerSource.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "inner" });
            var handler = new AliasedSourceItemTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("(inner) AS [q1]");
        }

        [TestMethod]
        public void ShouldFormatInnerValuesSourceInBraces()
        {
            var innerSource = new Mock<IValuesSource>();
            var query = new AliasedSourceItem(innerSource.Object);
            var aliasManager = new Mock<IAliasManager>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            aliasManager.Setup(x => x.GetAlias()).Returns("q1");
            innerSource.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(new FormattedQuery { Query = "inner" });
            var handler = new AliasedSourceItemTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Query.ShouldBeEqual("(inner) AS [q1]");
        }

        [TestMethod]
        public void ShouldForwardParametersFromInnerQuery()
        {
            var innerSource = new Mock<ISelectCommand>();
            var query = new AliasedSourceItem(innerSource.Object);
            var aliasManager = new Mock<IAliasManager>();
            var context = new Mock<IFormattingContext>();
            context.Setup(x => x.AliasManager).Returns(aliasManager.Object);
            aliasManager.Setup(x => x.GetAlias()).Returns("q1");
            var formattedQuery = new FormattedQuery { Parameters = new List<LinqParameterReference>() };
            innerSource.Setup(x => x.Accept<FormattedQuery>(_visitor.Object)).Returns(formattedQuery);
            var handler = new AliasedSourceItemTsqlFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(query, _visitor.Object);

            result.Parameters.ShouldBeTheSameInstance(formattedQuery.Parameters);
        }
    }
}