﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Formatter.Tsql.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Utilities;

namespace Qdarc.Queries.Formatter.Tsql.Tests.FormattersTests
{
    [TestClass]
    public class InsertCommandFormatterTests
    {
        private Mock<ITransformer> _visitor;

        [TestInitialize]
        public void Initialize()
        {
            _visitor = new Mock<ITransformer>();
        }

        [TestMethod]
        public void ShouldVisitValuesToInsert()
        {
            var tableReference = new Mock<ITableReference>();
            var source = new Mock<ISource>();
            var expression = new InsertCommand(
                tableReference.Object,
                source.Object,
                null,
                typeof(UserEntity));

            var context = new Mock<IFormattingContext>();
            tableReference.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "table" });
            source.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "values" });
            var property = ExpressionExtensions.GetProperty((UserEntity c) => c.Id);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetInsertableColumns(typeof(UserEntity)))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            new Mock<INamedSource>().Object,
                            property,
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            convention.Setup(x => x.GetColumnName(property))
                .Returns("columnName");
            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);
            var handler = new InsertCommandFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(expression, _visitor.Object);

            source.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitTableReference()
        {
            var tableReference = new Mock<ITableReference>();
            var source = new Mock<ISource>();
            var expression = new InsertCommand(
                tableReference.Object,
                source.Object,
                null,
                typeof(UserEntity));
            var property = ExpressionExtensions.GetProperty((UserEntity c) => c.Id);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetInsertableColumns(typeof(UserEntity)))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            new Mock<INamedSource>().Object,
                            property,
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            convention.Setup(x => x.GetColumnName(property))
                .Returns("columnName");
            var context = new Mock<IFormattingContext>();
            tableReference.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "table" });
            source.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "values" });
            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);
            var handler = new InsertCommandFormatter();
            handler.AttachContext(context.Object);

            handler.Handle(expression, _visitor.Object);

            tableReference.Verify(x => x.Accept<FormattedQuery>(_visitor.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldFormatInsertCommand()
        {
            var tableReference = new Mock<ITableReference>();
            var source = new Mock<ISource>();
            var expression = new InsertCommand(
                tableReference.Object,
                source.Object,
                null,
                typeof(UserEntity));
            var context = new Mock<IFormattingContext>();
            tableReference.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "table" });
            source.Setup(x => x.Accept<FormattedQuery>(_visitor.Object))
                .Returns(new FormattedQuery { Query = "values" });
            var convention = new Mock<IModelToSqlConvention>();
            var property = ExpressionExtensions.GetProperty((UserEntity c) => c.Id);
            convention.Setup(x => x.GetInsertableColumns(typeof(UserEntity)))
                .Returns(
                new[]
                    {
                        new LinqColumnReference(
                            new Mock<INamedSource>().Object,
                            property,
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            convention.Setup(x => x.GetColumnName(property))
                .Returns("columnName");

            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);
            var handler = new InsertCommandFormatter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression, _visitor.Object);

            result.Query.ShouldBeEqual("INSERT INTO table (columnName) values");
        }
    }
}
