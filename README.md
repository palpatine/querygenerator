#Introduction
At the moment this repository contains set of libraries that allow integration of databases with .NET applications. Those libraries can be divided into five groups:

1. [Connector](doc/Connector/Connector.md) - The idea behind this is to provide common API for any database specific connector. Also it provides interface abstraction that enables mocking on the lowest possible level.
2. [Model](doc/Model/Model.md) - Model is an abstract reproesentation of SQL script.
3. [Linq](doc/Linq/Linq.md)- Allows strongly typed way of declaring SQL queries. This part of the project transforms expression tree into a sql script model that in turn can be processed further.
4. [Formatter](doc/Formatter/Formatter.md)- Provides a way to format sql script model into valid sql script that can be executed on particular sql database.
5. [Parser](doc/Parser/Parser.md)- Sql script parser that generates sql abstract model based on the sql script.

#Technologies used
Almost all projects are created to be portable targeting .NET Framework 4.5, ASP.NET Core 1.0, Windows 8, Windows Phone 8.1. All are written in C# language.