using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class LinqSqlTypeReferenceSqlTypeFinderHandler : ISqlTypeFinderHandler<LinqColumnReference, ISqlType>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(LinqColumnReference expression) => true;

        public ISqlType Handle(LinqColumnReference expression, ISqlTypeFindingTransformer transformer)
        {
            var current = transformer.Property;
            transformer.Property = expression.Property;
            var type = expression.Source.Accept<ISqlType>(transformer);
            if (type == null)
            {
                transformer.Property = current;
            }

            return type;
        }
    }
}