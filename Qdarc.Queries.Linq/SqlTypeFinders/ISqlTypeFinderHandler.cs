using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    public interface ISqlTypeFinderHandler<TExpression, TResult>
        : ITransformer<TExpression, TResult, ISqlTypeFindingTransformer>
    {
        int Priority { get; }

        bool CanHandle(TExpression expression);
    }
}