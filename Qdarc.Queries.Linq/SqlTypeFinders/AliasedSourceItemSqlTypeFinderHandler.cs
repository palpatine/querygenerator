using System.Linq;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class AliasedSourceItemSqlTypeFinderHandler
        : ISqlTypeFinderHandler<AliasedSourceItem, ISqlType>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(AliasedSourceItem expression) => true;

        public ISqlType Handle(AliasedSourceItem expression, ISqlTypeFindingTransformer transformer)
        {
            if (expression.ColumnAliases != null)
            {
                return expression.ColumnAliases.Single(x => x.Alias == transformer.Property.Name).SqlType;
            }

            return expression.Source.Accept<ISqlType>(transformer);
        }
    }
}