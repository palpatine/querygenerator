using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class ClrTypeSqlTypeFinderHandler
        : ISqlTypeFinderHandler<LinqTableParameterReference, ISqlType>,
            ISqlTypeFinderHandler<TableReference, ISqlType>
    {
        private readonly IModelToSqlConvention _convention;

        public ClrTypeSqlTypeFinderHandler(IModelToSqlConvention convention) => _convention = convention;

        public int Priority { get; } = 0;

        public bool CanHandle(LinqTableParameterReference expression) => true;

        public bool CanHandle(TableReference expression) => true;

        public ISqlType Handle(LinqTableParameterReference expression, ISqlTypeFindingTransformer transformer) => _convention.GetSqlType(expression.ClrType, transformer.Property);

        public ISqlType Handle(TableReference expression, ISqlTypeFindingTransformer transformer) => _convention.GetSqlType(expression.ClrType, transformer.Property);
    }
}