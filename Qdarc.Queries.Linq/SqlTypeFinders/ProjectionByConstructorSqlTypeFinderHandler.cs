using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.Projections;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class ProjectionByConstructorSqlTypeFinderHandler
        : ISqlTypeFinderHandler<ProjectionByConstructor, ISqlType>
    {
        private static readonly Type[] SupportedTypes
            = new[]
            {
                typeof(Tuple<,>),
                typeof(Tuple<,,>),
                typeof(Tuple<,,,>),
                typeof(Tuple<,,,,>),
                typeof(Tuple<,,,,,>),
                typeof(Tuple<,,,,,,>),
                typeof(Tuple<,,,,,,,>)
            };

        public int Priority { get; } = 0;

        public bool CanHandle(ProjectionByConstructor expression)
        {
            return true;
        }

        public ISqlType Handle(ProjectionByConstructor expression, ISqlTypeFindingTransformer transformer)
        {
            int? parameterIndex = null;
            var propertyName = transformer.Property.Name;

            if (expression.ClrType.GetTypeInfo().GetCustomAttribute<CompilerGeneratedAttribute>() != null)
            {
                parameterIndex = GetParmParameterIndex(expression, propertyName);
            }

            if (expression.ClrType.IsConstructedGenericType && SupportedTypes.Contains(expression.ClrType.GetGenericTypeDefinition()))
            {
                propertyName = char.ToLowerInvariant(propertyName[0]) + propertyName.Substring(1);
                parameterIndex = GetParmParameterIndex(expression, propertyName);
            }

            if (parameterIndex != null)
            {
                return expression.Elements
                    .ElementAt(parameterIndex.Value)
                    .Accept<ISqlType>(transformer);
            }

            throw new NotImplementedException("T91");
        }

        private static int GetParmParameterIndex(ProjectionByConstructor expression, string propertyName)
        {
            return expression.Constructor.GetParameters()
                .Select((x, i) => new { x, i })
                .Where(x => x.x.Name == propertyName)
                .Select(x => x.i)
                .Single();
        }
    }
}