﻿using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class PrefixedSelectionSqlTypeFinderHandler : ISqlTypeFinderHandler<PrefixedSelection, ISqlType>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(PrefixedSelection expression) => true;

        public ISqlType Handle(PrefixedSelection expression, ISqlTypeFindingTransformer transformer) => expression.Selection.Accept<ISqlType>(transformer);
    }
}