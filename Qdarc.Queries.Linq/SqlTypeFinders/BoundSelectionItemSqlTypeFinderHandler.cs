using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class BoundSelectionItemSqlTypeFinderHandler : ISqlTypeFinderHandler<BoundSelectionItem, ISqlType>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(BoundSelectionItem expression) => true;

        public ISqlType Handle(BoundSelectionItem expression, ISqlTypeFindingTransformer transformer)
        {
            if (!Equals(expression.Member, transformer.Property))
            {
                return null;
            }

            return expression.Item.Accept<ISqlType>(transformer);
        }
    }
}