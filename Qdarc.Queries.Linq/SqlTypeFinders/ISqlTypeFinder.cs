﻿using System.Reflection;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    public interface ISqlTypeFinder
    {
        ISqlType TryFindType(INamedSource source, PropertyInfo property);
    }
}