using System.Reflection;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    public interface ISqlTypeFindingTransformer : ITransformer
    {
        PropertyInfo Property { get; set; }
    }
}