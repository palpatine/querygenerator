using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class AliasedSelectionItemSqlTypeFinderHandler : ISqlTypeFinderHandler<AliasedSelectionItem, ISqlType>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(AliasedSelectionItem expression) => true;

        public ISqlType Handle(AliasedSelectionItem expression, ISqlTypeFindingTransformer transformer) => expression.Item.Accept<ISqlType>(transformer);
    }
}