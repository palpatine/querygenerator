using System;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class SqlTypeFinder : ISqlTypeFinder, ISqlTypeFindingTransformer
    {
        private readonly IServiceProvider _resolver;

        public SqlTypeFinder(IServiceProvider resolver)
        {
            _resolver = resolver;
        }

        public PropertyInfo Property { get; set; }

        public TResult Handle<TExpression, TResult>(TExpression expression)
        {
            var handlers = _resolver.GetServices<ISqlTypeFinderHandler<TExpression, TResult>>();
            var handler = handlers.OrderBy(x => x.Priority).FirstOrDefault(x => x.CanHandle(expression));
            if (handler != null)
            {
                return handler.Handle(expression, this);
            }

            throw new InvalidOperationException($"There is no {typeof(ISqlTypeFinderHandler<,>).Name} from {typeof(TExpression).FullName} to {typeof(TResult)}");
        }

        public ISqlType TryFindType(INamedSource source, PropertyInfo property)
        {
            Property = property;
            var type = source.Accept<ISqlType>(this);
            return type;
        }
    }
}