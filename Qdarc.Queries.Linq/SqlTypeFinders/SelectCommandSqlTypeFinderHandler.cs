using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class SelectCommandSqlTypeFinderHandler : ISqlTypeFinderHandler<SelectCommand, ISqlType>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(SelectCommand expression) => true;

        public ISqlType Handle(SelectCommand expression, ISqlTypeFindingTransformer transformer) => expression.Selection.Accept<ISqlType>(transformer);
    }
}