using System.Linq;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.SqlTypeFinders
{
    internal class SqlSelectionSqlTypeFinderHandler
        : ISqlTypeFinderHandler<SqlSelection, ISqlType>,
            ISqlTypeFinderHandler<ProjectionByMembersAssignment, ISqlType>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(SqlSelection expression) => true;

        public bool CanHandle(ProjectionByMembersAssignment expression) => true;

        public ISqlType Handle(SqlSelection expression, ISqlTypeFindingTransformer transformer) => HandleSelection(expression, transformer);

        public ISqlType Handle(ProjectionByMembersAssignment expression, ISqlTypeFindingTransformer transformer) => HandleSelection(expression, transformer);

        private static ISqlType HandleSelection(ISqlSelection expression, ISqlTypeFindingTransformer transformer)
        {
            return expression.Elements
                .Where(x => x.ClrType == transformer.Property.PropertyType)
                .Select(x => x.Accept<ISqlType>(transformer))
                .FirstOrDefault(x => x != null);
        }
    }
}