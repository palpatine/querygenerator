﻿using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq
{
    public interface IVisitationContext
    {
        IModelToSqlConvention ModelToSqlConvention { get; }

        ISqlTypeManager TypeManager { get; }

        void AddParameterMapping(ParameterExpression parameter, ISource source);

        ISqlSelection ConvertToSelectionList(ISqlExpression expression);

        ISelectCommand EnsureSelection(ISqlExpression part);

        ISqlExpression FinalizeSelection(ISqlExpression part);

        ISqlSelection GenerateExplicitSelectionList(ISource source);

        ISource GetParameterMapping(ParameterExpression parameter);

        ISqlLogicalExpression ProcessSingleParameterFilter(UnaryExpression quote, ISelectCommand source);

        void RemoveParameterMapping(ParameterExpression parameter);

        ISqlExpression VisitExpression(Expression expression);

        ISqlLogicalExpression WrapInLogicalExpression(LinqColumnReference columnReference);

        SelectCommand WrapInSelectCommand(ISqlExpression part);

        bool CanApplyFilterDirectly(SelectCommand selectCommand);
    }
}