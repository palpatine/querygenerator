﻿using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;

namespace Qdarc.Queries.Linq
{
    public interface ISqlTypeManager
    {
        ISqlType Bit { get; }

        ISqlType Date { get; }

        ISqlType DateTime { get; }

        ISqlType Int { get; }

        ISqlType GetNVarChar(int capacity);

        ISqlType GetSqlType(ConstantExpression constant);

        ISqlType GetSqlType(Type type);

        ISqlType GetSqlType(ISqlType firstType, ISqlType secondType);

        ISqlType GetSqlType(Type type, object value);

        ISqlType TryGetSqlType(Type type);
    }
}