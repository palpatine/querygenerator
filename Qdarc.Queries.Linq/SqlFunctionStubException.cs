using System;

namespace Qdarc.Queries.Linq
{
    public sealed class SqlFunctionStubException : Exception
    {
        public SqlFunctionStubException()
            : base("This function is not meant to be called directly it represents sql function call and should be used in linq expressions")
        {
        }
    }
}