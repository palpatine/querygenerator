﻿using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators
{
    public interface IExplicitSelectionListGenerator
    {
        ISqlSelection Generate(ISource source);
    }
}