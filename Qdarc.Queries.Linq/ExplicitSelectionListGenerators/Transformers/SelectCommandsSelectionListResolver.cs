using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.ExplicitSelectionListTransformationHandler
{
    internal class SelectCommandsSelectionListResolver
        : ISelectionListResolver<SelectCommand, ISqlSelection>,
            ISelectionListResolver<UnionAllSelectionCommand, ISqlSelection>,
            ISelectionListResolver<UnionSelectionCommand, ISqlSelection>
    {
        public int Priority { get; } = 0;

        public ISqlSelection Handle(
            SelectCommand expression,
            IExplicitSelectionListTransformer transformer) => GenerateSelection(expression, transformer);

        public bool CanHandle(SelectCommand expression) => true;

        public bool CanHandle(UnionAllSelectionCommand expression) => true;

        public bool CanHandle(UnionSelectionCommand expression) => true;

        public ISqlSelection Handle(
            UnionAllSelectionCommand expression,
            IExplicitSelectionListTransformer transformer) => GenerateSelection(expression, transformer);

        public ISqlSelection Handle(
            UnionSelectionCommand expression,
            IExplicitSelectionListTransformer transformer) => GenerateSelection(expression, transformer);

        private static ISqlSelection GenerateSelection(ISelectCommand source, IExplicitSelectionListTransformer visitor)
        {
            if (visitor.WrapperSource == null)
            {
                return source.Selection;
            }

            return source.Selection.Accept<ISqlSelection>(visitor);
        }
    }
}