using System.Linq;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.ExplicitSelectionListTransformationHandler
{
    internal class AliasedSourceItemSelectionListResolver
        : ISelectionListResolver<AliasedSourceItem, ISqlSelection>
    {
        public virtual int Priority { get; } = 0;

        public virtual bool CanHandle(AliasedSourceItem expression) => true;

        public ISqlSelection Handle(AliasedSourceItem expression, IExplicitSelectionListTransformer transformer)
        {
            if (expression.ColumnAliases != null)
            {
                var selectionItems = expression.ColumnAliases
                    .Select(
                        (x, i) => new AliasedSelectionItem(
                            new SqlColumnReference(new ElementName(x.Alias), expression, x.ClrType, x.SqlType))
                        {
                            Alias = x.Alias
                        })
                    .ToArray();

                if (expression.ClrType != typeof(string) && expression.ClrType.GetTypeInfo().IsClass)
                {
                    var constructorInfo = expression.ClrType.GetTypeInfo()
                        .DeclaredConstructors
                        .Single();
                    if (constructorInfo.GetParameters().Length > 0)
                    {
                        return new ProjectionByConstructor(constructorInfo, selectionItems);
                    }
                }

                return new SqlSelection(selectionItems, expression.ClrType);
            }

            if (transformer.WrapperSource == null)
            {
                transformer.WrapperSource = expression;
            }

            var selection = expression.Source.Accept<ISqlSelection>(transformer);

            return selection;
        }
    }
}