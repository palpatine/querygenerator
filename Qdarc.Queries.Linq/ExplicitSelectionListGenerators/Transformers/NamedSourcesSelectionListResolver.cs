using System;
using System.Linq;
using System.Reflection;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.ExplicitSelectionListTransformationHandler
{
    internal class NamedSourcesSelectionListResolver
        : ISelectionListResolver<LinqTableParameterReference, ISqlSelection>,
            ISelectionListResolver<TabularFunctionCall, ISqlSelection>,
            ISelectionListResolver<TableReference, ISqlSelection>
    {
        private readonly IModelToSqlConvention _modelToSqlConvention;

        public NamedSourcesSelectionListResolver(IModelToSqlConvention modelToSqlConvention) =>
            _modelToSqlConvention = modelToSqlConvention;

        public virtual int Priority { get; } = 0;

        public virtual bool CanHandle(LinqTableParameterReference expression) => true;

        public ISqlSelection Handle(
            LinqTableParameterReference expression,
            IExplicitSelectionListTransformer transformer) => GenerateExplicitSelectionList(
            transformer.WrapperSource,
            expression.ClrType);

        public virtual bool CanHandle(TableReference expression) => true;

        public ISqlSelection Handle(TableReference expression, IExplicitSelectionListTransformer transformer) =>
            GenerateExplicitSelectionList(transformer.WrapperSource ?? expression, expression.ClrType);

        public virtual bool CanHandle(TabularFunctionCall expression) => true;

        public ISqlSelection Handle(TabularFunctionCall expression, IExplicitSelectionListTransformer transformer) =>
            GenerateExplicitSelectionList(transformer.WrapperSource ?? expression, expression.ClrType);

        private ISqlSelection GenerateExplicitSelectionList(INamedSource source, Type type)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            var columns = _modelToSqlConvention.GetSelectableColumns(source).ToArray();

            var selectionItems = columns.Select(
                    x => new AliasedSelectionItem(x)
                    {
                        Alias = x.Property.Name
                    })
                .ToArray();

            var constructorInfo = type.GetTypeInfo().DeclaredConstructors.Single();
            if (constructorInfo.GetParameters().Length > 0)
            {
                return new ProjectionByConstructor(constructorInfo, selectionItems);
            }

            return new SqlSelection(selectionItems, type);
        }
    }
}