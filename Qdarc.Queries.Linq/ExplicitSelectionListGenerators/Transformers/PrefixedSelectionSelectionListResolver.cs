﻿using System.Reflection;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.Transformers
{
    internal class PrefixedSelectionSelectionListResolver
        : ISelectionListResolver<PrefixedSelection, ISelectionItem>
    {
        public int Priority { get; } = 0;

        public bool CanHandle(PrefixedSelection expression) => true;

        public ISelectionItem Handle(PrefixedSelection expression, IExplicitSelectionListTransformer transformer)
        {
            var currentPropertyFactory = transformer.GetPropertyFromSource;
            transformer.GetPropertyFromSource =
                (name) => currentPropertyFactory(expression.Prefix).PropertyType.GetRuntimeProperty(name);

            var result = new PrefixedSelection(
                expression.Prefix,
                expression.Selection.Accept<ISqlSelection>(transformer));

            transformer.GetPropertyFromSource = currentPropertyFactory;

            return result;
        }
    }
}