﻿using System;
using System.Reflection;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators
{
    public interface IExplicitSelectionListTransformer : ITransformer
    {
        INamedSource WrapperSource { get; set; }
        PropertyInfo Property { get; set; }
        Func<string, PropertyInfo> GetPropertyFromSource { get; set; }
    }
}