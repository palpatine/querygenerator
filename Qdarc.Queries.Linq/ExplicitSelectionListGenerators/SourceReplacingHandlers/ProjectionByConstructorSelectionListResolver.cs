using System.Linq;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.SourceReplacingHandlers
{
    internal class ProjectionByConstructorSelectionListResolver
        : ISelectionListResolver<ProjectionByConstructor, ProjectionByConstructor>,
            ISelectionListResolver<ProjectionByConstructor, ISqlSelection>
    {
        public int Priority { get; } = 0;

        public ProjectionByConstructor Handle(
            ProjectionByConstructor expression,
            IExplicitSelectionListTransformer visitor)
        {
            var selectionItems = expression.Elements
                .Select(x => x.Accept<ISelectionItem>(visitor))
                .ToArray();
            return new ProjectionByConstructor(
                expression.Constructor,
                selectionItems);
        }

        public bool CanHandle(ProjectionByConstructor expression) => true;

        ISqlSelection ITransformer<ProjectionByConstructor, ISqlSelection, IExplicitSelectionListTransformer>.
            Handle(ProjectionByConstructor expression, IExplicitSelectionListTransformer transformer) => Handle(
            expression,
            transformer);
    }
}