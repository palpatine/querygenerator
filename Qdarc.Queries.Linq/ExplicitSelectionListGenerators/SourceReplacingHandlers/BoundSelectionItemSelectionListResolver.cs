﻿using System.Reflection;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.SourceReplacingHandlers
{
    internal class BoundSelectionItemSelectionListResolver
        : ISelectionListResolver<BoundSelectionItem, BoundSelectionItem>
    {
        public int Priority { get; } = 0;

        public BoundSelectionItem Handle(
            BoundSelectionItem expression,
            IExplicitSelectionListTransformer visitor)
        {
            visitor.Property = (PropertyInfo)expression.Member;
            var result = new BoundSelectionItem(
                expression.Member,
                expression.Item.Accept<ISelectionItem>(visitor));
            visitor.Property = null;
            return result;
        }

        public bool CanHandle(BoundSelectionItem expression) => true;
    }
}