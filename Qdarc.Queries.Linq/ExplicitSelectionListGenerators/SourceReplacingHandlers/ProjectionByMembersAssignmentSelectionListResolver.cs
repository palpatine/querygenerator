using System.Linq;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.SourceReplacingHandlers
{
    internal class
        ProjectionByMembersAssignmentSelectionListResolver
        : ISelectionListResolver<ProjectionByMembersAssignment, ProjectionByMembersAssignment>,
            ISelectionListResolver<ProjectionByMembersAssignment, ISqlSelection>
    {
        public int Priority { get; } = 0;

        public ProjectionByMembersAssignment Handle(
            ProjectionByMembersAssignment expression,
            IExplicitSelectionListTransformer visitor)
        {
            var selectionItems = expression.Bindings
                .Select(x => x.Accept<BoundSelectionItem>(visitor))
                .ToArray();
            return new ProjectionByMembersAssignment(
                expression.ConstructorCall.Accept<ProjectionByConstructor>(visitor),
                selectionItems);
        }

        public bool CanHandle(ProjectionByMembersAssignment expression) => true;

        ISqlSelection
            ITransformer<ProjectionByMembersAssignment, ISqlSelection, IExplicitSelectionListTransformer>.
            Handle(ProjectionByMembersAssignment expression, IExplicitSelectionListTransformer transformer) => Handle(
            expression,
            transformer);
    }
}