using System.Linq;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.SourceReplacingHandlers
{
    internal class SqlSelectionSourceSelectionListResolver
        : ISelectionListResolver<SqlSelection, ISqlSelection>
    {
        public int Priority { get; } = 0;

        public ISqlSelection Handle(SqlSelection expression, IExplicitSelectionListTransformer visitor)
        {
            var selectionItems = expression.Elements
                .Select(x => x.Accept<ISelectionItem>(visitor))
                .ToArray();
            return new SqlSelection(
                selectionItems,
                expression.ClrType);
        }

        public bool CanHandle(SqlSelection expression) => true;
    }
}