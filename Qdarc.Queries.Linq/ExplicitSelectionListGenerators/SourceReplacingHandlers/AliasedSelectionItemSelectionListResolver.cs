using System.Reflection;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.SourceReplacingHandlers
{
    internal class AliasedSelectionItemSelectionListResolver
        : ISelectionListResolver<AliasedSelectionItem, ISelectionItem>
    {
        public int Priority { get; } = 0;

        public ISelectionItem Handle(
            AliasedSelectionItem expression,
            IExplicitSelectionListTransformer visitor)
        {
            if (visitor.WrapperSource != null)
            {
                var item = new LinqColumnReference(
                    visitor.WrapperSource,
                    visitor.GetPropertyFromSource(expression.Alias),
                    expression.ClrType,
                    expression.SqlType);

                return new AliasedSelectionItem(item)
                {
                    Alias = expression.Alias
                };
            }

            return new AliasedSelectionItem(expression.Item.Accept<ISelectionItem>(visitor))
            {
                Alias = expression.Alias
            };
        }

        public bool CanHandle(AliasedSelectionItem expression) => true;
    }
}