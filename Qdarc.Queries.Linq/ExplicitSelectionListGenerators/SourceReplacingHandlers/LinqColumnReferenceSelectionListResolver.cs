using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators.SourceReplacingHandlers
{
    internal class LinqColumnReferenceSelectionListResolver
        : ISelectionListResolver<LinqColumnReference, ISelectionItem>
    {
        public int Priority { get; } = 0;

        public ISelectionItem Handle(
            LinqColumnReference expression,
            IExplicitSelectionListTransformer visitor)
        {
            var namedSource = visitor.WrapperSource ?? expression.Source;
            var property = visitor.Property ?? expression.Property;
            return new LinqColumnReference(namedSource, property, expression.ClrType, expression.SqlType);
        }

        public bool CanHandle(LinqColumnReference expression) => true;
    }
}