using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators
{
    public interface ISelectionListResolver<TExpression, TResult>
        : ITransformer<TExpression, TResult, IExplicitSelectionListTransformer>
    {
        int Priority { get; }

        bool CanHandle(TExpression expression);
    }
}