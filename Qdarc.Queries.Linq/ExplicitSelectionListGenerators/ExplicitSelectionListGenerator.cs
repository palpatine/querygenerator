﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.ExplicitSelectionListGenerators
{
    internal class ExplicitSelectionListGenerator : IExplicitSelectionListGenerator, IExplicitSelectionListTransformer
    {
        private readonly IServiceProvider _resolver;

        public ExplicitSelectionListGenerator(
            IServiceProvider resolver)
        {
            _resolver = resolver;
            GetPropertyFromSource = name => WrapperSource.ClrType.GetRuntimeProperty(name);
        }

        public INamedSource WrapperSource { get; set; }

        public PropertyInfo Property { get; set; }

        public Func<string, PropertyInfo> GetPropertyFromSource { get; set; }

        public ISqlSelection Generate(ISource source)
        {
            WrapperSource = null;
            return source.Accept<ISqlSelection>(this);
        }

        public TResult Handle<TExpression, TResult>(TExpression expression)
        {
            var handlers = _resolver.GetServices<ISelectionListResolver<TExpression, TResult>>();
            var handler = handlers.OrderBy(x => x.Priority).FirstOrDefault(x => x.CanHandle(expression));
            if (handler != null)
            {
                return handler.Handle(expression, this);
            }

            throw new InvalidOperationException($"There is no {typeof(ISelectionListResolver<,>).Name} from {typeof(TExpression).FullName} to {typeof(TResult)}");
        }
    }
}