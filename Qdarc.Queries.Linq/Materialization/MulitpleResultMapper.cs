using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Materialization
{
    internal class MulitpleResultMapper : IObjectsSetMapper
    {
        private readonly Type _resultType;
        private readonly IObjectMapperSelector _objectMapperSelector;

        public MulitpleResultMapper(
            Type resultType,
            IObjectMapperSelector objectMapperSelector)
        {
            _resultType = resultType;
            _objectMapperSelector = objectMapperSelector;
        }

        public IList ReadAll(Type elementType, ISqlDataReader reader)
        {
            var listType = typeof(List<>).MakeGenericType(TypeSystem.GetElementType(_resultType));
            var result = (IList)Activator.CreateInstance(listType);

            var properties = _resultType.GetRuntimeProperties()
                .Select(x => new { Result = x.GetCustomAttribute<SourceResultAttribute>(), Property = x })
                .Where(x => x.Result != null)
                .OrderBy(x => x.Result.ResultId)
                .ToArray();

            var element = Activator.CreateInstance(_resultType);

            foreach (var property in properties)
            {
                var propertyType = property.Property.PropertyType;
                if (!propertyType.IsConstructedGenericType)
                {
                    throw new MappingException("DTO mapped on multiple results must declare properties as generic collections.");
                }

                var innerType = propertyType.GenericTypeArguments[0];
                var materializer = _objectMapperSelector.BuildDirectObjectsSetMapper(innerType);
                var data = materializer.ReadAll(innerType, reader);
                property.Property.SetMethod.Invoke(element, new object[] { data });

                if (property != properties[properties.Length - 1] && !reader.NextResult())
                {
                    throw new MappingException($"Reader does not contain expected number of results. Materialized type: {_resultType.FullName}.");
                }
            }

            result.Add(element);
            return result;
        }
    }
}