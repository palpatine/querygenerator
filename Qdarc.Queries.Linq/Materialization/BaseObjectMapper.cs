using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Materialization
{
    internal abstract class BaseObjectMapper : IObjectsSetMapper, ISingleObjectsMapper
    {
        protected static readonly MethodInfo ProcessValueMethodDefinition =
            ExpressionExtensions.GetMethod(() => ProcessValue<int>(null, string.Empty))
                                .GetGenericMethodDefinition();

        protected Delegate Generator { get; set; }

        public IList ReadAll(Type elementType, ISqlDataReader reader)
        {
            var result = CreateList(elementType);

            while (reader.Read())
            {
                var element = ReadSingle(elementType, reader);
                result.Add(element);
            }

            return result;
        }

        public object ReadSingle(ISqlDataReader reader)
        {
            try
            {
                var element = Generator.DynamicInvoke(reader);
                return element;
            }
            catch (TargetInvocationException exceptionWrapper)
            {
                throw exceptionWrapper.InnerException;
            }
        }

        protected static bool IsNullable(Type expectedType)
        {
            return expectedType.IsConstructedGenericType && expectedType.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        protected static TValue ProcessValue<TValue>(object value, string mappingExceptionTemplate)
        {
            var expectedType = typeof(TValue);
            if (value == null && (!expectedType.GetTypeInfo().IsValueType || IsNullable(expectedType)))
            {
                return default(TValue);
            }
            else if (value == null)
            {
                throw new MappingException(string.Format(mappingExceptionTemplate, expectedType.Name, "NULL"));
            }
            else if (expectedType.GetTypeInfo().IsAssignableFrom(value.GetType().GetTypeInfo()))
            {
                return (TValue)value;
            }

            var expectedTypeName = expectedType.Name;
            if (IsNullable(expectedType))
            {
                expectedTypeName = expectedType.GenericTypeArguments[0].Name;
            }

            throw new MappingException(string.Format(mappingExceptionTemplate, expectedTypeName, value.GetType().Name));
        }

        private static IList CreateList(Type elementType)
        {
            var listType = typeof(List<>).MakeGenericType(elementType);
            var result = (IList)Activator.CreateInstance(listType);
            return result;
        }

        private object ReadSingle(Type elementType, ISqlDataReader reader)
        {
            var item = ReadSingle(reader);
            var itemType = item.GetType();
            if (elementType.GetTypeInfo().IsAssignableFrom(itemType.GetTypeInfo()))
            {
                return item;
            }

            if (elementType.IsConstructedGenericType
                && elementType.GetGenericTypeDefinition() == typeof(ITableQuery<>))
            {
                var innerType = elementType.GenericTypeArguments[0];
                var type = typeof(MaterializedTableQuery<>).MakeGenericType(innerType);
                var constructor = type.GetTypeInfo().DeclaredConstructors.Single();
                var list = CreateList(innerType);
                list.Add(item);
                return constructor.Invoke(new object[] { list });
            }

            throw new NotImplementedException();
        }
    }
}