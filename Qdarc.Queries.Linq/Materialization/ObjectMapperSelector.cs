using System;
using System.Reflection;
using Qdarc.Queries.Formatter;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Materialization
{
    internal class ObjectMapperSelector : IObjectMapperSelector
    {
        private readonly IModelToSqlConvention _convention;

        public ObjectMapperSelector(IModelToSqlConvention convention)
        {
            _convention = convention;
        }

        public IObjectsSetMapper BuildSetMapper(ISqlExpression part, FormattedQuery query)
        {
            var selectCommand = part as SelectCommand;
            if (selectCommand != null)
            {
                var mapper = BuildSetMapper(selectCommand.Selection);
                return mapper;
            }

            var directlyMaterialized = part as IDirectlyMaterialized;
            if (directlyMaterialized != null)
            {
                var attribute = directlyMaterialized.ClrType.GetTypeInfo().GetCustomAttribute<MultipleResultAttribute>();
                if (attribute != null)
                {
                    return new MulitpleResultMapper(directlyMaterialized.ClrType, this);
                }

                return BuildDirectObjectsSetMapper(directlyMaterialized.ClrType);
            }

            throw new NotImplementedException();
        }

        public IObjectsSetMapper BuildSetMapper(ISqlSelection selection)
        {
            return BuildMapper(selection);
        }

        public ISingleObjectsMapper BuildSingleObjectMapper(ISqlExpression part, FormattedQuery query)
        {
            var selectCommand = part as SelectCommand;
            if (selectCommand != null)
            {
                var mapper = BuildSingleObjectMapper(selectCommand.Selection);
                return mapper;
            }

            throw new NotImplementedException();
        }

        public IObjectsSetMapper BuildDirectObjectsSetMapper(Type type)
        {
            if (type.GetTypeInfo().IsClass && (type != typeof(string)))
            {
                return new DirectObjectsSetMapper(type, _convention);
            }

            return new SimpleTypeMapper(type);
        }

        public ISingleObjectsMapper BuildSingleObjectMapper(ISqlSelection selection)
        {
            return BuildMapper(selection);
        }

        private BaseObjectMapper BuildMapper(ISqlSelection selection)
        {
            if (selection is SqlSelection sqlSelection)
            {
                if (sqlSelection.ClrType.GetTypeInfo().IsClass && (sqlSelection.ClrType != typeof(string)))
                {
                    return new ComplexTypeObjectsSetMapper(sqlSelection);
                }

                return new SimpleTypeMapper(sqlSelection.ClrType);
            }
            else if (selection is ProjectionByConstructor projectonByConstructor)
            {
                return new ProjectionByConstructorObjectMapper(projectonByConstructor, this);
            }
            else if (selection is ProjectionByMembersAssignment projectionByMembersAssignment)
            {
                return new ComplexTypeObjectsSetMapper(projectionByMembersAssignment);
            }

            throw new NotImplementedException();
        }
    }
}