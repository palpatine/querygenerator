using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Materialization
{
    internal class ProjectionByConstructorObjectMapper : BaseObjectMapper
    {
        private static readonly MethodInfo ReadSingleMethod =
            ExpressionExtensions.GetMethod((ISingleObjectsMapper x) => x.ReadSingle(null));

        private static readonly MethodInfo SetItem = ExpressionExtensions.GetMethod((ISqlDataReader x) => x["a"]);
        private readonly IObjectMapperSelector _selector;

        public ProjectionByConstructorObjectMapper(ProjectionByConstructor selection, IObjectMapperSelector selector)
        {
            _selector = selector;
            var lambda = Generate(selection);
            Generator = lambda.Compile();
        }

        private static string GenerateConstructorFrendlyName(ConstructorInfo constructor)
        {
            return $"{constructor.DeclaringType.FullName}({string.Join(",", constructor.GetParameters().Select(x => x.ParameterType.FullName))})";
        }

        private LambdaExpression Generate(ProjectionByConstructor selection)
        {
            var parameter = Expression.Parameter(typeof(ISqlDataReader));
            var parameters = GetValues(selection.Elements, parameter);
            var convertedParameters = new List<Expression>();
            var constructorFrendlyName = GenerateConstructorFrendlyName(selection.Constructor);
            using (var enumerator = parameters.GetEnumerator())
            {
                foreach (var parameterInfo in selection.Constructor.GetParameters())
                {
                    enumerator.MoveNext();
                    var processValue = ProcessValueMethodDefinition.MakeGenericMethod(parameterInfo.ParameterType);
                    var exceptionTemplate = $"Expected type {{0}} but {{1}} was provided by reader for {parameterInfo.Name} parameter of {constructorFrendlyName}.";
                    var parameterValue = Expression.Call(
                        processValue,
                        enumerator.Current,
                        Expression.Constant(exceptionTemplate));
                    convertedParameters.Add(parameterValue);
                }
            }

            var result = Expression.New(selection.Constructor, convertedParameters);
            var type = typeof(Func<,>).MakeGenericType(typeof(ISqlDataReader), selection.Constructor.DeclaringType);
            var expression = Expression.Lambda(type, result, parameter);
            return expression;
        }

        private IEnumerable<Expression> GetValues(IEnumerable<ISelectionItem> elements, Expression parameter)
        {
            var result = new List<Expression>();
            foreach (var selectionItem in elements)
            {
                var aliasedSelectionItem = selectionItem as AliasedSelectionItem;

                if (aliasedSelectionItem != null)
                {
                    var alias = Expression.Constant(aliasedSelectionItem.ResolvedAlias);
                    result.Add(Expression.Call(parameter, SetItem, alias));
                    continue;
                }

                var prefixedSelection = selectionItem as PrefixedSelection;
                if (prefixedSelection != null)
                {
                    var mapper = _selector.BuildSingleObjectMapper(prefixedSelection.Selection);
                    var value = Expression.Call(Expression.Constant(mapper), ReadSingleMethod, parameter);
                    result.Add(value);
                    continue;
                }

                var selectCommandAsSelectionItem = selectionItem as SelectCommandAsSelectionItem;
                if (selectCommandAsSelectionItem != null)
                {
                    var mapper = _selector.BuildDirectObjectsSetMapper(
                        selectCommandAsSelectionItem
                            .Command.Selection.Elements.Single().ClrType);
                    var value = Expression.Call(Expression.Constant(mapper), ReadSingleMethod, parameter);
                    result.Add(value);
                    continue;
                }

                throw new NotImplementedException();
            }

            return result;
        }
    }
}