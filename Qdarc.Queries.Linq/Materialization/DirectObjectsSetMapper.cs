using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Materialization
{
    internal class DirectObjectsSetMapper : BaseObjectMapper
    {
        private readonly IModelToSqlConvention _convention;

        public DirectObjectsSetMapper(
            Type resultType,
            IModelToSqlConvention convention)
        {
            _convention = convention;
            var lambda = Generate(resultType);
            Generator = lambda.Compile();
        }

        private LambdaExpression Generate(Type returnType)
        {
            var parameter = Expression.Parameter(typeof(ISqlDataReader));
            var setItem = ExpressionExtensions.GetMethod((ISqlDataReader x) => x[default(string)]);
            var blockContent = new List<Expression>();
            var local = Expression.Variable(returnType);
            var result = Expression.New(returnType.GetTypeInfo().DeclaredConstructors.Single());
            var assingLocal = Expression.Assign(local, result);
            blockContent.Add(assingLocal);
            foreach (var selectionItem in returnType.GetRuntimeProperties())
            {
                var columnName = _convention.GetColumnName(selectionItem);
                var rawValue = Expression.Call(
                    parameter,
                    setItem,
                    Expression.Constant(columnName, typeof(string)));
                var property = Expression.Property(local, selectionItem.Name);
                var processValue = ProcessValueMethodDefinition.MakeGenericMethod(selectionItem.PropertyType);
                var exceptionTemplate =
                    $"Expected type {{0}} but {{1}} was provided by reader for {selectionItem.Name} property of {returnType.FullName}.";
                var value = Expression.Call(
                    processValue,
                    rawValue,
                    Expression.Constant(exceptionTemplate));
                var assigne = Expression.Assign(property, value);
                blockContent.Add(assigne);
            }

            blockContent.Add(local);
            var type = typeof(Func<,>).MakeGenericType(typeof(ISqlDataReader), returnType);
            var block = Expression.Block(new[] { local }, blockContent);
            var expression = Expression.Lambda(type, block, parameter);
            return expression;
        }
    }
}