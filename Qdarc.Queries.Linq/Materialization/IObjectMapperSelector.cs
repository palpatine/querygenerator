﻿using System;
using Qdarc.Queries.Formatter;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Materialization
{
    public interface IObjectMapperSelector
    {
        IObjectsSetMapper BuildSetMapper(ISqlExpression part, FormattedQuery query);

        IObjectsSetMapper BuildSetMapper(ISqlSelection selection);

        ISingleObjectsMapper BuildSingleObjectMapper(ISqlExpression part, FormattedQuery query);

        ISingleObjectsMapper BuildSingleObjectMapper(ISqlSelection selection);
        IObjectsSetMapper BuildDirectObjectsSetMapper(Type type);
    }
}