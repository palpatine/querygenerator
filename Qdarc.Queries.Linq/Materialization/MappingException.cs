﻿using System;

namespace Qdarc.Queries.Linq.Materialization
{
    public class MappingException : Exception
    {
        public MappingException(string message)
            : base(message)
        {
        }

        public MappingException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }
}