﻿using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Materialization
{
    public interface ISingleObjectsMapper
    {
        object ReadSingle(ISqlDataReader reader);
    }
}