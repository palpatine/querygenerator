using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Materialization
{
    internal class ComplexTypeObjectsSetMapper : BaseObjectMapper
    {
        public ComplexTypeObjectsSetMapper(SqlSelection selection)
        {
            var lambda = Generate(selection);
            Generator = lambda.Compile();
        }

        public ComplexTypeObjectsSetMapper(ProjectionByMembersAssignment selection)
        {
            var lambda = Generate(selection);
            Generator = lambda.Compile();
        }

        private static LambdaExpression Generate(ISqlSelection selection)
        {
            var parameter = Expression.Parameter(typeof(ISqlDataReader));
            var setItem = ExpressionExtensions.GetMethod((ISqlDataReader x) => x[default(string)]);
            var blockContent = new List<Expression>();
            var local = Expression.Variable(selection.ClrType);
            var result = Expression.New(selection.ClrType.GetTypeInfo().DeclaredConstructors.Single());
            var assingLocal = Expression.Assign(local, result);
            blockContent.Add(assingLocal);
            foreach (var selectionItem in selection.Elements)
            {
                var aliasedSelectionItem = selectionItem as IAliasedSelectionItem;
                if (aliasedSelectionItem != null)
                {
                    var rawValue = Expression.Call(
                        parameter,
                        setItem,
                        Expression.Constant(aliasedSelectionItem.ResolvedAlias, typeof(string)));
                    Expression setTarget;
                    Type memberToSetType;
                    var member = GetMemberToAssign(aliasedSelectionItem);
                    if (member is PropertyInfo propertyInfo)
                    {
                        setTarget = Expression.Property(local, propertyInfo);
                        memberToSetType = propertyInfo.PropertyType;
                    }
                    else if (member is FieldInfo fieldInfo)
                    {
                        setTarget = Expression.Field(local, fieldInfo);
                        memberToSetType = fieldInfo.FieldType;
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }

                    var processValue = ProcessValueMethodDefinition.MakeGenericMethod(memberToSetType);
                    var exceptionTemplate = $"Expected type {{0}} but {{1}} was provided by reader for {member.Name} property of {selection.ClrType.FullName}.";
                    var value = Expression.Call(
                        processValue,
                        rawValue,
                        Expression.Constant(exceptionTemplate));
                    var assigne = Expression.Assign(setTarget, value);
                    blockContent.Add(assigne);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            blockContent.Add(local);
            var type = typeof(Func<,>).MakeGenericType(typeof(ISqlDataReader), selection.ClrType);
            var block = Expression.Block(new[] { local }, blockContent);
            var expression = Expression.Lambda(type, block, new[] { parameter });
            return expression;
        }

        private static MemberInfo GetMemberToAssign(IAliasedSelectionItem aliasedSelectionItem)
        {
            if (aliasedSelectionItem is BoundSelectionItem boundSelectionItem)
            {
                return boundSelectionItem.Member;
            }

            var item = aliasedSelectionItem.Item as LinqColumnReference;
            PropertyInfo itemProperty;
            if (item != null)
            {
                itemProperty = item.Property;
            }
            else
            {
                throw new NotImplementedException();
            }

            return itemProperty;
        }
    }
}