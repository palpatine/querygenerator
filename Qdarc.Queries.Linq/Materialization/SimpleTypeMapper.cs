using System;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Materialization
{
    internal class SimpleTypeMapper : BaseObjectMapper
    {
        private const string MappingExceptionTemplate = "Expected type {0} but {1} was provided by reader.";

        public SimpleTypeMapper(Type resultType)
        {
            var method = ExpressionExtensions.GetMethod(() => Read<int>(null))
                 .GetGenericMethodDefinition()
                 .MakeGenericMethod(resultType);
            var delegateType = typeof(Func<,>).MakeGenericType(typeof(ISqlDataReader), resultType);
            Generator = method.CreateDelegate(delegateType);
        }

        private static TValue Read<TValue>(ISqlDataReader reader)
        {
            var rawValue = reader[0];
            return ProcessValue<TValue>(rawValue, MappingExceptionTemplate);
        }
    }
}