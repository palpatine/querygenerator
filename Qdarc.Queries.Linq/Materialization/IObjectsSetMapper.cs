﻿using System;
using System.Collections;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Materialization
{
    public interface IObjectsSetMapper
    {
        IList ReadAll(Type elementType, ISqlDataReader reader);
    }
}