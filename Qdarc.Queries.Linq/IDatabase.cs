﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Linq
{
    public interface IDatabase
    {
        IFunctionQuery<TResult> Function<TResult>(
            ElementName name,
            params ConstantExpression[] arguments);

        IFunctionQuery<TResult> Function<TResult>(
            string name,
            params ConstantExpression[] arguments);

        IFunctionQuery<TResult> Function<TResult>(
            string name,
            string schema,
            params ConstantExpression[] arguments);

        IFunctionQuery<TResult> Function<TResult>(
                    string name,
                    string schema,
                    string catalog,
                    params ConstantExpression[] arguments);

        IValuesQuery<TResult> Values<TResult>(
            IEnumerable<TResult> list);

        IEnumerable<TResult> Procedure<TResult>(
                    ElementName name,
                    params Expression[] arguments);

        void Procedure(
                    ElementName name,
                    params Expression[] arguments);

        ITableQuery<TStorable> Query<TStorable>();

        ITableQuery<TStorable> Query<TStorable>(Type concreteType);

        ITableQuery Query(Type concreteType);

        TResult ScalarFunction<TResult>(Expression<Func<IDatabase, TResult>> method);

        ITableParameter<TValue> TableParameter<TValue>(IEnumerable<TValue> value);

        int Insert<TEntity>(IEnumerable<TEntity> entities);

        int Insert<TEntity>(params TEntity[] entity);

        int Insert<TEntity>(ITableQuery<TEntity> source);

        int Insert<TEntity>(
            IValuesQuery<TEntity> entities);
    }
}