﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq
{
    public interface ILinqExpressionHandler
    {
        int Priority { get; }

        ExpressionType HandledType { get; }

        bool CanHandle(Expression expression);

        ISqlExpression Handle(Expression expression);

        void AttachContext(IVisitationContext context);
    }
}