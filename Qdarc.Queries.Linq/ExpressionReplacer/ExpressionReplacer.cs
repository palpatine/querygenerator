﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.ExpressionReplacer
{
    internal class ExpressionReplacer<TReplaced> : IExpressionReplacer<TReplaced>, IVisitationHandler<TReplaced, TReplaced>
        where TReplaced : ISqlExpression
    {
        private readonly IVisitor _visitor;
        private TReplaced _added;
        private TReplaced _removed;

        public ExpressionReplacer(IVisitor visitor)
        {
            _visitor = visitor;
            _visitor.Intercept(this);
        }

        public TExpression Replace<TExpression>(TExpression tree, TReplaced removed, TReplaced added)
        where TExpression : ISqlExpression
        {
            _removed = removed;
            _added = added;
            return tree.Accept<TExpression>(_visitor);
        }

        public TReplaced Handle(TReplaced expression, IVisitor transformer)
        {
            if (ReferenceEquals(expression, _removed))
            {
                return _added;
            }

            return expression;
        }
    }
}