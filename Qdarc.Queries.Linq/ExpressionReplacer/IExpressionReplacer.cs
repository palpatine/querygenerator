﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.ExpressionReplacer
{
    public interface IExpressionReplacer<TReplaced>
            where TReplaced : ISqlExpression
    {
        TExpression Replace<TExpression>(TExpression tree, TReplaced removed, TReplaced added)
            where TExpression : ISqlExpression;
    }
}