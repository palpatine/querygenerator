﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators.ExplicitSelectionListTransformationHandler;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators.SourceReplacingHandlers;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators.Transformers;
using Qdarc.Queries.Linq.ExpressionReplacer;
using Qdarc.Queries.Linq.Finalizers;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.SqlTypeFinders;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IServiceCollection registrar)
        {
            registrar.AddTransient<IVisitationContext, VisitationContext>();
            registrar.AddTransient<IDatabase, Database>();
            registrar.AddTransient<ITableQueryProvider, TableQueryProvider>();
            registrar.AddTransient<IObjectMapperSelector, ObjectMapperSelector>();
            registrar.AddTransient<IParametersBinder, ParametersBinder>();

            RegisterExplicitSelectionListHandlers(registrar);
            RegisterLinqExpressionHandlers(registrar);
            RegisterSqlTypeFinders(registrar);
            registrar.AddTransient<IResultFinalizer, SingleResultFinalizer>();

            registrar.AddTransient(typeof(IExpressionReplacer<>), typeof(ExpressionReplacer<>));
        }

        private static void RegisterExplicitSelectionListHandlers(IServiceCollection registrar)
        {
            registrar.AddTransient<IExplicitSelectionListGenerator, ExplicitSelectionListGenerator>();
            registrar.AddTransient<ISelectionListResolver<AliasedSelectionItem, ISelectionItem>, AliasedSelectionItemSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<BoundSelectionItem, BoundSelectionItem>, BoundSelectionItemSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<LinqColumnReference, ISelectionItem>, LinqColumnReferenceSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<ProjectionByConstructor, ProjectionByConstructor>, ProjectionByConstructorSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<ProjectionByConstructor, ISqlSelection>, ProjectionByConstructorSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<ProjectionByMembersAssignment, ProjectionByMembersAssignment>, ProjectionByMembersAssignmentSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<ProjectionByMembersAssignment, ISqlSelection>, ProjectionByMembersAssignmentSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<SqlSelection, ISqlSelection>, SqlSelectionSourceSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<AliasedSourceItem, ISqlSelection>, AliasedSourceItemSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<SelectCommand, ISqlSelection>, SelectCommandsSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<UnionAllSelectionCommand, ISqlSelection>, SelectCommandsSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<UnionSelectionCommand, ISqlSelection>, SelectCommandsSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<LinqTableParameterReference, ISqlSelection>, NamedSourcesSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<TabularFunctionCall, ISqlSelection>, NamedSourcesSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<TableReference, ISqlSelection>, NamedSourcesSelectionListResolver>();
            registrar.AddTransient<ISelectionListResolver<PrefixedSelection, ISelectionItem>, PrefixedSelectionSelectionListResolver>();
        }

        private static void RegisterLinqExpressionHandlers(IServiceCollection registrar)
        {
            registrar.AddTransient<ILinqExpressionHandler, ConstantExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, BitwiseAndArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, AddBinaryArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, BitwiseOrArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, DivideBinaryArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ExclusiveOrBinaryArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ModuloBinaryArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, MultiplyBinaryArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, SubtractBinaryArithmeticExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ParameterExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, InstanceMemberExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, FilterExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ProjectionExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, JoinEpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, CrossJoinEpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, EqualLogicalExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, GreaterThanLogicalExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, GreaterThanOrEqualLogicalExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, LessThanLogicalExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, LessThanOrEqualLogicalExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, NotEqualLogicalExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, AndAlsoLogicalConjunctionExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ExclusiveOrLogicalConjunctionExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, OrElseLogicalConjunctionExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, NewExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, MemberInitializationExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ConvertExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, OrderExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, AnyExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, TopExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, SingleExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, DateTimeMemberExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ParseExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, DirectSelectionExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ScalarFunctionCallExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, CountExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, CountFunctionExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, ValuesQueryConstantExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, QueryCallExpressionHanlder>();
            registrar.AddTransient<ILinqExpressionHandler, DatabaseInsertByValuesMethodCallHandler>();
            registrar.AddTransient<ILinqExpressionHandler, DatabaseInsertByQueryMethodCallHandler>();
            registrar.AddTransient<ILinqExpressionHandler, UpdateStatementHandler>();
            registrar.AddTransient<ILinqExpressionHandler, NotExpressionHandler>();
            registrar.AddTransient<ILinqExpressionHandler, DeleteStatementHandler>();
        }

        private static void RegisterSqlTypeFinders(IServiceCollection registrar)
        {
            registrar.AddTransient<ISqlTypeFinder, SqlTypeFinder>();
            registrar.AddTransient<ISqlTypeFinderHandler<TableReference, ISqlType>, ClrTypeSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<AliasedSourceItem, ISqlType>, AliasedSourceItemSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<SelectCommand, ISqlType>, SelectCommandSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<SqlSelection, ISqlType>, SqlSelectionSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<ProjectionByConstructor, ISqlType>, ProjectionByConstructorSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<ProjectionByMembersAssignment, ISqlType>, SqlSelectionSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<AliasedSelectionItem, ISqlType>, AliasedSelectionItemSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<LinqColumnReference, ISqlType>, LinqSqlTypeReferenceSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<BoundSelectionItem, ISqlType>, BoundSelectionItemSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<LinqTableParameterReference, ISqlType>, ClrTypeSqlTypeFinderHandler>();
            registrar.AddTransient<ISqlTypeFinderHandler<PrefixedSelection, ISqlType>, PrefixedSelectionSqlTypeFinderHandler>();
        }
    }
}