﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq
{
    internal sealed class Database : IDatabase
    {
        private readonly Func<ITableQueryProvider> _tableQueryProviderFactory;

        public Database(
            Func<ITableQueryProvider> tableQueryProviderFactory)
        {
            _tableQueryProviderFactory = tableQueryProviderFactory;
        }

        public IFunctionQuery<TResult> Function<TResult>(
            string name,
            string schema,
            string catalog,
            params ConstantExpression[] arguments)
        {
            return Function<TResult>(new ElementName(catalog, schema, name), arguments);
        }

        public IFunctionQuery<TResult> Function<TResult>(
            string name,
            string schema,
            params ConstantExpression[] arguments)
        {
            return Function<TResult>(name, schema, null, arguments);
        }

        public IFunctionQuery<TResult> Function<TResult>(
            string name,
            params ConstantExpression[] arguments)
        {
            return Function<TResult>(name, null, null, arguments);
        }

        public IFunctionQuery<TResult> Function<TResult>(
            ElementName name,
            params ConstantExpression[] arguments)
        {
            var query = new FunctionQuery<TResult>(_tableQueryProviderFactory(), name, arguments);
            return query;
        }

        public IEnumerable<TResult> Procedure<TResult>(
                     ElementName name,
                     params Expression[] arguments)
        {
            var tableQueryProvider = _tableQueryProviderFactory();
            var parameters = arguments.Select(x => (ISqlValueExpression)tableQueryProvider.Visit(x)).ToArray();
            return tableQueryProvider.Execute<IEnumerable<TResult>>(new ProcedureCall(name, parameters, typeof(TResult)));
        }

        public void Procedure(
                     ElementName name,
                     params Expression[] arguments)
        {
            var tableQueryProvider = _tableQueryProviderFactory();
            var parameters = arguments.Select(x => (ISqlValueExpression)tableQueryProvider.Visit(x)).ToArray();
            tableQueryProvider.ExecuteNonQuery(new ProcedureCall(name, parameters, null));
        }

        public ITableQuery<TStorable> Query<TStorable>()
        {
            var tableQuery = new TableQuery<TStorable>(_tableQueryProviderFactory());
            return tableQuery;
        }

        public ITableQuery<TStorable> Query<TStorable>(Type concreteType)
        {
            var type = typeof(TableQuery<>).MakeGenericType(concreteType);
            var query = (ITableQuery<TStorable>)Activator.CreateInstance(type, _tableQueryProviderFactory());
            return query;
        }

        public ITableQuery Query(Type concreteType)
        {
            var type = typeof(TableQuery<>).MakeGenericType(concreteType);
            var query = (ITableQuery)Activator.CreateInstance(type, _tableQueryProviderFactory());
            return query;
        }

        public TResult ScalarFunction<TResult>(Expression<Func<IDatabase, TResult>> method)
        {
            return TableQuery.Select(_tableQueryProviderFactory(), method);
        }

        public ITableParameter<TValue> TableParameter<TValue>(IEnumerable<TValue> value)
        {
            var tableQuery = new TableParameter<TValue>(_tableQueryProviderFactory(), Expression.Constant(value, typeof(IEnumerable<TValue>)));
            return tableQuery;
        }

        public IValuesQuery<TResult> Values<TResult>(IEnumerable<TResult> list)
        {
            var query = new ValuesQuery<TResult>(
                _tableQueryProviderFactory(),
                Expression.Constant(list));
            return query;
        }

        public int Insert<TEntity>(
            IEnumerable<TEntity> entities)
        {
            return Insert(Values(entities));
        }

        public int Insert<TEntity>(
            params TEntity[] entity)
        {
            return Insert(entity.AsEnumerable());
        }

        public int Insert<TEntity>(
            ITableQuery<TEntity> source)
        {
            var provider = _tableQueryProviderFactory();
            var method = ExpressionExtensions.GetMethod((IDatabase d) => d.Insert((ITableQuery<TEntity>)null));
            var arguments = new Expression[]
            {
                Expression.Constant(source)
            };

            var expression = Expression.Call(
                Expression.Constant(this),
                method,
                arguments);

            return provider.ExecuteNonQuery(expression);
        }

        public int Insert<TEntity>(
            IValuesQuery<TEntity> entities)
        {
            var provider = _tableQueryProviderFactory();
            var method = ExpressionExtensions.GetMethod((IDatabase d) => d.Insert((IValuesQuery<TEntity>)null));
            var arguments = new Expression[]
            {
                Expression.Constant(entities)
            };

            var expression = Expression.Call(
                Expression.Constant(this),
                method,
                arguments);

            return provider.ExecuteNonQuery(expression);
        }
    }
}