﻿using System.Collections;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Linq.Queries;

namespace Qdarc.Queries.Linq.Finalizers
{
    internal sealed class SingleResultFinalizer : IResultFinalizer
    {
        public double Priority => 0;

        public bool TryFinalizeResult(IList data, Expression expression, out object result)
        {
            result = null;
            if (expression.NodeType != ExpressionType.Call)
            {
                return false;
            }

            var call = (MethodCallExpression)expression;

            if (Equals(typeof(TableQuery).GetTypeInfo(), call.Method.DeclaringType.GetTypeInfo())
                || Equals(typeof(CountExtensions).GetTypeInfo(), call.Method.DeclaringType.GetTypeInfo())
                || Equals(typeof(Queryable).GetTypeInfo(), call.Method.DeclaringType.GetTypeInfo()))
            {
                var resultList = data.Cast<object>();
                if (call.Method.Name == "First")
                {
                    result = resultList.First();
                    return true;
                }

                if (call.Method.Name == "FirstOrDefault")
                {
                    result = resultList.FirstOrDefault();
                    return true;
                }

                if (call.Method.Name == "Single"
                    || call.Method.Name == "Count"
                    || call.Method.Name == "CountDistinct"
                    || call.Method.Name == "Any"
                    || call.Method.Name == "All"
                    || (call.Method.Name == "Select" && Equals(typeof(TableQuery).GetTypeInfo(), call.Method.DeclaringType.GetTypeInfo())))
                {
                    result = resultList.Single();
                    return true;
                }

                if (call.Method.Name == "SingleOrDefault")
                {
                    result = resultList.SingleOrDefault();
                    return true;
                }
            }

            return false;
        }
    }
}
