﻿using System.Collections;
using System.Linq.Expressions;

namespace Qdarc.Queries.Linq.Finalizers
{
    public interface IResultFinalizer
    {
        double Priority { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1007:UseGenericsWhereAppropriate", Justification ="Generic method with type unknown on compile time.")]
        bool TryFinalizeResult(IList data, Expression expression, out object result);
    }
}
