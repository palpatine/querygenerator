﻿using System;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SqlColumnNameAttribute : Attribute
    {
        public SqlColumnNameAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; }
    }
}