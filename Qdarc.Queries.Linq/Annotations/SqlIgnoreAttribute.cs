﻿using System;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SqlIgnoreAttribute : Attribute
    {
    }
}