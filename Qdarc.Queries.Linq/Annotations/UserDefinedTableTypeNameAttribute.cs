﻿using System;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class UserDefinedTableTypeNameAttribute : Attribute
    {
        public UserDefinedTableTypeNameAttribute(string table, string schema = null, string catalog = null)
        {
            Table = table;
            Schema = schema;
            Catalog = catalog;
        }

        public string Catalog { get; }

        public string Schema { get; }

        public string Table { get; }

        public CustomTypeReference TypeReference => new CustomTypeReference(new ElementName(Catalog, Schema, Table));
    }
}