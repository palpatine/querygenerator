﻿using System;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Method)]
    public sealed class ScalarFunctionAttribute : Attribute
    {
        public ScalarFunctionAttribute(string functionName, string schema = null, string catalog = null)
        {
            FunctionName = functionName;
            Schema = schema;
            Catalog = catalog;
        }

        public string Catalog { get; }

        public string Schema { get; }

        public string FunctionName { get; }

        public ElementName Name => new ElementName(Catalog, Schema, FunctionName);
    }
}
