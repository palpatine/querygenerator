﻿using System;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SqlColumnOrderAttribute : Attribute
    {
        public SqlColumnOrderAttribute(int ordinal)
        {
            Ordinal = ordinal;
        }

        public int Ordinal { get; }
    }
}