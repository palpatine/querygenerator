﻿using System;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class MultipleResultAttribute : Attribute
    {
    }
}