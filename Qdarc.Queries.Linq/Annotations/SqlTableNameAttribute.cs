﻿using System;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class SqlTableNameAttribute : Attribute
    {
        public SqlTableNameAttribute(string table, string schema = null, string catalog = null)
        {
            Table = table;
            Schema = schema;
            Catalog = catalog;
        }

        public string Catalog { get; }

        public string Schema { get; }

        public string Table { get; }

        public ElementName TableName => new ElementName(Catalog, Schema, Table);
    }
}