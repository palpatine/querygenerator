﻿using System;

namespace Qdarc.Queries.Linq.Annotations
{
    public class AnnotationException : Exception
    {
        public AnnotationException(string message)
            : base(message)
        {
        }

        public AnnotationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}