﻿using System;

namespace Qdarc.Queries.Linq.Annotations
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class SourceResultAttribute : Attribute
    {
        public SourceResultAttribute(int resultId)
        {
            ResultId = resultId;
        }

        public int ResultId { get; }
    }
}