﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ValuesQueryConstantExpressionHandler : LinqExpressionHandler<ConstantExpression>
    {
        public ValuesQueryConstantExpressionHandler()
            : base(ExpressionType.Constant)
        {
        }

        public override int Priority => 1;

        protected override bool CanHandle(ConstantExpression expression)
        {
            if (expression.Type.IsConstructedGenericType)
            {
                var type = expression.Type.GetGenericTypeDefinition();
                return type == typeof(IValuesQuery<>)
                       || type == typeof(ValuesQuery<>);
            }

            return false;
        }

        protected override ISqlExpression Handle(ConstantExpression expression)
        {
            var innerType = expression.Type.GenericTypeArguments[0];
            var query = (IValuesQuery)expression.Value;
            IList<ColumnAlias> columnAliases;
            IEnumerable<ValueSourceRow> rows;
            if (innerType != typeof(string) && innerType.GetTypeInfo().IsClass)
            {
                var columns = Context.ModelToSqlConvention.GetSelectableProperties(innerType).ToArray();
                rows = CreateComplexValuesList(query, columns.ToArray()).ToArray();
                columnAliases = columns
                    .Select((x, i) =>
                    {
                        var alias = Context.ModelToSqlConvention.GetColumnName(x);
                        var sqlType = Context.ModelToSqlConvention.TryGetSqlType(innerType, x) ?? GetTypeForColumn(rows, i);
                        return new ColumnAlias(
                            alias,
                            x.PropertyType,
                            sqlType);
                    })
                    .ToArray();
            }
            else
            {
                rows = CreateSimpleValuesList(query, innerType).ToArray();
                var columnType = GetTypeForColumn(rows);

                columnAliases = new[]
                {
                    new ColumnAlias(
                        "Value",
                        innerType,
                        columnType)
                };
            }

            rows = rows.Select(
                    x => new ValueSourceRow(
                        x.Values
                            .Select((z, i) => new SqlCastExpression(z, columnAliases[i].ClrType, columnAliases[i].SqlType))
                            .ToArray()))
                .ToArray();

            var values = new ValuesSource(rows, innerType);
            return new AliasedSourceItem(values, columnAliases);
        }

        private ISqlType GetTypeForColumn(IEnumerable<ValueSourceRow> rows, int columnId = 0)
        {
            var valuesTypes = rows.Select(x => x.Values.ElementAt(columnId).SqlType).ToArray();

            var columnType = valuesTypes.Skip(1)
                .Aggregate(valuesTypes.First(), Context.TypeManager.GetSqlType);

            return columnType;
        }

        private IEnumerable<ValueSourceRow> CreateComplexValuesList(IValuesQuery query, PropertyInfo[] runtimeProperties)
        {
            return ((IEnumerable)query.Values.Value).Cast<object>()
                .Select(x => new ValueSourceRow(runtimeProperties.Select(z =>
                    {
                        var value = z.GetValue(x);
                        return new SqlConstantExpression(
                            value,
                            z.PropertyType,
                            Context.TypeManager.GetSqlType(z.PropertyType, value));
                    }).ToArray())).ToArray();
        }

        private IEnumerable<ValueSourceRow> CreateSimpleValuesList(IValuesQuery query, Type innerType)
        {
            return ((IEnumerable)query.Values.Value).Cast<object>()
                .Select(x => new ValueSourceRow(new[]
                    {
                        new SqlConstantExpression(
                            x,
                            innerType,
                            Context.TypeManager.GetSqlType(innerType, x))
                    })).ToArray();
        }
    }
}