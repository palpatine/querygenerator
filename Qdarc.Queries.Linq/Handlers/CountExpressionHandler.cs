﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class CountExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public CountExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.DeclaringType == typeof(CountExtensions);
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var isDistinct = expression.Method.Name.Contains("Distinct");
            var query = Context.VisitExpression(expression.Arguments[0]);
            var selection = Context.EnsureSelection(query);
            var selectCommand = selection as SelectCommand;

            if (selectCommand == null)
            {
                selectCommand = Context.WrapInSelectCommand(selection);
            }

            ISelectionItem sqlCountExpression;

            if (expression.Arguments.Count == 2)
            {
                var quote = (UnaryExpression)expression.Arguments[1];
                var lambda = (LambdaExpression)quote.Operand;
                var parameter = lambda.Parameters.Single();

                Context.AddParameterMapping(parameter, selectCommand);
                var innerExpression = Context.VisitExpression(lambda.Body);
                Context.RemoveParameterMapping(parameter);

                var valueExpression = innerExpression as ISqlValueExpression;

                if (valueExpression != null)
                {
                    sqlCountExpression = new SqlCountExpression(
                        valueExpression,
                        isDistinct,
                        typeof(int),
                        Context.TypeManager.Int);
                }
                else
                {
                    var source = innerExpression as ISource;
                    if (source != null)
                    {
                        throw new InvalidModelException(
                               "Count expression cannot point to source item. Invalid expression '.Count(x)' was used rather than one pointing to column '.Count(x.Column)'.",
                               source);
                    }

                    throw new NotSupportedException();
                }
            }
            else
            {
                sqlCountExpression = new SqlAsteriskCountExpression(
                    typeof(int),
                    Context.TypeManager.Int);
            }

            var sqlCountExpressions = new[]
            {
                sqlCountExpression
            };
            selectCommand.Selection = new SqlSelection(
                sqlCountExpressions,
                typeof(int));

            return selectCommand;
        }
    }
}