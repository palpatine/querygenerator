using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class CrossJoinEpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public CrossJoinEpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.IsGenericMethod
                && expression.Method.Name == "CrossJoin";
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var genericArguments = expression.Method.GetGenericArguments();
            var isWithProjection = expression.Method.GetParameters().Length == 3;
            var left = (ISource)Context.VisitExpression(expression.Arguments[0]);
            var right = (ISource)Context.VisitExpression(expression.Arguments[1]);
            var rightNamed = right as AliasedSourceItem ?? new AliasedSourceItem(right);
            ISqlSelection selection = null;

            List<ISource> tables;
            if ((isWithProjection && genericArguments.Length == 3) || (!isWithProjection && genericArguments.Length == 2))
            {
                var leftNamed = left as AliasedSourceItem ?? new AliasedSourceItem(left);
                left = leftNamed;
                tables = new List<ISource> { leftNamed };
                if (!isWithProjection)
                {
                    selection =
                        new ProjectionByConstructor(
                            expression.Type.GenericTypeArguments[0].GetTypeInfo().DeclaredConstructors.Single(),
                            new ISelectionItem[]
                            {
                                new PrefixedSelection("Item1", Context.GenerateExplicitSelectionList(leftNamed)),
                                new PrefixedSelection("Item2", Context.GenerateExplicitSelectionList(rightNamed))
                            });
                }
            }
            else
            {
                var selectCommand = (SelectCommand)left;
                left = selectCommand.Source;
                var leftProjection = (ProjectionByConstructor)selectCommand.Selection;
                tables = FindTables(selectCommand.Source, genericArguments.Length - (isWithProjection ? 2 : 1)).ToList();
                if (!isWithProjection)
                {
                    selection =
                        new ProjectionByConstructor(
                            expression.Type.GenericTypeArguments[0].GetTypeInfo().DeclaredConstructors.Single(),
                            leftProjection.Elements.Concat(new ISelectionItem[]
                            {
                                new PrefixedSelection("Item" + genericArguments.Length, Context.GenerateExplicitSelectionList(rightNamed))
                            })
                                .ToArray());
                }
            }

            var join = new CrossJoinSource(left, rightNamed);

            if (expression.Arguments.Count == 2)
            {
                return new SelectCommand(
                    source: join,
                    selection: selection);
            }

            var projectionQuote = (UnaryExpression)expression.Arguments[2];
            var projectionLambda = (LambdaExpression)projectionQuote.Operand;

            for (var i = 0; i < projectionLambda.Parameters.Count - 1; i++)
            {
                Context.AddParameterMapping(projectionLambda.Parameters[i], tables[i]);
            }

            Context.AddParameterMapping(projectionLambda.Parameters.Last(), rightNamed);

            var projection = Context.VisitExpression(projectionLambda.Body);

            foreach (var parameterExpression in projectionLambda.Parameters)
            {
                Context.RemoveParameterMapping(parameterExpression);
            }

            return new SelectCommand(
                source: join,
                selection: Context.ConvertToSelectionList(projection));
        }

        private IEnumerable<ISource> FindTables(ISource root, int count)
        {
            var join = (CrossJoinSource)root;

            if (count == 2)
            {
                return new[] { join.Left, join.Right };
            }
            else
            {
                return FindTables(join.Left, count - 1).Concat(new[] { join.Right });
            }
        }
    }
}