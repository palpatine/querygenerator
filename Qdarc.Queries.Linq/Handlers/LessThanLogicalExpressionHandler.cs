using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class LessThanLogicalExpressionHandler : LogicalExpressionHandler
    {
        public LessThanLogicalExpressionHandler()
            : base(ExpressionType.LessThan)
        {
        }

        protected override ISqlExpression Create(ISqlValueExpression leftValue, ISqlValueExpression rightValue)
        {
            return new LessThanLogicalExpression(leftValue, rightValue);
        }
    }
}