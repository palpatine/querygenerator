﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.ExpressionReplacer;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class DeleteStatementHandler : LinqExpressionHandler<MethodCallExpression>
    {
        private IExpressionReplacer<INamedSource> _expressionReplacer;

        public DeleteStatementHandler(IExpressionReplacer<INamedSource> expressionReplacer)
            : base(ExpressionType.Call)
        {
            _expressionReplacer = expressionReplacer;
        }

        protected override bool CanHandle(MethodCallExpression expression)
            => expression.Method.DeclaringType == typeof(TableQueryDelete);

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var target = Context.VisitExpression(expression.Arguments[0]);
            INamedSource deletionTarget = null;
            ISqlLogicalExpression filter = null;
            ISource source = null;

            if (target is INamedSource namedSource)
            {
                deletionTarget = namedSource;
            }
            else if (target is SelectCommand selectCommand)
            {
                if (expression.Arguments.Count == 2)
                {
                    deletionTarget = (INamedSource)ProcessLabda(expression.Arguments[1], selectCommand.Source);
                    source = selectCommand.Source;
                }
                else if (selectCommand.Source is AliasedSourceItem aliasedSourceItem)
                {
                    deletionTarget = (INamedSource)aliasedSourceItem.Source;
                    if (selectCommand.Filter != null)
                    {
                        filter = _expressionReplacer.Replace(selectCommand.Filter, aliasedSourceItem, deletionTarget);
                    }
                }
                else
                {
                    deletionTarget = (INamedSource)selectCommand.Source;
                    filter = selectCommand.Filter;
                }
            }

            return new DeleteCommand(deletionTarget, filter, source);
        }

        private ISqlExpression ProcessLabda(Expression selector, ISource source)
        {
            var quote = (UnaryExpression)selector;
            var lambda = (LambdaExpression)quote.Operand;
            IEnumerable<ISource> sources;
            if (source is IMultisource multisource)
            {
                sources = multisource.Sources;
            }
            else
            {
                sources = new[]
                {
                    source
                };
            }

            using (var enumerator = sources.GetEnumerator())
            {
                foreach (var parameter in lambda.Parameters)
                {
                    enumerator.MoveNext();
                    Context.AddParameterMapping(parameter, enumerator.Current);
                }
            }

            var expression = Context.VisitExpression(lambda.Body);

            foreach (var parameter in lambda.Parameters)
            {
                Context.RemoveParameterMapping(parameter);
            }

            return expression;
        }
    }
}