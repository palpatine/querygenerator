﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class FilterExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public FilterExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.Name == "Where";
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var source = Context.VisitExpression(expression.Arguments[0]);

            if (source is SelectCommand selectCommand)
            {
                if (!Context.CanApplyFilterDirectly(selectCommand))
                {
                    selectCommand = Context.WrapInSelectCommand(selectCommand);
                }
            }
            else
            {
                selectCommand = Context.WrapInSelectCommand(source);
            }

            var logicalExpression = Context.ProcessSingleParameterFilter(
                (UnaryExpression)expression.Arguments[1],
                selectCommand);
            selectCommand.MergeFilter(logicalExpression);

            return selectCommand;
        }
    }
}