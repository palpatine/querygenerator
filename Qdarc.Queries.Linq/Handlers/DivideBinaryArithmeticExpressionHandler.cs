using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class DivideBinaryArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public DivideBinaryArithmeticExpressionHandler()
            : base(ExpressionType.Divide)
        {
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
            return new DivideValueExpression(left, right, type, sqlType);
        }
    }
}