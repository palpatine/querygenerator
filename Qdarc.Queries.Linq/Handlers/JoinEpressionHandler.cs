using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class JoinEpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public JoinEpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.IsGenericMethod
                && (expression.Method.Name == "Join"
                    || expression.Method.Name == "LeftJoin"
                    || expression.Method.Name == "RightJoin"
                    || expression.Method.Name == "FullJoin");
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var left = (ISource)Context.VisitExpression(expression.Arguments[0]);
            var right = (ISource)Context.VisitExpression(expression.Arguments[1]);
            var rightNamed = right as AliasedSourceItem ?? new AliasedSourceItem(right);
            var conditionQuote = (UnaryExpression)expression.Arguments[2];
            var conditionLambda = (LambdaExpression)conditionQuote.Operand;
            ISqlSelection selection = null;

            List<ISource> tables;
            if (conditionLambda.Parameters.Count == 2)
            {
                var leftNamed = left as AliasedSourceItem ?? new AliasedSourceItem(left);
                left = leftNamed;
                tables = new List<ISource> { leftNamed };

                if (expression.Arguments.Count == 3)
                {
                    selection =
                        new ProjectionByConstructor(
                            expression.Type.GenericTypeArguments[0].GetTypeInfo().DeclaredConstructors.Single(),
                            new[]
                            {
                                new PrefixedSelection("Item1", Context.GenerateExplicitSelectionList(leftNamed)),
                                new PrefixedSelection("Item2", Context.GenerateExplicitSelectionList(rightNamed))
                            });
                }
            }
            else
            {
                var selectCommand = (SelectCommand)left;
                left = selectCommand.Source;
                tables = FindTables(selectCommand.Source, conditionLambda.Parameters.Count - 1).ToList();
                var leftProjection = (ProjectionByConstructor)selectCommand.Selection;

                if (expression.Arguments.Count == 3)
                {
                    selection =
                        new ProjectionByConstructor(
                            expression.Type.GenericTypeArguments[0].GetTypeInfo().DeclaredConstructors.Single(),
                            leftProjection.Elements.Concat(new[]
                            {
                                new PrefixedSelection("Item" + conditionLambda.Parameters.Count, Context.GenerateExplicitSelectionList(rightNamed))
                            })
                                .ToArray());
                }
            }

            var @join = CreateConditionalJoinSource(expression, conditionLambda, tables, rightNamed, left);

            if (expression.Arguments.Count == 3)
            {
                return new SelectCommand(
                    source: join,
                    selection: selection);
            }

            return HandleProjection(expression, tables, rightNamed, @join);
        }

        private static ConditionalJoinSource CreateJoin(
            MethodCallExpression callExpression,
            ISource left,
            ISource right,
            ISqlLogicalExpression on)
        {
            if (callExpression.Method.Name == "Join")
            {
                return new InnerJoinSource(left, right, on);
            }
            else if (callExpression.Method.Name == "LeftJoin")
            {
                return new LeftJoinSource(left, right, on);
            }
            else if (callExpression.Method.Name == "RightJoin")
            {
                return new RightJoinSource(left, right, on);
            }
            else if (callExpression.Method.Name == "FullJoin")
            {
                return new FullJoinSource(left, right, on);
            }

            throw new InvalidOperationException();
        }

        private ConditionalJoinSource CreateConditionalJoinSource(
                    MethodCallExpression expression,
                    LambdaExpression conditionLambda,
                    List<ISource> tables,
                    INamedSource rightNamed,
                    ISource left)
        {
            for (var i = 0; i < conditionLambda.Parameters.Count - 1; i++)
            {
                Context.AddParameterMapping(conditionLambda.Parameters[i], tables[i]);
            }

            Context.AddParameterMapping(conditionLambda.Parameters.Last(), rightNamed);

            var on = (ISqlLogicalExpression)Context.VisitExpression(conditionLambda.Body);

            foreach (var parameterExpression in conditionLambda.Parameters)
            {
                Context.RemoveParameterMapping(parameterExpression);
            }

            var join = CreateJoin(expression, left, rightNamed, on);

            return join;
        }

        private IEnumerable<ISource> FindTables(ISource root, int count)
        {
            var join = (IJoinSource)root;

            if (count == 2)
            {
                return new[] { join.Left, join.Right };
            }
            else
            {
                return FindTables(join.Left, count - 1).Concat(new[] { join.Right });
            }
        }

        private ISqlExpression HandleProjection(
                    MethodCallExpression expression,
                    IReadOnlyList<ISource> tables,
                    ISource right,
                    ISource @join)
        {
            var projectionQuote = (UnaryExpression)expression.Arguments[3];
            var projectionLambda = (LambdaExpression)projectionQuote.Operand;

            for (var i = 0; i < projectionLambda.Parameters.Count - 1; i++)
            {
                Context.AddParameterMapping(projectionLambda.Parameters[i], tables[i]);
            }

            Context.AddParameterMapping(projectionLambda.Parameters.Last(), right);

            var projection = Context.VisitExpression(projectionLambda.Body);

            foreach (var parameterExpression in projectionLambda.Parameters)
            {
                Context.RemoveParameterMapping(parameterExpression);
            }

            return new SelectCommand(
                source: @join,
                selection: Context.ConvertToSelectionList(projection));
        }
    }
}