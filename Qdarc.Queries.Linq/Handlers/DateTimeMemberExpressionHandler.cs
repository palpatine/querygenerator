using System;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class DateTimeMemberExpressionHandler : LinqExpressionHandler<MemberExpression>
    {
        private static readonly PropertyInfo DateTimeNow = ExpressionExtensions.GetProperty(() => DateTime.Now);
        private static readonly PropertyInfo DateTimeUtcNow = ExpressionExtensions.GetProperty(() => DateTime.UtcNow);
        private static readonly PropertyInfo DateTimeToday = ExpressionExtensions.GetProperty(() => DateTime.Today);
        private static readonly MemberInfo DateTimeMinValue = ExpressionExtensions.GetMember(() => DateTime.MinValue);
        private static readonly MemberInfo DateTimeMaxValue = ExpressionExtensions.GetMember(() => DateTime.MaxValue);

        public DateTimeMemberExpressionHandler()
            : base(ExpressionType.MemberAccess)
        {
        }

        public override int Priority => 1;

        protected override bool CanHandle(MemberExpression expression)
        {
            return expression.Expression == null
                && expression.Member.DeclaringType == typeof(DateTime);
        }

        protected override ISqlExpression Handle(MemberExpression expression)
        {
            if (Equals(expression.Member, DateTimeNow))
            {
                return new CurrentDateAndTimeExpression(isUtc: false, type: Context.TypeManager.DateTime);
            }

            if (Equals(expression.Member, DateTimeUtcNow))
            {
                return new CurrentDateAndTimeExpression(isUtc: true, type: Context.TypeManager.DateTime);
            }

            if (Equals(expression.Member, DateTimeToday))
            {
                return new CurrentDateExpression(Context.TypeManager.Date);
            }

            if (Equals(expression.Member, DateTimeMinValue))
            {
                return new DateAndTimeMinValueExpression(Context.TypeManager.DateTime);
            }

            if (Equals(expression.Member, DateTimeMaxValue))
            {
                return new DateAndTimeMaxValueExpression(Context.TypeManager.DateTime);
            }

            throw new NotImplementedException();
        }
    }
}