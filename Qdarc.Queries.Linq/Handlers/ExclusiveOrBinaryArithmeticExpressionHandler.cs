using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ExclusiveOrBinaryArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public ExclusiveOrBinaryArithmeticExpressionHandler()
            : base(ExpressionType.ExclusiveOr)
        {
        }

        protected override bool CanHandle(BinaryExpression expression)
        {
            return expression.Type != typeof(bool);
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
            return new BitwiseExclusiveOrValueExpression(left, right, type, sqlType);
        }
    }
}