using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ParameterExpressionHandler : LinqExpressionHandler<ParameterExpression>
    {
        public ParameterExpressionHandler()
            : base(ExpressionType.Parameter)
        {
        }

        protected override ISqlExpression Handle(ParameterExpression expression)
        {
            return Context.GetParameterMapping(expression);
        }
    }
}