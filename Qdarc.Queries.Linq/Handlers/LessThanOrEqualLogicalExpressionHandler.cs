using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class LessThanOrEqualLogicalExpressionHandler : LogicalExpressionHandler
    {
        public LessThanOrEqualLogicalExpressionHandler()
            : base(ExpressionType.LessThanOrEqual)
        {
        }

        protected override ISqlExpression Create(ISqlValueExpression leftValue, ISqlValueExpression rightValue)
        {
            return new LessThanOrEqualLogicalExpression(leftValue, rightValue);
        }
    }
}