﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Linq.Handlers
{
    internal abstract class LogicalConjunctionExpressionHandler : LinqExpressionHandler<BinaryExpression>
    {
        protected LogicalConjunctionExpressionHandler(
            ExpressionType type)
            : base(type)
        {
        }

        protected abstract ISqlExpression Create(ISqlLogicalExpression left, ISqlLogicalExpression right);

        protected override ISqlExpression Handle(BinaryExpression expression)
        {
            var left = Context.VisitExpression(expression.Left);
            var right = Context.VisitExpression(expression.Right);

            ISqlLogicalExpression leftLogical;

            if (left is LinqColumnReference leftColumn)
            {
                leftLogical = Context.WrapInLogicalExpression(leftColumn);
            }
            else
            {
                leftLogical = (ISqlLogicalExpression)left;
            }

            ISqlLogicalExpression rightLogical;
            if (right is LinqColumnReference rightColumn)
            {
                rightLogical = Context.WrapInLogicalExpression(rightColumn);
            }
            else
            {
                rightLogical = (ISqlLogicalExpression)right;
            }

            var result = Create(
                leftLogical,
                rightLogical);

            return result;
        }
    }
}