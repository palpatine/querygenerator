﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class DatabaseInsertByQueryMethodCallHandler : LinqExpressionHandler<MethodCallExpression>
    {
        private static readonly MethodInfo InsertByValuesMethod = ExpressionExtensions.GetMethod((IDatabase d) => d.Insert((ITableQuery<int>)null))
            .GetGenericMethodDefinition();

        public DatabaseInsertByQueryMethodCallHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.DeclaringType == typeof(IDatabase)
                    && expression.Method.IsGenericMethod
                    && Equals(expression.Method.GetGenericMethodDefinition(), InsertByValuesMethod);
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var innerType = expression.Method.GetGenericArguments()[0];
            var source = (ISource)Context.VisitExpression(expression.Arguments[0]);

            var linqTableReference = new TableReference(
                Context.ModelToSqlConvention.GetTableName(innerType),
                innerType);

            return new InsertCommand(
                linqTableReference,
                source,
                null,
                innerType);
        }
    }
}
