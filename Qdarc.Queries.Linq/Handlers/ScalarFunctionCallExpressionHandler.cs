﻿using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ScalarFunctionCallExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public ScalarFunctionCallExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            var declaringType = expression.Method.DeclaringType.GetTypeInfo();
            if (declaringType.IsSealed
                && !declaringType.IsGenericType
                && expression.Method.IsDefined(typeof(ScalarFunctionAttribute), false))
            {
                var parameter = expression.Method.GetParameters()[0];
                return parameter.ParameterType == typeof(IDatabase);
            }

            return false;
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var arguments = expression.Arguments.Skip(1).Select(x => (ISqlValueExpression)Context.VisitExpression(x)).ToArray();
            var scalarFunction = expression.Method.GetCustomAttribute<ScalarFunctionAttribute>();
            return new ScalarFunctionCall(
                scalarFunction.Name,
                arguments,
                expression.Method.ReturnType,
                Context.ModelToSqlConvention.GetSqlType(expression.Method));
        }
    }
}