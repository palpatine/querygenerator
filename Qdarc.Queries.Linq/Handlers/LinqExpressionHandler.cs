﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal abstract class LinqExpressionHandler<TExpression> : ILinqExpressionHandler
        where TExpression : Expression
    {
        protected LinqExpressionHandler(ExpressionType handledType)
        {
            HandledType = handledType;
        }

        public ExpressionType HandledType { get; }

        public virtual int Priority { get; } = 0;

        protected IVisitationContext Context { get; private set; }

        public void AttachContext(IVisitationContext context)
        {
            Context = context;
        }

        public bool CanHandle(Expression expression)
        {
            return CanHandle((TExpression)expression);
        }

        public ISqlExpression Handle(Expression expression)
        {
            return Handle((TExpression)expression);
        }

        protected virtual bool CanHandle(TExpression expression)
        {
            return true;
        }

        protected abstract ISqlExpression Handle(TExpression expression);
    }
}