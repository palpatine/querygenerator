using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class MemberInitializationExpressionHandler : LinqExpressionHandler<MemberInitExpression>
    {
        public MemberInitializationExpressionHandler()
            : base(ExpressionType.MemberInit)
        {
        }

        protected override ISqlExpression Handle(MemberInitExpression expression)
        {
            var bindings = new List<BoundSelectionItem>();
            foreach (var memberBinding in expression.Bindings)
            {
                if (memberBinding.BindingType != MemberBindingType.Assignment)
                {
                    throw new NotImplementedException();
                }

                var memberAssignmentBinding = (MemberAssignment)memberBinding;

                var inner = Context.VisitExpression(memberAssignmentBinding.Expression);
                var value = inner as ISelectionItem;

                if (value == null && inner is ISelectCommand selectCommand)
                {
                    value = new SelectCommandAsSelectionItem(selectCommand);
                }
                else if (value == null)
                {
                    throw new NotImplementedException();
                }

                bindings.Add(new BoundSelectionItem(memberAssignmentBinding.Member, value));
            }

            var constructorCall = (ProjectionByConstructor)Context.VisitExpression(expression.NewExpression);
            return new ProjectionByMembersAssignment(constructorCall, bindings);
        }
    }
}