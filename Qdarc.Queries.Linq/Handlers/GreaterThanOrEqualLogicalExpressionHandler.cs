using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class GreaterThanOrEqualLogicalExpressionHandler : LogicalExpressionHandler
    {
        public GreaterThanOrEqualLogicalExpressionHandler()
            : base(ExpressionType.GreaterThanOrEqual)
        {
        }

        protected override ISqlExpression Create(ISqlValueExpression leftValue, ISqlValueExpression rightValue)
        {
            return new GreaterThanOrEqualLogicalExpression(leftValue, rightValue);
        }
    }
}