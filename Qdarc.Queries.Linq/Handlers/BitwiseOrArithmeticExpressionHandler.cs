using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class BitwiseOrArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public BitwiseOrArithmeticExpressionHandler()
            : base(ExpressionType.Or)
        {
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
             return new BitwiseOrValueExpression(left, right, type, sqlType);
        }
    }
}