﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class NewExpressionHandler : LinqExpressionHandler<NewExpression>
    {
        public NewExpressionHandler()
            : base(ExpressionType.New)
        {
        }

        protected override ISqlExpression Handle(NewExpression expression)
        {
            var parameters = expression.Constructor.GetParameters();
            var elements = expression.Arguments
                .Select((argument, id) =>
                {
                    var inner = Context.VisitExpression(argument);
                    if (inner is ISelectionItem selectionItem)
                    {
                        return new AliasedSelectionItem(selectionItem)
                        {
                            Alias = parameters[id].Name,
                        };
                    }

                    if (inner is ISqlSelection selection)
                    {
                        return new PrefixedSelection(parameters[id].Name, selection) as ISelectionItem;
                    }

                    if (inner is ISelectCommand selectCommand)
                    {
                        selectionItem = new SelectCommandAsSelectionItem(selectCommand);

                        return new AliasedSelectionItem(selectionItem)
                        {
                            Alias = parameters[id].Name
                        };
                    }

                    throw new NotImplementedException();
                }).ToList();

            return new ProjectionByConstructor(expression.Constructor, elements);
        }
    }
}