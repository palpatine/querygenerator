using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class MultiplyBinaryArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public MultiplyBinaryArithmeticExpressionHandler()
            : base(ExpressionType.Multiply)
        {
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
            return new MultiplyValueExpression(left, right, type, sqlType);
        }
    }
}