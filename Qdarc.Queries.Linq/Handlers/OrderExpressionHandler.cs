﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class OrderExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public OrderExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.Name == "OrderBy"
                || expression.Method.Name == "OrderByDescending"
                || expression.Method.Name == "ThanBy"
                || expression.Method.Name == "ThanByDescending";
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var source = Context.VisitExpression(expression.Arguments[0]);
            var selectCommand = source as SelectCommand;

            if (selectCommand == null)
            {
                var aliasedSourceItem = new AliasedSourceItem((ISource)source);
                selectCommand = new SelectCommand(
                    selection: Context.GenerateExplicitSelectionList(aliasedSourceItem),
                    source: aliasedSourceItem);
            }

            var quote = (UnaryExpression)expression.Arguments[1];
            var lambda = (LambdaExpression)quote.Operand;
            var parameter = lambda.Parameters.Single();

            Context.AddParameterMapping(parameter, selectCommand);

            var value = Context.VisitExpression(lambda.Body) as ISqlValueExpression;
            if (value == null)
            {
                throw new NotSupportedException("Ordering must be done on values.");
            }

            var @ascending = expression.Method.Name == "OrderBy" || expression.Method.Name == "ThanBy";
            var order = new OrderedValueExpression(
                value,
                @ascending);

            Context.RemoveParameterMapping(parameter);
            if (selectCommand.Order == null || expression.Method.Name.StartsWith("OrderBy", StringComparison.Ordinal))
            {
                selectCommand.Order = new[] { order };
            }
            else
            {
                selectCommand.Order = selectCommand.Order.Concat(new[] { order }).ToArray();
            }

            return selectCommand;
        }
    }
}