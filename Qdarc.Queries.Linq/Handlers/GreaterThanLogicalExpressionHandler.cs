﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class GreaterThanLogicalExpressionHandler : LogicalExpressionHandler
    {
        public GreaterThanLogicalExpressionHandler()
            : base(ExpressionType.GreaterThan)
        {
        }

        protected override ISqlExpression Create(ISqlValueExpression leftValue, ISqlValueExpression rightValue)
        {
            return new GreaterThanLogicalExpression(leftValue, rightValue);
        }
    }
}