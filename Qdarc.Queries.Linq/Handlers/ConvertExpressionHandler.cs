﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ConvertExpressionHandler : LinqExpressionHandler<UnaryExpression>
    {
        public ConvertExpressionHandler()
            : base(ExpressionType.Convert)
        {
        }

        protected override ISqlExpression Handle(UnaryExpression expression)
        {
            var result = (ISqlValueExpression)Context.VisitExpression(expression.Operand);
            return new SqlCastExpression(result, expression.Type, Context.TypeManager.GetSqlType(expression.Type));
        }
    }
}