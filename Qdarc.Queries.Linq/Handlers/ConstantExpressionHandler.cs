using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ConstantExpressionHandler : LinqExpressionHandler<ConstantExpression>
    {
        public ConstantExpressionHandler()
            : base(ExpressionType.Constant)
        {
        }

        protected override ISqlExpression Handle(ConstantExpression expression)
        {
            if (expression.Value == null)
            {
                return new SqlNullExpression(expression.Type, Context.TypeManager.TryGetSqlType(expression.Type));
            }

            if (expression.Type.IsConstructedGenericType)
            {
                var genericTypeDefinition = expression.Type.GetGenericTypeDefinition();
                if (genericTypeDefinition == typeof(ITableQuery<>)
                    || genericTypeDefinition == typeof(TableQuery<>))
                {
                    var value = (ITableQuery)expression.Value;
                    if (value.Expression.NodeType == ExpressionType.Constant)
                    {
                        var innerConstant = (ConstantExpression)value.Expression;
                        if (innerConstant.Value == value)
                        {
                            return new TableReference(
                                 Context.ModelToSqlConvention.GetTableName(expression.Type.GenericTypeArguments[0]),
                                 expression.Type.GenericTypeArguments[0]);
                        }
                    }

                    return Context.VisitExpression(value.Expression);
                }

                if (genericTypeDefinition == typeof(IFunctionQuery<>)
                    || genericTypeDefinition == typeof(FunctionQuery<>))
                {
                    var functionQuery = (IFunctionQuery)expression.Value;
                    return new TabularFunctionCall(
                        functionQuery.FunctionName,
                        functionQuery.Arguments.Select(x => (ISqlValueExpression)Context.VisitExpression(x)).ToArray(),
                        expression.Type.GenericTypeArguments[0]);
                }

                if (genericTypeDefinition == typeof(ITableParameter<>)
                    || genericTypeDefinition == typeof(TableParameter<>))
                {
                    var parameter = (ITableParameter)expression.Value;
                    var parameterType = expression.Type.GenericTypeArguments[0];
                    var userDefinedTableType = Context.ModelToSqlConvention.GetUserDefinedTableType(parameterType);

                    return new LinqTableParameterReference(
                        parameter.Values,
                        parameterType,
                        userDefinedTableType);
                }
            }

            return new SqlConstantExpression(
                expression.Value,
                expression.Type,
                Context.TypeManager.GetSqlType(expression));
        }
    }
}