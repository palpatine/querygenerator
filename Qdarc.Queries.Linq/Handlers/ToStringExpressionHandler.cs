using System;
using System.Globalization;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ToStringExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public ToStringExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.Name == "ToString";
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            IFormatProvider formatProvider = CultureInfo.CurrentCulture;
            var parameters = expression.Method.GetParameters();
            var inner = (ISqlValueExpression)Context.VisitExpression(expression.Object);
            var format = inner.ClrType == typeof(DateTime) ? "d" : "G";
            if ((parameters.Length == 1) && (parameters[0].ParameterType == typeof(IFormatProvider)))
            {
                formatProvider = GetFormatProvider(expression.Arguments[0]);
            }

            if ((parameters.Length == 1) && (parameters[0].ParameterType == typeof(string)))
            {
                format = GetFormat(expression.Arguments[0]);
            }

            if (parameters.Length == 2)
            {
                format = GetFormat(expression.Arguments[0]);
                formatProvider = GetFormatProvider(expression.Arguments[1]);
            }

            return new SqlFormatExpression(
                inner,
                format,
                formatProvider,
                typeof(string),
                Context.TypeManager.GetNVarChar(0));
        }

        private static string GetFormat(Expression parameter)
        {
            var lambda = Expression.Lambda<Func<string>>(parameter);
            var retriever = lambda.Compile();
            return retriever();
        }

        private static IFormatProvider GetFormatProvider(Expression parameter)
        {
            var lambda = Expression.Lambda<Func<IFormatProvider>>(parameter);
            var retriever = lambda.Compile();
            return retriever();
        }
    }
}