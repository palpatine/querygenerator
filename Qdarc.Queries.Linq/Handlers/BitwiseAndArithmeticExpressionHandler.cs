using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class BitwiseAndArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public BitwiseAndArithmeticExpressionHandler()
            : base(ExpressionType.And)
        {
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
            return new BitwiseAndValueExpression(left, right, type, sqlType);
        }
    }
}