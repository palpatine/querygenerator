﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class SingleExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public SingleExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.Name == "Single"
                || expression.Method.Name == "SingleOrDefault";
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var inner = Context.VisitExpression(expression.Arguments[0]);

            var value = new SqlConstantExpression(
                2,
                typeof(int),
                Context.TypeManager.GetSqlType(Expression.Constant(2)));
            var topExpression = new TopExpression(value, false, false);

            if (inner is SelectCommand selectCommand)
            {
                selectCommand.Top = topExpression;
                return selectCommand;
            }

            var selectionSource = (ISource)inner;

            return new SelectCommand(
                selection: Context.GenerateExplicitSelectionList(selectionSource),
                source: selectionSource,
                top: topExpression);
        }
    }
}