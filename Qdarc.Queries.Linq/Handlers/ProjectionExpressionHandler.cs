﻿using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ProjectionExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public ProjectionExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            if (expression.Method.Name == "Select")
            {
                var parameter = expression.Method.GetParameters()[0];
                return parameter.ParameterType.IsConstructedGenericType
                 && parameter.ParameterType.GetGenericTypeDefinition() == typeof(ITableQuery<>);
            }

            return false;
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var select = Context.VisitExpression(expression.Arguments[0]);

            var selectCommand = select as SelectCommand;

            if (selectCommand == null)
            {
                var aliasedSoruce = new AliasedSourceItem((ISource)select);
                selectCommand = new SelectCommand(
                    selection: Context.GenerateExplicitSelectionList(aliasedSoruce),
                    source: aliasedSoruce);
            }

            var quote = (UnaryExpression)expression.Arguments[1];
            var lambda = (LambdaExpression)quote.Operand;
            var parameter = lambda.Parameters.Single();
            Context.AddParameterMapping(parameter, selectCommand);
            var projection = Context.VisitExpression(lambda.Body);
            Context.RemoveParameterMapping(parameter);
            var valueExpression = projection as ISqlValueExpression;
            if (valueExpression != null)
            {
                selectCommand.Selection = new SqlSelection(new[] { valueExpression }, valueExpression.ClrType);
            }
            else
            {
                var command = projection as ISelectCommand;
                if (command != null)
                {
                    var selectCommandAsSelectionItem = new SelectCommandAsSelectionItem(command);
                    selectCommand.Selection = new SqlSelection(
                        new[] { selectCommandAsSelectionItem },
                        selectCommandAsSelectionItem.ClrType);
                }
                else
                {
                    selectCommand.Selection = (ISqlSelection)projection;
                }
            }

            return selectCommand;
        }
    }
}