﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal sealed class NotExpressionHandler : LinqExpressionHandler<UnaryExpression>
    {
        public NotExpressionHandler()
            : base(ExpressionType.Not)
        {
        }

        protected override ISqlExpression Handle(UnaryExpression expression)
        {
            var operandExpression = Context.VisitExpression(expression.Operand);
            if (operandExpression is ISqlLogicalExpression logicalExpression)
            {
                return new NegateLogicalExpression(logicalExpression);
            }
            else if (operandExpression is ISqlValueExpression valueExpression)
            {
                return new NegateValueExpression(valueExpression, valueExpression.ClrType, valueExpression.SqlType);
            }

            throw new NotImplementedException(operandExpression.GetType().FullName);
        }
    }
}
