using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal abstract class BinaryArithmeticExpressionHandler : LinqExpressionHandler<BinaryExpression>
    {
        protected BinaryArithmeticExpressionHandler(ExpressionType type)
            : base(type)
        {
        }

        protected override ISqlExpression Handle(BinaryExpression expression)
        {
            var left = (ISqlValueExpression)Context.VisitExpression(expression.Left);
            var right = (ISqlValueExpression)Context.VisitExpression(expression.Right);
            var type = Context.TypeManager.GetSqlType(left.SqlType, right.SqlType);
            var sqlExpression = CreateExpression(left, right, expression.Type, type);
            return sqlExpression;
        }

        protected abstract SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType);
    }
}