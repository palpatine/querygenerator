﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class TopExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public TopExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.Name == "Top";
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var query = Context.VisitExpression(expression.Arguments[0]);

            var selectCommand = query as SelectCommand;
            if (selectCommand == null)
            {
                var selectionSource = (ISource)query;
                selectCommand = new SelectCommand(
                    selection: Context.GenerateExplicitSelectionList(selectionSource),
                    source: selectionSource);
            }

            var value = (ISqlValueExpression)Context.VisitExpression(expression.Arguments[1]);
            selectCommand.Top = new TopExpression(value, false, false);

            return selectCommand;
        }
    }
}