using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class NotEqualLogicalExpressionHandler : LogicalExpressionHandler
    {
        public NotEqualLogicalExpressionHandler()
            : base(ExpressionType.NotEqual)
        {
        }

        protected override ISqlExpression Create(ISqlValueExpression leftValue, ISqlValueExpression rightValue)
        {
            return new NotEqualLogicalExpression(leftValue, rightValue);
        }
    }
}