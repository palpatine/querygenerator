﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.ExpressionReplacer;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class UpdateStatementHandler : LinqExpressionHandler<MethodCallExpression>
    {
        private readonly IExpressionReplacer<INamedSource> _sourceReplacer;

        public UpdateStatementHandler(IExpressionReplacer<INamedSource> sourceReplacer)
            : base(ExpressionType.Call)
        {
            _sourceReplacer = sourceReplacer;
        }

        protected override bool CanHandle(MethodCallExpression expression)
            => expression.Method.DeclaringType == typeof(TableQueryUpdate);

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var query = (ISource)Context.VisitExpression(expression.Arguments[0]);

            ISqlLogicalExpression filter = null;
            if (query is SelectCommand selectCommand)
            {
                filter = selectCommand.Filter;
                if (selectCommand.Source is AliasedSourceItem aliasedSourceItem)
                {
                    query = aliasedSourceItem.Source;

                    if (filter != null)
                    {
                        filter = _sourceReplacer.Replace(
                                                    filter,
                                                    aliasedSourceItem,
                                                    (INamedSource)aliasedSourceItem.Source);
                    }
                }
                else
                {
                    query = selectCommand.Source;
                }
            }

            Expression setExpression;
            ISource source;
            ISource target;

            if (expression.Arguments.Count == 2)
            {
                setExpression = expression.Arguments[1];
                source = null;
                target = query;
            }
            else
            {
                setExpression = expression.Arguments[2];
                source = query;
                target = (ISource)ProcessLabda(expression.Arguments[1], source);
            }

            var set = ProcessSet(setExpression, query);

            return new UpdateCommand(target, set, source, null, filter);
        }

        private IEnumerable<ElementNameValueAssociation> ProcessSet(Expression setExpression, ISource source)
        {
            var set = (ProjectionByMembersAssignment)ProcessLabda(setExpression, source);
            return set.Bindings.Select(x => new ElementNameValueAssociation(
                 new ElementName(x.Alias),
                 (ISqlValueExpression)x.Item))
                  .ToArray();
        }

        private ISqlExpression ProcessLabda(Expression setExpression, ISource source)
        {
            var quote = (UnaryExpression)setExpression;
            var lambda = (LambdaExpression)quote.Operand;
            IEnumerable<ISource> sources;
            if (source is IMultisource multisource)
            {
                sources = multisource.Sources;
            }
            else
            {
                sources = new[]
                 {
                    source
                };
            }

            using (var enumerator = sources.GetEnumerator())
            {
                foreach (var parameter in lambda.Parameters)
                {
                    enumerator.MoveNext();
                    Context.AddParameterMapping(parameter, enumerator.Current);
                }
            }

            var expression = Context.VisitExpression(lambda.Body);

            foreach (var parameter in lambda.Parameters)
            {
                Context.RemoveParameterMapping(parameter);
            }

            return expression;
        }
    }
}