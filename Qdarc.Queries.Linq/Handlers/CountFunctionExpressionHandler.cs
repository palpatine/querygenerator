﻿using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class CountFunctionExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public CountFunctionExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            if (expression.Method.DeclaringType == typeof(SqlFunctions))
            {
                return expression.Method.Name == "Count"
                    || expression.Method.Name == "CountDistinct";
            }

            return false;
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            if (expression.Arguments.Count == 2)
            {
                var isDistinct = expression.Method.Name.Contains("Distinct");
                var countByExpression = Context.VisitExpression(expression.Arguments[1]);
                var innerExpression = countByExpression as ISqlValueExpression;

                if (innerExpression == null)
                {
                    var source = countByExpression as ISource;
                    if (source != null)
                    {
                        throw new InvalidModelException(
                            "Count expression cannot point to source item. Invalid expression '.Count(x)' was used rather than one pointing to column '.Count(x.Column)'.",
                            source);
                    }

                    throw new NotSupportedException();
                }

                var sqlCountExpression =
                    new SqlCountExpression(
                        innerExpression,
                        isDistinct,
                        typeof(int),
                        Context.TypeManager.Int);

                return sqlCountExpression;
            }

            var count = new SqlAsteriskCountExpression(
                typeof(int),
                Context.TypeManager.Int);

            return count;
        }
    }
}