using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class SubtractBinaryArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public SubtractBinaryArithmeticExpressionHandler()
            : base(ExpressionType.Subtract)
        {
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
            return new SubtractValueExpression(left, right, type, sqlType);
        }
    }
}