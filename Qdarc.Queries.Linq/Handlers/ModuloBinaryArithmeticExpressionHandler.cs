using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ModuloBinaryArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public ModuloBinaryArithmeticExpressionHandler()
            : base(ExpressionType.Modulo)
        {
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
            return new ModuloValueExpression(left, right, type, sqlType);
        }
    }
}