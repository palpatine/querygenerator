﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal abstract class LogicalExpressionHandler : LinqExpressionHandler<BinaryExpression>
    {
        protected LogicalExpressionHandler(ExpressionType type)
            : base(type)
        {
        }

        protected override ISqlExpression Handle(BinaryExpression expression)
        {
            var left = Context.VisitExpression(expression.Left);
            var leftValue = GetValueExpression(left);
            var right = Context.VisitExpression(expression.Right);
            var rightValue = GetValueExpression(right);

            var leftParameter = leftValue as LinqParameterReference;
            var rightParameter = rightValue as LinqParameterReference;
            var promotedType = GetPromotedType(leftValue, rightValue);

            if (leftParameter != null)
            {
                leftParameter.SqlType = promotedType;
            }

            if (rightParameter != null)
            {
                rightParameter.SqlType = promotedType;
            }

            var result = Create(leftValue, rightValue);
            return result;
        }

        protected abstract ISqlExpression Create(ISqlValueExpression leftValue, ISqlValueExpression rightValue);

        private static ISqlValueExpression GetValueExpression(ISqlExpression left)
        {
            var command = left as ISelectCommand;
            if (command != null)
            {
                var singleSelection = command.Selection.Elements.Single();

                var value = singleSelection as ISqlValueExpression;
                if (value != null)
                {
                    return value;
                }

                if (singleSelection is AliasedSelectionItem aliased)
                {
                    return (ISqlValueExpression)aliased.Item;
                }

                throw new NotImplementedException();
            }

            var directValue = left as ISqlValueExpression;

            if (directValue != null)
            {
                return directValue;
            }

            throw new NotImplementedException();
        }

        private ISqlType GetPromotedType(ISqlValueExpression left, ISqlValueExpression right)
        {
            ISqlType promotedType;
            if (left.SqlType == null || right.SqlType == null)
            {
                promotedType = left.SqlType ?? right.SqlType;
            }
            else
            {
                promotedType = Context.TypeManager.GetSqlType(left.SqlType, right.SqlType);
            }

            return promotedType;
        }
    }
}