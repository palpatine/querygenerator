﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class OrElseLogicalConjunctionExpressionHandler : LogicalConjunctionExpressionHandler
    {
        public OrElseLogicalConjunctionExpressionHandler()
            : base(ExpressionType.OrElse)
        {
        }

        protected override ISqlExpression Create(ISqlLogicalExpression left, ISqlLogicalExpression right)
        {
            return new OrLogicalConjunctionExpression(left, right);
        }
    }
}