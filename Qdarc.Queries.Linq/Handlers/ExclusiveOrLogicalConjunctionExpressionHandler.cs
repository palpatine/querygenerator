﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ExclusiveOrLogicalConjunctionExpressionHandler : LogicalConjunctionExpressionHandler
    {
        public ExclusiveOrLogicalConjunctionExpressionHandler()
            : base(ExpressionType.ExclusiveOr)
        {
        }

        protected override bool CanHandle(BinaryExpression expression)
        {
            return expression.Type == typeof(bool);
        }

        protected override ISqlExpression Create(ISqlLogicalExpression left, ISqlLogicalExpression right)
        {
            return new ExclusiveOrLogicalConjunctionExpression(left, right);
        }
    }
}