﻿using System.Linq.Expressions;
using Qdarc.Queries.Formatter;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class AnyExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public AnyExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression) => expression.Method.Name == "Any";

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var query = Context.VisitExpression(expression.Arguments[0]);
            ISelectCommand selectCommand;

            if (expression.Arguments.Count == 2)
            {
                var preparedSelect = PrepareCommandForFiltering(query);
                var logicalExpression = Context.ProcessSingleParameterFilter(
                    (UnaryExpression)expression.Arguments[1],
                    preparedSelect);
                preparedSelect.MergeFilter(logicalExpression);
                selectCommand = preparedSelect;
            }
            else
            {
                selectCommand = query as ISelectCommand;
                if (selectCommand == null)
                {
                    selectCommand = Context.EnsureSelection(query);
                }
            }

            selectCommand.Selection = new SqlSelection(
                new[]
                {
                    new SqlConstantExpression(
                        1,
                        typeof(int),
                        Context.TypeManager.Int)
                },
                typeof(int));
            var sqlBooleanType = Context.TypeManager.Bit;

            var condition = new ExistsExpression(selectCommand, false);
            var ifTrue = new SqlConstantExpression(1, typeof(int), Context.TypeManager.Int);
            var ifFalse = new SqlConstantExpression(0, typeof(int), Context.TypeManager.Int);
            var inlineIf = new SqlInlineIfExpression(condition, ifTrue, ifFalse, ifTrue.ClrType, ifFalse.SqlType);
            var selectionList = new[]
            {
                new SqlCastExpression(
                    inlineIf,
                    typeof(bool),
                    sqlBooleanType)
            };
            var resultSelect = new SelectCommand(
                selection: new SqlSelection(selectionList, typeof(bool)));

            return resultSelect;
        }

        private SelectCommand PrepareCommandForFiltering(ISqlExpression sqlExpression)
        {
            if (sqlExpression is SelectCommand selectCommand)
            {
                if (Context.CanApplyFilterDirectly(selectCommand))
                {
                    return selectCommand;
                }

                selectCommand = Context.WrapInSelectCommand(selectCommand);
            }
            else
            {
                selectCommand = Context.WrapInSelectCommand(sqlExpression);
            }

            return selectCommand;
        }
    }
}