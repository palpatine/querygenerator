using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class AddBinaryArithmeticExpressionHandler : BinaryArithmeticExpressionHandler
    {
        public AddBinaryArithmeticExpressionHandler()
            : base(ExpressionType.Add)
        {
        }

        protected override SqlBinaryValueExpression CreateExpression(ISqlValueExpression left, ISqlValueExpression right, Type type, ISqlType sqlType)
        {
            return new AddValueExpression(left, right, type, sqlType);
        }
    }
}