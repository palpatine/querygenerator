﻿using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class AndAlsoLogicalConjunctionExpressionHandler : LogicalConjunctionExpressionHandler
    {
        public AndAlsoLogicalConjunctionExpressionHandler()
            : base(ExpressionType.AndAlso)
        {
        }

        protected override ISqlExpression Create(ISqlLogicalExpression left, ISqlLogicalExpression right)
        {
            return new AndLogicalConjunctionExpression(left, right);
        }
    }
}