﻿using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Handlers
{
    internal sealed class QueryCallExpressionHanlder : LinqExpressionHandler<MethodCallExpression>
    {
        private static readonly MethodInfo Method
            = ExpressionExtensions.GetMethod((IDatabase database) => database.Query<int>())
            .GetGenericMethodDefinition();

        public QueryCallExpressionHanlder()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.IsGenericMethod
                && Equals(expression.Method.GetGenericMethodDefinition(), Method);
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            return new TableReference(
                Context.ModelToSqlConvention.GetTableName(expression.Type.GenericTypeArguments[0]),
                expression.Type.GenericTypeArguments[0]);
        }
    }
}
