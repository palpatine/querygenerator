using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Linq.SqlTypeFinders;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class InstanceMemberExpressionHandler : LinqExpressionHandler<MemberExpression>
    {
        private readonly ISqlTypeFinder _sqlTypeFinder;

        public InstanceMemberExpressionHandler(ISqlTypeFinder sqlTypeFinder)
            : base(ExpressionType.MemberAccess) => _sqlTypeFinder = sqlTypeFinder;

        protected override bool CanHandle(MemberExpression expression) => expression.Expression != null;

        protected override ISqlExpression Handle(MemberExpression expression)
        {
            var field = expression.Member as FieldInfo;
            if (field != null)
            {
                return HandleFiled(expression, field);
            }

            var inner = Context.VisitExpression(expression.Expression);

            if (inner is IParameter)
            {
                return HandleLinqParameterReference(expression);
            }

            if (inner is ISelectCommand selectCommand)
            {
                return HandleSqlSelection(expression, selectCommand.Selection);
            }

            if (inner is ISqlSelection selection)
            {
                return HandleSqlSelection(expression, selection);
            }

            if (inner is INamedSource namedSource)
            {
                return HandleNamedSelectionSource(expression, namedSource);
            }

            if (inner is LinqColumnChainReference columnChainReference)
            {
                if (expression.Member is PropertyInfo property)
                {
                    ISqlType foundType = null;
                    if (property.PropertyType == typeof(string)
                        && !property.PropertyType.GetTypeInfo().IsClass)
                    {
                        foundType = _sqlTypeFinder.TryFindType(columnChainReference.Source, property)
                                    ?? throw new NotImplementedException();
                    }

                    return new LinqColumnChainReference(
                        columnChainReference.Source,
                        columnChainReference.Properties.Concat(
                            new[]
                            {
                                property
                            }),
                        property.PropertyType,
                        foundType);
                }

                throw new NotImplementedException();
            }

            throw new NotImplementedException();
        }

        private static ISqlExpression HandleSqlSelection(MemberExpression expression, ISqlSelection selection)
        {
            foreach (var item in selection.Elements)
            {
                if (item is PrefixedSelection prefixedSelection)
                {
                    if (prefixedSelection.Prefix == expression.Member.Name)
                    {
                        return prefixedSelection.Selection;
                    }
                }

                if (item is AliasedSelectionItem aliasedSelectionItem)
                {
                    if (aliasedSelectionItem.Alias == expression.Member.Name)
                    {
                        return aliasedSelectionItem.Item;
                    }
                }
            }

            throw new NotImplementedException();
        }

        private ISqlExpression HandleLinqParameterReference(MemberExpression expression)
        {
            var result = new LinqParameterReference(
                expression,
                expression.Type,
                Context.TypeManager.GetSqlType(expression.Type));

            return result;
        }

        private ISqlExpression HandleNamedSelectionSource(MemberExpression expression, INamedSource namedSource)
        {
            if (expression.Member is PropertyInfo property)
            {
                if (property.PropertyType != typeof(string)
                    && property.PropertyType.GetTypeInfo().IsClass)
                {
                    return new LinqColumnChainReference(
                        namedSource,
                        new[]
                        {
                            property
                        },
                        property.PropertyType,
                        Context.ModelToSqlConvention.TryGetSqlType(property.DeclaringType, property));
                }

                var foundType = _sqlTypeFinder.TryFindType(namedSource, property);
                if (foundType == null)
                {
                    throw new NotImplementedException();
                }

                var result = new LinqColumnReference(namedSource, property, property.PropertyType, foundType);
                return result;
            }

            throw new NotImplementedException();
        }

        private ISqlExpression HandleFiled(MemberExpression expression, FieldInfo field)
        {
            var result = new LinqParameterReference(
                expression,
                field.FieldType,
                Context.TypeManager.GetSqlType(field.FieldType));

            return result;
        }
    }
}