﻿using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal static class Helper
    {
        public static void MergeFilter(this SelectCommand selectCommand, ISqlLogicalExpression filter)
        {
            if (selectCommand.Filter == null)
            {
                selectCommand.Filter = filter;
            }
            else
            {
                selectCommand.Filter = new AndLogicalConjunctionExpression(
                    selectCommand.Filter,
                    filter);
            }
        }
    }
}