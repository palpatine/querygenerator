using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class EqualLogicalExpressionHandler : LogicalExpressionHandler
    {
        public EqualLogicalExpressionHandler()
            : base(ExpressionType.Equal)
        {
        }

        protected override ISqlExpression Create(ISqlValueExpression leftValue, ISqlValueExpression rightValue)
        {
            return new EqualLogicalExpression(leftValue, rightValue);
        }
    }
}