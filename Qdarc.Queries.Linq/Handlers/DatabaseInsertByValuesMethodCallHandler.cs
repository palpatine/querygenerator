﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class DatabaseInsertByValuesMethodCallHandler : LinqExpressionHandler<MethodCallExpression>
    {
        private static readonly MethodInfo InsertByValuesMethod = ExpressionExtensions.GetMethod((IDatabase d) => d.Insert((IValuesQuery<int>)null))
            .GetGenericMethodDefinition();

        public DatabaseInsertByValuesMethodCallHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.DeclaringType == typeof(IDatabase)
                    && expression.Method.IsGenericMethod
                    && Equals(expression.Method.GetGenericMethodDefinition(), InsertByValuesMethod);
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var innerType = expression.Method.GetGenericArguments()[0];

            var linqTableReference = new TableReference(
                Context.ModelToSqlConvention.GetTableName(innerType),
                innerType);
            var values = (IValuesQuery)((ConstantExpression)expression.Arguments.Single()).Value;

            return new InsertCommand(
                linqTableReference,
                CreateValuesSource(innerType, values),
                null,
                innerType);
        }

        private ValuesSource CreateValuesSource(Type innerType, IValuesQuery query)
        {
            IEnumerable<ValueSourceRow> rows;
            if (innerType != typeof(string) && innerType.GetTypeInfo().IsClass)
            {
                var columns = Context.ModelToSqlConvention.GetInsertableColumns(innerType).ToArray();
                rows = CreateComplexValuesList(query, columns.Select(x => x.Property).ToArray());
            }
            else
            {
                throw new NotSupportedException();
            }

            return new ValuesSource(rows, innerType);
        }

        private IEnumerable<ValueSourceRow> CreateComplexValuesList(IValuesQuery query, PropertyInfo[] runtimeProperties)
        {
            return ((IEnumerable)query.Values.Value).Cast<object>()
                .Select(x => new ValueSourceRow(runtimeProperties.Select(z =>
                    {
                        var value = z.GetValue(x);
                        return new SqlConstantExpression(
                            value,
                            z.PropertyType,
                            Context.TypeManager.GetSqlType(z.PropertyType, value));
                    }).ToArray())).ToArray();
        }
    }
}
