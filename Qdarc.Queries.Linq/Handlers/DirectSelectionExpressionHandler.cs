﻿using System.Linq.Expressions;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class DirectSelectionExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public DirectSelectionExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            if (expression.Method.DeclaringType == typeof(TableQuery)
                && expression.Method.Name == "Select")
            {
                var parameter = expression.Method.GetParameters()[0];
                return parameter.ParameterType == typeof(ITableQueryProvider);
            }

            return false;
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var quote = (UnaryExpression)expression.Arguments[1];
            var lambda = (LambdaExpression)quote.Operand;
            var selection = (ISqlValueExpression)Context.VisitExpression(lambda.Body);
            return new SelectCommand(
                selection: new SqlSelection(
                    new[]
                    {
                        selection
                    },
                    selection.ClrType));
        }
    }
}