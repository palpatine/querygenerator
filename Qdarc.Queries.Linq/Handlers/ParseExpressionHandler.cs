using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Handlers
{
    internal class ParseExpressionHandler : LinqExpressionHandler<MethodCallExpression>
    {
        public ParseExpressionHandler()
            : base(ExpressionType.Call)
        {
        }

        protected override bool CanHandle(MethodCallExpression expression)
        {
            return expression.Method.Name == "Parse";
        }

        protected override ISqlExpression Handle(MethodCallExpression expression)
        {
            var innerExpression = Context.VisitExpression(expression.Arguments[0]);
            return new SqlConvertExpression(
                (ISqlValueExpression)innerExpression,
                expression.Method.DeclaringType,
                Context.TypeManager.GetSqlType(expression.Method.DeclaringType));
        }
    }
}