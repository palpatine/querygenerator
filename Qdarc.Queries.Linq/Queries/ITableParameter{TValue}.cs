namespace Qdarc.Queries.Linq.Queries
{
    public interface ITableParameter<TValue> : ITableQuery<TValue>, ITableParameter
    {
    }
}