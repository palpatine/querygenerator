using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Qdarc.Queries.Linq.Queries
{
    public sealed class TableParameter<TValue> : ITableParameter<TValue>, IEnumerable<TValue>
    {
        public TableParameter(
            ITableQueryProvider provider,
            ConstantExpression expression)
        {
            Expression = Expression.Constant(this);
            Provider = provider;
            Values = expression;
        }

        public Type ElementType => typeof(TValue);

        public Expression Expression { get; }

        public ITableQueryProvider Provider { get; }

        public ConstantExpression Values { get; }

        public IEnumerator<TValue> GetEnumerator()
        {
            return ((IEnumerable<TValue>)Values.Value).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}