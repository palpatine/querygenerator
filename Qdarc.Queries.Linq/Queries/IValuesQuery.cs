﻿using System.Linq.Expressions;

namespace Qdarc.Queries.Linq.Queries
{
    public interface IValuesQuery
    {
        ConstantExpression Values { get; }
    }
}
