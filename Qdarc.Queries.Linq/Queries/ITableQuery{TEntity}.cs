﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Qdarc.Queries.Linq.Queries
{
    public interface ITableQuery<out TEntity> : ITableQuery
    {
    }
}