﻿using System.Collections.Generic;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Queries
{
    public interface IParametersBinder
    {
        void BindParameters(IEnumerable<IParameter> parameters, ISqlCommand sqlCommand, bool trace);
    }
}