using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Queries
{
    public static class OrderedTableQuery
    {
        public static List<TSource> Page<TSource>(
           this IOrderedTableQuery<TSource> query, int skip, int limit)
        {
            var pagedQuery = query.PageInner(skip, limit);

            return query.Provider.Execute<List<TSource>>(pagedQuery.Expression);
        }

        public static IOrderedTableQuery<TSource> ThanBy<TSource, TOrdered>(
            this IOrderedTableQuery<TSource> query,
            Expression<Func<TSource, TOrdered>> property)
        {
            var method = ExpressionExtensions.GetMethod(() => ThanBy<TSource, TOrdered>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateOrderedQuery<TSource>(expression);
        }

        public static IOrderedTableQuery<TSource> ThanByDescending<TSource, TOrdered>(
            this IOrderedTableQuery<TSource> query,
            Expression<Func<TSource, TOrdered>> property)
        {
            var method = ExpressionExtensions.GetMethod(() => ThanByDescending<TSource, TOrdered>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateOrderedQuery<TSource>(expression);
        }

        internal static IOrderedTableQuery<TSource> PageInner<TSource>(
          this IOrderedTableQuery<TSource> query, int skip, int limit)
        {
            var method = ExpressionExtensions.GetMethod(() => PageInner<TSource>(null, 0, 0));
            var arguments = new[]
            {
                query.Expression,
                Expression.Constant(skip),
                Expression.Constant(limit)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateOrderedQuery<TSource>(expression);
        }
    }
}