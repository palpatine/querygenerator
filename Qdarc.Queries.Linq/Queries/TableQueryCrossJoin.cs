using System;
using System.Linq.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Queries
{
    public static class TableQueryCrossJoin
    {
        public static ITableQuery<Tuple<TFirst, TSecond>> CrossJoin<TFirst, TSecond>(
           this ITableQuery<TFirst> query,
           ITableQuery<TSecond> other)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond>(null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond>>(expression);
        }

        public static ITableQuery<TProjection> CrossJoin<TFirst, TSecond, TProjection>(
            this ITableQuery<TFirst> query,
            ITableQuery<TSecond> other,
            Expression<Func<TFirst, TSecond, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TProjection>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird>> CrossJoin<TFirst, TSecond, TThird>(
            this ITableQuery<Tuple<TFirst, TSecond>> query,
            ITableQuery<TThird> other)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird>(null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird>>(expression);
        }

        public static ITableQuery<TProjection> CrossJoin<TFirst, TSecond, TThird, TProjection>(
            this ITableQuery<Tuple<TFirst, TSecond>> query,
            ITableQuery<TThird> other,
            Expression<Func<TFirst, TSecond, TThird, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird, TProjection>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth>> CrossJoin<TFirst, TSecond, TThird, TFourth>(
            this ITableQuery<Tuple<TFirst, TSecond, TThird>> query,
            ITableQuery<TFourth> other)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird, TFourth>(null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird, TFourth>>(expression);
        }

        public static ITableQuery<TProjection> CrossJoin<TFirst, TSecond, TThird, TFourth, TProjection>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird>> query,
           ITableQuery<TFourth> other,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird, TFourth, TProjection>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth>> query,
           ITableQuery<TFifth> other)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth>(null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>>(expression);
        }

        public static ITableQuery<TProjection> CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth, TProjection>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth>> query,
           ITableQuery<TFifth> other,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth, TProjection>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>> CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(
          this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> query,
          ITableQuery<TSixth> other)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>>(expression);
        }

        public static ITableQuery<TProjection> CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TProjection>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> query,
           ITableQuery<TSixth> other,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => CrossJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TProjection>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }
    }
}