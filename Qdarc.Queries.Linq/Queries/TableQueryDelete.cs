﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Queries
{
    public static class TableQueryDelete
    {
        public static int Delete<TFirst>(
            this ITableQuery<TFirst> query)
        {
            var method = ExpressionExtensions.GetMethod(() => Delete<TFirst>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.ExecuteNonQuery(expression);
        }

        public static int Delete<TFirst, TSecond, TTarget>(
            this ITableQuery<Tuple<TFirst, TSecond>> query,
            Expression<Func<TFirst, TSecond, TTarget>> targetSelector)
        {
            var method = ExpressionExtensions.GetMethod(() => Delete<TFirst, TSecond, TTarget>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(targetSelector),
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.ExecuteNonQuery(expression);
        }
    }
}
