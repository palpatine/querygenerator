﻿using System;
using System.Linq.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Queries
{
    public static class CountExtensions
    {
        public static int Count<TSource>(
           this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => Count<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<int>(expression);
        }

        public static int Count<TSource, TResult>(
           this ITableQuery<TSource> query,
           Expression<Func<TSource, TResult>> innerExpression)
        {
            var method = ExpressionExtensions.GetMethod(() => Count<TSource, TResult>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(innerExpression)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<int>(expression);
        }

        public static int CountDistinct<TSource, TResult>(
           this ITableQuery<TSource> query,
           Expression<Func<TSource, TResult>> innerExpression)
        {
            var method = ExpressionExtensions.GetMethod(() => CountDistinct<TSource, TResult>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(innerExpression)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<int>(expression);
        }
    }
}