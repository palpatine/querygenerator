using System.Linq.Expressions;

namespace Qdarc.Queries.Linq.Queries
{
    public interface ITableParameter
    {
        ConstantExpression Values { get; }
    }
}