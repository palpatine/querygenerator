using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Qdarc.Queries.Formatter;
using Qdarc.Queries.Linq.Finalizers;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Queries
{
    public class TableQueryProvider : ITableQueryProvider, IQueryProvider
    {
        private readonly IDatabaseConnectionProvider _databaseConnectionProvider;

        private readonly Func<IVisitationContext> _linqQueryVisitationContextFactory;
        private readonly IObjectMapperSelector _mapperSelector;
        private readonly IParametersBinder _parametersBinder;
        private readonly Func<IFormattedQueryTransformer> _queryFormattingContextFactory;
        private readonly IEnumerable<IResultFinalizer> _finalizers;

        public TableQueryProvider(
            IDatabaseConnectionProvider databaseConnectionProvider,
            Func<IVisitationContext> linqQueryVisitationContextFactory,
            Func<IFormattedQueryTransformer> queryFormattingContextFactory,
            IEnumerable<IResultFinalizer> finalizers,
            IObjectMapperSelector mapperSelector,
            IParametersBinder parametersBinder)
        {
            _databaseConnectionProvider = databaseConnectionProvider;
            _linqQueryVisitationContextFactory = linqQueryVisitationContextFactory;
            _queryFormattingContextFactory = queryFormattingContextFactory;
            _finalizers = finalizers.OrderBy(x => x.Priority).ToArray();
            _mapperSelector = mapperSelector;
            _parametersBinder = parametersBinder;
        }

        ////public IGroupingTableQuery<TKey, TSource, TProjection> CreateGroupingQuery<TKey, TSource, TProjection>(
        ////    Expression expression)
        ////{
        ////    return new GroupingTableQuery<TKey, TSource, TProjection>(this, expression);
        ////}

        public IQueryProvider AsQueryProvider()
        {
            return this;
        }

        public IOrderedTableQuery<TElement> CreateOrderedQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        public ITableQuery<TElement> CreateQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            var elementType = TypeSystem.GetElementType<TResult>();

            var result = Execute(elementType, expression);

            if (result == null)
            {
                return default(TResult);
            }

            if (result is TResult)
            {
                return (TResult)result;
            }

            return (TResult)Convert.ChangeType(result, typeof(TResult));
        }

        public object Execute(Expression expression)
        {
            return Execute(TypeSystem.GetElementType(expression.Type), expression);
        }

        public int ExecuteNonQuery(Expression expression)
        {
            var visitor = _linqQueryVisitationContextFactory();
            var part = visitor.VisitExpression(expression);
            return ExecuteNonQuery(part);
        }

        public TResult Execute<TResult>(ISqlExpression expression)
        {
            var elementType = TypeSystem.GetElementType<TResult>();
            using (var connection = _databaseConnectionProvider.Connection)
            {
                var resultList = Read(elementType, expression, connection);
                object result;
                if (TypeSystem.IsCollection(typeof(TResult)))
                {
                    if (connection.Trace)
                    {
                        Debug.WriteLine("retrived: " + resultList.Count);
                    }

                    result = resultList;
                }
                else
                {
                    var list = resultList.Cast<object>();
                    result = list.SingleOrDefault();
                }

                if (result == null)
                {
                    return default(TResult);
                }

                if (result is TResult)
                {
                    return (TResult)result;
                }

                return (TResult)Convert.ChangeType(result, typeof(TResult));
            }
        }

        public int ExecuteNonQuery(ISqlExpression expression)
        {
            using (var connection = _databaseConnectionProvider.Connection)
            {
                var parsedQuery = expression.Accept<FormattedQuery>(_queryFormattingContextFactory());
                if (connection.Trace)
                {
                    Debug.WriteLine("query: " + parsedQuery.Query);
                }

                var sqlCommand = connection.CreateSqlCommand(parsedQuery.Query);
                _parametersBinder.BindParameters(parsedQuery.Parameters, sqlCommand, connection.Trace);
                return sqlCommand.ExecuteNonQuery();
            }
        }

        IQueryable<TElement> IQueryProvider.CreateQuery<TElement>(Expression expression)
        {
            return new TableQuery<TElement>(this, expression);
        }

        IQueryable IQueryProvider.CreateQuery(Expression expression)
        {
            var type = TypeSystem.GetElementType(expression.Type);
            var queryType = typeof(TableQuery<>).MakeGenericType(type);

            var constructor = queryType
                .GetTypeInfo()
                .DeclaredConstructors
                .Single(x => x.GetParameters().Length == 2);

            return (IQueryable)constructor.Invoke(
                new object[] { this, expression });
        }

        public ISqlExpression Visit(Expression expression)
        {
            var visitor = _linqQueryVisitationContextFactory();
            var visitExpression = visitor.VisitExpression(expression);
            return visitExpression;
        }

        private IList Read(Type elementType, ISqlExpression part, ISqlConnection connection)
        {
            var parsedQuery = part.Accept<FormattedQuery>(_queryFormattingContextFactory());
            if (connection.Trace)
            {
                Debug.WriteLine("query: " + parsedQuery.Query);
            }

            var sqlCommand = connection.CreateSqlCommand(parsedQuery.Query);
            var mapper = _mapperSelector.BuildSetMapper(part, parsedQuery);
            _parametersBinder.BindParameters(parsedQuery.Parameters, sqlCommand, connection.Trace);
            IList result;
            using (var reader = sqlCommand.ExecuteReader())
            {
                result = mapper.ReadAll(elementType, reader);
            }

            return result;
        }

        private object Execute(Type elementType, Expression expression)
        {
            var visitor = _linqQueryVisitationContextFactory();
            var part = visitor.FinalizeSelection(visitor.VisitExpression(expression));

            using (var connection = _databaseConnectionProvider.Connection)
            {
                var result = Read(elementType, part, connection);

                if (TypeSystem.IsCollection(expression.Type))
                {
                    if (connection.Trace)
                    {
                        Debug.WriteLine("retrived: " + result.Count);
                    }

                    return result;
                }

                foreach (var finalizer in _finalizers)
                {
                    object finalizedResult;

                    if (finalizer.TryFinalizeResult(result, expression, out finalizedResult))
                    {
                        return finalizedResult;
                    }
                }

                var list = result.Cast<object>();
                return list.SingleOrDefault();
            }
        }

        ////public int ExecuteDelete(Expression expression)
        ////{
        ////    ////var connection = _connectionFactory();
        ////    ////try
        ////    ////{
        ////    ////    var part = ParseDeleteExpression(expression);
        ////    ////    var parsedQuery = _queryFormatter.ResolveDeletion(part);

        ////    ////    var sqlCommand = connection.CreateSqlCommand(parsedQuery.QueryText);
        ////    ////    BindParameters(parsedQuery, sqlCommand);

        ////    ////    return sqlCommand.ExecuteNonQuery();
        ////    ////}
        ////    ////finally
        ////    ////{
        ////    ////    DisposeOfConnection(connection);
        ////    ////}
        ////    return 0;
        ////}

        ////public string GetQueryText(Expression expression)
        ////{
        ////    var part = ParseQueryExpression(expression);
        ////    var parsedQuery = _queryFormatter.ResolveSelection(part);
        ////    return parsedQuery.ToString();
        ////}

        ////private Query ParseDeleteExpression(Expression expression)
        ////{
        ////    var generator = _queryTranslatorFactory();
        ////    var part = generator.TranslateDelete(expression);
        ////    return part;
        ////}
    }
}