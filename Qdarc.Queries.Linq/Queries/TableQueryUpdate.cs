﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Queries
{
    public static class TableQueryUpdate
    {
        public static int Update<TFirst>(
            this ITableQuery<TFirst> query,
            Expression<Func<TFirst, TFirst>> set)
        {
            var method = ExpressionExtensions.GetMethod(() => Update<TFirst>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(set),
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.ExecuteNonQuery(expression);
        }

        public static int Update<TFirst, TSecond, TUpdated>(
            this ITableQuery<Tuple<TFirst, TSecond>> query,
            Expression<Func<TFirst, TSecond, TUpdated>> targetSelector,
            Expression<Func<TFirst, TSecond, TUpdated>> set)
        {
            var method = ExpressionExtensions.GetMethod(() => Update<TFirst, TSecond, TUpdated>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(targetSelector),
                Expression.Quote(set),
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.ExecuteNonQuery(expression);
        }
    }
}
