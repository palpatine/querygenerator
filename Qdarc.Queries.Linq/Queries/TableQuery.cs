using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Queries
{
    public static class TableQuery
    {
        public static bool Any<TSource>(
            this ITableQuery<TSource> query)
        {
            var expression = AnyInner(query);

            return query.Provider.Execute<bool>(expression);
        }

        public static bool Any<TSource>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, bool>> predicate)
        {
            var expression = AnyInner(query, predicate);

            return query.Provider.Execute<bool>(expression);
        }

        public static IQueryable<TElement> AsQueryable<TElement>(this ITableQuery<TElement> query)
        {
            var method = ExpressionExtensions.GetMethod(() => AsQueryable<TElement>(null));
            return (IQueryable<TElement>)query.Provider.CreateQuery<TElement>(Expression.Call(method, query.Expression));
        }

        public static bool Contains<TSource>(
            this ITableQuery<TSource> query,
            TSource value)
        {
            var method = ExpressionExtensions.GetMethod(() => Contains<TSource>(null, default(TSource)));
            var arguments = new[]
            {
                query.Expression,
                Expression.Constant(value)
            };

            var containsQuery = query.Provider.CreateQuery<TSource>(Expression.Call(
                null,
                method,
                arguments));

            return containsQuery.Provider.Execute<bool>(containsQuery.Expression);
        }

        ////    return query.Provider.CreateGroupingQuery<TKey, TSource, TProjection>(Expression.Call(
        ////        null,
        ////        method,
        ////        arguments));
        ////}
        ////public static IGroupingTableQuery<TKey, TSource, TProjection> GroupBy<TSource, TKey, TProjection>(
        ////    this ITableQuery<TSource> query,
        ////    Expression<Func<TSource, TKey>> keyRetriver,
        ////    Expression<Func<IGroup<TKey, TSource>, TProjection>> projection)
        ////{
        ////    var method = ExpressionExtensions.GetMethod(() => GroupBy<TSource, TKey, TProjection>(null, null, null));
        ////    var arguments = new[]
        ////    {
        ////        query.Expression,
        ////        Expression.Quote(keyRetriver),
        ////        Expression.Quote(projection)
        ////    };
        ////public static ITableQuery Create(
        ////    Type resultType,
        ////    string query,
        ////    IEnumerable<Tuple<string, object>> arguments,
        ////    Func<ISqlConnection> connectionFactory)
        ////{
        ////    var type = typeof(TableQuery<>).MakeGenericType(resultType);
        ////    return (ITableQuery)Activator.CreateInstance(type, new object[] { connectionFactory, query, arguments });
        ////}

        public static TSource ElementAt<TSource>(
            this ITableQuery<TSource> query,
            int index)
        {
            var method = ExpressionExtensions.GetMethod(() => ElementAt<TSource>(null, 0));
            var arguments = new[]
            {
                query.Expression,
                Expression.Constant(index)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource First<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => First<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource FirstOrDefault<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => FirstOrDefault<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static IOrderedTableQuery<TSource> OrderBy<TSource, TOrdered>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, TOrdered>> property)
        {
            var method = ExpressionExtensions.GetMethod(() => OrderBy<TSource, TOrdered>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateOrderedQuery<TSource>(expression);
        }

        public static IOrderedTableQuery<TSource> OrderByDescending<TSource, TOrdered>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, TOrdered>> property)
        {
            var method = ExpressionExtensions.GetMethod(() => OrderByDescending<TSource, TOrdered>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateOrderedQuery<TSource>(expression);
        }

        public static TProjection Select<TProjection>(
            ITableQueryProvider provider,
            Expression<Func<IDatabase, TProjection>> projection)
        {
            var method = ExpressionExtensions.GetMethod(() => Select<TProjection>(null, null));
            var arguments = new Expression[]
            {
                Expression.Constant(null, typeof(ITableQueryProvider)),
                Expression.Quote(projection)
            };

            return provider.Execute<TProjection>(Expression.Call(
                null,
                method,
                arguments));
        }

        public static ITableQuery<TProjection> Select<TSource, TProjection>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, TProjection>> projection)
        {
            var method = ExpressionExtensions.GetMethod(() => Select<TSource, TProjection>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(projection)
            };

            var callExpression = Expression.Call(
                null,
                method,
                arguments);
            return query.Provider.CreateQuery<TProjection>(callExpression);
        }

        public static TSource Single<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => Single<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource Single<TSource>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => Single<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource SingleOrDefault<TSource>(
            this ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => SingleOrDefault<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static TSource SingleOrDefault<TSource>(
           this ITableQuery<TSource> query,
           Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => SingleOrDefault<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.Execute<TSource>(expression);
        }

        public static List<TSource> ToList<TSource>(
            this ITableQuery<TSource> query)
        {
            return query.Provider.Execute<List<TSource>>(query.Expression);
        }

        public static ITableQuery<TSource> Top<TSource>(this ITableQuery<TSource> query, int count)
        {
            var method = ExpressionExtensions.GetMethod(() => Top<TSource>(null, 0));
            var arguments = new[]
            {
                query.Expression,
                Expression.Constant(count)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);
            return query.Provider.CreateQuery<TSource>(expression);
        }

        public static ITableQuery<TSource> Where<TSource>(
            this ITableQuery<TSource> query,
            Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => Where<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);
            return query.Provider.CreateQuery<TSource>(expression);
        }

        internal static MethodCallExpression AnyInner<TSource>(ITableQuery<TSource> query, Expression<Func<TSource, bool>> predicate)
        {
            var method = ExpressionExtensions.GetMethod(() => Any<TSource>(null, null));
            var arguments = new[]
            {
                query.Expression,
                Expression.Quote(predicate)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);
            return expression;
        }

        internal static MethodCallExpression AnyInner<TSource>(ITableQuery<TSource> query)
        {
            var method = ExpressionExtensions.GetMethod(() => Any<TSource>(null));
            var arguments = new[]
            {
                query.Expression,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);
            return expression;
        }
    }
}