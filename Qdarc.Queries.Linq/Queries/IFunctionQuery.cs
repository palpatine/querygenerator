using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Linq.Queries
{
    public interface IFunctionQuery
    {
        IEnumerable<Expression> Arguments { get; }

        Type ElementType { get; }

        ElementName FunctionName { get; }
    }
}