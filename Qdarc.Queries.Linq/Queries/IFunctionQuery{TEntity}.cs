namespace Qdarc.Queries.Linq.Queries
{
    public interface IFunctionQuery<out TEntity> : ITableQuery<TEntity>
    {
    }
}