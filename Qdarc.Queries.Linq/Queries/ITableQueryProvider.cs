using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Queries
{
    public interface ITableQueryProvider
    {
        IQueryProvider AsQueryProvider();

        IOrderedTableQuery<TElement> CreateOrderedQuery<TElement>(Expression expression);

        ITableQuery<TElement> CreateQuery<TElement>(Expression expression);

        object Execute(Expression expression);

        int ExecuteNonQuery(Expression expression);

        int ExecuteNonQuery(ISqlExpression expression);

        TResult Execute<TResult>(Expression expression);

        TResult Execute<TResult>(ISqlExpression expression);

        ISqlExpression Visit(Expression expression);
    }
}