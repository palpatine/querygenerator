using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Queries
{
    internal class ParametersBinder : IParametersBinder
    {
        private readonly ISqlTypeManager _typeManager;
        private readonly ITableParameterValueConverter _tableParameterValueConverter;

        public ParametersBinder(
            ISqlTypeManager typeManager,
            ITableParameterValueConverter tableParameterValueConverter)
        {
            _typeManager = typeManager;
            _tableParameterValueConverter = tableParameterValueConverter;
        }

        public void BindParameters(
            IEnumerable<IParameter> parameters,
            ISqlCommand sqlCommand,
            bool trace)
        {
            var func = typeof(Func<>);
            foreach (var parameter in parameters)
            {
                var linqParameterReference = parameter as LinqParameterReference;
                if (linqParameterReference != null)
                {
                    var type = func.MakeGenericType(linqParameterReference.ClrType);
                    var lambda = Expression.Lambda(type, linqParameterReference.Expression);
                    var value = lambda.Compile().DynamicInvoke();
                    var directParameterType = _typeManager.GetSqlType(linqParameterReference.ClrType, value);
                    ////var converter =
                    ////    _queryParameterValueConverterFactory.Resolve(value.GetType());

                    ////if (converter != null)
                    ////{
                    ////    value = converter.Convert(value);
                    ////}
                    var sqlParameter = sqlCommand.AddParameter();
                    sqlParameter.Type = linqParameterReference.SqlType != null
                        ? _typeManager.GetSqlType(linqParameterReference.SqlType, directParameterType)
                        : directParameterType;
                    sqlParameter.Name = linqParameterReference.Name;
                    sqlParameter.Value = value;

                    if (trace)
                    {
                        Debug.WriteLine("{0} {1} = {2}", linqParameterReference.Name, linqParameterReference.SqlType, value ?? "<null>");
                    }
                }

                var tableParameter = parameter as LinqTableParameterReference;
                if (tableParameter != null)
                {
                    var resultType = typeof(IEnumerable<>).MakeGenericType(tableParameter.ClrType);
                    var type = func.MakeGenericType(resultType);
                    var lambda = Expression.Lambda(type, tableParameter.Expression);
                    var value = (IEnumerable)lambda.Compile().DynamicInvoke();

                    if (_tableParameterValueConverter != null)
                    {
                        value = _tableParameterValueConverter.Convert(tableParameter.ClrType, value);
                    }

                    var sqlParameter = sqlCommand.AddParameter();
                    sqlParameter.Type = tableParameter.SqlType;
                    sqlParameter.Name = tableParameter.Name;
                    sqlParameter.Value = value;

                    if (trace)
                    {
                        Debug.WriteLine("{0} {1} = <table>", tableParameter.Name, tableParameter.SqlType);
                    }
                }
            }
        }
    }
}