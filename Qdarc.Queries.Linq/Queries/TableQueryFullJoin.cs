using System;
using System.Linq.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Queries
{
    public static class TableQueryFullJoin
    {
        public static ITableQuery<Tuple<TFirst, TSecond>> FullJoin<TFirst, TSecond>(
           this ITableQuery<TFirst> query,
           ITableQuery<TSecond> other,
           Expression<Func<TFirst, TSecond, bool>> on)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond>>(expression);
        }

        public static ITableQuery<TProjection> FullJoin<TFirst, TSecond, TProjection>(
            this ITableQuery<TFirst> query,
            ITableQuery<TSecond> other,
            Expression<Func<TFirst, TSecond, bool>> on,
            Expression<Func<TFirst, TSecond, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TProjection>(null, null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on),
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird>> FullJoin<TFirst, TSecond, TThird>(
            this ITableQuery<Tuple<TFirst, TSecond>> query,
            ITableQuery<TThird> other,
            Expression<Func<TFirst, TSecond, TThird, bool>> on)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird>>(expression);
        }

        public static ITableQuery<TProjection> FullJoin<TFirst, TSecond, TThird, TProjection>(
            this ITableQuery<Tuple<TFirst, TSecond>> query,
            ITableQuery<TThird> other,
            Expression<Func<TFirst, TSecond, TThird, bool>> on,
            Expression<Func<TFirst, TSecond, TThird, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird, TProjection>(null, null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on),
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth>> FullJoin<TFirst, TSecond, TThird, TFourth>(
            this ITableQuery<Tuple<TFirst, TSecond, TThird>> query,
            ITableQuery<TFourth> other,
            Expression<Func<TFirst, TSecond, TThird, TFourth, bool>> on)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird, TFourth>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird, TFourth>>(expression);
        }

        public static ITableQuery<TProjection> FullJoin<TFirst, TSecond, TThird, TFourth, TProjection>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird>> query,
           ITableQuery<TFourth> other,
           Expression<Func<TFirst, TSecond, TThird, TFourth, bool>> on,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird, TFourth, TProjection>(null, null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on),
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> FullJoin<TFirst, TSecond, TThird, TFourth, TFifth>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth>> query,
           ITableQuery<TFifth> other,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, bool>> on)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird, TFourth, TFifth>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>>(expression);
        }

        public static ITableQuery<TProjection> FullJoin<TFirst, TSecond, TThird, TFourth, TFifth, TProjection>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth>> query,
           ITableQuery<TFifth> other,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, bool>> on,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird, TFourth, TFifth, TProjection>(null, null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on),
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }

        public static ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>> FullJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(
          this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> query,
          ITableQuery<TSixth> other,
          Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, bool>> on)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth, TSixth>>(expression);
        }

        public static ITableQuery<TProjection> FullJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TProjection>(
           this ITableQuery<Tuple<TFirst, TSecond, TThird, TFourth, TFifth>> query,
           ITableQuery<TSixth> other,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, bool>> on,
           Expression<Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TProjection>> selector)
        {
            var method = ExpressionExtensions.GetMethod(() => FullJoin<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TProjection>(null, null, null, null));
            var arguments = new[]
            {
                query.Expression,
                other.Expression,
                Expression.Quote(on),
                Expression.Quote(selector)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return query.Provider.CreateQuery<TProjection>(expression);
        }
    }
}