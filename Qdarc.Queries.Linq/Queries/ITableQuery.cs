using System;
using System.Collections;
using System.Linq.Expressions;

namespace Qdarc.Queries.Linq.Queries
{
    public interface ITableQuery : IEnumerable
    {
        Type ElementType { get; }

        Expression Expression { get; }

        ITableQueryProvider Provider { get; }
    }
}