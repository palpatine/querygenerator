﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Qdarc.Queries.Linq.Queries
{
    public sealed class ValuesQuery<TValue> : IValuesQuery<TValue>, IValuesQuery, IQueryable<TValue>
    {
        private readonly Expression _expression;

        private readonly ITableQueryProvider _provider;

        public ValuesQuery(ITableQueryProvider provider, ConstantExpression values)
        {
            if (provider == null)
            {
                throw new ArgumentNullException(nameof(provider));
            }

            if (values == null)
            {
                throw new ArgumentNullException(nameof(values));
            }

            _provider = provider;
            _expression = Expression.Constant(this);
            if (!typeof(IEnumerable<TValue>).GetTypeInfo().IsAssignableFrom(values.Type.GetTypeInfo()))
            {
                throw new ArgumentException($"Values must be collection of {typeof(TValue).Name}", nameof(values));
            }

            Values = values;
        }

        public ConstantExpression Values { get; }

        public Type ElementType => typeof(TValue);

        public Expression Expression => _expression;

        IQueryProvider IQueryable.Provider => Provider.AsQueryProvider();

        public ITableQueryProvider Provider => _provider;

        public IEnumerator<TValue> GetEnumerator()
        {
            return ((IEnumerable<TValue>)_provider.Execute(_expression)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
