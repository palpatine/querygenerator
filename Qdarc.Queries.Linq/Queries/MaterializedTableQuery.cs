﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Qdarc.Queries.Linq.Queries
{
    internal class MaterializedTableQuery<TEntity> : ITableQuery<TEntity>
    {
        private readonly List<TEntity> _values;

        public MaterializedTableQuery(IEnumerable<TEntity> values)
        {
            _values = values.ToList();
        }

        public Type ElementType => typeof(TEntity);

        public Expression Expression => null;

        public ITableQueryProvider Provider => null;

        public IEnumerator<TEntity> GetEnumerator()
        {
            return _values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}