using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace Qdarc.Queries.Linq.Queries
{
    public static class TypeSystem
    {
        public static Type GetElementType(Type seqType)
        {
            Type ienum = FindTableQuery(seqType);

            if (ienum == null)
            {
                return seqType;
            }

            return ienum.GenericTypeArguments[0];
        }

        public static Type GetElementType<TResult>()
        {
            var type = typeof(TResult);
            var elementType = type;
            if (elementType.IsConstructedGenericType && typeof(IEnumerable).GetTypeInfo().IsAssignableFrom(elementType.GetTypeInfo()))
            {
                elementType = type.GenericTypeArguments[0];
            }

            return elementType;
        }

        public static bool IsCollection(Type type)
        {
            return type.IsConstructedGenericType
                   && new[]
                       {
                           typeof(IEnumerable<>),
                           typeof(IOrderedEnumerable<>),
                           typeof(IQueryable<>),
                           typeof(IOrderedQueryable<>),
                           typeof(IList<>),
                           typeof(ICollection<>),
                           typeof(TableQuery<>),
                           typeof(IFunctionQuery<>),
                           typeof(FunctionQuery<>),
                           typeof(ITableQuery<>),
                           typeof(ValuesQuery<>),
                           typeof(IValuesQuery<>),
                           typeof(TableParameter<>),
                           typeof(ITableParameter<>),
                           typeof(Collection<>),
                           typeof(List<>)
                       }
                       .Contains(type.GetGenericTypeDefinition());
        }

        private static Type FindTableQuery(Type seqType)
        {
            if (seqType == null || seqType == typeof(string))
            {
                return null;
            }

            if (seqType.IsArray)
            {
                return typeof(ITableQuery<>).MakeGenericType(seqType.GetElementType());
            }

            if (seqType.GetTypeInfo().IsGenericType)
            {
                foreach (Type arg in seqType.GenericTypeArguments)
                {
                    Type ienum = typeof(ITableQuery<>).MakeGenericType(arg);
                    if (ienum.GetTypeInfo().IsAssignableFrom(seqType.GetTypeInfo()))
                    {
                        return ienum;
                    }

                    ienum = typeof(IQueryable<>).MakeGenericType(arg);
                    if (ienum.GetTypeInfo().IsAssignableFrom(seqType.GetTypeInfo()))
                    {
                        return ienum;
                    }
                }
            }

            IEnumerable<Type> ifaces = seqType.GetTypeInfo().ImplementedInterfaces;
            if (ifaces != null && ifaces.Any())
            {
                foreach (Type iface in ifaces)
                {
                    Type ienum = FindTableQuery(iface);
                    if (ienum != null)
                    {
                        return ienum;
                    }
                }
            }

            if (seqType.GetTypeInfo().BaseType != null && seqType.GetTypeInfo().BaseType != typeof(object))
            {
                return FindTableQuery(seqType.GetTypeInfo().BaseType);
            }

            return null;
        }
    }
}