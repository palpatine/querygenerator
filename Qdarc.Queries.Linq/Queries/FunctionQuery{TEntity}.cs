using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Linq.Queries
{
    public sealed class FunctionQuery<TEntity> : IFunctionQuery<TEntity>, IFunctionQuery, IQueryable<TEntity>
    {
        private readonly Expression _expression;

        private readonly ITableQueryProvider _provider;

        public FunctionQuery(ITableQueryProvider provider, ElementName functionName, IEnumerable<Expression> arguments)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
            Arguments = arguments ?? throw new ArgumentNullException(nameof(arguments));
            FunctionName = functionName ?? throw new ArgumentNullException(nameof(functionName));
            _expression = Expression.Constant(this);
        }

        public IEnumerable<Expression> Arguments { get; }

        public Type ElementType => typeof(TEntity);

        public ElementName FunctionName { get; }

        public Expression Expression => _expression;

        IQueryProvider IQueryable.Provider => Provider.AsQueryProvider();

        public ITableQueryProvider Provider => _provider;

        public IEnumerator<TEntity> GetEnumerator()
        {
            return ((IEnumerable<TEntity>)_provider.Execute(_expression)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}