using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Qdarc.Queries.Linq.Queries
{
    public sealed class TableQuery<TEntity> : ITableQuery<TEntity>, IOrderedQueryable<TEntity>,
        IOrderedTableQuery<TEntity>
    {
        private readonly Expression _expression;

        private readonly ITableQueryProvider _provider;

        public TableQuery(ITableQueryProvider provider)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            _provider = provider;
            _expression = Expression.Constant(this);
        }

        public TableQuery(
            ITableQueryProvider provider,
            Expression expression)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider");
            }

            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }

            if (!typeof(ITableQuery<TEntity>).GetTypeInfo().IsAssignableFrom(expression.Type.GetTypeInfo())
                && !typeof(IOrderedTableQuery<TEntity>).GetTypeInfo().IsAssignableFrom(expression.Type.GetTypeInfo())
                && !typeof(IQueryable<TEntity>).GetTypeInfo().IsAssignableFrom(expression.Type.GetTypeInfo()))
            {
                throw new ArgumentOutOfRangeException("expression");
            }

            _provider = provider;
            _expression = expression;
        }

        public Type ElementType => typeof(TEntity);

        public Expression Expression => _expression;

        IQueryProvider IQueryable.Provider => Provider.AsQueryProvider();

        public ITableQueryProvider Provider => _provider;

        public IEnumerator<TEntity> GetEnumerator()
        {
            return ((IEnumerable<TEntity>)_provider.Execute(_expression)).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}