namespace Qdarc.Queries.Linq.Queries
{
    public interface IValuesQuery<TValue> : IValuesQuery, ITableQuery<TValue>
    {
    }
}