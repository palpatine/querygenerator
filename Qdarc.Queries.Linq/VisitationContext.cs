﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq
{
    public class VisitationContext : IVisitationContext
    {
        private readonly Func<IEnumerable<ILinqExpressionHandler>> _handlersFactory;
        private readonly Func<IModelToSqlConvention> _modelToSqlConventionFactory;
        private readonly Dictionary<ParameterExpression, ISource> _parameterMapping
            = new Dictionary<ParameterExpression, ISource>();

        private readonly Func<ISqlTypeManager> _typeManagerFactory;
        private IDictionary<ExpressionType, IEnumerable<ILinqExpressionHandler>> _handlers;
        private IModelToSqlConvention _modelToSqlConvention;
        private ISqlTypeManager _typeManager;
        private IExplicitSelectionListGenerator _explicitSelectionListGenerator;

        public VisitationContext(
            Func<ISqlTypeManager> typeProviderFactory,
            Func<IModelToSqlConvention> modelToSqlConventionFactory,
            Func<IEnumerable<ILinqExpressionHandler>> handlersFactory,
            IExplicitSelectionListGenerator explicitSelectionListGenerator)
        {
            _typeManagerFactory = typeProviderFactory;
            _modelToSqlConventionFactory = modelToSqlConventionFactory;
            _handlersFactory = handlersFactory;
            _explicitSelectionListGenerator = explicitSelectionListGenerator;
        }

        public IModelToSqlConvention ModelToSqlConvention => _modelToSqlConvention ?? (_modelToSqlConvention = _modelToSqlConventionFactory());

        public ISqlTypeManager TypeManager => _typeManager ?? (_typeManager = _typeManagerFactory());

        private IDictionary<ExpressionType, IEnumerable<ILinqExpressionHandler>> Handlers => _handlers ?? (_handlers = CreateHandlers());

        public void AddParameterMapping(ParameterExpression parameter, ISource source)
        {
            _parameterMapping.Add(parameter, source);
        }

        public ISqlSelection ConvertToSelectionList(ISqlExpression expression)
        {
            var columnReference = expression as IColumnReference;

            if (columnReference != null)
            {
                return new SqlSelection(new[] { columnReference }, columnReference.ClrType);
            }

            var aggregateExpression = expression as ISqlAggregateExpression;

            if (aggregateExpression != null)
            {
                return new SqlSelection(new[] { aggregateExpression }, aggregateExpression.ClrType);
            }

            var source = expression as INamedSource;
            if (source != null)
            {
                return GenerateExplicitSelectionList(source);
            }
            else
            {
                return (ISqlSelection)expression;
            }
        }

        public ISelectCommand EnsureSelection(ISqlExpression part)
        {
            if (part is ISelectCommand selectCommand)
            {
                return selectCommand;
            }

            return WrapInSelectCommand(part);
        }

        public SelectCommand WrapInSelectCommand(ISqlExpression part)
        {
            var source = part as ISource;
            if (source == null)
            {
                throw new InvalidOperationException();
            }

            var namedSource = part as AliasedSourceItem;

            if (namedSource == null)
            {
                namedSource = new AliasedSourceItem(source);
            }

            return new SelectCommand(
                selection: GenerateExplicitSelectionList(namedSource),
                source: namedSource);
        }

        public ISqlExpression FinalizeSelection(ISqlExpression part)
        {
            return this.EnsureSelection(part);
        }

        public ISqlSelection GenerateExplicitSelectionList(ISource source)
        {
            return _explicitSelectionListGenerator.Generate(source);
        }

        public ISource GetParameterMapping(ParameterExpression parameter)
        {
            return _parameterMapping[parameter];
        }

        public ISqlLogicalExpression ProcessSingleParameterFilter(UnaryExpression quote, ISelectCommand source)
        {
            var lambda = (LambdaExpression)quote.Operand;
            var parameter = lambda.Parameters.Single();

            AddParameterMapping(parameter, source);
            var expression = VisitExpression(lambda.Body);
            RemoveParameterMapping(parameter);

            var columnReference = expression as LinqColumnReference;

            if (columnReference != null)
            {
                return WrapInLogicalExpression(columnReference);
            }
            else
            {
                return (ISqlLogicalExpression)expression;
            }
        }

        public void RemoveParameterMapping(ParameterExpression parameter)
        {
            _parameterMapping.Remove(parameter);
        }

        public ISqlExpression VisitExpression(Expression expression)
        {
            if (Handlers.ContainsKey(expression.NodeType))
            {
                foreach (var linqExpressionHandler in Handlers[expression.NodeType])
                {
                    if (linqExpressionHandler.HandledType == expression.NodeType && linqExpressionHandler.CanHandle(expression))
                    {
                        return linqExpressionHandler.Handle(expression);
                    }
                }
            }

            throw new InvalidOperationException($"Unhandled expression type {expression.NodeType}");
        }

        public ISqlLogicalExpression WrapInLogicalExpression(LinqColumnReference columnReference)
        {
            if (columnReference.ClrType != typeof(bool)
                && columnReference.ClrType != typeof(bool?))
            {
                throw new InvalidOperationException($"Property {columnReference.Property.DeclaringType.Name}.{columnReference.Property.Name} cannot be wrapped in logical expression.");
            }

            var sqlConstantExpression = new SqlConstantExpression(1, typeof(int), TypeManager.GetSqlType(Expression.Constant(1)));
            return new EqualLogicalExpression(columnReference, sqlConstantExpression);
        }

        public bool CanApplyFilterDirectly(SelectCommand selectCommand)
        {
            return selectCommand.Group == null && selectCommand.Top == null;
        }

        private IDictionary<ExpressionType, IEnumerable<ILinqExpressionHandler>> CreateHandlers()
        {
            var handlers = _handlersFactory().ToArray();
            foreach (var linqExpressionHandler in handlers)
            {
                linqExpressionHandler.AttachContext(this);
            }

            var result = handlers
                .GroupBy(x => x.HandledType, (key, x) => new { Key = key, Value = x.OrderByDescending(z => z.Priority).ToArray() })
                .ToDictionary(x => x.Key, x => x.Value.AsEnumerable());

            return result;
        }
    }
}