using System;

namespace Qdarc.Queries.Linq.Extensions
{
    public interface IQueryParameterValueConverterFactory
    {
        IQueryParameterValueConverter<TElement> Resolve<TElement>();

        IQueryParameterValueConverter Resolve(Type type);
    }
}