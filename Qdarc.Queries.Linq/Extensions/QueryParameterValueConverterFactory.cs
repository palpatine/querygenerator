using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Qdarc.Queries.Linq.Extensions
{
    internal sealed class QueryParameterValueConverterFactory : IQueryParameterValueConverterFactory
    {
        private readonly ConcurrentDictionary<Type, IQueryParameterValueConverter> _converters
            = new ConcurrentDictionary<Type, IQueryParameterValueConverter>();

        public QueryParameterValueConverterFactory(IEnumerable<IQueryParameterValueConverter> converters)
        {
            foreach (var queryParameterValueConverter in converters)
            {
                if (!_converters.TryAdd(queryParameterValueConverter.HandledType, queryParameterValueConverter))
                {
                    throw new InvalidOperationException(
                        $"Cannot register multiple converters handling the same type: {queryParameterValueConverter.HandledType.FullName}");
                }
            }
        }

        public IQueryParameterValueConverter<TElement> Resolve<TElement>()
        {
            return (IQueryParameterValueConverter<TElement>)Resolve(typeof(TElement));
        }

        public IQueryParameterValueConverter Resolve(Type type)
        {
            if (_converters.ContainsKey(type))
            {
                return _converters[type];
            }

            var interfaces = type.GetTypeInfo().ImplementedInterfaces;
            return interfaces.Select(Resolve)
                .Where(converter => converter != null)
                .OrderByDescending(x => x.Priority)
                .FirstOrDefault();
        }
    }
}