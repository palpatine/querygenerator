using System;

namespace Qdarc.Queries.Linq.Extensions
{
    /// <summary>
    /// Time provider.
    /// </summary>
    public interface ITimeProvider
    {
        /// <summary>
        /// Gets the now.
        /// </summary>
        /// <value>The now.</value>
        DateTime Now { get; }
    }
}