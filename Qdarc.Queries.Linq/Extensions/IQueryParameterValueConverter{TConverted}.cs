namespace Qdarc.Queries.Linq.Extensions
{
    public interface IQueryParameterValueConverter<in TConverted> : IQueryParameterValueConverter
    {
        object Convert(TConverted value);
    }
}