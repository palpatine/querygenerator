using System;

namespace Qdarc.Queries.Linq.Extensions
{
    public interface IQueryParameterValueConverter
    {
        Type HandledType { get; }

        double Priority { get; }

        object Convert(object value);
    }
}