﻿using System.Runtime.CompilerServices;

#if DEBUG
[assembly: InternalsVisibleTo("Qdarc.Queries.Linq.Tests")]
[assembly: InternalsVisibleTo("Qdarc.Queries.Tsql.Integration.Tests")]
#endif