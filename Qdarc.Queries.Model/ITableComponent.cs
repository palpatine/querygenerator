﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model
{
    public interface ITableComponent : ISqlExpression
    {
    }
}