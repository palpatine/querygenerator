﻿using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model
{
    public interface ITableReference : ISource
    {
    }
}