﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model
{
    public interface IModelToSqlConvention
    {
        string CompoundColumnSeparator { get; }

        ElementName GetColumnName(INamedSource source, PropertyInfo property);

        ElementName GetColumnName(INamedSource source, IEnumerable<PropertyInfo> properties);

        string GetColumnName(PropertyInfo propertyInfo);

        IEnumerable<LinqColumnReference> GetSelectableColumns(INamedSource source);

        IEnumerable<PropertyInfo> GetSelectableProperties(Type source);

        IEnumerable<LinqColumnReference> GetInsertableColumns(Type type);

        ISqlType TryGetSqlType(Type entityType, PropertyInfo property);

        ISqlType GetSqlType(Type entityType, PropertyInfo property);

        ElementName GetTableName(Type type);

        ISqlType GetSqlType(MethodInfo method);

        ISqlType GetUserDefinedTableType(Type type);
    }
}