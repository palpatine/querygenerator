namespace Qdarc.Queries.Model.Constraints
{
    public enum ForeignKeyTriggerKind
    {
        OnUpdate,
        OnDelete
    }
}