﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public interface IColumnConstraint
    {
        TResult Accept<TResult>(ITransformer transformer);
    }
}