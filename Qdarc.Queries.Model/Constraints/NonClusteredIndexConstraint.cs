﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class NonClusteredIndexConstraint : ColumnsTableConstraint, IIndexConstraint
    {
        public NonClusteredIndexConstraint(string name, bool isUnique, bool isPrimaryKey)
            : base(name)
        {
            IsUnique = isUnique;
            IsPrimaryKey = isPrimaryKey;
            if (isPrimaryKey && !isUnique)
            {
                throw new InvalidOperationException("Primary key must be unique");
            }
        }

        public bool IsPrimaryKey { get; }

        public bool IsUnique { get; }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<NonClusteredIndexConstraint, TResult>(this);
    }
}