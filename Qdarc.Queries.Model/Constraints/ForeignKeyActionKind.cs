namespace Qdarc.Queries.Model.Constraints
{
    public enum ForeignKeyActionKind
    {
        Undefined,
        NoAction,
        Cascade,
        SetNull,
        SetDefault
    }
}