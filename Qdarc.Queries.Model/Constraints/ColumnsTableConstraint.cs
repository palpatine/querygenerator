using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public class ColumnsTableConstraint : Constraint, ITableComponent
    {
        private readonly List<OrderedColumnReference> _columnReferences = new List<OrderedColumnReference>();

        public ColumnsTableConstraint(string name)
            : base(name)
        {
        }

        public IEnumerable<OrderedColumnReference> ColumnReferences => _columnReferences;

        public void SetColumnReference(IEnumerable<OrderedColumnReference> columnReferences)
        {
            if (_columnReferences.Any())
            {
                throw new InvalidOperationException();
            }

            _columnReferences.AddRange(columnReferences);
        }

        public void SetColumnReference(params OrderedColumnReference[] columnReferences)
        {
            SetColumnReference((IEnumerable<OrderedColumnReference>)columnReferences);
        }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<ColumnsTableConstraint, TResult>(this);
    }
}