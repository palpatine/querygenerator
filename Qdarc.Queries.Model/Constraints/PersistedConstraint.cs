using Qdarc.Queries.Model.Declarations;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class PersistedConstraint : IColumnConstraint
    {
        public PersistedNullableValue NullableValue { get; set; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<PersistedConstraint, TResult>(this);
    }
}