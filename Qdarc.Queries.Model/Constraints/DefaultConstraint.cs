using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class DefaultConstraint : Constraint
    {
        public DefaultConstraint(string name, ISqlValueExpression value)
            : base(name)
        {
            Value = value;
        }

        public ISqlValueExpression Value { get; }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<DefaultConstraint, TResult>(this);
    }
}