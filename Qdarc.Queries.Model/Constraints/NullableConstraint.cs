﻿using Qdarc.Queries.Model.Declarations;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class NullableConstraint : IColumnConstraint
    {
        public NullableValue NullableValue { get; set; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<NullableConstraint, TResult>(this);
    }
}