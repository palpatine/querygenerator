﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class CheckConstraint : Constraint, ITableComponent
    {
        public CheckConstraint(string name, ISqlLogicalExpression expression)
            : base(name) => Expression = expression;

        public ISqlLogicalExpression Expression { get; }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<CheckConstraint, TResult>(this);
    }
}