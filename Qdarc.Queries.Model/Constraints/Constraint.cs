using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public abstract class Constraint : IColumnConstraint
    {
        protected Constraint(string name) => Name = name;

        public string Name { get; }

        public abstract TResult Accept<TResult>(ITransformer transformer);
    }
}