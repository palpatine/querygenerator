using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class ForeignKeyConstraint : Constraint, ITableComponent
    {
        private readonly List<IColumnReference> _referencedTableColumns = new List<IColumnReference>();
        private readonly List<IColumnReference> _targetColumnReferences = new List<IColumnReference>();

        public ForeignKeyConstraint(string name)
            : base(name)
        {
        }

        public IEnumerable<ForeignKeyAction> Actions { get; set; }

        public IEnumerable<IColumnReference> ReferencedTableColumns => _referencedTableColumns;

        public ITableReference Table { get; set; }

        public IEnumerable<IColumnReference> TargetColumnReferences => _targetColumnReferences;

        public void SetReferencedTableColumns(IEnumerable<IColumnReference> columnReferences)
        {
            if (_referencedTableColumns.Any())
            {
                throw new InvalidOperationException();
            }

            _referencedTableColumns.AddRange(columnReferences);
        }

        public void SetReferencedTableColumns(params IColumnReference[] columnReferences)
        {
            SetReferencedTableColumns((IEnumerable<IColumnReference>)columnReferences);
        }

        public void SetTargetColumns(IEnumerable<IColumnReference> columnReferences)
        {
            if (_targetColumnReferences.Any())
            {
                throw new InvalidOperationException();
            }

            _targetColumnReferences.AddRange(columnReferences);
        }

        public void SetTargetColumns(params IColumnReference[] columnReferences)
        {
            SetTargetColumns((IEnumerable<IColumnReference>)columnReferences);
        }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<ForeignKeyConstraint, TResult>(this);
    }
}