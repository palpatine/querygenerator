﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class ClusteredIndexConstraint : ColumnsTableConstraint, IIndexConstraint
    {
        public ClusteredIndexConstraint(string name, bool isUnique, bool isPrimaryKey)
            : base(name)
        {
            IsUnique = isUnique;
            IsPrimaryKey = isPrimaryKey;
        }

        public bool IsPrimaryKey { get; }

        public bool IsUnique { get; }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<ClusteredIndexConstraint, TResult>(this);
    }
}