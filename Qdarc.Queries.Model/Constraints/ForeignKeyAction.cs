using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Constraints
{
    public sealed class ForeignKeyAction
    {
        public ForeignKeyActionKind ActionKind { get; set; }

        public ForeignKeyTriggerKind TriggerKind { get; set; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ForeignKeyAction, TResult>(this);
    }
}