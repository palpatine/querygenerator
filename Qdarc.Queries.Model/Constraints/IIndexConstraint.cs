﻿using System.Collections.Generic;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Model.Constraints
{
    public interface IIndexConstraint : ITableComponent
    {
        bool IsPrimaryKey { get; }

        bool IsUnique { get; }
        IEnumerable<OrderedColumnReference> ColumnReferences { get; }
        void SetColumnReference(IEnumerable<OrderedColumnReference> columnReferences);
        void SetColumnReference(params OrderedColumnReference[] columnReferences);
    }
}