using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Sources
{
    public sealed class ColumnAlias
    {
        public ColumnAlias(string @alias, Type clrType, ISqlType sqlType)
        {
            Alias = alias ?? throw new ArgumentNullException(nameof(alias));
            ClrType = clrType;
            SqlType = sqlType;
        }

        public string Alias { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ColumnAlias, TResult>(this);
    }
}