﻿using System.Collections.Generic;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Expressions
{
    public interface IValuesSource : ISource
    {
        IEnumerable<ValueSourceRow> Rows { get; }
    }
}