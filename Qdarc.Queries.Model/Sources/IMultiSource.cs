﻿using System.Collections.Generic;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Sources
{
    public interface IMultisource : ISource
    {
        IEnumerable<ISource> Sources { get; }
    }
}