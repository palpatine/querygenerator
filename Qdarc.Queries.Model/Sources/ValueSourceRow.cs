using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class ValueSourceRow
    {
        public ValueSourceRow(IEnumerable<ISqlValueExpression> values)
        {
            Values = values?.ToArray() ?? throw new ArgumentNullException(nameof(values));

            if (Values != null && Values.Any(x => x == null))
            {
                throw new ArgumentException("Value cannot be null.", nameof(values));
            }
        }

        public IEnumerable<ISqlValueExpression> Values { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ValueSourceRow, TResult>(this);
    }
}