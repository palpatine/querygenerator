﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class ValuesSource : IValuesSource
    {
        public ValuesSource(
            IEnumerable<ValueSourceRow> rows,
            Type clrType)
        {
            Rows = rows?.ToArray() ?? throw new ArgumentNullException(nameof(rows));
            ClrType = clrType;
            if (!Rows.Any())
            {
                throw new ArgumentException("At least one row is required", nameof(rows));
            }

            if (Rows.Any(x => x == null))
            {
                throw new ArgumentException("Row cannot be null.", nameof(rows));
            }
        }

        public IEnumerable<ValueSourceRow> Rows { get; }

        public Type ClrType { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ValuesSource, TResult>(this);
    }
}