using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Sources
{
    public sealed class AliasedSourceItem : INamedSource
    {
        public AliasedSourceItem(ISource source, IEnumerable<ColumnAlias> columnAliases = null, string alias = null)
        {
            Source = source ?? throw new ArgumentNullException(nameof(source));
            ColumnAliases = columnAliases?.ToArray();
            Alias = alias;

            if (ColumnAliases != null && ColumnAliases.Any(x => x == null))
            {
                throw new ArgumentException("Column aliases cannot be null.", nameof(columnAliases));
            }
        }

        public string Alias { get; set; }

        public IEnumerable<ColumnAlias> ColumnAliases { get; }

        public ISource Source { get; }

        public ElementName Name => new ElementName(Alias);

        public Type ClrType => Source.ClrType;

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<AliasedSourceItem, TResult>(this);
    }
}