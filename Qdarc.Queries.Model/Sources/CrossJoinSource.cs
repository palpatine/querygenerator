using System;
using System.Collections.Generic;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Sources
{
    public sealed class CrossJoinSource : IJoinSource
    {
        public CrossJoinSource(ISource left, ISource right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
        }

        public ISource Left { get; }

        public ISource Right { get; }

        public Type ClrType => typeof(Tuple<,>).MakeGenericType(Left.ClrType, Right.ClrType);

        public IEnumerable<ISource> Sources
        {
            get
            {
                var sources = new List<ISource>();
                if (Left is IMultisource left)
                {
                    sources.AddRange(left.Sources);
                }
                else
                {
                    sources.Add(Left);
                }

                if (Right is IMultisource right)
                {
                    sources.AddRange(right.Sources);
                }
                else
                {
                    sources.Add(Right);
                }

                return sources;
            }
        }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<CrossJoinSource, TResult>(this);
    }
}