using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Sources
{
    public sealed class RightJoinSource : ConditionalJoinSource
    {
        public RightJoinSource(ISource left, ISource right, ISqlLogicalExpression @on)
            : base(left, right, @on)
        {
        }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<RightJoinSource, TResult>(this);
    }
}