using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Sources
{
    public interface IJoinSource : IMultisource
    {
        ISource Left { get; }

        ISource Right { get; }
    }
}