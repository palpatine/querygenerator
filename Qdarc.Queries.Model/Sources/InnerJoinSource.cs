using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Sources
{
    public sealed class InnerJoinSource : ConditionalJoinSource
    {
        public InnerJoinSource(ISource left, ISource right, ISqlLogicalExpression @on)
            : base(left, right, @on)
        {
        }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<InnerJoinSource, TResult>(this);
    }
}