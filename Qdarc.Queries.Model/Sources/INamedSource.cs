﻿using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Sources
{
    public interface INamedSource : ISource
    {
        ElementName Name { get; }
    }
}