using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public interface IDirectlyMaterialized
    {
        Type ClrType { get; }
    }
}