﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Declarations;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IServiceCollection registrar)
        {
            registrar.AddTransient<IVisitor, Visitor>();

            registrar.AddSingleton<IVisitationHandler<SelectCommand, SelectCommand>, SelectCommandVisitor>();
            registrar.AddSingleton<IVisitationHandler<CurrentDateAndTimeExpression, CurrentDateAndTimeExpression>, CurrentDateAndTimeExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<CurrentDateExpression, CurrentDateExpression>, CurrentDateExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<DateAndTimeMaxValueExpression, DateAndTimeMaxValueExpression>, DateAndTimeMaxValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<DateAndTimeMinValueExpression, DateAndTimeMinValueExpression>, DateAndTimeMinValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<LinqParameterReference, LinqParameterReference>, LinqParameterReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<NegateValueExpression, NegateValueExpression>, NegateValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<ScalarFunctionCall, ScalarFunctionCall>, ScalarFunctionCallVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlCallExpression, SqlCallExpression>, SqlCallExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlCaseExpression, SqlCaseExpression>, SqlCaseExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlCaseSwitchExpression, SqlCaseSwitchExpression>, SqlCaseSwitchExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlCastExpression, SqlCastExpression>, SqlCastExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlCoalesceExpression, SqlCoalesceExpression>, SqlCoalesceExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlFormatExpression, SqlFormatExpression>, SqlFormatExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlInlineIfExpression, SqlInlineIfExpression>, SqlInlineIfExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlNullExpression, SqlNullExpression>, SqlNullExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlUnaryMinusExpression, SqlUnaryMinusExpression>, SqlUnaryMinusExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlUnaryPlusExpression, SqlUnaryPlusExpression>, SqlUnaryPlusExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlVariable, SqlVariable>, SqlVariableVisitor>();
            registrar.AddSingleton<IVisitationHandler<AddValueExpression, AddValueExpression>, AddValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<BitwiseAndValueExpression, BitwiseAndValueExpression>, BitwiseAndValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<BitwiseExclusiveOrValueExpression, BitwiseExclusiveOrValueExpression>, BitwiseExclusiveOrValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<BitwiseOrValueExpression, BitwiseOrValueExpression>, BitwiseOrValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<LinqColumnReference, LinqColumnReference>, LinqColumnReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<LinqColumnChainReference, LinqColumnChainReference>, LinqColumnChainReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<DivideValueExpression, DivideValueExpression>, DivideValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<ModuloValueExpression, ModuloValueExpression>, ModuloValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SubtractValueExpression, SubtractValueExpression>, SubtractValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<MultiplyValueExpression, MultiplyValueExpression>, MultiplyValueExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlAsteriskCountBigExpression, SqlAsteriskCountBigExpression>, SqlAsteriskCountBigExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlAsteriskCountExpression, SqlAsteriskCountExpression>, SqlAsteriskCountExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlColumnReference, SqlColumnReference>, SqlColumnReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlCountBigExpression, SqlCountBigExpression>, SqlCountBigExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlCountExpression, SqlCountExpression>, SqlCountExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<ComputedColumnDeclaration, ComputedColumnDeclaration>, ComputedColumnDeclarationVisitor>();
            registrar.AddSingleton<IVisitationHandler<ColumnDeclaration, ColumnDeclaration>, ColumnDeclarationVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlConvertExpression, SqlConvertExpression>, SqlConvertExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlConstantExpression, SqlConstantExpression>, SqlConstantExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<NotLessThanLogicalExpression, NotLessThanLogicalExpression>, NotLessThanLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<NotGreaterThanLogicalExpression, NotGreaterThanLogicalExpression>, NotGreaterThanLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<NotEqualLogicalExpression, NotEqualLogicalExpression>, NotEqualLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<NotEqualIsoLogicalExpression, NotEqualIsoLogicalExpression>, NotEqualIsoLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<LessThanOrEqualLogicalExpression, LessThanOrEqualLogicalExpression>, LessThanOrEqualLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<LessThanLogicalExpression, LessThanLogicalExpression>, LessThanLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<GreaterThanOrEqualLogicalExpression, GreaterThanOrEqualLogicalExpression>, GreaterThanOrEqualLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<GreaterThanLogicalExpression, GreaterThanLogicalExpression>, GreaterThanLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<EqualLogicalExpression, EqualLogicalExpression>, EqualLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlLogicalNegateExpression, SqlLogicalNegateExpression>, SqlLogicalNegateExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlIsNullLogicalExpression, SqlIsNullLogicalExpression>, SqlIsNullLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlInLogicalExpression, SqlInLogicalExpression>, SqlInLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<OrLogicalConjunctionExpression, OrLogicalConjunctionExpression>, OrLogicalConjunctionExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<NegateLogicalExpression, NegateLogicalExpression>, NegateLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<ExistsExpression, ExistsExpression>, ExistsExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<ExclusiveOrLogicalConjunctionExpression, ExclusiveOrLogicalConjunctionExpression>, ExclusiveOrLogicalConjunctionExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<BetweenLogicalExpression, BetweenLogicalExpression>, BetweenLogicalExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<AndLogicalConjunctionExpression, AndLogicalConjunctionExpression>, AndLogicalConjunctionExpressionVisitor>();
            registrar.AddSingleton<IVisitationHandler<ProjectionByConstructor, ProjectionByConstructor>, ProjectionByConstructorVisitor>();
            registrar.AddSingleton<IVisitationHandler<ProjectionByMembersAssignment, ProjectionByMembersAssignment>, ProjectionByMembersAssignmentVisitor>();
            registrar.AddSingleton<IVisitationHandler<SqlSelection, SqlSelection>, SqlSelectionVisitor>();
            registrar.AddSingleton<IVisitationHandler<LinqTableParameterReference, LinqTableParameterReference>, LinqTableParameterReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<AliasedSourceItem, AliasedSourceItem>, AliasedSourceItemVisitor>();
            registrar.AddSingleton<IVisitationHandler<CrossJoinSource, CrossJoinSource>, CrossJoinSourceVisitor>();
            registrar.AddSingleton<IVisitationHandler<FullJoinSource, FullJoinSource>, FullJoinSourceVisitor>();
            registrar.AddSingleton<IVisitationHandler<InnerJoinSource, InnerJoinSource>, InnerJoinSourceVisitor>();
            registrar.AddSingleton<IVisitationHandler<LeftJoinSource, LeftJoinSource>, LeftJoinSourceVisitor>();
            registrar.AddSingleton<IVisitationHandler<RightJoinSource, RightJoinSource>, RightJoinSourceVisitor>();
            registrar.AddSingleton<IVisitationHandler<TableReference, TableReference>, TableReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<DeletedTableReference, DeletedTableReference>, DeletedTableReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<InsertedTableReference, InsertedTableReference>, InsertedTableReferenceVisitor>();
            registrar.AddSingleton<IVisitationHandler<TabularFunctionCall, TabularFunctionCall>, TabularFunctionCallVisitor>();
            registrar.AddSingleton<IVisitationHandler<UnionAllSelectionCommand, UnionAllSelectionCommand>, UnionAllSelectionCommandVisitor>();
            registrar.AddSingleton<IVisitationHandler<UnionSelectionCommand, UnionSelectionCommand>, UnionSelectionCommandVisitor>();
            registrar.AddSingleton<IVisitationHandler<ValuesSource, ValuesSource>, ValuesSourceVisitor>();
            registrar.AddSingleton<IVisitationHandler<ElementNameValueAssociation, ElementNameValueAssociation>, ElementNameValueAssociationVisitor>();
            registrar.AddSingleton<IVisitationHandler<UpdateCommand, UpdateCommand>, UpdateCommandVisitor>();
            registrar.AddSingleton<IVisitationHandler<DeleteCommand, DeleteCommand>, DeleteCommandVisitor>();
        }
    }
}
