﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Linq.Projections
{
    public sealed class ProjectionByConstructor : ISqlSelection
    {
        public ProjectionByConstructor(ConstructorInfo constructor, IEnumerable<ISelectionItem> elements)
        {
            Constructor = constructor ?? throw new ArgumentNullException(nameof(constructor));
            var selectionItems = (elements ?? throw new ArgumentNullException(nameof(elements))) as ISelectionItem[] ?? elements.ToArray();
            Elements = selectionItems;
            if (selectionItems.Any(x => x == null))
            {
                throw new ArgumentException("Selection item cannot be null.", nameof(elements));
            }

            if (constructor.GetParameters().Length != selectionItems.Length)
            {
                throw new InvalidModelException(
                    "Constructor projection must be supplied with appropriate number of arguments.",
                    this);
            }
        }

        public ConstructorInfo Constructor { get; }

        public IEnumerable<ISelectionItem> Elements { get; }

        public Type ClrType => Constructor.DeclaringType;

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<ProjectionByConstructor, TResult>(this);
    }
}