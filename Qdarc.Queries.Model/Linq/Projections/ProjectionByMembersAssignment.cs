using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Linq.Projections
{
    public sealed class ProjectionByMembersAssignment : ISqlSelection
    {
        public ProjectionByMembersAssignment(
            ProjectionByConstructor constructorCall,
            IEnumerable<BoundSelectionItem> bindings)
        {
            ConstructorCall = constructorCall ?? throw new ArgumentNullException(nameof(constructorCall));
            Bindings = (bindings ?? throw new ArgumentNullException(nameof(bindings))).ToArray();
            if (Bindings.Any(x => x == null))
            {
                throw new ArgumentException("Binding cannot be null", nameof(bindings));
            }
        }

        public IEnumerable<BoundSelectionItem> Bindings { get; }

        public ProjectionByConstructor ConstructorCall { get; }

        public IEnumerable<ISelectionItem> Elements => Bindings;

        public Type ClrType => ConstructorCall.ClrType;

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<ProjectionByMembersAssignment, TResult>(this);
    }
}