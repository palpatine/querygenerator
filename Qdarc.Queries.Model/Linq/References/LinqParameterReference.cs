﻿using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Linq.References
{
    public sealed class LinqParameterReference : ISqlValueExpression, IParameter
    {
        public LinqParameterReference(Expression expression, Type clrType, ISqlType sqlType)
        {
            Expression = expression ?? throw new ArgumentNullException(nameof(expression));
            ClrType = clrType;
            SqlType = sqlType;
        }

        public Expression Expression { get; }

        public string Name { get; set; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; set; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<LinqParameterReference, TResult>(this);
    }
}