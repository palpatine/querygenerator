using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Linq.References
{
    public interface IParameter : ISqlExpression
    {
        string Name { get; set; }
    }
}