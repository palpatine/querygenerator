﻿using System;
using System.Linq.Expressions;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Linq.References
{
    public sealed class LinqTableParameterReference : ISqlValueExpression, ISource, IParameter
    {
        public LinqTableParameterReference(Expression expression, Type clrType, ISqlType sqlType, string name = null)
        {
            Expression = expression ?? throw new ArgumentNullException(nameof(expression));
            ClrType = clrType ?? throw new ArgumentNullException(nameof(clrType));
            SqlType = sqlType ?? throw new ArgumentNullException(nameof(sqlType));
            Name = name;
        }

        public Expression Expression { get; }

        public string Name { get; set; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; set; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<LinqTableParameterReference, TResult>(this);
    }
}