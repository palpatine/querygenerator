using System;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Linq.References
{
    public sealed class LinqColumnReference : IColumnReference
    {
        public LinqColumnReference(INamedSource source, PropertyInfo property, Type clrType, ISqlType sqlType)
        {
            ClrType = clrType;
            SqlType = sqlType;
            Source = source ?? throw new ArgumentNullException(nameof(source));
            Property = property ?? throw new ArgumentNullException(nameof(property));
        }

        public PropertyInfo Property { get; }

        public INamedSource Source { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<LinqColumnReference, TResult>(this);
    }
}