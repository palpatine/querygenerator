using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Linq.References
{
    public sealed class LinqColumnChainReference : IColumnReference
    {
        public LinqColumnChainReference(INamedSource source, IEnumerable<PropertyInfo> properties, Type clrType, ISqlType sqlType)
        {
            ClrType = clrType;
            SqlType = sqlType;
            Source = source ?? throw new ArgumentNullException(nameof(source));
            Properties = properties?.ToArray() ?? throw new ArgumentNullException(nameof(properties));

            if (!Properties.Any())
            {
                throw new ArgumentException("There must be at least one property.", nameof(properties));
            }
        }

        public IEnumerable<PropertyInfo> Properties { get; }

        public INamedSource Source { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<LinqColumnChainReference, TResult>(this);
    }
}