﻿using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation
{
    public interface IVisitationHandler<in TIn, out TOut> : ITransformer<TIn, TOut, IVisitor>, IVisitationHandler
    {
    }
}