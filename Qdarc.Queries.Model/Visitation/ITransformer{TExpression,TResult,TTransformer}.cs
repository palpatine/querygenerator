namespace Qdarc.Queries.Model.Visitation
{
    public interface ITransformer<in TExpression, out TResult, TTransformer>
        where TTransformer : ITransformer
    {
        TResult Handle(TExpression expression, TTransformer transformer);
    }
}