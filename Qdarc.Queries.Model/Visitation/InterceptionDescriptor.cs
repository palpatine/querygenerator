using System;
using System.Reflection;

namespace Qdarc.Queries.Model.Visitation
{
    internal class InterceptionDescriptor
    {
        public Type In { get; set; }

        public Type Out { get; set; }

        public IVisitationHandler Handler { get; set; }
        public TypeInfo InTypeInfo { get; set; }
        public TypeInfo OutTypeInfo { get; set; }
    }
}