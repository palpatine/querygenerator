using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Model.Visitation
{
    internal sealed class Visitor : IVisitor
    {
        private static readonly MethodInfo ExecuteInterceptorMethod =
            ExpressionExtensions.GetMethod(
                (Visitor v) => v.ExecuteInterceptor<ISqlValueExpression, ISqlValueExpression>(null, null))
                .GetGenericMethodDefinition();

        private readonly IServiceProvider _resolver;

        private readonly List<InterceptionDescriptor> _interceptors = new List<InterceptionDescriptor>();

        public Visitor(IServiceProvider resolver)
        {
            _resolver = resolver;
        }

        public void Intercept<TIn, TOut>(IVisitationHandler<TIn, TOut> handler)
            where TIn : TOut
            where TOut : ISqlExpression
        {
            var descriptor = new InterceptionDescriptor
            {
                In = typeof(TIn),
                InTypeInfo = typeof(TIn).GetTypeInfo(),
                Out = typeof(TOut),
                OutTypeInfo = typeof(TOut).GetTypeInfo(),
                Handler = handler
            };
            _interceptors.Add(descriptor);
        }

        TResult ITransformer.Handle<TExpression, TResult>(TExpression expression)
        {
            var expressionType = typeof(TExpression);
            var resultType = typeof(TResult);
            if (!typeof(ISqlExpression).GetTypeInfo().IsAssignableFrom(resultType.GetTypeInfo())
                || !(expression is ISqlExpression))
            {
                throw new InvalidOperationException(
                    $"Expression and result must be derived from {typeof(ISqlExpression).FullName}, expression: {expressionType.FullName}, result: {resultType.FullName}.");
            }

            var expressionTypeInfo = expressionType.GetTypeInfo();
            var resultTypeInfo = resultType.GetTypeInfo();

            foreach (var interceptionDescriptor in _interceptors)
            {
                if (interceptionDescriptor.InTypeInfo.IsAssignableFrom(expressionTypeInfo)
                    && resultTypeInfo.IsAssignableFrom(interceptionDescriptor.OutTypeInfo))
                {
                    var result = ExecuteInterceptorMethod
                        .MakeGenericMethod(interceptionDescriptor.In, interceptionDescriptor.Out)
                        .Invoke(this, new object[] { interceptionDescriptor.Handler, expression });

                    if (!ReferenceEquals(result, expression))
                    {
                        return (TResult)result;
                    }
                }
            }

            var handler = _resolver.GetService<IVisitationHandler<TExpression, TExpression>>();
            return (TResult)(object)handler.Handle(expression, this);
        }

        private TOut ExecuteInterceptor<TIn, TOut>(IVisitationHandler<TIn, TOut> handler, TIn expression)
        {
            return handler.Handle(expression, this);
        }
    }
}