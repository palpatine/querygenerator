using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation
{
    public interface IVisitor : ITransformer
    {
        void Intercept<TIn, TOut>(IVisitationHandler<TIn, TOut> handler)
            where TIn : TOut
            where TOut : ISqlExpression;
    }
}