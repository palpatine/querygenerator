namespace Qdarc.Queries.Model.Visitation
{
    public interface ITransformer
    {
        TResult Handle<TExpression, TResult>(TExpression expression);
    }
}