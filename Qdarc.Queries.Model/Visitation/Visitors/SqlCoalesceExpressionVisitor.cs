using System.Linq;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlCoalesceExpressionVisitor : IVisitationHandler<SqlCoalesceExpression, SqlCoalesceExpression>
    {
        public SqlCoalesceExpression Handle(SqlCoalesceExpression expression, IVisitor transformer)
        {
            return new SqlCoalesceExpression(
                expression.Expressions?.Select(x => x.Accept<ISqlValueExpression>(transformer)).ToArray(),
                expression.ClrType,
                expression.SqlType);
        }
    }
}