using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlAsteriskCountExpressionVisitor : IVisitationHandler<SqlAsteriskCountExpression, SqlAsteriskCountExpression>
    {
        public SqlAsteriskCountExpression Handle(SqlAsteriskCountExpression expression, IVisitor transformer)
        {
            return new SqlAsteriskCountExpression(
                expression.ClrType,
                expression.SqlType);
        }
    }
}