﻿using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ElementNameValueAssociationVisitor : IVisitationHandler<ElementNameValueAssociation, ElementNameValueAssociation>
    {
        public ElementNameValueAssociation Handle(ElementNameValueAssociation expression, IVisitor transformer)
        {
            return new ElementNameValueAssociation(
                new ElementName(expression.ElementName),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}