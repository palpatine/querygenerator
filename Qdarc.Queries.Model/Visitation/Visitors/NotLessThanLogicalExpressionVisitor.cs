using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class NotLessThanLogicalExpressionVisitor : IVisitationHandler<NotLessThanLogicalExpression, NotLessThanLogicalExpression>
    {
        public NotLessThanLogicalExpression Handle(NotLessThanLogicalExpression expression, IVisitor transformer)
        {
            return new NotLessThanLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}