using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlFormatExpressionVisitor : IVisitationHandler<SqlFormatExpression, SqlFormatExpression>
    {
        public SqlFormatExpression Handle(SqlFormatExpression expression, IVisitor transformer)
        {
            return new SqlFormatExpression(
                expression.Expression.Accept<ISqlValueExpression>(transformer),
                expression.Format,
                expression.FormatProvider,
                expression.ClrType,
                expression.SqlType);
        }
    }
}