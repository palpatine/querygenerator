using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class LessThanLogicalExpressionVisitor : IVisitationHandler<LessThanLogicalExpression, LessThanLogicalExpression>
    {
        public LessThanLogicalExpression Handle(LessThanLogicalExpression expression, IVisitor transformer)
        {
            return new LessThanLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}