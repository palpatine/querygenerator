using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlColumnReferenceVisitor : IVisitationHandler<SqlColumnReference, SqlColumnReference>
    {
        public SqlColumnReference Handle(SqlColumnReference expression, IVisitor transformer)
        {
            return new SqlColumnReference(
                new ElementName(expression.ColumnName),
                expression.Source?.Accept<ISource>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}