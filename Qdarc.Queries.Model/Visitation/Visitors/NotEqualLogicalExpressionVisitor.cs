using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class NotEqualLogicalExpressionVisitor : IVisitationHandler<NotEqualLogicalExpression, NotEqualLogicalExpression>
    {
        public NotEqualLogicalExpression Handle(NotEqualLogicalExpression expression, IVisitor transformer)
        {
            return new NotEqualLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}