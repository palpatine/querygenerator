using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class NotGreaterThanLogicalExpressionVisitor : IVisitationHandler<NotGreaterThanLogicalExpression, NotGreaterThanLogicalExpression>
    {
        public NotGreaterThanLogicalExpression Handle(NotGreaterThanLogicalExpression expression, IVisitor transformer)
        {
            return new NotGreaterThanLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}