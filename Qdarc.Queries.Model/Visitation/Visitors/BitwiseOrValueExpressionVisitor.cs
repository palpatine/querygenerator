using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class BitwiseOrValueExpressionVisitor : IVisitationHandler<BitwiseOrValueExpression, BitwiseOrValueExpression>
    {
        public BitwiseOrValueExpression Handle(BitwiseOrValueExpression expression, IVisitor transformer)
        {
            return new BitwiseOrValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}