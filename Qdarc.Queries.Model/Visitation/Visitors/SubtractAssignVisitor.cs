using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SubtractAssignVisitor : IVisitationHandler<SubtractAssign, SubtractAssign>
    {
        public SubtractAssign Handle(SubtractAssign expression, IVisitor transformer)
        {
            return new SubtractAssign(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}