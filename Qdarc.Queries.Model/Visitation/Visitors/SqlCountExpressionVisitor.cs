using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlCountExpressionVisitor : IVisitationHandler<SqlCountExpression, SqlCountExpression>
    {
        public SqlCountExpression Handle(SqlCountExpression expression, IVisitor transformer)
        {
            return new SqlCountExpression(
                expression.Value?.Accept<ISqlValueExpression>(transformer),
                expression.IsDistinct,
                expression.ClrType,
                expression.SqlType);
        }
    }
}