using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class MultiplyAssignVisitor : IVisitationHandler<MultiplyAssign, MultiplyAssign>
    {
        public MultiplyAssign Handle(MultiplyAssign expression, IVisitor transformer)
        {
            return new MultiplyAssign(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}