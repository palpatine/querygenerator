﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class AssignVisitor : IVisitationHandler<Assign, Assign>
    {
        public Assign Handle(Assign expression, IVisitor transformer)
        {
            return new Assign(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}
