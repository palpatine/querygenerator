using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Declarations;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ComputedColumnDeclarationVisitor : IVisitationHandler<ComputedColumnDeclaration, ComputedColumnDeclaration>
    {
        public ComputedColumnDeclaration Handle(ComputedColumnDeclaration expression, IVisitor transformer)
        {
            return new ComputedColumnDeclaration
            {
                Check = expression.Check?.Accept<CheckConstraint>(transformer),
                ClrType = expression.ClrType,
                Name = expression.Name,
                SqlType = expression.SqlType,
                DefaultValue = expression.DefaultValue?.Accept<DefaultConstraint>(transformer),
                Index = expression.Index?.Accept<IIndexConstraint>(transformer),
                PrimaryKey = expression.PrimaryKey?.Accept<IIndexConstraint>(transformer),
                TableReference = expression.TableReference?.Accept<ITableReference>(transformer),
                Expression = expression.Expression?.Accept<ISqlValueExpression>(transformer),
                Persisted = expression.Persisted?.Accept<PersistedConstraint>(transformer)
            };
        }
    }
}