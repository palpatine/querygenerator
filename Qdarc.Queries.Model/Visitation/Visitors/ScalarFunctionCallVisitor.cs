using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ScalarFunctionCallVisitor : IVisitationHandler<ScalarFunctionCall, ScalarFunctionCall>
    {
        public ScalarFunctionCall Handle(ScalarFunctionCall expression, IVisitor transformer)
        {
            return new ScalarFunctionCall(
                new ElementName(expression.Name),
                expression.Arguments?.Select(x => x.Accept<ISqlValueExpression>(transformer)).ToArray(),
                expression.ClrType,
                expression.SqlType);
        }
    }
}