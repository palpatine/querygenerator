using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlConstantExpressionVisitor : IVisitationHandler<SqlConstantExpression, SqlConstantExpression>
    {
        public SqlConstantExpression Handle(SqlConstantExpression expression, IVisitor transformer)
        {
            return new SqlConstantExpression(
                expression.Value,
                expression.ClrType,
                expression.SqlType);
        }
    }
}