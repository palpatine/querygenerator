using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class TableReferenceVisitor : IVisitationHandler<TableReference, TableReference>
    {
        public TableReference Handle(TableReference expression, IVisitor transformer)
        {
            return new TableReference(
                expression.Name != null ? new ElementName(expression.Name) : null,
                expression.ClrType);
        }
    }
}