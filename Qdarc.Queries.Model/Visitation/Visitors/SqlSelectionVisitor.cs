using System.Linq;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlSelectionVisitor : IVisitationHandler<SqlSelection, SqlSelection>
    {
        public SqlSelection Handle(SqlSelection expression, IVisitor transformer)
        {
            return new SqlSelection(
                expression.Elements?.Select(x => x.Accept<ISelectionItem>(transformer)).ToArray(),
                expression.ClrType);
        }
    }
}