using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ModuloValueExpressionVisitor : IVisitationHandler<ModuloValueExpression, ModuloValueExpression>
    {
        public ModuloValueExpression Handle(ModuloValueExpression expression, IVisitor transformer)
        {
            return new ModuloValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}