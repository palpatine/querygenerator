using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class LinqColumnReferenceVisitor : IVisitationHandler<LinqColumnReference, LinqColumnReference>
    {
        public LinqColumnReference Handle(LinqColumnReference expression, IVisitor transformer)
        {
            return new LinqColumnReference(
                expression.Source.Accept<INamedSource>(transformer),
                expression.Property,
                expression.ClrType,
                expression.SqlType);
        }
    }
}