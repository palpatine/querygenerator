using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class LeftJoinSourceVisitor : IVisitationHandler<LeftJoinSource, LeftJoinSource>
    {
        public LeftJoinSource Handle(LeftJoinSource expression, IVisitor transformer)
        {
            return new LeftJoinSource(
                expression.Left.Accept<ISource>(transformer),
                expression.Right.Accept<ISource>(transformer),
                expression.On.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}