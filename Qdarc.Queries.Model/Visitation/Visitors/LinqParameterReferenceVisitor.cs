using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class LinqParameterReferenceVisitor : IVisitationHandler<LinqParameterReference, LinqParameterReference>
    {
        public LinqParameterReference Handle(LinqParameterReference expression, IVisitor transformer)
        {
            return new LinqParameterReference(
                expression.Expression,
                expression.ClrType,
                expression.SqlType);
        }
    }
}