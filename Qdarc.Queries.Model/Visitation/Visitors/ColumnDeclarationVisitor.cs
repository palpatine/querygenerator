using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Declarations;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ColumnDeclarationVisitor : IVisitationHandler<ColumnDeclaration, ColumnDeclaration>
    {
        public ColumnDeclaration Handle(ColumnDeclaration expression, IVisitor transformer)
        {
            return new ColumnDeclaration
            {
                Check = expression.Check?.Accept<CheckConstraint>(transformer),
                ClrType = expression.ClrType,
                Name = expression.Name,
                SqlType = expression.SqlType,
                DefaultValue = expression.DefaultValue?.Accept<DefaultConstraint>(transformer),
                Identity = expression.Identity?.Accept<IdentityDeclaration>(transformer),
                NullableValue = expression.NullableValue,
                Index = expression.Index?.Accept<IIndexConstraint>(transformer),
                PrimaryKey = expression.PrimaryKey?.Accept<IIndexConstraint>(transformer),
                TableReference = expression.TableReference?.Accept<ITableReference>(transformer)
            };
        }
    }
}