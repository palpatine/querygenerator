using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class CurrentDateAndTimeExpressionVisitor : IVisitationHandler<CurrentDateAndTimeExpression, CurrentDateAndTimeExpression>
    {
        public CurrentDateAndTimeExpression Handle(CurrentDateAndTimeExpression expression, IVisitor transformer)
        {
            return new CurrentDateAndTimeExpression(expression.IsUtc, expression.SqlType);
        }
    }
}