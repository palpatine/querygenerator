using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class BetweenLogicalExpressionVisitor : IVisitationHandler<BetweenLogicalExpression, BetweenLogicalExpression>
    {
        public BetweenLogicalExpression Handle(BetweenLogicalExpression expression, IVisitor transformer)
        {
            return new BetweenLogicalExpression(
                expression.Value?.Accept<ISqlValueExpression>(transformer),
                expression.LowerBound?.Accept<ISqlValueExpression>(transformer),
                expression.UpperBound?.Accept<ISqlValueExpression>(transformer),
                expression.IsNegated);
        }
    }
}