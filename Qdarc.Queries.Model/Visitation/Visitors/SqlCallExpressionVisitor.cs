using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlCallExpressionVisitor : IVisitationHandler<SqlCallExpression, SqlCallExpression>
    {
        public SqlCallExpression Handle(SqlCallExpression expression, IVisitor transformer)
        {
            return new SqlCallExpression(
                new ElementName(expression.Name),
                expression.Arguments?.Select(x => x.Accept<ISqlValueExpression>(transformer)).ToArray(),
                expression.ClrType,
                expression.SqlType);
        }
    }
}