using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class UnionAllSelectionCommandVisitor : IVisitationHandler<UnionAllSelectionCommand, UnionAllSelectionCommand>
    {
        public UnionAllSelectionCommand Handle(UnionAllSelectionCommand expression, IVisitor transformer)
        {
            return new UnionAllSelectionCommand(
                expression.Left?.Accept<ISelectCommand>(transformer),
                expression.Right?.Accept<ISelectCommand>(transformer));
        }
    }
}