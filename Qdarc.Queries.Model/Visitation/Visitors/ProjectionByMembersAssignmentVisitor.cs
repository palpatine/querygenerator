using System.Linq;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ProjectionByMembersAssignmentVisitor : IVisitationHandler<ProjectionByMembersAssignment, ProjectionByMembersAssignment>
    {
        public ProjectionByMembersAssignment Handle(ProjectionByMembersAssignment expression, IVisitor transformer)
        {
            return new ProjectionByMembersAssignment(
                expression.ConstructorCall.Accept<ProjectionByConstructor>(transformer),
                expression.Bindings.Select(x => x.Accept<BoundSelectionItem>(transformer)).ToArray());
        }
    }
}