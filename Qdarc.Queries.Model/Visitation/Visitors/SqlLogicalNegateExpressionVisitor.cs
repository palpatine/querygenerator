using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlLogicalNegateExpressionVisitor : IVisitationHandler<SqlLogicalNegateExpression, SqlLogicalNegateExpression>
    {
        public SqlLogicalNegateExpression Handle(SqlLogicalNegateExpression expression, IVisitor transformer)
        {
            return new SqlLogicalNegateExpression(
                expression.Value?.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}