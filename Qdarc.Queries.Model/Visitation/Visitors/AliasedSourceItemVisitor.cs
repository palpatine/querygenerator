using System.Linq;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class AliasedSourceItemVisitor : IVisitationHandler<AliasedSourceItem, AliasedSourceItem>
    {
        public AliasedSourceItem Handle(AliasedSourceItem expression, IVisitor transformer)
        {
            return new AliasedSourceItem(
                expression.Source.Accept<ISource>(transformer),
                expression.ColumnAliases?.Select(x => x.Accept<ColumnAlias>(transformer)).ToArray(),
                expression.Alias);
        }
    }
}