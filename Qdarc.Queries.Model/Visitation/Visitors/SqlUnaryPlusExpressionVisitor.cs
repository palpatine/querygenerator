using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlUnaryPlusExpressionVisitor : IVisitationHandler<SqlUnaryPlusExpression, SqlUnaryPlusExpression>
    {
        public SqlUnaryPlusExpression Handle(SqlUnaryPlusExpression expression, IVisitor transformer)
        {
            return new SqlUnaryPlusExpression(
                expression.Value?.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}