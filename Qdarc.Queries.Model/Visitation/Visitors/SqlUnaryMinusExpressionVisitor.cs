using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlUnaryMinusExpressionVisitor : IVisitationHandler<SqlUnaryMinusExpression, SqlUnaryMinusExpression>
    {
        public SqlUnaryMinusExpression Handle(SqlUnaryMinusExpression expression, IVisitor transformer)
        {
            return new SqlUnaryMinusExpression(
                expression.Value?.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}