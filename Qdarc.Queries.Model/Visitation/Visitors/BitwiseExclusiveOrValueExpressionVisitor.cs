using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class BitwiseExclusiveOrValueExpressionVisitor : IVisitationHandler<BitwiseExclusiveOrValueExpression, BitwiseExclusiveOrValueExpression>
    {
        public BitwiseExclusiveOrValueExpression Handle(BitwiseExclusiveOrValueExpression expression, IVisitor transformer)
        {
            return new BitwiseExclusiveOrValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}