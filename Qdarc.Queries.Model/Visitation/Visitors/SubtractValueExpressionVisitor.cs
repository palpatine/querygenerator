using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SubtractValueExpressionVisitor : IVisitationHandler<SubtractValueExpression, SubtractValueExpression>
    {
        public SubtractValueExpression Handle(SubtractValueExpression expression, IVisitor transformer)
        {
            return new SubtractValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}