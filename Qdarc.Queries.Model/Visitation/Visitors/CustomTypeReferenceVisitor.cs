using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class CustomTypeReferenceVisitor : IVisitationHandler<CustomTypeReference, CustomTypeReference>
    {
        public CustomTypeReference Handle(CustomTypeReference expression, IVisitor transformer)
        {
            return new CustomTypeReference(
                new ElementName(expression.Name));
        }
    }
}