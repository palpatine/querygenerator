using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class CrossJoinSourceVisitor : IVisitationHandler<CrossJoinSource, CrossJoinSource>
    {
        public CrossJoinSource Handle(CrossJoinSource expression, IVisitor transformer)
        {
            return new CrossJoinSource(
                expression.Left.Accept<ISource>(transformer),
                expression.Right.Accept<ISource>(transformer));
        }
    }
}