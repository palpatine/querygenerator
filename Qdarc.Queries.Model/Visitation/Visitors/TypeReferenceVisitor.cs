using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class TypeReferenceVisitor : IVisitationHandler<TypeReference, TypeReference>
    {
        public TypeReference Handle(TypeReference expression, IVisitor transformer)
        {
            return new TypeReference(expression.Name);
        }
    }
}