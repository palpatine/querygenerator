using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class DivideAssignVisitor : IVisitationHandler<DivideAssign, DivideAssign>
    {
        public DivideAssign Handle(DivideAssign expression, IVisitor transformer)
        {
            return new DivideAssign(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}