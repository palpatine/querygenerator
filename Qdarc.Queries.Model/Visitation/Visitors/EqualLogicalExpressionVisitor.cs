using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class EqualLogicalExpressionVisitor : IVisitationHandler<EqualLogicalExpression, EqualLogicalExpression>
    {
        public EqualLogicalExpression Handle(EqualLogicalExpression expression, IVisitor transformer)
        {
            return new EqualLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}