using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlCountBigExpressionVisitor : IVisitationHandler<SqlCountBigExpression, SqlCountBigExpression>
    {
        public SqlCountBigExpression Handle(SqlCountBigExpression expression, IVisitor transformer)
        {
            return new SqlCountBigExpression(
                expression.Value?.Accept<ISqlValueExpression>(transformer),
                expression.IsDistinct,
                expression.ClrType,
                expression.SqlType);
        }
    }
}