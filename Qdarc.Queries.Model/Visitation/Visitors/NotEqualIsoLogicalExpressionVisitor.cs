using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class NotEqualIsoLogicalExpressionVisitor : IVisitationHandler<NotEqualIsoLogicalExpression, NotEqualIsoLogicalExpression>
    {
        public NotEqualIsoLogicalExpression Handle(NotEqualIsoLogicalExpression expression, IVisitor transformer)
        {
            return new NotEqualIsoLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}