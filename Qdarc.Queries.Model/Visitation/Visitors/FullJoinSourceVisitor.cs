using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class FullJoinSourceVisitor : IVisitationHandler<FullJoinSource, FullJoinSource>
    {
        public FullJoinSource Handle(FullJoinSource expression, IVisitor transformer)
        {
            return new FullJoinSource(
                expression.Left.Accept<ISource>(transformer),
                expression.Right.Accept<ISource>(transformer),
                expression.On.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}