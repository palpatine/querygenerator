using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlIsNullLogicalExpressionVisitor : IVisitationHandler<SqlIsNullLogicalExpression, SqlIsNullLogicalExpression>
    {
        public SqlIsNullLogicalExpression Handle(SqlIsNullLogicalExpression expression, IVisitor transformer)
        {
            return new SqlIsNullLogicalExpression(
                expression.Expression?.Accept<ISqlValueExpression>(transformer),
                expression.IsNegated);
        }
    }
}