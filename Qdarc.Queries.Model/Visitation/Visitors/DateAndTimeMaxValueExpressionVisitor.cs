using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class DateAndTimeMaxValueExpressionVisitor : IVisitationHandler<DateAndTimeMaxValueExpression, DateAndTimeMaxValueExpression>
    {
        public DateAndTimeMaxValueExpression Handle(DateAndTimeMaxValueExpression expression, IVisitor transformer)
        {
            return new DateAndTimeMaxValueExpression(expression.SqlType);
        }
    }
}