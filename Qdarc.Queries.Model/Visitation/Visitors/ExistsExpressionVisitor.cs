using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ExistsExpressionVisitor : IVisitationHandler<ExistsExpression, ExistsExpression>
    {
        public ExistsExpression Handle(ExistsExpression expression, IVisitor transformer)
        {
            return new ExistsExpression(
                expression.Select?.Accept<ISelectCommand>(transformer),
                expression.IsNegated);
        }
    }
}