using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class UnionSelectionCommandVisitor : IVisitationHandler<UnionSelectionCommand, UnionSelectionCommand>
    {
        public UnionSelectionCommand Handle(UnionSelectionCommand expression, IVisitor transformer)
        {
            return new UnionSelectionCommand(
                expression.Left?.Accept<ISelectCommand>(transformer),
                expression.Right?.Accept<ISelectCommand>(transformer));
        }
    }
}