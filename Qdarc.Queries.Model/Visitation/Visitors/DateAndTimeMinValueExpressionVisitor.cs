using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class DateAndTimeMinValueExpressionVisitor : IVisitationHandler<DateAndTimeMinValueExpression, DateAndTimeMinValueExpression>
    {
        public DateAndTimeMinValueExpression Handle(DateAndTimeMinValueExpression expression, IVisitor transformer)
        {
            return new DateAndTimeMinValueExpression(expression.SqlType);
        }
    }
}