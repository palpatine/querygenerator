using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class OrLogicalConjunctionExpressionVisitor : IVisitationHandler<OrLogicalConjunctionExpression, OrLogicalConjunctionExpression>
    {
        public OrLogicalConjunctionExpression Handle(OrLogicalConjunctionExpression expression, IVisitor transformer)
        {
            return new OrLogicalConjunctionExpression(
                expression.Left.Accept<ISqlLogicalExpression>(transformer),
                expression.Right.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}