using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlConvertExpressionVisitor : IVisitationHandler<SqlConvertExpression, SqlConvertExpression>
    {
        public SqlConvertExpression Handle(SqlConvertExpression expression, IVisitor transformer)
        {
            return new SqlConvertExpression(
                expression.Expression.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}