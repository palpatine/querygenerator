using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ProcedureCallVisitor : IVisitationHandler<ProcedureCall, ProcedureCall>
    {
        public ProcedureCall Handle(ProcedureCall expression, IVisitor transformer)
        {
            return new ProcedureCall(
                new ElementName(expression.Name),
                expression.Arguments.Select(x => x.Accept<ISqlValueExpression>(transformer)),
                expression.ClrType);
        }
    }
}