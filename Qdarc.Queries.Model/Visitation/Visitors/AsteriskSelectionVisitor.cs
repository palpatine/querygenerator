using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class AsteriskSelectionVisitor : IVisitationHandler<AsteriskSelection, AsteriskSelection>
    {
        public AsteriskSelection Handle(AsteriskSelection expression, IVisitor transformer)
        {
            return new AsteriskSelection(
                expression.Source?.Accept<INamedSource>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}