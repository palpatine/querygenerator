using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class AddAssignVisitor : IVisitationHandler<AddAssign, AddAssign>
    {
        public AddAssign Handle(AddAssign expression, IVisitor transformer)
        {
            return new AddAssign(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}