using System.Linq;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ProjectionByConstructorVisitor : IVisitationHandler<ProjectionByConstructor, ProjectionByConstructor>
    {
        public ProjectionByConstructor Handle(ProjectionByConstructor expression, IVisitor transformer)
        {
            return new ProjectionByConstructor(
                expression.Constructor,
                expression.Elements.Select(x => x.Accept<ISelectionItem>(transformer)).ToArray());
        }
    }
}