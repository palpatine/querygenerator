using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ModuloAssignVisitor : IVisitationHandler<ModuloAssign, ModuloAssign>
    {
        public ModuloAssign Handle(ModuloAssign expression, IVisitor transformer)
        {
            return new ModuloAssign(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}