using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class CurrentDateExpressionVisitor : IVisitationHandler<CurrentDateExpression, CurrentDateExpression>
    {
        public CurrentDateExpression Handle(CurrentDateExpression expression, IVisitor transformer)
        {
            return new CurrentDateExpression(expression.SqlType);
        }
    }
}