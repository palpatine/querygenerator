using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlCastExpressionVisitor : IVisitationHandler<SqlCastExpression, SqlCastExpression>
    {
        public SqlCastExpression Handle(SqlCastExpression expression, IVisitor transformer)
        {
            return new SqlCastExpression(
                expression.Expression?.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}