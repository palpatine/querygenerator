using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class PrefixedSelectionVisitor : IVisitationHandler<PrefixedSelection, PrefixedSelection>
    {
        public PrefixedSelection Handle(PrefixedSelection expression, IVisitor transformer)
        {
            return new PrefixedSelection(
                expression.Prefix,
                expression.Selection.Accept<ISqlSelection>(transformer));
        }
    }
}