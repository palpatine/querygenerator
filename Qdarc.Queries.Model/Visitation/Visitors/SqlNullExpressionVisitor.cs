using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlNullExpressionVisitor : IVisitationHandler<SqlNullExpression, SqlNullExpression>
    {
        public SqlNullExpression Handle(SqlNullExpression expression, IVisitor transformer)
        {
            return new SqlNullExpression(
                expression.ClrType,
                expression.SqlType);
        }
    }
}