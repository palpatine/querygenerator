using System.Linq;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ValuesSourceVisitor : IVisitationHandler<ValuesSource, ValuesSource>
    {
        public ValuesSource Handle(ValuesSource expression, IVisitor transformer)
        {
            return new ValuesSource(
                expression.Rows?.Select(x => x.Accept<ValueSourceRow>(transformer)).ToArray(),
                expression.ClrType);
        }
    }
}