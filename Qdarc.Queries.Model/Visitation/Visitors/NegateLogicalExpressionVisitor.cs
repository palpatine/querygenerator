using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class NegateLogicalExpressionVisitor : IVisitationHandler<NegateLogicalExpression, NegateLogicalExpression>
    {
        public NegateLogicalExpression Handle(NegateLogicalExpression expression, IVisitor transformer)
        {
            return new NegateLogicalExpression(
                expression.LogicalExpression?.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}