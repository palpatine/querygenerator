using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class OrderedColumnReferenceVisitor : IVisitationHandler<OrderedColumnReference, OrderedColumnReference>
    {
        public OrderedColumnReference Handle(OrderedColumnReference expression, IVisitor transformer)
        {
            return new OrderedColumnReference(
                expression.ColumnReference.Accept<IColumnReference>(transformer),
                expression.Ascending);
        }
    }
}