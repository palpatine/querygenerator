using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class LessThanOrEqualLogicalExpressionVisitor : IVisitationHandler<LessThanOrEqualLogicalExpression, LessThanOrEqualLogicalExpression>
    {
        public LessThanOrEqualLogicalExpression Handle(LessThanOrEqualLogicalExpression expression, IVisitor transformer)
        {
            return new LessThanOrEqualLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}