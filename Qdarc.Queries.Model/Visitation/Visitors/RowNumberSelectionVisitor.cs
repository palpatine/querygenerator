using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class RowNumberSelectionVisitor : IVisitationHandler<RowNumberSelection, RowNumberSelection>
    {
        public RowNumberSelection Handle(RowNumberSelection expression, IVisitor transformer)
        {
            return new RowNumberSelection(
                expression.Order.Select(x => x.Accept<OrderedValueExpression>(transformer)),
                expression.Partition?.Select(x => x.Accept<ISqlValueExpression>(transformer)),
                expression.ClrType,
                expression.SqlType);
        }
    }
}