using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class InnerJoinSourceVisitor : IVisitationHandler<InnerJoinSource, InnerJoinSource>
    {
        public InnerJoinSource Handle(InnerJoinSource expression, IVisitor transformer)
        {
            return new InnerJoinSource(
                expression.Left.Accept<ISource>(transformer),
                expression.Right.Accept<ISource>(transformer),
                expression.On.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}