using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class ExclusiveOrLogicalConjunctionExpressionVisitor : IVisitationHandler<ExclusiveOrLogicalConjunctionExpression, ExclusiveOrLogicalConjunctionExpression>
    {
        public ExclusiveOrLogicalConjunctionExpression Handle(ExclusiveOrLogicalConjunctionExpression expression, IVisitor transformer)
        {
            return new ExclusiveOrLogicalConjunctionExpression(
                expression.Left?.Accept<ISqlLogicalExpression>(transformer),
                expression.Right?.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}