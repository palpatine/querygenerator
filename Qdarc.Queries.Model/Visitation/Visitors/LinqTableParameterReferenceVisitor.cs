using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class LinqTableParameterReferenceVisitor : IVisitationHandler<LinqTableParameterReference, LinqTableParameterReference>
    {
        public LinqTableParameterReference Handle(LinqTableParameterReference expression, IVisitor transformer)
        {
            return new LinqTableParameterReference(
                expression.Expression,
                expression.ClrType,
                expression.SqlType,
                expression.Name);
        }
    }
}