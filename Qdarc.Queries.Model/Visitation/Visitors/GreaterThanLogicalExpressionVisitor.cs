using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class GreaterThanLogicalExpressionVisitor : IVisitationHandler<GreaterThanLogicalExpression, GreaterThanLogicalExpression>
    {
        public GreaterThanLogicalExpression Handle(GreaterThanLogicalExpression expression, IVisitor transformer)
        {
            return new GreaterThanLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}