using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class BitwiseAndValueExpressionVisitor : IVisitationHandler<BitwiseAndValueExpression, BitwiseAndValueExpression>
    {
        public BitwiseAndValueExpression Handle(BitwiseAndValueExpression expression, IVisitor transformer)
        {
            return new BitwiseAndValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}