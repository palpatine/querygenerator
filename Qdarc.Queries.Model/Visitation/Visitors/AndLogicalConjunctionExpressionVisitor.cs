using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class AndLogicalConjunctionExpressionVisitor : IVisitationHandler<AndLogicalConjunctionExpression, AndLogicalConjunctionExpression>
    {
        public AndLogicalConjunctionExpression Handle(AndLogicalConjunctionExpression expression, IVisitor transformer)
        {
            return new AndLogicalConjunctionExpression(
                expression.Left.Accept<ISqlLogicalExpression>(transformer),
                expression.Right.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}