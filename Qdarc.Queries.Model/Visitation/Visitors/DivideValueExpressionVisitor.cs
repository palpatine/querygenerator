using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class DivideValueExpressionVisitor : IVisitationHandler<DivideValueExpression, DivideValueExpression>
    {
        public DivideValueExpression Handle(DivideValueExpression expression, IVisitor transformer)
        {
            return new DivideValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}