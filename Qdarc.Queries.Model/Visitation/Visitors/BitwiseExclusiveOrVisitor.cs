﻿using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class BitwiseExclusiveOrVisitor : IVisitationHandler<BitwiseExclusiveOr, BitwiseExclusiveOr>
    {
        public BitwiseExclusiveOr Handle(BitwiseExclusiveOr expression, IVisitor transformer)
        {
            return new BitwiseExclusiveOr(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}