using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SelectCommandAsSelectionItemVisitor : IVisitationHandler<SelectCommandAsSelectionItem, SelectCommandAsSelectionItem>
    {
        public SelectCommandAsSelectionItem Handle(SelectCommandAsSelectionItem expression, IVisitor transformer)
        {
            return new SelectCommandAsSelectionItem(
                expression.Command.Accept<ISelectCommand>(transformer));
        }
    }
}