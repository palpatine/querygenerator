using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class DeletedTableReferenceVisitor : IVisitationHandler<DeletedTableReference, DeletedTableReference>
    {
        public DeletedTableReference Handle(DeletedTableReference expression, IVisitor transformer)
        {
            return new DeletedTableReference(expression.ClrType);
        }
    }
}