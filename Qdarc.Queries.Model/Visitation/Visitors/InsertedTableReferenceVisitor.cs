using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class InsertedTableReferenceVisitor : IVisitationHandler<InsertedTableReference, InsertedTableReference>
    {
        public InsertedTableReference Handle(InsertedTableReference expression, IVisitor transformer)
        {
            return new InsertedTableReference(expression.ClrType);
        }
    }
}