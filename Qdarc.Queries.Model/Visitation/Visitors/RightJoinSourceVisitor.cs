using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class RightJoinSourceVisitor : IVisitationHandler<RightJoinSource, RightJoinSource>
    {
        public RightJoinSource Handle(RightJoinSource expression, IVisitor transformer)
        {
            return new RightJoinSource(
                expression.Left.Accept<ISource>(transformer),
                expression.Right.Accept<ISource>(transformer),
                expression.On.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}