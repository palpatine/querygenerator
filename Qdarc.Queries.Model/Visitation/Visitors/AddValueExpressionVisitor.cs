using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class AddValueExpressionVisitor : IVisitationHandler<AddValueExpression, AddValueExpression>
    {
        public AddValueExpression Handle(AddValueExpression expression, IVisitor transformer)
        {
            return new AddValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}