﻿using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class BitwiseAddVisitor : IVisitationHandler<BitwiseAdd, BitwiseAdd>
    {
        public BitwiseAdd Handle(BitwiseAdd expression, IVisitor transformer)
        {
            return new BitwiseAdd(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}