using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class BitwiseOrVisitor : IVisitationHandler<BitwiseOr, BitwiseOr>
    {
        public BitwiseOr Handle(BitwiseOr expression, IVisitor transformer)
        {
            return new BitwiseOr(
                expression.Assignable.Accept<IAssignable>(transformer),
                expression.Value.Accept<ISqlValueExpression>(transformer));
        }
    }
}