using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlAsteriskCountBigExpressionVisitor : IVisitationHandler<SqlAsteriskCountBigExpression, SqlAsteriskCountBigExpression>
    {
        public SqlAsteriskCountBigExpression Handle(SqlAsteriskCountBigExpression expression, IVisitor transformer)
        {
            return new SqlAsteriskCountBigExpression(
                expression.ClrType,
                expression.SqlType);
        }
    }
}