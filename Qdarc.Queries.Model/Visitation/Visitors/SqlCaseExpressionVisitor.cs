using System.Linq;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlCaseExpressionVisitor : IVisitationHandler<SqlCaseExpression, SqlCaseExpression>
    {
        public SqlCaseExpression Handle(SqlCaseExpression expression, IVisitor transformer)
        {
            return new SqlCaseExpression(
                expression.Cases?.Select(x => x.Accept<SqlLogicalWhenThen>(transformer)).ToArray(),
                expression.Else?.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}