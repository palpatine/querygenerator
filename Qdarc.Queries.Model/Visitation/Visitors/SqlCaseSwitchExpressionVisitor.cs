using System.Linq;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlCaseSwitchExpressionVisitor : IVisitationHandler<SqlCaseSwitchExpression, SqlCaseSwitchExpression>
    {
        public SqlCaseSwitchExpression Handle(SqlCaseSwitchExpression expression, IVisitor transformer)
        {
            return new SqlCaseSwitchExpression(
                expression.InitialValue?.Accept<ISqlValueExpression>(transformer),
                expression.Cases?.Select(x => x.Accept<SqlWhenThen>(transformer)).ToArray(),
                expression.Else?.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}