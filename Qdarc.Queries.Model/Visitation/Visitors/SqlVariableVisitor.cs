using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlVariableVisitor : IVisitationHandler<SqlVariable, SqlVariable>
    {
        public SqlVariable Handle(SqlVariable expression, IVisitor transformer)
        {
            return new SqlVariable(
                expression.Name,
                expression.IsSystem,
                expression.ClrType,
                expression.SqlType);
        }
    }
}