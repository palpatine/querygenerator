﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class DeleteCommandVisitor : IVisitationHandler<DeleteCommand, DeleteCommand>
    {
        public DeleteCommand Handle(DeleteCommand expression, IVisitor transformer)
        {
            return new DeleteCommand(
                expression.Target.Accept<INamedSource>(transformer),
                expression.Filter?.Accept<ISqlLogicalExpression>(transformer),
                expression.Source?.Accept<ISource>(transformer));
        }
    }
}
