using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlInlineIfExpressionVisitor : IVisitationHandler<SqlInlineIfExpression, SqlInlineIfExpression>
    {
        public SqlInlineIfExpression Handle(SqlInlineIfExpression expression, IVisitor transformer)
        {
            return new SqlInlineIfExpression(
                expression.Condition?.Accept<ISqlLogicalExpression>(transformer),
                expression.IfTrue?.Accept<ISqlValueExpression>(transformer),
                expression.IfFalse?.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}