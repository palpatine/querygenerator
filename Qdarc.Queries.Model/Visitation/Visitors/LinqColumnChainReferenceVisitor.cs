using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class LinqColumnChainReferenceVisitor : IVisitationHandler<LinqColumnChainReference, LinqColumnChainReference>
    {
        public LinqColumnChainReference Handle(LinqColumnChainReference expression, IVisitor transformer)
        {
            return new LinqColumnChainReference(
                expression.Source.Accept<INamedSource>(transformer),
                expression.Properties,
                expression.ClrType,
                expression.SqlType);
        }
    }
}