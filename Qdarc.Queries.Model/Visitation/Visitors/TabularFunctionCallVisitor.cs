using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class TabularFunctionCallVisitor : IVisitationHandler<TabularFunctionCall, TabularFunctionCall>
    {
        public TabularFunctionCall Handle(TabularFunctionCall expression, IVisitor transformer)
        {
            return new TabularFunctionCall(
                new ElementName(expression.Name),
                expression.Arguments?.Select(z => z.Accept<ISqlValueExpression>(transformer)).ToArray(),
                expression.ClrType);
        }
    }
}