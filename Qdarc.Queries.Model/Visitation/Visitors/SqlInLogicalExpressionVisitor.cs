using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SqlInLogicalExpressionVisitor : IVisitationHandler<SqlInLogicalExpression, SqlInLogicalExpression>
    {
        public SqlInLogicalExpression Handle(SqlInLogicalExpression expression, IVisitor transformer)
        {
            if (expression.Set != null)
            {
                return new SqlInLogicalExpression(
                    expression.Value.Accept<ISqlValueExpression>(transformer),
                    expression.IsNegated,
                    expression.Set.Select(x => x.Accept<ISqlValueExpression>(transformer)).ToArray());
            }
            else
            {
                return new SqlInLogicalExpression(
                    expression.Value.Accept<ISqlValueExpression>(transformer),
                    expression.IsNegated,
                    expression.Selection.Accept<ISelectCommand>(transformer));
            }
        }
    }
}