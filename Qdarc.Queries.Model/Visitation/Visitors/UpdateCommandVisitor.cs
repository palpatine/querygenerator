﻿using System.Linq;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class UpdateCommandVisitor : IVisitationHandler<UpdateCommand, UpdateCommand>
    {
        public UpdateCommand Handle(UpdateCommand expression, IVisitor transformer)
        {
            return new UpdateCommand(
                expression.Target.Accept<ISource>(transformer),
                expression.Set.Select(x => x.Accept<ElementNameValueAssociation>(transformer)),
                expression.Source?.Accept<ISource>(transformer),
                expression.Top?.Accept<TopExpression>(transformer),
                expression.Filter.Accept<ISqlLogicalExpression>(transformer));
        }
    }
}