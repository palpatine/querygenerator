using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class MultiplyValueExpressionVisitor : IVisitationHandler<MultiplyValueExpression, MultiplyValueExpression>
    {
        public MultiplyValueExpression Handle(MultiplyValueExpression expression, IVisitor transformer)
        {
            return new MultiplyValueExpression(
                expression.Left.Accept<ISqlValueExpression>(transformer),
                expression.Right.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}