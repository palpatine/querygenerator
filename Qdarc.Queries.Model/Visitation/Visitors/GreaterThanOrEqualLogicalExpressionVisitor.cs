using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class GreaterThanOrEqualLogicalExpressionVisitor : IVisitationHandler<GreaterThanOrEqualLogicalExpression, GreaterThanOrEqualLogicalExpression>
    {
        public GreaterThanOrEqualLogicalExpression Handle(GreaterThanOrEqualLogicalExpression expression, IVisitor transformer)
        {
            return new GreaterThanOrEqualLogicalExpression(
                expression.Left?.Accept<ISqlValueExpression>(transformer),
                expression.Right?.Accept<ISqlValueExpression>(transformer));
        }
    }
}