using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class NegateValueExpressionVisitor : IVisitationHandler<NegateValueExpression, NegateValueExpression>
    {
        public NegateValueExpression Handle(NegateValueExpression expression, IVisitor transformer)
        {
            return new NegateValueExpression(
                expression.ValueExpression.Accept<ISqlValueExpression>(transformer),
                expression.ClrType,
                expression.SqlType);
        }
    }
}