using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class AliasedSelectionItemVisitor : IVisitationHandler<AliasedSelectionItem, AliasedSelectionItem>
    {
        public AliasedSelectionItem Handle(AliasedSelectionItem expression, IVisitor transformer)
        {
            return new AliasedSelectionItem(
                expression.Item.Accept<ISelectionItem>(transformer),
                expression.Alias);
        }
    }
}