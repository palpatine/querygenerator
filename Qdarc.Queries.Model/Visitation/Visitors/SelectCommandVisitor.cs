﻿using System.Linq;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Model.Visitation.Visitors
{
    internal class SelectCommandVisitor : IVisitationHandler<SelectCommand, SelectCommand>
    {
        public SelectCommand Handle(SelectCommand expression, IVisitor transformer)
        {
            return new SelectCommand(
                expression.Source?.Accept<ISource>(transformer),
                expression.Selection?.Accept<ISqlSelection>(transformer),
                expression.Filter?.Accept<ISqlLogicalExpression>(transformer),
                expression.IsDistinct,
                expression.Top?.Accept<TopExpression>(transformer),
                expression.Group?.Accept<GroupExpression>(transformer),
                expression.Order?.Select(z => z.Accept<OrderedValueExpression>(transformer)).ToArray(),
                expression.With?.Select(z => z.Accept<WithExpression>(transformer)).ToArray());
        }
    }
}