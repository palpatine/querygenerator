﻿using System;
using System.Collections.Generic;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlCaseExpression : ISqlValueExpression
    {
        public SqlCaseExpression(IEnumerable<SqlLogicalWhenThen> cases, ISqlValueExpression @else, Type clrType, ISqlType sqlType)
        {
            Cases = cases;
            Else = @else;
            ClrType = clrType;
            SqlType = sqlType;
        }

        public IEnumerable<SqlLogicalWhenThen> Cases { get; }

        public ISqlValueExpression Else { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlCaseExpression, TResult>(this);
    }
}