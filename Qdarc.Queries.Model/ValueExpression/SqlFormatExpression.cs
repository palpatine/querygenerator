﻿using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlFormatExpression : ISqlValueExpression
    {
        public SqlFormatExpression(ISqlValueExpression expression, string format, IFormatProvider formatProvider, Type clrType, ISqlType sqlType)
        {
            Expression = expression ?? throw new ArgumentNullException(nameof(expression));
            Format = format ?? throw new ArgumentNullException(nameof(format));
            FormatProvider = formatProvider ?? throw new ArgumentNullException(nameof(formatProvider));
            ClrType = clrType;
            SqlType = sqlType;
        }

        public ISqlValueExpression Expression { get; }

        public string Format { get; }

        public IFormatProvider FormatProvider { get; }
        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlFormatExpression, TResult>(this);
    }
}