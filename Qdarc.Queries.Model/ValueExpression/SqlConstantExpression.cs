using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlConstantExpression : ISqlValueExpression
    {
        public SqlConstantExpression(object value, Type clrType, ISqlType sqlType)
        {
            Value = value;
            ClrType = clrType;
            SqlType = sqlType;
        }

        public object Value { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlConstantExpression, TResult>(this);
    }
}