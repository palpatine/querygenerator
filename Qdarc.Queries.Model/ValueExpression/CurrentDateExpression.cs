﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class CurrentDateExpression : ISqlValueExpression
    {
        public CurrentDateExpression(ISqlType type)
        {
            SqlType = type;
            ClrType = typeof(DateTime);
        }

        public Type ClrType { get; set; }

        public ISqlType SqlType { get; set; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<CurrentDateExpression, TResult>(this);
    }
}