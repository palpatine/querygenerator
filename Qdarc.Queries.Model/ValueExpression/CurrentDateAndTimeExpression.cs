﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class CurrentDateAndTimeExpression : ISqlValueExpression
    {
        public CurrentDateAndTimeExpression(bool isUtc, ISqlType type)
        {
            IsUtc = isUtc;
            ClrType = typeof(DateTime);
            SqlType = type;
        }

        public bool IsUtc { get; }

        public Type ClrType { get; set; }

        public ISqlType SqlType { get; set; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<CurrentDateAndTimeExpression, TResult>(this);
    }
}