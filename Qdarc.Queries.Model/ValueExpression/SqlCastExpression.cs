using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlCastExpression : ISqlValueExpression
    {
        public SqlCastExpression(
            ISqlValueExpression expression,
            Type clrType,
            ISqlType sqlType)
        {
            Expression = expression ?? throw new ArgumentNullException(nameof(expression));
            ClrType = clrType;
            SqlType = sqlType;
        }

        public ISqlValueExpression Expression { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlCastExpression, TResult>(this);
    }
}