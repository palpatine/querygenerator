﻿using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlNullExpression : ISqlValueExpression
    {
        public SqlNullExpression(Type clrType, ISqlType sqlType)
        {
            ClrType = clrType;
            SqlType = sqlType;
        }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlNullExpression, TResult>(this);
    }
}