using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class MultiplyValueExpression : SqlBinaryValueExpression
    {
        public MultiplyValueExpression(ISqlValueExpression left, ISqlValueExpression right, Type clrType, ISqlType sqlType)
            : base(left, right, clrType, sqlType)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<MultiplyValueExpression, TResult>(this);
    }
}