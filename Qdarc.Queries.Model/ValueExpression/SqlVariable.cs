﻿using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlVariable : ISqlValueExpression, IAssignable
    {
        public SqlVariable(string name, bool isSystem, Type clrType, ISqlType sqlType)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            IsSystem = isSystem;
            ClrType = clrType;
            SqlType = sqlType;
        }

        public bool IsSystem { get; }

        public string Name { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<SqlVariable, TResult>(this);
    }
}