﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlCaseSwitchExpression : ISqlValueExpression
    {
        public SqlCaseSwitchExpression(ISqlValueExpression initialValue, IEnumerable<SqlWhenThen> cases, ISqlValueExpression @else, Type clrType, ISqlType sqlType)
        {
            InitialValue = initialValue ?? throw new ArgumentNullException(nameof(initialValue));
            Cases = cases?.ToArray() ?? throw new ArgumentNullException(nameof(cases));
            Else = @else;
            ClrType = clrType;
            SqlType = sqlType;

            if (Cases.Any(x => x == null))
            {
                throw new ArgumentException("Case cannot be null.", nameof(cases));
            }

            if (!Cases.Any())
            {
                throw new ArgumentException("At least one case is required.", nameof(cases));
            }
        }

        public IEnumerable<SqlWhenThen> Cases { get; }

        public ISqlValueExpression Else { get; }

        public ISqlValueExpression InitialValue { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlCaseSwitchExpression, TResult>(this);
    }
}