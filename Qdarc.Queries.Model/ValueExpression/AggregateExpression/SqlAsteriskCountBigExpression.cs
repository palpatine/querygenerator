using System;
using System.Collections.Generic;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlAsteriskCountBigExpression : ISqlAggregateExpression
    {
        public SqlAsteriskCountBigExpression(Type clrType, ISqlType sqlType)
        {
            ClrType = clrType;
            SqlType = sqlType;
        }

        public Type ClrType { get; }

        public IEnumerable<OrderedValueExpression> Order { get; set; }

        public IEnumerable<ISqlValueExpression> Partition { get; set; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlAsteriskCountBigExpression, TResult>(this);
    }
}