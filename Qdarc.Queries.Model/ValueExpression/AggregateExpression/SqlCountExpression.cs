﻿using System;
using System.Collections.Generic;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlCountExpression : ISqlAggregateExpression
    {
        // todo: assume clrType to be int
        public SqlCountExpression(
            ISqlValueExpression value,
            bool isDistinct,
            Type clrType,
            ISqlType sqlType)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            IsDistinct = isDistinct;
            ClrType = clrType;
            SqlType = sqlType;
        }

        public bool IsDistinct { get; }

        public ISqlValueExpression Value { get; }
        public Type ClrType { get; }

        public IEnumerable<OrderedValueExpression> Order { get; set; }

        public IEnumerable<ISqlValueExpression> Partition { get; set; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlCountExpression, TResult>(this);
    }
}