using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlCoalesceExpression : ISqlValueExpression
    {
        public SqlCoalesceExpression(IEnumerable<ISqlValueExpression> expressions, Type clrType, ISqlType sqlType)
        {
            Expressions = expressions?.ToArray() ?? throw new ArgumentNullException(nameof(expressions));
            ClrType = clrType;
            SqlType = sqlType;

            if (Expressions.Any(x => x == null))
            {
                throw new ArgumentException("Case cannot be null.", nameof(expressions));
            }

            if (!Expressions.Any())
            {
                throw new ArgumentException("At least one case is required.", nameof(expressions));
            }
        }

        public IEnumerable<ISqlValueExpression> Expressions { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlCoalesceExpression, TResult>(this);
    }
}