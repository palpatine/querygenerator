using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class NegateValueExpression : ISqlValueExpression
    {
        public NegateValueExpression(ISqlValueExpression valueExpression, Type clrType, ISqlType sqlType)
        {
            ClrType = clrType;
            SqlType = sqlType;
            ValueExpression = valueExpression ?? throw new ArgumentNullException(nameof(valueExpression));
        }

        public ISqlValueExpression ValueExpression { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<NegateValueExpression, TResult>(this);
    }
}