﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlUnaryPlusExpression : ISqlValueExpression
    {
        public SqlUnaryPlusExpression(ISqlValueExpression value, Type clrType, ISqlType sqlType)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            ClrType = clrType;
            SqlType = sqlType;
        }

        public ISqlValueExpression Value { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlUnaryPlusExpression, TResult>(this);
    }
}