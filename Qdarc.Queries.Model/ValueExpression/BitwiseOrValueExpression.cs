using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class BitwiseOrValueExpression : SqlBinaryValueExpression
    {
        public BitwiseOrValueExpression(ISqlValueExpression left, ISqlValueExpression right, Type clrType, ISqlType sqlType)
            : base(left, right, clrType, sqlType)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<BitwiseOrValueExpression, TResult>(this);
    }
}