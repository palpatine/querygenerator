﻿using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlInlineIfExpression : ISqlValueExpression
    {
        public SqlInlineIfExpression(
            ISqlLogicalExpression condition,
            ISqlValueExpression ifTrue,
            ISqlValueExpression ifFalse,
            Type clrType,
            ISqlType sqlType)
        {
            Condition = condition ?? throw new ArgumentNullException(nameof(condition));
            IfTrue = ifTrue ?? throw new ArgumentNullException(nameof(ifTrue));
            IfFalse = ifFalse ?? throw new ArgumentNullException(nameof(ifFalse));
            ClrType = clrType;
            SqlType = sqlType;
        }

        public ISqlLogicalExpression Condition { get; }

        public ISqlValueExpression IfFalse { get; }

        public ISqlValueExpression IfTrue { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlInlineIfExpression, TResult>(this);
    }
}