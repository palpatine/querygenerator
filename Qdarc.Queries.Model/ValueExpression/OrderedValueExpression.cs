using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class OrderedValueExpression
    {
        public OrderedValueExpression(ISqlValueExpression value, bool @ascending)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            Ascending = @ascending;
        }

        public bool Ascending { get; }

        public ISqlValueExpression Value { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<OrderedValueExpression, TResult>(this);
    }
}