using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public abstract class SqlBinaryValueExpression : ISqlValueExpression
    {
        protected SqlBinaryValueExpression(
            ISqlValueExpression left,
            ISqlValueExpression right,
            Type clrType,
            ISqlType sqlType)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
            ClrType = clrType;
            SqlType = sqlType;
        }

        public ISqlValueExpression Left { get; }

        public ISqlValueExpression Right { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public abstract TResult Accept<TResult>(ITransformer transformer);
    }
}