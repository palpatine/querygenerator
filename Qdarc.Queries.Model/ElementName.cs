using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Names
{
    public sealed class ElementName : IEnumerable<string>
    {
        private readonly List<string> _parts = new List<string>();

        public ElementName(string first, params string[] elements)
            : this(new[] { first }.Concat(elements))
        {
        }

        public ElementName(IEnumerable<string> elements)
        {
            _parts.AddRange((elements ?? throw new ArgumentNullException(nameof(elements)))
                .Where(x => !string.IsNullOrWhiteSpace(x)));
            if (!_parts.Any())
            {
                throw new ArgumentException("At least one element is required.", nameof(elements));
            }
        }

        public int Count => _parts.Count;

        public string this[int id] => _parts[id];

        public IEnumerator<string> GetEnumerator() => _parts.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public void AddPart(string part)
        {
            _parts.Add(part);
        }

        public override int GetHashCode() => ToString().GetHashCode();

        public override string ToString()
        {
            return _parts.Aggregate(string.Empty, (x, y) => $"{x}/{y}");
        }

        public override bool Equals(object obj)
        {
            var second = obj as ElementName;
            if (second == null || Count != second.Count)
            {
                return false;
            }

            for (var i = 0; i < Count; i++)
            {
                if (_parts[i] != second._parts[i])
                {
                    return false;
                }
            }

            return true;
        }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ElementName, TResult>(this);
    }
}