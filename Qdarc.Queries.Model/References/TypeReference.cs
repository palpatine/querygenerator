using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public sealed class TypeReference : ISqlType
    {
        public TypeReference(string name) => Name = name;

        public string Name { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<TypeReference, TResult>(this);
    }
}