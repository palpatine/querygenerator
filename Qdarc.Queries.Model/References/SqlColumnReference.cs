using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public sealed class SqlColumnReference : IColumnReference
    {
        public SqlColumnReference(ElementName columnName, ISource source, Type clrType, ISqlType sqlType)
        {
            ColumnName = columnName ?? throw new ArgumentNullException(nameof(columnName));
            Source = source;
            ClrType = clrType;
            SqlType = sqlType;
        }

        public ElementName ColumnName { get; }

        public ISource Source { get; set; }

        public Type ClrType { get;  }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlColumnReference, TResult>(this);
    }
}