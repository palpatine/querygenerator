﻿using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public sealed class CustomTypeReference : ISqlType
    {
        public CustomTypeReference(ElementName name) => Name = name;

        public ElementName Name { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<CustomTypeReference, TResult>(this);
    }
}