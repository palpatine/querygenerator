﻿using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public sealed class DeletedTableReference : ISource
    {
        public DeletedTableReference(Type clrType)
        {
            ClrType = clrType;
        }

        public Type ClrType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<DeletedTableReference, TResult>(this);
    }
}