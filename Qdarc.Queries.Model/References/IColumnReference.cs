using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public interface IColumnReference : ISqlValueExpression, IAssignable
    {
    }
}