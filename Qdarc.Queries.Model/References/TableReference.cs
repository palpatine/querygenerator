using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public sealed class TableReference : ITableReference, INamedSource
    {
        public TableReference(ElementName name, Type clrType = null)
        {
            Name = name;
            ClrType = clrType;
        }

        public ElementName Name { get; }

        public Type ClrType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<TableReference, TResult>(this);
    }
}