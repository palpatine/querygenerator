using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public sealed class OrderedColumnReference
    {
        public OrderedColumnReference(IColumnReference columnReference, bool? @ascending = null)
        {
            ColumnReference = columnReference;
            Ascending = @ascending;
        }

        public bool? Ascending { get; }

        public IColumnReference ColumnReference { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<OrderedColumnReference, TResult>(this);
    }
}