﻿using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.References
{
    public sealed class InsertedTableReference : ISource
    {
        public InsertedTableReference(Type clrType)
        {
            ClrType = clrType;
        }

        public Type ClrType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<InsertedTableReference, TResult>(this);
    }
}