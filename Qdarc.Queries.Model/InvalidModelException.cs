﻿using System;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Model
{
    public sealed class InvalidModelException : Exception
    {
        public InvalidModelException(string message, ISqlExpression expression, Exception innerException)
            : base(message, innerException) => Expression = expression;

        public InvalidModelException(string message, ISqlExpression expression)
            : base(message) => Expression = expression;

        public ISqlExpression Expression { get; }
    }
}