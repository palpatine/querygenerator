using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class WithExpression
    {
        public WithExpression(string name, ISelectCommand @select, IEnumerable<string> columns)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Select = @select ?? throw new ArgumentNullException(nameof(select));
            Columns = columns?.ToArray();

            if (Columns != null && Columns.Any(x => x == null))
            {
                throw new ArgumentException("Column alias cannot be null.", nameof(columns));
            }
        }

        public IEnumerable<string> Columns { get; }

        public string Name { get; }

        public ISelectCommand Select { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<WithExpression, TResult>(this);
    }
}