﻿using System;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class SelectCommandAsSelectionItem : ISqlValueExpression
    {
        public SelectCommandAsSelectionItem(ISelectCommand command)
        {
            Command = command ?? throw new ArgumentNullException(nameof(command));
            ClrType = Command.Selection.Elements.Single().ClrType;
            SqlType = Command.Selection.Elements.Single().SqlType;
        }

        public ISelectCommand Command { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SelectCommandAsSelectionItem, TResult>(this);
    }
}