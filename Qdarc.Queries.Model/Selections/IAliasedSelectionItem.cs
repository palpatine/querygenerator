﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public interface IAliasedSelectionItem : ISelectionItem
    {
        string Alias { get; set; }

        string ResolvedAlias { get; }

        ISelectionItem Item { get; }
    }
}