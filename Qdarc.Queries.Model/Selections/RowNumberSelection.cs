using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class RowNumberSelection : ISelectionItem
    {
        public RowNumberSelection(
            IEnumerable<OrderedValueExpression> order,
            IEnumerable<ISqlValueExpression> partition = null,
            Type clrType = null,
            ISqlType sqlType = null)
        {
            Order = order?.ToArray() ?? throw new ArgumentNullException(nameof(order));
            Partition = partition?.ToArray();
            ClrType = clrType;
            SqlType = sqlType;

            if (Order.Any(x => x == null))
            {
                throw new ArgumentException("Order elements cannot be null.", nameof(order));
            }

            if (Partition != null && Partition.Any(x => x == null))
            {
                throw new ArgumentException("Partition elements cannot be null.", nameof(partition));
            }
        }

        public IEnumerable<OrderedValueExpression> Order { get; }

        public IEnumerable<ISqlValueExpression> Partition { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<RowNumberSelection, TResult>(this);
    }
}