using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class BitwiseAdd : BaseAssignExpression
    {
        public BitwiseAdd(IAssignable assignable, ISqlValueExpression value)
            : base(assignable, value)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer)
            => transformer.Handle<BitwiseAdd, TResult>(this);
    }
}