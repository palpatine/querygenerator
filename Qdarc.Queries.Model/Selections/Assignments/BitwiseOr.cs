using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class BitwiseOr : BaseAssignExpression
    {
        public BitwiseOr(IAssignable assignable, ISqlValueExpression value)
            : base(assignable, value)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer)
            => transformer.Handle<BitwiseOr, TResult>(this);
    }
}