using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Model.Expressions
{
    public interface IAssignable : ISqlExpression
    {
    }
}