using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class MultiplyAssign : BaseAssignExpression
    {
        public MultiplyAssign(IAssignable assignable, ISqlValueExpression value)
            : base(assignable, value)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer)
            => transformer.Handle<MultiplyAssign, TResult>(this);
    }
}