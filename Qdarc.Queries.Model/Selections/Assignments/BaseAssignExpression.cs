﻿using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public abstract class BaseAssignExpression : ISelectionItem
    {
        protected BaseAssignExpression(
            IAssignable assignable,
            ISqlValueExpression value)
        {
            Assignable = assignable ?? throw new ArgumentNullException(nameof(assignable));
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public ISqlValueExpression Value { get; }

        public IAssignable Assignable { get; }

        public Type ClrType => Value.ClrType;

        public ISqlType SqlType => Value.SqlType;

        public abstract TResult Accept<TResult>(ITransformer transformer);
    }
}