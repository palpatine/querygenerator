using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class Assign : BaseAssignExpression
    {
        public Assign(IAssignable assignable, ISqlValueExpression value)
            : base(assignable, value)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer)
            => transformer.Handle<Assign, TResult>(this);
    }
}