using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class BitwiseExclusiveOr : BaseAssignExpression
    {
        public BitwiseExclusiveOr(IAssignable assignable, ISqlValueExpression value)
            : base(assignable, value)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer)
            => transformer.Handle<BitwiseExclusiveOr, TResult>(this);
    }
}