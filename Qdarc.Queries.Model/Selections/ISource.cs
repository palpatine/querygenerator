using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public interface ISource : ISqlExpression
    {
        Type ClrType { get; }
    }
}