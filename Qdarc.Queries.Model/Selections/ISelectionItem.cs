﻿using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public interface ISelectionItem : ISqlExpression
    {
        Type ClrType { get; }

        ISqlType SqlType { get; }
    }
}