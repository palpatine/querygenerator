using System;
using System.Reflection;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class BoundSelectionItem : IAliasedSelectionItem
    {
        public BoundSelectionItem(MemberInfo member, ISelectionItem item)
        {
            Member = member ?? throw new ArgumentNullException(nameof(member));
            Item = item ?? throw new ArgumentNullException(nameof(item));
            Alias = Member.Name;
        }

        public MemberInfo Member { get; }

        public string Alias { get; set; }

        public string ResolvedAlias { get; set; }

        public ISelectionItem Item { get; }

        public Type ClrType => Item.ClrType;

        public ISqlType SqlType => Item.SqlType;

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<BoundSelectionItem, TResult>(this);
    }
}