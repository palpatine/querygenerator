using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class PrefixedSelection : ISelectionItem
    {
        public PrefixedSelection(string prefix, ISqlSelection selection)
        {
            Prefix = prefix ?? throw new ArgumentNullException(nameof(prefix));
            Selection = selection ?? throw new ArgumentNullException(nameof(selection));
        }

        public ISqlSelection Selection { get; }

        public string Prefix { get; }

        public Type ClrType => Selection.ClrType;

        public ISqlType SqlType => null;

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<PrefixedSelection, TResult>(this);
    }
}