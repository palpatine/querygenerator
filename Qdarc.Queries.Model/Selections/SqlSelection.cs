using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class SqlSelection : ISqlSelection
    {
        public SqlSelection(IEnumerable<ISelectionItem> elements, Type clrType = null)
        {
            Elements = (elements ?? throw new ArgumentNullException(nameof(elements))).ToArray();
            ClrType = clrType;
            if (Elements.Any(x => x == null))
            {
                throw new ArgumentException("Selection items cannot be null.", nameof(elements));
            }
        }

        public IEnumerable<ISelectionItem> Elements { get; }

        public Type ClrType { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<SqlSelection, TResult>(this);
    }
}