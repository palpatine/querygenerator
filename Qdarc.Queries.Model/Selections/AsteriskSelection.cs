using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class AsteriskSelection : ISelectionItem
    {
        public AsteriskSelection(INamedSource source = null, Type clrType = null, ISqlType sqlType = null)
        {
            Source = source;
            ClrType = clrType;
            SqlType = sqlType;
        }

        public INamedSource Source { get; }

        public Type ClrType { get; }

        public ISqlType SqlType { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<AsteriskSelection, TResult>(this);
    }
}