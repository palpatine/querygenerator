using System.Collections.Generic;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public interface ISqlAggregateExpression : ISqlValueExpression
    {
        IEnumerable<OrderedValueExpression> Order { get; set; }

        IEnumerable<ISqlValueExpression> Partition { get; set; }
    }
}