using Qdarc.Queries.Model.Commands;

namespace Qdarc.Queries.Model.Selections
{
    public interface ISelectCommand : ISqlCommand, ISource
    {
        ISqlSelection Selection { get; set; }
    }
}