using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class TopExpression
    {
        public TopExpression(ISqlValueExpression value, bool isPercent, bool withTies)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            IsPercent = isPercent;
            WithTies = withTies;
        }

        public bool IsPercent { get; }

        public ISqlValueExpression Value { get; }

        public bool WithTies { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<TopExpression, TResult>(this);
    }
}