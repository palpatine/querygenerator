﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class GroupExpression
    {
        public GroupExpression(IEnumerable<ISqlValueExpression> values, ISqlLogicalExpression filter = null)
        {
            Values = values?.ToArray() ?? throw new ArgumentNullException(nameof(values));
            Filter = filter;

            if (Values.Any(x => x == null))
            {
                throw new ArgumentException("Values cannot be null.", nameof(values));
            }
        }

        public ISqlLogicalExpression Filter { get; }

        public IEnumerable<ISqlValueExpression> Values { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<GroupExpression, TResult>(this);
    }
}