using System;
using System.Collections.Generic;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public interface ISqlSelection : ISqlExpression
    {
        IEnumerable<ISelectionItem> Elements { get; }

        Type ClrType { get; }
    }
}