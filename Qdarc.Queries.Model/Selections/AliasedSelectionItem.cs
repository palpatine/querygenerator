﻿using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Selections
{
    public sealed class AliasedSelectionItem : IAliasedSelectionItem
    {
        public AliasedSelectionItem(ISelectionItem item, string alias = null)
        {
            Item = item ?? throw new ArgumentNullException(nameof(item));
            Alias = alias;
        }

        public string Alias { get; set; }

        public ISelectionItem Item { get; }

        public string ResolvedAlias { get; set; }

        public Type ClrType => Item.ClrType;

        public ISqlType SqlType => Item.SqlType;

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<AliasedSelectionItem, TResult>(this);
    }
}