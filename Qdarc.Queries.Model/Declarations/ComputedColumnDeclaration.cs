﻿using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Declarations
{
    public sealed class ComputedColumnDeclaration : ColumnDeclarationBase
    {
        public ISqlValueExpression Expression { get; set; }

        public PersistedConstraint Persisted { get; set; }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<ComputedColumnDeclaration, TResult>(this);
    }
}