namespace Qdarc.Queries.Model.Declarations
{
    public enum NullableValue
    {
        Default,
        Null,
        NotNull
    }
}