namespace Qdarc.Queries.Model.Declarations
{
    public enum PersistedNullableValue
    {
        Default,
        NotNull
    }
}