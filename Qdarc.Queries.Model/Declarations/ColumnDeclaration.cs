using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Declarations
{
    public sealed class ColumnDeclaration : ColumnDeclarationBase
    {
        public IdentityDeclaration Identity { get; set; }

        public NullableValue NullableValue { get; set; }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<ColumnDeclaration, TResult>(this);
    }
}