using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Declarations
{
    public sealed class IdentityDeclaration : IColumnConstraint
    {
        public int? Increment { get; set; }

        public int? Seed { get; set; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<IdentityDeclaration, TResult>(this);
    }
}