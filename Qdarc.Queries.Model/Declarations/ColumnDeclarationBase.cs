using System;
using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Declarations
{
    public abstract class ColumnDeclarationBase : IColumnReference, ITableComponent
    {
        public CheckConstraint Check { get; set; }

        public ElementName ColumnName => new ElementName(Name);

        public DefaultConstraint DefaultValue { get; set; }

        public IIndexConstraint Index { get; set; }

        public string Name { get; set; }

        public IIndexConstraint PrimaryKey { get; set; }

        public ITableReference TableReference { get; set; }

        public Type ClrType { get; set; }

        public ISqlType SqlType { get; set; }

        public abstract TResult Accept<TResult>(ITransformer transformer);
    }
}