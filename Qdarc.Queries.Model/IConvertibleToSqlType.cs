﻿namespace Qdarc.Queries.Model
{
    public interface IConvertibleToSqlType
    {
        ISqlType SqlType { get; }
    }
}