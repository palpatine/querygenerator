﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class AndLogicalConjunctionExpression : ISqlLogicalExpression
    {
        public AndLogicalConjunctionExpression(
            ISqlLogicalExpression left,
            ISqlLogicalExpression right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
        }

        public ISqlLogicalExpression Left { get; }

        public ISqlLogicalExpression Right { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<AndLogicalConjunctionExpression, TResult>(this);
    }
}