using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class LessThanLogicalExpression : SqlBinaryLogicalExpression
    {
        public LessThanLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<LessThanLogicalExpression, TResult>(this);
    }
}