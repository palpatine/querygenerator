﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class NotEqualLogicalExpression : SqlBinaryLogicalExpression
    {
        public NotEqualLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<NotEqualLogicalExpression, TResult>(this);
    }
}