﻿using System;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class ExistsExpression : ISqlLogicalExpression
    {
        public ExistsExpression(ISelectCommand @select, bool isNegated)
        {
            Select = @select ?? throw new ArgumentNullException(nameof(select));
            IsNegated = isNegated;
        }

        public bool IsNegated { get; }

        public ISelectCommand Select { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ExistsExpression, TResult>(this);
    }
}