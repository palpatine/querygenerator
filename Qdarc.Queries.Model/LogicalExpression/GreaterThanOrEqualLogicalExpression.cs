using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class GreaterThanOrEqualLogicalExpression : SqlBinaryLogicalExpression
    {
        public GreaterThanOrEqualLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<GreaterThanOrEqualLogicalExpression, TResult>(this);
    }
}