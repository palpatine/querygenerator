﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public abstract class SqlBinaryLogicalExpression : ISqlLogicalExpression
    {
        protected SqlBinaryLogicalExpression(
            ISqlValueExpression left,
            ISqlValueExpression right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
        }

        public ISqlValueExpression Left { get; }

        public ISqlValueExpression Right { get; }

        public abstract TResult Accept<TResult>(ITransformer transformer);
    }
}