using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class NotEqualIsoLogicalExpression : SqlBinaryLogicalExpression
    {
        public NotEqualIsoLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<NotEqualIsoLogicalExpression, TResult>(this);
    }
}