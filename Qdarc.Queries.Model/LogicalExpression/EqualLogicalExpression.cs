using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class EqualLogicalExpression : SqlBinaryLogicalExpression
    {
        public EqualLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<EqualLogicalExpression, TResult>(this);
    }
}