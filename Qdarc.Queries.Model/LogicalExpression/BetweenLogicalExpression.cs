using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class BetweenLogicalExpression : ISqlLogicalExpression
    {
        public BetweenLogicalExpression(
            ISqlValueExpression value,
            ISqlValueExpression lowerBound,
            ISqlValueExpression upperBound,
            bool isNegated)
        {
            IsNegated = isNegated;
            LowerBound = lowerBound ?? throw new ArgumentNullException(nameof(lowerBound));
            UpperBound = upperBound ?? throw new ArgumentNullException(nameof(upperBound));
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public bool IsNegated { get; }

        public ISqlValueExpression LowerBound { get; }

        public ISqlValueExpression UpperBound { get; }

        public ISqlValueExpression Value { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<BetweenLogicalExpression, TResult>(this);
    }
}