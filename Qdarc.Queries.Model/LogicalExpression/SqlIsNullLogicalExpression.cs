﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlIsNullLogicalExpression : ISqlLogicalExpression
    {
        public SqlIsNullLogicalExpression(ISqlValueExpression expression, bool isNegated)
        {
            Expression = expression ?? throw new ArgumentNullException(nameof(expression));
            IsNegated = isNegated;
        }

        public ISqlValueExpression Expression { get; }

        public bool IsNegated { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlIsNullLogicalExpression, TResult>(this);
    }
}