using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class LessThanOrEqualLogicalExpression : SqlBinaryLogicalExpression
    {
        public LessThanOrEqualLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<LessThanOrEqualLogicalExpression, TResult>(this);
    }
}