﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class NotLessThanLogicalExpression : SqlBinaryLogicalExpression
    {
        public NotLessThanLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<NotLessThanLogicalExpression, TResult>(this);
    }
}