﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class OrLogicalConjunctionExpression : ISqlLogicalExpression
    {
        public OrLogicalConjunctionExpression(
            ISqlLogicalExpression left,
            ISqlLogicalExpression right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
        }

        public ISqlLogicalExpression Left { get; }

        public ISqlLogicalExpression Right { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<OrLogicalConjunctionExpression, TResult>(this);
    }
}