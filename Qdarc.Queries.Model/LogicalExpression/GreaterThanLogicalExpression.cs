using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class GreaterThanLogicalExpression : SqlBinaryLogicalExpression
    {
        public GreaterThanLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<GreaterThanLogicalExpression, TResult>(this);
    }
}