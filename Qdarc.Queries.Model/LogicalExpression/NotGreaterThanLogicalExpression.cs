﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class NotGreaterThanLogicalExpression : SqlBinaryLogicalExpression
    {
        public NotGreaterThanLogicalExpression(ISqlValueExpression left, ISqlValueExpression right)
            : base(left, right)
        {
        }

        public override TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<NotGreaterThanLogicalExpression, TResult>(this);
    }
}