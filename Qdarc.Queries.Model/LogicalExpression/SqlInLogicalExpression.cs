﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlInLogicalExpression : ISqlLogicalExpression
    {
        // todo: split into two seperate expressions
        public SqlInLogicalExpression(ISqlValueExpression value, bool isNegated, IEnumerable<ISqlValueExpression> set)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            Set = set ?? throw new ArgumentNullException(nameof(set));
            IsNegated = isNegated;
            if (set.Any(x => x == null))
            {
                throw new ArgumentException("Elements in set cannot be null.", nameof(set));
            }
        }

        public SqlInLogicalExpression(ISqlValueExpression value, bool isNegated, ISelectCommand selection)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
            Selection = selection ?? throw new ArgumentNullException(nameof(selection));
            IsNegated = isNegated;
        }

        public bool IsNegated { get; }

        public ISelectCommand Selection { get; }

        public IEnumerable<ISqlValueExpression> Set { get; }

        public ISqlValueExpression Value { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlInLogicalExpression, TResult>(this);
    }
}