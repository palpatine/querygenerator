using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlLogicalNegateExpression : ISqlLogicalExpression
    {
        public SqlLogicalNegateExpression(ISqlLogicalExpression value)
        {
            Value = value ?? throw new ArgumentNullException(nameof(value));
        }

        public ISqlLogicalExpression Value { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<SqlLogicalNegateExpression, TResult>(this);
    }
}