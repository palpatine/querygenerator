using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class NegateLogicalExpression : ISqlLogicalExpression
    {
        // todo: merge SqlLogicalNegateExpression
        public NegateLogicalExpression(ISqlLogicalExpression logicalExpression)
        {
            LogicalExpression = logicalExpression ?? throw new ArgumentNullException(nameof(logicalExpression));
        }

        public ISqlLogicalExpression LogicalExpression { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<NegateLogicalExpression, TResult>(this);
    }
}