﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class ElementNameValueAssociation : ISqlExpression
    {
        public ElementNameValueAssociation(ElementName elementName, ISqlValueExpression value)
        {
            ElementName = elementName;
            Value = value;
        }

        public ElementName ElementName { get; }

        public ISqlValueExpression Value { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ElementNameValueAssociation, TResult>(this);
    }
}