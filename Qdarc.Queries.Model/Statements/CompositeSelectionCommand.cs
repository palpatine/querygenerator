using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public abstract class CompositeSelectionCommand : ISelectCommand
    {
        protected CompositeSelectionCommand(ISelectCommand left, ISelectCommand right)
        {
            Left = left ?? throw new ArgumentNullException(nameof(left));
            Right = right ?? throw new ArgumentNullException(nameof(right));
        }

        public ISelectCommand Left { get; }

        public ISelectCommand Right { get; }

        public ISqlSelection Selection
        {
            get => Left.Selection;
            set
            {
                Left.Selection = value;
                Right.Selection = value;
            }
        }

        public Type ClrType => Left.ClrType;

        public abstract TResult Accept<TResult>(ITransformer transformer);
    }
}