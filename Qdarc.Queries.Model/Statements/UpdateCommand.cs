﻿using System;
using System.Collections.Generic;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class UpdateCommand : ISqlCommand
    {
        public UpdateCommand(
            ISource target,
            IEnumerable<ElementNameValueAssociation> set,
            ISource source = null,
            TopExpression top = null,
            ISqlLogicalExpression filter = null)
        {
            Target = target;
            Set = set;
            Source = source;
            Top = top;
            Filter = filter;
        }

        public TopExpression Top { get; }

        public ISource Source { get; }

        public ISource Target { get; }

        public ISqlLogicalExpression Filter { get; }

        public IEnumerable<ElementNameValueAssociation> Set { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<UpdateCommand, TResult>(this);
    }
}