﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class TabularFunctionCall : ITableReference, INamedSource
    {
        public TabularFunctionCall(ElementName name, IEnumerable<ISqlValueExpression> arguments, Type clrType)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Arguments = (arguments ?? throw new ArgumentNullException(nameof(arguments))).ToArray();
            ClrType = clrType;

            if (Arguments.Any(x => x == null))
            {
                throw new ArgumentException("Argument cannot be null", nameof(arguments));
            }
        }

        public IEnumerable<ISqlValueExpression> Arguments { get; }

        public Type ClrType { get; }

        public ElementName Name { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<TabularFunctionCall, TResult>(this);
    }
}