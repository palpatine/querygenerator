using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class UnionAllSelectionCommand : CompositeSelectionCommand
    {
        public UnionAllSelectionCommand(ISelectCommand left, ISelectCommand right)
            : base(left, right)
        {
        }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<UnionAllSelectionCommand, TResult>(this);
    }
}