using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class CreateSchemaCommand : ISqlCommand
    {
        public CreateSchemaCommand(string name, ElementName owner)
        {
            Name = name;
            Owner = owner;
        }

        public string Name { get; }

        public ElementName Owner { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<CreateSchemaCommand, TResult>(this);
    }
}