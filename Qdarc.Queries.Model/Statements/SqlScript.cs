using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class SqlScript : ISqlExpression, IDirectlyMaterialized
    {
        public SqlScript(IEnumerable<ISqlCommand> commands, Type clrType)
        {
            Commands = commands?.ToArray() ?? throw new ArgumentNullException(nameof(commands));
            ClrType = clrType;

            if (Commands.Any(x => x == null))
            {
                throw new ArgumentException("Command cannot be null.", nameof(commands));
            }

            if (!Commands.Any())
            {
                throw new ArgumentException("There must be at least one command.", nameof(commands));
            }
        }

        public IEnumerable<ISqlCommand> Commands { get; }

        public Type ClrType { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<SqlScript, TResult>(this);
    }
}