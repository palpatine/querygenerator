﻿using System;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class SqlWhenThen
    {
        public SqlWhenThen(ISqlValueExpression @when, ISqlValueExpression then)
        {
            When = when ?? throw new ArgumentNullException(nameof(when));
            Then = then ?? throw new ArgumentNullException(nameof(then));
        }

        public ISqlValueExpression Then { get; }

        public ISqlValueExpression When { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<SqlWhenThen, TResult>(this);
    }
}