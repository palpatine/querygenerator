using System.Collections.Generic;
using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class CreateIndexCommand : ISqlCommand
    {
        public CreateIndexCommand(IIndexConstraint index, ITableReference table, ISqlLogicalExpression @where)
        {
            Index = index;
            Table = table;
            Where = @where;
        }

        public ITableReference Table { get; }

        public ISqlLogicalExpression Where { get; }

        public IIndexConstraint Index { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<CreateIndexCommand, TResult>(this);
    }
}