using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class SelectCommand : ISelectCommand
    {
        public SelectCommand(
            ISource source = null,
            ISqlSelection selection = null,
            ISqlLogicalExpression filter = null,
            bool isDistinct = false,
            TopExpression top = null,
            GroupExpression group = null,
            IEnumerable<OrderedValueExpression> order = null,
            IEnumerable<WithExpression> with = null)
        {
            Source = source;
            Selection = selection;
            Filter = filter;
            IsDistinct = isDistinct;
            Top = top;
            Group = group;
            Order = order?.ToArray();
            With = with?.ToArray();
        }

        public ISqlLogicalExpression Filter { get; set; }

        public GroupExpression Group { get; }

        public bool IsDistinct { get; }

        public IEnumerable<OrderedValueExpression> Order { get; set; }

        public ISource Source { get; }

        public TopExpression Top { get; set; }

        public IEnumerable<WithExpression> With { get; set; }

        public ISqlSelection Selection { get; set; }

        public Type ClrType => Selection.ClrType;

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<SelectCommand, TResult>(this);
    }
}