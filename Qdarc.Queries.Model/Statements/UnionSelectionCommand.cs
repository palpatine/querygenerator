using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class UnionSelectionCommand : CompositeSelectionCommand
    {
        public UnionSelectionCommand(ISelectCommand left, ISelectCommand right)
            : base(left, right)
        {
        }

        public override TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<UnionSelectionCommand, TResult>(this);
    }
}