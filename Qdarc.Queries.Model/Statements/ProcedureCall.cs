﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public sealed class ProcedureCall : ISqlExpression, IDirectlyMaterialized
    {
        public ProcedureCall(ElementName name, IEnumerable<ISqlValueExpression> arguments, Type clrType)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Arguments = arguments?.ToArray() ?? throw new ArgumentNullException(nameof(arguments));
            ClrType = clrType;

            if (Arguments.Any(x => x == null))
            {
                throw new ArgumentException("Argument cannot be null.", nameof(arguments));
            }
        }

        public IEnumerable<ISqlValueExpression> Arguments { get; }

        public ElementName Name { get; }

        public Type ClrType { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<ProcedureCall, TResult>(this);
    }
}