﻿using System.Collections.Generic;
using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Declarations;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class CreateTableCommand : ISqlCommand
    {
        public CreateTableCommand(
            ElementName name,
            IIndexConstraint primaryKey,
            IEnumerable<IIndexConstraint> indexes,
            IEnumerable<ColumnDeclarationBase> columnDeclarations,
            IEnumerable<DefaultConstraint> defaultValues,
            IEnumerable<CheckConstraint> checks)
        {
            Name = name;
            ColumnDeclarations = columnDeclarations;
            DefaultValues = defaultValues;
            Checks = checks;
            Indexes = indexes;
            PrimaryKey = primaryKey;
        }

        public IEnumerable<CheckConstraint> Checks { get; }

        public IEnumerable<ColumnDeclarationBase> ColumnDeclarations { get; }

        public IEnumerable<DefaultConstraint> DefaultValues { get; }

        public IEnumerable<IIndexConstraint> Indexes { get; }

        public ElementName Name { get; }

        public IIndexConstraint PrimaryKey { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<CreateTableCommand, TResult>(this);
    }
}