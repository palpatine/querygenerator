﻿using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class InsertCommand : ISqlCommand
    {
        public InsertCommand(ITableReference tableReference, ISource values, TopExpression top = null, Type clrType = null)
        {
            TableReference = tableReference ?? throw new ArgumentNullException(nameof(tableReference));
            Values = values ?? throw new ArgumentNullException(nameof(values));
            Top = top;
            ClrType = clrType;
        }

        public TopExpression Top { get; }

        public Type ClrType { get; }

        public ITableReference TableReference { get; }

        public ISource Values { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<InsertCommand, TResult>(this);
    }
}