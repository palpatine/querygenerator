﻿using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class DeleteCommand : ISqlCommand
    {
        public DeleteCommand(INamedSource target, ISqlLogicalExpression filter = null, ISource source = null)
        {
            Target = target ?? throw new ArgumentNullException(nameof(target));
            Source = source;
            Filter = filter;
        }

        public INamedSource Target { get; }

        public ISource Source { get; }

        public ISqlLogicalExpression Filter { get; }

        public TResult Accept<TResult>(ITransformer transformer) => transformer.Handle<DeleteCommand, TResult>(this);
    }
}