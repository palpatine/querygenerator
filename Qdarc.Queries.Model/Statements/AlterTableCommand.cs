using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Commands
{
    public sealed class AlterTableCommand : ISqlCommand
    {
        public AlterTableCommand(ITableReference table, IEnumerable<ITableComponent> addedComponents)
        {
            Table = table;
            AddedComponents = addedComponents.ToArray();
        }

        public IEnumerable<ITableComponent> AddedComponents { get; }

        public ITableReference Table { get; }

        public TResult Accept
            <TResult>(ITransformer transformer) => transformer.Handle<AlterTableCommand, TResult>(this);
    }
}