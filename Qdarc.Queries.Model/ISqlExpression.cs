﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Expressions
{
    public interface ISqlExpression
    {
        TResult Accept<TResult>(ITransformer transformer);
    }
}