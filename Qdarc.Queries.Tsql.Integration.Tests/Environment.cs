﻿using System.IO;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public static class Environment
    {
        public static IContainer Container { get; private set; }

        [AssemblyCleanup]
        public static void Cleanup()
        {
        }

        [AssemblyInitialize]
        public static void Create(TestContext context)
        {
            var containerBuilder = new ContainerBuilder();
            var registrar = new ServiceCollection();
            Bootstrapper.Bootstrap(registrar);
            Queries.Model.Bootstrapper.Bootstrap(registrar);
            Queries.Linq.Bootstrapper.Bootstrap(registrar);
            Queries.Linq.Tsql.Bootstrapper.Bootstrap(registrar);
            Queries.Formatter.Tsql.Bootstrapper.Bootstrap(registrar);
            containerBuilder.Populate(registrar);
            Container = containerBuilder.Build();
        }
    }
}