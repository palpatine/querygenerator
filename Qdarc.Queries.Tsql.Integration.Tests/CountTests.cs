using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Sales;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class CountTests
    {
        /// <summary>
        /// SELECT COUNT(*) FROM [Sales].[Customer] AS [q1]
        /// </summary>
        [TestMethod]
        public void CountStar()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Count();
            query.ShouldBeEqual(19820);
        }

        [TestMethod]
        public void CountStarInSingleItemProjection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Select(x => database.Count());
            query.Single().ShouldBeEqual(19820);
        }

        [TestMethod]
        [Ignore]
        public void MultipleAggregateFunctionsInSingleQuery()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Select(x => new { Count = database.Count(x.PersonId), Max = database.Max(x.PersonId) });

            query.Single().Count.ShouldBeEqual(19119);
            query.Single().Max.ShouldBeEqual(20777);
        }

        /// <summary>
        /// SELECT COUNT([q1].[PersonId]) AS[Count] FROM[Sales].[Customer] AS [q1]
        /// </summary>
        [TestMethod]
        public void CountByColumnFunctionsInSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Select(x => new { Count = database.Count(x.PersonId) }).ToList();

            query.Single().Count.ShouldBeEqual(19119);
        }

        /// <summary>
        /// SELECT COUNT(DISTINCT [q1].[StoreId]) AS [Count] FROM [Sales].[Customer] AS [q1]
        /// </summary>
        [TestMethod]
        public void CountByColumnDistinctFunctionsInSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Select(x => new { Count = database.CountDistinct(x.StoreId) }).ToList();

            query.Single().Count.ShouldBeEqual(701);
        }

        /// <summary>
        /// SELECT COUNT(*) AS [Count] FROM [Sales].[Customer] AS [q1]
        /// </summary>
        [TestMethod]
        public void CountAllFunctionsInSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Select(x => new { Count = database.Count() }).ToList();

            query.Single().Count.ShouldBeEqual(19820);
        }

        /// <summary>
        /// SELECT
        ///     (SELECT COUNT(*)
        ///     FROM [Sales].[SalesOrderHeader] AS [q2]
        ///     WHERE ([q2].[CustomerId] = [q1].[CustomerId])) AS [Count]
        /// FROM [Sales].[Customer] AS [q1]
        /// </summary>
        [TestMethod]
        public void CountAllFunctionsInNestedSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>()
                .Select(
                    x => new
                    {
                        Count = database.Query<SalesOrderHeader>().Where(z => z.CustomerId == x.CustomerId).Count()
                    })
                .ToList();

            query.Count.ShouldBeEqual(19820);
        }

        /// <summary>
        /// SELECT COUNT([q1].[PersonId]) FROM [Sales].[Customer] AS [q1]
        /// </summary>
        [TestMethod]
        public void CountAllIfColumnIsNotNull()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Count(x => x.PersonId);
            query.ShouldBeEqual(19119);
        }

        /// <summary>
        /// SELECT COUNT(DISTINCT [q1].[StoreId]) FROM [Sales].[Customer] AS [q1]
        /// </summary>
        [TestMethod]
        public void CountDistinct()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().CountDistinct(x => x.StoreId);
            query.ShouldBeEqual(701);
        }

        /// <summary>
        /// SELECT
        /// COUNT(*)
        /// FROM
        ///     [Person].[Address] AS [q1]
        ///     INNER JOIN [Person].[BusinessEntityAddress] AS [q2] ON ([q1].[AddressID] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void InnerJoinCountAsterisk()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .Join(database.Query<BusinessEntityAddress>(), (a, bea) => a.AddressId == bea.AddressId)
                .Count();

            query.ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT
        ///     COUNT(*)
        ///     FROM
        ///         [Person].[Address] AS [q1]
        ///         INNER JOIN [Person].[BusinessEntityAddress] AS [q2] ON ([q1].[AddressID] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void InnerJoinCountAsteriskSingleSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .Join(
                    database.Query<BusinessEntityAddress>(),
                    (a, bea) => a.AddressId == bea.AddressId,
                    (a, bea) => database.Count());
            var result = query.ToList();
            result.Single().ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT COUNT([q1].[AddressLine2])
        /// FROM [Person].[Address] AS [q1]
        /// INNER JOIN [Person].[BusinessEntityAddress] AS [q2] ON ([q1].[AddressID] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void InnerJoinCountAsteriskOnColumnJoinedTable()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .Join(
                    database.Query<BusinessEntityAddress>(),
                    (a, bea) => a.AddressId == bea.AddressId,
                    (a, bea) => database.Count(a.AddressLine2));
            var result = query.ToList();

            result.Single().ShouldBeEqual(362);
        }

        /// <summary>
        /// Invalid query:
        /// this would produce SELECT COUNT(a.*) and this is invalid for tsql.
        /// </summary>
        [TestMethod]
        public void InnerJoinCountAsteriskOnJoinedTable()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .Join(
                    database.Query<BusinessEntityAddress>(),
                    (a, bea) => a.AddressId == bea.AddressId,
                    (a, bea) => database.Count(a));

            QAssert.ShouldThrow<InvalidModelException>(() => query.ToList());
        }
    }
}