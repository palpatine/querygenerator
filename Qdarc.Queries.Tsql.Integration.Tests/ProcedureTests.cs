﻿using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Tsql.Integration.Tests.Model;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Dto;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class ProcedureTests
    {
        /// <summary>
        /// SELECT 4; SELECT 'a';
        /// </summary>
        [TestMethod]
        public void MultipleResultMapping()
        {
            var firstCommand = new SelectCommand(
                selection: new SqlSelection(
                    new[]
                    {
                        new SqlConstantExpression(
                            4,
                            typeof(int),
                            TsqlTypeProvider.Int)
                    },
                    typeof(int)));

            var secondCommand = new SelectCommand(
                selection:
                    new SqlSelection(
                        new[]
                        {
                            new SqlConstantExpression(
                                "a",
                                typeof(string),
                                TsqlTypeProvider.VarChar(1))
                        },
                        typeof(string)));

            var commands = new[] { firstCommand, secondCommand };
            var provider = Environment.Container.Resolve<ITableQueryProvider>();
            var script = new SqlScript(commands, typeof(MultipleResultDto));
            var reslut = provider.Execute<MultipleResultDto>(script);
            reslut.First.Count().ShouldBeEqual(1);
            reslut.First.Single().ShouldBeEqual(4);
            reslut.Second.Count().ShouldBeEqual(1);
            reslut.Second.Single().ShouldBeEqual("a");
        }

        /// <summary>
        /// DECLARE @arg0 INT = (4);
        /// EXEC [dbo].[uspGetEmployeeManagers] @arg0
        /// </summary>
        [TestMethod]
        public void ShouldExecuteProcedure()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var result = database.GetEmployeeManagers(4);
            result.Count().ShouldBeEqual(3);
            result.First().BusinessEntityId.ShouldBeEqual(4);
        }

        /// <summary>
        /// EXEC [dbo].[CalculateSum] @param1
        /// @param1 [IntValue] = <table>
        /// </summary>
        [TestMethod]
        public void ShouldExecuteProcedureWithTableParameter()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var result = database.CalculateSum(new[] { 1, 2, 3 });
            result.First().ShouldBeEqual(6);
        }

        /// <summary>
        /// DECLARE @arg0 INT = (4);
        /// EXEC [dbo].[uspGetEmployeeManagers] @arg0
        /// </summary>
        [TestMethod]
        public void ShouldExecuteProcedureWithoutResult()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.GetEmployeeManagersWithoutResult(4);
        }
    }
}