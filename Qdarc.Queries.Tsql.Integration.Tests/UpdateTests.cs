﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Custom;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class UpdateTests
    {
        [TestInitialize]
        public void Initialize()
        {
            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM [custom].[UserEntity]";
                    command.ExecuteNonQuery();
                }

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "INSERT INTO [custom].[UserEntity] (Id, Name) "
                                          + "VALUES (1, N'name'), (2000, N'value')";
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// UPDATE [custom].[UserEntity] SET [Id] = 5
        /// </summary>
        [TestMethod]
        public void ShouldUpdateValueInDatabase()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Update(x => new UserEntity
                {
                    Id = 5
                });

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(5);
                    reader["Name"].ShouldBeEqual("name");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(5);
                    reader["Name"].ShouldBeEqual("value");
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// UPDATE [custom].[UserEntity] SET [Name] = [Name] + N'NAME'
        /// </summary>
        [TestMethod]
        public void ShouldUpdateValueUsingPreviousValue()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Update(
                    x => new UserEntity
                    {
                        Name = x.Name + "NAME"
                    });

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(1);
                    reader["Name"].ShouldBeEqual("nameNAME");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader["Name"].ShouldBeEqual("valueNAME");
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// UPDATE [custom].[UserEntity]
        /// SET
        ///     [Name] = (
        ///                 SELECT TOP (2) [q1].[City] FROM [Person].[Address] AS [q1]
        ///                 WHERE ([q1].[AddressID] = [custom].[UserEntity].[Id])
        ///              )
        /// </summary>
        [TestMethod]
        public void ShouldUpdateValueUsingSubQuery()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Update(
                    x => new UserEntity
                    {
                        Name = database.Query<Address>()
                                        .Where(y => y.AddressId == x.Id)
                                        .Select(z => z.City)
                                        .Single()
                    });

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(1);
                    reader["Name"].ShouldBeEqual("Bothell");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader["Name"].ShouldBeEqual(DBNull.Value);
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// UPDATE [q1] SET [q1].[Name] = N'NAME'
        /// FROM [custom].[UserEntity] AS [q1]
        /// INNER JOIN [Person].[Address] [q2] ON [q1].[Id] = [q2].[AddressId]
        /// </summary>
        [TestMethod]
        public void ShouldUpdateAfterJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Join(database.Query<Address>(), (u, a) => u.Id == a.AddressId)
                .Update(
                    (u, a) => u,
                    (u, a) => new UserEntity
                    {
                        Name = "NAME"
                    });

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(1);
                    reader["Name"].ShouldBeEqual("NAME");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader["Name"].ShouldBeEqual("value");
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// UPDATE [custom].[UserEntity] SET [Name] = N'NAME' WHERE [Id] = 1
        /// </summary>
        [TestMethod]
        public void ShouldFilterOnUpdate()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Where(x => x.Id == 1)
                .Update(
                    x => new UserEntity
                    {
                        Name = "NAME"
                    });

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(1);
                    reader["Name"].ShouldBeEqual("NAME");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader["Name"].ShouldBeEqual("value");
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// UPDATE [q1] SET [q1].[Name] = [q1].[Name] + [q2].[City]
        /// FROM [custom].[UserEntity] AS [q1]
        /// INNER JOIN [Person].[Address] [q2] ON [q1].[Id] = [q2].[AddressId]
        /// </summary>
        [TestMethod]
        public void ShouldUpdateUsingOtherSources()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Join(database.Query<Address>(), (u, a) => u.Id == a.AddressId)
                .Update(
                    (u, a) => u,
                    (u, a) => new UserEntity
                    {
                        Name = u.Name + a.City
                    });

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(1);
                    reader["Name"].ShouldBeEqual("nameBothell");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader["Name"].ShouldBeEqual("value");
                    reader.Read().ShouldBeFalse();
                }
            }
        }
    }
}