﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class TabularFunctionTests
    {
        /// <summary>
        /// SELECT
        ///     [q1].[BusinessEntityType] AS [BusinessEntityType],
        ///     [q1].[FirstName] AS [FirstName],
        ///     [q1].[JobTitle] AS [JobTitle],
        ///     [q1].[LastName] AS [LastName],
        ///     [q1].[PersonId] AS [PersonId]
        /// FROM [dbo].[ufnGetContactInformation](1) AS [q1]
        /// </summary>
        [TestMethod]
        public void ShouldExecuteBasicSelectFormTabularFunction()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var data = database.GetContactInformation(1).ToList();
            data.ShouldNotBeNull();
            data.Count.ShouldBeEqual(1);
            data[0].FirstName.ShouldBeEqual("Ken");
            data[0].LastName.ShouldBeEqual("Sánchez");
            data[0].JobTitle.ShouldBeEqual("Chief Executive Officer");
            data[0].PersonId.ShouldBeEqual(1);
        }
    }
}