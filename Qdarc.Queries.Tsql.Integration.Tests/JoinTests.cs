using System;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Dto;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Types;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class JoinTests
    {
        /// <summary>
        ///  SELECT
        ///      [q1].[AddressID] AS [item1.AddressId]
        ///     ,[q1].[AddressLine1] AS [item1.AddressLine1]
        ///     ,[q1].[AddressLine2] AS [item1.AddressLine2]
        ///     ,[q1].[City] AS [item1.City]
        ///     ,[q1].[ModifiedDate] AS [item1.ModifiedDate]
        ///     ,[q1].[PostalCode] AS [item1.PostalCode]
        ///     ,[q1].[Rowguid] AS [item1.Rowguid]
        ///     ,[q1].[SpatialLocation] AS [item1.SpatialLocation]
        ///     ,[q1].[StateProvinceId] AS [item1.StateProvinceId]
        ///     ,[q2].[AddressID] AS [item2.AddressId]
        ///     ,[q2].[AddressTypeID] AS [item2.AddressTypeId]
        ///     ,[q2].[BusinessEntityID] AS [item2.BusinessEntityId]
        ///     ,[q3].[AddressTypeId] AS [item3.AddressTypeId]
        ///     ,[q3].[ModifiedDate] AS [item3.ModifiedDate]
        ///     ,[q3].[Name] AS [item3.Name]
        ///     ,[q3].[Rowguid] AS [item3.Rowguid]
        /// FROM
        ///     [Person].[Address] AS [q1]
        /// INNER JOIN
        ///     [Person].[BusinessEntityAddress] AS [q2]
        ///    ON
        ///     ([q1].[AddressID] = [q2].[AddressID])
        /// INNER JOIN
        ///     [Person].[AddressType] AS [q3]
        ///    ON
        ///     ([q2].[AddressTypeID] = [q3].[AddressTypeId])
        /// </summary>
        [TestMethod]
        public void DoubleInnerJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .Join(database.Query<BusinessEntityAddress>(), (a, bea) => a.AddressId == bea.AddressId)
                .Join(database.Query<AddressType>(), (a, bea, at) => bea.AddressTypeId == at.AddressTypeId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Item1.AddressId],
        ///     [q1].[AddressLine1] AS [Item1.AddressLine1],
        ///     [q1].[AddressLine2] AS [Item1.AddressLine2],
        ///     [q1].[City] AS [Item1.City],
        ///     [q1].[ModifiedDate] AS [Item1.ModifiedDate],
        ///     [q1].[PostalCode] AS [Item1.PostalCode],
        ///     [q1].[Rowguid] AS [Item1.Rowguid],
        ///     [q1].[SpatialLocation] AS [Item1.SpatialLocation],
        ///     [q1].[StateProvinceId] AS [Item1.StateProvinceId],
        ///     [q2].[AddressID] AS [Item2.AddressId],
        ///     [q2].[AddressTypeID] AS [Item2.AddressTypeId],
        ///     [q2].[BusinessEntityID] AS [Item2.BusinessEntityId],
        ///     [q4].[AddressTypeId] AS [Item3.AddressTypeId],
        ///     [q4].[ModifiedDate] AS [Item3.ModifiedDate],
        ///     [q4].[Name] AS [Item3.Name],
        ///     [q4].[Rowguid] AS [Item3.Rowguid]
        /// FROM [Person].[Address] AS [q1]
        /// INNER JOIN (
        ///         SELECT
        ///             [q3].[AddressID] AS [AddressId],
        ///             [q3].[AddressTypeID] AS [AddressTypeId],
        ///             [q3].[BusinessEntityID] AS [BusinessEntityId]
        ///         FROM [Person].[BusinessEntityAddress] AS [q3]
        ///         WHERE ([q3].[BusinessEntityID] = 1)
        ///         ) AS [q2] ON ([q1].[AddressID] = [q2].[AddressID])
        /// INNER JOIN (
        ///         SELECT
        ///             [q5].[AddressTypeId] AS [AddressTypeId],
        ///             [q5].[ModifiedDate] AS [ModifiedDate],
        ///             [q5].[Name] AS [Name],
        ///             [q5].[Rowguid] AS [Rowguid]
        ///         FROM [Person].[AddressType] AS [q5]
        ///         WHERE ([q5].[ModifiedDate] &lt; GETDATE())
        ///         ) AS [q4] ON ([q2].[AddressTypeID] = [q4].[AddressTypeId])
        /// </summary>
        [TestMethod]
        public void DoubleInnerJoinWithFieldsUsedToBuildInnerQueries()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var address = database.Query<BusinessEntityAddress>().Where(x => x.BusinessEntityId == 1);
            var addressType = database.Query<AddressType>().Where(x => x.ModifiedDate < DateTime.Now);

            var query = database.Query<Address>()
                .Join(address, (a, bea) => a.AddressId == bea.AddressId)
                .Join(addressType, (a, bea, at) => bea.AddressTypeId == at.AddressTypeId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT
        ///      [q1].[Id] AS [Item1.Id]
        ///     ,[q5].[AddressTypeId] AS [Item2.AddressTypeId]
        ///     ,[q5].[ModifiedDate] AS [Item2.ModifiedDate]
        ///     ,[q5].[Name] AS [Item2.Name]
        ///     ,[q5].[Rowguid] AS [Item2.Rowguid]
        /// FROM (
        ///     SELECT [q3].[AddressTypeID] AS [Id]
        ///     FROM [Person].[Address] AS [q2]
        ///     INNER JOIN (
        ///         SELECT
        ///              [q4].[AddressID] AS [AddressId]
        ///             ,[q4].[AddressTypeID] AS [AddressTypeId]
        ///             ,[q4].[BusinessEntityID] AS [BusinessEntityId]
        ///         FROM [Person].[BusinessEntityAddress] AS [q4]
        ///         WHERE ([q4].[BusinessEntityID] = 1)
        ///     ) AS [q3] ON ([q2].[AddressID] = [q3].[AddressID])
        /// ) AS [q1]
        /// INNER JOIN (
        ///     SELECT
        ///          [q6].[AddressTypeId] AS [AddressTypeId]
        ///         ,[q6].[ModifiedDate] AS [ModifiedDate]
        ///         ,[q6].[Name] AS [Name]
        ///         ,[q6].[Rowguid] AS [Rowguid]
        ///     FROM [Person].[AddressType] AS [q6]
        ///     WHERE ([q6].[ModifiedDate] &lt; GETDATE())
        /// ) AS [q5] ON ([q1].[Id] = [q5].[AddressTypeId])
        /// </summary>
        [TestMethod]
        public void SubQueryProjectionUsingMemberAssignmentProjection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var address = database.Query<BusinessEntityAddress>().Where(x => x.BusinessEntityId == 1);
            var addressType = database.Query<AddressType>().Where(x => x.ModifiedDate < DateTime.Now);

            var query = database.Query<Address>()
                .Join(address, (a, bea) => a.AddressId == bea.AddressId, (a, bea) => new NamedDto2 { Id = bea.AddressTypeId })
                .Join(addressType, (bea, at) => bea.Id == at.AddressTypeId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT [q3].[AddressTypeId] AS [Id]
        /// FROM [Person].[Address] AS [q1]
        /// INNER JOIN [Person].[BusinessEntityAddress] AS [q2] ON ([q1].[AddressID] = [q2].[AddressID])
        /// INNER JOIN [Person].[AddressType] AS [q3] ON ([q2].[AddressTypeID] = [q3].[AddressTypeId])
        /// </summary>
        [TestMethod]
        public void SelectOnJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var address = database.Query<BusinessEntityAddress>();
            var addressType = database.Query<AddressType>();

            var query = database.Query<Address>()
                .Join(address, (a, bea) => a.AddressId == bea.AddressId)
                .Join(addressType, (a, bea, at) => bea.AddressTypeId == at.AddressTypeId)
                .Select(x => new NamedDto2 { Id = x.Item3.AddressTypeId });

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT [q3].[AddressTypeId] AS [Id]
        /// FROM [Person].[Address] AS [q1]
        /// INNER JOIN [Person].[BusinessEntityAddress] AS [q2] ON ([q1].[AddressID] = [q2].[AddressID])
        /// INNER JOIN [Person].[AddressType] AS [q3] ON ([q2].[AddressTypeID] = [q3].[AddressTypeId])
        /// </summary>
        [TestMethod]
        public void SelectOnJoinViaTuple()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var address = database.Query<BusinessEntityAddress>();
            var addressType = database.Query<AddressType>();

            var query = database.Query<Address>()
                .Join(address, (a, bea) => a.AddressId == bea.AddressId)
                .Join(addressType, (a_bea, at) => a_bea.Item2.AddressTypeId == at.AddressTypeId)
                .Select(x => new NamedDto2 { Id = x.Item2.AddressTypeId });

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        [TestMethod]
        public void SubQueryProjectionUsingProjectionByConstructor()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var address = database.Query<BusinessEntityAddress>().Where(x => x.BusinessEntityId == 1);
            var addressType = database.Query<AddressType>().Where(x => x.ModifiedDate < DateTime.Now);

            var query = database.Query<Address>()
                .Join(address, (a, bea) => a.AddressId == bea.AddressId, (a, bea) => new { bea.AddressTypeId })
                .Join(addressType, (bea, at) => bea.AddressTypeId == at.AddressTypeId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Item1.AddressId],
        ///     [q1].[AddressTypeID] AS [Item1.AddressTypeId],
        ///     [q1].[BusinessEntityID] AS [Item1.BusinessEntityId],
        ///     [q2].[AddressTypeId] AS [Item2.AddressTypeId],
        ///     [q2].[ModifiedDate] AS [Item2.ModifiedDate],
        ///     [q2].[Name] AS [Item2.Name],
        ///     [q2].[Rowguid] AS [Item2.Rowguid]
        /// FROM
        ///     [Person].[BusinessEntityAddress] AS [q1]
        /// INNER JOIN (
        ///     SELECT
        ///         [q3].[AddressTypeId] AS [AddressTypeId],
        ///         [q3].[ModifiedDate] AS [ModifiedDate],
        ///         [q3].[Name] AS [Name],
        ///         [q3].[Rowguid] AS [Rowguid]
        ///     FROM [Person].[AddressType] AS [q3]
        ///     WHERE ([q3].[ModifiedDate] &lt; GETDATE())
        ///     ) AS [q2] ON ([q1].[AddressTypeID] = [q2].[AddressTypeId])
        /// </summary>
        [TestMethod]
        public void DoubleInnerJoinOnSubQueries()
        {
            var database = Environment.Container.Resolve<IDatabase>();

            var query = database
                            .Query<BusinessEntityAddress>()
                            .Join(
                                database.Query<AddressType>()
                                .Where(
                                        x => x.ModifiedDate < DateTime.Now),
                                (bea, at) => bea.AddressTypeId == at.AddressTypeId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Item1.AddressId],
        ///     [q1].[AddressLine1] AS [Item1.AddressLine1],
        ///     [q1].[AddressLine2] AS [Item1.AddressLine2],
        ///     [q1].[City] AS [Item1.City],
        ///     [q1].[ModifiedDate] AS [Item1.ModifiedDate],
        ///     [q1].[PostalCode] AS [Item1.PostalCode],
        ///     [q1].[Rowguid] AS [Item1.Rowguid],
        ///     [q1].[SpatialLocation] AS [Item1.SpatialLocation],
        ///     [q1].[StateProvinceId] AS [Item1.StateProvinceId],
        ///     [q2].[AddressID] AS [Item2.AddressId],
        ///     [q2].[AddressTypeID] AS [Item2.AddressTypeId],
        ///     [q2].[BusinessEntityID] AS [Item2.BusinessEntityId]
        /// FROM [Person].[Address] AS [q1]
        /// FULL JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///     ON ([q1].[AddressID] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void FullJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .FullJoin(database.Query<BusinessEntityAddress>(), (a, bea) => a.AddressId == bea.AddressId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Item1.AddressId],
        ///     [q1].[AddressLine1] AS [Item1.AddressLine1],
        ///     [q1].[AddressLine2] AS [Item1.AddressLine2],
        ///     [q1].[City] AS [Item1.City],
        ///     [q1].[ModifiedDate] AS [Item1.ModifiedDate],
        ///     [q1].[PostalCode] AS [Item1.PostalCode],
        ///     [q1].[Rowguid] AS [Item1.Rowguid],
        ///     [q1].[SpatialLocation] AS [Item1.SpatialLocation],
        ///     [q1].[StateProvinceId] AS [Item1.StateProvinceId],
        ///     [q2].[AddressID] AS [Item2.AddressId],
        ///     [q2].[AddressTypeID] AS [Item2.AddressTypeId],
        ///     [q2].[BusinessEntityID] AS [Item2.BusinessEntityId]
        /// FROM [Person].[Address] AS [q1]
        /// INNER JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///     ON ([q1].[AddressID] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void InnerJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .Join(database.Query<BusinessEntityAddress>(), (a, bea) => a.AddressId == bea.AddressId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Item1.AddressId],
        ///     [q1].[AddressLine1] AS [Item1.AddressLine1],
        ///     [q1].[AddressLine2] AS [Item1.AddressLine2],
        ///     [q1].[City] AS [Item1.City],
        ///     [q1].[ModifiedDate] AS [Item1.ModifiedDate],
        ///     [q1].[PostalCode] AS [Item1.PostalCode],
        ///     [q1].[Rowguid] AS [Item1.Rowguid],
        ///     [q1].[SpatialLocation] AS [Item1.SpatialLocation],
        ///     [q1].[StateProvinceId] AS [Item1.StateProvinceId],
        ///     [q2].[AddressID] AS [Item2.AddressId],
        ///     [q2].[AddressTypeID] AS [Item2.AddressTypeId],
        ///     [q2].[BusinessEntityID] AS [Item2.BusinessEntityId]
        /// FROM [Person].[Address] AS [q1]
        /// LEFT JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///     ON ([q1].[AddressID] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void LeftJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .LeftJoin(database.Query<BusinessEntityAddress>(), (a, bea) => a.AddressId == bea.AddressId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Item1.AddressId],
        ///     [q1].[AddressLine1] AS [Item1.AddressLine1],
        ///     [q1].[AddressLine2] AS [Item1.AddressLine2],
        ///     [q1].[City] AS [Item1.City],
        ///     [q1].[ModifiedDate] AS [Item1.ModifiedDate],
        ///     [q1].[PostalCode] AS [Item1.PostalCode],
        ///     [q1].[Rowguid] AS [Item1.Rowguid],
        ///     [q1].[SpatialLocation] AS [Item1.SpatialLocation],
        ///     [q1].[StateProvinceId] AS [Item1.StateProvinceId],
        ///     [q2].[AddressID] AS [Item2.AddressId],
        ///     [q2].[AddressTypeID] AS [Item2.AddressTypeId],
        ///     [q2].[BusinessEntityID] AS [Item2.BusinessEntityId]
        /// FROM [Person].[Address] AS [q1]
        /// RIGHT JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///     ON ([q1].[AddressID] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void RightJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .RightJoin(database.Query<BusinessEntityAddress>(), (a, bea) => a.AddressId == bea.AddressId);

            var data = query.ToList();
            data.Count.ShouldBeEqual(19614);
        }

        [TestMethod]
        public void ShouldJoinWithTableParameter()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .RightJoin(
                    database.TableParameter(new[] { 1, 2, 3, 4 }.Select(x => new IntValue(x))),
                    (a, tp) => a.AddressId == tp.Value);

            var data = query.ToList();
            data.Count.ShouldBeEqual(4);
        }

        /// <summary>
        ///  SELECT
        /// [q1].[AddressID] AS [AddressId],
        /// [q1].[AddressLine1] AS [AddressLine1],
        /// [q1].[AddressLine2] AS [AddressLine2],
        /// [q1].[City] AS [City],
        /// [q1].[ModifiedDate] AS [ModifiedDate],
        /// [q1].[PostalCode] AS [PostalCode],
        /// [q1].[Rowguid] AS [Rowguid],
        /// [q1].[SpatialLocation] AS [SpatialLocation],
        /// [q1].[StateProvinceId] AS [StateProvinceId]
        /// FROM [Person].[Address] AS [q1]
        /// INNER JOIN [Person].[BusinessEntityAddress] AS [q2]
        /// ON (([q2].[AddressTypeID] = 5) AND ([q1].[AddressID] = [q2].[AddressID]))
        /// </summary>
        [TestMethod]
        public void SelectFullTableAfterJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .Join(
                database.Query<BusinessEntityAddress>(),
                (a, bea) => bea.AddressTypeId == 5 && a.AddressId == bea.AddressId,
                (a, bea) => a);

            var data = query.ToList();
            data.Count.ShouldBeEqual(35);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Item1.AddressId],
        ///     [q1].[AddressLine1] AS [Item1.AddressLine1],
        ///     [q1].[AddressLine2] AS [Item1.AddressLine2],
        ///     [q1].[City] AS [Item1.City],
        ///     [q1].[ModifiedDate] AS [Item1.ModifiedDate],
        ///     [q1].[PostalCode] AS [Item1.PostalCode],
        ///     [q1].[Rowguid] AS [Item1.Rowguid],
        ///     [q1].[SpatialLocation] AS [Item1.SpatialLocation],
        ///     [q1].[StateProvinceId] AS [Item1.StateProvinceId],
        ///
        ///     [q2].[AddressID] AS [Item2.AddressId],
        ///     [q2].[AddressTypeID] AS [Item2.AddressTypeId],
        ///     [q2].[BusinessEntityID] AS [Item2.BusinessEntityId]
        ///
        /// FROM [Person].[Address] AS [q1]
        /// FULL JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///     ON ([q1].[AddressID] = [q2].[AddressID])
        /// WHERE ([q1].[AddressID] = 1)
        /// </summary>
        [TestMethod]
        public void FilterAfterJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .FullJoin(
                    database.Query<BusinessEntityAddress>(),
                    (a, bea) => a.AddressId == bea.AddressId)
                .Where(x => x.Item1.AddressId == 1);

            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[City] AS [City],
        ///     [q2].[BusinessEntityID] AS [BusinessEntityId],
        ///     [q1].[AddressID] AS [K.AddressId]
        /// FROM [Person].[Address] AS [q1]
        /// FULL JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///      ON ([q1].[AddressID] = [q2].[AddressID])
        /// WHERE ([q1].[AddressID] = 1)
        /// </summary>
        [TestMethod]
        public void FilterAfterJoinComplexSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .FullJoin(
                    database.Query<BusinessEntityAddress>(),
                    (a, bea) => a.AddressId == bea.AddressId,
                    (a, bea) => new { a.City, bea.BusinessEntityId, K = new { a.AddressId } })
                .Where(x => x.K.AddressId == 1);

            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT [q1].[AddressID]
        /// FROM [Person].[Address] AS [q1]
        /// FULL JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///     ON ([q1].[AddressID] = [q2].[AddressID])
        /// WHERE ([q1].[AddressID] = 1)
        /// </summary>
        [TestMethod]
        public void FilterAfterJoinWithSingleSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .FullJoin(
                    database.Query<BusinessEntityAddress>(),
                    (a, bea) => a.AddressId == bea.AddressId,
                    (a, bea) => a.AddressId)
                .Where(x => x == 1);

            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        ///  SELECT
        ///     [q1].[AddressID] AS [AddressId],
        ///     [q2].[AddressTypeID] AS [AddressTypeId]
        ///  FROM
        ///     [Person].[Address] AS [q1]
        /// FULL JOIN [Person].[BusinessEntityAddress] AS [q2]
        ///     ON ([q1].[AddressID] = [q2].[AddressID])
        /// WHERE
        ///     ([q2].[AddressTypeID] = 1)
        /// </summary>
        [TestMethod]
        public void FilterAfterJoinWithCustomSelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Address>()
                .FullJoin(
                    database.Query<BusinessEntityAddress>(),
                    (a, bea) => a.AddressId == bea.AddressId,
                    (a, bea) => new { a.AddressId, bea.AddressTypeId })
                .Where(x => x.AddressTypeId == 1);

            var data = query.ToList();
            data.Count.ShouldBeEqual(0);
        }
    }
}