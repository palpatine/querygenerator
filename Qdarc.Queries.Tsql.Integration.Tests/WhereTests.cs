using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Production;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Sales;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class WhereTests
    {
        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],
        ///     [q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],
        ///     [q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE (
        ///         ([q1].[ChangeNumber] > 100)
        ///       AND
        ///         (
        ///             ([q1].[FolderFlag] = 1)
        ///         OR
        ///             ([q1].[DocumentLevel] != 8)
        ///         )
        ///     )
        /// </summary>
        [TestMethod]
        public void ComplexFilter()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query =
                database.Query<Document>().Where(x => x.ChangeNumber > 100 && (x.FolderFlag || x.DocumentLevel != 8));
            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT [q1].[ChangeNumber] AS [ChangeNumber],[q1].[Document] AS [Content],[q1].[DocumentLevel] AS [DocumentLevel],[q1].[DocumentNode] AS [DocumentNode],[q1].[DocumentSummary] AS [DocumentSummary],[q1].[FileExtension] AS [FileExtension],[q1].[FileName] AS [FileName],[q1].[FolderFlag] AS [FolderFlag],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[Owner] AS [Owner],[q1].[Revision] AS [Revision],[q1].[Rowguid] AS [Rowguid],[q1].[Status] AS [Status],[q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE (NOT (([q1].[ChangeNumber] > 100) AND (([q1].[FolderFlag] = 1) OR (CAST([q1].[DocumentLevel] AS INT) != CAST(8 AS INT)))))
        /// </summary>
        [TestMethod]
        public void LogicalExpressionNegation()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query =
                database.Query<Document>().Where(x => !(x.ChangeNumber > 100 && (x.FolderFlag || x.DocumentLevel != 8)));
            var data = query.ToList();
            data.Count.ShouldBeEqual(12);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],
        ///     [q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],
        ///     [q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE (
        ///         [q1].[Document] IS NULL
        ///       )
        /// </summary>
        [TestMethod]
        public void ComparisonWithNullOnLeft()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query =
                database.Query<Document>().Where(x => x.Content == null);
            var data = query.ToList();
            data.Count.ShouldBeEqual(4);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],
        ///     [q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],
        ///     [q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE (
        ///         [q1].[Document] IS NOT NULL
        ///       )
        /// </summary>
        [TestMethod]
        public void ComparisonWithNotNullOnLeft()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query =
                database.Query<Document>().Where(x => x.Content != null);
            var data = query.ToList();
            data.Count.ShouldBeEqual(9);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],
        ///     [q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],
        ///     [q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE (
        ///         [q1].[Document] IS NULL
        ///       )
        /// </summary>
        [TestMethod]
        public void ComparisonWithNullOnRight()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query =
#pragma warning disable SA1131 // Use readable conditions
                database.Query<Document>().Where(x => null == x.Content);
#pragma warning restore SA1131 // Use readable conditions
            var data = query.ToList();
            data.Count.ShouldBeEqual(4);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],
        ///     [q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],
        ///     [q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE (
        ///         [q1].[Document] IS NOT NULL
        ///       )
        /// </summary>
        [TestMethod]
        public void ComparisonWithNotNullOnRight()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query =
#pragma warning disable SA1131 // Use readable conditions
                database.Query<Document>().Where(x => null != x.Content);
#pragma warning restore SA1131 // Use readable conditions
            var data = query.ToList();
            data.Count.ShouldBeEqual(9);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],
        ///     [q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],
        ///     [q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE ([q1].[FolderFlag] = 1)
        /// </summary>
        [TestMethod]
        public void FilterOnBoolean()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Document>().Where(x => x.FolderFlag);
            var data = query.ToList();
            data.Count.ShouldBeEqual(4);
        }

        /// <summary>
        ///  SELECT [q1].[ChangeNumber] AS [ChangeNumber],[q1].[Document] AS [Content],[q1].[DocumentLevel] AS [DocumentLevel],[q1].[DocumentNode] AS [DocumentNode],[q1].[DocumentSummary] AS [DocumentSummary],[q1].[FileExtension] AS [FileExtension],[q1].[FileName] AS [FileName],[q1].[FolderFlag] AS [FolderFlag],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[Owner] AS [Owner],[q1].[Revision] AS [Revision],[q1].[Rowguid] AS [Rowguid],[q1].[Status] AS [Status],[q1].[Title] AS [Title]
        ///  FROM [Production].[Document] AS [q1]
        ///  WHERE ([q1].[ChangeNumber] > 100)
        /// </summary>
        [TestMethod]
        public void FilterOnComparison()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Document>().Where(x => x.ChangeNumber > 100);
            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        [TestMethod]
        public void FilterOnString()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Document>().Where(x => x.FileName == "Introduction 1.doc");
            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT [q1].[ChangeNumber] AS [ChangeNumber],[q1].[Document] AS [Content],[q1].[DocumentLevel] AS [DocumentLevel],[q1].[DocumentNode] AS [DocumentNode],[q1].[DocumentSummary] AS [DocumentSummary],[q1].[FileExtension] AS [FileExtension],[q1].[FileName] AS [FileName],[q1].[FolderFlag] AS [FolderFlag],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[Owner] AS [Owner],[q1].[Revision] AS [Revision],[q1].[Rowguid] AS [Rowguid],[q1].[Status] AS [Status],[q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE ([q1].[ChangeNumber] > @param1)
        /// @param1 INT = 100
        /// </summary>
        [TestMethod]
        public void FilterOnComparisonWithIntVariable()
        {
            var value = 100;
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Document>().Where(x => x.ChangeNumber > value);
            var data = query.ToList();
            data.Count.ShouldBeEqual(1);
        }

        /// <summary>
        /// SELECT [q1].[ChangeNumber] AS [ChangeNumber],[q1].[Document] AS [Content],[q1].[DocumentLevel] AS [DocumentLevel],[q1].[DocumentNode] AS [DocumentNode],[q1].[DocumentSummary] AS [DocumentSummary],[q1].[FileExtension] AS [FileExtension],[q1].[FileName] AS [FileName],[q1].[FolderFlag] AS [FolderFlag],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[Owner] AS [Owner],[q1].[Revision] AS [Revision],[q1].[Rowguid] AS [Rowguid],[q1].[Status] AS [Status],[q1].[Title] AS [Title]
        ///  FROM [Production].[Document] AS [q1]
        /// WHERE (([q1].[ChangeNumber] > @param1) AND ([q1].[Owner] != @param2))
        /// @param1 INT = 100
        /// @param2 INT = 100
        /// </summary>
        [TestMethod]
        public void FilterOnComparisonWithIntVariableUsedTwice()
        {
            var value = 100;
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Document>().Where(x => x.ChangeNumber > value && x.Owner != value);
            query.ToList();
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.Inconclusive("Investigate if it is possible to generate single variable T78");
        }

        /// <summary>
        /// SELECT [q1].[ChangeNumber] AS [ChangeNumber],[q1].[Document] AS [Content],[q1].[DocumentLevel] AS [DocumentLevel],[q1].[DocumentNode] AS [DocumentNode],[q1].[DocumentSummary] AS [DocumentSummary],[q1].[FileExtension] AS [FileExtension],[q1].[FileName] AS [FileName],[q1].[FolderFlag] AS [FolderFlag],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[Owner] AS [Owner],[q1].[Revision] AS [Revision],[q1].[Rowguid] AS [Rowguid],[q1].[Status] AS [Status],[q1].[Title] AS [Title] FROM [Production].[Document] AS [q1] WHERE ([q1].[Title] = @param1)
        /// @param1 NVARCHAR(50) = title
        /// </summary>
        [TestMethod]
        public void FilterOnNVarCharComparisonWithStringVariable()
        {
            var value = "title";
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Document>().Where(x => x.Title == value);
            var data = query.ToList();
            data.Count.ShouldBeEqual(0);
        }

        /// <summary>
        /// SELECT [q1].[ChangeNumber] AS [ChangeNumber],[q1].[Document] AS [Content],[q1].[DocumentLevel] AS [DocumentLevel],[q1].[DocumentNode] AS [DocumentNode],[q1].[DocumentSummary] AS [DocumentSummary],[q1].[FileExtension] AS [FileExtension],[q1].[FileName] AS [FileName],[q1].[FolderFlag] AS [FolderFlag],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[Owner] AS [Owner],[q1].[Revision] AS [Revision],[q1].[Rowguid] AS [Rowguid],[q1].[Status] AS [Status],[q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE ([q1].[Title] + N'aa' = @param1)
        /// @param1 NVARCHAR(50) = title
        /// </summary>
        [TestMethod]
        public void FilterOnNVarCharSumComparisonWithStringVariable()
        {
            var value = "title";
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Document>().Where(x => x.Title + "aa" == value);
            var data = query.ToList();
            data.Count.ShouldBeEqual(0);
        }

        /// <summary>
        /// SELECT [q1].[AccountNumber] AS [AccountNumber],[q1].[CustomerId] AS [CustomerId],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[PersonId] AS [PersonId],[q1].[RowGuid] AS [RowGuid],[q1].[StoreId] AS [StoreId],[q1].[TerritoryId] AS [TerritoryId]
        /// FROM [Sales].[Customer] AS [q1]
        /// WHERE ([q1].[AccountNumber] = @param1)
        /// @param1 NVARCHAR(50) = title
        /// </summary>
        [TestMethod]
        public void FilterOnVarCharComparisonWithStringVariable()
        {
            var value = "title";
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<Customer>().Where(x => x.AccountNumber == value);
            var data = query.ToList();
            data.Count.ShouldBeEqual(0);
        }
    }
}