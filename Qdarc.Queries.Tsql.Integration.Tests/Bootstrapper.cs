﻿using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Linq.Tsql.Conventions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Tsql.Integration.Tests.Data;
using Qdarc.Sql.Connector;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IServiceCollection registrar)
        {
            registrar.AddTransient<ITableParameterValueConverter, TableParameterValueConverter>();
            registrar.AddTransient<IDatabaseConnectionProvider, DatabaseConnectionProvider>();
            registrar.AddTransient<IModelToSqlConvention, AttributesAnnotationModelToSqlConvention>();
        }
    }
}