﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class SomeInsaneSamplesTests
    {
        /// <summary>
        /// select(select top 1 count(a.BusinessEntityId) FROM Person.Person p)
        /// from Person.EmailAddress a
        /// JOIN Person.Password pp on pp.BusinessEntityId != a.BusinessEntityId
        /// where a.BusinessEntityId = 1
        /// </summary>
        [TestMethod]
        public void InNestedSelectionCountOuterTableColumn()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<EmailAddress>()
                  .Join(database.Query<Password>(), (ea, pp) => ea.BusinessEntityID != pp.BusinessEntityID)
                  .Where(x => x.Item1.BusinessEntityID == 1)
                  .Select(x => database.Query<Person>().Select(z => database.Count(x.Item1.BusinessEntityID)).Top(1));
            var result = query.ToList();

            result.ShouldNotBeNull();
        }

        [TestMethod]
        public void InNestedSelectionCountOuterTableColumn2()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<EmailAddress>()
                .Join(
                    database.Query<Password>(),
                    (ea, pp) => ea.BusinessEntityID != pp.BusinessEntityID,
                    (ea, pp) => new { ea.Email, pp.BusinessEntityID })
                .Where(x => x.BusinessEntityID == 1)
                .Select(x => database.Query<Person>().Select(z => database.Count(x.BusinessEntityID)).Top(1));
            var result = query.ToList();

            result.ShouldNotBeNull();
        }
    }
}