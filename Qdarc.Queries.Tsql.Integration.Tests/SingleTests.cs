using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class SingleTests
    {
        /// <summary>
        /// SELECT TOP (2)
        ///     [dbo].[AWBuildVersion].[Database Version] AS [DatabaseVersion],
        ///     [dbo].[AWBuildVersion].[ModifiedDate] AS [ModifiedDate],
        ///     [dbo].[AWBuildVersion].[SystemInformationID] AS [SystemInformationId],
        ///     [dbo].[AWBuildVersion].[VersionDate] AS [VersionDate]
        /// FROM [dbo].[AWBuildVersion]
        /// </summary>
        [TestMethod]
        public void Single()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var data = database.Query<BuildVersion>().Single();
            data.SystemInformationId.ShouldBeEqual((byte)1);
        }

        /// <summary>
        /// SELECT TOP (2)
        ///     [dbo].[AWBuildVersion].[Database Version] AS [DatabaseVersion],
        ///     [dbo].[AWBuildVersion].[ModifiedDate] AS [ModifiedDate],
        ///     [dbo].[AWBuildVersion].[SystemInformationID] AS [SystemInformationId],
        ///     [dbo].[AWBuildVersion].[VersionDate] AS [VersionDate]
        /// FROM [dbo].[AWBuildVersion]
        /// </summary>
        [TestMethod]
        public void SingleOrDefault()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var data = database.Query<BuildVersion>().SingleOrDefault();
            data.SystemInformationId.ShouldBeEqual((byte)1);
        }
    }
}