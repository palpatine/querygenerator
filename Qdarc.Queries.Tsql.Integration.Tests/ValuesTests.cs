﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Custom;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class ValuesTests
    {
        /// <summary>
        /// SELECT [Value] AS [Value] FROM (VALUES (1), (3), (2)) AS [q1](Value)
        /// </summary>
        [TestMethod]
        public void ShouldExecuteSelectionOfSimpleTypeValues()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var values = database.Values(new[] { 1, 3, 2 }).ToList();
            values.Count.ShouldBeEqual(3);
            values[0].ShouldBeEqual(1);
            values[1].ShouldBeEqual(3);
            values[2].ShouldBeEqual(2);
        }

        /// <summary>
        /// SELECT
        ///     [FirstColumn] AS [FirstColumn],
        ///     [SecondColumn] AS [SecondColumn]
        /// FROM (
        ///     VALUES (1, N'a'), (2, N'z'), (3, N'c')
        ///     ) AS [q1](FirstColumn, SecondColumn)
        /// </summary>
        [TestMethod]
        public void ShouldExecuteSelectionOfComplexTypeValues()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var values = database
                .Values(new[]
                {
                    new { FirstColumn = 1, SecondColumn = "a" },
                    new { FirstColumn = 2, SecondColumn = "z" },
                    new { FirstColumn = 3, SecondColumn = "c" }
                })
                .ToList();
            values.Count.ShouldBeEqual(3);
            values[0].FirstColumn.ShouldBeEqual(1);
            values[0].SecondColumn.ShouldBeEqual("a");
            values[1].FirstColumn.ShouldBeEqual(2);
            values[1].SecondColumn.ShouldBeEqual("z");
            values[2].FirstColumn.ShouldBeEqual(3);
            values[2].SecondColumn.ShouldBeEqual("c");
        }

        [TestMethod]
        public void ShouldBeAbleToJoinWithValuesExpression()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var values = database
                .Values(new[]
                {
                    new { FirstColumn = 1, SecondColumn = "a" },
                    new { FirstColumn = 2, SecondColumn = "z" },
                    new { FirstColumn = 3, SecondColumn = "c" }
                })
                .Join(database.Query<Address>(), (v, a) => v.FirstColumn == a.AddressId)
                .ToList();
            values.Count.ShouldBeEqual(3);
        }
    }
}
