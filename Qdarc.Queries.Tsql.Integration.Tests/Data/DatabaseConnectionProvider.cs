using System.Configuration;
using Qdarc.Sql.Connector;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Queries.Tsql.Integration.Tests.Data
{
    internal sealed class DatabaseConnectionProvider : IDatabaseConnectionProvider
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        public ISqlConnection Connection
        {
            get
            {
                var configurationProvider = ConfigurationManager.ConnectionStrings["AdventureWorks2012"];
                var connection = new SqlConnectionWrapper(configurationProvider.ConnectionString) { Trace = true };
                return connection;
            }
        }
    }
}