﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Dto;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Types;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model
{
    public static class DatabaseProcedures
    {
        public static IEnumerable<ManagerDto> GetEmployeeManagers(this IDatabase database, int businessEntityId)
        {
            return database.Procedure<ManagerDto>(new ElementName("dbo", "uspGetEmployeeManagers"), Expression.Constant(businessEntityId));
        }

        public static IEnumerable<int> CalculateSum(this IDatabase database, IEnumerable<int> values)
        {
            return database.Procedure<int>(new ElementName("dbo", "CalculateSum"), database.TableParameter(values.Select(x => new IntValue(x))).Expression);
        }

        public static void GetEmployeeManagersWithoutResult(this IDatabase database, int businessEntityId)
        {
            database.Procedure(new ElementName("dbo", "uspGetEmployeeManagers"), Expression.Constant(businessEntityId));
        }
    }
}
