﻿using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Person
{
    [SqlTableName("BusinessEntityAddress", "Person")]
    internal class BusinessEntityAddress
    {
        [SqlColumnName("AddressID")]
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int AddressId { get; set; }

        [SqlColumnName("AddressTypeID")]
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int AddressTypeId { get; set; }

        [SqlColumnName("BusinessEntityID")]
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int BusinessEntityId { get; set; }
    }
}