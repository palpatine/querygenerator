﻿using System;
using Microsoft.SqlServer.Types;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Person
{
    [SqlTableName("Address", "Person")]
    internal class Address
    {
        private int _addressIs;

        private string _addressLine1;

        private string _addressLine2;

        private string _city;

        private DateTime _modifiedDate;

        private string _postalCode;

        private Guid _rowguid;

        private SqlGeography _spatialLocation;

        private int _stateProvinceId;

        [SqlColumnName("AddressID")]
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int AddressId
        {
            get => _addressIs;

            set
            {
                if (_addressIs == value)
                {
                    return;
                }

                _addressIs = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 60)]
        public string AddressLine1
        {
            get => _addressLine1;

            set
            {
                if (_addressLine1 == value)
                {
                    return;
                }

                _addressLine1 = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.NVarChar, true, 60)]
        public string AddressLine2
        {
            get => _addressLine2;

            set
            {
                if (_addressLine2 == value)
                {
                    return;
                }

                _addressLine2 = value;
                Changed = true;
            }
        }

        [SqlIgnore]
        public bool Changed { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 30)]
        public string City
        {
            get => _city;

            set
            {
                if (_city == value)
                {
                    return;
                }

                _city = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.DateTime, false)]
        public DateTime ModifiedDate
        {
            get => _modifiedDate;

            set
            {
                if (_modifiedDate == value)
                {
                    return;
                }

                _modifiedDate = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 15)]
        public string PostalCode
        {
            get => _postalCode;

            set
            {
                if (_postalCode == value)
                {
                    return;
                }

                _postalCode = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.UniqueIdentifier, false)]
        public Guid Rowguid
        {
            get => _rowguid;

            set
            {
                if (_rowguid == value)
                {
                    return;
                }

                _rowguid = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.Geography, true)]
        public SqlGeography SpatialLocation
        {
            get => _spatialLocation;

            set
            {
                if (_spatialLocation == value)
                {
                    return;
                }

                _spatialLocation = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int StateProvinceId
        {
            get => _stateProvinceId;

            set
            {
                if (_stateProvinceId == value)
                {
                    return;
                }

                _stateProvinceId = value;
                Changed = true;
            }
        }
    }
}