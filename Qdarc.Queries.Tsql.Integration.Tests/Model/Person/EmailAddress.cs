﻿using System;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Person
{
    [SqlTableName("EmailAddress", "Person")]
    internal class EmailAddress
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int BusinessEntityID { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int EmailAddressID { get; set; }

        [SqlColumnName("EmailAddress")]
        [TsqlType(TsqlNativeTypesNames.NVarChar, true, 50)]
        public string Email { get; set; }

        [SqlColumnName("rowguid")]
        [TsqlType(TsqlNativeTypesNames.UniqueIdentifier, false)]
        public Guid RowGuid { get; set; }

        [TsqlType(TsqlNativeTypesNames.DateTime, false)]
        public DateTime ModifiedDate { get; set; }
    }
}