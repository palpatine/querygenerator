using System;
using Qdarc.Queries.Linq.Annotations;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Person
{
    [SqlTableName("AWBuildVersion", "dbo")]
    internal class BuildVersion
    {
        public DateTime VersionDate { get; set; }

        [SqlColumnName("Database Version")]
        public string DatabaseVersion { get; set; }

        [SqlColumnName("SystemInformationID")]
        public byte SystemInformationId { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}