﻿using System;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Sales
{
    [SqlTableName("Customer", "Sales")]
    internal class Customer
    {
        [TsqlType(TsqlNativeTypesNames.VarChar, true, 10)]
        public string AccountNumber { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int CustomerId { get; set; }

        [TsqlType(TsqlNativeTypesNames.DateTime, false)]
        public DateTime ModifiedDate { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, true)]
        public int PersonId { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, true)]
        public int TerritoryId { get; set; }

        [TsqlType(TsqlNativeTypesNames.UniqueIdentifier, false)]
        public Guid RowGuid { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, true)]
        public int StoreId { get; set; }
    }
}