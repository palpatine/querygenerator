﻿using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Sales
{
    [SqlTableName("SalesOrderHeader", "Sales")]
    internal class SalesOrderHeader
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int CustomerId { get; set; }
    }
}