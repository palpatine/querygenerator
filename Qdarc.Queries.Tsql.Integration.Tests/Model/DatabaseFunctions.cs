﻿using System.Linq.Expressions;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Dto;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model
{
    public static class DatabaseFunctions
    {
        public static IFunctionQuery<ContactInformationDto> GetContactInformation(this IDatabase database, int personId)
        {
            return database.Function<ContactInformationDto>(
                new ElementName("dbo", "ufnGetContactInformation"),
                Expression.Constant(personId));
        }

        [ScalarFunction("ufnGetDocumentStatusText", "dbo")]
        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 16)]
        public static string GetDocumentStatusText(this IDatabase database, byte status)
        {
            return database.ScalarFunction(x => GetDocumentStatusText(x, status));
        }
    }
}