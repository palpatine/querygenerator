﻿using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Custom
{
    [SqlTableName("UserEntity", "custom")]
    public class UserEntity
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int Id { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string Name { get; set; }
    }
}
