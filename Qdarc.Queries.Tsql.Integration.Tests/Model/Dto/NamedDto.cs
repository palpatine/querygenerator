using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Dto
{
    public class NamedDto
    {
        public NamedDto(int id, string name)
        {
            Id = id;
            Name = name;
        }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string Name { get; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int Id { get; }
    }
}