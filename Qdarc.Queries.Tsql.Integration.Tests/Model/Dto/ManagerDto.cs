﻿using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Dto
{
    public class ManagerDto
    {
        public int BusinessEntityId { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string FirstName { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string LastName { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string ManagerFirstName { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string ManagerLastName { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string OrganizationNode { get; set; }
    }
}