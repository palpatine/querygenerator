using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Dto
{
    public class NamedDto2
    {
        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string Name { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int Id { get; set; }
    }
}