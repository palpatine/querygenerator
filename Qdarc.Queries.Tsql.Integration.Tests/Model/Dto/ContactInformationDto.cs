using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Dto
{
    public class ContactInformationDto
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int PersonId { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string FirstName { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string LastName { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string JobTitle { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string BusinessEntityType { get; set; }
    }
}