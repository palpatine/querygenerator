﻿using System.Collections.Generic;
using Qdarc.Queries.Linq.Annotations;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Dto
{
    [MultipleResult]
    internal class MultipleResultDto
    {
        [SourceResult(0)]
        public IEnumerable<int> First { get; set; }

        [SourceResult(1)]
        public IEnumerable<string> Second { get; set; }
    }
}