﻿namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Production
{
    internal interface IOwned
    {
        int Owner { get; set; }
    }

    internal interface IOwnedExplicit
    {
        int Owner { get; set; }
    }
}