﻿using System;
using Microsoft.SqlServer.Types;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Production
{
    [SqlTableName("Document", "Production")]
    internal class Document : IOwned, IOwnedExplicit
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int ChangeNumber { get; set; }

        [SqlColumnName("Document")]
        [TsqlType(TsqlNativeTypesNames.Binary, true, -1)]
        public byte[] Content { get; set; }

        [TsqlType(TsqlNativeTypesNames.SmallInt, true)]
        [TsqlComputedColumn]
        public short? DocumentLevel { get; set; }

        [TsqlType(TsqlNativeTypesNames.HierarchyId, false)]
        public SqlHierarchyId DocumentNode { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, true, -1)]
        public string DocumentSummary { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 8)]
        public string FileExtension { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 400)]
        public string FileName { get; set; }

        [TsqlType(TsqlNativeTypesNames.Bit, false)]
        public bool FolderFlag { get; set; }

        [TsqlType(TsqlNativeTypesNames.DateTime, false)]
        public DateTime ModifiedDate { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int Owner { get; set; }

        [TsqlType(TsqlNativeTypesNames.NChar, false, 5)]
        public string Revision { get; set; }

        [TsqlType(TsqlNativeTypesNames.UniqueIdentifier, false)]
        public Guid Rowguid { get; set; }

        [TsqlType(TsqlNativeTypesNames.TinyInt, false)]
        public byte Status { get; set; }

        [TsqlType(TsqlNativeTypesNames.NVarChar, false, 50)]
        public string Title { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        int IOwnedExplicit.Owner
        {
            get => Owner;
            set => Owner = value;
        }
    }
}