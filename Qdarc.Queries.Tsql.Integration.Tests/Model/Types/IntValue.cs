﻿using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Tsql.Integration.Tests.Model.Types
{
    [UserDefinedTableTypeName("IntList", "dbo")]
    public class IntValue
    {
        public IntValue(int value)
        {
            Value = value;
        }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        [SqlColumnName("Id")]
        public int Value { get; set; }
    }
}