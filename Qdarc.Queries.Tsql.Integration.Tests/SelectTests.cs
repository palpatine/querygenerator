﻿using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Custom;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Dto;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Production;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Types;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class SelectTests
    {
        /// <summary>
        /// SELECT
        ///     CAST(IIF(EXISTS(
        ///         SELECT 1
        ///         FROM [Person].[AddressType] AS [q1])
        ///      , 1, 0) AS BIT)
        /// </summary>
        [TestMethod]
        public void Any()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var data = database.Query<AddressType>().Any();

            data.ShouldBeTrue();
        }

        /// <summary>
        /// SELECT
        ///     CAST(IIF(EXISTS(
        ///         SELECT [q1].[AddressTypeId] AS [AddressTypeId],[q1].[ModifiedDate] AS [ModifiedDate],[q1].[Name] AS [Name],[q1].[Rowguid] AS [Rowguid]
        ///         FROM [Person].[AddressType] AS [q1]
        ///         WHERE ([q1].[Name] = N'HomeOffice'))
        ///     , 1, 0) AS BIT)
        /// </summary>
        [TestMethod]
        public void AnyWithCondition()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var data = database.Query<AddressType>().Any(x => x.Name == "HomeOffice");

            data.ShouldBeFalse();
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressID] AS [Id]
        ///     ,[q1].[City] AS [Name]
        /// FROM [Person].[Address] AS [q1]
        /// WHERE ([q1].[AddressID] = 2)
        /// </summary>
        [TestMethod]
        public void MemberAssignmentProjection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var result = database.Query<Address>()
                  .Where(x => x.AddressId == 2)
                  .Select(x => new UserEntity { Id = x.AddressId, Name = x.City })
                  .ToList();

            result.Single().Id.ShouldBeEqual(2);
            result.Single().Name.ShouldBeEqual("Bothell");
        }

        /// <summary>
        /// SELECT
        ///     [q1].[AddressTypeId] AS [AddressTypeId],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Name] AS [Name],
        ///     [q1].[Rowguid] AS [Rowguid]
        /// FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void SelectAll()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>();
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],
        ///     [q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],
        ///     [q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE ([q1].[Owner] = 220)
        /// </summary>
        [TestMethod]
        public void SelectByAbstractionExplicitlyImplemented()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var result = database.Query<IOwnedExplicit>(typeof(Document))
                .Where(x => x.Owner == 220)
                .ToList();

            result.Count.ShouldBeEqual(5);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[ChangeNumber] AS [ChangeNumber],
        ///     [q1].[Document] AS [Content],
        ///     [q1].[DocumentLevel] AS [DocumentLevel],
        ///     [q1].[DocumentNode] AS [DocumentNode],
        ///     [q1].[DocumentSummary] AS [DocumentSummary],
        ///     [q1].[FileExtension] AS [FileExtension],
        ///     [q1].[FileName] AS [FileName],
        ///     [q1].[FolderFlag] AS [FolderFlag],
        ///     [q1].[ModifiedDate] AS [ModifiedDate],
        ///     [q1].[Owner] AS [Owner],[q1].[Revision] AS [Revision],
        ///     [q1].[Rowguid] AS [Rowguid],[q1].[Status] AS [Status],
        ///     [q1].[Title] AS [Title]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE ([q1].[Owner] = 220)
        /// </summary>
        [TestMethod]
        public void SelectByAbstraction()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var result = database.Query<IOwned>(typeof(Document))
                .Where(x => x.Owner == 220)
                .ToList();

            result.Count.ShouldBeEqual(5);
        }

        /// <summary>
        /// SELECT [q1].[Id] AS [Value] FROM @param1 AS [q1]
        /// @param1 [IntList] = <table>
        /// </summary>
        [TestMethod]
        public void SelectFromTableParameter()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var value = new[] { 1, 2, 3, 4 }.Select(x => new IntValue(x));
            var result = database.TableParameter(value)
                .ToList();

            result.Count.ShouldBeEqual(4);
        }

        /// <summary>
        /// SELECT
        ///     [q1].[Name] AS [Name],
        ///     [q1].[AddressTypeId] AS [Id]
        /// FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void SelectProjectionNewAnonymousObject()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Select(x => new { x.Name, Id = x.AddressTypeId });
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
        }

        /// <summary>
        /// SELECT
        ///     (SELECT TOP (2) [q2].[City]
        ///          FROM [Person].[Address] AS [q2]
        ///          WHERE ([q2].[AddressID] = [q1].[AddressTypeId])) AS [Name]
        /// FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void SelectConstructorProjectionWithSubQuery()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Select(x => new
            {
                Name = database.Query<Address>().Where(z => z.AddressId == x.AddressTypeId).Select(z => z.City).Single(),
            });
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
        }

        /// <summary>
        /// SELECT
        ///     (SELECT TOP (2) [q2].[City]
        ///     FROM [Person].[Address] AS [q2]
        ///      WHERE ([q2].[AddressID] = [q1].[AddressTypeId])) AS [Name]
        ///  FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void SelectAssignmentProjectionWithSubQuery()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Select(x => new UserEntity
            {
                Name = database.Query<Address>().Where(z => z.AddressId == x.AddressTypeId).Select(z => z.City).Single()
            });
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
        }

        /// <summary>
        /// SELECT [q1].[Name] FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void SelectProjectionSingleColumn()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Select(x => x.Name);
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
            CollectionAssert.AreEquivalent(
                data,
                new[]
                {
                    "Archive",
                    "Billing",
                    "Home",
                    "Main Office",
                    "Primary",
                    "Shipping"
                });
        }

        /// <summary>
        /// SELECT 1 FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void SelectConstantInProjection()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Select(x => 1);
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
            CollectionAssert.AreEquivalent(
                data,
                new[]
                {
                    1,
                    1,
                    1,
                    1,
                    1,
                    1
                });
        }

        /// <summary>
        /// SELECT [q1].[AddressTypeId] FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void SelectProjectionSingleColumnWithInt32ToNullableInt32Conversion()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Select(x => (int?)x.AddressTypeId);
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
            CollectionAssert.AreEquivalent(
                data,
                new[]
                {
                    6,
                    5,
                    4,
                    3,
                    1,
                    2
                });
        }

        /// <summary>
        /// SELECT [q1].[AddressTypeId] AS [id],[q1].[Name] AS [name] FROM [Person].[AddressType] AS [q1]
        /// </summary>
        [TestMethod]
        public void ShouldSelectDtoWithConstructorInitialization()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Select(x => new NamedDto(x.AddressTypeId, x.Name));
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
        }

        /// <summary>
        /// SELECT ~([q1].[ChangeNumber]) FROM [Production].[Document] AS [q1]
        /// </summary>
        [TestMethod]
        public void ValueExpressionNegation()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query =
                database.Query<Document>().Select(x => ~x.ChangeNumber);
            var data = query.ToList();
            data.Count.ShouldBeEqual(13);
        }

        /// <summary>
        /// SELECT
        ///      [q1].[AddressTypeId] AS [Item1.id]
        ///     ,[q1].[Name] AS [Item1.name]
        ///     ,[q3].[AddressID] AS [Item2.AddressId]
        ///     ,[q3].[AddressLine1] AS [Item2.AddressLine1]
        ///     ,[q3].[AddressLine2] AS [Item2.AddressLine2]
        ///     ,[q3].[City] AS [Item2.City]
        ///     ,[q3].[ModifiedDate] AS [Item2.ModifiedDate]
        ///     ,[q3].[PostalCode] AS [Item2.PostalCode]
        ///     ,[q3].[Rowguid] AS [Item2.Rowguid]
        ///     ,[q3].[SpatialLocation] AS [Item2.SpatialLocation]
        ///     ,[q3].[StateProvinceId] AS [Item2.StateProvinceId]
        /// FROM (
        ///     SELECT
        ///          [q2].[AddressTypeId] AS [id]
        ///         ,[q2].[Name] AS [name]
        ///     FROM [Person].[AddressType] AS [q2]
        ///     ) AS [q1]
        /// INNER JOIN [Person].[Address] AS [q3] ON ([q1].[Id] = [q3].[AddressID])
        /// </summary>
        [TestMethod]
        public void ShouldJoinOnSubSelectionWithConstructorInitialization()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.Inconclusive("T91");
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>()
                .Select(x => new NamedDto(x.AddressTypeId, x.Name))
                .Join(database.Query<Address>(), (t, a) => t.Id == a.AddressId);
            var data = query.ToList();

            data.Count.ShouldBeEqual(6);
        }
    }
}