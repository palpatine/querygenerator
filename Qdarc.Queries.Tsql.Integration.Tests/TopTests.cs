﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class TopTests
    {
        /// <summary>
        /// SELECT TOP (2)
        ///     [Person].[AddressType].[AddressTypeId] AS [AddressTypeId],
        ///     [Person].[AddressType].[ModifiedDate] AS [ModifiedDate],
        ///     [Person].[AddressType].[Name] AS [Name],
        ///     [Person].[AddressType].[Rowguid] AS [Rowguid]
        /// FROM [Person].[AddressType]
        /// </summary>
        [TestMethod]
        public void Top()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var query = database.Query<AddressType>().Top(2);
            var data = query.ToList();
            data.Count.ShouldBeEqual(2);
        }
    }
}