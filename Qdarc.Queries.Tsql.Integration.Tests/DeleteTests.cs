﻿using System.Configuration;
using System.Data.SqlClient;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Custom;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class DeleteTests
    {
        [TestInitialize]
        public void Initialize()
        {
            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM [custom].[UserEntity]";
                    command.ExecuteNonQuery();
                }

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "INSERT INTO [custom].[UserEntity] (Id, Name) "
                                          + "VALUES (1, N'name'), (2000, N'value')";
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// DELETE FROM [custom].[UserEntity]
        /// </summary>
        [TestMethod]
        public void ShouldDeleteAllRows()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Delete();

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// DELETE FROM [custom].[UserEntity] WHERE ([custom].[UserEntity].[Id] = 1)
        /// </summary>
        [TestMethod]
        public void ShouldDeleteOnlySelectedRow()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Where(x => x.Id == 1)
                .Delete();

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// DELETE FROM [custom].[UserEntity] WHERE ([custom].[UserEntity].[Id] = @param1)
        /// </summary>
        [TestMethod]
        public void ShouldDeleteOnlySelectedRowUsingParameter()
        {
            var id = 1;
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Where(x => x.Id == id)
                .Delete();

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// DELETE q1
        /// FROM [custom].[UserEntity] AS [q1]
        /// INNER JOIN [Person].[Address] AS [q2] ON ([q1].[Id] = [q2].[AddressID])
        /// </summary>
        [TestMethod]
        public void ShouldDeleteUsingJoin()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            database.Query<UserEntity>()
                .Join(database.Query<Address>(), (x, y) => x.Id == y.AddressId)
                .Delete((u, a) => u);

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2000);
                    reader.Read().ShouldBeFalse();
                }
            }
        }
    }
}