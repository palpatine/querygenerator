﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Production;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class ScalarFunctionTests
    {
        /// <summary>
        /// SELECT [dbo].[ufnGetDocumentStatusText](@param1)
        /// @param1 TINYINT = 1
        /// </summary>
        [TestMethod]
        public void ShouldExecuteBasicSelectFormScalarFunction()
        {
            var database = Environment.Container.Resolve<IDatabase>();
            var data = database.GetDocumentStatusText(1);
            data.ShouldBeEqual("Pending approval");
        }

        /// <summary>
        /// SELECT [dbo].[ufnGetDocumentStatusText]([q1].[Status])
        ///  FROM [Production].[Document] AS [q1]
        ///  WHERE ([q1].[FileName] = N'Crank Arm and Tire Maintenance.doc')
        /// </summary>
        [TestMethod]
        public void ShouldExecuteScalarFunctionInSelectClause()
        {
            var database = Environment.Container.Resolve<IDatabase>();

            var data = database.Query<Document>()
                .Where(x => x.FileName == "Crank Arm and Tire Maintenance.doc")
                .Select(x => database.GetDocumentStatusText(x.Status))
                .ToList();
            data[0].ShouldBeEqual("Pending approval");
        }

        /// <summary>
        /// SELECT
        ///     [q1].[FileName]
        /// FROM [Production].[Document] AS [q1]
        /// WHERE ([dbo].[ufnGetDocumentStatusText]([q1].[Status]) = N'Obsolete')
        /// </summary>
        [TestMethod]
        public void ShouldExecuteScalarFunctionInWhereClause()
        {
            var database = Environment.Container.Resolve<IDatabase>();

            var data = database.Query<Document>()
                .Where(x => database.GetDocumentStatusText(x.Status) == "Obsolete")
                .Select(x => x.FileName)
                .ToList();

            data[0].ShouldBeEqual("Training Wheels 2.doc");
        }
    }
}