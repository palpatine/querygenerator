﻿using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Custom;
using Qdarc.Queries.Tsql.Integration.Tests.Model.Person;

namespace Qdarc.Queries.Tsql.Integration.Tests
{
    [TestClass]
    public class InsertTests
    {
        [TestInitialize]
        public void Initialize()
        {
            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "DELETE FROM [custom].[UserEntity]";
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// INSERT INTO [custom].[UserEntity] (Id, Name) VALUES (1, N'name')
        /// </summary>
        [TestMethod]
        public void ShouldInsertEntityIntoTable()
        {
            var database = Environment.Container.Resolve<IDatabase>();

            var result = database.Insert(new UserEntity { Id = 1, Name = "name" });

            result.ShouldBeEqual(1);

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(1);
                    reader["Name"].ShouldBeEqual("name");
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// INSERT INTO [custom].[UserEntity] (Id, Name) VALUES (1, N'a'), (2, N'z'), (3, N'c')
        /// </summary>
        [TestMethod]
        public void ShouldInsertCollection()
        {
            var database = Environment.Container.Resolve<IDatabase>();

            var result = database.Insert(new[]
                {
                    new UserEntity { Id = 1, Name = "a" },
                    new UserEntity { Id = 2, Name = "z" },
                    new UserEntity { Id = 3, Name = "c" }
                }.AsEnumerable());

            result.ShouldBeEqual(3);

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity] ORDER BY [Id]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(1);
                    reader["Name"].ShouldBeEqual("a");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2);
                    reader["Name"].ShouldBeEqual("z");
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(3);
                    reader["Name"].ShouldBeEqual("c");
                    reader.Read().ShouldBeFalse();
                }
            }
        }

        /// <summary>
        /// INSERT INTO
        ///     [custom].[UserEntity] (Id, Name)
        /// SELECT
        ///     [q1].[AddressID] AS [Id],
        ///     [q1].[City] AS [Name]
        /// FROM [Person].[Address] AS [q1]
        /// WHERE ([q1].[AddressID] = 2)
        /// </summary>
        [TestMethod]
        public void ShouldInsertBySelection()
        {
            var database = Environment.Container.Resolve<IDatabase>();

            var query = database.Query<Address>()
                .Where(x => x.AddressId == 2)
                .Select(x => new UserEntity { Id = x.AddressId, Name = x.City });
            var result = database.Insert(query);

            result.ShouldBeEqual(1);

            using (var connection = new SqlConnection(
                ConfigurationManager.ConnectionStrings["AdventureWorks2012"].ConnectionString))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "SELECT [Id], [Name] FROM [custom].[UserEntity]";
                    var reader = command.ExecuteReader();
                    reader.Read().ShouldBeTrue();
                    reader["Id"].ShouldBeEqual(2);
                    reader["Name"].ShouldBeEqual("Bothell");
                    reader.Read().ShouldBeFalse();
                }
            }
        }
    }
}
