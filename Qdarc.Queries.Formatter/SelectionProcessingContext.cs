﻿using System;
using System.Collections.Generic;
using System.Linq;
using Qdarc.Utilities;

namespace Qdarc.Queries.Formatter
{
    public class SelectionProcessingContext : ISelectionProcessingContext
    {
        private readonly List<string> _aliasPrefix = new List<string>();

        public IEnumerable<string> AliasPrefixes => _aliasPrefix;

        public IDisposable AppendPrefix(string prefix)
        {
            _aliasPrefix.Add(prefix);

            return new Disposer(() =>
            {
                var last = _aliasPrefix.Last();
                if (last != prefix)
                {
                    throw new InvalidOperationException("Context is corrupted");
                }
                _aliasPrefix.RemoveAt(_aliasPrefix.Count - 1);
            });
        }
    }
}