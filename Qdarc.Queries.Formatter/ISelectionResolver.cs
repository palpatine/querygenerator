﻿using System.Collections.Generic;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Formatter
{
    public interface ISelectionResolver
    {
        IEnumerable<ISelectionItem> GetSelectionList(AliasedSourceItem aliasedSource, string prefix);
    }
}