using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter
{
    public abstract class FormattingHandler<TExpression> : IFormattingHandler<TExpression, FormattedQuery>
    {
        public int Priority { get; } = 0;

        protected IFormattingContext Context { get; private set; }

        public abstract FormattedQuery Handle(TExpression expression, ITransformer transformer);

        public void AttachContext(IFormattingContext context)
        {
            Context = context;
        }

        public virtual bool CanHandle(TExpression expression)
        {
            return true;
        }
    }
}