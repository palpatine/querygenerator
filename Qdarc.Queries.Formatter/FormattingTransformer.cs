﻿using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;

namespace Qdarc.Queries.Formatter
{
    public class FormattingTransformer : IFormattedQueryTransformer
    {
        private readonly IServiceProvider _resolver;
        private readonly IFormattingContext _context;

        public FormattingTransformer(IServiceProvider resolver, IFormattingContext context)
        {
            _resolver = resolver;
            _context = context;
        }

        public TResult Handle<TExpression, TResult>(TExpression expression)
        {
            var handlers = _resolver.GetServices<IFormattingHandler<TExpression, TResult>>();
            var handler = handlers.OrderBy(x => x.Priority).FirstOrDefault(x => x.CanHandle(expression));
            if (handler == null)
            {
                throw new InvalidOperationException($"There is no formatting handler of {typeof(TExpression).FullName}");
            }

            handler.AttachContext(_context);
            return handler.Handle(expression, this);
        }
    }
}