﻿using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter
{
    public interface IFormattingHandler<in TExpression, TResult> : ITransformer<TExpression, TResult, ITransformer>
    {
        int Priority { get; }

        bool CanHandle(TExpression expression);

        void AttachContext(IFormattingContext context);
    }
}