﻿using System;
using System.Collections.Generic;

namespace Qdarc.Queries.Formatter
{
    public interface ISelectionProcessingContext
    {
        IEnumerable<string> AliasPrefixes { get; }

        IDisposable AppendPrefix(string prefix);
    }
}