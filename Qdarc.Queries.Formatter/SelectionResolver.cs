﻿using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Formatter
{
    internal class SelectionResolver : ISelectionResolver
    {
        private readonly IFormattingContext _context;

        public SelectionResolver(IFormattingContext context)
        {
            _context = context;
        }

        public IEnumerable<ISelectionItem> GetSelectionList(AliasedSourceItem aliasedSource, string prefix)
        {
            var source = aliasedSource.Source;
            if (source is TableReference)
            {
                return _context.ModelToSqlConvention.GetSelectableColumns(aliasedSource)
                    .Select(x => CreateAliasedSelectionItem(prefix, x))
                    .ToArray();
            }
            else
            {
                var selectCommand = (SelectCommand)source;
                return selectCommand.Selection.Elements.Select(
                    x =>
                    {
                        var selectionItem = (AliasedSelectionItem)x;
                        return new AliasedSelectionItem(x)
                        {
                            Alias = $"{prefix}.{selectionItem.Alias}"
                        };
                    });
            }
        }

        private static AliasedSelectionItem CreateAliasedSelectionItem(
          string prefix,
          LinqColumnReference columnReference)
        {
            return new AliasedSelectionItem(columnReference)
            {
                Alias = $"{prefix}.{columnReference.Property.Name}"
            };
        }
    }
}