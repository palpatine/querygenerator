﻿using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Formatter
{
    public interface IAliasManager
    {
        string GetAlias();

        string GetAlias(IParameter query);
    }
}