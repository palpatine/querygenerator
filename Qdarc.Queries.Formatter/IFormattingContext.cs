﻿using System;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Formatter
{
    public interface IFormattingContext
    {
        IAliasManager AliasManager { get; }

        ISelectionProcessingContext CurrentSelectionProcessingContext { get; }

        IModelToSqlConvention ModelToSqlConvention { get; }

        FormattingOptions Options { get; }

        IDisposable OpenSelectionProcessingContext();
    }
}