﻿namespace Qdarc.Queries.Formatter
{
    public class FormattingOptions
    {
        public bool MakeQueryReadable { get; set; }
    }
}