﻿using System.Collections.Generic;
using System.Linq;
using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Formatter
{
    public class FormattedQuery
    {
        public FormattedQuery()
        {
            Parameters = Enumerable.Empty<IParameter>();
        }

        public IEnumerable<IParameter> Parameters { get; set; }

        public string Query { get; set; }
    }
}