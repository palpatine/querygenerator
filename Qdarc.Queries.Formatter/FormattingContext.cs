﻿using System;
using System.Collections.Generic;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Formatter
{
    public class FormattingContext : IFormattingContext
    {
        private readonly Func<IAliasManager> _aliasManagerFactory;
        private readonly Func<IModelToSqlConvention> _modelToSqlConventionFactory;
        private readonly Stack<SelectionProcessingContext> _selectonProcessingContexts = new Stack<SelectionProcessingContext>();
        private IAliasManager _aliasManager;
        private IModelToSqlConvention _modelToSqlConvention;

        public FormattingContext(
            FormattingOptions options,
            Func<IModelToSqlConvention> modelToSqlConventionFactory,
            Func<IAliasManager> aliasManagerFactory)
        {
            _modelToSqlConventionFactory = modelToSqlConventionFactory;
            _aliasManagerFactory = aliasManagerFactory;
            Options = options;
        }

        public IAliasManager AliasManager => _aliasManager ?? (_aliasManager = _aliasManagerFactory());

        public IModelToSqlConvention ModelToSqlConvention => _modelToSqlConvention ?? (_modelToSqlConvention = _modelToSqlConventionFactory());

        public FormattingOptions Options { get; }

        public ISelectionProcessingContext CurrentSelectionProcessingContext => _selectonProcessingContexts.Peek();

        public IDisposable OpenSelectionProcessingContext()
        {
            var context = new SelectionProcessingContext();
            _selectonProcessingContexts.Push(context);

            return new Disposer(() =>
            {
                var value = _selectonProcessingContexts.Pop();
                if (value != context)
                {
                    throw new InvalidOperationException("Context is corrupted");
                }
            });
        }
   }
}