﻿namespace Qdarc.Sql.Connector
{
    /// <summary>
    /// Describes the current state of the connection to a data source.
    /// </summary>
    public enum ConnectionState
    {
        Undefined = 0,

        /// <summary>
        /// The connection is closed
        /// </summary>
        Closed,

        /// <summary>
        /// The connection is open.
        /// </summary>
        Open,

        /// <summary>
        /// The connection object is connecting to the data source.
        /// </summary>
        Connecting,

        /// <summary>
        /// The connection object is executing a command.
        /// </summary>
        Executing,

        /// <summary>
        /// The connection object is retrieving data.
        /// </summary>
        Fetching,

        /// <summary>
        /// The connection to the data source is broken. This can occur only after the connection has
        /// been opened. A connection in this state may be closed and then re-opened.
        /// </summary>
        Broken
    }
}