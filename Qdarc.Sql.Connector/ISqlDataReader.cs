﻿using System;
using System.Threading.Tasks;

namespace Qdarc.Sql.Connector
{
    public interface ISqlDataReader : IDisposable
    {
        object this[int columnId] { get; }

        object this[string name] { get; }

        /// <summary>
        /// Advances the IDataReader to the next record.
        /// </summary>
        /// <returns>True if there are more rows, otherwise false.</returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        bool Read();

        /// <summary>
        /// Advances the IDataReader to the next record.
        /// </summary>
        /// <returns>True if there are more rows, otherwise false.</returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        Task<bool> ReadAsync();

        bool NextResult();

        Task<bool> NextResultAsync();
    }
}