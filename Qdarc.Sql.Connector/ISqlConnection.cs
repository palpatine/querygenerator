﻿using System;

namespace Qdarc.Sql.Connector
{
    public interface ISqlConnection
        : IDisposable
    {
        ConnectionState State { get; }

        bool Trace { get; }

        /// <summary>
        /// Starts a database transaction.
        /// </summary>
        /// <returns>An object representing the new transaction.</returns>
        /// <exception cref="SqlConnectorException">
        /// A connection-level error occurred while opening the transaction.
        /// </exception>
        /// <exception cref="ObjectDisposedException">Connection is Disposed</exception>
        ISqlTransaction BeginTransaction();

        /// <summary>
        /// Creates the SQL command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <returns>An object representing the new SqlCommand.</returns>
        /// <exception cref="ObjectDisposedException">Connection is Disposed</exception>
        ISqlCommand CreateSqlCommand(
            string command);
    }
}