﻿using System;

namespace Qdarc.Sql.Connector
{
    public class SqlConnectorException : Exception
    {
        public SqlConnectorException(
            string message,
            Exception innerException,
            string commandText = null)
            : base(message, innerException)
        {
            CommandText = commandText;
        }

        public string CommandText { get; set; }
    }
}