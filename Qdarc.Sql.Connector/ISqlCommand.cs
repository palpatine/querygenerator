﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Qdarc.Sql.Connector
{
    public interface ISqlCommand : IDisposable
    {
        IEnumerable<ISqlParameter> Parameters { get; }

        ISqlParameter AddParameter();

        /// <summary>
        /// Executes a Transact-SQL statement against the connection.
        /// </summary>
        /// <returns>The number of rows affected.</returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        int ExecuteNonQuery();

        /// <summary>
        /// Executes a Transact-SQL statement against the connection.
        /// </summary>
        /// <returns>The number of rows affected.</returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        Task<int> ExecuteNonQueryAsync();

        /// <summary>
        /// Executes a Transact-SQL statement against the connection.
        /// </summary>
        /// <returns>A SqlDataReaderWrapper object.</returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        ISqlDataReader ExecuteReader();

        /// <summary>
        /// Executes a Transact-SQL statement against the connection.
        /// </summary>
        /// <returns>A SqlDataReaderWrapper object.</returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        Task<ISqlDataReader> ExecuteReaderAsync();

        /// <summary>
        /// Executes a Transact-SQL statement against the connection.
        /// </summary>
        /// <returns>
        /// The first column of the first row in the result set, or a null reference if the result
        /// set is empty. Returns a maximum of 2033 characters.
        /// </returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        object ExecuteScalar();

        /// <summary>
        /// Executes a Transact-SQL statement against the connection.
        /// </summary>
        /// <returns>
        /// The first column of the first row in the result set, or a null reference if the result
        /// set is empty. Returns a maximum of 2033 characters.
        /// </returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        Task<object> ExecuteScalarAsync();
    }
}