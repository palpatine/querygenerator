﻿namespace Qdarc.Sql.Connector
{
    public interface ISqlConnectorExceptionProcessor
    {
        void ProcessSqlConnectorException(SqlConnectorException exception);
    }
}