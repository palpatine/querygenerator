﻿using System;
using System.Collections;

namespace Qdarc.Sql.Connector
{
    public interface ITableParameterValueConverter
    {
        IEnumerable Convert(Type itemType, IEnumerable data);
    }
}