namespace Qdarc.Sql.Connector
{
    public interface ISqlTransaction
    {
        /// <summary>
        /// Commits the database transaction.
        /// </summary>
        /// <exception cref="SqlConnectorException">
        /// An error occurred while trying to commit the transaction.
        /// </exception>
        void Commit();

        /// <summary>
        /// Rolls back a transaction from a pending state.
        /// </summary>
        /// <exception cref="SqlConnectorException">
        /// An error occurred while trying to rollback the transaction.
        /// </exception>
        void Rollback();
    }
}