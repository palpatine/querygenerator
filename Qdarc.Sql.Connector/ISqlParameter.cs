﻿using Qdarc.Queries.Model;

namespace Qdarc.Sql.Connector
{
    public interface ISqlParameter
    {
        string Name { get; set; }

        ISqlType Type { get; set; }

        object Value { get; set; }
    }
}