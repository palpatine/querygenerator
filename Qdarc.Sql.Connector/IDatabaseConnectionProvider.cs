﻿namespace Qdarc.Sql.Connector
{
    public interface IDatabaseConnectionProvider
    {
        ISqlConnection Connection { get; }
    }
}