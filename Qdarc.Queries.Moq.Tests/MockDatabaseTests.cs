﻿using System.Collections.Generic;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Moq.Tests.Model;
using Unity;

namespace Qdarc.Queries.Moq.Tests
{
    [TestClass]
    public class MockDatabaseTests
    {
        [TestMethod]
        public void ShouldCreateDatabase()
        {
            var container = new Mock<IUnityContainer>(MockBehavior.Strict);
            var subContainer = new Mock<IUnityContainer>();
            var database = new Mock<IDatabase>();
            subContainer.Setup(x => x.Resolve(typeof(IDatabase), null))
                .Returns(database.Object);
            container.Setup(x => x.CreateChildContainer()).Returns(subContainer.Object);
            var convention = new Mock<IModelToSqlConvention>();
            using (var mockDatabase = new MockDatabase(
                 container.Object,
                 convention.Object))
            {
                subContainer.Verify(x => x.Resolve(typeof(IDatabase), null), Times.Once);
                mockDatabase.Database.ShouldBeTheSameInstance(database.Object);
            }
        }

        [TestMethod]
        public void ShouldSetupValueByElementName()
        {
            var container = new Mock<IUnityContainer>(MockBehavior.Strict);
            var subContainer = new Mock<IUnityContainer>();
            var database = new Mock<IDatabase>();
            subContainer.Setup(x => x.Resolve(typeof(IDatabase), null))
                .Returns(database.Object);
            container.Setup(x => x.CreateChildContainer()).Returns(subContainer.Object);
            var convention = new Mock<IModelToSqlConvention>();
            using (var mockDatabase = new MockDatabase(
                 container.Object,
                 convention.Object))
            {
                var elementName = new ElementName("my", "name");
                var data = new List<User> { new User() };
                mockDatabase.Setup(elementName, data);
                var mockData = mockDatabase.GetData(elementName);
                mockData.ShouldNotBeNull();
                mockData[0].ShouldBeTheSameInstance(data[0]);
            }
        }

        [TestMethod]
        public void ShouldDisposeSubContainer()
        {
            var container = new Mock<IUnityContainer>(MockBehavior.Strict);
            var subContainer = new Mock<IUnityContainer>();
            var database = new Mock<IDatabase>();
            subContainer.Setup(x => x.Resolve(typeof(IDatabase), null))
                .Returns(database.Object);
            container.Setup(x => x.CreateChildContainer()).Returns(subContainer.Object);
            var convention = new Mock<IModelToSqlConvention>();
            using (var mockDatabase = new MockDatabase(
                 container.Object,
                 convention.Object))
            {
            }

            subContainer.Verify(x => x.Dispose(), Times.Once);
        }
    }
}
