﻿namespace Qdarc.Queries.Moq.Tests.Model
{
    public class User
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
