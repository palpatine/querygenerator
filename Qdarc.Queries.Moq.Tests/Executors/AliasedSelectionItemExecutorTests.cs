﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Moq.Executors;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class AliasedSelectionItemExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitAliasedSourceItem()
        {
            var executor = new AliasedSelectionItemExecutor();
            executor.CanHandle(new AliasedSelectionItem(new Mock<ISelectionItem>().Object))
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitInnerSource()
        {
            var executor = new AliasedSelectionItemExecutor();
            var innerItem = new Mock<ISelectionItem>();
            var item = new AliasedSelectionItem(innerItem.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState();
            innerItem.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(executionState);
            executor.Handle(item, context.Object);
            innerItem.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldReturnInnerState()
        {
            var executor = new AliasedSelectionItemExecutor();
            var innerItem = new Mock<ISelectionItem>();
            var item = new AliasedSelectionItem(innerItem.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState();
            innerItem.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(executionState);
            var result = executor.Handle(item, context.Object);
            result.Expression.ShouldBeTheSameInstance(executionState.Expression);
        }

        [TestMethod]
        public void ShouldSetName()
        {
            var executor = new AliasedSelectionItemExecutor();
            var innerItem = new Mock<ISelectionItem>();
            var item = new AliasedSelectionItem(innerItem.Object)
            {
                Alias = "Id"
            };
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState();
            innerItem.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(executionState);

            var result = executor.Handle(item, context.Object);

            result.Name.ShouldBeEqual("Id");
        }
    }
}
