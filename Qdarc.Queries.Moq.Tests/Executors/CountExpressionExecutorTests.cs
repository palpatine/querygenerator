﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Queries.Moq.Helpers;
using Qdarc.Queries.Moq.Tests.Model;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class CountExpressionExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitConstantExpression()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            executor.CanHandle(new SqlCountExpression(new Mock<ISqlValueExpression>().Object, false, null, null)).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldGenerateCountExpression()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                false,
                null,
                null);
            var parameter = Expression.Parameter(typeof(User));
            var valueExpression = Expression.Property(parameter, "Name");
            value.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = valueExpression });
            parameterReplacer.Setup(x => x.Replace(valueExpression))
                .Returns(valueExpression);
            parameterReplacer.Setup(x => x.Parameters).Returns(new[] { parameter });

            var result = executor.Handle(expression, context.Object);

            result.IsAggregate.ShouldBeTrue();
            result.ExpressionFactory.ShouldNotBeNull();
            result.Expression.ShouldBeNull();
            var users = new List<User> { new User(), new User(), new User() };
            var coreExpression = Expression.Constant(users);
            var resultExpression = result.ExpressionFactory(coreExpression);
            var lambda = Expression.Lambda<Func<int>>(resultExpression);
            var resultValue = lambda.Compile()();
            resultValue.ShouldBeEqual(0);
        }

        [TestMethod]
        public void ShouldVisitValue()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                false,
                null,
                null);
            var parameter = Expression.Parameter(typeof(User));
            var valueExpression = Expression.Property(parameter, "Name");
            value.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = valueExpression });
            parameterReplacer.Setup(x => x.Replace(valueExpression))
                .Returns(valueExpression);
            parameterReplacer.Setup(x => x.Parameters).Returns(new[] { parameter });

            executor.Handle(expression, context.Object);

            value.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldReplaceParameters()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                false,
                null,
                null);
            var parameter = Expression.Parameter(typeof(User));
            var valueExpression = Expression.Property(parameter, "Name");
            value.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = valueExpression });
            parameterReplacer.Setup(x => x.Replace(valueExpression))
                .Returns(valueExpression);
            parameterReplacer.Setup(x => x.Parameters).Returns(new[] { parameter });

            executor.Handle(expression, context.Object);

            parameterReplacer.Verify(x => x.Replace(valueExpression), Times.Once);
        }

        [TestMethod]
        public void ShouldNotReplaceParametersIfValueIsNotNullable()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                false,
                null,
                null);
            var parameter = Expression.Parameter(typeof(User));
            var valueExpression = Expression.Property(parameter, "Id");
            value.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = valueExpression });

            executor.Handle(expression, context.Object);

            parameterReplacer.Verify(x => x.Replace(valueExpression), Times.Never);
        }

        [TestMethod]
        public void ShouldGenerateCountExpressionForNotNullableValue()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                false,
                null,
                null);
            var parameter = Expression.Parameter(typeof(User));
            var valueExpression = Expression.Property(parameter, "Id");
            value.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = valueExpression });

            var result = executor.Handle(expression, context.Object);

            result.IsAggregate.ShouldBeTrue();
            result.ExpressionFactory.ShouldNotBeNull();
            result.Expression.ShouldBeNull();
            var users = new List<User> { new User(), new User(), new User() };
            var coreExpression = Expression.Constant(users);
            var resultExpression = result.ExpressionFactory(coreExpression);
            var methodCall = resultExpression.ShouldBeInstanceOf<MethodCallExpression>();
            methodCall.Method.ShouldBeEqual(
                ExpressionExtensions.GetMethod(() => Enumerable.Count<User>(null)));
        }

        [TestMethod]
        public void ShouldGenerateCountWhereExpressionForNullableValue()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                false,
                null,
                null);
            var parameter = Expression.Parameter(typeof(User));
            var valueExpression = Expression.Property(parameter, "Name");
            value.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = valueExpression });
            parameterReplacer.Setup(x => x.Replace(valueExpression))
               .Returns(valueExpression);
            parameterReplacer.Setup(x => x.Parameters).Returns(new[] { parameter });

            var result = executor.Handle(expression, context.Object);

            result.IsAggregate.ShouldBeTrue();
            result.ExpressionFactory.ShouldNotBeNull();
            result.Expression.ShouldBeNull();
            var users = new List<User> { new User(), new User(), new User() };
            var coreExpression = Expression.Constant(users);
            var resultExpression = result.ExpressionFactory(coreExpression);
            var methodCall = resultExpression.ShouldBeInstanceOf<MethodCallExpression>();
            methodCall.Method.ShouldBeEqual(
                ExpressionExtensions.GetMethod(() => Enumerable.Count<User>(null, x => true)));
        }

        [TestMethod]
        public void ShouldCountNotNullValues()
        {
            var parameterReplacer = new Mock<IParameterReplacer>();
            var executor = new CountExpressionExecutor(parameterReplacer.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                false,
                null,
                null);
            var parameter = Expression.Parameter(typeof(User));
            var valueExpression = Expression.Property(parameter, "Name");
            value.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = valueExpression });
            parameterReplacer.Setup(x => x.Replace(valueExpression))
               .Returns(valueExpression);
            parameterReplacer.Setup(x => x.Parameters).Returns(new[] { parameter });

            var result = executor.Handle(expression, context.Object);

            result.IsAggregate.ShouldBeTrue();
            result.ExpressionFactory.ShouldNotBeNull();
            result.Expression.ShouldBeNull();
            var users = new List<User> { new User(), new User { Name = "a" }, new User() };
            var coreExpression = Expression.Constant(users);
            var resultExpression = result.ExpressionFactory(coreExpression);
            var lambda = Expression.Lambda<Func<int>>(resultExpression);
            var resultValue = lambda.Compile()();
            resultValue.ShouldBeEqual(1);
        }
    }
}
