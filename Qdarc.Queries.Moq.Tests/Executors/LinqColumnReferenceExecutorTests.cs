﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Queries.Moq.Tests.Model;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class LinqColumnReferenceExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitInlineIfExpression()
        {
            var executor = new LinqColumnReferenceExecutor();
            executor.CanHandle(new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((User u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object)).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldCreatePropertyAccessToSourceItem()
        {
            var executor = new LinqColumnReferenceExecutor();
            var source = new Mock<INamedSource>();
            var expression = new LinqColumnReference(
                source.Object,
                ExpressionExtensions.GetProperty((User u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var context = new Mock<IMockSourceExecutionContext>();

            var parameter = Expression.Parameter(typeof(User));
            context.Setup(x => x.GetParameterMapping(source.Object))
                .Returns(parameter);
            var result = executor.Handle(expression, context.Object);

            var memberAccess = result.Expression.ShouldBeInstanceOf<MemberExpression>();
            memberAccess.Expression.ShouldBeTheSameInstance(parameter);
            memberAccess.Member.ShouldBeTheSameInstance(expression.Property);
        }
    }
}
