﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Moq.Executors;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class AliasedSourceItemExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitAliasedSourceItem()
        {
            var executor = new AliasedSourceItemExecutor();
            executor.CanHandle(new AliasedSourceItem(new Mock<ISource>().Object))
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitInnerSource()
        {
            var executor = new AliasedSourceItemExecutor();
            var source = new Mock<INamedSource>();
            var item = new AliasedSourceItem(source.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            executor.Handle(item, context.Object);
            source.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldReturnInnerState()
        {
            var executor = new AliasedSourceItemExecutor();
            var source = new Mock<INamedSource>();
            var item = new AliasedSourceItem(source.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState();
            source.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(executionState);
            var result = executor.Handle(item, context.Object);
            result.ShouldBeTheSameInstance(executionState);
        }
    }
}
