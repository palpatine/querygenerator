﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Queries.Moq.Tests.Model;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class TableReferenceExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitTableReference()
        {
            var database = new Mock<IMockDatabase>();
            var executor = new TableReferenceExecutor(database.Object);
            executor.CanHandle(new TableReference(new ElementName("Name"), typeof(User)))
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldRetrieveDataForAppropriateElement()
        {
            var database = new Mock<IMockDatabase>();
            var executor = new TableReferenceExecutor(database.Object);
            var item = new TableReference(
                new ElementName("my", "name"),
                typeof(User));
            var list = new List<User>();
            database.Setup(x => x.GetData(item.Name)).Returns(list);
            var context = new Mock<IMockSourceExecutionContext>();
            var result = executor.Handle(item, context.Object);
            database.Verify(x => x.GetData(item.Name), Times.Once);
            var constant = result.Expression.ShouldBeInstanceOf<ConstantExpression>();
            constant.Value.ShouldBeTheSameInstance(list);
        }
    }
}
