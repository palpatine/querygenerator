﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Moq.Executors;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class ConstantExpressionExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitConstantExpression()
        {
            var executor = new ConstantExpressionExecutor();
            var sqlConstantExpression = new SqlConstantExpression(1, typeof(int), new Mock<ISqlType>().Object);
            executor.CanHandle(sqlConstantExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldReturnConstantExpression()
        {
            var executor = new ConstantExpressionExecutor();
            var conetxt = new Mock<IMockSourceExecutionContext>();
            var expression = new SqlConstantExpression(3, typeof(int), new Mock<ISqlType>().Object);

            var result = executor.Handle(expression, conetxt.Object);

            result.ShouldNotBeNull();
            var constant = result.Expression.ShouldBeInstanceOf<ConstantExpression>();
            constant.Value.ShouldBeEqual(3);
        }
    }
}
