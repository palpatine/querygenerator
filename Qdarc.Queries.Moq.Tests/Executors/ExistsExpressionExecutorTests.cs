﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Queries.Moq.Tests.Model;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class ExistsExpressionExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitExistsExpression()
        {
            var executor = new ExistsExpressionExecutor();
            executor.CanHandle(new ExistsExpression(new Mock<ISelectCommand>().Object, false)).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitInnerSelection()
        {
            var executor = new ExistsExpressionExecutor();
            var conetxt = new Mock<IMockSourceExecutionContext>();
            var selection = new Mock<ISelectCommand>();
            var expression = new ExistsExpression(selection.Object, false);
            var inner = Expression.Constant(new List<User>());
            selection.Setup(x => x.Accept<ExecutionState>(conetxt.Object)).Returns(new ExecutionState { Expression = inner });

            executor.Handle(expression, conetxt.Object);

            selection.Verify(x => x.Accept<ExecutionState>(conetxt.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldReturnCallToEnumerableAnyMethodOnTheInnerSelection()
        {
            var executor = new ExistsExpressionExecutor();
            var conetxt = new Mock<IMockSourceExecutionContext>();
            var selection = new Mock<ISelectCommand>();
            var expression = new ExistsExpression(selection.Object, false);
            var inner = Expression.Constant(new List<User>());
            selection.Setup(x => x.Accept<ExecutionState>(conetxt.Object)).Returns(new ExecutionState { Expression = inner });

            var result = executor.Handle(expression, conetxt.Object);

            result.ShouldNotBeNull();
            var call = result.Expression.ShouldBeInstanceOf<MethodCallExpression>();
            call.Method.ShouldBeEqual(ExpressionExtensions.GetMethod(() => Enumerable.Any<User>(null)));
            call.Object.ShouldBeNull();
            call.Arguments.Count.ShouldBeEqual(1);
            call.Arguments.Single().ShouldBeTheSameInstance(inner);
        }

        [TestMethod]
        public void ShouldReturnNegatedCallToEnumerableAnyMethodOnTheInnerSelectionIfExpressionIsNegated()
        {
            var executor = new ExistsExpressionExecutor();
            var conetxt = new Mock<IMockSourceExecutionContext>();
            var selection = new Mock<ISelectCommand>();
            var expression = new ExistsExpression(selection.Object, true);
            var inner = Expression.Constant(new List<User>());
            selection.Setup(x => x.Accept<ExecutionState>(conetxt.Object))
                .Returns(new ExecutionState { Expression = inner });

            var result = executor.Handle(expression, conetxt.Object);

            result.ShouldNotBeNull();
            var negation = result.Expression.ShouldBeInstanceOf<UnaryExpression>();
            negation.NodeType.ShouldBeEqual(ExpressionType.Not);
            var call = negation.Operand.ShouldBeInstanceOf<MethodCallExpression>();
            call.Method.ShouldBeEqual(ExpressionExtensions.GetMethod(() => Enumerable.Any<User>(null)));
            call.Object.ShouldBeNull();
            call.Arguments.Count.ShouldBeEqual(1);
            call.Arguments.Single().ShouldBeTheSameInstance(inner);
        }
    }
}
