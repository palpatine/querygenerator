﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Queries.Moq.Tests.Model;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class SelectionExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitSqlSelection()
        {
            var executor = new SelectionExecutor();
            executor.CanHandle(new SqlSelection(Enumerable.Empty<ISelectionItem>()))
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldProcessSelectionItemsIfNoStateIsGiven()
        {
            var executor = new SelectionExecutor();
            var selectionItem = new Mock<ISelectionItem>();
            var item = new SqlSelection(new[] { selectionItem.Object }, typeof(int));
            var context = new Mock<IMockSourceExecutionContext>();
            var selection = Expression.Constant(1);
            selectionItem.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(new ExecutionState { Expression = selection });

            executor.Handle(item, context.Object);

            selectionItem.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldThrowWhenIntegerResultIsExpectedAndMultipleSelectionIsProvided()
        {
            var executor = new SelectionExecutor();
            var selectionItem1 = new Mock<ISelectionItem>().Object;
            var selectionItem2 = new Mock<ISelectionItem>().Object;
            var item = new SqlSelection(new[] { selectionItem1, selectionItem2 }, typeof(int));

            var context = new Mock<IMockSourceExecutionContext>();

            QAssert.ShouldThrow<InvalidModelException>(() => executor.Handle(item, context.Object));
        }

        [TestMethod]
        public void ShouldThrowWhenBooleanResultIsExpectedAndMultipleSelectionIsProvided()
        {
            var executor = new SelectionExecutor();
            var selectionItem1 = new Mock<ISelectionItem>().Object;
            var selectionItem2 = new Mock<ISelectionItem>().Object;
            var item = new SqlSelection(new[] { selectionItem1, selectionItem2 }, typeof(bool));

            var context = new Mock<IMockSourceExecutionContext>();

            QAssert.ShouldThrow<InvalidModelException>(() => executor.Handle(item, context.Object));
        }

        [TestMethod]
        public void ShouldResolveSingleSelectionItemIfSingleSelectionResultIsExpected()
        {
            var executor = new SelectionExecutor();
            var selectionItem = new Mock<ISelectionItem>();
            var item = new SqlSelection(new[] { selectionItem.Object }, typeof(int));
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState { Expression = Expression.Constant(2) };
            selectionItem.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(executionState);

            var result = executor.Handle(item, context.Object);

            result.Expression.ShouldBeTheSameInstance(executionState.Expression);
        }

        [TestMethod]
        public void ShouldCreateProjectionByPropertyAssignment()
        {
            var executor = new SelectionExecutor();
            var selectionItem = new Mock<ISelectionItem>();
            var item = new SqlSelection(new[] { selectionItem.Object }, typeof(User));
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState { Expression = Expression.Constant(2), Name = "Id" };
            selectionItem.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(executionState);

            var result = executor.Handle(item, context.Object);

            var blockExpression = result.Expression.ShouldBeInstanceOf<BlockExpression>();
            var lambda = Expression.Lambda<Func<User>>(blockExpression);
            var mapedUser = lambda.Compile()();
            mapedUser.ShouldNotBeNull();
            mapedUser.Id.ShouldBeEqual(2);
        }

        [TestMethod]
        public void ShouldCreateProjectionAndElevateTypeToNullable()
        {
            var executor = new SelectionExecutor();
            var selectionItem = new Mock<ISelectionItem>();
            var item = new SqlSelection(new[] { selectionItem.Object }, typeof(int?));
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState { Expression = Expression.Constant(2) };
            selectionItem.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(executionState);

            var result = executor.Handle(item, context.Object);

            var unaryExpression = result.Expression.ShouldBeInstanceOf<UnaryExpression>();
            unaryExpression.Type.ShouldBeEqual(typeof(int?));
            var lambda = Expression.Lambda<Func<int?>>(unaryExpression);
            var mapedId = lambda.Compile()();
            mapedId.ShouldNotBeNull();
            mapedId.ShouldBeEqual(2);
        }

        [TestMethod]
        public void ShouldCreateProjectionAndConvertType()
        {
            var executor = new SelectionExecutor();
            var selectionItem = new Mock<ISelectionItem>();
            var item = new SqlSelection(new[] { selectionItem.Object }, typeof(long));
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState { Expression = Expression.Constant(2) };
            selectionItem.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(executionState);

            var result = executor.Handle(item, context.Object);

            var unaryExpression = result.Expression.ShouldBeInstanceOf<UnaryExpression>();
            unaryExpression.Type.ShouldBeEqual(typeof(long));
            var lambda = Expression.Lambda<Func<long>>(unaryExpression);
            var mapedId = lambda.Compile()();
            mapedId.ShouldBeEqual(2);
        }

        [TestMethod]
        public void ShouldHandleSingleSelectionAggregate()
        {
            var executor = new SelectionExecutor();
            var selectionItem = new Mock<ISelectionItem>();
            var item = new SqlSelection(new[] { selectionItem.Object }, typeof(long));
            var context = new Mock<IMockSourceExecutionContext>();
            var executionState = new ExecutionState
            {
                IsAggregate = true,
            };
            selectionItem.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(executionState);

            var result = executor.Handle(item, context.Object);

            result.ShouldBeTheSameInstance(executionState);
        }
    }
}
