﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Moq.Executors;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class CastExpressionExecutorTests
    {
        [TestMethod]
        public void ShouldHandleSqlCastExpression()
        {
            var exectutor = new CastExpressionExecutor();
            var sqlCastExpression = new SqlCastExpression(
                new Mock<ISqlValueExpression>().Object,
                typeof(int),
                new Mock<ISqlType>().Object);

            exectutor.CanHandle(sqlCastExpression)
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitCastedExpression()
        {
            var exectutor = new CastExpressionExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var castedExpression = new Mock<ISqlValueExpression>();
            var castedResult = new ExecutionState
            {
                Expression = Expression.Constant(1)
            };
            castedExpression.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(castedResult);
            var expression = new SqlCastExpression(
                castedExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            exectutor.Handle(expression, context.Object);

            castedExpression.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldReturnExpressionStateContainingAppropriateCast()
        {
            var exectutor = new CastExpressionExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var castedExpression = new Mock<ISqlValueExpression>();
            var castedResult = new ExecutionState
            {
                Expression = Expression.Constant(1)
            };
            castedExpression.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(castedResult);
            var expression = new SqlCastExpression(
                castedExpression.Object,
                typeof(long),
                new Mock<ISqlType>().Object);
            var result = exectutor.Handle(expression, context.Object);
            result.ShouldNotBeNull();
            var unaryExpression = result.Expression.ShouldBeInstanceOf<UnaryExpression>();
            unaryExpression.Operand.ShouldBeTheSameInstance(castedResult.Expression);
            unaryExpression.Type.ShouldBeEqual(typeof(long));
        }

        [TestMethod]
        public void ShouldCorrectlyHandleCastingBitToBoolean()
        {
            var exectutor = new CastExpressionExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var castedExpression = new Mock<ISqlValueExpression>();
            var castedResult = new ExecutionState
            {
                Expression = Expression.Constant(1)
            };
            castedExpression.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(castedResult);
            var expression = new SqlCastExpression(
                castedExpression.Object,
                typeof(bool),
                new Mock<ISqlType>().Object);
            var result = exectutor.Handle(expression, context.Object);
            result.ShouldNotBeNull();
            var binaryExpression = result.Expression.ShouldBeInstanceOf<BinaryExpression>();
            binaryExpression.Left.ShouldBeTheSameInstance(castedResult.Expression);
            var constant = binaryExpression.Right.ShouldBeInstanceOf<ConstantExpression>();
            constant.Value.ShouldBeEqual(1);
            binaryExpression.NodeType.ShouldBeEqual(ExpressionType.Equal);
        }
    }
}
