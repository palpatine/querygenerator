﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Queries.Moq.Tests.Model;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class SelectCommandExecutorTests
    {
        private static readonly MethodInfo Count =
            ExpressionExtensions.GetMethod(() => Enumerable.Count<int>(null))
                .GetGenericMethodDefinition();

        [TestMethod]
        public void ShouldBeAbleToVisitSelectCommand()
        {
            var executor = new SelectCommandExecutor();
            executor.CanHandle(new SelectCommand())
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldGenerateParameterExpressionForSelectionResolution()
        {
            var executor = new SelectCommandExecutor();
            var source = new Mock<INamedSource>();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand(
                source.Object,
                selection.Object);
            var sourceVisitationResult = new ExecutionState
            {
                Expression = Expression.Constant(new List<User>())
            };
            var context = new Mock<IMockSourceExecutionContext>();
            source.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(sourceVisitationResult);
            ParameterExpression parameter = null;
            context.Setup(
                    x => x.AddParameterMapping(
                        item.Source,
                        It.IsAny<ParameterExpression>()))
                .Callback((ISource s, ParameterExpression p) => parameter = p);
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    (IMockSourceExecutionContext x) => new ExecutionState
                    {
                        Expression = parameter
                    });
            executor.Handle(item, context.Object);
            context.Verify(
                x => x.AddParameterMapping(
                    item.Source,
                    It.Is<ParameterExpression>(z => z.Type == typeof(User))),
                Times.Once);
            context.Verify(x => x.RemoveParameterMapping(item.Source), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitSource()
        {
            var executor = new SelectCommandExecutor();
            var source = new Mock<INamedSource>();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand(
                source.Object,
                selection.Object);
            var sourceVisitationResult = new ExecutionState
            {
                Expression = Expression.Constant(new List<User>())
            };
            var context = new Mock<IMockSourceExecutionContext>();
            ParameterExpression parameter = null;
            context.Setup(
                    x => x.AddParameterMapping(
                        item.Source,
                        It.IsAny<ParameterExpression>()))
                .Callback((ISource s, ParameterExpression p) => parameter = p);
            source.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(sourceVisitationResult);
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    (IMockSourceExecutionContext x) => new ExecutionState
                    {
                        Expression = parameter
                    });
            executor.Handle(item, context.Object);
            source.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldNotVisitSourceIfItIsNotDefined()
        {
            var executor = new SelectCommandExecutor();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand(
                null,
                selection.Object);
            var context = new Mock<IMockSourceExecutionContext>();
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    new ExecutionState
                    {
                        Expression = Expression.Constant(3)
                    });

            executor.Handle(item, context.Object);
        }

        [TestMethod]
        public void ShouldVisitSelection()
        {
            var executor = new SelectCommandExecutor();
            var source = new Mock<INamedSource>();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand(
                source.Object,
                selection.Object);
            var sourceVisitationResult = new ExecutionState
            {
                Expression = Expression.Constant(new List<User>())
            };
            var context = new Mock<IMockSourceExecutionContext>();
            ParameterExpression parameter = null;
            context.Setup(
                    x => x.AddParameterMapping(
                        item.Source,
                        It.IsAny<ParameterExpression>()))
                .Callback((ISource s, ParameterExpression p) => parameter = p);
            source.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(sourceVisitationResult);
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    (IMockSourceExecutionContext x) => new ExecutionState
                    {
                        Expression = parameter
                    });

            executor.Handle(item, context.Object);
            selection.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldExecuteSelection()
        {
            var executor = new SelectCommandExecutor();
            var source = new Mock<INamedSource>();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand(
                source.Object,
                selection.Object);
            var users = new List<User>
            {
                new User
                {
                    Id = 1
                }
            };
            var sourceVisitationResult = new ExecutionState
            {
                Expression = Expression.Constant(users)
            };
            var context = new Mock<IMockSourceExecutionContext>();
            ParameterExpression parameter = null;
            context.Setup(
                    x => x.AddParameterMapping(
                        item.Source,
                        It.IsAny<ParameterExpression>()))
                .Callback((ISource s, ParameterExpression p) => parameter = p);
            source.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(sourceVisitationResult);
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    (IMockSourceExecutionContext x) => new ExecutionState
                    {
                        Expression = parameter
                    });

            var result = executor.Handle(item, context.Object);
            var lambda = Expression.Lambda<Func<IList>>(result.Expression);
            var mappedResult = lambda.Compile()();
            var mappedUsers = mappedResult.ShouldBeInstanceOf<List<User>>();
            mappedUsers[0].ShouldBeTheSameInstance(users[0]);
        }

        [TestMethod]
        public void ShouldExecuteSelectionAfterProjectionChangingType()
        {
            var executor = new SelectCommandExecutor();
            var source = new Mock<INamedSource>();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand(
                source.Object,
                selection.Object);
            var users = new List<User>
            {
                new User
                {
                    Id = 1
                }
            };
            var sourceVisitationResult = new ExecutionState
            {
                Expression = Expression.Constant(users)
            };
            var context = new Mock<IMockSourceExecutionContext>();
            ParameterExpression parameter = null;
            context.Setup(
                    x => x.AddParameterMapping(
                        item.Source,
                        It.IsAny<ParameterExpression>()))
                .Callback((ISource s, ParameterExpression p) => parameter = p);
            source.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(sourceVisitationResult);
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    (IMockSourceExecutionContext x) => new ExecutionState
                    {
                        Expression = Expression.Property(
                            parameter,
                            ExpressionExtensions.GetProperty((User u) => u.Id))
                    });

            var result = executor.Handle(item, context.Object);
            var lambda = Expression.Lambda<Func<IList>>(result.Expression);
            var mappedResult = lambda.Compile()();
            var mappedIds = mappedResult.ShouldBeInstanceOf<List<int>>();
            mappedIds[0].ShouldBeEqual(users[0].Id);
        }

        [TestMethod]
        public void ShouldHandleSingleSelectionWithoutSource()
        {
            var executor = new SelectCommandExecutor();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand
            {
                Selection = selection.Object
            };
            var context = new Mock<IMockSourceExecutionContext>();
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    new ExecutionState
                    {
                        Expression = Expression.Constant(3)
                    });

            var result = executor.Handle(item, context.Object);

            var arrayCreation = result.Expression.ShouldBeInstanceOf<NewArrayExpression>();
            var lambda = Expression.Lambda<Func<IList>>(arrayCreation);
            var mapedId = lambda.Compile()();
            mapedId[0].ShouldBeEqual(3);
        }

        [TestMethod]
        public void ShouldHandleSingleSelectionAggregate()
        {
            var executor = new SelectCommandExecutor();
            var source = new Mock<INamedSource>();
            var selection = new Mock<ISqlSelection>();
            var item = new SelectCommand(
                source.Object,
                selection.Object);
            var users = new List<User>
            {
                new User
                {
                    Name = "A"
                },
                new User
                {
                    Id = 1
                },
                new User
                {
                    Name = "B"
                }
            };
            var sourceVisitationResult = new ExecutionState
            {
                Expression = Expression.Constant(users)
            };
            var context = new Mock<IMockSourceExecutionContext>();
            ParameterExpression parameter = null;
            context.Setup(
                    x => x.AddParameterMapping(
                        item.Source,
                        It.IsAny<ParameterExpression>()))
                .Callback((ISource s, ParameterExpression p) => parameter = p);
            source.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(sourceVisitationResult);
            selection.Setup(x => x.Accept<ExecutionState>(context.Object))
                .Returns(
                    new ExecutionState
                    {
                        IsAggregate = true,
                        ExpressionFactory = x =>
                        {
                            var countMethod = Count.MakeGenericMethod(x.Type.GenericTypeArguments[0]);
                            return Expression.Call(null, countMethod, x);
                        }
                    });

            var result = executor.Handle(item, context.Object);

            var arrayCreation = result.Expression.ShouldBeInstanceOf<NewArrayExpression>();
            var lambda = Expression.Lambda<Func<IList>>(arrayCreation);
            var mapedId = lambda.Compile()();
            mapedId[0].ShouldBeEqual(3);
        }
    }
}