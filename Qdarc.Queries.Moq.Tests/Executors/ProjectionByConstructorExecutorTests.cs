﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Moq.Executors;
using Qdarc.Utilities;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class ProjectionByConstructorExecutorTests
    {
        private static readonly ConstructorInfo Constructor
            = ExpressionExtensions.GetConstructor(() => new Tuple<int, int>(1, 2));

        [TestMethod]
        public void ShouldBeAbleToVisitProjectionByConstructor()
        {
            var executor = new ProjectionByConstructorExecutor();
            executor.CanHandle(new ProjectionByConstructor(Constructor, new[] { new Mock<ISelectionItem>().Object, new Mock<ISelectionItem>().Object }))
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitConstructorParameters()
        {
            var executor = new ProjectionByConstructorExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var item1 = new Mock<ISqlValueExpression>();
            var item2 = new Mock<ISqlValueExpression>();
            var item1Expression = Expression.Constant(2);
            var item2Expression = Expression.Constant(-2);
            item1.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = item1Expression });
            item2.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = item2Expression });
            var expression = new ProjectionByConstructor(Constructor, new[] { item1.Object, item2.Object });

            executor.Handle(expression, context.Object);

            item1.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
            item2.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldCreateConstructorCall()
        {
            var executor = new ProjectionByConstructorExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var item1 = new Mock<ISqlValueExpression>();
            var item2 = new Mock<ISqlValueExpression>();
            var item1Expression = Expression.Constant(2);
            var item2Expression = Expression.Constant(-2);
            item1.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = item1Expression });
            item2.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = item2Expression });
            var expression = new ProjectionByConstructor(Constructor, new[] { item1.Object, item2.Object });

            var result = executor.Handle(expression, context.Object);

            var newExpression = result.Expression.ShouldBeInstanceOf<NewExpression>();
            newExpression.Constructor.ShouldBeTheSameInstance(Constructor);
            newExpression.Arguments.Count.ShouldBeEqual(2);
            newExpression.Arguments[0].ShouldBeTheSameInstance(item1Expression);
            newExpression.Arguments[1].ShouldBeTheSameInstance(item2Expression);
        }
    }
}
