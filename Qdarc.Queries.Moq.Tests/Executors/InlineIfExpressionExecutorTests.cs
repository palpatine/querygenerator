﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Moq.Executors;

namespace Qdarc.Queries.Moq.Tests.Executors
{
    [TestClass]
    public class InlineIfExpressionExecutorTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitInlineIfExpression()
        {
            var executor = new InlineIfExpressionExecutor();
            var sqlInlineIfExpression = new SqlInlineIfExpression(
                new Mock<ISqlLogicalExpression>().Object,
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object,
                null,
                null);
            executor.CanHandle(sqlInlineIfExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitCondition()
        {
            var executor = new InlineIfExpressionExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var onTrueExpression = Expression.Constant(1);
            var onFalseExpression = Expression.Constant(0);
            var testExpression = Expression.Constant(true);
            ifTrue.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onTrueExpression });
            ifFalse.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onFalseExpression });
            condition.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = testExpression });

            var expression = new SqlInlineIfExpression(
                                condition.Object,
                                ifTrue.Object,
                                ifFalse.Object,
                                typeof(int),
                                null);

            executor.Handle(expression, context.Object);

            condition.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitNegativeBranch()
        {
            var executor = new InlineIfExpressionExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var onTrueExpression = Expression.Constant(1);
            var onFalseExpression = Expression.Constant(0);
            var testExpression = Expression.Constant(true);
            ifTrue.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onTrueExpression });
            ifFalse.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onFalseExpression });
            condition.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = testExpression });

            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                null);

            executor.Handle(expression, context.Object);

            ifFalse.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitPositiveBranch()
        {
            var executor = new InlineIfExpressionExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var onTrueExpression = Expression.Constant(1);
            var onFalseExpression = Expression.Constant(0);
            var testExpression = Expression.Constant(true);
            ifTrue.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onTrueExpression });
            ifFalse.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onFalseExpression });
            condition.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = testExpression });

            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                null);

            executor.Handle(expression, context.Object);

            ifTrue.Verify(x => x.Accept<ExecutionState>(context.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldCreateConditionalExpression()
        {
            var executor = new InlineIfExpressionExecutor();
            var context = new Mock<IMockSourceExecutionContext>();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var onTrueExpression = Expression.Constant(1);
            var onFalseExpression = Expression.Constant(0);
            var testExpression = Expression.Constant(true);
            ifTrue.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onTrueExpression });
            ifFalse.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = onFalseExpression });
            condition.Setup(x => x.Accept<ExecutionState>(context.Object)).Returns(new ExecutionState { Expression = testExpression });

            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                null);

            var result = executor.Handle(expression, context.Object);
            result.ShouldNotBeNull();
            var conditional = result.Expression.ShouldBeInstanceOf<ConditionalExpression>();
            conditional.IfFalse.ShouldBeTheSameInstance(onFalseExpression);
            conditional.IfTrue.ShouldBeTheSameInstance(onTrueExpression);
            conditional.Test.ShouldBeTheSameInstance(testExpression);
        }
    }
}
