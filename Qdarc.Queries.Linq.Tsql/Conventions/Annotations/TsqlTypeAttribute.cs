﻿using System;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql.Conventions.Annotations
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1019:DefineAccessorsForAttributeArguments", Justification = "Properties exists but are nullable.")]
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method)]
    public sealed class TsqlTypeAttribute : Attribute, IConvertibleToSqlType
    {
        public TsqlTypeAttribute(string name, bool isNullable)
        {
            Name = name;
            IsNullable = isNullable;
        }

        public TsqlTypeAttribute(string name, bool isNullable, int capacity)
        {
            Name = name;
            Capacity = capacity;
            IsNullable = isNullable;
        }

        public TsqlTypeAttribute(string name, bool isNullable, byte precision, byte scale)
        {
            Name = name;
            Scale = scale;
            IsNullable = isNullable;
            Precision = precision;
        }

        public int? Capacity { get; }

        public bool IsNullable { get; }

        public string Name { get; }

        public byte? Precision { get; }

        public byte? Scale { get; }

        public ISqlType SqlType
        {
            get
            {
                if (Capacity.HasValue)
                {
                    return new TsqlType(Name, IsNullable, Capacity.Value);
                }

                if (Precision != null && Scale != null)
                {
                    return new TsqlType(Name, IsNullable, Precision.Value, Scale.Value);
                }

                return new TsqlType(Name);
            }
        }

        public string TypeAliasName { get; set; }
    }
}