﻿using System;

namespace Qdarc.Queries.Linq.Tsql.Conventions.Annotations
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class TsqlComputedColumnAttribute : Attribute
    {
    }
}