﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Tsql.Conventions
{
    public class AttributesAnnotationModelToSqlConvention : IModelToSqlConvention
    {
        public string CompoundColumnSeparator { get; } = ".";

        public ElementName GetColumnName(INamedSource source, PropertyInfo property)
        {
            return new ElementName(source.Name.Concat(new[] { GetColumnName(property) }).ToArray());
        }

        public ElementName GetColumnName(
            INamedSource source,
            IEnumerable<PropertyInfo> properties)
        {
            var columnName = string.Join(CompoundColumnSeparator, properties.Select(x => x.Name));

            return new ElementName(source.Name.Concat(new[] { columnName }).ToArray());
        }

        public string GetColumnName(PropertyInfo propertyInfo)
        {
            return propertyInfo.GetCustomAttribute<SqlColumnNameAttribute>()?.Name ?? propertyInfo.Name;
        }

        public IEnumerable<LinqColumnReference> GetInsertableColumns(Type type)
        {
            var source = new TableReference(
                GetTableName(type),
                type);
            return type.GetRuntimeProperties()
                .Where(IsInsertable)
                .OrderBy(x => x.GetCustomAttribute<SqlColumnOrderAttribute>()?.Ordinal ?? int.MaxValue)
                .ThenBy(x => x.Name)
                .Select(x => new LinqColumnReference(source, x, x.PropertyType, x.GetCustomAttribute<TsqlTypeAttribute>()?.SqlType))
                .ToArray();
        }

        public IEnumerable<LinqColumnReference> GetSelectableColumns(INamedSource source)
        {
            return GetSelectableProperties(source.ClrType)
                .Select(x =>
                       new LinqColumnReference(source, x, x.PropertyType, x.GetCustomAttribute<TsqlTypeAttribute>()?.SqlType))
                .ToArray();
        }

        public IEnumerable<PropertyInfo> GetSelectableProperties(Type source)
        {
            return source.GetRuntimeProperties()
                .Where(IsSelectable)
                .OrderBy(x => x.GetCustomAttribute<SqlColumnOrderAttribute>()?.Ordinal ?? int.MaxValue)
                .ThenBy(x => x.Name)
                .ToArray();
        }

        public ISqlType GetSqlType(
            Type entityType,
            PropertyInfo property)
        {
            return TryGetSqlType(entityType, property)
                ?? throw new AnnotationException($"Cannot determine sql source for property that is not annotated. Type {entityType.FullName}, Property: {property.Name}.");
        }

        public ISqlType TryGetSqlType(
            Type entityType,
            PropertyInfo property)
        {
            PropertyInfo mappedProperty = property;
            if (property.DeclaringType.GetTypeInfo().IsInterface)
            {
                var typeInfo = entityType.GetTypeInfo();
                var map = typeInfo.GetRuntimeInterfaceMap(property.DeclaringType);
                var id = map.InterfaceMethods.ToList().IndexOf(property.GetMethod);
                var classMember = map.TargetMethods[id];
                mappedProperty = entityType.GetRuntimeProperties()
                    .Single(x => Equals(x.GetMethod, classMember));
            }

            var typeAttribute = mappedProperty.GetCustomAttribute<TsqlTypeAttribute>();

            return typeAttribute?.SqlType;
        }

        public ISqlType GetSqlType(MethodInfo method)
        {
            var typeAttribute = method.GetCustomAttribute<TsqlTypeAttribute>();
            if (typeAttribute == null)
            {
                throw new AnnotationException($"Cannot determine sql source for method that is not annotated. Type {method.DeclaringType.FullName}, Method: {method.Name}.");
            }

            return typeAttribute.SqlType;
        }

        public ElementName GetTableName(Type type)
        {
            var nameAttribute = type.GetTypeInfo().GetCustomAttribute<SqlTableNameAttribute>();
            if (nameAttribute != null)
            {
                return nameAttribute.TableName;
            }
            else
            {
                return new ElementName(type.Name);
            }
        }

        public ISqlType GetUserDefinedTableType(Type type)
        {
            var attribute = type.GetTypeInfo().GetCustomAttribute<UserDefinedTableTypeNameAttribute>();
            if (attribute == null)
            {
                throw new AnnotationException(
                    $"Type {type.FullName} needs to be described with{typeof(UserDefinedTableTypeNameAttribute).FullName} to be used as table parameter");
            }

            return attribute.TypeReference;
        }

        private static bool IsSelectable(PropertyInfo property)
        {
            return (property.GetMethod?.IsPublic ?? false)
                   && property.GetCustomAttribute<SqlIgnoreAttribute>() == null;
        }

        private static bool IsInsertable(PropertyInfo property)
        {
            return (property.GetMethod?.IsPublic ?? false)
                   && property.GetCustomAttribute<SqlIgnoreAttribute>() == null
                   && property.GetCustomAttribute<SqlComputedColumnAttribute>() == null;
        }
    }
}