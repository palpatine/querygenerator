﻿using Microsoft.Extensions.DependencyInjection;

namespace Qdarc.Queries.Linq.Tsql
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IServiceCollection registrar)
        {
            registrar.AddTransient<ISqlTypeManager, TsqlTypeManager>();
        }
    }
}