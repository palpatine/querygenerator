﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql
{
    public class TsqlTypeManager : ISqlTypeManager
    {
        private const byte DefaultCapacity = 50;

        private const byte DefaultPrecision = 18;

        private const byte DefaultScale = 0;

        private const byte MaximalPrecision = 38;
        public ISqlType Bit => TsqlTypeProvider.Bit;

        public ISqlType Date => TsqlTypeProvider.Date;

        public ISqlType DateTime => TsqlTypeProvider.DateTime;

        public ISqlType Int => TsqlTypeProvider.Int;

        public ISqlType GetNVarChar(int capacity)
        {
            return TsqlTypeProvider.NVarChar(DefaultCapacity);
        }

        public ISqlType GetSqlType(ConstantExpression constant)
        {
            var type = constant.Value == null ? constant.Type : constant.Value.GetType();
            return GetSqlType(type, constant.Value);
        }

        public ISqlType GetSqlType(Type type, object value)
        {
            var typeName = GetTypeName(type);

            if (TsqlNativeTypesNames.HasPrecisionAndScale(typeName))
            {
                var data = GetPrecisionAndScale(typeName, value);
                return new TsqlType(typeName, data.Item1, data.Item2);
            }

            if (TsqlNativeTypesNames.HasCapacity(typeName))
            {
                var capacity = GetCapacity(typeName, value);
                return new TsqlType(typeName, capacity);
            }

            return new TsqlType(typeName);
        }

        public ISqlType GetSqlType(Type type)
        {
            var sqlType = TryGetSqlType(type);

            if (sqlType == null)
            {
                throw new InvalidOperationException($"Sql type not found for {type.FullName}.");
            }

            return sqlType;
        }

        public ISqlType TryGetSqlType(Type type)
        {
            string typeName;

            if (!TryGetTypeName(type, out typeName))
            {
                return null;
            }

            if (TsqlNativeTypesNames.HasPrecisionAndScale(typeName))
            {
                return new TsqlType(typeName, DefaultPrecision, DefaultScale);
            }

            if (TsqlNativeTypesNames.HasCapacity(typeName))
            {
                return new TsqlType(typeName, DefaultCapacity);
            }

            return new TsqlType(typeName);
        }

        public ISqlType GetSqlType(ISqlType firstType, ISqlType secondType)
        {
            var first = (TsqlType)firstType;
            var second = (TsqlType)secondType;
            var firstTypePriority = TsqlConstants.TypePriorities.IndexOf(first.Name);
            var secondTypePriority = TsqlConstants.TypePriorities.IndexOf(second.Name);

            if (firstTypePriority == secondTypePriority)
            {
                return SelectWiderType(first, second);
            }

            VerifyTypesCompatibility(first, second);

            if (firstTypePriority < secondTypePriority)
            {
                return firstType;
            }

            return secondType;
        }

        private static int GetCapacity(string typeName, object value)
        {
            if (value == null)
            {
                return 1;
            }

            switch (typeName)
            {
                case TsqlNativeTypesNames.VarBinary:
                case TsqlNativeTypesNames.Binary:
                    {
                        var collection = (byte[])value;
                        return collection.Length;
                    }

                case TsqlNativeTypesNames.VarChar:
                case TsqlNativeTypesNames.NVarChar:
                case TsqlNativeTypesNames.NChar:
                case TsqlNativeTypesNames.Char:
                    {
                        var collection = (string)value;
                        return collection.Length;
                    }

                default:
                    {
                        throw new NotImplementedException();
                    }
            }
        }

        private static Tuple<byte, byte> GetPrecisionAndScale(string typeName, object value)
        {
            switch (typeName)
            {
                case TsqlNativeTypesNames.Decimal:
                case TsqlNativeTypesNames.Numeric:
                    {
                        if (value != null)
                        {
                            var stringValue = ((decimal)value).ToString(CultureInfo.InvariantCulture).ToCharArray();
                            return Tuple.Create(
                                (byte)stringValue.Count(x => x != '.'),
                                (byte)(stringValue.SkipWhile(x => x != '.').Count() - 1));
                        }

                        return Tuple.Create(MaximalPrecision, DefaultScale);
                    }

                default:
                    {
                        throw new NotImplementedException();
                    }
            }
        }

        private static string GetTypeName(Type type)
        {
            if (TsqlConstants.TypesNamesMap.ContainsKey(type))
            {
                return TsqlConstants.TypesNamesMap[type];
            }

            throw new InvalidOperationException();
        }

        private static bool TryGetTypeName(Type type, out string typeName)
        {
            if (TsqlConstants.TypesNamesMap.ContainsKey(type))
            {
                typeName = TsqlConstants.TypesNamesMap[type];
                return true;
            }

            typeName = null;
            return false;
        }

        private static ISqlType SelectWiderType(TsqlType first, TsqlType second)
        {
            if (first.Name != second.Name)
            {
                throw new InvalidOperationException("Single type must be used");
            }

            if (first.Scale.HasValue && first.Precision.HasValue && second.Scale.HasValue && second.Precision.HasValue)
            {
                return new TsqlType(
                    first.Name,
                    Math.Max(first.Precision.Value, second.Precision.Value),
                    Math.Max(first.Scale.Value, second.Scale.Value));
            }

            if (first.Capacity.HasValue && second.Capacity.HasValue)
            {
                return new TsqlType(
                    first.Name,
                    first.Capacity.Value == 0 || second.Capacity.Value == 0 ? 0 : Math.Max(first.Capacity.Value, second.Capacity.Value));
            }

            return first;
        }

        private static void VerifyTypesCompatibility(TsqlType first, TsqlType second)
        {
            var d = first.Name == second.Name;
            if (d)
            {
            }

            ////todo: throw if types conversion is not supported
        }
    }
}