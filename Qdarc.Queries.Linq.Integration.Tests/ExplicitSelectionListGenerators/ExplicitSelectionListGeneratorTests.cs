﻿using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators;
using Qdarc.Queries.Linq.Integration.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Asserts;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Integration.Tests.ExplicitSelectionListGenerators
{
    [TestClass]
    public class ExplicitSelectionListGeneratorTests
    {
        [TestMethod]
        public void TableReferenceShouldGenerateSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new TableReference(new ElementName("Name"), typeof(UserEntity));

                var selectionList = generator.Generate(reference);
                selectionList.ShouldNotBeNull();
                selectionList.Elements.Count().ShouldBeEqual(2);
            }
        }

        [TestMethod]
        public void TableReferenceShouldAssignSourceToColumnsInSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            var reference = new TableReference(new ElementName("Name"), typeof(UserEntity));
            convention.Setup(x => x.GetSelectableColumns(reference))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();

                generator.Generate(reference);

                convention.Verify(x => x.GetSelectableColumns(reference), Times.Once);
            }
        }

        [TestMethod]
        public void TableReferenceShouldWrapColumnReferencesInAliases()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new TableReference(new ElementName("Name"), typeof(UserEntity));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Item.ShouldBeTheSameInstance(firstColumn);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Item.ShouldBeTheSameInstance(secondColumn);
            }
        }

        [TestMethod]
        public void TableReferenceShouldAssignAliasesToPropertiesNames()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new TableReference(new ElementName("Name"), typeof(UserEntity));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Alias.ShouldBeEqual(firstColumn.Property.Name);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Alias.ShouldBeEqual(secondColumn.Property.Name);
            }
        }

        [TestMethod]
        public void TabularFunctionCallShouldGenerateSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(int),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new TabularFunctionCall(
                    new ElementName("Name"),
                    Enumerable.Empty<ISqlValueExpression>(),
                    typeof(UserEntity));

                var selectionList = generator.Generate(reference);
                selectionList.ShouldNotBeNull();
                selectionList.Elements.Count().ShouldBeEqual(2);
            }
        }

        [TestMethod]
        public void TabularFunctionCallShouldAssignSourceToColumnsInSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(int),
                new Mock<ISqlType>().Object);
            var reference = new TabularFunctionCall(

                new ElementName("Name"),
                Enumerable.Empty<ISqlValueExpression>(),
                typeof(UserEntity));

            convention.Setup(x => x.GetSelectableColumns(reference))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();

                generator.Generate(reference);

                convention.Verify(x => x.GetSelectableColumns(reference), Times.Once);
            }
        }

        [TestMethod]
        public void TabularFunctionCallShouldWrapColumnReferencesInAliases()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new TabularFunctionCall(
                    new ElementName("Name"),
                    Enumerable.Empty<ISqlValueExpression>(),
                    typeof(UserEntity));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Item.ShouldBeTheSameInstance(firstColumn);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Item.ShouldBeTheSameInstance(secondColumn);
            }
        }

        [TestMethod]
        public void TabularFunctionCallShouldAssignAliasesToPropertiesNames()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new TabularFunctionCall(
                    new ElementName("Name"),
                    Enumerable.Empty<ISqlValueExpression>(),
                    typeof(UserEntity));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Alias.ShouldBeEqual(firstColumn.Property.Name);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Alias.ShouldBeEqual(secondColumn.Property.Name);
            }
        }

        [TestMethod]
        public void AliasedTableReferenceShouldGenerateSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(int),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new AliasedSourceItem(
                    new TableReference(new ElementName("Name"), typeof(UserEntity)));

                var selectionList = generator.Generate(reference);
                selectionList.ShouldNotBeNull();
                selectionList.Elements.Count().ShouldBeEqual(2);
            }
        }

        [TestMethod]
        public void AliasedTableReferenceShouldAssignSourceToColumnsInSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var reference = new AliasedSourceItem(
                new TableReference(new ElementName("Name"), typeof(UserEntity)));

            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(reference))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();

                generator.Generate(reference);

                convention.Verify(x => x.GetSelectableColumns(reference), Times.Once);
            }
        }

        [TestMethod]
        public void AliasedTableReferenceShouldWrapColumnReferencesInAliases()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new AliasedSourceItem(
                    new TableReference(new ElementName("Name"), typeof(UserEntity)));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Item.ShouldBeTheSameInstance(firstColumn);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Item.ShouldBeTheSameInstance(secondColumn);
            }
        }

        [TestMethod]
        public void AliasedTableReferenceShouldAssignAliasesToPropertiesNames()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new AliasedSourceItem(
                    new TableReference(new ElementName("Name"), typeof(UserEntity)));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Alias.ShouldBeEqual(firstColumn.Property.Name);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Alias.ShouldBeEqual(secondColumn.Property.Name);
            }
        }

        [TestMethod]
        public void AliasedTabularFunctionCallShouldGenerateSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new AliasedSourceItem(
                    new TabularFunctionCall(
                        new ElementName("Name"),
                        Enumerable.Empty<ISqlValueExpression>(),
                        typeof(UserEntity)));

                var selectionList = generator.Generate(reference);
                selectionList.ShouldNotBeNull();
                selectionList.Elements.Count().ShouldBeEqual(2);
            }
        }

        [TestMethod]
        public void AliasedTabularFunctionCallShouldAssignSourceToColumnsInSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            var reference = new AliasedSourceItem(
                new TabularFunctionCall(
                    new ElementName("Name"),
                    Enumerable.Empty<ISqlValueExpression>(),
                    typeof(UserEntity)));

            convention.Setup(x => x.GetSelectableColumns(reference))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();

                generator.Generate(reference);

                convention.Verify(x => x.GetSelectableColumns(reference), Times.Once);
            }
        }

        [TestMethod]
        public void AliasedTabularFunctionCallShouldWrapColumnReferencesInAliases()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new AliasedSourceItem(
                    new TabularFunctionCall(
                        new ElementName("Name"),
                        Enumerable.Empty<ISqlValueExpression>(),
                        typeof(UserEntity)));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Item.ShouldBeTheSameInstance(firstColumn);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Item.ShouldBeTheSameInstance(secondColumn);
            }
        }

        [TestMethod]
        public void AliasedTabularFunctionCallShouldAssignAliasesToPropertiesNames()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var secondColumn = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity x) => x.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(It.Is<INamedSource>(z => z.ClrType == typeof(UserEntity))))
                .Returns(
                    new[]
                    {
                        firstColumn,
                        secondColumn
                    });

            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var reference = new AliasedSourceItem(
                    new TabularFunctionCall(
                        new ElementName("Name"),
                        Enumerable.Empty<ISqlValueExpression>(),
                        typeof(UserEntity)));

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Alias.ShouldBeEqual(firstColumn.Property.Name);
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Alias.ShouldBeEqual(secondColumn.Property.Name);
            }
        }

        [TestMethod]
        public void AliasedValuesSourceShouldGenerateSelectionList()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstValue = new Mock<ISqlValueExpression>();
            var secondValue = new Mock<ISqlValueExpression>();
            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var rows = new[]
                {
                    new ValueSourceRow(
                        new[]
                        {
                            firstValue.Object,
                            secondValue.Object
                        })
                };
                var valuesSource = new ValuesSource(rows, typeof(UserEntity));
                var reference = new AliasedSourceItem(
                    valuesSource,
                    new[]
                    {
                        new ColumnAlias("Id", null, null),
                        new ColumnAlias("Name", null, null)
                    }
                );

                var selectionList = generator.Generate(reference);
                selectionList.ShouldNotBeNull();
                selectionList.Elements.Count().ShouldBeEqual(2);
            }
        }

        [TestMethod]
        public void AliasedValuesSourceShouldCreateSelectionListIfThereIsParameterlessConstructorDefined()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var firstValue = new Mock<ISqlValueExpression>();
            var secondValue = new Mock<ISqlValueExpression>();
            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var rows = new[]
                {
                    new ValueSourceRow(
                        new[]
                        {
                            firstValue.Object,
                            secondValue.Object
                        })
                };
                var valuesSource = new ValuesSource(rows, typeof(UserEntity));
                var reference = new AliasedSourceItem(
                    valuesSource,
                    new[]
                    {
                        new ColumnAlias("Id", null, null),
                        new ColumnAlias("Name", null, null)
                    }
                );

                var selection = generator.Generate(reference);
                selection.ShouldBeInstanceOf<SqlSelection>();
            }
        }

        [TestMethod]
        public void AliasedValuesSourceShouldCreateProjectionByConstructorIfThereIsNoParameterlessConstructorDefined()
        {
            var itemSample = new
            {
                Id = 1,
                Name = "a"
            };
            var convention = new Mock<IModelToSqlConvention>();
            var firstValue = new Mock<ISqlValueExpression>();
            var secondValue = new Mock<ISqlValueExpression>();
            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var rows = new[]
                {
                    new ValueSourceRow(
                        new[]
                        {
                            firstValue.Object,
                            secondValue.Object
                        })
                };
                var valuesSource = new ValuesSource(rows, itemSample.GetType());
                var reference = new AliasedSourceItem(
                    valuesSource,
                    new[]
                    {
                        new ColumnAlias("Id", null, null),
                        new ColumnAlias("Name", null, null)
                    });

                var selection = generator.Generate(reference);
                selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            }
        }

        [TestMethod]
        public void AliasedValuesSourceShouldWrapColumnReferencesInAliases()
        {
            var convention = new Mock<IModelToSqlConvention>();
            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var firstValue = new Mock<ISqlValueExpression>();
                firstValue.Setup(x => x.ClrType).Returns(typeof(int));
                var secondValue = new Mock<ISqlValueExpression>();
                secondValue.Setup(x => x.ClrType).Returns(typeof(string));
                var rows = new[]
                {
                    new ValueSourceRow(
                        new[]
                        {
                            firstValue.Object,
                            secondValue.Object
                        })
                };
                var valuesSource = new ValuesSource(rows, typeof(UserEntity));
                var reference = new AliasedSourceItem(
                    valuesSource,
                    new[]
                    {
                        new ColumnAlias("Id", typeof(int), null),
                        new ColumnAlias("Name", typeof(string), null)
                    });

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                var firstColumn = firstAlias.Item.ShouldBeInstanceOf<SqlColumnReference>();
                firstColumn.ClrType.ShouldBeEqual(typeof(int));
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                var secondColumn = secondAlias.Item.ShouldBeInstanceOf<SqlColumnReference>();
                secondColumn.ClrType.ShouldBeEqual(typeof(string));
            }
        }

        [TestMethod]
        public void AliasedValuesSourceShouldAssignAliasesToPropertiesNames()
        {
            var convention = new Mock<IModelToSqlConvention>();
            using (var scope = Environment.Container.BeginLifetimeScope(x => x.RegisterInstance(convention.Object)))
            {
                var generator = scope.Resolve<IExplicitSelectionListGenerator>();
                var firstValue = new Mock<ISqlValueExpression>();
                var secondValue = new Mock<ISqlValueExpression>();
                var rows = new[]
                {
                    new ValueSourceRow(
                        new[]
                        {
                            firstValue.Object,
                            secondValue.Object
                        })
                };
                var valuesSource = new ValuesSource(rows, typeof(UserEntity));
                var reference = new AliasedSourceItem(
                    valuesSource,
                    new[]
                    {
                        new ColumnAlias("Id", null, null),
                        new ColumnAlias("Name", null, null)
                    });

                var list = generator.Generate(reference);
                var firstAlias = list.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
                firstAlias.Alias.ShouldBeEqual("Id");
                var secondAlias = list.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
                secondAlias.Alias.ShouldBeEqual("Name");
            }
        }
    }
}