﻿using System;
using System.Runtime.Remoting.Contexts;

namespace Qdarc.Queries.Linq.Integration.Tests.TestModel
{
    public class UserEntity 
    {
        public IContributeServerContextSink Context { get; set; }

        public string FirstName { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string LastName { get; set; }

        public int? ParentId { get; set; }

        public DateTime WorkEnd { get; set; }
    }

    public class EmailAddress
    {
        public int Id { get; set; }
    }

    public class Password
    {
        public int Id { get; set; }

    }

    public class Person
    {
        public int Id { get; set; }

    }
}