﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Sql.Connector;
using Qdarc.Queries.Model;

namespace Qdarc.Queries.Linq.Integration.Tests
{
    [TestClass]
    public static class Environment
    {
        public static IContainer Container { get; private set; }

        [AssemblyCleanup]
        public static void Cleanup()
        {
        }

        [AssemblyInitialize]
        public static void Create(TestContext context)
        {
            var containerBuilder = new ContainerBuilder();
            var registrar = new ServiceCollection();
            Linq.Bootstrapper.Bootstrap(registrar);
            Qdarc.Queries.Model.Bootstrapper.Bootstrap(registrar);
            containerBuilder.Populate(registrar);
            Container = containerBuilder.Build();
        }
    }
}