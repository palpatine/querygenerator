﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Tests
{
    [TestClass]
    public class GeneralRulesForModelTests
    {
        [TestMethod]
        public void ModelClassesShouldBeSealedIfAreNotDerivedFrom()
        {
            var types = typeof(SqlScript).Assembly.GetTypes()
                .Where(
                    x => x.IsClass
                         && !x.Namespace.StartsWith(typeof(Visitor).Namespace, StringComparison.Ordinal)
                         && x != typeof(Bootstrapper)
                         && !x.IsAbstract
                         && !x.IsSealed)
                .ToArray();

            var message = new StringBuilder("Found not sealed model classes:");
            var isInError = false;
            foreach (var type in types)
            {
                var isBaseClass = typeof(SqlScript).Assembly
                    .GetTypes()
                    .Any(x => x.BaseType == type);

                if (isBaseClass)
                {
                    continue;
                }

                message.AppendLine(type.FullName);
                isInError = true;
            }

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(isInError, message.ToString());
        }

        [TestMethod]
        public void EachModelClassShouldDeclareTransformingAcceptMethod()
        {
            var types = typeof(SqlScript).Assembly.GetTypes()
                .Where(
                    x => x.IsClass
                         && !x.Namespace.StartsWith(typeof(Visitor).Namespace, StringComparison.Ordinal)
                         && !typeof(Exception).IsAssignableFrom(x)
                         && x != typeof(Bootstrapper)
                         && x.GetCustomAttribute<CompilerGeneratedAttribute>() == null
                         && !x.Namespace.StartsWith("Qdarc.Queries.Model.Visitation", StringComparison.Ordinal))
                .ToArray();

            var message = new StringBuilder("Found model class T that does not declare Accept method TResult Accept<TResult>(ITransformer<TResult>):");
            var isInError = false;
            foreach (var type in types)
            {
                var methods = type.GetMethods(
                        BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
                    .Where(
                        x => x.Name == "Accept"
                             && x.IsGenericMethodDefinition
                             && x.ReturnType == x.GetGenericArguments().Single()
                             && x.GetParameters().Length == 1
                             && x.GetParameters().Single().ParameterType == typeof(ITransformer));
                if (methods.Count() != 1)
                {
                    isInError = true;
                    message.AppendLine(type.FullName);
                }
            }

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.IsFalse(isInError, message.ToString());
        }
    }
}