﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Tests.Visitation
{
    [TestClass]
    public class VisitorTests
    {
        [TestMethod]
        public void ShouldResolveGenericHandlerIfNoInterceptorIsMatched()
        {
            var resolver = new Mock<IServiceProvider>();
            IVisitor visitor = new Visitor(resolver.Object);
            resolver.Setup(x => x.GetService(typeof(IVisitationHandler<SqlScript, SqlScript>)))
                .Returns(new Mock<IVisitationHandler<SqlScript, SqlScript>>().Object);

            visitor.Handle<SqlScript, SqlScript>(new SqlScript(new[] { new Mock<ISelectCommand>().Object }, null));

            resolver.Verify(x => x.GetService(typeof(IVisitationHandler<SqlScript, SqlScript>)), Times.Once);
        }

        [TestMethod]
        public void ShouldExecuteGenericHandlerIfNoInterceptorIsMatched()
        {
            var resolver = new Mock<IServiceProvider>();
            IVisitor visitor = new Visitor(resolver.Object);
            var handler = new Mock<IVisitationHandler<SqlScript, SqlScript>>();
            resolver.Setup(x => x.GetService(typeof(IVisitationHandler<SqlScript, SqlScript>)))
                .Returns(handler.Object);
            var expression = new SqlScript(new[] { new Mock<ISelectCommand>().Object }, null);
            var expectedResult = new SqlScript(new[] { new Mock<ISelectCommand>().Object }, null);
            handler.Setup(x => x.Handle(expression, visitor))
                .Returns(expectedResult);

            var result = visitor.Handle<SqlScript, SqlScript>(expression);

            result.ShouldBeTheSameInstance(expectedResult);
        }

        [TestMethod]
        public void ShouldReturnResultOfGenericHandlerIfNoInterceptorIsMatched()
        {
            var resolver = new Mock<IServiceProvider>();
            IVisitor visitor = new Visitor(resolver.Object);
            var handler = new Mock<IVisitationHandler<SqlScript, SqlScript>>();
            resolver.Setup(x => x.GetService(typeof(IVisitationHandler<SqlScript, SqlScript>)))
                .Returns(handler.Object);
            var expression = new SqlScript(new[] { new Mock<ISelectCommand>().Object }, null);

            visitor.Handle<SqlScript, SqlScript>(expression);

            handler.Verify(x => x.Handle(expression, visitor), Times.Once);
        }

        [TestMethod]
        public void ShouldExecuteInterceptorIfMatchesExecutionCallDirectly()
        {
            var resolver = new Mock<IServiceProvider>();
            IVisitor visitor = new Visitor(resolver.Object);
            var interceptor = new Mock<IVisitationHandler<MultiplyValueExpression, ISqlValueExpression>>();
            visitor.Intercept(interceptor.Object);
            var expression = new MultiplyValueExpression(
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object,
                null,
                null);

            visitor.Handle<MultiplyValueExpression, ISqlValueExpression>(expression);

            interceptor.Verify(x => x.Handle(expression, visitor), Times.Once);
        }

        [TestMethod]
        public void ShouldExecuteInterceptorIfMatchesInParameterIndirectly()
        {
            var resolver = new Mock<IServiceProvider>();
            IVisitor visitor = new Visitor(resolver.Object);
            var interceptor = new Mock<IVisitationHandler<ISqlValueExpression, ISqlValueExpression>>();
            visitor.Intercept(interceptor.Object);
            var expression = new MultiplyValueExpression(
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object,
                null,
                null);

            visitor.Handle<MultiplyValueExpression, ISqlValueExpression>(expression);

            interceptor.Verify(x => x.Handle(expression, visitor), Times.Once);
        }

        [TestMethod]
        public void ShouldExecuteInterceptorIfMatchesOutParameterIndirectly()
        {
            var resolver = new Mock<IServiceProvider>();
            IVisitor visitor = new Visitor(resolver.Object);
            var interceptor = new Mock<IVisitationHandler<MultiplyValueExpression, MultiplyValueExpression>>();
            visitor.Intercept(interceptor.Object);
            var expression = new MultiplyValueExpression(
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object,
                null,
                null);

            visitor.Handle<MultiplyValueExpression, ISqlValueExpression>(expression);

            interceptor.Verify(x => x.Handle(expression, visitor), Times.Once);
        }

        [TestMethod]
        public void ShouldReturnResultOfInterceptorIfMatched()
        {
            var resolver = new Mock<IServiceProvider>();
            IVisitor visitor = new Visitor(resolver.Object);
            var interceptor = new Mock<IVisitationHandler<MultiplyValueExpression, ISqlValueExpression>>();
            visitor.Intercept(interceptor.Object);
            var expression = new MultiplyValueExpression(
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object,
                null,
                null);
            var expectedResult = new Mock<ISqlValueExpression>().Object;
            interceptor.Setup(x => x.Handle(expression, visitor))
                .Returns(expectedResult);

            var result = visitor.Handle<MultiplyValueExpression, ISqlValueExpression>(expression);

            result.ShouldBeTheSameInstance(expectedResult);
        }
    }
}