using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlCoalesceExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlCoalesceExpressionVisitor();
            var valueExpressions = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlCoalesceExpression(
                valueExpressions.Select(x => x.Object),
                typeof(int),
                new Mock<ISqlType>().Object);
            var visited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
            };
            var transformer = new Mock<IVisitor>();
            valueExpressions[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[0]);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlCoalesceExpressionVisitor();
            var valueExpressions = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlCoalesceExpression(
                valueExpressions.Select(x => x.Object),
                typeof(int),
                new Mock<ISqlType>().Object);
            var visited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
            };
            var transformer = new Mock<IVisitor>();
            valueExpressions[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlCoalesceExpressionVisitor();
            var valueExpressions = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlCoalesceExpression(
                valueExpressions.Select(x => x.Object),
                typeof(int),
                new Mock<ISqlType>().Object);
            var visited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
            };
            var transformer = new Mock<IVisitor>();
            valueExpressions[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldVisitEachExpression()
        {
            var handler = new SqlCoalesceExpressionVisitor();
            var valueExpressions = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlCoalesceExpression(
                valueExpressions.Select(x => x.Object),
                typeof(int),
                new Mock<ISqlType>().Object);
            var visited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            var transformer = new Mock<IVisitor>();

            valueExpressions[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[0]);
            valueExpressions[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[1]);

            handler.Handle(expression, transformer.Object);

            valueExpressions[0].Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
            valueExpressions[1].Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedExpressions()
        {
            var handler = new SqlCoalesceExpressionVisitor();
            var valueExpressions = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlCoalesceExpression(
                valueExpressions.Select(x => x.Object),
                typeof(int),
                new Mock<ISqlType>().Object);
            var visited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            var transformer = new Mock<IVisitor>();

            valueExpressions[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[0]);
            valueExpressions[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[1]);

            var result = handler.Handle(expression, transformer.Object);

            result.Expressions.ElementAt(0).ShouldBeTheSameInstance(visited[0]);
            result.Expressions.ElementAt(1).ShouldBeTheSameInstance(visited[1]);
        }
    }
}