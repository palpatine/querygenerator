using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlCaseSwitchExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldAssignVisitedInitialValue()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.InitialValue.ShouldBeTheSameInstance(initialVisited);
        }

        [TestMethod]
        public void ShouldVisitInitialValue()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);

            handler.Handle(expression, transformer.Object);

            initialValueExpression.Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedElseExpression()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var elseExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                elseExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var elseVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);
            elseExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(elseVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Else.ShouldBeTheSameInstance(elseVisited);
        }

        [TestMethod]
        public void ShouldVisitElseExpression()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var elseExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                elseExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var elseVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);
            elseExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(elseVisited);

            handler.Handle(expression, transformer.Object);

            elseExpression.Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitEachCase()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var elseExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object),
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                elseExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object),
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[1]))
                .Returns(casesVisited[1]);

            handler.Handle(expression, transformer.Object);

            transformer.Verify(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]), Times.Once);
            transformer.Verify(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[1]), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedCases()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object),
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object),
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[1]))
                .Returns(casesVisited[1]);

            var result = handler.Handle(expression, transformer.Object);

            result.Cases.ElementAt(0).ShouldBeTheSameInstance(casesVisited[0]);
            result.Cases.ElementAt(1).ShouldBeTheSameInstance(casesVisited[1]);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlCaseSwitchExpressionVisitor();
            var initialValueExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseSwitchExpression(
                initialValueExpression.Object,
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var initialVisited = new Mock<ISqlValueExpression>().Object;
            var casesVisited = new[]
            {
                new SqlWhenThen(new Mock<ISqlValueExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            initialValueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object)).Returns(initialVisited);
            transformer.Setup(x => x.Handle<SqlWhenThen, SqlWhenThen>(cases[0]))
                .Returns(casesVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }
    }
}