using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class DeletedTableReferenceVisitorTests
    {
        public static DeletedTableReference Handle(DeletedTableReference expression) => new DeletedTableReference(expression.ClrType);

        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new DeletedTableReferenceVisitor();
            var expression = new DeletedTableReference(
                typeof(int));

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new DeletedTableReferenceVisitor();
            var expression = new DeletedTableReference(
                typeof(int));

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }
    }
}