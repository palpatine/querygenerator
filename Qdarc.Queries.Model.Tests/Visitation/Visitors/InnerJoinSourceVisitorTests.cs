using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class InnerJoinSourceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new InnerJoinSourceVisitor();
            var left = new Mock<ISource>();
            var right = new Mock<ISource>();
            var on = new Mock<ISqlLogicalExpression>();
            var expression = new InnerJoinSource(
                left.Object,
                right.Object,
                on.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISource>().Object;
            var rightVisited = new Mock<ISource>().Object;
            var onVisited = new Mock<ISqlLogicalExpression>().Object;
            on.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(onVisited);
            left.Setup(x => x.Accept<ISource>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISource>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var handler = new InnerJoinSourceVisitor();
            var left = new Mock<ISource>();
            var right = new Mock<ISource>();
            var on = new Mock<ISqlLogicalExpression>();
            var expression = new InnerJoinSource(
                left.Object,
                right.Object,
                on.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISource>().Object;
            var rightVisited = new Mock<ISource>().Object;
            var onVisited = new Mock<ISqlLogicalExpression>().Object;
            on.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(onVisited);
            left.Setup(x => x.Accept<ISource>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISource>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            left.Verify(x => x.Accept<ISource>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedLeft()
        {
            var handler = new InnerJoinSourceVisitor();
            var left = new Mock<ISource>();
            var right = new Mock<ISource>();
            var on = new Mock<ISqlLogicalExpression>();
            var expression = new InnerJoinSource(
                left.Object,
                right.Object,
                on.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISource>().Object;
            var rightVisited = new Mock<ISource>().Object;
            var onVisited = new Mock<ISqlLogicalExpression>().Object;
            on.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(onVisited);
            left.Setup(x => x.Accept<ISource>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISource>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Left.ShouldBeTheSameInstance(leftVisited);
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var handler = new InnerJoinSourceVisitor();
            var left = new Mock<ISource>();
            var right = new Mock<ISource>();
            var on = new Mock<ISqlLogicalExpression>();
            var expression = new InnerJoinSource(
                left.Object,
                right.Object,
                on.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISource>().Object;
            var rightVisited = new Mock<ISource>().Object;
            var onVisited = new Mock<ISqlLogicalExpression>().Object;
            on.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(onVisited);
            left.Setup(x => x.Accept<ISource>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISource>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            right.Verify(x => x.Accept<ISource>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedRight()
        {
            var handler = new InnerJoinSourceVisitor();
            var left = new Mock<ISource>();
            var right = new Mock<ISource>();
            var on = new Mock<ISqlLogicalExpression>();
            var expression = new InnerJoinSource(
                left.Object,
                right.Object,
                on.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISource>().Object;
            var rightVisited = new Mock<ISource>().Object;
            var onVisited = new Mock<ISqlLogicalExpression>().Object;
            on.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(onVisited);
            left.Setup(x => x.Accept<ISource>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISource>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Right.ShouldBeTheSameInstance(rightVisited);
        }

        [TestMethod]
        public void ShouldVisitOn()
        {
            var handler = new InnerJoinSourceVisitor();
            var left = new Mock<ISource>();
            var right = new Mock<ISource>();
            var on = new Mock<ISqlLogicalExpression>();
            var expression = new InnerJoinSource(
                left.Object,
                right.Object,
                on.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISource>().Object;
            var rightVisited = new Mock<ISource>().Object;
            var onVisited = new Mock<ISqlLogicalExpression>().Object;
            on.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(onVisited);
            left.Setup(x => x.Accept<ISource>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISource>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            on.Verify(x => x.Accept<ISqlLogicalExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedOn()
        {
            var handler = new InnerJoinSourceVisitor();
            var left = new Mock<ISource>();
            var right = new Mock<ISource>();
            var on = new Mock<ISqlLogicalExpression>();
            var expression = new InnerJoinSource(
                left.Object,
                right.Object,
                on.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISource>().Object;
            var rightVisited = new Mock<ISource>().Object;
            var onVisited = new Mock<ISqlLogicalExpression>().Object;
            on.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(onVisited);
            left.Setup(x => x.Accept<ISource>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISource>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.On.ShouldBeTheSameInstance(onVisited);
        }
    }
}