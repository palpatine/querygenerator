using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class NegateValueExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new NegateValueExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new NegateValueExpression(valueExpression.Object, typeof(int), new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(new Mock<ISqlValueExpression>().Object);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new NegateValueExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new NegateValueExpression(valueExpression.Object, typeof(int), new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(new Mock<ISqlValueExpression>().Object);

            var result = handler.Handle(expression, transformer.Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new NegateValueExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new NegateValueExpression(value.Object, typeof(int), new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(new Mock<ISqlValueExpression>().Object);

            var result = handler.Handle(expression, transformer);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldVisitValueExpression()
        {
            var handler = new NegateValueExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new NegateValueExpression(valueExpression.Object, typeof(int), new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(new Mock<ISqlValueExpression>().Object);

            handler.Handle(expression, transformer);

            valueExpression.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedValueExpression()
        {
            var handler = new NegateValueExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new NegateValueExpression(valueExpression.Object, typeof(int), new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.ValueExpression.ShouldBeTheSameInstance(visited);
        }
    }
}