using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ModuloAssignVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ModuloAssignVisitor();
            var assignable = new Mock<IAssignable>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new ModuloAssign(
                assignable.Object,
                value.Object);
            var transformer = new Mock<IVisitor>();
            var assignableVisited = new Mock<IAssignable>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            assignable.Setup(x => x.Accept<IAssignable>(transformer.Object))
                .Returns(assignableVisited);
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitAssignable()
        {
            var handler = new ModuloAssignVisitor();
            var assignable = new Mock<IAssignable>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new ModuloAssign(
                assignable.Object,
                value.Object);
            var transformer = new Mock<IVisitor>();
            var assignableVisited = new Mock<IAssignable>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            assignable.Setup(x => x.Accept<IAssignable>(transformer.Object))
                .Returns(assignableVisited);
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);

            handler.Handle(expression, transformer.Object);

            assignable.Verify(x => x.Accept<IAssignable>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedAssignable()
        {
            var handler = new ModuloAssignVisitor();
            var assignable = new Mock<IAssignable>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new ModuloAssign(
                assignable.Object,
                value.Object);
            var transformer = new Mock<IVisitor>();
            var assignableVisited = new Mock<IAssignable>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            assignable.Setup(x => x.Accept<IAssignable>(transformer.Object))
                .Returns(assignableVisited);
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Assignable.ShouldBeTheSameInstance(assignableVisited);
        }

        [TestMethod]
        public void ShouldVisitValue()
        {
            var handler = new ModuloAssignVisitor();
            var assignable = new Mock<IAssignable>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new ModuloAssign(
                assignable.Object,
                value.Object);
            var transformer = new Mock<IVisitor>();
            var assignableVisited = new Mock<IAssignable>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            assignable.Setup(x => x.Accept<IAssignable>(transformer.Object))
                .Returns(assignableVisited);
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);

            handler.Handle(expression, transformer.Object);

            value.Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedValue()
        {
            var handler = new ModuloAssignVisitor();
            var assignable = new Mock<IAssignable>();
            var value = new Mock<ISqlValueExpression>();
            var expression = new ModuloAssign(
                assignable.Object,
                value.Object);
            var transformer = new Mock<IVisitor>();
            var assignableVisited = new Mock<IAssignable>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            assignable.Setup(x => x.Accept<IAssignable>(transformer.Object))
                .Returns(assignableVisited);
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Value.ShouldBeTheSameInstance(valueVisited);
        }
    }
}