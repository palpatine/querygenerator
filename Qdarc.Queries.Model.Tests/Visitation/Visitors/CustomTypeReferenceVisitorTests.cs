using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class CustomTypeReferenceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new CustomTypeReferenceVisitor();
            var expression = new CustomTypeReference(
                new ElementName("Name"));

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCloneName()
        {
            var handler = new CustomTypeReferenceVisitor();
            var expression = new CustomTypeReference(
                new ElementName("Name"));

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Name.ShouldBeEqual(expression.Name);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result.Name, expression.Name);
        }
    }
}