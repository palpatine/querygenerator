using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class TypeReferenceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new TypeReferenceVisitor();
            var expression = new TypeReference(
                "Name");

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopyName()
        {
            var handler = new TypeReferenceVisitor();
            var expression = new TypeReference(
                "Name");

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Name.ShouldBeEqual(expression.Name);
        }
    }
}