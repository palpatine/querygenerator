using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class LinqParameterReferenceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new LinqParameterReferenceVisitor();
            var expression = new LinqParameterReference(Expression.Constant(0), typeof(int), new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new LinqParameterReferenceVisitor();
            var expression = new LinqParameterReference(Expression.Constant(0), typeof(int), new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyExpression()
        {
            var handler = new LinqParameterReferenceVisitor();
            var expression = new LinqParameterReference(Expression.Constant(0), typeof(int), new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Expression.ShouldBeTheSameInstance(expression.Expression);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new LinqParameterReferenceVisitor();
            var expression = new LinqParameterReference(Expression.Constant(0), typeof(int), new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }
    }
}