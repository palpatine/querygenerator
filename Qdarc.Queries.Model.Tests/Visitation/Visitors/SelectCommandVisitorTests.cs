﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SelectCommandVisitorTests
    {
        [TestMethod]
        public void ShouldVisitSource()
        {
            var handler = new SelectCommandVisitor();
            var source = new Mock<ISource>();
            var command = new SelectCommand(source.Object);
            var transformer = new Mock<IVisitor>().Object;

            handler.Handle(command, transformer);

            source.Verify(x => x.Accept<ISource>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSource()
        {
            var handler = new SelectCommandVisitor();
            var source = new Mock<ISource>();
            var command = new SelectCommand(source.Object);
            var transformer = new Mock<IVisitor>().Object;
            var visitedSource = new Mock<ISource>();
            source.Setup(x => x.Accept<ISource>(transformer))
                .Returns(visitedSource.Object);

            var result = handler.Handle(command, transformer);

            result.Source.ShouldBeTheSameInstance(visitedSource.Object);
        }

        [TestMethod]
        public void ShouldCopyDistinct()
        {
            var handler = new SelectCommandVisitor();
            var command = new SelectCommand(isDistinct: true);
            var transformer = new Mock<IVisitor>().Object;

            var result = handler.Handle(command, transformer);

            result.IsDistinct.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SelectCommandVisitor();
            var command = new SelectCommand();
            var transformer = new Mock<IVisitor>().Object;

            var result = handler.Handle(command, transformer);

            result.ShouldNotBeNull();
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(command, result);
        }

        [TestMethod]
        public void ShouldVisitSelection()
        {
            var handler = new SelectCommandVisitor();
            var selection = new Mock<ISqlSelection>();
            var command = new SelectCommand(selection: selection.Object);
            var transformer = new Mock<IVisitor>().Object;

            handler.Handle(command, transformer);

            selection.Verify(x => x.Accept<ISqlSelection>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSelection()
        {
            var handler = new SelectCommandVisitor();
            var selection = new Mock<ISqlSelection>();
            var command = new SelectCommand(selection: selection.Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlSelection>();
            selection.Setup(x => x.Accept<ISqlSelection>(transformer))
                .Returns(visited.Object);

            var result = handler.Handle(command, transformer);

            result.Selection.ShouldBeTheSameInstance(visited.Object);
        }

        [TestMethod]
        public void ShouldVisitFilter()
        {
            var handler = new SelectCommandVisitor();
            var filter = new Mock<ISqlLogicalExpression>();
            var command = new SelectCommand(filter: filter.Object);
            var transformer = new Mock<IVisitor>().Object;

            handler.Handle(command, transformer);

            filter.Verify(x => x.Accept<ISqlLogicalExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedFilter()
        {
            var handler = new SelectCommandVisitor();
            var filter = new Mock<ISqlLogicalExpression>();
            var command = new SelectCommand(filter: filter.Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlLogicalExpression>();
            filter.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(visited.Object);

            var result = handler.Handle(command, transformer);

            result.Filter.ShouldBeTheSameInstance(visited.Object);
        }

        [TestMethod]
        public void ShouldVisitTop()
        {
            var handler = new SelectCommandVisitor();
            var top = new TopExpression(new Mock<ISqlValueExpression>().Object, false, false);
            var command = new SelectCommand(top: top);
            var transformer = new Mock<IVisitor>();

            handler.Handle(command, transformer.Object);

            transformer.Verify(x => x.Handle<TopExpression, TopExpression>(top), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedTop()
        {
            var handler = new SelectCommandVisitor();
            var top = new TopExpression(new Mock<ISqlValueExpression>().Object, false, false);
            var command = new SelectCommand(top: top);
            var transformer = new Mock<IVisitor>();
            var visited = new TopExpression(new Mock<ISqlValueExpression>().Object, false, false);
            transformer.Setup(x => x.Handle<TopExpression, TopExpression>(top))
                .Returns(visited);

            var result = handler.Handle(command, transformer.Object);

            result.Top.ShouldBeTheSameInstance(visited);
        }

        [TestMethod]
        public void ShouldVisitGroup()
        {
            var handler = new SelectCommandVisitor();
            var group = new GroupExpression(new[] { new Mock<ISqlValueExpression>().Object });
            var command = new SelectCommand(group: group);
            var transformer = new Mock<IVisitor>();

            handler.Handle(command, transformer.Object);

            transformer.Verify(x => x.Handle<GroupExpression, GroupExpression>(group), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedGroup()
        {
            var handler = new SelectCommandVisitor();
            var group = new GroupExpression(new[] { new Mock<ISqlValueExpression>().Object });
            var command = new SelectCommand(group: group);
            var transformer = new Mock<IVisitor>();
            var visited = new GroupExpression(new[] { new Mock<ISqlValueExpression>().Object });
            transformer.Setup(x => x.Handle<GroupExpression, GroupExpression>(group))
                .Returns(visited);

            var result = handler.Handle(command, transformer.Object);

            result.Group.ShouldBeTheSameInstance(visited);
        }

        [TestMethod]
        public void ShouldVisitEachElementOfOrder()
        {
            var handler = new SelectCommandVisitor();
            var order = new[]
            {
                new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true),
                new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true)
            };
            var command = new SelectCommand(order: order);
            var transformer = new Mock<IVisitor>();

            handler.Handle(command, transformer.Object);

            transformer.Verify(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]), Times.Once);
            transformer.Verify(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[1]), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedOrderElements()
        {
            var handler = new SelectCommandVisitor();
            var order = new[]
            {
                new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true),
                new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true)
            };
            var command = new SelectCommand(order: order);
            var transformer = new Mock<IVisitor>();
            var visited = new[]
            {
                new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true),
                new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true)
            };
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(visited[0]);
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[1]))
                .Returns(visited[1]);

            var result = handler.Handle(command, transformer.Object);

            result.Order.ElementAt(0).ShouldBeTheSameInstance(visited.ElementAt(0));
            result.Order.ElementAt(1).ShouldBeTheSameInstance(visited.ElementAt(1));
        }

        [TestMethod]
        public void ShouldVisitEachElementOfWith()
        {
            var handler = new SelectCommandVisitor();
            var with = new[]
            {
                new WithExpression("Name", new Mock<ISelectCommand>().Object, null),
                new WithExpression("Name", new Mock<ISelectCommand>().Object, null)
            };
            var command = new SelectCommand(with: with);
            var transformer = new Mock<IVisitor>();

            handler.Handle(command, transformer.Object);

            transformer.Verify(x => x.Handle<WithExpression, WithExpression>(with[0]), Times.Once);
            transformer.Verify(x => x.Handle<WithExpression, WithExpression>(with[1]), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedWithElements()
        {
            var handler = new SelectCommandVisitor();
            var with = new[]
            {
                new WithExpression("Name", new Mock<ISelectCommand>().Object, null),
                new WithExpression("Name", new Mock<ISelectCommand>().Object, null)
            };
            var command = new SelectCommand(with: with);
            var transformer = new Mock<IVisitor>();
            var visited = new[]
            {
                new WithExpression("Name", new Mock<ISelectCommand>().Object, null),
                new WithExpression("Name", new Mock<ISelectCommand>().Object, null)
            };
            transformer.Setup(x => x.Handle<WithExpression, WithExpression>(with[0]))
                .Returns(visited[0]);
            transformer.Setup(x => x.Handle<WithExpression, WithExpression>(with[1]))
                .Returns(visited[1]);

            var result = handler.Handle(command, transformer.Object);

            result.With.ElementAt(0).ShouldBeTheSameInstance(visited.ElementAt(0));
            result.With.ElementAt(1).ShouldBeTheSameInstance(visited.ElementAt(1));
        }
    }
}