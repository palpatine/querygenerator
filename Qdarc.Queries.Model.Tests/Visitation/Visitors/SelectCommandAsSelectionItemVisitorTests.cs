using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SelectCommandAsSelectionItemVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SelectCommandAsSelectionItemVisitor();
            var command = new Mock<ISelectCommand>();
            var selection = new Mock<ISqlSelection>();
            selection.Setup(x => x.Elements)
                .Returns(
                    new[]
                    {
                        new Mock<ISelectionItem>().Object
                    });
            command.Setup(x => x.Selection).Returns(selection.Object);
            var expression = new SelectCommandAsSelectionItem(
                command.Object);
            var transformer = new Mock<IVisitor>();
            var commandVisited = new Mock<ISelectCommand>();
            commandVisited.Setup(x => x.Selection).Returns(selection.Object);
            command.Setup(x => x.Accept<ISelectCommand>(transformer.Object))
                .Returns(commandVisited.Object);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result, expression);
        }

        [TestMethod]
        public void ShouldAssignVisitedCommand()
        {
            var handler = new SelectCommandAsSelectionItemVisitor();
            var command = new Mock<ISelectCommand>();
            var selection = new Mock<ISqlSelection>();
            selection.Setup(x => x.Elements)
                .Returns(
                    new[]
                    {
                        new Mock<ISelectionItem>().Object
                    });
            command.Setup(x => x.Selection).Returns(selection.Object);
            var expression = new SelectCommandAsSelectionItem(
                command.Object);
            var transformer = new Mock<IVisitor>();
            var commandVisited = new Mock<ISelectCommand>();
            commandVisited.Setup(x => x.Selection).Returns(selection.Object);
            command.Setup(x => x.Accept<ISelectCommand>(transformer.Object))
                .Returns(commandVisited.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.Command.ShouldBeTheSameInstance(commandVisited.Object);
        }

        [TestMethod]
        public void ShouldVisitCommand()
        {
            var handler = new SelectCommandAsSelectionItemVisitor();
            var command = new Mock<ISelectCommand>();
            var selection = new Mock<ISqlSelection>();
            selection.Setup(x => x.Elements)
                .Returns(
                    new[]
                    {
                        new Mock<ISelectionItem>().Object
                    });
            command.Setup(x => x.Selection).Returns(selection.Object);
            var expression = new SelectCommandAsSelectionItem(
                command.Object);
            var transformer = new Mock<IVisitor>();
            var commandVisited = new Mock<ISelectCommand>();
            commandVisited.Setup(x => x.Selection).Returns(selection.Object);
            command.Setup(x => x.Accept<ISelectCommand>(transformer.Object))
                .Returns(commandVisited.Object);

            handler.Handle(expression, transformer.Object);

            command.Verify(x => x.Accept<ISelectCommand>(transformer.Object), Times.Once);
        }
    }
}