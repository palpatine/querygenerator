using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class CurrentDateAndTimeExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new CurrentDateAndTimeExpressionVisitor();
            var expression = new CurrentDateAndTimeExpression(true, new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopyUtc()
        {
            var handler = new CurrentDateAndTimeExpressionVisitor();
            var expression = new CurrentDateAndTimeExpression(true, new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.IsUtc.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new CurrentDateAndTimeExpressionVisitor();
            var expression = new CurrentDateAndTimeExpression(true, new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }
    }
}