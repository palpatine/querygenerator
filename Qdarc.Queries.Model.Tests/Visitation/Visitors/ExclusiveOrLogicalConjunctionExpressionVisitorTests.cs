using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ExclusiveOrLogicalConjunctionExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ExclusiveOrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new ExclusiveOrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISqlLogicalExpression>().Object;
            var rightVisited = new Mock<ISqlLogicalExpression>().Object;
            var transformer = new Mock<IVisitor>().Object;
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var handler = new ExclusiveOrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new ExclusiveOrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISqlLogicalExpression>().Object;
            var rightVisited = new Mock<ISqlLogicalExpression>().Object;
            var transformer = new Mock<IVisitor>().Object;
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            right.Verify(x => x.Accept<ISqlLogicalExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedRight()
        {
            var handler = new ExclusiveOrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new ExclusiveOrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISqlLogicalExpression>().Object;
            var rightVisited = new Mock<ISqlLogicalExpression>().Object;
            var transformer = new Mock<IVisitor>().Object;
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Right.ShouldBeTheSameInstance(rightVisited);
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var handler = new ExclusiveOrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new ExclusiveOrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISqlLogicalExpression>().Object;
            var rightVisited = new Mock<ISqlLogicalExpression>().Object;
            var transformer = new Mock<IVisitor>().Object;
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            left.Verify(x => x.Accept<ISqlLogicalExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedLeft()
        {
            var handler = new ExclusiveOrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new ExclusiveOrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISqlLogicalExpression>().Object;
            var rightVisited = new Mock<ISqlLogicalExpression>().Object;
            var transformer = new Mock<IVisitor>().Object;
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Left.ShouldBeTheSameInstance(leftVisited);
        }
    }
}