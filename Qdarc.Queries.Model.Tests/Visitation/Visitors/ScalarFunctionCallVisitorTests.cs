using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ScalarFunctionCallVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ScalarFunctionCallVisitor();
            var expression = new ScalarFunctionCall(
                new ElementName("A"),
                new ISqlValueExpression[0],
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new ScalarFunctionCallVisitor();
            var expression = new ScalarFunctionCall(
                new ElementName("A"),
                new ISqlValueExpression[0],
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new ScalarFunctionCallVisitor();
            var expression = new ScalarFunctionCall(
                new ElementName("A"),
                new ISqlValueExpression[0],
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCloneElementName()
        {
            var handler = new ScalarFunctionCallVisitor();
            var expression = new ScalarFunctionCall(
                new ElementName("A"),
                new ISqlValueExpression[0],
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Name.ShouldBeEqual(expression.Name);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression.Name, result.Name);
        }

        [TestMethod]
        public void ShouldVisitEachArgument()
        {
            var handler = new ScalarFunctionCallVisitor();
            var arguments = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new ScalarFunctionCall(
                new ElementName("A"),
                arguments.Select(x => x.Object),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var visited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            arguments[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[0]);
            arguments[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[1]);

            handler.Handle(expression, transformer.Object);

            arguments[0].Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
            arguments[1].Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedArguments()
        {
            var handler = new ScalarFunctionCallVisitor();
            var arguments = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new ScalarFunctionCall(
                new ElementName("A"),
                arguments.Select(x => x.Object),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var visited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            arguments[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[0]);
            arguments[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(visited[1]);

            var result = handler.Handle(expression, transformer.Object);

            result.Arguments.ElementAt(0).ShouldBeTheSameInstance(visited[0]);
            result.Arguments.ElementAt(1).ShouldBeTheSameInstance(visited[1]);
        }
    }
}