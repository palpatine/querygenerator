using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;
using Qdarc.Utilities;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ProjectionByMembersAssignmentVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ProjectionByMembersAssignmentVisitor();
            var constructorCall = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            var boundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var visitedBoundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var expression = new ProjectionByMembersAssignment(
                constructorCall,
                boundSelectionItems);
            var transformer = new Mock<IVisitor>();
            var visitedExpression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            transformer.Setup(x => x.Handle<ProjectionByConstructor, ProjectionByConstructor>(constructorCall))
                .Returns(visitedExpression);
            transformer.Setup(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[0]))
                .Returns(visitedBoundSelectionItems[0]);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitBindings()
        {
            var handler = new ProjectionByMembersAssignmentVisitor();
            var constructorCall = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            var boundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object),
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var visitedBoundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object),
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var expression = new ProjectionByMembersAssignment(
                constructorCall,
                boundSelectionItems);
            var transformer = new Mock<IVisitor>();
            var visitedExpression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            transformer.Setup(x => x.Handle<ProjectionByConstructor, ProjectionByConstructor>(constructorCall))
                .Returns(visitedExpression);
            transformer.Setup(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[0]))
                .Returns(visitedBoundSelectionItems[0]);
            transformer.Setup(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[1]))
                .Returns(visitedBoundSelectionItems[1]);

            handler.Handle(expression, transformer.Object);

            transformer.Verify(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[0]), Times.Once);
            transformer.Verify(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[1]), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedBindings()
        {
            var handler = new ProjectionByMembersAssignmentVisitor();
            var constructorCall = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            var boundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object),
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var visitedBoundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object),
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var expression = new ProjectionByMembersAssignment(
                constructorCall,
                boundSelectionItems);
            var transformer = new Mock<IVisitor>();
            var visitedExpression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            transformer.Setup(x => x.Handle<ProjectionByConstructor, ProjectionByConstructor>(constructorCall))
                .Returns(visitedExpression);
            transformer.Setup(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[0]))
                .Returns(visitedBoundSelectionItems[0]);
            transformer.Setup(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[1]))
                .Returns(visitedBoundSelectionItems[1]);

            var result = handler.Handle(expression, transformer.Object);

            result.Bindings.ElementAt(0).ShouldBeTheSameInstance(visitedBoundSelectionItems[0]);
            result.Bindings.ElementAt(1).ShouldBeTheSameInstance(visitedBoundSelectionItems[1]);
        }

        [TestMethod]
        public void ShouldVisitConstructorCall()
        {
            var handler = new ProjectionByMembersAssignmentVisitor();
            var constructorCall = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            var boundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var visitedBoundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var expression = new ProjectionByMembersAssignment(
                constructorCall,
                boundSelectionItems);
            var transformer = new Mock<IVisitor>();
            var visitedExpression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            transformer.Setup(x => x.Handle<ProjectionByConstructor, ProjectionByConstructor>(constructorCall))
                .Returns(visitedExpression);
            transformer.Setup(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[0]))
                .Returns(visitedBoundSelectionItems[0]);

            handler.Handle(expression, transformer.Object);

            transformer.Verify(x => x.Handle<ProjectionByConstructor, ProjectionByConstructor>(constructorCall), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedConstructorCall()
        {
            var handler = new ProjectionByMembersAssignmentVisitor();
            var constructorCall = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            var boundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var visitedBoundSelectionItems = new[]
            {
                new BoundSelectionItem(ExpressionExtensions.GetProperty((Entity e) => e.Property), new Mock<ISelectionItem>().Object)
            };
            var expression = new ProjectionByMembersAssignment(
                constructorCall,
                boundSelectionItems);
            var transformer = new Mock<IVisitor>();
            var visitedConstructorCall = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity()),
                new ISelectionItem[0]);
            transformer.Setup(x => x.Handle<ProjectionByConstructor, ProjectionByConstructor>(constructorCall))
                .Returns(visitedConstructorCall);
            transformer.Setup(x => x.Handle<BoundSelectionItem, BoundSelectionItem>(boundSelectionItems[0]))
                .Returns(visitedBoundSelectionItems[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.ConstructorCall.ShouldBeTheSameInstance(visitedConstructorCall);
        }
    }
}