namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    internal class Entity
    {
        public Entity()
        {
        }

        public Entity(int property, string name)
        {
            Property = property;
            Name = name;
        }

        public Entity(int property) => Property = property;

        public int Property { get; set; }
        public string Name { get; }
    }
}