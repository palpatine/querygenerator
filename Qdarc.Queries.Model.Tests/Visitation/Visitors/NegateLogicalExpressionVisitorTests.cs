using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class NegateLogicalExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new NegateLogicalExpressionVisitor();
            var value = new Mock<ISqlLogicalExpression>();
            var expression = new NegateLogicalExpression(
                value.Object);
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlLogicalExpression>().Object;
            value.Setup(x => x.Accept<ISqlLogicalExpression>(transformer.Object))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitValue()
        {
            var handler = new NegateLogicalExpressionVisitor();
            var value = new Mock<ISqlLogicalExpression>();
            var expression = new NegateLogicalExpression(
                value.Object);
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlLogicalExpression>().Object;
            value.Setup(x => x.Accept<ISqlLogicalExpression>(transformer.Object))
                .Returns(valueVisited);

            handler.Handle(expression, transformer.Object);

            value.Verify(x => x.Accept<ISqlLogicalExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedValue()
        {
            var handler = new NegateLogicalExpressionVisitor();
            var value = new Mock<ISqlLogicalExpression>();
            var expression = new NegateLogicalExpression(
                value.Object);
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlLogicalExpression>().Object;
            value.Setup(x => x.Accept<ISqlLogicalExpression>(transformer.Object))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.LogicalExpression.ShouldBeTheSameInstance(valueVisited);
        }
    }
}