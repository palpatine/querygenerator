using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class BetweenLogicalExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldAssignVisitedValue()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            var result = handler.Handle(expression, transformer);

            result.Value.ShouldBeTheSameInstance(valueVisited);
        }

        [TestMethod]
        public void ShouldVisitValue()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            handler.Handle(expression, transformer);

            value.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedLowerBound()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            var result = handler.Handle(expression, transformer);

            result.LowerBound.ShouldBeTheSameInstance(lowerBoundVisited);
        }

        [TestMethod]
        public void ShouldVisitLowerBound()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            handler.Handle(expression, transformer);

            lowerBound.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedUpperBound()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            var result = handler.Handle(expression, transformer);

            result.UpperBound.ShouldBeTheSameInstance(upperBoundVisited);
        }

        [TestMethod]
        public void ShouldVisitUpperBound()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            handler.Handle(expression, transformer);

            upperBound.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldCopyNegated()
        {
            var handler = new BetweenLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var lowerBound = new Mock<ISqlValueExpression>();
            var upperBound = new Mock<ISqlValueExpression>();
            var expression = new BetweenLogicalExpression(
                value.Object,
                lowerBound.Object,
                upperBound.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            var lowerBoundVisited = new Mock<ISqlValueExpression>().Object;
            lowerBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(lowerBoundVisited);
            var upperBoundVisited = new Mock<ISqlValueExpression>().Object;
            upperBound.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(upperBoundVisited);

            var result = handler.Handle(expression, transformer);

            result.IsNegated.ShouldBeEqual(expression.IsNegated);
        }
    }
}