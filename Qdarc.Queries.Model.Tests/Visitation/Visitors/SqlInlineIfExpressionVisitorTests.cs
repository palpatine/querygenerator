using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlInlineIfExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldAssignVisitedIfTrue()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            var result = handler.Handle(expression, transformer);

            result.IfTrue.ShouldBeTheSameInstance(ifTrueVisited);
        }

        [TestMethod]
        public void ShouldVisitIfTrue()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            handler.Handle(expression, transformer);

            ifTrue.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedCondition()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            var result = handler.Handle(expression, transformer);

            result.Condition.ShouldBeTheSameInstance(conditionVisited);
        }

        [TestMethod]
        public void ShouldVisitCondition()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            handler.Handle(expression, transformer);

            condition.Verify(x => x.Accept<ISqlLogicalExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedIfFalse()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            var result = handler.Handle(expression, transformer);

            result.IfFalse.ShouldBeTheSameInstance(ifFalseVisited);
        }

        [TestMethod]
        public void ShouldVisitIfFalse()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            handler.Handle(expression, transformer);

            ifFalse.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            var result = handler.Handle(expression, transformer);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlInlineIfExpressionVisitor();
            var condition = new Mock<ISqlLogicalExpression>();
            var ifTrue = new Mock<ISqlValueExpression>();
            var ifFalse = new Mock<ISqlValueExpression>();
            var expression = new SqlInlineIfExpression(
                condition.Object,
                ifTrue.Object,
                ifFalse.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var conditionVisited = new Mock<ISqlLogicalExpression>().Object;
            var ifTrueVisited = new Mock<ISqlValueExpression>().Object;
            var ifFalseVisited = new Mock<ISqlValueExpression>().Object;
            condition.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(conditionVisited);
            ifTrue.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifTrueVisited);
            ifFalse.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(ifFalseVisited);

            var result = handler.Handle(expression, transformer);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }
    }
}