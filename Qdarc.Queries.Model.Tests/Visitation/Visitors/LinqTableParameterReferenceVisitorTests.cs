using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class LinqTableParameterReferenceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new LinqTableParameterReferenceVisitor();
            var expression = new LinqTableParameterReference(
                Expression.Constant(3),
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new LinqTableParameterReferenceVisitor();
            var expression = new LinqTableParameterReference(
                Expression.Constant(3),
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new LinqTableParameterReferenceVisitor();
            var expression = new LinqTableParameterReference(
                Expression.Constant(3),
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCopyName()
        {
            var handler = new LinqTableParameterReferenceVisitor();
            var expression = new LinqTableParameterReference(
                Expression.Constant(3),
                typeof(int),
                new Mock<ISqlType>().Object,
                "Name");

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Name.ShouldBeTheSameInstance(expression.Name);
        }

        [TestMethod]
        public void ShouldCopyExpression()
        {
            var handler = new LinqTableParameterReferenceVisitor();
            var expression = new LinqTableParameterReference(
                Expression.Constant(3),
                typeof(int),
                new Mock<ISqlType>().Object,
                "Name");

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Expression.ShouldBeTheSameInstance(expression.Expression);
        }
    }
}