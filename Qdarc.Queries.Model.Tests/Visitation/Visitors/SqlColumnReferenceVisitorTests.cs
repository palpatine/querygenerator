using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlColumnReferenceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlColumnReferenceVisitor();
            var source = new Mock<ISource>();
            var expression = new SqlColumnReference(
                new ElementName("Name"),
                source.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var sourceVisited = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer))
                .Returns(sourceVisited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlColumnReferenceVisitor();
            var source = new Mock<ISource>();
            var expression = new SqlColumnReference(
                new ElementName("Name"),
                source.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var sourceVisited = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer))
                .Returns(sourceVisited);

            var result = handler.Handle(expression, transformer);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlColumnReferenceVisitor();
            var source = new Mock<ISource>();
            var expression = new SqlColumnReference(
                new ElementName("Name"),
                source.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var sourceVisited = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer))
                .Returns(sourceVisited);

            var result = handler.Handle(expression, transformer);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCopyColumnName()
        {
            var handler = new SqlColumnReferenceVisitor();
            var source = new Mock<ISource>();
            var expression = new SqlColumnReference(
                new ElementName("Name"),
                source.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var sourceVisited = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer))
                .Returns(sourceVisited);

            var result = handler.Handle(expression, transformer);

            result.ColumnName.ShouldBeEqual(expression.ColumnName);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result.ColumnName, expression.ColumnName);
        }

        [TestMethod]
        public void ShouldVisitSource()
        {
            var handler = new SqlColumnReferenceVisitor();
            var source = new Mock<ISource>();
            var expression = new SqlColumnReference(
                new ElementName("Name"),
                source.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var sourceVisited = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer))
                .Returns(sourceVisited);

            handler.Handle(expression, transformer);

            source.Verify(x => x.Accept<ISource>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSource()
        {
            var handler = new SqlColumnReferenceVisitor();
            var source = new Mock<ISource>();
            var expression = new SqlColumnReference(
                new ElementName("Name"),
                source.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var sourceVisited = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer))
                .Returns(sourceVisited);

            var result = handler.Handle(expression, transformer);

            result.Source.ShouldBeTheSameInstance(sourceVisited);
        }
    }
}