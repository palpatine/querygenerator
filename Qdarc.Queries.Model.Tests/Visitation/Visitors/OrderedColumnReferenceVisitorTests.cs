using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class OrderedColumnReferenceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new OrderedColumnReferenceVisitor();
            var columnReference = new Mock<IColumnReference>();
            var expression = new OrderedColumnReference(
                columnReference.Object);
            var transformer = new Mock<IVisitor>();
            var columnReferenceVisited = new Mock<IColumnReference>().Object;
            columnReference.Setup(x => x.Accept<IAssignable>(transformer.Object))
                .Returns(columnReferenceVisited);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitColumnReference()
        {
            var handler = new OrderedColumnReferenceVisitor();
            var columnReference = new Mock<IColumnReference>();
            var expression = new OrderedColumnReference(
                columnReference.Object);
            var transformer = new Mock<IVisitor>();
            var columnReferenceVisited = new Mock<IColumnReference>().Object;
            columnReference.Setup(x => x.Accept<IColumnReference>(transformer.Object))
                .Returns(columnReferenceVisited);

            handler.Handle(expression, transformer.Object);

            columnReference.Verify(x => x.Accept<IColumnReference>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedColumnReference()
        {
            var handler = new OrderedColumnReferenceVisitor();
            var columnReference = new Mock<IColumnReference>();
            var expression = new OrderedColumnReference(
                columnReference.Object);
            var transformer = new Mock<IVisitor>();
            var columnReferenceVisited = new Mock<IColumnReference>().Object;
            columnReference.Setup(x => x.Accept<IColumnReference>(transformer.Object))
                .Returns(columnReferenceVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.ColumnReference.ShouldBeTheSameInstance(columnReferenceVisited);
        }

        [TestMethod]
        public void ShouldCopyAscending()
        {
            var handler = new OrderedColumnReferenceVisitor();
            var columnReference = new Mock<IColumnReference>();
            var expression = new OrderedColumnReference(
                columnReference.Object,
                true);
            var transformer = new Mock<IVisitor>();
            var columnReferenceVisited = new Mock<IColumnReference>().Object;
            columnReference.Setup(x => x.Accept<IAssignable>(transformer.Object))
                .Returns(columnReferenceVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Ascending.ShouldBeEqual(expression.Ascending);
        }
    }
}