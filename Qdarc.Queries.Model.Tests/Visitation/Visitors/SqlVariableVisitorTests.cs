using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlVariableVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlVariableVisitor();
            var expression = new SqlVariable(
                "Name",
                true,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlVariableVisitor();
            var expression = new SqlVariable(
                "Name",
                true,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlVariableVisitor();
            var expression = new SqlVariable(
                "Name",
                true,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCopyName()
        {
            var handler = new SqlVariableVisitor();
            var expression = new SqlVariable(
                "Name",
                true,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Name.ShouldBeTheSameInstance(expression.Name);
        }

        [TestMethod]
        public void ShouldCopyIsSystem()
        {
            var handler = new SqlVariableVisitor();
            var expression = new SqlVariable(
                "Name",
                true,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.IsSystem.ShouldBeEqual(expression.IsSystem);
        }
    }
}