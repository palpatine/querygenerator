using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class AliasedSourceItemVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new AliasedSourceItemVisitor();
            var source = new Mock<ISource>();
            var expression = new AliasedSourceItem(
                source.Object);
            var transformer = new Mock<IVisitor>();
            var sourceTransformed = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer.Object))
                .Returns(sourceTransformed);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitSource()
        {
            var handler = new AliasedSourceItemVisitor();
            var source = new Mock<ISource>();
            var expression = new AliasedSourceItem(
                source.Object);
            var transformer = new Mock<IVisitor>();
            var sourceTransformed = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer.Object))
                .Returns(sourceTransformed);

            handler.Handle(expression, transformer.Object);

            source.Verify(x => x.Accept<ISource>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSource()
        {
            var handler = new AliasedSourceItemVisitor();
            var source = new Mock<ISource>();
            var expression = new AliasedSourceItem(
                source.Object);
            var transformer = new Mock<IVisitor>();
            var visitedSource = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer.Object))
                .Returns(visitedSource);

            var result = handler.Handle(expression, transformer.Object);

            result.Source.ShouldBeTheSameInstance(visitedSource);
        }

        [TestMethod]
        public void ShouldCopyAlias()
        {
            var handler = new AliasedSourceItemVisitor();
            var source = new Mock<ISource>();
            var expression = new AliasedSourceItem(
                source.Object,
                alias: "Name");
            var transformer = new Mock<IVisitor>();
            var visitedSource = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer.Object))
                .Returns(visitedSource);

            var result = handler.Handle(expression, transformer.Object);

            result.Alias.ShouldBeTheSameInstance(expression.Alias);
        }

        [TestMethod]
        public void ShouldVisitColumnAliases()
        {
            var handler = new AliasedSourceItemVisitor();
            var source = new Mock<ISource>();
            var aliases = new[]
            {
                new ColumnAlias("Name1", null, null),
                new ColumnAlias("Name2", null, null)
            };
            var expression = new AliasedSourceItem(
                source.Object,
                aliases);
            var visitedAliases = new[]
            {
                new ColumnAlias("Name1", null, null),
                new ColumnAlias("Name2", null, null)
            };
            var transformer = new Mock<IVisitor>();
            var visitedSource = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer.Object))
                .Returns(visitedSource);
            transformer.Setup(x => x.Handle<ColumnAlias, ColumnAlias>(aliases[0]))
                .Returns(visitedAliases[0]);
            transformer.Setup(x => x.Handle<ColumnAlias, ColumnAlias>(aliases[1]))
                .Returns(visitedAliases[1]);

            handler.Handle(expression, transformer.Object);

            transformer.Verify(x => x.Handle<ColumnAlias, ColumnAlias>(aliases[0]), Times.Once);
            transformer.Verify(x => x.Handle<ColumnAlias, ColumnAlias>(aliases[1]), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedAliases()
        {
            var handler = new AliasedSourceItemVisitor();
            var source = new Mock<ISource>();
            var aliases = new[]
            {
                new ColumnAlias("Name1", null, null),
                new ColumnAlias("Name2", null, null)
            };
            var expression = new AliasedSourceItem(
                source.Object,
                aliases);
            var visitedAliases = new[]
            {
                new ColumnAlias("Name1", null, null),
                new ColumnAlias("Name2", null, null)
            };
            var transformer = new Mock<IVisitor>();
            var visitedSource = new Mock<ISource>().Object;
            source.Setup(x => x.Accept<ISource>(transformer.Object))
                .Returns(visitedSource);
            transformer.Setup(x => x.Handle<ColumnAlias, ColumnAlias>(aliases[0]))
                .Returns(visitedAliases[0]);
            transformer.Setup(x => x.Handle<ColumnAlias, ColumnAlias>(aliases[1]))
                .Returns(visitedAliases[1]);

            var result = handler.Handle(expression, transformer.Object);

            result.Source.ShouldBeTheSameInstance(visitedSource);
        }
    }
}