using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ExistsExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ExistsExpressionVisitor();
            var select = new Mock<ISelectCommand>();
            var expression = new ExistsExpression(
                select.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            select.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(selectVisited);
            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitSelect()
        {
            var handler = new ExistsExpressionVisitor();
            var select = new Mock<ISelectCommand>();
            var expression = new ExistsExpression(
                select.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            select.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(selectVisited);
            handler.Handle(expression, transformer);

            select.Verify(x => x.Accept<ISelectCommand>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSelect()
        {
            var handler = new ExistsExpressionVisitor();
            var select = new Mock<ISelectCommand>();
            var expression = new ExistsExpression(
                select.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            select.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(selectVisited);
            var result = handler.Handle(expression, transformer);

            result.Select.ShouldBeTheSameInstance(selectVisited);
        }

        [TestMethod]
        public void ShouldCopyIsNegated()
        {
            var handler = new ExistsExpressionVisitor();
            var select = new Mock<ISelectCommand>();
            var expression = new ExistsExpression(
                select.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            select.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(selectVisited);
            var result = handler.Handle(expression, transformer);

            result.IsNegated.ShouldBeEqual(expression.IsNegated);
        }
    }
}