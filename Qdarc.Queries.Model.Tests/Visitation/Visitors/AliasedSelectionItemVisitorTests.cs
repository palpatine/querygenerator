using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class AliasedSelectionItemVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new AliasedSelectionItemVisitor();
            var selection = new Mock<ISelectionItem>();
            var expression = new AliasedSelectionItem(
                selection.Object);
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISelectionItem>().Object;
            selection.Setup(x => x.Accept<ISelectionItem>(transformer.Object))
                .Returns(selectionVisited);
            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result, expression);
        }

        [TestMethod]
        public void ShouldAssignVisitedSelection()
        {
            var handler = new AliasedSelectionItemVisitor();
            var selection = new Mock<ISelectionItem>();
            var expression = new AliasedSelectionItem(
                selection.Object);
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISelectionItem>().Object;
            selection.Setup(x => x.Accept<ISelectionItem>(transformer.Object))
                .Returns(selectionVisited);
            var result = handler.Handle(expression, transformer.Object);

            result.Item.ShouldBeTheSameInstance(selectionVisited);
        }

        [TestMethod]
        public void ShouldCopyAlias()
        {
            var handler = new AliasedSelectionItemVisitor();
            var selection = new Mock<ISelectionItem>();
            var expression = new AliasedSelectionItem(
                selection.Object,
                "name");
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISelectionItem>().Object;
            selection.Setup(x => x.Accept<ISelectionItem>(transformer.Object))
                .Returns(selectionVisited);
            var result = handler.Handle(expression, transformer.Object);

            result.Alias.ShouldBeEqual(expression.Alias);
        }

        [TestMethod]
        public void ShouldVisitSelection()
        {
            var handler = new AliasedSelectionItemVisitor();
            var selection = new Mock<ISelectionItem>();
            var expression = new AliasedSelectionItem(
                selection.Object);
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISelectionItem>().Object;
            selection.Setup(x => x.Accept<ISelectionItem>(transformer.Object))
                .Returns(selectionVisited);

            handler.Handle(expression, transformer.Object);

            selection.Verify(x => x.Accept<ISelectionItem>(transformer.Object), Times.Once);
        }
    }
}