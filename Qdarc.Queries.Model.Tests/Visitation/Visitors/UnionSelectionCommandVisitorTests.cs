using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class UnionSelectionCommandVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new UnionSelectionCommandVisitor();
            var left = new Mock<ISelectCommand>();
            var right = new Mock<ISelectCommand>();
            var transformer = new Mock<IVisitor>().Object;
            var expression = new UnionSelectionCommand(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISelectCommand>().Object;
            var rightVisited = new Mock<ISelectCommand>().Object;
            left.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var handler = new UnionSelectionCommandVisitor();
            var left = new Mock<ISelectCommand>();
            var right = new Mock<ISelectCommand>();
            var transformer = new Mock<IVisitor>().Object;
            var expression = new UnionSelectionCommand(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISelectCommand>().Object;
            var rightVisited = new Mock<ISelectCommand>().Object;
            left.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            left.Verify(x => x.Accept<ISelectCommand>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedLeft()
        {
            var handler = new UnionSelectionCommandVisitor();
            var left = new Mock<ISelectCommand>();
            var right = new Mock<ISelectCommand>();
            var transformer = new Mock<IVisitor>().Object;
            var expression = new UnionSelectionCommand(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISelectCommand>().Object;
            var rightVisited = new Mock<ISelectCommand>().Object;
            left.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Left.ShouldBeTheSameInstance(leftVisited);
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var handler = new UnionSelectionCommandVisitor();
            var left = new Mock<ISelectCommand>();
            var right = new Mock<ISelectCommand>();
            var transformer = new Mock<IVisitor>().Object;
            var expression = new UnionSelectionCommand(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISelectCommand>().Object;
            var rightVisited = new Mock<ISelectCommand>().Object;
            left.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            right.Verify(x => x.Accept<ISelectCommand>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedRight()
        {
            var handler = new UnionSelectionCommandVisitor();
            var left = new Mock<ISelectCommand>();
            var right = new Mock<ISelectCommand>();
            var transformer = new Mock<IVisitor>().Object;
            var expression = new UnionSelectionCommand(
                left.Object,
                right.Object);
            var leftVisited = new Mock<ISelectCommand>().Object;
            var rightVisited = new Mock<ISelectCommand>().Object;
            left.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Right.ShouldBeTheSameInstance(rightVisited);
        }
    }
}