using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ModuloValueExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ModuloValueExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new ModuloValueExpression(
                left.Object,
                right.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new ModuloValueExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new ModuloValueExpression(
                left.Object,
                right.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new ModuloValueExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new ModuloValueExpression(
                left.Object,
                right.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var handler = new ModuloValueExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new ModuloValueExpression(
                left.Object,
                right.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            handler.Handle(expression, transformer.Object);

            left.Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var handler = new ModuloValueExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new ModuloValueExpression(
                left.Object,
                right.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            handler.Handle(expression, transformer.Object);

            right.Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedLeft()
        {
            var handler = new ModuloValueExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new ModuloValueExpression(
                left.Object,
                right.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Left.ShouldBeTheSameInstance(leftVisited);
        }

        [TestMethod]
        public void ShouldAssignVisitedRight()
        {
            var handler = new ModuloValueExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new ModuloValueExpression(
                left.Object,
                right.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Right.ShouldBeTheSameInstance(rightVisited);
        }
    }
}