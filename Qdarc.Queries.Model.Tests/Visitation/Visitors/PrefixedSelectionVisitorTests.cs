using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class PrefixedSelectionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new PrefixedSelectionVisitor();
            var selection = new Mock<ISqlSelection>();
            var expression = new PrefixedSelection(
                "name",
                selection.Object);
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISqlSelection>().Object;
            selection.Setup(x => x.Accept<ISqlSelection>(transformer.Object))
                .Returns(selectionVisited);
            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result, expression);
        }

        [TestMethod]
        public void ShouldAssignVisitedSelection()
        {
            var handler = new PrefixedSelectionVisitor();
            var selection = new Mock<ISqlSelection>();
            var expression = new PrefixedSelection(
                "name",
                selection.Object);
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISqlSelection>().Object;
            selection.Setup(x => x.Accept<ISqlSelection>(transformer.Object))
                .Returns(selectionVisited);
            var result = handler.Handle(expression, transformer.Object);

            result.Selection.ShouldBeTheSameInstance(selectionVisited);
        }

        [TestMethod]
        public void ShouldCopyPrefix()
        {
            var handler = new PrefixedSelectionVisitor();
            var selection = new Mock<ISqlSelection>();
            var expression = new PrefixedSelection(
                "name",
                selection.Object);
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISqlSelection>().Object;
            selection.Setup(x => x.Accept<ISqlSelection>(transformer.Object))
                .Returns(selectionVisited);
            var result = handler.Handle(expression, transformer.Object);

            result.Prefix.ShouldBeEqual(expression.Prefix);
        }

        [TestMethod]
        public void ShouldVisitSelection()
        {
            var handler = new PrefixedSelectionVisitor();
            var selection = new Mock<ISqlSelection>();
            var expression = new PrefixedSelection(
                "name",
                selection.Object);
            var transformer = new Mock<IVisitor>();
            var selectionVisited = new Mock<ISqlSelection>().Object;
            selection.Setup(x => x.Accept<ISqlSelection>(transformer.Object))
                .Returns(selectionVisited);

            handler.Handle(expression, transformer.Object);

            selection.Verify(x => x.Accept<ISqlSelection>(transformer.Object), Times.Once);
        }
    }
}