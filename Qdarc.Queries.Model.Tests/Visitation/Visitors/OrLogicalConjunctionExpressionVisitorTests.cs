using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class OrLogicalConjunctionExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new OrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlLogicalExpression>();
            var rightVisited = new Mock<ISqlLogicalExpression>();
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited.Object);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited.Object);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var handler = new OrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlLogicalExpression>();
            var rightVisited = new Mock<ISqlLogicalExpression>();
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited.Object);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited.Object);

            var result = handler.Handle(expression, transformer);

            result.Left.ShouldBeTheSameInstance(leftVisited.Object);
        }

        [TestMethod]
        public void ShouldAssignVisitedLeft()
        {
            var handler = new OrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlLogicalExpression>();
            var rightVisited = new Mock<ISqlLogicalExpression>();
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited.Object);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited.Object);

            var result = handler.Handle(expression, transformer);

            result.Left.ShouldBeTheSameInstance(leftVisited.Object);
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var handler = new OrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlLogicalExpression>();
            var rightVisited = new Mock<ISqlLogicalExpression>();
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited.Object);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited.Object);

            var result = handler.Handle(expression, transformer);

            result.Right.ShouldBeTheSameInstance(rightVisited.Object);
        }

        [TestMethod]
        public void ShouldAssignVisitedRight()
        {
            var handler = new OrLogicalConjunctionExpressionVisitor();
            var left = new Mock<ISqlLogicalExpression>();
            var right = new Mock<ISqlLogicalExpression>();
            var expression = new OrLogicalConjunctionExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlLogicalExpression>();
            var rightVisited = new Mock<ISqlLogicalExpression>();
            left.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(leftVisited.Object);
            right.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(rightVisited.Object);

            var result = handler.Handle(expression, transformer);

            result.Right.ShouldBeTheSameInstance(rightVisited.Object);
        }
    }
}