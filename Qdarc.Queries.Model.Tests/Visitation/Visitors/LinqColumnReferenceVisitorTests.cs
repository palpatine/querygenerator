using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;
using Qdarc.Utilities;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class LinqColumnReferenceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new LinqColumnReferenceVisitor();
            var source = new Mock<INamedSource>();
            var expression = new LinqColumnReference(
                source.Object,
                ExpressionExtensions.GetProperty((Entity e) => e.Property),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>();
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited.Object);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new LinqColumnReferenceVisitor();
            var source = new Mock<INamedSource>();
            var expression = new LinqColumnReference(
                source.Object,
                ExpressionExtensions.GetProperty((Entity e) => e.Property),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>();
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new LinqColumnReferenceVisitor();
            var source = new Mock<INamedSource>();
            var expression = new LinqColumnReference(
                source.Object,
                ExpressionExtensions.GetProperty((Entity e) => e.Property),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>();
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCopyProperty()
        {
            var handler = new LinqColumnReferenceVisitor();
            var source = new Mock<INamedSource>();
            var expression = new LinqColumnReference(
                source.Object,
                ExpressionExtensions.GetProperty((Entity e) => e.Property),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>();
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.Property.ShouldBeTheSameInstance(expression.Property);
        }

        [TestMethod]
        public void ShouldVisitedSource()
        {
            var handler = new LinqColumnReferenceVisitor();
            var source = new Mock<INamedSource>();
            var expression = new LinqColumnReference(
                source.Object,
                ExpressionExtensions.GetProperty((Entity e) => e.Property),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>();
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited.Object);

            handler.Handle(expression, transformer.Object);

            source.Verify(x => x.Accept<INamedSource>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSource()
        {
            var handler = new LinqColumnReferenceVisitor();
            var source = new Mock<INamedSource>();
            var expression = new LinqColumnReference(
                source.Object,
                ExpressionExtensions.GetProperty((Entity e) => e.Property),
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>();
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.Source.ShouldBeTheSameInstance(sourceVisited.Object);
        }
    }
}