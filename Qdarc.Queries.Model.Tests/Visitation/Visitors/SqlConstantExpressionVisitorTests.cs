using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlConstantExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlConstantExpressionVisitor();
            var expression = new SqlConstantExpression(
                9,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlConstantExpressionVisitor();
            var expression = new SqlConstantExpression(
                9,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlConstantExpressionVisitor();
            var expression = new SqlConstantExpression(
                9,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCopyValue()
        {
            var handler = new SqlConstantExpressionVisitor();
            var expression = new SqlConstantExpression(
                9,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.Value.ShouldBeTheSameInstance(expression.Value);
        }
    }
}