using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlCaseExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlCaseExpressionVisitor();
            var cases = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseExpression(
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlCaseExpressionVisitor();
            var cases = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseExpression(
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlCaseExpressionVisitor();
            var cases = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseExpression(
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldVisitEachCase()
        {
            var handler = new SqlCaseExpressionVisitor();
            var cases = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object),
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseExpression(
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();

            handler.Handle(expression, transformer.Object);

            transformer.Verify(x => x.Handle<SqlLogicalWhenThen, SqlLogicalWhenThen>(cases[0]), Times.Once);
            transformer.Verify(x => x.Handle<SqlLogicalWhenThen, SqlLogicalWhenThen>(cases[1]), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedCases()
        {
            var handler = new SqlCaseExpressionVisitor();
            var cases = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object),
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseExpression(
                cases,
                null,
                typeof(int),
                new Mock<ISqlType>().Object);
            var visited = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object),
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var transformer = new Mock<IVisitor>();

            transformer.Setup(x => x.Handle<SqlLogicalWhenThen, SqlLogicalWhenThen>(cases[0]))
                .Returns(visited[0]);
            transformer.Setup(x => x.Handle<SqlLogicalWhenThen, SqlLogicalWhenThen>(cases[1]))
                .Returns(visited[1]);

            var result = handler.Handle(expression, transformer.Object);

            result.Cases.ElementAt(0).ShouldBeTheSameInstance(visited[0]);
            result.Cases.ElementAt(1).ShouldBeTheSameInstance(visited[1]);
        }

        [TestMethod]
        public void ShouldVisitElse()
        {
            var handler = new SqlCaseExpressionVisitor();
            var elseExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseExpression(
                cases,
                elseExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            handler.Handle(expression, transformer);

            elseExpression.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedElse()
        {
            var handler = new SqlCaseExpressionVisitor();
            var elseExpression = new Mock<ISqlValueExpression>();
            var cases = new[]
            {
                new SqlLogicalWhenThen(new Mock<ISqlLogicalExpression>().Object, new Mock<ISqlValueExpression>().Object)
            };
            var expression = new SqlCaseExpression(
                cases,
                elseExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            elseExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.Else.ShouldBeTheSameInstance(visited);
        }
    }
}