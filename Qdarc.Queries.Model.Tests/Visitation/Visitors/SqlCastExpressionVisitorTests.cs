using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlCastExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlCastExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new SqlCastExpression(
                valueExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlCastExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new SqlCastExpression(
                valueExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlCastExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new SqlCastExpression(
                valueExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldAssignVisitedExpression()
        {
            var handler = new SqlCastExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new SqlCastExpression(
                valueExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.Expression.ShouldBeTheSameInstance(visited);
        }

        [TestMethod]
        public void ShouldVisitExpression()
        {
            var handler = new SqlCastExpressionVisitor();
            var valueExpression = new Mock<ISqlValueExpression>();
            var expression = new SqlCastExpression(
                valueExpression.Object,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            valueExpression.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            handler.Handle(expression, transformer);

            valueExpression.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }
    }
}