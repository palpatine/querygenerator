using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlAsteriskCountExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlAsteriskCountExpressionVisitor();
            var expression = new SqlAsteriskCountExpression(
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlAsteriskCountExpressionVisitor();
            var expression = new SqlAsteriskCountExpression(
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlAsteriskCountExpressionVisitor();
            var expression = new SqlAsteriskCountExpression(
                typeof(int),
                new Mock<ISqlType>().Object);

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }
    }
}