using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;
using Qdarc.Utilities;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ProjectionByConstructorVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ProjectionByConstructorVisitor();
            var elements = new[]
            {
                new Mock<ISelectionItem>()
            };
            var expression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity(4)),
                elements.Select(x => x.Object));
            var transformer = new Mock<IVisitor>().Object;
            var visitedElements = new[]
            {
                new Mock<ISelectionItem>().Object
            };
            elements[0]
                .Setup(x => x.Accept<ISelectionItem>(transformer))
                .Returns(visitedElements[0]);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitElements()
        {
            var handler = new ProjectionByConstructorVisitor();
            var elements = new[]
            {
                new Mock<ISelectionItem>(),
                new Mock<ISelectionItem>()
            };
            var expression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity(4, "n")),
                elements.Select(x => x.Object));
            var transformer = new Mock<IVisitor>().Object;
            var visitedElements = new[]
            {
                new Mock<ISelectionItem>().Object,
                new Mock<ISelectionItem>().Object
            };
            elements[0]
                .Setup(x => x.Accept<ISelectionItem>(transformer))
                .Returns(visitedElements[0]);
            elements[1]
                .Setup(x => x.Accept<ISelectionItem>(transformer))
                .Returns(visitedElements[1]);

            handler.Handle(expression, transformer);

            elements[0]
                .Verify(x => x.Accept<ISelectionItem>(transformer), Times.Once);
            elements[1]
                .Verify(x => x.Accept<ISelectionItem>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedElements()
        {
            var handler = new ProjectionByConstructorVisitor();
            var elements = new[]
            {
                new Mock<ISelectionItem>(),
                new Mock<ISelectionItem>()
            };
            var expression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity(4, "n")),
                elements.Select(x => x.Object));
            var transformer = new Mock<IVisitor>().Object;
            var visitedElements = new[]
            {
                new Mock<ISelectionItem>().Object,
                new Mock<ISelectionItem>().Object
            };
            elements[0]
                .Setup(x => x.Accept<ISelectionItem>(transformer))
                .Returns(visitedElements[0]);
            elements[1]
                .Setup(x => x.Accept<ISelectionItem>(transformer))
                .Returns(visitedElements[1]);

            var result = handler.Handle(expression, transformer);

            result.Elements.ElementAt(0).ShouldBeTheSameInstance(visitedElements[0]);
            result.Elements.ElementAt(1).ShouldBeTheSameInstance(visitedElements[1]);
        }

        [TestMethod]
        public void ShouldCopyConstructor()
        {
            var handler = new ProjectionByConstructorVisitor();
            var elements = new[]
            {
                new Mock<ISelectionItem>(),
                new Mock<ISelectionItem>()
            };
            var expression = new ProjectionByConstructor(
                ExpressionExtensions.GetConstructor(() => new Entity(4, "n")),
                elements.Select(x => x.Object));
            var transformer = new Mock<IVisitor>().Object;
            var visitedElements = new[]
            {
                new Mock<ISelectionItem>().Object,
                new Mock<ISelectionItem>().Object
            };
            elements[0]
                .Setup(x => x.Accept<ISelectionItem>(transformer))
                .Returns(visitedElements[0]);
            elements[1]
                .Setup(x => x.Accept<ISelectionItem>(transformer))
                .Returns(visitedElements[1]);

            var result = handler.Handle(expression, transformer);

            result.Constructor.ShouldBeTheSameInstance(expression.Constructor);
        }
    }
}