using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlCountExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlCountExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                true,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new SqlCountExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                true,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new SqlCountExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                true,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldAssignVisitedValue()
        {
            var handler = new SqlCountExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                true,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.Value.ShouldBeTheSameInstance(visited);
        }

        [TestMethod]
        public void ShouldVisitValue()
        {
            var handler = new SqlCountExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlCountExpression(
                value.Object,
                true,
                typeof(int),
                new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(visited);

            handler.Handle(expression, transformer);

            value.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }
    }
}