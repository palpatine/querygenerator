using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlLogicalNegateExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlLogicalNegateExpressionVisitor();
            var value = new Mock<ISqlLogicalExpression>();
            var expression = new SqlLogicalNegateExpression(
                value.Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlLogicalExpression>().Object;
            value.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldAssignVisitedValue()
        {
            var handler = new SqlLogicalNegateExpressionVisitor();
            var value = new Mock<ISqlLogicalExpression>();
            var expression = new SqlLogicalNegateExpression(
                value.Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlLogicalExpression>().Object;
            value.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(visited);

            var result = handler.Handle(expression, transformer);

            result.Value.ShouldBeTheSameInstance(visited);
        }

        [TestMethod]
        public void ShouldVisitValue()
        {
            var handler = new SqlLogicalNegateExpressionVisitor();
            var value = new Mock<ISqlLogicalExpression>();
            var expression = new SqlLogicalNegateExpression(
                value.Object);
            var transformer = new Mock<IVisitor>().Object;
            var visited = new Mock<ISqlLogicalExpression>().Object;
            value.Setup(x => x.Accept<ISqlLogicalExpression>(transformer))
                .Returns(visited);

            handler.Handle(expression, transformer);

            value.Verify(x => x.Accept<ISqlLogicalExpression>(transformer), Times.Once);
        }
    }
}