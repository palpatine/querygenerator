using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class ValuesSourceVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new ValuesSourceVisitor();
            var rows = new[]
            {
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    })
            };
            var expression = new ValuesSource(
                rows,
                typeof(int));
            var rowsVisited = new[]
            {
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    })
            };
            var transformer = new Mock<IVisitor>();
            transformer.Setup(x => x.Handle<ValueSourceRow, ValueSourceRow>(rows[0]))
                .Returns(rowsVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new ValuesSourceVisitor();
            var rows = new[]
            {
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    })
            };
            var expression = new ValuesSource(
                rows,
                typeof(int));
            var rowsVisited = new[]
            {
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    })
            };
            var transformer = new Mock<IVisitor>();
            transformer.Setup(x => x.Handle<ValueSourceRow, ValueSourceRow>(rows[0]))
                .Returns(rowsVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldVisitRows()
        {
            var handler = new ValuesSourceVisitor();
            var rows = new[]
            {
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    }),
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    })
            };
            var expression = new ValuesSource(
                rows,
                typeof(int));
            var rowsVisited = new[]
            {
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    }),
                new ValueSourceRow(
                    new[]
                    {
                        new Mock<ISqlValueExpression>().Object
                    })
            };
            var transformer = new Mock<IVisitor>();
            transformer.Setup(x => x.Handle<ValueSourceRow, ValueSourceRow>(rows[0]))
                .Returns(rowsVisited[0]);
            transformer.Setup(x => x.Handle<ValueSourceRow, ValueSourceRow>(rows[1]))
                .Returns(rowsVisited[1]);

            handler.Handle(expression, transformer.Object);

            transformer.Verify(x => x.Handle<ValueSourceRow, ValueSourceRow>(rows[0]), Times.Once);
            transformer.Verify(x => x.Handle<ValueSourceRow, ValueSourceRow>(rows[1]), Times.Once);
        }
    }
}