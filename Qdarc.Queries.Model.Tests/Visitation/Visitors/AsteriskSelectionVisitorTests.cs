using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class AsteriskSelectionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new AsteriskSelectionVisitor();
            var expression = new AsteriskSelection();
            var transformer = new Mock<IVisitor>();

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result, expression);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new AsteriskSelectionVisitor();
            var expression = new AsteriskSelection(
                clrType: typeof(Entity));
            var transformer = new Mock<IVisitor>();

            var result = handler.Handle(expression, transformer.Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new AsteriskSelectionVisitor();
            var expression = new AsteriskSelection(
                sqlType: new Mock<ISqlType>().Object);
            var transformer = new Mock<IVisitor>();

            var result = handler.Handle(expression, transformer.Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldVisitSource()
        {
            var handler = new AsteriskSelectionVisitor();
            var source = new Mock<INamedSource>();
            var expression = new AsteriskSelection(source.Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>().Object;
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited);

            handler.Handle(expression, transformer.Object);

            source.Verify(x => x.Accept<INamedSource>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSource()
        {
            var handler = new AsteriskSelectionVisitor();
            var source = new Mock<INamedSource>();
            var expression = new AsteriskSelection(source.Object);
            var transformer = new Mock<IVisitor>();
            var sourceVisited = new Mock<INamedSource>().Object;
            source.Setup(x => x.Accept<INamedSource>(transformer.Object))
                .Returns(sourceVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Source.ShouldBeTheSameInstance(sourceVisited);
        }
    }
}