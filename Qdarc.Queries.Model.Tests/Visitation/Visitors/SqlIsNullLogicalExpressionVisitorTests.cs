using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlIsNullLogicalExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new SqlIsNullLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlIsNullLogicalExpression(
                value.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitValue()
        {
            var handler = new SqlIsNullLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlIsNullLogicalExpression(
                value.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);

            handler.Handle(expression, transformer);

            value.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedValue()
        {
            var handler = new SqlIsNullLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlIsNullLogicalExpression(
                value.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer);

            result.Expression.ShouldBeTheSameInstance(valueVisited);
        }

        [TestMethod]
        public void ShouldCopyIsNegated()
        {
            var handler = new SqlIsNullLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var expression = new SqlIsNullLogicalExpression(
                value.Object,
                true);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);

            var result = handler.Handle(expression, transformer);

            result.IsNegated.ShouldBeEqual(expression.IsNegated);
        }
    }
}