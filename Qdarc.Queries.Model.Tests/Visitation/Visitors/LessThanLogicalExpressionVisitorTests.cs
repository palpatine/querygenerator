using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class LessThanLogicalExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new LessThanLogicalExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new LessThanLogicalExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>();
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var handler = new LessThanLogicalExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new LessThanLogicalExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            left.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var handler = new LessThanLogicalExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new LessThanLogicalExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(rightVisited);

            handler.Handle(expression, transformer);

            left.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedLeft()
        {
            var handler = new LessThanLogicalExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new LessThanLogicalExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Left.ShouldBeTheSameInstance(leftVisited);
        }

        [TestMethod]
        public void ShouldAssignVisitedRight()
        {
            var handler = new LessThanLogicalExpressionVisitor();
            var left = new Mock<ISqlValueExpression>();
            var right = new Mock<ISqlValueExpression>();
            var expression = new LessThanLogicalExpression(
                left.Object,
                right.Object);
            var transformer = new Mock<IVisitor>().Object;
            var leftVisited = new Mock<ISqlValueExpression>().Object;
            var rightVisited = new Mock<ISqlValueExpression>().Object;
            left.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(leftVisited);
            right.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(rightVisited);

            var result = handler.Handle(expression, transformer);

            result.Right.ShouldBeTheSameInstance(rightVisited);
        }
    }
}