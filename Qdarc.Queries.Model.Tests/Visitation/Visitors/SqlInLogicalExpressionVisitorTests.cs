using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class SqlInLogicalExpressionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpressionInSelectionMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var select = new Mock<ISelectCommand>();
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                select.Object);
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);
            select.Setup(x => x.Accept<ISelectCommand>(transformer.Object))
                .Returns(selectVisited);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldAssignVisitedValueInSelectionMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var select = new Mock<ISelectCommand>();
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                select.Object);
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);
            select.Setup(x => x.Accept<ISelectCommand>(transformer.Object))
                .Returns(selectVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Value.ShouldBeTheSameInstance(valueVisited);
        }

        [TestMethod]
        public void ShouldVisitValueInSelectionMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var select = new Mock<ISelectCommand>();
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                select.Object);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            select.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(selectVisited);

            handler.Handle(expression, transformer);

            value.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldCopyIsNegatedInSelectionMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var select = new Mock<ISelectCommand>();
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                select.Object);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            select.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(selectVisited);

            var result = handler.Handle(expression, transformer);

            result.IsNegated.ShouldBeEqual(expression.IsNegated);
        }

        [TestMethod]
        public void ShouldAssignVisitedSelectInSelectionMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var select = new Mock<ISelectCommand>();
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                select.Object);
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);
            select.Setup(x => x.Accept<ISelectCommand>(transformer.Object))
                .Returns(selectVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Selection.ShouldBeTheSameInstance(selectVisited);
        }

        [TestMethod]
        public void ShouldVisitSelectInSelectionMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var select = new Mock<ISelectCommand>();
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                select.Object);
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var selectVisited = new Mock<ISelectCommand>().Object;
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            select.Setup(x => x.Accept<ISelectCommand>(transformer))
                .Returns(selectVisited);

            handler.Handle(expression, transformer);

            select.Verify(x => x.Accept<ISelectCommand>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldCreateNewInstanceOfExpressionInSetMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var set = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                set.Select(x => x.Object));
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var setVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object
            };
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);
            set[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(setVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldAssignVisitedValueInSetMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var set = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                set.Select(x => x.Object));
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var setVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object
            };
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);
            set[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(setVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.Value.ShouldBeTheSameInstance(valueVisited);
        }

        [TestMethod]
        public void ShouldVisitValueInSetMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var set = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                set.Select(x => x.Object));
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var setVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object
            };
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            set[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(setVisited[0]);

            handler.Handle(expression, transformer);

            value.Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignVisitedSetInSetMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var set = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                set.Select(x => x.Object));
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var setVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);
            set[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(setVisited[0]);
            set[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(setVisited[1]);

            var result = handler.Handle(expression, transformer.Object);

            result.Set.ElementAt(0).ShouldBeTheSameInstance(setVisited[0]);
            result.Set.ElementAt(1).ShouldBeTheSameInstance(setVisited[1]);
        }

        [TestMethod]
        public void ShouldVisitSetInSetMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var set = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                set.Select(x => x.Object));
            var transformer = new Mock<IVisitor>().Object;
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var setVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(valueVisited);
            set[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(setVisited[0]);
            set[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(setVisited[1]);

            handler.Handle(expression, transformer);

            set[0].Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
            set[1].Verify(x => x.Accept<ISqlValueExpression>(transformer), Times.Once);
        }

        [TestMethod]
        public void ShouldCopyIsNegatedInSetMode()
        {
            var handler = new SqlInLogicalExpressionVisitor();
            var value = new Mock<ISqlValueExpression>();
            var set = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new SqlInLogicalExpression(
                value.Object,
                true,
                set.Select(x => x.Object));
            var transformer = new Mock<IVisitor>();
            var valueVisited = new Mock<ISqlValueExpression>().Object;
            var setVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object
            };
            value.Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(valueVisited);
            set[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(setVisited[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.IsNegated.ShouldBeEqual(expression.IsNegated);
        }
    }
}