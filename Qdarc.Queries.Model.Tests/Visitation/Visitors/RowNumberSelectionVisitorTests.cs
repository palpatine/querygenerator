using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class RowNumberSelectionVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new RowNumberSelectionVisitor();
            var order = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var expression = new RowNumberSelection(
                order);
            var transformer = new Mock<IVisitor>();
            var orderVisisted = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(orderVisisted[0]);

            var result = handler.Handle(expression, transformer.Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldVisitOrder()
        {
            var handler = new RowNumberSelectionVisitor();
            var order = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var expression = new RowNumberSelection(
                order);
            var transformer = new Mock<IVisitor>();
            var orderVisisted = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(orderVisisted[0]);

            handler.Handle(expression, transformer.Object);

            transformer.Verify(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]), Times.Once());
        }

        [TestMethod]
        public void ShouldAssignVisitedOrder()
        {
            var handler = new RowNumberSelectionVisitor();
            var order = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var expression = new RowNumberSelection(
                order);
            var transformer = new Mock<IVisitor>();
            var orderVisisted = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(orderVisisted[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.Order.ElementAt(0).ShouldBeTheSameInstance(orderVisisted.ElementAt(0));
        }

        [TestMethod]
        public void ShouldAssignVisitedPartition()
        {
            var handler = new RowNumberSelectionVisitor();
            var order = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var partition = new[] { new Mock<ISqlValueExpression>() };
            var expression = new RowNumberSelection(
                order,
                partition.Select(x => x.Object));
            var transformer = new Mock<IVisitor>();
            var orderVisisted = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var partitionVisited = new Mock<ISqlValueExpression>().Object;
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(orderVisisted[0]);
            partition[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(partitionVisited);

            var result = handler.Handle(expression, transformer.Object);

            result.Partition.ElementAt(0).ShouldBeTheSameInstance(partitionVisited);
        }

        [TestMethod]
        public void ShouldVisitPartition()
        {
            var handler = new RowNumberSelectionVisitor();
            var order = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var partition = new[] { new Mock<ISqlValueExpression>() };
            var expression = new RowNumberSelection(
                order,
                partition.Select(x => x.Object));
            var transformer = new Mock<IVisitor>();
            var orderVisisted = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var partitionVisited = new Mock<ISqlValueExpression>().Object;
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(orderVisisted[0]);
            partition[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer.Object))
                .Returns(partitionVisited);

            handler.Handle(expression, transformer.Object);

            partition[0]
                .Verify(x => x.Accept<ISqlValueExpression>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldCopySqlType()
        {
            var handler = new RowNumberSelectionVisitor();
            var order = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var expression = new RowNumberSelection(
                order);
            var transformer = new Mock<IVisitor>();
            var orderVisisted = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(orderVisisted[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.SqlType.ShouldBeTheSameInstance(expression.SqlType);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new RowNumberSelectionVisitor();
            var order = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            var expression = new RowNumberSelection(
                order);
            var transformer = new Mock<IVisitor>();
            var orderVisisted = new[] { new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, true) };
            transformer.Setup(x => x.Handle<OrderedValueExpression, OrderedValueExpression>(order[0]))
                .Returns(orderVisisted[0]);

            var result = handler.Handle(expression, transformer.Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }
    }
}