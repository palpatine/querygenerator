using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class TabularFunctionCallVisitorTests
    {
        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new TabularFunctionCallVisitor();
            var arguments = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new TabularFunctionCall(
                new ElementName("name"),
                arguments.Select(x => x.Object),
                typeof(int));
            var argumentsVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object
            };
            var transformer = new Mock<IVisitor>().Object;
            arguments[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(argumentsVisited[0]);
            var result = handler.Handle(expression, transformer);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new TabularFunctionCallVisitor();
            var arguments = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new TabularFunctionCall(
                new ElementName("name"),
                arguments.Select(x => x.Object),
                typeof(int));
            var argumentsVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object
            };
            var transformer = new Mock<IVisitor>().Object;
            arguments[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(argumentsVisited[0]);
            var result = handler.Handle(expression, transformer);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }

        [TestMethod]
        public void ShouldCopyName()
        {
            var handler = new TabularFunctionCallVisitor();
            var arguments = new[]
            {
                new Mock<ISqlValueExpression>()
            };
            var expression = new TabularFunctionCall(
                new ElementName("name"),
                arguments.Select(x => x.Object),
                typeof(int));
            var argumentsVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object
            };
            var transformer = new Mock<IVisitor>().Object;
            arguments[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(argumentsVisited[0]);
            var result = handler.Handle(expression, transformer);

            result.Name.ShouldBeEqual(expression.Name);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result.Name, expression.Name);
        }

        [TestMethod]
        public void ShouldVisitArguments()
        {
            var handler = new TabularFunctionCallVisitor();
            var arguments = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new TabularFunctionCall(
                new ElementName("name"),
                arguments.Select(x => x.Object),
                typeof(int));
            var argumentsVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            var transformer = new Mock<IVisitor>().Object;
            arguments[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(argumentsVisited[0]);
            arguments[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(argumentsVisited[1]);
            var result = handler.Handle(expression, transformer);

            result.Name.ShouldBeEqual(expression.Name);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(result.Name, expression.Name);
        }

        [TestMethod]
        public void ShouldAssignVisitedArguments()
        {
            var handler = new TabularFunctionCallVisitor();
            var arguments = new[]
            {
                new Mock<ISqlValueExpression>(),
                new Mock<ISqlValueExpression>()
            };
            var expression = new TabularFunctionCall(
                new ElementName("name"),
                arguments.Select(x => x.Object),
                typeof(int));
            var argumentsVisited = new[]
            {
                new Mock<ISqlValueExpression>().Object,
                new Mock<ISqlValueExpression>().Object
            };
            var transformer = new Mock<IVisitor>().Object;
            arguments[0]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(argumentsVisited[0]);
            arguments[1]
                .Setup(x => x.Accept<ISqlValueExpression>(transformer))
                .Returns(argumentsVisited[1]);
            var result = handler.Handle(expression, transformer);

            result.Arguments.ElementAt(0).ShouldBeTheSameInstance(argumentsVisited[0]);
            result.Arguments.ElementAt(1).ShouldBeTheSameInstance(argumentsVisited[1]);
        }
    }
}