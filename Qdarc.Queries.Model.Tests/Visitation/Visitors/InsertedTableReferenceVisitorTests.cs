using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;
using Qdarc.Queries.Model.Visitation.Visitors;

namespace Qdarc.Queries.Model.Tests.Visitation.Visitors
{
    [TestClass]
    public class InsertedTableReferenceVisitorTests
    {
        public static InsertedTableReference Handle(InsertedTableReference expression) => new InsertedTableReference(expression.ClrType);

        [TestMethod]
        public void ShouldCreateNewInstanceOfExpression()
        {
            var handler = new InsertedTableReferenceVisitor();
            var expression = new InsertedTableReference(
                typeof(int));

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreNotSame(expression, result);
        }

        [TestMethod]
        public void ShouldCopyClrType()
        {
            var handler = new InsertedTableReferenceVisitor();
            var expression = new InsertedTableReference(
                typeof(int));

            var result = handler.Handle(expression, new Mock<IVisitor>().Object);

            result.ClrType.ShouldBeTheSameInstance(expression.ClrType);
        }
    }
}