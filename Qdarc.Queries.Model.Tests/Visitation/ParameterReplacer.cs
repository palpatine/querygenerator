﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Tests.Visitation
{
    internal class ParameterReplacer
    {
        private readonly IVisitor _visitor;

        public ParameterReplacer(IVisitor visitor) => _visitor = visitor;

        public ISqlExpression Replace(ISqlExpression expression) => expression.Accept<ISqlExpression>(_visitor);
    }
}