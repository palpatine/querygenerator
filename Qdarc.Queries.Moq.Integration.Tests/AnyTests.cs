﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Moq.Integration.Tests.Model.Person;
using Unity;

namespace Qdarc.Queries.Moq.Integration.Tests
{
    [TestClass]
    public class AnyTests
    {
        [TestMethod]
        public void ShouldResolveTrueIfSourceContainsOneItem()
        {
            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var data = database.Query<AddressType>().Any();

                data.ShouldBeTrue();
            }
        }

        [TestMethod]
        public void ShouldResolveTrueIfSourceContainsManyItems()
        {
            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                },
                new AddressType
                {
                    AddressTypeId = 2,
                    ModifiedDate = DateTime.Now.AddDays(-2),
                    Name = "Work"
                },
                new AddressType
                {
                    AddressTypeId = 3,
                    ModifiedDate = DateTime.Now.AddDays(-3),
                    Name = "Mobile"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var data = database.Query<AddressType>().Any();

                data.ShouldBeTrue();
            }
        }

        [TestMethod]
        public void ShouldResolveFalseIfSourceContainsNoItems()
        {
            // test data
            var addressTypes = new AddressType[0];

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var data = database.Query<AddressType>().Any();

                data.ShouldBeFalse();
            }
        }
    }
}
