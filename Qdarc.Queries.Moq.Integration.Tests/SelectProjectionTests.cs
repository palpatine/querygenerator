﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Moq.Integration.Tests.Model.Person;
using Unity;

namespace Qdarc.Queries.Moq.Integration.Tests
{
    [TestClass]
    public class SelectProjectionTests
    {
        [TestMethod]
        public void SelectProjectionNewAnonymousObject()
        {
            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var data = database.Query<AddressType>().Select(x => new { x.Name, Id = x.AddressTypeId })
                    .ToList();

                data.Count.ShouldBeEqual(1);
                var item = data.Single();
                item.Id.ShouldBeEqual(addressTypes[0].AddressTypeId);
                item.Name.ShouldBeEqual(addressTypes[0].Name);
            }
        }

        [TestMethod]
        public void SelectProjectionSingleColumn()
        {
            var addressTypes = new[]
             {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;
                var query = database.Query<AddressType>().Select(x => x.Name);
                var data = query.ToList();

                data.Count.ShouldBeEqual(1);
                CollectionAssert.AreEquivalent(
                    data,
                    new[]
                    {
                    "Home"
                    });
            }
        }

        [TestMethod]
        public void SelectProjectionSingleColumnWithInt32ToNullableInt32Conversion()
        {
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;
                var query = database.Query<AddressType>().Select(x => (int?)x.AddressTypeId);
                var data = query.ToList();

                data.Count.ShouldBeEqual(1);
                CollectionAssert.AreEquivalent(
                    data,
                    new[]
                    {
                    1
                    });
            }
        }
    }
}
