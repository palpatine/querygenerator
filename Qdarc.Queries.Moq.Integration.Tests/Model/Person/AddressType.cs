using System;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Moq.Integration.Tests.Model.Person
{
    [SqlTableName("AddressType", "Person")]
    internal class AddressType
    {
        private int _addressTypeId;

        private DateTime _modifiedDate;

        private string _name;

        private Guid _rowguid;

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int AddressTypeId
        {
            get => _addressTypeId;

            set
            {
                if (_addressTypeId == value)
                {
                    return;
                }

                _addressTypeId = value;
                Changed = true;
            }
        }

        [SqlIgnore]
        public bool Changed { get; set; }

        [TsqlType(TsqlNativeTypesNames.DateTime, false)]
        public DateTime ModifiedDate
        {
            get => _modifiedDate;

            set
            {
                if (_modifiedDate == value)
                {
                    return;
                }

                _modifiedDate = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.VarChar, false, 50, TypeAliasName = "Name")]
        public string Name
        {
            get => _name;

            set
            {
                if (_name == value)
                {
                    return;
                }

                _name = value;
                Changed = true;
            }
        }

        [TsqlType(TsqlNativeTypesNames.UniqueIdentifier, false)]
        public Guid Rowguid
        {
            get => _rowguid;

            set
            {
                if (_rowguid == value)
                {
                    return;
                }

                _rowguid = value;
                Changed = true;
            }
        }
    }
}