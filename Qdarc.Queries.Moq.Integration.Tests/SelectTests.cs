﻿using System;
using System.Linq;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Moq.Integration.Tests.Model.Person;
using Unity;

namespace Qdarc.Queries.Moq.Integration.Tests
{
    [TestClass]
    public class SelectTests
    {
        [TestMethod]
        public void ShouldMockTableSource()
        {
            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;
                var query = database.Query<AddressType>();
                var data = query.ToList();

                data.Count.ShouldBeEqual(1);
                var item = data.Single();
                item.AddressTypeId.ShouldBeEqual(addressTypes[0].AddressTypeId);
                item.ModifiedDate.ShouldBeEqual(addressTypes[0].ModifiedDate);
                item.Name.ShouldBeEqual(addressTypes[0].Name);
            }
        }
    }
}
