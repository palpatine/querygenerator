﻿using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Linq.Tsql.Conventions;
using Qdarc.Queries.Model;

namespace Qdarc.Queries.Moq.Integration.Tests
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IServiceCollection registrar)
        {
            registrar.AddTransient<IModelToSqlConvention, AttributesAnnotationModelToSqlConvention>();
        }
    }
}