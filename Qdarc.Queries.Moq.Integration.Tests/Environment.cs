﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities.Ioc.Unity;
using Unity;
using Unity.Injection;

namespace Qdarc.Queries.Moq.Integration.Tests
{
    [TestClass]
    public static class Environment
    {
        public static UnityContainer Container { get; private set; }

        [AssemblyCleanup]
        public static void Cleanup()
        {
        }

        [AssemblyInitialize]
        public static void Create(TestContext context)
        {
            Container = new UnityContainer();
            var registrar = new ServiceCollection();
            Container.RegisterType(
                typeof(IEnumerable<>),
                new InjectionFactory((container, type, name) =>
                {
                    var innerTyp = type.GenericTypeArguments[0];
                    var array = innerTyp.MakeArrayType();
                    return container.Resolve(array);
                }));

            Bootstrapper.Bootstrap(registrar);
            Queries.Model.Bootstrapper.Bootstrap(registrar);
            Linq.Bootstrapper.Bootstrap(registrar);
            Linq.Tsql.Bootstrapper.Bootstrap(registrar);
            Moq.Bootstrapper.Bootstrap(registrar);
            Populate(registrar);
        }

        public static void Populate(IServiceCollection services)
        {
            foreach (var descriptor in services.Where(x => x.ImplementationType == null))
            {
                if (descriptor.ImplementationFactory != null)
                {
                    Container.RegisterType(
                        descriptor.ServiceType,
                        new InjectionFactory((context, type, n) =>
                        {
                            var serviceProvider = context.Resolve<IServiceProvider>();
                            return descriptor.ImplementationFactory(serviceProvider);
                        }));
                }
                else
                {
                    Container
                        .RegisterInstance(descriptor.ServiceType, descriptor.ImplementationInstance);
                }
            }

            var transient = services.Where(x => x.ImplementationType != null)
                .GroupBy(x => x.ServiceType);

            foreach (var set in transient)
            {
                if (set.Count() == 1)
                {
                    var descriptor = set.Single();
                    Container.RegisterType(descriptor.ServiceType, descriptor.ImplementationType);
                    Container.RegisterType(
                        descriptor.ServiceType,
                        descriptor.ImplementationType,
                        descriptor.ImplementationType.FullName);
                }
                else
                {
                    foreach (var descriptor in set)
                    {
                        Container.RegisterType(
                            descriptor.ServiceType,
                            descriptor.ImplementationType,
                            descriptor.ImplementationType.FullName);
                    }

                    var defaultImplementation = set.Last();
                    Container.RegisterType(defaultImplementation.ServiceType, defaultImplementation.ImplementationType);
                }
            }
        }
    }
}