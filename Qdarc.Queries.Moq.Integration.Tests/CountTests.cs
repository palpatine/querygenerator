﻿using System;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Moq.Integration.Tests.Model.Person;
using Unity;

namespace Qdarc.Queries.Moq.Integration.Tests
{
    [TestClass]
    public class CountTests
    {
        [TestMethod]
        public void CountStar()
        {
            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var query = database.Query<AddressType>().Count();
                query.ShouldBeEqual(1);
            }
        }

        [TestMethod]
        public void CountStarInSingleItemProjection()
        {
            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var query = database.Query<AddressType>().Select(x => database.Count());
                query.Single().ShouldBeEqual(1);
            }
        }

        [TestMethod]
        public void CountStarInProjection()
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.Inconclusive("not implemented");

            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    AddressTypeId = 1,
                    ModifiedDate = DateTime.Now.AddDays(-1),
                    Name = "Home"
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var query = database.Query<AddressType>().Select(x => new { Count = database.Count() });
                query.Single().Count.ShouldBeEqual(1);
            }
        }

        [TestMethod]
        public void CountByProperty()
        {
            // test data
            var addressTypes = new[]
            {
                new AddressType
                {
                    Name = "Home"
                },
                new AddressType
                {
                    Name = null
                }
            };

            // setup code
            using (var mockDatabase = Environment.Container.Resolve<IMockDatabase>())
            {
                mockDatabase.Setup(addressTypes);

                // tested code
                var database = mockDatabase.Database;

                var query = database.Query<AddressType>().Count(x => x.Name);
                query.ShouldBeEqual(1);
            }
        }
    }
}
