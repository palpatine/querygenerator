﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Formatter.Tsql
{
    internal class FormattingHelper
    {
        private const string ElementNameReqularExpression =
             @"^(?:(?:(?<name>[^\[\].@#]+)|(?:\[(?<name>.*)\]))\.)*(?:(?<name>[^\[\].@#]+)|(?:\[(?<name>.*)\]))$";

        public static string FormatElementName(string name)
        {
            return $"[{name}]";
        }

        public static ElementName GetElementName(string typeName)
        {
            var result = Regex.Match(typeName, ElementNameReqularExpression);
            if (!result.Success)
            {
                throw new InvalidOperationException($"Type name does not conform to TSQL identifier standard. {typeName}");
            }

            return new ElementName(result.Groups["name"].Captures.Cast<Capture>().Select(x => x.Value));
        }

        internal static string FormatElementName(ElementName name)
        {
            var result = new StringBuilder();

            foreach (var part in name)
            {
                result.AppendFormat("[{0}].", part);
            }

            result.Length--;

            return result.ToString();
        }
    }
}