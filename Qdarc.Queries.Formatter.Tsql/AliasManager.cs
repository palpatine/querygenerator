using System.Collections.Generic;
using Qdarc.Queries.Model.Linq.References;

namespace Qdarc.Queries.Formatter.Tsql
{
    public sealed class AliasManager : IAliasManager
    {
        private const string ParameterAliasBase = "@param";
        private const string QueryAliasBase = "q";

        private readonly Dictionary<string, int> _aliasCounters = new Dictionary<string, int>();

        public string GetAlias()
        {
            return GetAlias(QueryAliasBase);
        }

        public string GetAlias(IParameter query)
        {
            if (query.Name == null)
            {
                query.Name = GetAlias(ParameterAliasBase);
            }

            return query.Name;
        }

        private string GetAlias(string name)
        {
            return name + GetAliasId(name);
        }

        private int GetAliasId(string name)
        {
            if (!_aliasCounters.ContainsKey(name))
            {
                _aliasCounters.Add(name, 0);
            }

            var aliasId = _aliasCounters[name] + 1;
            _aliasCounters[name] = aliasId;
            return aliasId;
        }
    }
}