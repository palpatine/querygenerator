using System.Collections.Generic;
using System.Text;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class SqlSelectionTsqlFormatter : FormattingHandler<SqlSelection>
    {
        public override FormattedQuery Handle(SqlSelection expression, ITransformer transformer)
        {
            var select = new StringBuilder();
            var parameters = new List<IParameter>();

            foreach (var selectionItem in expression.Elements)
            {
                var formattedSelectionItem = selectionItem.Accept<FormattedQuery>(transformer);
                select.AppendFormat("{0},", formattedSelectionItem.Query);
                parameters.AddRange(formattedSelectionItem.Parameters);
            }

            select.Length--;

            return new FormattedQuery
            {
                Parameters = parameters,
                Query = select.ToString()
            };
        }
    }
}