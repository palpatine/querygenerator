﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class LinqColumnChainReferenceTsqlFormatter : FormattingHandler<LinqColumnChainReference>
    {
        public override FormattedQuery Handle(LinqColumnChainReference expression, ITransformer transformer)
        {
            var columnName = Context.ModelToSqlConvention.GetColumnName(
                expression.Source,
                expression.Properties);
            return new FormattedQuery
            {
                Query = FormattingHelper.FormatElementName(columnName),
            };
        }
    }
}