using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class NotGreaterThanLogicalExpressionTsqlFormatter : FormattingHandler<NotGreaterThanLogicalExpression>
    {
        public override FormattedQuery Handle(NotGreaterThanLogicalExpression expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);

            var result = new FormattedQuery
            {
                Parameters = left.Parameters.Concat(right.Parameters),
                Query = $"{left.Query} !> {right.Query}"
            };

            return result;
        }
    }
}