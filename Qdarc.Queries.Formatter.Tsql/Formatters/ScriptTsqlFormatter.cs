﻿using System;
using System.Linq;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ScriptTsqlFormatter : FormattingHandler<SqlScript>
    {
        public override FormattedQuery Handle(SqlScript expression, ITransformer transformer)
        {
            var commands = expression.Commands.Select(x => x.Accept<FormattedQuery>(transformer)).ToArray();

            return new FormattedQuery
            {
                Parameters = commands.SelectMany(x => x.Parameters).ToArray(),
                Query = string.Join(";" + Environment.NewLine, commands.Select(x => x.Query)) + ";"
            };
        }
    }
}
