using System.Linq;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class AliasedSelectionItemTsqlFormatter : FormattingHandler<AliasedSelectionItem>
    {
        public override FormattedQuery Handle(AliasedSelectionItem expression, ITransformer transformer)
        {
            expression.Alias = expression.Alias ?? Context.AliasManager.GetAlias();
            var prefixChain = Context
                                .CurrentSelectionProcessingContext
                                .AliasPrefixes
                                .Concat(new[] { expression.Alias });
            expression.ResolvedAlias = string.Join(
                Context.ModelToSqlConvention.CompoundColumnSeparator,
                prefixChain);
            var itemQuery = expression.Item.Accept<FormattedQuery>(transformer);

            return new FormattedQuery
            {
                Query = $"{itemQuery.Query} AS {FormattingHelper.FormatElementName(expression.ResolvedAlias)}",
                Parameters = itemQuery.Parameters
            };
        }
    }
}