﻿using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class SqlColumnReferenceTsqlFormatter : FormattingHandler<SqlColumnReference>
    {
        public override FormattedQuery Handle(SqlColumnReference expression, ITransformer transformer)
        {
            return new FormattedQuery
            {
                Query = FormattingHelper.FormatElementName(expression.ColumnName),
            };
        }
    }
}