﻿using System.Linq;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class BoundSelectionItemTsqlFormatter : FormattingHandler<BoundSelectionItem>
    {
        public override FormattedQuery Handle(BoundSelectionItem expression, ITransformer transformer)
        {
            expression.Alias = expression.Alias ?? Context.AliasManager.GetAlias();
            var prefixChain = Context
                .CurrentSelectionProcessingContext
                .AliasPrefixes
                .Concat(new[] { expression.Alias });
            expression.ResolvedAlias = string.Join(".", prefixChain);
            var itemQuery = expression.Item.Accept<FormattedQuery>(transformer);

            return new FormattedQuery
            {
                Query = $"{itemQuery.Query} AS {FormattingHelper.FormatElementName(expression.ResolvedAlias)}",
                Parameters = itemQuery.Parameters
            };
        }
    }
}
