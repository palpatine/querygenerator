﻿using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal sealed class LinqTableParameterReferenceFormatter : FormattingHandler<LinqTableParameterReference>
    {
        public override FormattedQuery Handle(LinqTableParameterReference expression, ITransformer transformer)
        {
            var parameterName = Context.AliasManager.GetAlias(expression);
            var type = (CustomTypeReference)expression.SqlType;

            return new FormattedQuery
            {
                Query = parameterName,
                Parameters = new[]
                {
                    new LinqTableParameterReference(
                        expression.Expression,
                        expression.ClrType,
                        new TypeReference(FormattingHelper.FormatElementName(type.Name)),
                        expression.Name)
                }
            };
        }
    }
}
