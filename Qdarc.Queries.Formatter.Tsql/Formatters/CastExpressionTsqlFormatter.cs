﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class CastExpressionTsqlFormatter : FormattingHandler<SqlCastExpression>
    {
        public override FormattedQuery Handle(SqlCastExpression expression, ITransformer transformer)
        {
            var select = expression.Expression.Accept<FormattedQuery>(transformer);
            var result = new FormattedQuery
            {
                Query = $"CAST({select.Query} AS {expression.SqlType})",
                Parameters = select.Parameters
            };

            return result;
        }
    }
}