﻿using System;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ConstantExpressionTsqlFormatter : FormattingHandler<SqlConstantExpression>
    {
        public override FormattedQuery Handle(SqlConstantExpression expression, ITransformer transformer)
        {
            if (expression.Value == null)
            {
                throw new InvalidOperationException(
                    $"Null cannot be passed as constant expression use {typeof(SqlNullExpression).Name}");
            }

            var type = (TsqlType)expression.SqlType;

            string text;

            if (type.Name == TsqlNativeTypesNames.VarChar)
            {
                text = $"'{expression.Value.ToString().Replace("'", "''")}'";
            }
            else if (type.Name == TsqlNativeTypesNames.NVarChar)
            {
                text = $"N'{expression.Value.ToString().Replace("'", "''")}'";
            }
            else if (type.Name == TsqlNativeTypesNames.Char)
            {
                text = $"'{expression.Value}'";
            }
            else if (type.Name == TsqlNativeTypesNames.Int)
            {
                text = $"{expression.Value}";
            }
            else if (type.Name == TsqlNativeTypesNames.Bit)
            {
                text = $"CAST({Convert.ToInt32(expression.Value)} AS BIT)";
            }
            else
            {
                throw new NotSupportedException();
            }

            return new FormattedQuery
            {
                Query = text
            };
        }
    }
}