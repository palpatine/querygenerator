﻿using System.Collections.Generic;
using System.Text;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class UpdateCommandFormatter : FormattingHandler<UpdateCommand>
    {
        public override FormattedQuery Handle(UpdateCommand expression, ITransformer transformer)
        {
            var parameters = new List<IParameter>();
            var source = expression.Source?.Accept<FormattedQuery>(transformer);

            var builder = new StringBuilder("UPDATE");
            if (expression.Target is AliasedSourceItem alias)
            {
                builder.AppendFormat(" {0} SET", alias.Alias);
            }
            else
            {
                var target = expression.Target.Accept<FormattedQuery>(transformer);
                builder.AppendFormat(" {0} SET", target.Query);
                parameters.AddRange(target.Parameters);
            }

            foreach (var association in expression.Set)
            {
                var value = association.Value.Accept<FormattedQuery>(transformer);
                builder.AppendFormat(
                    " {0} = {1},",
                    FormattingHelper.FormatElementName(association.ElementName),
                    value.Query);
                parameters.AddRange(value.Parameters);
            }

            builder.Length--;

            if (source != null)
            {
                builder.AppendFormat(" FROM {0}", source.Query);
                parameters.AddRange(source.Parameters);
            }

            if (expression.Filter != null)
            {
                var filter = expression.Filter.Accept<FormattedQuery>(transformer);
                builder.AppendFormat(" WHERE {0}", filter.Query);
                parameters.AddRange(filter.Parameters);
            }

            return new FormattedQuery
            {
                Query = builder.ToString(),
                Parameters = parameters
            };
        }
    }
}