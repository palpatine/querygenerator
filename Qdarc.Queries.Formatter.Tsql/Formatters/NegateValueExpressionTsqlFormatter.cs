﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class NegateValueExpressionTsqlFormatter : FormattingHandler<NegateValueExpression>
    {
        public override FormattedQuery Handle(NegateValueExpression expression, ITransformer transformer)
        {
            var inner = expression.ValueExpression.Accept<FormattedQuery>(transformer);
            return new FormattedQuery
            {
                Query = $"~({inner.Query})",
                Parameters = inner.Parameters
            };
        }
    }
}