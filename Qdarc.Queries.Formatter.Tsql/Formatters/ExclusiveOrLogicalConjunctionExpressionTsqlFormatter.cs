﻿using System;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ExclusiveOrLogicalConjunctionExpressionTsqlFormatter : FormattingHandler<ExclusiveOrLogicalConjunctionExpression>
    {
        public override FormattedQuery Handle(ExclusiveOrLogicalConjunctionExpression expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);
            var result = new FormattedQuery
            {
                Parameters = left.Parameters.Concat(right.Parameters),
                Query = $"IIF({left.Query}, 1, 0) ^ IIF({right.Query}, 1, 0) = 1"
            };

            return result;
        }
    }
}