using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class PrefixedSelectionTsqlFormatter : FormattingHandler<PrefixedSelection>
    {
        public override FormattedQuery Handle(PrefixedSelection expression, ITransformer transformer)
        {
            using (Context.CurrentSelectionProcessingContext.AppendPrefix(expression.Prefix))
            {
                return expression.Selection.Accept<FormattedQuery>(transformer);
            }
        }
    }
}