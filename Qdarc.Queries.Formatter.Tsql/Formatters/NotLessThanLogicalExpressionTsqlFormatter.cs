﻿using System;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class NotLessThanLogicalExpressionTsqlFormatter : FormattingHandler<NotLessThanLogicalExpression>
    {
        public override FormattedQuery Handle(NotLessThanLogicalExpression expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);

            var result = new FormattedQuery
            {
                Parameters = left.Parameters.Concat(right.Parameters),
                Query = $"{left.Query} !< {right.Query}"
            };

            return result;
        }
    }
}
