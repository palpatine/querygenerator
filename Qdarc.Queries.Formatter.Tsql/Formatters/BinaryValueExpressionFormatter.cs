﻿using System;
using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class BinaryValueExpressionFormatter : FormattingHandler<SqlBinaryValueExpression>
    {
        public override FormattedQuery Handle(SqlBinaryValueExpression expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);
            var @operator = GetOperator(expression.GetType());
            return new FormattedQuery
            {
                Query = $"{left.Query} {@operator} {right.Query}",
                Parameters = left.Parameters.Concat(right.Parameters)
            };
        }

        private static string GetOperator(Type type)
        {
            if (type == typeof(AddValueExpression))
            {
                return "+";
            }
            else if (type == typeof(SubtractValueExpression))
            {
                return "-";
            }
            else if (type == typeof(BitwiseAndValueExpression))
            {
                return "&";
            }
            else if (type == typeof(BitwiseExclusiveOrValueExpression))
            {
                return "^";
            }
            else if (type == typeof(BitwiseOrValueExpression))
            {
                return "|";
            }
            else if (type == typeof(DivideValueExpression))
            {
                return "/";
            }
            else if (type == typeof(ModuloValueExpression))
            {
                return "%";
            }
            else if (type == typeof(MultiplyValueExpression))
            {
                return "*";
            }

            throw new NotImplementedException();
        }
    }
}