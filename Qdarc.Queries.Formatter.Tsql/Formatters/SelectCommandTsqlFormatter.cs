﻿using System.Collections.Generic;
using System.Text;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class SelectCommandTsqlFormatter : FormattingHandler<SelectCommand>
    {
        public override FormattedQuery Handle(SelectCommand expression, ITransformer transformer)
        {
            var select = new StringBuilder("SELECT ");
            var parameters = new List<IParameter>();

            if (expression.Top != null)
            {
                var topExpression = expression.Top.Value.Accept<FormattedQuery>(transformer);
                select.AppendFormat("TOP ({0}) ", topExpression.Query);
                parameters.AddRange(topExpression.Parameters);

                if (expression.Top.IsPercent)
                {
                    select.Append("PERCENT ");
                }

                if (expression.Top.WithTies)
                {
                    select.Append("WITH TIES ");
                }
            }

            FormattedQuery source = null;
            if (expression.Source != null)
            {
                source = expression.Source.Accept<FormattedQuery>(transformer);
                parameters.AddRange(source.Parameters);
            }

            using (Context.OpenSelectionProcessingContext())
            {
                var selection = expression.Selection.Accept<FormattedQuery>(transformer);

                select.Append(selection.Query);
                parameters.AddRange(selection.Parameters);
            }

            if (source != null)
            {
                select.AppendFormat(" FROM {0}", source.Query);
            }

            if (expression.Filter != null)
            {
                var filter = expression.Filter.Accept<FormattedQuery>(transformer);
                select.AppendFormat(" WHERE ({0})", filter.Query);
                parameters.AddRange(filter.Parameters);
            }

            var result = new FormattedQuery
            {
                Query = select.ToString(),
                Parameters = parameters
            };

            return result;
        }
    }
}