﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class DeleteCommandFormatter : FormattingHandler<DeleteCommand>
    {
        public override FormattedQuery Handle(DeleteCommand expression, ITransformer transformer)
        {
            var parameters = new List<IParameter>();
            string query;

            if (expression.Source == null)
            {
                var target = expression.Target.Accept<FormattedQuery>(transformer);
                parameters.AddRange(target.Parameters);
                query = $"DELETE FROM {target.Query}";
            }
            else
            {
                var source = expression.Source.Accept<FormattedQuery>(transformer);
                var target = (AliasedSourceItem)expression.Target;
                query = $"DELETE {target.Alias} FROM {source.Query}";
                parameters.AddRange(source.Parameters);
            }

            if (expression.Filter != null)
            {
                var filter = expression.Filter.Accept<FormattedQuery>(transformer);
                query += $" WHERE ({filter.Query})";
                parameters.AddRange(filter.Parameters);
            }

            return new FormattedQuery
            {
                Query = query,
                Parameters = parameters
            };
        }
    }
}
