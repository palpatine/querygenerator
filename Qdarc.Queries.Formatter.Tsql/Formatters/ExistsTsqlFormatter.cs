﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ExistsTsqlFormatter : FormattingHandler<ExistsExpression>
    {
        public override FormattedQuery Handle(ExistsExpression expression, ITransformer transformer)
        {
            var select = expression.Select.Accept<FormattedQuery>(transformer);

            var result = new FormattedQuery
            {
                Parameters = select.Parameters
            };

            if (expression.IsNegated)
            {
                result.Query = $"NOT EXISTS({select.Query})";
            }
            else
            {
                result.Query = $"EXISTS({select.Query})";
            }

            return result;
        }
    }
}