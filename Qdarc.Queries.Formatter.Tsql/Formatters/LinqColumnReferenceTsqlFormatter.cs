using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class LinqColumnReferenceTsqlFormatter : FormattingHandler<LinqColumnReference>
    {
        public override FormattedQuery Handle(LinqColumnReference expression, ITransformer transformer)
        {
            var columnName = Context.ModelToSqlConvention.GetColumnName(
                expression.Source,
                expression.Property);
            return new FormattedQuery
            {
                Query = FormattingHelper.FormatElementName(columnName),
            };
        }
    }
}