﻿using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ValuesSourceTsqlFormatter : FormattingHandler<ValuesSource>
    {
        public override FormattedQuery Handle(ValuesSource expression, ITransformer transformer)
        {
            var value = expression.Rows
                .Select(x => x.Values.Select(z => z.Accept<FormattedQuery>(transformer)).ToArray())
                .ToArray();

            var formattedValues = string.Join("), (", value.Select(x => string.Join(", ", x.Select(z => z.Query))));

            return new FormattedQuery
            {
                Query = $"VALUES ({formattedValues})"
            };
        }
    }
}