﻿using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class AliasedSourceItemTsqlFormatter : FormattingHandler<AliasedSourceItem>
    {
        public override FormattedQuery Handle(AliasedSourceItem expression, ITransformer transformer)
        {
            expression.Alias = expression.Alias ?? Context.AliasManager.GetAlias();
            var itemQuery = expression.Source.Accept<FormattedQuery>(transformer);
            var columnsList = FormatColumnsList(expression);
            if (expression.Source is ISelectCommand
                || expression.Source is IValuesSource)
            {
                return new FormattedQuery
                {
                    Query = $"({itemQuery.Query}) AS {FormattingHelper.FormatElementName(expression.Alias)}{columnsList}",
                    Parameters = itemQuery.Parameters
                };
            }

            return new FormattedQuery
            {
                Query = $"{itemQuery.Query} AS {FormattingHelper.FormatElementName(expression.Alias)}{columnsList}",
                Parameters = itemQuery.Parameters
            };
        }

        private static string FormatColumnsList(AliasedSourceItem query)
        {
            if (query.ColumnAliases != null)
            {
                return $"({string.Join(", ", query.ColumnAliases.Select(x => x.Alias).ToArray())})";
            }

            return string.Empty;
        }
    }
}