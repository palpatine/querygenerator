﻿using System.Linq;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class InsertCommandFormatter : FormattingHandler<InsertCommand>
    {
        public override FormattedQuery Handle(InsertCommand expression, ITransformer transformer)
        {
            var values = expression.Values.Accept<FormattedQuery>(transformer);
            var tableName = expression.TableReference.Accept<FormattedQuery>(transformer);
            var columns = Context.ModelToSqlConvention.GetInsertableColumns(expression.ClrType);
            var formatedColumns = string.Join(
                ", ",
                columns.Select(x => Context.ModelToSqlConvention.GetColumnName(x.Property)));
            return new FormattedQuery
            {
                Query = $"INSERT INTO {tableName.Query} ({formatedColumns}) {values.Query}"
            };
        }
    }
}
