﻿using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class InlineIfTsqlFormatter : FormattingHandler<SqlInlineIfExpression>
    {
        public override FormattedQuery Handle(SqlInlineIfExpression expression, ITransformer transformer)
        {
            var condition = expression.Condition.Accept<FormattedQuery>(transformer);
            var ifTrue = expression.IfTrue.Accept<FormattedQuery>(transformer);
            var ifFalse = expression.IfFalse.Accept<FormattedQuery>(transformer);
            var result = new FormattedQuery
            {
                Query = $"IIF({condition.Query}, {ifTrue.Query}, {ifFalse.Query})",
                Parameters = condition.Parameters.Concat(ifTrue.Parameters).Concat(ifFalse.Parameters)
            };

            return result;
        }
    }
}