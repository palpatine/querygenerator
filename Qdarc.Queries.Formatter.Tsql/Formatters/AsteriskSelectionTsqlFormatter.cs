﻿using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class AsteriskSelectionTsqlFormatter : FormattingHandler<AsteriskSelection>
    {
        public override FormattedQuery Handle(AsteriskSelection expression, ITransformer transformer)
        {
            if (expression.Source != null)
            {
                return new FormattedQuery
                {
                    Query = $"{FormattingHelper.FormatElementName(expression.Source.Name)}.*"
                };
            }

            return new FormattedQuery
            {
                Query = "*"
            };
        }
    }
}