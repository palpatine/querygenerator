﻿using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class CountTsqlFormatter : FormattingHandler<SqlCountExpression>
    {
        public override FormattedQuery Handle(SqlCountExpression expression, ITransformer transformer)
        {
            var innerQuery = expression.Value.Accept<FormattedQuery>(transformer);

            return new FormattedQuery
            {
                Query = $"COUNT({(expression.IsDistinct ? "DISTINCT " : string.Empty)}{innerQuery.Query})"
            };
        }
    }
}