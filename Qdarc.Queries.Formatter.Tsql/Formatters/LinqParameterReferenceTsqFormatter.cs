﻿using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class LinqParameterReferenceTsqFormatter : FormattingHandler<LinqParameterReference>
    {
        public override FormattedQuery Handle(LinqParameterReference expression, ITransformer transformer)
        {
            var parameterName = Context.AliasManager.GetAlias(expression);
            return new FormattedQuery
            {
                Query = parameterName,
                Parameters = new[] { expression }
            };
        }
    }
}