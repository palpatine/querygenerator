﻿using System.Linq;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ProcedureCallTsqlFormatter : FormattingHandler<ProcedureCall>
    {
        public override FormattedQuery Handle(ProcedureCall expression, ITransformer transformer)
        {
            var name = FormattingHelper.FormatElementName(expression.Name);
            if (expression.Arguments != null && expression.Arguments.Any())
            {
                var arguments = expression.Arguments
               .Select(x => new
               {
                   IsDirectParameter = x is IParameter,
                   Formatted = x.Accept<FormattedQuery>(transformer),
                   SqlType = x.SqlType
               })
               .ToArray();

                var parametersDeclaration = arguments
                     .Select((x, i) => new { Argument = x, Id = i })
                     .Where(x => !x.Argument.IsDirectParameter)
                     .Aggregate(
                         string.Empty,
                         (x, y) =>
                                 $"{x}, @arg{y.Id} {FormatTypeName(y.Argument.SqlType)} = ({y.Argument.Formatted.Query})",
                         (value) => value.Length == 0 ? string.Empty : $"DECLARE{value.Substring(1)};");

                var parametersList = string.Join(
                    ", ",
                    arguments.Select((x, i) => x.IsDirectParameter ? x.Formatted.Query : $"@arg{i}"));

                return new FormattedQuery
                {
                    Query = $"{parametersDeclaration} EXEC {name} {parametersList}",
                    Parameters = arguments.SelectMany(x => x.Formatted.Parameters).ToArray()
                };
            }
            else
            {
                return new FormattedQuery
                {
                    Query = $"EXEC {name}"
                };
            }
        }

        private static string FormatTypeName(ISqlType sqlType)
        {
            if (sqlType is CustomTypeReference customTypeReference)
            {
                return FormattingHelper.FormatElementName(customTypeReference.Name);
            }

            if (sqlType is TypeReference typeReference)
            {
                return $"[{typeReference.Name}]";
            }

            return sqlType.ToString();
        }
    }
}