using System.Collections.Generic;
using System.Text;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ProjectionByConstructorTsqlFormatter : FormattingHandler<ProjectionByConstructor>
    {
        public override FormattedQuery Handle(ProjectionByConstructor expression, ITransformer transformer)
        {
            var queryText = new StringBuilder();
            var parameters = new List<IParameter>();

            foreach (var selectionItem in expression.Elements)
            {
                var formated = selectionItem.Accept<FormattedQuery>(transformer);
                queryText.Append(formated.Query);
                queryText.Append(",");
                parameters.AddRange(formated.Parameters);
            }

            queryText.Length--;

            return new FormattedQuery
            {
                Query = queryText.ToString(),
                Parameters = parameters
            };
        }
    }
}