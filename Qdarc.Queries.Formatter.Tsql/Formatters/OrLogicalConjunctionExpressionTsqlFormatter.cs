using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class OrLogicalConjunctionExpressionTsqlFormatter : FormattingHandler<OrLogicalConjunctionExpression>
    {
        public override FormattedQuery Handle(OrLogicalConjunctionExpression expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);

            var result = new FormattedQuery
            {
                Parameters = left.Parameters.Concat(right.Parameters),
                Query = $"({left.Query}) OR ({right.Query})"
            };

            return result;
        }
    }
}