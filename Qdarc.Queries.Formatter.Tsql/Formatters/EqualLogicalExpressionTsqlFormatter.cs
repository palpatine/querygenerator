using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class EqualLogicalExpressionTsqlFormatter : FormattingHandler<EqualLogicalExpression>
    {
        public override FormattedQuery Handle(EqualLogicalExpression expression, ITransformer transformer)
        {
            var isLeftNull = expression.Left is SqlNullExpression;
            var isRightNull = expression.Right is SqlNullExpression;

            if (isLeftNull && isRightNull)
            {
                return new FormattedQuery
                {
                    Query = "NULL IS NULL"
                };
            }

            if (isLeftNull)
            {
                var right = expression.Right.Accept<FormattedQuery>(transformer);
                var result = new FormattedQuery
                {
                    Parameters = right.Parameters,
                    Query = $"{right.Query} IS NULL"
                };
                return result;
            }
            else if (isRightNull)
            {
                var left = expression.Left.Accept<FormattedQuery>(transformer);
                var result = new FormattedQuery
                {
                    Parameters = left.Parameters,
                    Query = $"{left.Query} IS NULL"
                };
                return result;
            }
            else
            {
                var left = expression.Left.Accept<FormattedQuery>(transformer);
                var right = expression.Right.Accept<FormattedQuery>(transformer);
                var result = new FormattedQuery
                {
                    Parameters = left.Parameters.Concat(right.Parameters),
                    Query = $"{left.Query} = {right.Query}"
                };
                return result;
            }
        }
    }
}