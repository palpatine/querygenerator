﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class NegateLogicalExpressionTsqlFormatter : FormattingHandler<NegateLogicalExpression>
    {
        public override FormattedQuery Handle(NegateLogicalExpression expression, ITransformer transformer)
        {
            var inner = expression.LogicalExpression.Accept<FormattedQuery>(transformer);
            return new FormattedQuery
            {
                Query = $"NOT ({inner.Query})",
                Parameters = inner.Parameters
            };
        }
    }
}
