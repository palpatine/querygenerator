﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class CurrentDateAndTimeTsqlFormatter : FormattingHandler<CurrentDateAndTimeExpression>
    {
        public override FormattedQuery Handle(CurrentDateAndTimeExpression expression, ITransformer transformer)
        {
            return new FormattedQuery
            {
                Query = expression.IsUtc ? "GETUTCDATE()" : "GETDATE()"
            };
        }
    }
}
