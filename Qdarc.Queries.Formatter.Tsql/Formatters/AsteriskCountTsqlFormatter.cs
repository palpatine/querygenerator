﻿using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class AsteriskCountTsqlFormatter : FormattingHandler<SqlAsteriskCountExpression>
    {
        public override FormattedQuery Handle(SqlAsteriskCountExpression expression, ITransformer transformer)
        {
            return new FormattedQuery
            {
                Query = $"COUNT(*)"
            };
        }
    }
}