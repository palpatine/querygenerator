﻿using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class SelectCommandAsSelectionItemTsqlFormatter : FormattingHandler<SelectCommandAsSelectionItem>
    {
        public override FormattedQuery Handle(SelectCommandAsSelectionItem expression, ITransformer transformer)
        {
            var inner = expression.Command.Accept<FormattedQuery>(transformer);
            return new FormattedQuery
            {
                Parameters = inner.Parameters,
                Query = $"({inner.Query})"
            };
        }
    }
}
