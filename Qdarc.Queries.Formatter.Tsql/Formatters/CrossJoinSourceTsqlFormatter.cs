using System.Linq;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class CrossJoinSourceTsqlFormatter : FormattingHandler<CrossJoinSource>
    {
        public override FormattedQuery Handle(CrossJoinSource expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);

            return new FormattedQuery
            {
                Query = $"{left.Query} CROSS JOIN {right.Query}",
                Parameters = left.Parameters.Concat(right.Parameters)
            };
        }
    }
}