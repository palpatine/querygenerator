using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class AndLogicalConjunctionExpressionTsqlFormatter : FormattingHandler<AndLogicalConjunctionExpression>
    {
        public override FormattedQuery Handle(AndLogicalConjunctionExpression expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);

            var result = new FormattedQuery
            {
                Parameters = left.Parameters.Concat(right.Parameters),
                Query = $"({left.Query}) AND ({right.Query})"
            };

            return result;
        }
    }
}