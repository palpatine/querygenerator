﻿using System.Linq;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class TabularFunctionCallTsqlFormatter : FormattingHandler<TabularFunctionCall>
    {
        public override FormattedQuery Handle(TabularFunctionCall expression, ITransformer transformer)
        {
            var arguments = expression.Arguments.Select(x => x.Accept<FormattedQuery>(transformer)).ToArray();
            var formattedArguments = string.Join(", ", arguments.Select(x => x.Query));
            var functionName = FormattingHelper.FormatElementName(expression.Name);
            return new FormattedQuery
            {
                Query = $"{functionName}({formattedArguments})",
                Parameters = arguments.SelectMany(x => x.Parameters).ToArray()
            };
        }
    }
}