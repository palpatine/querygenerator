﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class SqlNullExpressionFormatter : FormattingHandler<SqlNullExpression>
    {
        public override FormattedQuery Handle(SqlNullExpression expression, ITransformer transformer)
        {
            return new FormattedQuery
            {
                Query = "NULL"
            };
        }
    }
}
