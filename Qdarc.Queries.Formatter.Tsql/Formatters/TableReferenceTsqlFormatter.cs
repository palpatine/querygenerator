﻿using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class TableReferenceTsqlFormatter : FormattingHandler<TableReference>
    {
        public override FormattedQuery Handle(TableReference expression, ITransformer transformer)
        {
            var tableName = Context.ModelToSqlConvention.GetTableName(expression.ClrType);

            return new FormattedQuery
            {
                Query = FormattingHelper.FormatElementName(tableName)
            };
        }
    }
}