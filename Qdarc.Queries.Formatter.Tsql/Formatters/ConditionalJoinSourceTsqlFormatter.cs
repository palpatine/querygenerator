using System;
using System.Linq;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql.Formatters
{
    internal class ConditionalJoinSourceTsqlFormatter : FormattingHandler<ConditionalJoinSource>
    {
        public override FormattedQuery Handle(ConditionalJoinSource expression, ITransformer transformer)
        {
            var left = expression.Left.Accept<FormattedQuery>(transformer);
            var right = expression.Right.Accept<FormattedQuery>(transformer);
            var on = expression.On.Accept<FormattedQuery>(transformer);

            return new FormattedQuery
            {
                Query = $"{left.Query} {FormatJoinKind(expression.GetType())} JOIN {right.Query} ON ({on.Query})",
                Parameters = left.Parameters.Concat(right.Parameters).Concat(on.Parameters)
            };
        }

        private static string FormatJoinKind(Type joinKind)
        {
            if (joinKind == typeof(FullJoinSource))
            {
                return "FULL";
            }
            else if (joinKind == typeof(InnerJoinSource))
            {
                return "INNER";
            }
            else if (joinKind == typeof(LeftJoinSource))
            {
                return "LEFT";
            }
            else if (joinKind == typeof(RightJoinSource))
            {
                return "RIGHT";
            }

            throw new InvalidOperationException($"unknown kind of join: {joinKind.FullName}");
        }
    }
}