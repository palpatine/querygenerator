﻿using System.Reflection;
using System.Resources;
using System.Runtime.CompilerServices;

#if DEBUG
[assembly: InternalsVisibleTo("Qdarc.Queries.Formatter.Tsql.Tests")]
[assembly: InternalsVisibleTo("Qdarc.Queries.Tsql.Integration.Tests")]
#endif