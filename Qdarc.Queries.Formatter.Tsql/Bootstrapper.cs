﻿using System.Linq.Expressions;
using Microsoft.Extensions.DependencyInjection;
using Qdarc.Queries.Formatter.Tsql.Formatters;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Formatter.Tsql
{
    public static class Bootstrapper
    {
        public static void Bootstrap(IServiceCollection registrar)
        {
            registrar.AddTransient<FormattingOptions, FormattingOptions>();
            registrar.AddTransient<IAliasManager, AliasManager>();
            registrar.AddTransient<IFormattingContext, FormattingContext>();
            registrar.AddTransient<IFormattedQueryTransformer, FormattingTransformer>();

            registrar.AddTransient<IFormattingHandler<SqlNullExpression, FormattedQuery>, SqlNullExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlConstantExpression, FormattedQuery>, ConstantExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SelectCommand, FormattedQuery>, SelectCommandTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<AsteriskSelection, FormattedQuery>, AsteriskSelectionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<TableReference, FormattedQuery>, TableReferenceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<AndLogicalConjunctionExpression, FormattedQuery>, AndLogicalConjunctionExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<OrLogicalConjunctionExpression, FormattedQuery>, OrLogicalConjunctionExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<ExclusiveOrLogicalConjunctionExpression, FormattedQuery>, ExclusiveOrLogicalConjunctionExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<CrossJoinSource, FormattedQuery>, CrossJoinSourceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<FullJoinSource, FormattedQuery>, ConditionalJoinSourceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<LeftJoinSource, FormattedQuery>, ConditionalJoinSourceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<RightJoinSource, FormattedQuery>, ConditionalJoinSourceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<InnerJoinSource, FormattedQuery>, ConditionalJoinSourceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<LinqParameterReference, FormattedQuery>, LinqParameterReferenceTsqFormatter>();
            registrar.AddTransient<IFormattingHandler<AliasedSourceItem, FormattedQuery>, AliasedSourceItemTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<PrefixedSelection, FormattedQuery>, PrefixedSelectionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<AliasedSelectionItem, FormattedQuery>, AliasedSelectionItemTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<NotLessThanLogicalExpression, FormattedQuery>, NotLessThanLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<NotGreaterThanLogicalExpression, FormattedQuery>, NotGreaterThanLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<NotEqualLogicalExpression, FormattedQuery>, NotEqualLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<NotEqualIsoLogicalExpression, FormattedQuery>, NotEqualIsoLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<LessThanOrEqualLogicalExpression, FormattedQuery>, LessThanOrEqualLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<LessThanLogicalExpression, FormattedQuery>, LessThanLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<GreaterThanLogicalExpression, FormattedQuery>, GreaterThanLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<GreaterThanOrEqualLogicalExpression, FormattedQuery>, GreaterThanOrEqualLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<EqualLogicalExpression, FormattedQuery>, EqualLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<LinqColumnReference, FormattedQuery>, LinqColumnReferenceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlInlineIfExpression, FormattedQuery>, InlineIfTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<ExistsExpression, FormattedQuery>, ExistsTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlCastExpression, FormattedQuery>, CastExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlSelection, FormattedQuery>, SqlSelectionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<ProjectionByConstructor, FormattedQuery>, ProjectionByConstructorTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<TabularFunctionCall, FormattedQuery>, TabularFunctionCallTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<ScalarFunctionCall, FormattedQuery>, ScalarFunctionCallTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<ProcedureCall, FormattedQuery>, ProcedureCallTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlScript, FormattedQuery>, ScriptTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SubtractValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<MultiplyValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<AddValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<BitwiseAndValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<BitwiseExclusiveOrValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<BitwiseOrValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<DivideValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<ModuloValueExpression, FormattedQuery>, BinaryValueExpressionFormatter>();
            registrar.AddTransient<IFormattingHandler<ValuesSource, FormattedQuery>, ValuesSourceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlColumnReference, FormattedQuery>, SqlColumnReferenceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlCountExpression, FormattedQuery>, CountTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<LinqTableParameterReference, FormattedQuery>, LinqTableParameterReferenceFormatter>();
            registrar.AddTransient<IFormattingHandler<SelectCommandAsSelectionItem, FormattedQuery>, SelectCommandAsSelectionItemTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<SqlAsteriskCountExpression, FormattedQuery>, AsteriskCountTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<InsertCommand, FormattedQuery>, InsertCommandFormatter>();
            registrar.AddTransient<IFormattingHandler<CurrentDateAndTimeExpression, FormattedQuery>, CurrentDateAndTimeTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<ProjectionByMembersAssignment, FormattedQuery>, ProjectionByMembersAssignmentTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<BoundSelectionItem, FormattedQuery>, BoundSelectionItemTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<LinqColumnChainReference, FormattedQuery>, LinqColumnChainReferenceTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<NegateLogicalExpression, FormattedQuery>, NegateLogicalExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<NegateValueExpression, FormattedQuery>, NegateValueExpressionTsqlFormatter>();
            registrar.AddTransient<IFormattingHandler<UpdateCommand, FormattedQuery>, UpdateCommandFormatter>();
            registrar.AddTransient<IFormattingHandler<DeleteCommand, FormattedQuery>, DeleteCommandFormatter>();
        }
    }
}