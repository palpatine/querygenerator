﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Qdarc.Sql.Connector.SqlClient.Tests.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Qdarc.Sql.Connector.SqlClient.Tests.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT [Tests].[ExecuteScriptTests] ([TestUid], [Data]) 
        ///VALUES (&apos;{0}&apos;, &apos;data1&apos;);.
        /// </summary>
        internal static string ExecuteNonQueryTestQuery_tpl {
            get {
                return ResourceManager.GetString("ExecuteNonQueryTestQuery_tpl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT COUNT(*)
        ///FROM [Tests].[ExecuteScriptTests]
        ///WHERE [TestUid] = @testUid
        ///  AND [Data] = &apos;data1&apos;.
        /// </summary>
        internal static string ExecuteNonQueryVeryficationQuery {
            get {
                return ResourceManager.GetString("ExecuteNonQueryVeryficationQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT [Tests].[ExecuteScriptTests] ([TestUid], [Data]) 
        ///VALUES (&apos;{0}&apos;, &apos;data1&apos;);
        ///GO
        ///x.
        /// </summary>
        internal static string ExecuteScriptTestErrorQuery {
            get {
                return ResourceManager.GetString("ExecuteScriptTestErrorQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to INSERT [Tests].[ExecuteScriptTests] ([TestUid], [Data]) 
        ///VALUES (&apos;{0}&apos;, &apos;data1&apos;);
        ///GO
        ///INSERT [Tests].[ExecuteScriptTests] ([TestUid], [Data]) 
        ///VALUES (&apos;{0}&apos;, &apos;data2&apos;);.
        /// </summary>
        internal static string ExecuteScriptTestQuery {
            get {
                return ResourceManager.GetString("ExecuteScriptTestQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT COUNT(*)
        ///FROM [Tests].[ExecuteScriptTests]
        ///WHERE [TestUid] = @testUid
        ///  AND [Data] IN (&apos;data1&apos;,&apos;data2&apos;).
        /// </summary>
        internal static string ExecuteScriptVeryficationQuery {
            get {
                return ResourceManager.GetString("ExecuteScriptVeryficationQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SELECT 
        ///  @var AS c1
        ///INTO #tmp;
        ///
        ///SELECT
        ///    t.[name] AS [Name]
        ///  , c.[precision] AS [Precision]
        ///  , c.[scale] AS [Scale]
        ///  , CASE WHEN t.[name] IN (&apos;nchar&apos;,&apos;nvarchar&apos;) THEN c.[max_length] / IIF(c.[max_length]&gt;0, 2, 1)
        ///         WHEN t.[name] IN (&apos;char&apos;,&apos;varchar&apos;,&apos;binary&apos;,&apos;varbinary&apos;) THEN c.[max_length]
        ///         ELSE 0 END AS [Capacity]
        ///FROM tempdb.sys.columns AS c
        ///JOIN sys.types AS t ON t.system_type_id = c.system_type_id
        ///                    AND t.user_type_id = c.user_type_id
        ///WHERE object_id  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string ParameterMetadataQuery {
            get {
                return ResourceManager.GetString("ParameterMetadataQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = &apos;Tests&apos;)
        ///    EXEC (N&apos;CREATE SCHEMA [Tests] AUTHORIZATION [dbo]&apos;)
        ///
        ///IF OBJECT_ID(&apos;Tests.ExecuteScriptTests&apos;) IS NULL
        ///BEGIN
        ///    CREATE TABLE [Tests].[ExecuteScriptTests]
        ///    (
        ///        [Id] INT IDENTITY(1,1) NOT NULL,
        ///        [Timestamp] DATETIME NOT NULL,
        ///        [TestUid] UNIQUEIDENTIFIER NOT NULL,
        ///        [Data] NVARCHAR(MAX)
        ///    )
        ///
        ///    ALTER TABLE [Tests].[ExecuteScriptTests] 
        ///        ADD CONSTRAINT [DF_ExecuteScriptTests]
        ///        DEFAULT G [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string SqlCommandWrapperTestsInitializeQuery {
            get {
                return ResourceManager.GetString("SqlCommandWrapperTestsInitializeQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = &apos;Tests&apos;)
        ///    EXEC (N&apos;CREATE SCHEMA [Tests] AUTHORIZATION [dbo]&apos;)
        ///
        ///IF OBJECT_ID(&apos;Tests.ExecuteScriptTests&apos;) IS NULL
        ///BEGIN
        ///    CREATE TABLE [Tests].[ExecuteScriptTests]
        ///    (
        ///        [Id] INT IDENTITY(1,1) NOT NULL,
        ///        [Timestamp] DATETIME NOT NULL,
        ///        [TestUid] UNIQUEIDENTIFIER NOT NULL,
        ///        [Data] NVARCHAR(MAX)
        ///    )
        ///
        ///    ALTER TABLE [Tests].[ExecuteScriptTests] 
        ///        ADD CONSTRAINT [DF_ExecuteScriptTests]
        ///        DEFAULT G [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string SqlConnectionWrapperExecuteScriptTestsInitializeQuery {
            get {
                return ResourceManager.GetString("SqlConnectionWrapperExecuteScriptTestsInitializeQuery", resourceCulture);
            }
        }
    }
}
