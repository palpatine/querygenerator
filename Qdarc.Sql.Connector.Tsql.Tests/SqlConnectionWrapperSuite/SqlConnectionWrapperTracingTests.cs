﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Common.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests.SqlConnectionWrapperSuite
{
    [TestClass]
    public class SqlConnectionWrapperTracingTests
    {
        private Mock<ILog> _logMoq;

        private static string AdventureWorks2012ConnectionString
            => ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"].ConnectionString;

        [TestMethod]
        public void BeginTransaction_DebugStartingMessageShouldBeLogged()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                connectionWrapper.BeginTransaction();
            }

            _logMoq.Verify(x => x.Debug($"BeginTransaction Starting"), Times.Once);
        }

        [TestMethod]
        public void BeginTransactionWhenConnectionIsDisposed_ErrorMessageShouldBeLogged()
        {
            var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString);
            connectionWrapper.Dispose();

            QAssert.ShouldThrow<ObjectDisposedException>(
                () =>
                {
                    connectionWrapper.BeginTransaction();
                },
                exception =>
                {
                    _logMoq.Verify(x => x.Error("BeginTransaction Error occured", exception), Times.Once);
                    return true;
                });
        }

        [TestMethod]
        public void BeginTransactionWhenOtherTransactionIsAlreadyCreated_ErrorMessageShouldBeLogged()
        {
            var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString);

            connectionWrapper.BeginTransaction();

            QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    connectionWrapper.BeginTransaction();
                },
                exception =>
                {
                    _logMoq.Verify(x => x.Error("BeginTransaction Error occured", exception), Times.Once);
                    return true;
                });
        }

        [TestMethod]
        public void CreateSqlCommandWhenConnectionIsDisposed_ErrorMessageShouldBeLogged()
        {
            var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString);
            connectionWrapper.Dispose();

            QAssert.ShouldThrow<ObjectDisposedException>(
                () =>
                {
                    connectionWrapper.CreateSqlCommand(null);
                },
                exception =>
                {
                    _logMoq.Verify(x => x.Error("CreateSqlCommand Error occured", exception), Times.Once);
                    return true;
                });
        }

        [TestMethod]
        public void CreateSqlProcedureCommandWhenConnectionIsDisposed_ErrorMessageShouldBeLogged()
        {
            var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString);
            connectionWrapper.Dispose();

            QAssert.ShouldThrow<ObjectDisposedException>(
                () =>
                {
                    connectionWrapper.CreateSqlProcedureCommand("ProcedureName");
                },
                exception =>
                {
                    _logMoq.Verify(x => x.Error("CreateSqlCommand Error occured", exception), Times.Once);
                    return true;
                });
        }

        [TestMethod]
        public void CreateSqlProcedureCommandWithEmptyName_ErrorMessageShouldBeLogged()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                QAssert.ShouldThrow<ArgumentNullException>(
                () =>
                {
                    // ReSharper disable once AccessToDisposedClosure
                    using (connectionWrapper.CreateSqlProcedureCommand(null))
                    {
                    }
                },
                exception =>
                {
                    _logMoq.Verify(x => x.Error("CreateSqlProcedureCommand Error occured", exception), Times.Once);
                    return true;
                });
            }
        }

        [TestMethod]
        public void DisposeConnection_DebugCompletedMessageShouldBeLogged()
        {
            var connection = new SqlConnectionWrapper(AdventureWorks2012ConnectionString);
            connection.Dispose();

            _logMoq.Verify(
                x => x.Debug(@"Dispose Completed
Connection Closed"),
                Times.Once);
        }

        [TestMethod]
        public void ExecuteScript_DebugCompletedMessageShouldBeLogged()
        {
            const string query = @"
SELECT 2
GO
SELECT 'a'";

            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                connectionWrapper.ExecuteScript(query);
            }

            _logMoq.Verify(x => x.Debug("ExecuteScript Completed"), Times.Once);
        }

        [TestMethod]
        public void ExecuteScript_DebugStartingMessageShouldBeLogged()
        {
            const string query = @"
SELECT 2
GO
SELECT 'a'";

            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                connectionWrapper.ExecuteScript(query);
            }

            _logMoq.Verify(x => x.Debug("ExecuteScript Starting"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteScriptAsync_DebugCompletedMessageShouldBeLogged()
        {
            const string query = @"
SELECT 2
GO
SELECT 'a'";

            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                await connectionWrapper.ExecuteScriptAsync(query);
            }

            _logMoq.Verify(x => x.Debug("ExecuteScriptAsync Completed"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteScriptAsync_DebugStartingMessageShouldBeLogged()
        {
            const string query = @"
SELECT 2
GO
SELECT 'a'";

            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                await connectionWrapper.ExecuteScriptAsync(query);
            }

            _logMoq.Verify(x => x.Debug("ExecuteScriptAsync Starting"), Times.Once);
        }

        [TestMethod]
        public void OpenConnectionToDatabase_DebugStartingMessageShouldBeLogged()
        {
            using (new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                _logMoq.Verify(x => x.Debug($".ctor Starting{System.Environment.NewLine}Opening Connection"), Times.Once);
            }
        }

        [TestMethod]
        public void OpenConnectionToDatabase_TraceMessageShouldBeLogged()
        {
            using (new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                _logMoq.Verify(
                    x => x.Trace($@".ctor connectionString:
{AdventureWorks2012ConnectionString}"),
                    Times.Once);
            }
        }

        [TestMethod]
        public void OpenConnectionToNonExistingDatabase_ErrorMessageShouldBeLogged()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["AzureFakeDatabase"].ConnectionString;
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();

            QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    using (new SqlConnectionWrapper(connectionString, sqlExceptionProcessorMoq.Object))
                    {
                    }
                },
                exception =>
                {
                    _logMoq.Verify(x => x.Error(".ctor Error occured", exception), Times.Once);
                    return true;
                });
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var logFactoryAdapterMoq = new Mock<ILoggerFactoryAdapter>();
            _logMoq = new Mock<ILog>();
            _logMoq.Setup(x => x.IsTraceEnabled).Returns(true);
            _logMoq.Setup(x => x.IsDebugEnabled).Returns(true);
            _logMoq.Setup(x => x.IsInfoEnabled).Returns(true);
            _logMoq.Setup(x => x.IsWarnEnabled).Returns(true);
            _logMoq.Setup(x => x.IsErrorEnabled).Returns(true);
            logFactoryAdapterMoq.Setup(x => x.GetLogger(It.IsAny<Type>())).Returns(new Mock<ILog>().Object);
            logFactoryAdapterMoq.Setup(x => x.GetLogger(typeof(SqlConnectionWrapper))).Returns(_logMoq.Object);

            LogManager.Adapter = logFactoryAdapterMoq.Object;
        }
    }
}