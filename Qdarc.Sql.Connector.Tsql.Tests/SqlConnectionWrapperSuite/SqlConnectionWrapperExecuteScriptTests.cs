﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;
using Qdarc.Sql.Connector.SqlClient.Tests.Properties;

namespace Qdarc.Sql.Connector.SqlClient.Tests.SqlConnectionWrapperExecuteScriptSuite
{
    [TestClass]
    public class SqlConnectionWrapperExecuteScriptTests
    {
        private Guid _testUid;

        [TestMethod]
        public void ExecuteScript_ExceptionShouldBeProcessed()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();

            var sqlScript = string.Format(Resources.ExecuteScriptTestErrorQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString, sqlExceptionProcessorMoq.Object))
            {
                QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    // ReSharper disable once AccessToDisposedClosure
                    connectionWrapper.ExecuteScript(sqlScript);
                },
                exception =>
                {
                    sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                    return true;
                });
            }
        }

        [TestMethod]
        public void ExecuteScript_ExceptionShouldContainCommandText()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestErrorQuery, _testUid);
            var failedQuery = "x";

            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    // ReSharper disable once AccessToDisposedClosure
                    connectionWrapper.ExecuteScript(sqlScript);
                },
                exception =>
                {
                    var innerException = (SqlConnectorException)exception.InnerException;
                    innerException.CommandText.ShouldBeEqual(failedQuery);
                    return true;
                });
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteScript_ScriptWasExecuted()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                connectionWrapper.ExecuteScript(sqlScript);
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(2);
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScriptAsync_ExceptionShouldBeProcessed()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();

            var sqlScript = string.Format(Resources.ExecuteScriptTestErrorQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString, sqlExceptionProcessorMoq.Object))
            {
                await QAssert.ShouldThrowAsync<SqlConnectorException>(
                async () =>
                {
                    // ReSharper disable once AccessToDisposedClosure
                    await connectionWrapper.ExecuteScriptAsync(sqlScript);
                },
                exception =>
                {
                    sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                    return true;
                });
            }
        }

        [TestMethod]
        public async Task ExecuteScriptAsync_ExceptionShouldContainCommandText()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestErrorQuery, _testUid);
            var failedQuery = "x";

            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                await QAssert.ShouldThrowAsync<SqlConnectorException>(
                async () =>
                {
                    // ReSharper disable once AccessToDisposedClosure
                    await connectionWrapper.ExecuteScriptAsync(sqlScript);
                },
                exception =>
                {
                    var innerException = (SqlConnectorException)exception.InnerException;
                    innerException.CommandText.ShouldBeEqual(failedQuery);
                    return true;
                });
            }
        }

        [TestMethod]
        public async Task ExecuteScriptAsync_ScriptWasExecuted()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                await connectionWrapper.ExecuteScriptAsync(sqlScript);
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(2);
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScriptAsyncWithCommittedTransaction_ScriptWasExecuted()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                await connectionWrapper.ExecuteScriptAsync(sqlScript);
                transaction.Commit();
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(2);
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScriptAsyncWithoutCommittedTransaction_ScriptWasExecutedChangesWasRollback()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                connectionWrapper.BeginTransaction();
                await connectionWrapper.ExecuteScriptAsync(sqlScript);
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(0);
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScriptAsyncWithRollbackTransaction_ScriptWasExecuted()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                await connectionWrapper.ExecuteScriptAsync(sqlScript);
                transaction.Rollback();
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(0);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteScriptWithCommittedTransaction_ScriptWasExecuted()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                connectionWrapper.ExecuteScript(sqlScript);
                transaction.Commit();
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(2);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteScriptWithoutCommittedTransaction_ScriptWasExecutedChangesWasRollback()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                connectionWrapper.BeginTransaction();
                connectionWrapper.ExecuteScript(sqlScript);
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(0);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteScriptWithRollbackTransaction_ScriptWasExecuted()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var sqlScript = string.Format(Resources.ExecuteScriptTestQuery, _testUid);
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                connectionWrapper.ExecuteScript(sqlScript);
                transaction.Rollback();
            }

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteScriptVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(0);
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestInitialize]
        public void TestInitialize()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            _testUid = Guid.NewGuid();

            using (var connection = new SqlConnection(configurationProvider.ConnectionString))
            {
                using (var command = new SqlCommand(Resources.SqlConnectionWrapperExecuteScriptTestsInitializeQuery, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}