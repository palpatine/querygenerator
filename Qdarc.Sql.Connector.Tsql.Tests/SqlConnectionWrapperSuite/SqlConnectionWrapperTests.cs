﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests.SqlConnectionWrapperSuite
{
    [TestClass]
    public class SqlConnectionWrapperTests
    {
        [TestMethod]
        public void BeginTransaction_TransactionShouldBeCreated()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                transaction.ShouldBeInstanceOf<SqlTransactionWrapper>();
            }
        }

        [TestMethod]
        public void BeginTransactionWhenConnectionIsDisposed_ShouldThrowObjectDisposedException()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString);
            connectionWrapper.Dispose();

            QAssert.ShouldThrow<ObjectDisposedException>(
                () =>
                {
                    connectionWrapper.BeginTransaction();
                });
        }

        [TestMethod]
        public void BeginTransactionWhenOtherTransactionIsAlreadyCreated_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString, sqlExceptionProcessorMoq.Object);

            connectionWrapper.BeginTransaction();

            QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    connectionWrapper.BeginTransaction();
                },
                exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
        }

        [TestMethod]
        public void ConvertBrokenConnectionStateType_ShouldReturnBroken()
        {
            SqlConnectionWrapper
                .ConvertConnectionStateType(System.Data.ConnectionState.Broken)
                .ShouldBeEqual(ConnectionState.Broken);
        }

        [TestMethod]
        public void ConvertClosedConnectionStateType_ShouldReturnClosed()
        {
            SqlConnectionWrapper
                .ConvertConnectionStateType(System.Data.ConnectionState.Closed)
                .ShouldBeEqual(ConnectionState.Closed);
        }

        [TestMethod]
        public void ConvertConnectingConnectionStateType_ShouldReturnConnecting()
        {
            SqlConnectionWrapper
                .ConvertConnectionStateType(System.Data.ConnectionState.Connecting)
                .ShouldBeEqual(ConnectionState.Connecting);
        }

        [TestMethod]
        public void ConvertExecutingConnectionStateType_ShouldReturnExecuting()
        {
            SqlConnectionWrapper
                .ConvertConnectionStateType(System.Data.ConnectionState.Executing)
                .ShouldBeEqual(ConnectionState.Executing);
        }

        [TestMethod]
        public void ConvertFetchingConnectionStateType_ShouldReturnFetching()
        {
            SqlConnectionWrapper
                .ConvertConnectionStateType(System.Data.ConnectionState.Fetching)
                .ShouldBeEqual(ConnectionState.Fetching);
        }

        [TestMethod]
        public void ConvertOpenConnectionStateType_ShouldReturnOpen()
        {
            SqlConnectionWrapper
                .ConvertConnectionStateType(System.Data.ConnectionState.Open)
                .ShouldBeEqual(ConnectionState.Open);
        }

        [TestMethod]
        public void ConvertUndefinedConnectionStateType_ShouldThrowInvalidOperationException()
        {
            QAssert.ShouldThrow<InvalidOperationException>(
                () =>
                {
                    SqlConnectionWrapper.ConvertConnectionStateType((System.Data.ConnectionState)(-30));
                });
        }

        [TestMethod]
        public void CreateSqlCommand_CommandShouldBeCreated()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlCommand(null))
                {
                    command.ShouldBeInstanceOf<SqlCommandWrapper>();
                }
            }
        }

        [TestMethod]
        public void CreateSqlCommandWhenConnectionIsDisposed_ShouldThrowObjectDisposedException()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString);
            connectionWrapper.Dispose();

            QAssert.ShouldThrow<ObjectDisposedException>(
                () =>
                {
                    connectionWrapper.CreateSqlCommand(null);
                });
        }

        [TestMethod]
        public void CreateSqlProcedureCommand_CommandShouldBeCreated()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlProcedureCommand("ProcedureName"))
                {
                    command.ShouldBeInstanceOf<SqlCommandWrapper>();
                }
            }
        }

        [TestMethod]
        public void CreateSqlProcedureCommandWhenConnectionIsDisposed_ShouldThrowObjectDisposedException()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString);
            connectionWrapper.Dispose();

            QAssert.ShouldThrow<ObjectDisposedException>(
                () =>
                {
                    connectionWrapper.CreateSqlProcedureCommand("ProcedureName");
                });
        }

        [TestMethod]
        public void CreateSqlProcedureCommandWithEmptyName_ShouldThrowArgumentNullException()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                QAssert.ShouldThrow<ArgumentNullException>(
                () =>
                {
                    // ReSharper disable once AccessToDisposedClosure
                    using (connectionWrapper.CreateSqlProcedureCommand(null))
                    {
                    }
                });
            }
        }

        [TestMethod]
        public void DisposeConnection_ConnectionShouldBeClosed()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var connection = new SqlConnectionWrapper(configurationProvider.ConnectionString);
            connection.Dispose();

            connection.State.ShouldBeEqual(ConnectionState.Closed);
        }

        [TestMethod]
        public void OpenConnectionToDatabase_ConnectionShouldBeOpen()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            using (var connection = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                connection.State.ShouldBeEqual(ConnectionState.Open);
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "Qdarc.Sql.Connector.SqlClient.SqlConnectionWrapper")]
        [TestMethod]
        public void OpenConnectionToNonExistingDatabase_ExceptionShouldBeProcessed()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureFakeDatabase"];
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();

            QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    // ReSharper disable once ObjectCreationAsStatement
                    new SqlConnectionWrapper(configurationProvider.ConnectionString, sqlExceptionProcessorMoq.Object);
                },
                exception =>
                {
                    sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                    return true;
                });
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "Qdarc.Sql.Connector.SqlClient.SqlConnectionWrapper")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [TestMethod]
        public void OpenConnectionToNonExistingDatabase_ShouldThrowSqlConnectorException()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureFakeDatabase"];

            QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    // ReSharper disable once ObjectCreationAsStatement
                    new SqlConnectionWrapper(configurationProvider.ConnectionString);
                },
                exception =>
            {
                var sqlException = (SqlException)exception.InnerException;
                return sqlException.Class == 11 && sqlException.Number == 4060;
            });
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "Qdarc.Sql.Connector.SqlClient.SqlConnectionWrapper")]
        [TestMethod]
        public void OpenConnectionToNonExistingServer_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            var configurationProvider = ConfigurationManager.ConnectionStrings["FakeDataSource"];

            QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    // ReSharper disable once ObjectCreationAsStatement
                    new SqlConnectionWrapper(configurationProvider.ConnectionString, sqlExceptionProcessorMoq.Object);
                },
                exception =>
                {
                    sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                    return true;
                });
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "Qdarc.Sql.Connector.SqlClient.SqlConnectionWrapper")]
        [TestMethod]
        public void OpenConnectionToNonExistingServer_ShouldThrowSqlConnectorException()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["FakeDataSource"];

            QAssert.ShouldThrow<SqlConnectorException>(
                () =>
                {
                    // ReSharper disable once ObjectCreationAsStatement
                    new SqlConnectionWrapper(configurationProvider.ConnectionString);
                },
                exception =>
                {
                    var sqlException = (SqlException)exception.InnerException;
                    return sqlException.Class == 20 && sqlException.Number == 53;
                });
        }
    }
}