﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests
{
    [TestClass]
    public class SqlTransactionWrapperTests
    {
        [TestMethod]
        public void CommitWhenTransactionIsFinished_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];
            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString, sqlExceptionProcessorMoq.Object))
            {
                var transaction = connectionWrapper.BeginTransaction();
                transaction.Rollback();

                QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        transaction.Commit();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
            }
        }

        [TestMethod]
        public void RollbackWhenTransactionIsFinished_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            using (var connectionWrapper = new SqlConnectionWrapper(configurationProvider.ConnectionString, sqlExceptionProcessorMoq.Object))
            {
                var transaction = connectionWrapper.BeginTransaction();
                transaction.Rollback();

                QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        transaction.Rollback();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
            }
        }
    }
}