﻿using System;
using System.Configuration;
using Common.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests.SqlTransactionWrapperSuite
{
    [TestClass]
    public class SqlTransactionWrapperTracingTests
    {
        private Mock<ILog> _logMoq;

        private static string AdventureWorks2012ConnectionString
            => ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"].ConnectionString;

        [TestMethod]
        public void Commit_DebugCompletedMessageShouldBeLogged()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                transaction.Commit();

                _logMoq.Verify(x => x.Debug("Commit Completed"), Times.Once);
            }
        }

        [TestMethod]
        public void CommitWhenTransactionIsFinished_ErrorMessageShouldBeLogged()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                transaction.Rollback();

                QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        transaction.Commit();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("Commit Error occured", exception), Times.Once);
                        return true;
                    });
            }
        }

        [TestMethod]
        public void Rollback_DebugCompletedMessageShouldBeLogged()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                transaction.Rollback();

                _logMoq.Verify(x => x.Debug("Rollback Completed"), Times.Once);
            }
        }

        [TestMethod]
        public void RollbackWhenTransactionIsFinished_ErrorMessageShouldBeLogged()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(AdventureWorks2012ConnectionString))
            {
                var transaction = connectionWrapper.BeginTransaction();
                transaction.Rollback();

                QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        transaction.Rollback();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("Rollback Error occured", exception), Times.Once);
                        return true;
                    });
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var logFactoryAdapterMoq = new Mock<ILoggerFactoryAdapter>();
            _logMoq = new Mock<ILog>();
            _logMoq.Setup(x => x.IsTraceEnabled).Returns(true);
            _logMoq.Setup(x => x.IsDebugEnabled).Returns(true);
            _logMoq.Setup(x => x.IsInfoEnabled).Returns(true);
            _logMoq.Setup(x => x.IsWarnEnabled).Returns(true);
            _logMoq.Setup(x => x.IsErrorEnabled).Returns(true);
            logFactoryAdapterMoq.Setup(x => x.GetLogger(It.IsAny<Type>())).Returns(new Mock<ILog>().Object);
            logFactoryAdapterMoq.Setup(x => x.GetLogger(typeof(SqlTransactionWrapper))).Returns(_logMoq.Object);

            LogManager.Adapter = logFactoryAdapterMoq.Object;
        }
    }
}