﻿using System.Configuration;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests.Integration
{
    [TestClass]
    public class ReceiveErrorsFromServerTests
    {
        private static string ConnectionString
            => ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"].ConnectionString;

        [TestMethod]
        public async Task CreateCommandAndExecuteNonQueryAsyncWithWrongCommand_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongCommand Text";

            using (var connectionWrapper = new SqlConnectionWrapper(
                ConnectionString,
                sqlExceptionProcessorMoq.Object))
            {
                using (var command = connectionWrapper.CreateSqlCommand(commandText))
                {
                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await command.ExecuteNonQueryAsync();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task CreateProcedureAndExecuteScalarAsyncWithWrongCommand_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongProcedureName";

            using (var connectionWrapper = new SqlConnectionWrapper(
                ConnectionString,
                sqlExceptionProcessorMoq.Object))
            {
                using (var command = connectionWrapper.CreateSqlProcedureCommand(commandText))
                {
                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await command.ExecuteScalarAsync();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteReaderAndReadAsyncWithWrongCommand_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "SELECT * FROM (VALUES (1),('a')) v(i)";

            using (var connectionWrapper = new SqlConnectionWrapper(
                ConnectionString,
                sqlExceptionProcessorMoq.Object))
            {
                using (var command = connectionWrapper.CreateSqlCommand(commandText))
                {
                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await QAssert.ShouldThrowAsync<SqlConnectorException>(
                            async () =>
                            {
                                await reader.ReadAsync();
                            },
                            exception =>
                            {
                                sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                                return true;
                            });
                    }
                }
            }
        }

        [TestMethod]
        public void ExecuteReaderAndReadWithWrongCommand_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "SELECT * FROM (VALUES (1),('a')) v(i)";

            using (var connectionWrapper = new SqlConnectionWrapper(
                ConnectionString,
                sqlExceptionProcessorMoq.Object))
            {
                using (var command = connectionWrapper.CreateSqlCommand(commandText))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        QAssert.ShouldThrow<SqlConnectorException>(
                            () =>
                            {
                                reader.Read();
                            },
                            exception =>
                            {
                                sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                                return true;
                            });
                    }
                }
            }
        }
    }
}