﻿using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Sql.Connector.SqlClient;
using Qdarc.Sql.Connector.SqlClient.Tests.Properties;

namespace Qdarc.Sql.Connector.SqlClient.Tests.Integration
{
    [TestClass]
    public class SendParametersToSqlServerTests
    {
        private static string ConnectionString
            => ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"].ConnectionString;

        [TestMethod]
        public async Task CheckIfParameterIsDecimalWithRestrictedPrecisionAndScale()
        {
            const byte parameterPrecision = 5;
            const byte parameterScale = 1;

            using (var connectionWrapper = new SqlConnectionWrapper(ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlCommand(Resources.ParameterMetadataQuery))
                {
                    var parameter = command.AddParameter();
                    parameter.Name = "@var";
                    parameter.Type = TsqlTypeProvider.Decimal(parameterPrecision, parameterScale);

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        var baseType = (string)reader["Name"];
                        var precision = (byte)reader["Precision"];
                        var scale = (byte)reader["Scale"];

                        TsqlNativeTypesNames.NormalizeName(baseType).ShouldBeEqual(TsqlNativeTypesNames.Decimal);
                        precision.ShouldBeEqual(parameterPrecision);
                        scale.ShouldBeEqual(parameterScale);
                    }
                }
            }
        }

        [TestMethod]
        public async Task CheckIfTableParameterIsSend()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlProcedureCommand("CalculateSum"))
                {
                    var parameter = command.AddParameter();
                    parameter.Name = "@values";
                    parameter.Type = new TypeReference("[dbo].[IntList]");
                    parameter.Value = new[] { 1, 4, 5 }.Select(x =>
                      {
                          SqlMetaData[] metaData = new SqlMetaData[1];
                          metaData[0] = new SqlMetaData("ID", SqlDbType.Int);
                          SqlDataRecord record = new SqlDataRecord(metaData);
                          record.SetValues(new object[] { x });
                          return record;
                      })
                      .ToArray();

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        var value = reader[0];
                        var result = (int)value;
                        result.ShouldBeEqual(10);
                    }
                }
            }
        }

        [TestMethod]
        public async Task CheckIfTableParameterIsSendExecProcedure()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlCommand("exec CalculateSum @values"))
                {
                    var parameter = command.AddParameter();
                    parameter.Name = "@values";
                    parameter.Type = new TypeReference("[dbo].[IntList]");
                    parameter.Value = new[] { 1, 4, 5 }.Select(x =>
                    {
                        SqlMetaData[] metaData = new SqlMetaData[1];
                        metaData[0] = new SqlMetaData("ID", SqlDbType.Int);
                        SqlDataRecord record = new SqlDataRecord(metaData);
                        record.SetValues(new object[] { x });
                        return record;
                    })
                      .ToArray();

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        var value = reader[0];
                        var result = (int)value;
                        result.ShouldBeEqual(10);
                    }
                }
            }
        }

        [TestMethod]
        public async Task CheckIfParameterIsNVarChar()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlCommand(Resources.ParameterMetadataQuery))
                {
                    var parameter = command.AddParameter();
                    parameter.Name = "@var";
                    parameter.Type = TsqlTypeProvider.NVarCharMax;

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        var baseType = (string)reader["Name"];
                        var capacity = (int)reader["Capacity"];

                        TsqlNativeTypesNames.NormalizeName(baseType).ShouldBeEqual(TsqlNativeTypesNames.NVarChar);
                        capacity.ShouldBeEqual(-1);
                    }
                }
            }
        }

        [TestMethod]
        public async Task CheckIfParameterIsNVarCharWithRestrictedCapacity()
        {
            const byte parameterCapacity = 5;

            using (var connectionWrapper = new SqlConnectionWrapper(ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlCommand(Resources.ParameterMetadataQuery))
                {
                    var parameter = command.AddParameter();
                    parameter.Name = "@var";
                    parameter.Type = TsqlTypeProvider.NVarChar(parameterCapacity);

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        var baseType = (string)reader["Name"];
                        var capacity = (int)reader["Capacity"];

                        TsqlNativeTypesNames.NormalizeName(baseType).ShouldBeEqual(TsqlNativeTypesNames.NVarChar);
                        capacity.ShouldBeEqual(parameterCapacity);
                    }
                }
            }
        }

        [TestMethod]
        public async Task CheckIfParameterIsVarChar()
        {
            using (var connectionWrapper = new SqlConnectionWrapper(ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlCommand(Resources.ParameterMetadataQuery))
                {
                    var parameter = command.AddParameter();
                    parameter.Name = "@var";
                    parameter.Type = TsqlTypeProvider.VarCharMax;

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        var baseType = (string)reader["Name"];
                        var capacity = (int)reader["Capacity"];

                        TsqlNativeTypesNames.NormalizeName(baseType).ShouldBeEqual(TsqlNativeTypesNames.VarChar);
                        capacity.ShouldBeEqual(-1);
                    }
                }
            }
        }

        [TestMethod]
        public async Task CheckIfParameterIsVarCharWithRestrictedCapacity()
        {
            const byte parameterCapacity = 5;

            using (var connectionWrapper = new SqlConnectionWrapper(ConnectionString))
            {
                using (var command = connectionWrapper.CreateSqlCommand(Resources.ParameterMetadataQuery))
                {
                    var parameter = command.AddParameter();
                    parameter.Name = "@var";
                    parameter.Type = TsqlTypeProvider.VarChar(parameterCapacity);

                    using (var reader = await command.ExecuteReaderAsync())
                    {
                        await reader.ReadAsync();
                        var baseType = (string)reader["Name"];
                        var capacity = (int)reader["Capacity"];

                        TsqlNativeTypesNames.NormalizeName(baseType).ShouldBeEqual(TsqlNativeTypesNames.VarChar);
                        capacity.ShouldBeEqual(parameterCapacity);
                    }
                }
            }
        }
    }
}