﻿SELECT 
  @var AS c1
INTO #tmp;

SELECT
    t.[name] AS [Name]
  , c.[precision] AS [Precision]
  , c.[scale] AS [Scale]
  , CASE WHEN t.[name] IN ('nchar','nvarchar') THEN c.[max_length] / IIF(c.[max_length]>0, 2, 1)
         WHEN t.[name] IN ('char','varchar','binary','varbinary') THEN c.[max_length]
         ELSE 0 END AS [Capacity]
FROM tempdb.sys.columns AS c
JOIN sys.types AS t ON t.system_type_id = c.system_type_id
                    AND t.user_type_id = c.user_type_id
WHERE object_id = object_id('tempdb..#tmp');

DROP TABLE #tmp;