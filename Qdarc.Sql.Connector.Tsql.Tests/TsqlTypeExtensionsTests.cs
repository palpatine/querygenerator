﻿using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests
{
    [TestClass]
    public class TsqlTypeExtensionsTests
    {
        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapBigInt()
        {
            new TsqlType(TsqlNativeTypesNames.BigInt)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.BigInt);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapBinary()
        {
            new TsqlType(TsqlNativeTypesNames.Binary)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Binary);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapBit()
        {
            new TsqlType(TsqlNativeTypesNames.Bit)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Bit);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapChar()
        {
            new TsqlType(TsqlNativeTypesNames.Char)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Char);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapDate()
        {
            new TsqlType(TsqlNativeTypesNames.Date)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Date);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapDateDateTime()
        {
            new TsqlType(TsqlNativeTypesNames.DateTime)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.DateTime);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapDateDateTime2()
        {
            new TsqlType(TsqlNativeTypesNames.DateTime2)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.DateTime2);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapDateTimeOffset()
        {
            new TsqlType(TsqlNativeTypesNames.DateTimeOffset)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.DateTimeOffset);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapDecimal()
        {
            new TsqlType(TsqlNativeTypesNames.Decimal)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Decimal);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapFloat()
        {
            new TsqlType(TsqlNativeTypesNames.Float)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Float);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapImage()
        {
            new TsqlType(TsqlNativeTypesNames.Image)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Image);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapInt()
        {
            new TsqlType(TsqlNativeTypesNames.Int)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Int);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapMoney()
        {
            new TsqlType(TsqlNativeTypesNames.Money)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Money);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapNChar()
        {
            new TsqlType(TsqlNativeTypesNames.NChar)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.NChar);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapNText()
        {
            new TsqlType(TsqlNativeTypesNames.NText)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.NText);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapNumeric()
        {
            new TsqlType(TsqlNativeTypesNames.Numeric)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Decimal);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapNVarChar()
        {
            new TsqlType(TsqlNativeTypesNames.NVarChar)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.NVarChar);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapReal()
        {
            new TsqlType(TsqlNativeTypesNames.Real)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Real);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapSmallDateTime()
        {
            new TsqlType(TsqlNativeTypesNames.SmallDateTime)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.SmallDateTime);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapSmallInt()
        {
            new TsqlType(TsqlNativeTypesNames.SmallInt)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.SmallInt);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapSmallMoney()
        {
            new TsqlType(TsqlNativeTypesNames.SmallMoney)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.SmallMoney);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapText()
        {
            new TsqlType(TsqlNativeTypesNames.Text)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Text);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapTime()
        {
            new TsqlType(TsqlNativeTypesNames.Time)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Time);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapTimestamp()
        {
            new TsqlType(TsqlNativeTypesNames.Timestamp)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Timestamp);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapTinyInt()
        {
            new TsqlType(TsqlNativeTypesNames.TinyInt)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.TinyInt);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapUniqueIdentifier()
        {
            new TsqlType(TsqlNativeTypesNames.UniqueIdentifier)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.UniqueIdentifier);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapVarBinary()
        {
            new TsqlType(TsqlNativeTypesNames.VarBinary)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.VarBinary);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapVarChar()
        {
            new TsqlType(TsqlNativeTypesNames.VarChar)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.VarChar);
        }

        [TestMethod]
        public void GetSqlDbType_ShouldCorrectlyMapXml()
        {
            new TsqlType(TsqlNativeTypesNames.Xml)
                .GetSqlDbType()
                .ShouldBeEqual(SqlDbType.Xml);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapBigInt()
        {
            SqlDbType.BigInt
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.BigInt);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapBinary()
        {
            SqlDbType.Binary
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Binary);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapBit()
        {
            SqlDbType.Bit
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Bit);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapChar()
        {
            SqlDbType.Char
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Char);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapDate()
        {
            SqlDbType.Date
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Date);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapDateTime()
        {
            SqlDbType.DateTime
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.DateTime);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapDateTime2()
        {
            SqlDbType.DateTime2
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.DateTime2);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapDateTimeOffset()
        {
            SqlDbType.DateTimeOffset
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.DateTimeOffset);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapDecimal()
        {
            SqlDbType.Decimal
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Decimal);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapFloat()
        {
            SqlDbType.Float
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Float);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapImage()
        {
            SqlDbType.Image
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Image);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapInt()
        {
            SqlDbType.Int
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Int);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapMoney()
        {
            SqlDbType.Money
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Money);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapNChar()
        {
            SqlDbType.NChar
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.NChar);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapNText()
        {
            SqlDbType.NText
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.NText);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapNVarChar()
        {
            SqlDbType.NVarChar
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.NVarChar);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapReal()
        {
            SqlDbType.Real
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Real);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapSmallDateTime()
        {
            SqlDbType.SmallDateTime
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.SmallDateTime);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapSmallInt()
        {
            SqlDbType.SmallInt
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.SmallInt);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapSmallMoney()
        {
            SqlDbType.SmallMoney
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.SmallMoney);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapText()
        {
            SqlDbType.Text
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Text);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapTime()
        {
            SqlDbType.Time
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Time);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapTimestamp()
        {
            SqlDbType.Timestamp
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Timestamp);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapTinyInt()
        {
            SqlDbType.TinyInt
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.TinyInt);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapUniqueIdentifier()
        {
            SqlDbType.UniqueIdentifier
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.UniqueIdentifier);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapVarBinary()
        {
            SqlDbType.VarBinary
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.VarBinary);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapVarChar()
        {
            SqlDbType.VarChar
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.VarChar);
        }

        [TestMethod]
        public void GetTsqlTypeName_ShouldCorrectlyMapXml()
        {
            SqlDbType.Xml
                .GetTsqlTypeName()
                .ShouldBeEqual(TsqlNativeTypesNames.Xml);
        }
    }
}