﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests
{
    [TestClass]
    public class SqlScriptHelperTests
    {
        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithBlockComment_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx/*
*/aa");
            result.ShouldBeEqual("xxaa");
        }

        // Known Bug
        [Ignore]
        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithBlockCommentInSingleQuotes_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx ' a /*aa*/ '");
            result.ShouldBeEqual("xx ' a /*aa*/ '");
        }

        // Known Bug
        [Ignore]
        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithBlockCommentInSquareBrackets_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx [ a /*aa*/ ]");
            result.ShouldBeEqual("xx [ a /*aa*/ ]");
        }

        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithEmptyValue_ShouldReturnEmptyString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(string.Empty);
            result.ShouldBeEqual(string.Empty);
        }

        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithManySingleLineComment_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx --aa
--bb");
            result.ShouldBeEqual("xx ");
        }

        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithNestedMultilineComment_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx/*/* de */
*/aa");
            result.ShouldBeEqual("xxaa");
        }

        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithNullValue_ShouldReturnEmptyString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(null);
            result.ShouldBeEqual(string.Empty);
        }

        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithoutComment_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx
gg");
            result.ShouldBeEqual("xx\r\ngg");
        }

        // Known Bug
        [Ignore]
        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithSBlockCommentInDoubleQuotes_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx "" a /*aa*/ """);
            result.ShouldBeEqual("xx \" a /*aa*/ \"");
        }

        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithSingleLineComment_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx --aa");
            result.ShouldBeEqual("xx ");
        }

        // Known Bug
        [Ignore]
        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithSingleLineCommentInDoubleQuotes_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx "" a --aa """);
            result.ShouldBeEqual("xx \" a --aa \"");
        }

        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithSingleLineCommentInMultilineScript_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx
--aa
gg");
            result.ShouldBeEqual("xx\r\ngg");
        }

        // Known Bug
        [Ignore]
        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithSingleLineCommentInSingleQuotes_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx ' a --aa '");
            result.ShouldBeEqual("xx ' a --aa '");
        }

        // Known Bug
        [Ignore]
        [TestMethod]
        public void RemoveCommentsFromSqlScriptWithSingleLineCommentInSquareBrackets_ShouldReturnString()
        {
            var result = SqlScriptHelper.RemoveCommentsFromSqlScript(@"xx [ a --aa ]");
            result.ShouldBeEqual("xx [ a --aa ]");
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatement_ShouldReturnStringCollection()
        {
            const string sample = @"xx --a
Go
aa";
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(sample);
            result.AllCorrespondingElementsShouldBeEqual(new[] { "xx --a", "aa" });
        }

        // Known Bug
        [Ignore]
        [TestMethod]
        public void SplitSqlScriptByGoStatementMultilineCommented_ShouldReturnSingleCommand()
        {
            const string sample = @"xx /*
Go
*/
aa";
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(sample);
            result.AllCorrespondingElementsShouldBeEqual(new[] { sample });
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatementSingleLineCommented_ShouldReturnSingleCommand()
        {
            const string sample = @"xx
-- Go
aa";
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(sample);
            result.AllCorrespondingElementsShouldBeEqual(new[] { sample });
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatementWithBlockComments_ShouldReturnStringCollection()
        {
            const string sample = @"xx
 /*comment */  Go   /*Comment */
aa";
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(sample);
            result.AllCorrespondingElementsShouldBeEqual(new[] { "xx\r\n /*comment */", "/*Comment */\r\naa" });
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatementWithComments_ShouldReturnStringCollection()
        {
            const string sample = @"xx
/*comment */ Go   -- Comment
aa";
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(sample);
            result.AllCorrespondingElementsShouldBeEqual(new[] { "xx\r\n/*comment */", "-- Comment\r\naa" });
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatementWithEmptyValue_ShouldReturnEmptyCollection()
        {
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(string.Empty);
            result.Any().ShouldBeFalse();
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatementWithGivenCount_ShouldReturnStringCollection()
        {
            const string sample = @"xx
Go 3
aa";
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(sample);
            result.AllCorrespondingElementsShouldBeEqual(new[] { "xx", "xx", "xx", "aa" });
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatementWithMultipleGoStatement_ShouldReturnStringCollection()
        {
            const string sample = @"xx
    Go

GO
aa";
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(sample);
            result.AllCorrespondingElementsShouldBeEqual(new[] { "xx", "aa" });
        }

        [TestMethod]
        public void SplitSqlScriptByGoStatementWithNullValue_ShouldReturnEmptyCollection()
        {
            var result = SqlScriptHelper.SplitSqlScriptByGoStatement(null);
            result.Any().ShouldBeFalse();
        }
    }
}