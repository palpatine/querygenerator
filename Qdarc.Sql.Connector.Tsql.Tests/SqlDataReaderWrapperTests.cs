﻿using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests
{
    [TestClass]
    public class SqlDataReaderWrapperTests
    {
        private static string ConnectionString
            => ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"].ConnectionString;

        [TestMethod]
        public void Read_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();

            var query = @"SELECT * FROM (VALUES (1),('a')) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query, sqlExceptionProcessorMoq.Object))
                    {
                        QAssert.ShouldThrow<SqlConnectorException>(
                            () =>
                            {
                                readerWrapper.Read();
                            },
                            exception =>
                            {
                                sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                                return true;
                            });
                    }
                }
            }
        }

        [TestMethod]
        public void Read_ExceptionShouldContainCommandText()
        {
            const string query = @"SELECT * FROM (VALUES (1),('a')) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        QAssert.ShouldThrow<SqlConnectorException>(
                            () =>
                            {
                                readerWrapper.Read();
                            },
                            exception =>
                    {
                        exception.CommandText.ShouldBeEqual(query);
                        return true;
                    });
                    }
                }
            }
        }

        [TestMethod]
        public void Read_ShouldReturnFalseIfNoResultLeft()
        {
            var query = @"SELECT * FROM (VALUES (1)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    reader.Read();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        var readSucces = readerWrapper.Read();
                        readSucces.ShouldBeFalse();
                    }
                }
            }
        }

        [TestMethod]
        public void NextResult_ShouldReturnFalseIfNoResultLeft()
        {
            var query = @"SELECT * FROM (VALUES (1)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        var readSucces = readerWrapper.NextResult();
                        readSucces.ShouldBeFalse();
                    }
                }
            }
        }

        [TestMethod]
        public void NextResult_ShouldReturnTrueIfResultExist()
        {
            var query = @"SELECT * FROM (VALUES (1)) v(i);SELECT * FROM (VALUES (1)) v(i);";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        var readSucces = readerWrapper.NextResult();
                        readSucces.ShouldBeTrue();
                    }
                }
            }
        }

        [TestMethod]
        public void NextResult_ShouldReturnTrueIfResultExist_MultipleResultsSetFalse()
        {
            var query = @"SELECT * FROM (VALUES (1)) v(i);SELECT * FROM (VALUES (1)) v(i);";

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012MultipleActiveResultSetsFalse"].ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        var readSucces = readerWrapper.NextResult();
                        readSucces.ShouldBeTrue();
                    }
                }
            }
        }

        [TestMethod]
        public async Task NextResultAsync_ShouldReturnFalseIfNoResultLeft()
        {
            var query = @"SELECT * FROM (VALUES (1)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        var readSucces = await readerWrapper.NextResultAsync();
                        readSucces.ShouldBeFalse();
                    }
                }
            }
        }

        [TestMethod]
        public async Task NextResultAsync_ShouldReturnTrueIfResultExist()
        {
            var query = @"SELECT * FROM (VALUES (1)) v(i);SELECT * FROM (VALUES (1)) v(i);";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        var readSucces = await readerWrapper.NextResultAsync();
                        readSucces.ShouldBeTrue();
                    }
                }
            }
        }

        [TestMethod]
        public void Read_ShouldReturnNullIfDbNullWasRead()
        {
            var query = @"SELECT * FROM (VALUES (NULL),(2)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        readerWrapper.Read();
                        var value = readerWrapper[0];
                        value.ShouldBeNull();
                    }
                }
            }
        }

        [TestMethod]
        public void Read_ShouldReturnTrueIfResultExists()
        {
            var query = @"SELECT * FROM (VALUES (1)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        var readSucces = readerWrapper.Read();
                        readSucces.ShouldBeTrue();
                    }
                }
            }
        }

        [TestMethod]
        public void Read_ValueHasRead()
        {
            var query = @"SELECT * FROM (VALUES (1),(2)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        readerWrapper.Read();
                        var value = readerWrapper[0];
                        value.ShouldBeEqual(1);
                    }
                }
            }
        }

        [TestMethod]
        public async Task ReadAsync_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            var query = @"SELECT * FROM (VALUES (1),('a')) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query, sqlExceptionProcessorMoq.Object))
                    {
                        await QAssert.ShouldThrowAsync<SqlConnectorException>(
                            async () =>
                            {
                                await readerWrapper.ReadAsync();
                            },
                            exception =>
                            {
                                sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                                return true;
                            });
                    }
                }
            }
        }

        [TestMethod]
        public async Task ReadAsync_ExceptionShouldContainCommandText()
        {
            var query = @"SELECT * FROM (VALUES (1),('a')) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = command.ExecuteReader();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        await QAssert.ShouldThrowAsync<SqlConnectorException>(
                            async () =>
                            {
                                await readerWrapper.ReadAsync();
                            },
                            exception =>
                    {
                        exception.CommandText.ShouldBeEqual(query);
                        return true;
                    });
                    }
                }
            }
        }

        [TestMethod]
        public async Task ReadAsync_ValueHasBeenReadByColumnId()
        {
            var query = @"SELECT * FROM (VALUES (1),(2)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = await command.ExecuteReaderAsync();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        await readerWrapper.ReadAsync();
                        var value = readerWrapper[0];
                        value.ShouldBeEqual(1);
                    }
                }
            }
        }

        [TestMethod]
        public async Task ReadAsync_ValueHasBeenReadByColumnName()
        {
            var query = @"SELECT * FROM (VALUES (1),(2)) v(i)";

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    var reader = await command.ExecuteReaderAsync();
                    using (var readerWrapper = new SqlDataReaderWrapper(reader, query))
                    {
                        await readerWrapper.ReadAsync();
                        var value = readerWrapper["i"];
                        value.ShouldBeEqual(1);
                    }
                }
            }
        }
    }
}