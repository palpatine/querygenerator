﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.SqlServer.Server;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests
{
    [TestClass]
    public class SqlParameterWrapperTests
    {
        [TestMethod]
        public void Name_ShouldGetCorrectlyValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameter.ParameterName = "xx";
            parameterWrapper.Name.ShouldBeEqual("xx");
        }

        [TestMethod]
        public void Name_ShouldSetCorrectlyValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Name = "xx";
            parameter.ParameterName.ShouldBeEqual("xx");
        }

        [TestMethod]
        public void SqlParameter_ShouldGetCorrectlyValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.SqlParameter.ShouldBeTheSameInstance(parameter);
        }

        [TestMethod]
        public void ToString_ShouldPresentNullValue()
        {
            var parameter = new SqlParameter();
            parameter.SqlDbType = SqlDbType.Decimal;
            parameter.Value = DBNull.Value;
            var parameterWrapper = new SqlParameterWrapper(parameter);
            var text = parameterWrapper.ToString();

            text.ShouldBeEqual("NULL");
        }

        [TestMethod]
        public void ToString_ShouldPresentStructuredType()
        {
            var parameter = new SqlParameter();
            parameter.SqlDbType = SqlDbType.Structured;
            parameter.TypeName = "[dbo].[Type]";
            parameter.Value = new List<SqlDataRecord>();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            var text = parameterWrapper.ToString();

            text.ShouldBeEqual("<set:[dbo].[Type]>");
        }

        [TestMethod]
        public void Type_ShouldAssignComplexType()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Type = new TypeReference("[dbo].typeName");
            parameter.SqlDbType.ShouldBeEqual(SqlDbType.Structured);
            parameter.TypeName.ShouldBeEqual("[dbo].typeName");
        }

        [TestMethod]
        public void Type_ShouldGetCorrectlyPrecision()
        {
            var parameter = new SqlParameter("name", SqlDbType.Decimal);
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameter.Precision = 13;
            var type = (TsqlType)parameterWrapper.Type;
            type.Precision.ShouldBeEqual((byte)13);
        }

        [TestMethod]
        public void Type_ShouldGetCorrectlyScale()
        {
            var parameter = new SqlParameter("name", SqlDbType.Decimal);
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameter.Scale = 13;
            var type = (TsqlType)parameterWrapper.Type;
            type.Scale.ShouldBeEqual((byte)13);
        }

        [TestMethod]
        public void Type_ShouldGetCorrectlyTypeCapacity()
        {
            var parameter = new SqlParameter("name", SqlDbType.Char, 13);
            var parameterWrapper = new SqlParameterWrapper(parameter);
            var type = (TsqlType)parameterWrapper.Type;
            type.Capacity.ShouldBeEqual(13);
        }

        [TestMethod]
        public void Type_ShouldGetCorrectlyTypeIsNullable()
        {
            var parameter = new SqlParameter("name", SqlDbType.Int);
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameter.IsNullable = true;
            var type = (TsqlType)parameterWrapper.Type;
            type.IsNullable.ShouldBeTrue();
        }

        [TestMethod]
        public void Type_ShouldGetCorrectlyTypeName()
        {
            var parameter = new SqlParameter("name", SqlDbType.Int);
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Type.ShouldBeInstanceOf<TsqlType>().Name.ShouldBeEqual("INT");
        }

        [TestMethod]
        public void Type_ShouldRecognizeComplexType()
        {
            var parameter = new SqlParameter();
            parameter.SqlDbType = SqlDbType.Structured;
            parameter.TypeName = "typeName";
            var parameterWrapper = new SqlParameterWrapper(parameter);
            var customType = parameterWrapper.Type.ShouldBeInstanceOf<TypeReference>();
            customType.Name.ShouldBeEqual("typeName");
        }

        [TestMethod]
        public void Type_ShouldRecognizeComplexTypeWithPrefixedName()
        {
            var parameter = new SqlParameter();
            parameter.SqlDbType = SqlDbType.Structured;
            parameter.TypeName = "[dbo].typeName";
            var parameterWrapper = new SqlParameterWrapper(parameter);
            var customType = parameterWrapper.Type.ShouldBeInstanceOf<TypeReference>();
            customType.Name.ShouldBeEqual("[dbo].typeName");
        }

        [TestMethod]
        public void Type_ShouldSetCorrectlyTypeIsNullableValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Type = new TsqlType("INT", true);
            parameter.IsNullable.ShouldBeTrue();
        }

        [TestMethod]
        public void Type_ShouldSetCorrectlyTypeNameValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Type = new TsqlType("INT");
            parameter.SqlDbType.ShouldBeEqual(SqlDbType.Int);
        }

        [TestMethod]
        public void Type_ShouldSetCorrectlyTypePrecisionAndScaleValues()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Type = new TsqlType(TsqlNativeTypesNames.Decimal, false, 13, 7);
            parameter.Precision.ShouldBeEqual((byte)13);
            parameter.Scale.ShouldBeEqual((byte)7);
        }

        [TestMethod]
        public void Type_ShouldSetCorrectlyTypeSizeValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Type = new TsqlType(TsqlNativeTypesNames.Char, false, 13);
            parameter.Size.ShouldBeEqual(13);
        }

        [TestMethod]
        public void Type_unicode_ShouldGetCorrectlyTypeCapacity()
        {
            var parameter = new SqlParameter("name", SqlDbType.NVarChar, 13);
            var parameterWrapper = new SqlParameterWrapper(parameter);
            var type = (TsqlType)parameterWrapper.Type;
            type.Capacity.ShouldBeEqual(13);
        }

        [TestMethod]
        public void Value_ShouldGetCorrectlyValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameter.Value = "xx";
            parameterWrapper.Value.ShouldBeEqual("xx");
        }

        [TestMethod]
        public void Value_ShouldGetNull()
        {
            var parameter = new SqlParameter();
            parameter.Value = DBNull.Value;

            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Value.ShouldBeNull();
        }

        [TestMethod]
        public void Value_ShouldGetParameterExistingValue()
        {
            var parameter = new SqlParameter();
            parameter.Value = "xx";
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Value.ShouldBeEqual("xx");
        }

        [TestMethod]
        public void Value_ShouldSetCorrectlyValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Value = "xx";
            parameter.Value.ShouldBeEqual("xx");
        }

        [TestMethod]
        public void Value_ShouldSetDbNullValue()
        {
            var parameter = new SqlParameter();
            var parameterWrapper = new SqlParameterWrapper(parameter);
            parameterWrapper.Value = null;
            parameter.Value.ShouldBeEqual(DBNull.Value);
        }
    }
}