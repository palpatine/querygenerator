﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Qdarc.Sql.Connector.SqlClient.Tests.Environment
{
    [TestClass]
    public static class AppBootstrapper
    {
        public static IContainer Container { get; private set; }

        [AssemblyCleanup]
        public static void Cleanup()
        {
        }

        [AssemblyInitialize]
        public static void Create(TestContext context)
        {
            var containerBuilder = new ContainerBuilder();
            var registrar = new ServiceCollection();
            containerBuilder.Populate(registrar);
            Container = containerBuilder.Build();
        }
    }
}