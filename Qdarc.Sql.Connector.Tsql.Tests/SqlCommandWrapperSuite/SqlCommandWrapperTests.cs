﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;
using Qdarc.Sql.Connector.SqlClient.Tests.Properties;

namespace Qdarc.Sql.Connector.SqlClient.Tests.SqlCommandWrapperSuite
{
    [TestClass]
    public class SqlCommandWrapperTests
    {
        private Guid _testUid;

        private static string ConnectionString
                    => ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"].ConnectionString;

        [TestMethod]
        public void AddParameter_ParameterShouldBeAddedToCollection()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("x", connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.AddParameter();
                        var parameters = commandwrapper.Parameters;
                        parameters.ShouldBeInstanceOf<IEnumerable<ISqlParameter>>();
                        parameters.Count().ShouldBeEqual(1);
                    }
                }
            }
        }

        [TestMethod]
        public void AddParameter_ParameterShouldBeReturned()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("x", connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        var parameter = commandwrapper.AddParameter();
                        parameter.ShouldBeInstanceOf<ISqlParameter>();
                    }
                }
            }
        }

        [TestMethod]
        public void AddParameter_ParameterShouldBeReturnedWithDbNullValue()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("x", connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        var parameter = (SqlParameterWrapper)commandwrapper.AddParameter();
                        parameter.SqlParameter.Value.ShouldBeEqual(DBNull.Value);
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteNonQuery_CommandHasSideEffects()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        var affectedRows = commandwrapper.ExecuteNonQuery();
                        affectedRows.ShouldBeEqual(1);
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteNonQuery_CommandWasExecuted()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteNonQuery();
                    }
                }
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteNonQueryVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(1);
                }
            }
        }

        [TestMethod]
        public void ExecuteNonQuery_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command, sqlExceptionProcessorMoq.Object);

                    QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        commandwrapper.ExecuteNonQuery();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public void ExecuteNonQuery_ExceptionShouldContainCommandText()
        {
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        commandwrapper.ExecuteNonQuery();
                    },
                    exception =>
                    {
                        exception.CommandText.ShouldBeEqual(commandText);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_CommandHasSideEffects()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(query, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);
                    var affectedRows = await commandwrapper.ExecuteNonQueryAsync();
                    affectedRows.ShouldBeEqual(1);
                }
            }
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_CommandWasExecuted()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(query, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);
                    await commandwrapper.ExecuteNonQueryAsync();
                }
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(Resources.ExecuteNonQueryVeryficationQuery, connection))
                {
                    connection.Open();
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = command.ExecuteScalar();
                    result.ShouldBeEqual(1);
                }
            }
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command, sqlExceptionProcessorMoq.Object);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_ExceptionShouldContainCommandText()
        {
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    },
                    exception =>
                    {
                        exception.CommandText.ShouldBeEqual(commandText);
                        return true;
                    });
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteReader_CommandWasExecuted()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Resources.ExecuteNonQueryVeryficationQuery, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        command.Parameters.AddWithValue("@testUid", _testUid);
                        var result = commandwrapper.ExecuteReader();
                        result.ShouldBeInstanceOf<SqlDataReaderWrapper>();
                    }
                }
            }
        }

        [TestMethod]
        public void ExecuteReader_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command, sqlExceptionProcessorMoq.Object))
                    {
                        QAssert.ShouldThrow<SqlConnectorException>(
                            () =>
                            {
                                commandwrapper.ExecuteReader();
                            },
                            exception =>
                            {
                                sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                                return true;
                            });
                    }
                }
            }
        }

        [TestMethod]
        public void ExecuteReader_ExceptionShouldContainCommandText()
        {
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        QAssert.ShouldThrow<SqlConnectorException>(
                            () =>
                            {
                                commandwrapper.ExecuteReader();
                            },
                            exception =>
                            {
                                exception.CommandText.ShouldBeEqual(commandText);
                                return true;
                            });
                    }
                }
            }
        }

        [TestMethod]
        public async Task ExecuteReaderAsync_CommandWasExecuted()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Resources.ExecuteNonQueryVeryficationQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = await commandwrapper.ExecuteReaderAsync();
                    result.ShouldBeInstanceOf<SqlDataReaderWrapper>();
                }
            }
        }

        [TestMethod]
        public async Task ExecuteReaderAsync_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command, sqlExceptionProcessorMoq.Object);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteReaderAsync_ExceptionShouldContainCommandText()
        {
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    },
                    exception =>
                    {
                        exception.CommandText.ShouldBeEqual(commandText);
                        return true;
                    });
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestMethod]
        public void ExecuteScalar_CommandWasExecuted()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Resources.ExecuteNonQueryVeryficationQuery, connection))
                {
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        var result = commandwrapper.ExecuteScalar();
                        result.ShouldBeEqual(1);
                    }
                }
            }
        }

        [TestMethod]
        public void ExecuteScalar_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command, sqlExceptionProcessorMoq.Object);

                    QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        commandwrapper.ExecuteScalar();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public void ExecuteScalar_ExceptionShouldContainCommandText()
        {
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        commandwrapper.ExecuteScalar();
                    },
                    exception =>
                    {
                        exception.CommandText.ShouldBeEqual(commandText);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScalarAsync_CommandWasExecuted()
        {
            var query = string.Format(Resources.ExecuteNonQueryTestQuery_tpl, _testUid);

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(query, connection))
                {
                    connection.Open();
                    await command.ExecuteNonQueryAsync();
                }
            }

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Resources.ExecuteNonQueryVeryficationQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);
                    command.Parameters.AddWithValue("@testUid", _testUid);
                    var result = await commandwrapper.ExecuteScalarAsync();
                    result.ShouldBeEqual(1);
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScalarAsync_ExceptionShouldBeProcessed()
        {
            var sqlExceptionProcessorMoq = new Mock<ISqlConnectorExceptionProcessor>();
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command, sqlExceptionProcessorMoq.Object);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    },
                    exception =>
                    {
                        sqlExceptionProcessorMoq.Verify(x => x.ProcessSqlConnectorException(exception));
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScalarAsync_ExceptionShouldContainCommandText()
        {
            const string commandText = "WrongCommand Text";

            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(commandText, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    },
                    exception =>
                    {
                        exception.CommandText.ShouldBeEqual(commandText);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public void ParametersWithEmptyCollection_ShouldReturnEmptyCollection()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("x", connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        var parameters = commandwrapper.Parameters;
                        parameters.ShouldBeInstanceOf<IEnumerable<ISqlParameter>>();
                        parameters.Any().ShouldBeFalse();
                    }
                }
            }
        }

        [TestMethod]
        public void ParametersWithGivenCollection_ShouldReturnCollection()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand("x", connection))
                {
                    command.Parameters.AddWithValue("@sample", "value");
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        var parameters = commandwrapper.Parameters;
                        parameters.ShouldBeInstanceOf<IEnumerable<ISqlParameter>>();
                        parameters.Count().ShouldBeEqual(1);
                    }
                }
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [TestInitialize]
        public void TestInitialize()
        {
            _testUid = Guid.NewGuid();

            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = new SqlCommand(Resources.SqlCommandWrapperTestsInitializeQuery, connection))
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}