﻿IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = 'Tests')
    EXEC (N'CREATE SCHEMA [Tests] AUTHORIZATION [dbo]')

IF OBJECT_ID('Tests.ExecuteScriptTests') IS NULL
BEGIN
    CREATE TABLE [Tests].[ExecuteScriptTests]
    (
        [Id] INT IDENTITY(1,1) NOT NULL,
        [Timestamp] DATETIME NOT NULL,
        [TestUid] UNIQUEIDENTIFIER NOT NULL,
        [Data] NVARCHAR(MAX)
    )

    ALTER TABLE [Tests].[ExecuteScriptTests] 
        ADD CONSTRAINT [DF_ExecuteScriptTests]
        DEFAULT GETUTCDATE() FOR [Timestamp]
END

DELETE [Tests].[ExecuteScriptTests]
WHERE [Timestamp] < DATEADD(d,-1,GETUTCDATE())