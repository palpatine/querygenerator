﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Common.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests.SqlCommandWrapperSuite
{
    [TestClass]
    public class SqlCommandWrapperTracingTests
    {
        private const string Query = "SELECT * FROM (VALUES (3,'a'), (1,'B')) v(a,b)";
        private const string WrongQuery = "WrongCommand Text";

        private Mock<ILog> _logMoq;

        private static string ConnectionString
                    => ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"].ConnectionString;

        [TestMethod]
        public void ExecuteNonQuery_DebugCompletedMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteNonQuery();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteNonQuery Completed{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public void ExecuteNonQuery_DebugStartingMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteNonQuery();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteNonQuery Starting{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public void ExecuteNonQuery_ErrorMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(WrongQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        commandwrapper.ExecuteNonQuery();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("ExecuteNonQuery Error occured", exception), Times.Once);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public void ExecuteNonQuery_TraceResultShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteNonQuery();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteNonQuery Result:
-1"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_DebugCompletedMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteNonQueryAsync Completed{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_DebugStartingMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteNonQueryAsync Starting{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_ErrorMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(WrongQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("ExecuteNonQueryAsync Error occured", exception), Times.Once);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsync_TraceResultShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteNonQueryAsync Result:
-1"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsyncWithParameter_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("@param", 2);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteNonQueryAsync Parameters:
@param INT = 2"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteNonQueryAsyncWithParameters_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    var date = new DateTime(2016, 1, 2, 3, 4, 5).AddMilliseconds(660);
                    command.Parameters.AddWithValue("@paramInt", 2);
                    command.Parameters.AddWithValue("@paramStr", "text");
                    command.Parameters.AddWithValue("@paramDate", date);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteNonQueryAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteNonQueryAsync Parameters:
@paramInt INT = 2
@paramStr NVARCHAR(4) = N'text'
@paramDate DATETIME = '2016-01-02T03:04:05.660'"), Times.Once);
        }

        [TestMethod]
        public void ExecuteNonQueryWithParameter_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("@param", 2);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteNonQuery();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteNonQuery Parameters:
@param INT = 2"), Times.Once);
        }

        [TestMethod]
        public void ExecuteNonQueryWithParameters_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    var date = new DateTime(2016, 1, 2, 3, 4, 5).AddMilliseconds(660);
                    command.Parameters.AddWithValue("@paramInt", 2);
                    command.Parameters.AddWithValue("@paramStr", "text");
                    command.Parameters.AddWithValue("@paramDate", date);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteNonQuery();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteNonQuery Parameters:
@paramInt INT = 2
@paramStr NVARCHAR(4) = N'text'
@paramDate DATETIME = '2016-01-02T03:04:05.660'"), Times.Once);
        }

        [TestMethod]
        public void ExecuteReader_DebugCompletedMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteReader();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteReader Completed{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public void ExecuteReader_DebugStartingMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteReader();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteReader Starting{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public void ExecuteReader_ErrorMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(WrongQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        commandwrapper.ExecuteReader();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("ExecuteReader Error occured", exception), Times.Once);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        [Ignore]
        public void ExecuteReader_TraceResultShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteReader();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteReader Result:
-1"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteReaderAsync_DebugCompletedMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteReaderAsync Completed{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteReaderAsync_DebugStartingMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteReaderAsync Starting{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteReaderAsync_ErrorMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(WrongQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("ExecuteReaderAsync Error occured", exception), Times.Once);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        [Ignore]
        public async Task ExecuteReaderAsync_TraceResultShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteReaderAsync Result:
-1"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteReaderAsyncWithParameter_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("@param", 2);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteReaderAsync Parameters:
@param INT = 2"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteReaderAsyncWithParameters_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    var date = new DateTime(2016, 1, 2, 3, 4, 5).AddMilliseconds(660);
                    command.Parameters.AddWithValue("@paramInt", 2);
                    command.Parameters.AddWithValue("@paramStr", "text");
                    command.Parameters.AddWithValue("@paramDate", date);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteReaderAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteReaderAsync Parameters:
@paramInt INT = 2
@paramStr NVARCHAR(4) = N'text'
@paramDate DATETIME = '2016-01-02T03:04:05.660'"), Times.Once);
        }

        [TestMethod]
        public void ExecuteReaderWithParameter_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("@param", 2);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteReader();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteReader Parameters:
@param INT = 2"), Times.Once);
        }

        [TestMethod]
        public void ExecuteReaderWithParameters_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    var date = new DateTime(2016, 1, 2, 3, 4, 5).AddMilliseconds(660);
                    command.Parameters.AddWithValue("@paramInt", 2);
                    command.Parameters.AddWithValue("@paramStr", "text");
                    command.Parameters.AddWithValue("@paramDate", date);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteReader();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteReader Parameters:
@paramInt INT = 2
@paramStr NVARCHAR(4) = N'text'
@paramDate DATETIME = '2016-01-02T03:04:05.660'"), Times.Once);
        }

        [TestMethod]
        public void ExecuteScalar_DebugCompletedMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteScalar();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteScalar Completed{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public void ExecuteScalar_DebugStartingMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteScalar();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteScalar Starting{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public void ExecuteScalar_ErrorMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(WrongQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    QAssert.ShouldThrow<SqlConnectorException>(
                    () =>
                    {
                        commandwrapper.ExecuteScalar();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("ExecuteScalar Error occured", exception), Times.Once);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public void ExecuteScalar_TraceResultShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteScalar();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteScalar Result:
3"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteScalarAsync_DebugCompletedMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteScalarAsync Completed{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteScalarAsync_DebugStartingMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    }
                }
            }

            _logMoq.Verify(x => x.Debug($"ExecuteScalarAsync Starting{System.Environment.NewLine}{Query}"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteScalarAsync_ErrorMessageShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(WrongQuery, connection))
                {
                    var commandwrapper = new SqlCommandWrapper(command);

                    await QAssert.ShouldThrowAsync<SqlConnectorException>(
                    async () =>
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    },
                    exception =>
                    {
                        _logMoq.Verify(x => x.Error("ExecuteScalarAsync Error occured", exception), Times.Once);
                        return true;
                    });
                }
            }
        }

        [TestMethod]
        public async Task ExecuteScalarAsync_TraceResultShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteScalarAsync Result:
3"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteScalarAsyncWithParameter_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("@param", 2);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteScalarAsync Parameters:
@param INT = 2"), Times.Once);
        }

        [TestMethod]
        public async Task ExecuteScalarAsyncWithParameters_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    var date = new DateTime(2016, 1, 2, 3, 4, 5).AddMilliseconds(660);
                    command.Parameters.AddWithValue("@paramInt", 2);
                    command.Parameters.AddWithValue("@paramStr", "text");
                    command.Parameters.AddWithValue("@paramDate", date);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        await commandwrapper.ExecuteScalarAsync();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteScalarAsync Parameters:
@paramInt INT = 2
@paramStr NVARCHAR(4) = N'text'
@paramDate DATETIME = '2016-01-02T03:04:05.660'"), Times.Once);
        }

        [TestMethod]
        public void ExecuteScalarWithParameter_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    command.Parameters.AddWithValue("@param", 2);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteScalar();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteScalar Parameters:
@param INT = 2"), Times.Once);
        }

        [TestMethod]
        public void ExecuteScalarWithParameters_TraceParametersShouldBeLogged()
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(Query, connection))
                {
                    var date = new DateTime(2016, 1, 2, 3, 4, 5).AddMilliseconds(660);
                    command.Parameters.AddWithValue("@paramInt", 2);
                    command.Parameters.AddWithValue("@paramStr", "text");
                    command.Parameters.AddWithValue("@paramDate", date);
                    using (var commandwrapper = new SqlCommandWrapper(command))
                    {
                        commandwrapper.ExecuteScalar();
                    }
                }
            }

            _logMoq.Verify(
                x => x.Trace(@"ExecuteScalar Parameters:
@paramInt INT = 2
@paramStr NVARCHAR(4) = N'text'
@paramDate DATETIME = '2016-01-02T03:04:05.660'"), Times.Once);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var logFactoryAdapterMoq = new Mock<ILoggerFactoryAdapter>();
            _logMoq = new Mock<ILog>();
            _logMoq.Setup(x => x.IsTraceEnabled).Returns(true);
            _logMoq.Setup(x => x.IsDebugEnabled).Returns(true);
            _logMoq.Setup(x => x.IsInfoEnabled).Returns(true);
            _logMoq.Setup(x => x.IsWarnEnabled).Returns(true);
            _logMoq.Setup(x => x.IsErrorEnabled).Returns(true);
            logFactoryAdapterMoq.Setup(x => x.GetLogger(It.IsAny<Type>())).Returns(new Mock<ILog>().Object);
            logFactoryAdapterMoq.Setup(x => x.GetLogger(typeof(SqlCommandWrapper))).Returns(_logMoq.Object);
            LogManager.Adapter = logFactoryAdapterMoq.Object;
        }
    }
}