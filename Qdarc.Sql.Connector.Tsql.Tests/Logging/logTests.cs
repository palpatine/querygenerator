﻿using System.Configuration;
using Common.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Sql.Connector.SqlClient;

namespace Qdarc.Sql.Connector.SqlClient.Tests.Logging
{
    [TestClass]
    public class LogTests
    {
        [TestMethod]
        [Ignore]
        public void OpenConnectionToDatabase_ConnectionShouldBeOpen()
        {
            var configurationProvider = ConfigurationManager.ConnectionStrings["AzureAdventureWorks2012"];

            var logFactoryAdapterMoq = new Mock<ILoggerFactoryAdapter>();
            var logMoq = new Mock<ILog>(MockBehavior.Strict);
            logMoq.Setup(x => x.Debug("Debug"));
            logFactoryAdapterMoq.Setup(x => x.GetLogger(typeof(SqlConnectionWrapper))).Returns(logMoq.Object);

            ////var properties = new NameValueCollection();
            ////properties["level"] = "ALL";
            ////properties["showLogName"] = "true";
            ////properties["showDataTime"] = "true";
            ////properties["dateTimeFormat"] = "yyyy/MM/dd HH:mm:ss:fff";

            ////LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter(properties);
            LogManager.Adapter = logFactoryAdapterMoq.Object;

            using (var connection = new SqlConnectionWrapper(configurationProvider.ConnectionString))
            {
                connection.State.ShouldBeEqual(ConnectionState.Open);
            }
        }
    }
}
