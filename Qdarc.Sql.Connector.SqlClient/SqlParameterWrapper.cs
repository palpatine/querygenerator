using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Sql.Connector.SqlClient
{
    public class SqlParameterWrapper : ISqlParameter
    {
        public SqlParameterWrapper(SqlParameter sqlParameter)
        {
            SqlParameter = sqlParameter;
        }

        public string Name
        {
            get => SqlParameter.ParameterName;
            set => SqlParameter.ParameterName = value;
        }

        public ISqlType Type
        {
            get
            {
                if (SqlParameter.SqlDbType == SqlDbType.Structured)
                {
                    return new TypeReference(SqlParameter.TypeName);
                }
                else
                {
                    var typeName = SqlParameter.SqlDbType.GetTsqlTypeName();

                    var isNullable = SqlParameter.IsNullable;
                    var size = SqlParameter.Size;
                    var precision = SqlParameter.Precision;
                    var scale = SqlParameter.Scale;

                    if (TsqlNativeTypesNames.HasCapacity(typeName))
                    {
                        return new TsqlType(typeName, isNullable, size);
                    }

                    if (TsqlNativeTypesNames.HasPrecisionAndScale(typeName))
                    {
                        return new TsqlType(typeName, isNullable, precision, scale);
                    }

                    return new TsqlType(typeName, isNullable);
                }
            }
            set
            {
                if (value is TsqlType tsqlType)
                {
                    SqlParameter.SqlDbType = tsqlType.GetSqlDbType();
                    SqlParameter.IsNullable = tsqlType.IsNullable;
                    SqlParameter.Size = tsqlType.Capacity ?? 0;
                    SqlParameter.Precision = tsqlType.Precision ?? 0;
                    SqlParameter.Scale = tsqlType.Scale ?? 0;

                    return;
                }

                var customType = value as TypeReference;

                if (customType == null)
                {
                    throw new InvalidOperationException(
                        $"{typeof(TsqlType).Name} or {typeof(TypeReference).Name} expected");
                }

                SqlParameter.SqlDbType = SqlDbType.Structured;
                SqlParameter.TypeName = customType.Name;
            }
        }

        public object Value
        {
            get => SqlParameter.Value == DBNull.Value ? null : SqlParameter.Value;
            set => SqlParameter.Value = value ?? DBNull.Value;
        }

        internal SqlParameter SqlParameter { get; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")
        ]
        public override string ToString()
        {
            if (Value == null)
            {
                return "NULL";
            }

            if (SqlParameter.SqlDbType == SqlDbType.Structured)
            {
                return $"<set:{SqlParameter.TypeName}>";
            }

            var typeName = SqlParameter.SqlDbType.GetTsqlTypeName();

            string stringValue;
            switch (typeName)
            {
                case TsqlNativeTypesNames.Char:
                case TsqlNativeTypesNames.VarChar:
                case TsqlNativeTypesNames.Text:
                case TsqlNativeTypesNames.UniqueIdentifier:
                    stringValue = $"'{Value}'";
                    break;

                case TsqlNativeTypesNames.NChar:
                case TsqlNativeTypesNames.NVarChar:
                case TsqlNativeTypesNames.NText:
                case TsqlNativeTypesNames.Xml:
                    stringValue = $"N'{Value}'";
                    break;

                case TsqlNativeTypesNames.Binary:
                case TsqlNativeTypesNames.VarBinary:
                case TsqlNativeTypesNames.Image:
                    stringValue = $"0x{Value}";
                    break;

                case TsqlNativeTypesNames.Date:
                case TsqlNativeTypesNames.DateTime:
                case TsqlNativeTypesNames.DateTime2:
                case TsqlNativeTypesNames.SmallDateTime:
                case TsqlNativeTypesNames.Time:
                case TsqlNativeTypesNames.DateTimeOffset:
                    stringValue =
                        $"'{((DateTime)Value).ToString("yyyy-MM-ddTHH:mm:ss.fff", CultureInfo.InvariantCulture)}'";
                    break;

                case TsqlNativeTypesNames.Decimal:
                case TsqlNativeTypesNames.Numeric:
                    stringValue = $"'{((decimal)Value).ToString("G", CultureInfo.InvariantCulture)}'";
                    break;

                default:
                    stringValue = Value.ToString();
                    break;
            }

            return $"{Name} {Type} = {stringValue}";
        }
    }
}