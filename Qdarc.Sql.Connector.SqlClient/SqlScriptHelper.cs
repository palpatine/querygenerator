﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Qdarc.Sql.Connector.SqlClient
{
    internal static class SqlScriptHelper
    {
        private const string MultilineCommentPattern =
            @"/\*(?>(?:(?!\*/|/\*).)*)(?>(?:/\*(?>(?:(?!\*/|/\*).)*)\*/(?>(?:(?!\*/|/\*).)*))*).*?\*/";

        private const string SingleLineCommentPattern =
            @"--.*?\r?(?:\n|$)";

        internal static string RemoveCommentsFromSqlScript(string sqlScript)
        {
            const RegexOptions regexOptions = RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace;

            var result = Regex.Replace(sqlScript ?? string.Empty, $@"{MultilineCommentPattern}|{SingleLineCommentPattern}", string.Empty, regexOptions);

            return result;
        }

        internal static IReadOnlyCollection<string> SplitSqlScriptByGoStatement(string sqlScript)
        {
            const RegexOptions regexOptions = RegexOptions.Singleline | RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace;

            var regExFindGo = $@"
(?<QueryBefore>.*?
\r?\n
[ \t]*
(?>{MultilineCommentPattern})*)
[ \t]*GO(?:[ \t]+(?<count>\d+))?[ \t]*
(?<AfterComment>(?:{MultilineCommentPattern})*
[ \t]*
(?:$|\r?\n|(?:{SingleLineCommentPattern})))";

            var result = new List<string>();
            var matchCollection = Regex.Matches($"{sqlScript}{Environment.NewLine}GO", regExFindGo, regexOptions);
            string afterComment = null;
            foreach (Match item in matchCollection)
            {
                var queryBefore = afterComment + item.Groups["QueryBefore"].Value;
                var count = item.Groups["count"].Success ? int.Parse(item.Groups["count"].Value) : 1;

                if (!string.IsNullOrWhiteSpace(queryBefore))
                {
                    for (var i = 0; i < count; i++)
                    {
                        result.Add(queryBefore.Trim());
                    }
                }

                afterComment = item.Groups["AfterComment"].Value;
            }

            return result;
        }
    }
}