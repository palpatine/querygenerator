using System;
using System.Data.SqlClient;
using Common.Logging;
using Qdarc.Sql.Connector.SqlClient.Helpers;
using Qdarc.Sql.Connector.SqlClient.Properties;

namespace Qdarc.Sql.Connector.SqlClient
{
    public class SqlTransactionWrapper : ISqlTransaction
    {
        private readonly ISqlConnectorExceptionProcessor _sqlExceptionProcessor;
        private readonly ILog _log = LogManager.GetLogger<SqlTransactionWrapper>();
        private readonly SqlTransaction _transaction;

        public SqlTransactionWrapper(
            SqlTransaction transaction,
            ISqlConnectorExceptionProcessor sqlExceptionProcessor = null)
        {
            _transaction = transaction;
            _sqlExceptionProcessor = sqlExceptionProcessor;
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
                _log.DebugMethodCompleted();
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(Resources.ErrorOccuredWhileCommitingTransaction, exception);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        public void Rollback()
        {
            try
            {
                _transaction.Rollback();
                _log.DebugMethodCompleted();
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(Resources.ErrorOccuredWhileRollbackingTransaction, exception);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }
    }
}