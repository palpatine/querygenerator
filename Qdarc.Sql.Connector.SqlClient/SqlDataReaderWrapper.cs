using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Qdarc.Sql.Connector.SqlClient.Properties;

namespace Qdarc.Sql.Connector.SqlClient
{
    public sealed class SqlDataReaderWrapper : ISqlDataReader
    {
        private readonly string _commandText;
        private readonly SqlDataReader _dataReader;
        private readonly ISqlConnectorExceptionProcessor _sqlExceptionProcessor;

        public SqlDataReaderWrapper(
            SqlDataReader sqlDataReader,
            string commandText,
            ISqlConnectorExceptionProcessor sqlExceptionProcessor = null)
        {
            _dataReader = sqlDataReader;
            _commandText = commandText;
            _sqlExceptionProcessor = sqlExceptionProcessor;
        }

        public object this[int columnId] => ProcessValue(_dataReader[columnId]);

        public object this[string name] => ProcessValue(_dataReader[name]);

        public void Dispose()
        {
            _dataReader.Dispose();
        }

        public bool Read()
        {
            try
            {
                return _dataReader.Read();
            }
            catch (Exception sqlException)
            {
                var exception = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    sqlException,
                    _commandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(exception);
                throw exception;
            }
        }

        public async Task<bool> ReadAsync()
        {
            try
            {
                return await _dataReader.ReadAsync();
            }
            catch (Exception sqlException)
            {
                var exception = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    sqlException,
                    _commandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(exception);
                throw exception;
            }
        }

        public async Task<bool> NextResultAsync()
        {
            try
            {
                return await _dataReader.NextResultAsync();
            }
            catch (Exception sqlException)
            {
                var exception = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    sqlException,
                    _commandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(exception);
                throw exception;
            }
        }

        public bool NextResult()
        {
            try
            {
                return _dataReader.NextResult();
            }
            catch (Exception sqlException)
            {
                var exception = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    sqlException,
                    _commandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(exception);
                throw exception;
            }
        }

        private static object ProcessValue(object value)
        {
            return value == DBNull.Value ? null : value;
        }
    }
}