﻿using System;
using System.Threading.Tasks;

namespace Qdarc.Sql.Connector.SqlClient
{
    public interface ITSqlConnection : ISqlConnection
    {
        /// <summary>
        /// Creates the SQL procedure command.
        /// </summary>
        /// <param name="procedureName">Name of the procedure.</param>
        /// <returns>An object representing the new SqlCommand.</returns>
        /// <exception cref="System.ArgumentNullException">procedureName</exception>
        /// <exception cref="ObjectDisposedException">Connection is Disposed</exception>
        ISqlCommand CreateSqlProcedureCommand(string procedureName);

        /// <summary>
        /// Executes the script.
        /// </summary>
        /// <param name="script">The script.</param>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        void ExecuteScript(string script);

        /// <summary>
        /// Executes the script asynchronous.
        /// </summary>
        /// <param name="script">The script.</param>
        /// <returns>Awaitable object.</returns>
        /// <exception cref="SqlConnectorException">
        /// SQL Server returned an error while executing the command text.
        /// </exception>
        Task ExecuteScriptAsync(string script);
    }
}