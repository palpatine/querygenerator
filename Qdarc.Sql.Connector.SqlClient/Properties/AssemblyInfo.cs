﻿using System.Runtime.CompilerServices;

#if DEBUG
[assembly: InternalsVisibleTo("Qdarc.Sql.Connector.SqlClient.Tests")]
#endif