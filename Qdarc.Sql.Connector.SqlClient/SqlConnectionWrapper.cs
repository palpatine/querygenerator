using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Common.Logging;
using Qdarc.Sql.Connector.SqlClient.Helpers;
using Qdarc.Sql.Connector.SqlClient.Properties;

namespace Qdarc.Sql.Connector.SqlClient
{
    public sealed class SqlConnectionWrapper : ITSqlConnection
    {
        private readonly SqlConnection _connection;
        private readonly ISqlConnectorExceptionProcessor _sqlExceptionProcessor;
        private readonly ILog _log = LogManager.GetLogger<SqlConnectionWrapper>();
        private bool _isDisposed;
        private SqlTransaction _transaction;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlConnectionWrapper"/> class, adn opens connection.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <param name="sqlExceptionProcessor">The SQL exception processor.</param>
        /// <exception cref="SqlConnectorException">
        /// A connection-level error occurred while opening the connection.
        /// </exception>
        public SqlConnectionWrapper(
            string connectionString,
            ISqlConnectorExceptionProcessor sqlExceptionProcessor = null)
        {
            _sqlExceptionProcessor = sqlExceptionProcessor;
            _connection = new SqlConnection(connectionString);
            try
            {
                _log.DebugMethodStarting("Opening Connection");
                _log.TraceMessage($"{nameof(connectionString)}:{Environment.NewLine}{connectionString}");
                _connection.Open();
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(Resources.ErrorOccuredWhileOpeningConnection, exception);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        public ConnectionState State => ConvertConnectionStateType(_connection.State);

        [ExcludeFromCodeCoverage]
        public bool Trace { get; set; }

        public ISqlTransaction BeginTransaction()
        {
            if (_isDisposed)
            {
                var exception = new ObjectDisposedException(GetType().Name);
                _log.ErrorMethod(exception);
                throw exception;
            }

            try
            {
                _log.DebugMethodStarting();
                _transaction = _connection.BeginTransaction();
                return new SqlTransactionWrapper(_transaction, _sqlExceptionProcessor);
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(Resources.ErrorOccuredWhileCreatingTransaction, exception);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        public ISqlCommand CreateSqlCommand(string command)
        {
            return CreateSqlCommand(command, CommandType.Text);
        }

        public ISqlCommand CreateSqlProcedureCommand(string procedureName)
        {
            if (string.IsNullOrWhiteSpace(procedureName))
            {
                var exception = new ArgumentNullException(nameof(procedureName));
                _log.ErrorMethod(exception);
                throw exception;
            }

            return CreateSqlCommand(procedureName, CommandType.StoredProcedure);
        }

        public void Dispose()
        {
            _connection.Close();
            _log.DebugMethodCompleted("Connection Closed");
            _connection.Dispose();
            _isDisposed = true;
        }

        public void ExecuteScript(string script)
        {
            try
            {
                _log.DebugMethodStarting();
                var queries = SqlScriptHelper.SplitSqlScriptByGoStatement(script);
                var commands = queries.Select(CreateSqlCommand);
                foreach (var command in commands)
                {
                    command.ExecuteNonQuery();
                }

                _log.DebugMethodCompleted();
            }
            catch (Exception sqlException)
            {
                var exception = new SqlConnectorException(Resources.ErrorOccuredWhileExecutingScript, sqlException);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(exception);
                throw exception;
            }
        }

        public async Task ExecuteScriptAsync(string script)
        {
            try
            {
                _log.DebugMethodStarting();
                var queries = SqlScriptHelper.SplitSqlScriptByGoStatement(script);
                var commands = queries.Select(CreateSqlCommand);
                foreach (var command in commands)
                {
                    await command.ExecuteNonQueryAsync();
                }

                _log.DebugMethodCompleted();
            }
            catch (Exception sqlException)
            {
                var exception = new SqlConnectorException(Resources.ErrorOccuredWhileExecutingScript, sqlException);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(exception);
                throw exception;
            }
        }

        internal static ConnectionState ConvertConnectionStateType(System.Data.ConnectionState state)
        {
            switch (state)
            {
                case System.Data.ConnectionState.Closed:
                    return ConnectionState.Closed;

                case System.Data.ConnectionState.Open:
                    return ConnectionState.Open;

                case System.Data.ConnectionState.Connecting:
                    return ConnectionState.Connecting;

                case System.Data.ConnectionState.Executing:
                    return ConnectionState.Executing;

                case System.Data.ConnectionState.Fetching:
                    return ConnectionState.Fetching;

                case System.Data.ConnectionState.Broken:
                    return ConnectionState.Broken;

                default:
                    throw new InvalidOperationException();
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
        private ISqlCommand CreateSqlCommand(
                            string command,
                            CommandType commandType)
        {
            if (_isDisposed)
            {
                var exception = new ObjectDisposedException(GetType().Name);
                _log.ErrorMethod(exception);
                throw exception;
            }

            var sqlCommand = new SqlCommand(
                command,
                _connection,
                _transaction)
            {
                CommandType = commandType
            };
            return new SqlCommandWrapper(sqlCommand, _sqlExceptionProcessor);
        }
    }
}