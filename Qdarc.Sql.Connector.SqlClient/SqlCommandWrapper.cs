using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Common.Logging;
using Qdarc.Sql.Connector.SqlClient.Helpers;
using Qdarc.Sql.Connector.SqlClient.Properties;

namespace Qdarc.Sql.Connector.SqlClient
{
    public sealed class SqlCommandWrapper : ISqlCommand
    {
        private readonly SqlCommand _sqlCommand;
        private readonly ISqlConnectorExceptionProcessor _sqlExceptionProcessor;
        private readonly ILog _log = LogManager.GetLogger<SqlCommandWrapper>();

        public SqlCommandWrapper(
            SqlCommand sqlCommand,
            ISqlConnectorExceptionProcessor sqlExceptionProcessor = null)
        {
            _sqlCommand = sqlCommand;
            _sqlExceptionProcessor = sqlExceptionProcessor;
        }

        public IEnumerable<ISqlParameter> Parameters => _sqlCommand.Parameters.Cast<SqlParameter>().Select(x => new SqlParameterWrapper(x));

        public ISqlParameter AddParameter()
        {
            var parameter = new SqlParameter
            {
                Value = DBNull.Value
            };
            parameter = _sqlCommand.Parameters.Add(parameter);
            return new SqlParameterWrapper(parameter);
        }

        public void Dispose()
        {
            _sqlCommand.Dispose();
        }

        public int ExecuteNonQuery()
        {
            try
            {
                _log.DebugMethodStarting(_sqlCommand.CommandText);
                _log.TraceParameters(Parameters);
                var result = _sqlCommand.ExecuteNonQuery();
                _log.DebugMethodCompleted(_sqlCommand.CommandText);
                _log.TraceResult(result);
                return result;
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    exception,
                    _sqlCommand.CommandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        public async Task<int> ExecuteNonQueryAsync()
        {
            try
            {
                _log.DebugMethodStarting(_sqlCommand.CommandText);
                _log.TraceParameters(Parameters);
                var result = await _sqlCommand.ExecuteNonQueryAsync();
                _log.DebugMethodCompleted(_sqlCommand.CommandText);
                _log.TraceResult(result);
                return result;
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    exception,
                    _sqlCommand.CommandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Value returned to user")]
        public ISqlDataReader ExecuteReader()
        {
            try
            {
                _log.DebugMethodStarting(_sqlCommand.CommandText);
                _log.TraceParameters(Parameters);
                var result = new SqlDataReaderWrapper(
                    _sqlCommand.ExecuteReader(),
                    _sqlCommand.CommandText,
                    _sqlExceptionProcessor);
                _log.DebugMethodCompleted(_sqlCommand.CommandText);
                return result;
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    exception,
                    _sqlCommand.CommandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Value returned to user")]
        public async Task<ISqlDataReader> ExecuteReaderAsync()
        {
            try
            {
                _log.DebugMethodStarting(_sqlCommand.CommandText);
                _log.TraceParameters(Parameters);
                var result = new SqlDataReaderWrapper(
                    await _sqlCommand.ExecuteReaderAsync(),
                    _sqlCommand.CommandText,
                    _sqlExceptionProcessor);
                _log.DebugMethodCompleted(_sqlCommand.CommandText);
                return result;
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    exception,
                    _sqlCommand.CommandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        public object ExecuteScalar()
        {
            try
            {
                _log.DebugMethodStarting(_sqlCommand.CommandText);
                _log.TraceParameters(Parameters);
                var result = _sqlCommand.ExecuteScalar();
                _log.DebugMethodCompleted(_sqlCommand.CommandText);
                _log.TraceResult(result);
                return result;
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    exception,
                    _sqlCommand.CommandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }

        public async Task<object> ExecuteScalarAsync()
        {
            try
            {
                _log.DebugMethodStarting(_sqlCommand.CommandText);
                _log.TraceParameters(Parameters);
                var result = await _sqlCommand.ExecuteScalarAsync();
                _log.DebugMethodCompleted(_sqlCommand.CommandText);
                _log.TraceResult(result);
                return result;
            }
            catch (Exception exception)
            {
                var sqlException = new SqlConnectorException(
                    Resources.ErrorOccuredWhileExecutingQuery,
                    exception,
                    _sqlCommand.CommandText);
                _sqlExceptionProcessor?.ProcessSqlConnectorException(sqlException);
                _log.ErrorMethod(sqlException);
                throw sqlException;
            }
        }
    }
}