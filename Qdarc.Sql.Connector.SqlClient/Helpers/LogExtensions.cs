﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Common.Logging;

namespace Qdarc.Sql.Connector.SqlClient.Helpers
{
    public static class LogExtensions
    {
        public static void DebugMethodCompleted(
            this ILog log,
            string logEntry = null,
            [CallerMemberName] string caller = null)
        {
            if (!log.IsDebugEnabled)
            {
                return;
            }

            var msg = string.IsNullOrEmpty(logEntry) ? null : $"{Environment.NewLine}{logEntry}";
            log.Debug($"{caller} Completed{msg}");
        }

        public static void DebugMethodStarting(
                    this ILog log,
                    string logEntry = null,
                    [CallerMemberName] string caller = null)
        {
            if (!log.IsDebugEnabled)
            {
                return;
            }

            var msg = string.IsNullOrEmpty(logEntry) ? null : $"{Environment.NewLine}{logEntry}";
            log.Debug($"{caller} Starting{msg}");
        }

        public static void ErrorMethod(
            this ILog log,
            Exception exception,
            [CallerMemberName] string caller = null)
        {
            if (!log.IsErrorEnabled)
            {
                return;
            }

            log.Error($"{caller} Error occured", exception);
        }

        public static void TraceParameters(
            this ILog log,
            IEnumerable<ISqlParameter> parametersCollection,
            [CallerMemberName] string caller = null)
        {
            if (!log.IsTraceEnabled || !parametersCollection.Any())
            {
                return;
            }

            var parameters = parametersCollection.Select(x => x.ToString()).ToList();
            var message = string.Join(Environment.NewLine, parameters);

            log.Trace($"{caller} Parameters:{Environment.NewLine}{message}");
        }

        public static void TraceMessage(
            this ILog log,
            string logEntry,
            [CallerMemberName] string caller = null)
        {
            if (!log.IsTraceEnabled)
            {
                return;
            }

            log.Trace($"{caller} {logEntry}");
        }

        public static void TraceResult(
            this ILog log,
            object result,
            [CallerMemberName] string caller = null)
        {
            if (!log.IsTraceEnabled)
            {
                return;
            }

            var message = result;

            log.Trace($"{caller} Result:{Environment.NewLine}{message}");
        }
    }
}