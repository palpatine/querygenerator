using System.Collections.Generic;
using System.Data;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Sql.Connector.SqlClient
{
    public static class TsqlTypeExtensions
    {
        private static readonly Dictionary<string, SqlDbType> NamesToTypesMap = new Dictionary<string, SqlDbType>
             {
                 { TsqlNativeTypesNames.DateTimeOffset, SqlDbType.DateTimeOffset },
                 { TsqlNativeTypesNames.DateTime2, SqlDbType.DateTime2 },
                 { TsqlNativeTypesNames.DateTime, SqlDbType.DateTime },
                 { TsqlNativeTypesNames.SmallDateTime, SqlDbType.SmallDateTime },
                 { TsqlNativeTypesNames.Date, SqlDbType.Date },
                 { TsqlNativeTypesNames.Time, SqlDbType.Time },
                 { TsqlNativeTypesNames.Float, SqlDbType.Float },
                 { TsqlNativeTypesNames.Real, SqlDbType.Real },
                 { TsqlNativeTypesNames.Decimal, SqlDbType.Decimal },
                 { TsqlNativeTypesNames.Money, SqlDbType.Money },
                 { TsqlNativeTypesNames.SmallMoney, SqlDbType.SmallMoney },
                 { TsqlNativeTypesNames.BigInt, SqlDbType.BigInt },
                 { TsqlNativeTypesNames.Int, SqlDbType.Int },
                 { TsqlNativeTypesNames.SmallInt, SqlDbType.SmallInt },
                 { TsqlNativeTypesNames.TinyInt, SqlDbType.TinyInt },
                 { TsqlNativeTypesNames.Bit, SqlDbType.Bit },
                 { TsqlNativeTypesNames.NText, SqlDbType.NText },
                 { TsqlNativeTypesNames.Text, SqlDbType.Text },
                 { TsqlNativeTypesNames.Image, SqlDbType.Image },
                 { TsqlNativeTypesNames.Timestamp, SqlDbType.Timestamp },
                 { TsqlNativeTypesNames.UniqueIdentifier, SqlDbType.UniqueIdentifier },
                 { TsqlNativeTypesNames.NVarChar, SqlDbType.NVarChar },
                 { TsqlNativeTypesNames.NChar, SqlDbType.NChar },
                 { TsqlNativeTypesNames.VarChar, SqlDbType.VarChar },
                 { TsqlNativeTypesNames.Char, SqlDbType.Char },
                 { TsqlNativeTypesNames.VarBinary, SqlDbType.VarBinary },
                 { TsqlNativeTypesNames.Binary, SqlDbType.Binary },
                 { TsqlNativeTypesNames.Numeric, SqlDbType.Decimal },
                 { TsqlNativeTypesNames.Xml, SqlDbType.Xml }
             };

        private static readonly Dictionary<SqlDbType, string> TypesToNamesMap = new Dictionary<SqlDbType, string>
             {
                 { SqlDbType.DateTimeOffset, TsqlNativeTypesNames.DateTimeOffset },
                 { SqlDbType.DateTime2, TsqlNativeTypesNames.DateTime2 },
                 { SqlDbType.DateTime, TsqlNativeTypesNames.DateTime },
                 { SqlDbType.SmallDateTime, TsqlNativeTypesNames.SmallDateTime },
                 { SqlDbType.Date, TsqlNativeTypesNames.Date },
                 { SqlDbType.Time, TsqlNativeTypesNames.Time },
                 { SqlDbType.Float, TsqlNativeTypesNames.Float },
                 { SqlDbType.Real, TsqlNativeTypesNames.Real },
                 { SqlDbType.Decimal, TsqlNativeTypesNames.Decimal },
                 { SqlDbType.Money, TsqlNativeTypesNames.Money },
                 { SqlDbType.SmallMoney, TsqlNativeTypesNames.SmallMoney },
                 { SqlDbType.BigInt, TsqlNativeTypesNames.BigInt },
                 { SqlDbType.Int, TsqlNativeTypesNames.Int },
                 { SqlDbType.SmallInt, TsqlNativeTypesNames.SmallInt },
                 { SqlDbType.TinyInt, TsqlNativeTypesNames.TinyInt },
                 { SqlDbType.Bit, TsqlNativeTypesNames.Bit },
                 { SqlDbType.NText, TsqlNativeTypesNames.NText },
                 { SqlDbType.Text, TsqlNativeTypesNames.Text },
                 { SqlDbType.Image, TsqlNativeTypesNames.Image },
                 { SqlDbType.Timestamp, TsqlNativeTypesNames.Timestamp },
                 { SqlDbType.UniqueIdentifier, TsqlNativeTypesNames.UniqueIdentifier },
                 { SqlDbType.NVarChar, TsqlNativeTypesNames.NVarChar },
                 { SqlDbType.NChar, TsqlNativeTypesNames.NChar },
                 { SqlDbType.VarChar, TsqlNativeTypesNames.VarChar },
                 { SqlDbType.Char, TsqlNativeTypesNames.Char },
                 { SqlDbType.VarBinary, TsqlNativeTypesNames.VarBinary },
                 { SqlDbType.Binary, TsqlNativeTypesNames.Binary },
                 { SqlDbType.Xml, TsqlNativeTypesNames.Xml }
             };

        public static SqlDbType GetSqlDbType(this TsqlType type)
        {
            return NamesToTypesMap[type.Name];
        }

        public static string GetTsqlTypeName(this SqlDbType type)
        {
            return TypesToNamesMap[type];
        }
    }
}