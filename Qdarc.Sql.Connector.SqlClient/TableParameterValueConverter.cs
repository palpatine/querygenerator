﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SqlServer.Server;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Sql.Connector.SqlClient
{
    public class TableParameterValueConverter : ITableParameterValueConverter
    {
        private readonly IModelToSqlConvention _modelToSqlConvention;

        public TableParameterValueConverter(IModelToSqlConvention modelToSqlConvention)
        {
            _modelToSqlConvention = modelToSqlConvention;
        }

        public IEnumerable Convert(Type itemType, IEnumerable data)
        {
            var columns = _modelToSqlConvention
                .GetSelectableProperties(itemType);

            var udtColumns = new List<SqlMetaData>();

            foreach (var column in columns)
            {
                var columnName = _modelToSqlConvention.GetColumnName(column);
                var sqlType = (TsqlType)_modelToSqlConvention.GetSqlType(itemType, column);

                SqlMetaData metaData;
                if (sqlType.Capacity.HasValue)
                {
                    metaData = new SqlMetaData(
                        columnName,
                        sqlType.GetSqlDbType(),
                        sqlType.Capacity.Value);
                }
                else if (sqlType.Precision.HasValue)
                {
                    metaData = new SqlMetaData(
                        columnName,
                        sqlType.GetSqlDbType(),
                        sqlType.Precision.Value,
                        sqlType.Scale.Value);
                }
                else
                {
                    metaData = new SqlMetaData(
                        columnName,
                        sqlType.GetSqlDbType());
                }

                udtColumns.Add(metaData);
            }

            var records = new List<SqlDataRecord>();

            foreach (var element in data)
            {
                var record = new SqlDataRecord(udtColumns.ToArray());
                record.SetValues(columns.Select(column =>
                {
                    var value = column.GetValue(element);
                    if (value == null)
                    {
                        return DBNull.Value;
                    }

                    return value;
                }).ToArray());
                records.Add(record);
            }

            return records;
        }
    }
}