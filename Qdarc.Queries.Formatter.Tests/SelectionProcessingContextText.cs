﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Queries.Formatter.Tests
{
    [TestClass]
    public class SelectionProcessingContextText
    {
        [TestMethod]
        public void ShouldGatherPrefixesInOrderOfAddition()
        {
            var context = new SelectionProcessingContext();
            context.AppendPrefix("prefix1");
            context.AppendPrefix("prefix2");

            context.AliasPrefixes.AllCorrespondingElementsShouldBeEqual(
                new[] { "prefix1", "prefix2" });
        }

        [TestMethod]
        public void ShouldRemovePrefixIfContextIsDisposed()
        {
            var context = new SelectionProcessingContext();
            context.AppendPrefix("prefix1");
            context.AppendPrefix("prefix2").Dispose();

            context.AliasPrefixes.AllCorrespondingElementsShouldBeEqual(
                new[] { "prefix1" });
        }

        [TestMethod]
        public void ShouldRemovePreventContextCorruptionIfPrefixesAreDisposedInWrongOrder()
        {
            var context = new SelectionProcessingContext();
            var first = context.AppendPrefix("prefix1");
            context.AppendPrefix("prefix2");

            QAssert.ShouldThrow<InvalidOperationException>(() => first.Dispose());
        }
    }
}
