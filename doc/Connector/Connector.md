
#Logger severity Levels:
- Trace - Start of important method with parameters. The end of important method with result.
- Debug - Start / end of important method.
- Info  - For statistical purposes.
- Warn  - No configuration, default confiruration was used.
- Error - error was occured.
- Fatal - error was occured and stops application 	