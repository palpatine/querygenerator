﻿using System;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Linq.Tests.SqlAbstraction
{
    internal class SqlType : ISqlType
    {
        public string Name { get; }
    }
}