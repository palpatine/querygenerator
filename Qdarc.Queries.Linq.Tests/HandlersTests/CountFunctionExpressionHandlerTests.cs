﻿using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class CountFunctionExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldBeAbleToHandleCount()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            IDatabase database = null;
            Expression<Func<int>> expression = () => database.Count();
            handler.CanHandle(expression.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleCountDistinctWithInnerExpression()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            IDatabase database = null;
            Expression<Func<UserEntity, int>> expression = x => database.CountDistinct(x.ParentId);
            handler.CanHandle(expression.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleCountWithInnerExpression()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            IDatabase database = null;
            Expression<Func<UserEntity, int>> expression = x => database.Count(x.ParentId);
            handler.CanHandle(expression.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleCount()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            IDatabase database = null;
            Expression<Func<int>> expression = () => database.Count();
            var call = (MethodCallExpression)expression.Body;

            var result = handler.Handle(call);

            var countExpression = result.ShouldBeInstanceOf<SqlAsteriskCountExpression>();
            countExpression.ClrType.ShouldBeEqual(typeof(int));
            countExpression.Order.ShouldBeNull();
            countExpression.Partition.ShouldBeNull();
            countExpression.SqlType.ShouldBeEqual(intType.Object);
        }

        [TestMethod]
        public void ShouldHandleCountDistinct()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            var innerExpression = new Mock<ISqlValueExpression>().Object;
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            IDatabase database = null;
            Expression<Func<UserEntity, int>> expression = x => database.CountDistinct(x.FirstName);
            var call = (MethodCallExpression)expression.Body;
            context.Setup(x => x.VisitExpression(call.Arguments[1])).Returns(innerExpression);

            var result = handler.Handle(call);

            var countExpression = result.ShouldBeInstanceOf<SqlCountExpression>();
            countExpression.ClrType.ShouldBeEqual(typeof(int));
            countExpression.IsDistinct.ShouldBeTrue();
            countExpression.Order.ShouldBeNull();
            countExpression.Partition.ShouldBeNull();
            countExpression.Value.ShouldBeTheSameInstance(innerExpression);
            countExpression.SqlType.ShouldBeEqual(intType.Object);
        }

        [TestMethod]
        public void ShouldHandleCountWithInnerExpression()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            var innerExpression = new Mock<ISqlValueExpression>().Object;
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            IDatabase database = null;
            Expression<Func<UserEntity, int>> expression = x => database.Count(x.LastName);
            var call = (MethodCallExpression)expression.Body;
            context.Setup(x => x.VisitExpression(call.Arguments[1])).Returns(innerExpression);

            var result = handler.Handle(call);

            var countExpression = result.ShouldBeInstanceOf<SqlCountExpression>();
            countExpression.ClrType.ShouldBeEqual(typeof(int));
            countExpression.IsDistinct.ShouldBeFalse();
            countExpression.Order.ShouldBeNull();
            countExpression.Partition.ShouldBeNull();
            countExpression.Value.ShouldBeTheSameInstance(innerExpression);
            countExpression.SqlType.ShouldBeEqual(intType.Object);
        }

        [TestMethod]
        public void ShouldThrowOnCountWithInnerExpressionPointingToSource()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            var innerExpression = new Mock<INamedSource>().Object;
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            IDatabase database = null;
            Expression<Func<UserEntity, int>> expression = x => database.Count(x);
            var call = (MethodCallExpression)expression.Body;
            context.Setup(x => x.VisitExpression(call.Arguments[1])).Returns(innerExpression);

            QAssert.ShouldThrow<InvalidModelException>(() => handler.Handle(call));
        }

        [TestMethod]
        public void ShouldNotVisitFirstArgument()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountFunctionExpressionHandler();
            handler.AttachContext(context.Object);
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            IDatabase database = null;
            Expression<Func<UserEntity, int>> expression = x => database.Count(x.FirstName);
            var call = (MethodCallExpression)expression.Body;
            context.Setup(x => x.VisitExpression(call.Arguments[1]))
                .Returns(new Mock<ISqlValueExpression>().Object);

            handler.Handle(call);

            context.Verify(x => x.VisitExpression(call.Arguments[0]), Times.Never);
        }
    }
}