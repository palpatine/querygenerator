﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.ExpressionReplacer;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class UpdateStatementHandlerTests
    {
        private static readonly PropertyInfo UserEntityId =
            ExpressionExtensions.GetProperty((UserEntity x) => x.Id);

        private static readonly Expression<Func<UserEntity, UserEntity>> SingleSourceSetExpression =
            x => new UserEntity { Id = 9 };

        private static readonly Expression<Func<UserEntity, ContactEntity, UserEntity>> DoubleSourceSetExpression =
            (x, y) => new UserEntity { Id = 9 };

        private static readonly Expression<Func<UserEntity, ContactEntity, UserEntity>> DoubleSourceTargetSelector
            = (u, c) => u;

        private static readonly ConstantExpression SingleSourceInnerQuery = Expression.Constant(
            new Mock<ITableQuery<UserEntity>>().Object,
            typeof(ITableQuery<UserEntity>));

        private static readonly ConstantExpression DoubleSourceInnerQuery = Expression.Constant(
            new Mock<ITableQuery<Tuple<UserEntity, ContactEntity>>>().Object,
            typeof(ITableQuery<Tuple<UserEntity, ContactEntity>>));

        private static readonly MethodInfo SingleSourceUpdateMethod
            = ExpressionExtensions.GetMethod(() => TableQueryUpdate.Update<UserEntity>(null, null));

        private static readonly MethodInfo DoubleSourceUpdateMethod
            = ExpressionExtensions.GetMethod(() => TableQueryUpdate.Update<UserEntity, ContactEntity, UserEntity>(null, null, null));

        private static readonly Expression SingleSourceUpdateCallExpression
            = Expression.Call(null, SingleSourceUpdateMethod, SingleSourceInnerQuery, Expression.Quote(SingleSourceSetExpression));

        private static readonly Expression DoubleSourceUpdateCallExpression
            = Expression.Call(
                null,
                DoubleSourceUpdateMethod,
                DoubleSourceInnerQuery,
                Expression.Quote(DoubleSourceTargetSelector),
                Expression.Quote(DoubleSourceSetExpression));

        [TestMethod]
        public void ShouldSelectQueryAsTargetIfTargetSelectorIsNotProvided()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new UpdateStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var linqTableReference = new TableReference(new ElementName("name"), typeof(UserEntity));
            context.Setup(x => x.VisitExpression(SingleSourceInnerQuery))
                .Returns(linqTableReference);
            var projectionByMembersAssignment = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(ExpressionExtensions.GetConstructor(() => new UserEntity()), Enumerable.Empty<ISelectionItem>()),
                Enumerable.Empty<BoundSelectionItem>());
            context.Setup(x => x.VisitExpression(SingleSourceSetExpression.Body))
                .Returns(projectionByMembersAssignment);

            var result = handler.Handle(SingleSourceUpdateCallExpression);

            var update = result.ShouldBeInstanceOf<UpdateCommand>();
            update.Target.ShouldBeTheSameInstance(linqTableReference);
            update.Source.ShouldBeNull();
        }

        [TestMethod]
        public void ShouldProcessTargetSelector()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new UpdateStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var target = new Mock<ISource>().Object;
            var otherSource = new Mock<ISource>().Object;
            var source = new Mock<IMultisource>();
            source.Setup(x => x.Sources)
                .Returns(
                    new[]
                    {
                        target,
                        otherSource
                    });
            context.Setup(x => x.VisitExpression(DoubleSourceInnerQuery))
                .Returns(source.Object);
            context.Setup(x => x.VisitExpression(DoubleSourceTargetSelector.Body))
                .Returns(target);
            var projectionByMembersAssignment = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(ExpressionExtensions.GetConstructor(() => new UserEntity()), Enumerable.Empty<ISelectionItem>()),
                Enumerable.Empty<BoundSelectionItem>());
            context.Setup(x => x.VisitExpression(DoubleSourceSetExpression.Body))
                .Returns(projectionByMembersAssignment);

            var result = handler.Handle(DoubleSourceUpdateCallExpression);

            var update = result.ShouldBeInstanceOf<UpdateCommand>();
            update.Target.ShouldBeTheSameInstance(target);
            update.Source.ShouldBeTheSameInstance(source.Object);
        }

        [TestMethod]
        public void ShouldGetFilterFromQuery()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new UpdateStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var query = new SelectCommand(
                new Mock<ISource>().Object,
                filter: new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(SingleSourceInnerQuery))
                .Returns(query);
            var projectionByMembersAssignment = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(ExpressionExtensions.GetConstructor(() => new UserEntity()), Enumerable.Empty<ISelectionItem>()),
                Enumerable.Empty<BoundSelectionItem>());
            context.Setup(x => x.VisitExpression(SingleSourceSetExpression.Body))
                .Returns(projectionByMembersAssignment);

            var result = handler.Handle(SingleSourceUpdateCallExpression);

            var update = result.ShouldBeInstanceOf<UpdateCommand>();
            update.Filter.ShouldBeTheSameInstance(query.Filter);
            update.Target.ShouldBeTheSameInstance(query.Source);
        }

        [TestMethod]
        public void ShouldGetSourceFromQuery()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new UpdateStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var query = new SelectCommand(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(SingleSourceInnerQuery))
                .Returns(query);
            var projectionByMembersAssignment = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(ExpressionExtensions.GetConstructor(() => new UserEntity()), Enumerable.Empty<ISelectionItem>()),
                Enumerable.Empty<BoundSelectionItem>());
            context.Setup(x => x.VisitExpression(SingleSourceSetExpression.Body))
                .Returns(projectionByMembersAssignment);

            var result = handler.Handle(SingleSourceUpdateCallExpression);

            var update = result.ShouldBeInstanceOf<UpdateCommand>();
            update.Target.ShouldBeTheSameInstance(query.Source);
        }

        [TestMethod]
        public void ShouldGetUnwrapSourceFromQuery()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new UpdateStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var source = new Mock<ISource>().Object;
            var query = new SelectCommand(new AliasedSourceItem(source));
            context.Setup(x => x.VisitExpression(SingleSourceInnerQuery))
                .Returns(query);
            var projectionByMembersAssignment = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(ExpressionExtensions.GetConstructor(() => new UserEntity()), Enumerable.Empty<ISelectionItem>()),
                Enumerable.Empty<BoundSelectionItem>());
            context.Setup(x => x.VisitExpression(SingleSourceSetExpression.Body))
                .Returns(projectionByMembersAssignment);

            var result = handler.Handle(SingleSourceUpdateCallExpression);

            var update = result.ShouldBeInstanceOf<UpdateCommand>();
            update.Target.ShouldBeTheSameInstance(source);
        }

        [TestMethod]
        public void ShouldProcessBoundItemsOfProjectionByMembersAssignment()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new UpdateStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var linqTableReference = new TableReference(new ElementName("name"), typeof(UserEntity));
            context.Setup(x => x.VisitExpression(SingleSourceInnerQuery))
                .Returns(linqTableReference);
            var selectionItem = new Mock<ISqlValueExpression>();
            var projectionByMembersAssignment = new ProjectionByMembersAssignment(
                new ProjectionByConstructor(ExpressionExtensions.GetConstructor(() => new UserEntity()), Enumerable.Empty<ISelectionItem>()),
                new[] { new BoundSelectionItem(UserEntityId, selectionItem.Object) });
            context.Setup(x => x.VisitExpression(SingleSourceSetExpression.Body))
                .Returns(projectionByMembersAssignment);

            var result = handler.Handle(SingleSourceUpdateCallExpression);

            var update = result.ShouldBeInstanceOf<UpdateCommand>();
            update.Set.Single().ElementName[0].ShouldBeEqual("Id");
            update.Set.Single().Value.ShouldBeTheSameInstance(selectionItem.Object);
        }
    }
}
