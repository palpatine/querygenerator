﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class ProjectionExpressionHandlerTests
    {
        private readonly Expression<Func<UserEntity, int>> _expressionStub = x => x.Id;
        private Mock<IVisitationContext> _context;
        private ProjectionExpressionHandler _handler;
        private ISqlExpression _parsedSelectionList;
        private ISqlExpression _sourceSqlExpression;
        private ITableQuery<UserEntity> _sourceStub;

        [TestMethod]
        public void ShouldCreateSelectionFromSingleValueExpression()
        {
            _sourceSqlExpression = new SelectCommand();
            _parsedSelectionList = new Mock<ISqlValueExpression>().Object;
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Select<UserEntity, int>(null, null));
            var arguments = new[]
            {
                _sourceStub.Expression,
                Expression.Quote(_expressionStub)
            };
            var callExpression = Expression.Call(
                null,
                method,
                arguments);
            var sqlExpression = _handler.Handle(callExpression);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Selection.Elements.Count().ShouldBeEqual(1);
            select.Selection.Elements.Single().ShouldBeTheSameInstance(_parsedSelectionList);
        }

        [TestMethod]
        public void ShouldNotHandleDirectSelectionExpression()
        {
            _handler.AttachContext(_context.Object);
            Expression<Func<int>> call = () => TableQuery.Select(null, x => 1);
            _handler.CanHandle(call.Body).ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldPreserveBaseSelectCommand()
        {
            _sourceSqlExpression = new SelectCommand();
            _parsedSelectionList = new Mock<ISqlValueExpression>().Object;
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Select<UserEntity, int>(null, null));
            var arguments = new[]
            {
                _sourceStub.Expression,
                Expression.Quote(_expressionStub)
            };
            var callExpression = Expression.Call(
                null,
                method,
                arguments);
            var sqlExpression = _handler.Handle(callExpression);
            sqlExpression.ShouldBeTheSameInstance(_sourceSqlExpression);
        }

        [TestMethod]
        public void ShouldSetSqlSelectionAsSelection()
        {
            _sourceSqlExpression = new SelectCommand();
            _parsedSelectionList = new Mock<ISqlSelection>().Object;
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Select<UserEntity, int>(null, null));
            var arguments = new[]
            {
                _sourceStub.Expression,
                Expression.Quote(_expressionStub)
            };
            var callExpression = Expression.Call(
                null,
                method,
                arguments);
            var sqlExpression = _handler.Handle(callExpression);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Selection.ShouldBeTheSameInstance(_parsedSelectionList);
        }

        [TestMethod]
        public void ShouldWrapSelectionSourceIntoSelectCommand()
        {
            _sourceSqlExpression = new Mock<ISource>().Object;
            _parsedSelectionList = new ProjectionByConstructor(
                typeof(Tuple<int>).GetConstructors().Single(),
                new[] { new Mock<ISelectionItem>().Object });
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Select<UserEntity, int>(null, null));
            var arguments = new[]
            {
                _sourceStub.Expression,
                Expression.Quote(_expressionStub)
            };
            var callExpression = Expression.Call(
                null,
                method,
                arguments);
            var sqlExpression = _handler.Handle(callExpression);
            var selectCommand = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var aliasedSource = selectCommand.Source.ShouldBeInstanceOf<AliasedSourceItem>();
            aliasedSource.Source.ShouldBeTheSameInstance(_sourceSqlExpression);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var sourceMock = new Mock<ITableQuery<UserEntity>>();
            sourceMock.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<UserEntity>)));
            _sourceStub = sourceMock.Object;
            _context = new Mock<IVisitationContext>();
            _context.Setup(x => x.VisitExpression(_sourceStub.Expression))
                .Returns((Expression x) => _sourceSqlExpression);
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.MemberAccess)))
                .Returns((Expression x) => _parsedSelectionList);

            _handler = new ProjectionExpressionHandler();
            _handler.AttachContext(_context.Object);
        }
    }
}