﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.SqlAbstraction;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class ConvertExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldApplyTypeConversion()
        {
            var queryPart = Expression.Convert(Expression.Constant(0), typeof(int?));
            var typeManger = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typeManger.Setup(x => x.GetSqlType(typeof(int?)))
                .Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManger.Object);
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns(
                    (ConstantExpression x) => new LinqColumnReference(
                        new Mock<INamedSource>().Object,
                        ExpressionExtensions.GetProperty((UserEntity e) => e.Id),
                        typeof(int),
                        new SqlType()));

            var handler = new ConvertExpressionHandler();
            handler.AttachContext(context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<SqlCastExpression>();
            sqlExpression.SqlType.ShouldBeTheSameInstance(sqlType);
            sqlExpression.ClrType.ShouldBeEqual(typeof(int?));
        }
    }
}