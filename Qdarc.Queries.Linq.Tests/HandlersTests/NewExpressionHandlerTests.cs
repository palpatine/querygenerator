﻿using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class NewExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldHandleSelectionItemsAsConstructorParametersAndWrapThenInAliasedSelections()
        {
            var context = new Mock<IVisitationContext>();
            context
               .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
               .Returns((ConstantExpression x) =>
                {
                    var mock = new Mock<ISqlValueExpression>();
                    mock.Setup(y => y.ClrType).Returns(x.Type);
                    return mock.Object;
                });
            var handler = new NewExpressionHandler();
            handler.AttachContext(context.Object);
            var constructorInfo = typeof(DtoWithConstructor).GetConstructors().Single();
            var expression = Expression.New(constructorInfo, Expression.Constant(3), Expression.Constant("NAME"));

            var sqlExpression = handler.Handle(expression);

            var projection = sqlExpression.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.Constructor.ShouldBeEqual(constructorInfo);
            projection.Elements.Count().ShouldBeEqual(2);
            var first = projection.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
            first.Item.ShouldNotBeNull();
            first.Alias.ShouldBeEqual("id");
            var second = projection.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
            second.Item.ShouldNotBeNull();
            second.Alias.ShouldBeEqual("name");
        }

        [TestMethod]
        public void ShouldHandleSelectCommandAsConstructorParametersAndWrapThenInAliasedSelections()
        {
            var context = new Mock<IVisitationContext>();
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression y) =>
                {
                    var command = new Mock<ISelectCommand>();
                    var selection = new Mock<ISqlSelection>();
                    selection.Setup(x => x.Elements)
                        .Returns(
                            new[]
                            {
                                new Mock<ISelectionItem>().Object
                            });
                    command.Setup(x => x.Selection)
                        .Returns(selection.Object);

                    return command.Object;
                });
            var handler = new NewExpressionHandler();
            handler.AttachContext(context.Object);
            var constructorInfo = typeof(DtoWithConstructor).GetConstructors().Single();
            var expression = Expression.New(constructorInfo, Expression.Constant(3), Expression.Constant("NAME"));

            var sqlExpression = handler.Handle(expression);

            var projection = sqlExpression.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.Constructor.ShouldBeEqual(constructorInfo);
            projection.Elements.Count().ShouldBeEqual(2);
            var first = projection.Elements.First().ShouldBeInstanceOf<AliasedSelectionItem>();
            first.Item.ShouldBeInstanceOf<SelectCommandAsSelectionItem>();
            first.Alias.ShouldBeEqual("id");
            var second = projection.Elements.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
            second.Item.ShouldBeInstanceOf<SelectCommandAsSelectionItem>();
            second.Alias.ShouldBeEqual("name");
        }

        [TestMethod]
        public void ShouldHandleSqlSelectionsAsConstructorParametersAndWrapThenInPrefixedSelection()
        {
            var selection = new Mock<ISqlSelection>();
            var context = new Mock<IVisitationContext>();
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression x) => selection.Object);

            var handler = new NewExpressionHandler();
            handler.AttachContext(context.Object);
            var constructorInfo = typeof(DtoWithConstructor).GetConstructors().Single();
            var expression = Expression.New(constructorInfo, Expression.Constant(3), Expression.Constant("NAME"));

            var sqlExpression = handler.Handle(expression);

            var projection = sqlExpression.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.Constructor.ShouldBeEqual(constructorInfo);
            projection.Elements.Count().ShouldBeEqual(2);
            var first = projection.Elements.First().ShouldBeInstanceOf<PrefixedSelection>();
            first.Selection.ShouldBeTheSameInstance(selection.Object);
            first.Prefix.ShouldBeEqual("id");
            var second = projection.Elements.Last().ShouldBeInstanceOf<PrefixedSelection>();
            second.Selection.ShouldBeTheSameInstance(selection.Object);
            second.Prefix.ShouldBeEqual("name");
        }
    }
}