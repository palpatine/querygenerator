using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.SqlTypeFinders;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class InstanceMemberExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldBeAbleToVisitNotStaticMemberExpressions()
        {
            var value = new UserEntity();
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            var expression = Expression.MakeMemberAccess(
                Expression.Constant(value),
                ExpressionExtensions.GetProperty((UserEntity x) => x.Id));

            handler.CanHandle(expression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldNotBeAbleToVisitStaticMemberExpressions()
        {
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            var expression = Expression.Property(
                null,
                ExpressionExtensions.GetProperty(() => DateTime.Now));

            handler.CanHandle(expression).ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldAssumeFiledAccessToBeParameterReference()
        {
            var value = 0;
            Expression<Func<int>> lambda = () => value;
            var expression = lambda.Body;
            var sqlType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetSqlType(typeof(int)))
                .Returns(sqlType.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            var parameter = result.ShouldBeInstanceOf<LinqParameterReference>();
            parameter.ClrType.ShouldBeEqual(typeof(int));
            parameter.Expression.ShouldBeTheSameInstance(expression);
            parameter.SqlType.ShouldBeTheSameInstance(sqlType.Object);
        }

        [TestMethod]
        public void ShouldAssumeParameterReferenceIfBaseExpressionIsParameterExpression()
        {
            Expression<Func<UserEntity, int>> lambda = x => x.Id;
            var expression = lambda.Body;
            var sqlType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetSqlType(typeof(int)))
                .Returns(sqlType.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            context.Setup(x => x.VisitExpression(It.IsAny<ParameterExpression>()))
                .Returns(new Mock<IParameter>().Object);
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            var parameter = result.ShouldBeInstanceOf<LinqParameterReference>();
            parameter.ClrType.ShouldBeEqual(typeof(int));
            parameter.Expression.ShouldBeTheSameInstance(expression);
            parameter.SqlType.ShouldBeTheSameInstance(sqlType.Object);
        }

        [TestMethod]
        public void ShouldFindSelectionItemAliasedByNameOfReferencedPropertyIfBaseExpressionIsSelectCommand()
        {
            Expression<Func<UserEntity, int>> lambda = x => x.Id;
            var expression = lambda.Body;
            var sqlType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetSqlType(typeof(int)))
                .Returns(sqlType.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var item = new Mock<ISelectionItem>();
            var sqlSelection = new SqlSelection(new[] { new AliasedSelectionItem(item.Object) { Alias = "Id" } });
            var selectCommand = new SelectCommand(
                selection: sqlSelection);
            context.Setup(x => x.VisitExpression(It.IsAny<ParameterExpression>()))
                .Returns(selectCommand);
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            result.ShouldBeTheSameInstance(item.Object);
        }

        [TestMethod]
        public void ShouldFindSelectionPrefixedByNameOfReferencedPropertyIfBaseExpressionIsSelectCommand()
        {
            Expression<Func<UserEntity, int>> lambda = x => x.Id;
            var expression = lambda.Body;
            var sqlType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetSqlType(typeof(int)))
                .Returns(sqlType.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var item = new Mock<ISqlSelection>();
            var sqlSelection = new SqlSelection(new[] { new PrefixedSelection("Id", item.Object) });
            var selectCommand = new SelectCommand
            {
                Selection = sqlSelection
            };
            context.Setup(x => x.VisitExpression(It.IsAny<ParameterExpression>()))
                .Returns(selectCommand);
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            result.ShouldBeTheSameInstance(item.Object);
        }

        [TestMethod]
        public void ShouldFindSelectionItemAliasedByNameOfReferencedPropertyIfBaseExpressionIsSqlSelection()
        {
            Expression<Func<UserEntity, int>> lambda = x => x.Id;
            var expression = lambda.Body;
            var sqlType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetSqlType(typeof(int)))
                .Returns(sqlType.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var item = new Mock<ISelectionItem>();
            var sqlSelection = new SqlSelection(new[] { new AliasedSelectionItem(item.Object) { Alias = "Id" } });
            context.Setup(x => x.VisitExpression(It.IsAny<ParameterExpression>()))
                .Returns(sqlSelection);
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            result.ShouldBeTheSameInstance(item.Object);
        }

        [TestMethod]
        public void ShouldFindSelectionPrefixedByNameOfReferencedPropertyIfBaseExpressionIsSqlSelection()
        {
            Expression<Func<UserEntity, int>> lambda = x => x.Id;
            var expression = lambda.Body;
            var sqlType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetSqlType(typeof(int)))
                .Returns(sqlType.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var item = new Mock<ISqlSelection>();
            var sqlSelection = new SqlSelection(new[] { new PrefixedSelection("Id", item.Object) });
            context.Setup(x => x.VisitExpression(It.IsAny<ParameterExpression>()))
                .Returns(sqlSelection);
            var columnFinder = new Mock<ISqlTypeFinder>();
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            result.ShouldBeTheSameInstance(item.Object);
        }

        [TestMethod]
        public void ShouldCreateColumnReferenceIfBaseExpressionIsNamedSource()
        {
            Expression<Func<UserEntity, int>> lambda = x => x.Id;
            var expression = lambda.Body;
            var sqlType = new Mock<ISqlType>();
            var propertyInfo = ExpressionExtensions.GetProperty((UserEntity u) => u.Id);
            var context = new Mock<IVisitationContext>();
            var source = new Mock<INamedSource>();
            source.Setup(x => x.ClrType).Returns(typeof(UserEntity));
            context.Setup(x => x.VisitExpression(It.IsAny<ParameterExpression>()))
                .Returns(source.Object);
            var columnFinder = new Mock<ISqlTypeFinder>();
            columnFinder.Setup(x => x.TryFindType(source.Object, propertyInfo))
                .Returns(sqlType.Object);
            var handler = new InstanceMemberExpressionHandler(columnFinder.Object);
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            var column = result.ShouldBeInstanceOf<LinqColumnReference>();
            column.ClrType.ShouldBeEqual(typeof(int));
            column.Property.ShouldBeTheSameInstance(propertyInfo);
            column.Source.ShouldBeTheSameInstance(source.Object);
            column.SqlType.ShouldBeTheSameInstance(sqlType.Object);
        }
    }
}