﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class ConstantExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldHandleConstantExpression()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new ConstantExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(Expression.Constant(1)).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitFunctionQueryConstantExpression()
        {
            var arguments = new List<ConstantExpression> { Expression.Constant(4) };
            var queryPart = new FunctionQuery<UserEntity>(new Mock<ITableQueryProvider>().Object, new ElementName("name"), arguments);

            var context = new Mock<IVisitationContext>();
            var modelToSqlConvention = new Mock<IModelToSqlConvention>();
            context.Setup(x => x.ModelToSqlConvention).Returns(modelToSqlConvention.Object);
            var processedArgument = new Mock<ISqlValueExpression>().Object;
            context.Setup(x => x.VisitExpression(arguments[0])).Returns(processedArgument);
            var handler = new ConstantExpressionHandler();
            handler.AttachContext(context.Object);

            var parseResult = handler.Handle(queryPart.Expression);

            var functionCall = parseResult.ShouldBeInstanceOf<TabularFunctionCall>();
            functionCall.ClrType.ShouldBeEqual(typeof(UserEntity));
            functionCall.Arguments.AllCorrespondingElementsShouldBeEqual(new[] { processedArgument });
            functionCall.Name.ShouldNotBeNull();
            functionCall.Name[0].ShouldBeEqual("name");
        }

        [TestMethod]
        public void ShouldVisitTableParameterConstantExpression()
        {
            var arguments = Expression.Constant(new[] { new IntValue(5) });
            var queryPart = new TableParameter<IntValue>(new Mock<ITableQueryProvider>().Object, arguments);

            var context = new Mock<IVisitationContext>();
            var modelToSqlConvention = new Mock<IModelToSqlConvention>();
            context.Setup(x => x.ModelToSqlConvention).Returns(modelToSqlConvention.Object);
            var handler = new ConstantExpressionHandler();
            handler.AttachContext(context.Object);
            var typeManager = new Mock<ISqlTypeManager>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            modelToSqlConvention.Setup(x => x.GetUserDefinedTableType(typeof(IntValue)))
                .Returns(new Mock<ISqlType>().Object);
            var parseResult = handler.Handle(queryPart.Expression);

            context.Verify(x => x.VisitExpression(It.IsAny<Expression>()), Times.Never);
            var tableParameter = parseResult.ShouldBeInstanceOf<LinqTableParameterReference>();
            tableParameter.ClrType.ShouldBeEqual(typeof(IntValue));
            tableParameter.Expression.ShouldBeTheSameInstance(arguments);
        }

        [TestMethod]
        public void ShouldVisitTableQueryConstantExpression()
        {
            var queryPart = new TableQuery<UserEntity>(new Mock<ITableQueryProvider>().Object);

            var context = new Mock<IVisitationContext>();
            var modelToSqlConvention = new Mock<IModelToSqlConvention>();
            modelToSqlConvention.Setup(x => x.GetTableName(typeof(UserEntity)))
                .Returns(new ElementName("name"));
            context.Setup(x => x.ModelToSqlConvention).Returns(modelToSqlConvention.Object);
            var handler = new ConstantExpressionHandler();
            handler.AttachContext(context.Object);
            var parseResult = handler.Handle(queryPart.Expression);
            var tableReference = parseResult.ShouldBeInstanceOf<TableReference>();
            tableReference.ClrType.ShouldBeEqual(typeof(UserEntity));
        }

        [TestMethod]
        public void ShouldVisitValueConstantExpression()
        {
            var queryPart = Expression.Constant(6);
            var context = new Mock<IVisitationContext>();
            var typesManager = new Mock<ISqlTypeManager>();
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            typesManager.Setup(x => x.GetSqlType(queryPart))
                .Returns(new Mock<ISqlType>().Object);

            var handler = new ConstantExpressionHandler();
            handler.AttachContext(context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<SqlConstantExpression>();
            sqlExpression.Value.ShouldBeEqual(6);
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            typesManager.Verify(c => c.GetSqlType(queryPart), Times.Once);
        }
    }
}