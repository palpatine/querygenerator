﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class MemberInitializationExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldProcessConstructorCall()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new MemberInitializationExpressionHandler();
            handler.AttachContext(context.Object);
            var projectionByConstructor =
                new ProjectionByConstructor(
                    ExpressionExtensions.GetConstructor(() => new UserEntity()),
                    Enumerable.Empty<ISelectionItem>());
            context.Setup(x => x.VisitExpression(It.IsAny<ConstantExpression>()))
                .Returns((ConstantExpression x) => new Mock<ISelectionItem>().Object);
            context.Setup(x => x.VisitExpression(It.IsAny<NewExpression>()))
                .Returns(projectionByConstructor);
            Expression<Func<UserEntity>> expression = () => new UserEntity { FirstName = "name" };

            var result = handler.Handle(expression.Body);
            var projectinByAssignment = result.ShouldBeInstanceOf<ProjectionByMembersAssignment>();
            projectinByAssignment.ConstructorCall.ShouldBeTheSameInstance(projectionByConstructor);
        }

        [TestMethod]
        public void ShouldBindSelectionItem()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new MemberInitializationExpressionHandler();
            handler.AttachContext(context.Object);
            var projectionByConstructor =
                new ProjectionByConstructor(
                    ExpressionExtensions.GetConstructor(() => new UserEntity()),
                    Enumerable.Empty<ISelectionItem>());
            var selectionItem = new Mock<ISelectionItem>().Object;
            context.Setup(x => x.VisitExpression(It.IsAny<ConstantExpression>()))
                .Returns(selectionItem);
            context.Setup(x => x.VisitExpression(It.IsAny<NewExpression>()))
                .Returns(projectionByConstructor);
            Expression<Func<UserEntity>> expression = () => new UserEntity { FirstName = "name" };

            var result = handler.Handle(expression.Body);
            var projectinByAssignment = result.ShouldBeInstanceOf<ProjectionByMembersAssignment>();
            projectinByAssignment.Bindings.Single().Item.ShouldBeTheSameInstance(selectionItem);
        }

        [TestMethod]
        public void ShouldAliasBoundSelectionItem()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new MemberInitializationExpressionHandler();
            handler.AttachContext(context.Object);
            var projectionByConstructor =
                new ProjectionByConstructor(
                    ExpressionExtensions.GetConstructor(() => new UserEntity()),
                    Enumerable.Empty<ISelectionItem>());
            var selectionItem = new Mock<ISelectionItem>().Object;
            context.Setup(x => x.VisitExpression(It.IsAny<ConstantExpression>()))
                .Returns(selectionItem);
            context.Setup(x => x.VisitExpression(It.IsAny<NewExpression>()))
                .Returns(projectionByConstructor);
            Expression<Func<UserEntity>> expression = () => new UserEntity { FirstName = "name" };

            var result = handler.Handle(expression.Body);
            var projectinByAssignment = result.ShouldBeInstanceOf<ProjectionByMembersAssignment>();
            projectinByAssignment.Bindings.Single().Alias.ShouldBeEqual("FirstName");
        }

        [TestMethod]
        public void ShouldBindSelectCommand()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new MemberInitializationExpressionHandler();
            handler.AttachContext(context.Object);
            var projectionByConstructor =
                new ProjectionByConstructor(
                    ExpressionExtensions.GetConstructor(() => new UserEntity()),
                    Enumerable.Empty<ISelectionItem>());
            var command = new Mock<ISelectCommand>();
            var selection = new Mock<ISqlSelection>();
            selection.Setup(x => x.Elements)
                .Returns(
                    new[]
                    {
                        new Mock<ISelectionItem>().Object
                    });
            command.Setup(x => x.Selection)
                .Returns(selection.Object);
            var selectCommand = command.Object;
            context.Setup(x => x.VisitExpression(It.IsAny<ConstantExpression>()))
                .Returns(selectCommand);
            context.Setup(x => x.VisitExpression(It.IsAny<NewExpression>()))
                .Returns(projectionByConstructor);
            Expression<Func<UserEntity>> expression = () => new UserEntity { FirstName = "name" };

            var result = handler.Handle(expression.Body);
            var projectinByAssignment = result.ShouldBeInstanceOf<ProjectionByMembersAssignment>();
            var selectionItem = projectinByAssignment.Bindings.Single().Item.ShouldBeInstanceOf<SelectCommandAsSelectionItem>();
            selectionItem.Command.ShouldBeTheSameInstance(selectCommand);
        }
    }
}
