﻿using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class DatabaseInsertMethodCallHandlerTests
    {
        private readonly MethodInfo _insertByValuesMethod = ExpressionExtensions.GetMethod((IDatabase d) => d.Insert((IValuesQuery<UserEntity>)null));

        [TestMethod]
        public void ShouldHandleDatabaseInsertByEnumerableMethod()
        {
            var instance = new Mock<IDatabase>().Object;
            var values = new Mock<IValuesQuery<UserEntity>>();
            var arguments = new Expression[]
            {
                Expression.Constant(values.Object, typeof(IValuesQuery<UserEntity>))
            };
            var expression = Expression.Call(
                Expression.Constant(instance, typeof(IDatabase)),
                _insertByValuesMethod,
                arguments);
            var handler = new DatabaseInsertByValuesMethodCallHandler();

            handler.CanHandle(expression);
        }

        [TestMethod]
        public void ShouldCreateInsertCommand()
        {
            var instance = new Mock<IDatabase>().Object;
            var values = new Mock<IValuesQuery<UserEntity>>();
            values.Setup(x => x.Values)
                .Returns(Expression.Constant(new[] { new UserEntity() }));
            var arguments = new Expression[]
            {
                Expression.Constant(values.Object, typeof(IValuesQuery<UserEntity>))
            };
            var expression = Expression.Call(
                Expression.Constant(instance, typeof(IDatabase)),
                _insertByValuesMethod,
                arguments);
            var context = new Mock<IVisitationContext>();
            var convention = new Mock<IModelToSqlConvention>();
            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);
            var handler = new DatabaseInsertByValuesMethodCallHandler();
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<InsertCommand>();
        }

        [TestMethod]
        public void ShouldReferenceCorrectTable()
        {
            var instance = new Mock<IDatabase>().Object;
            var values = new Mock<IValuesQuery<UserEntity>>();
            values.Setup(x => x.Values)
                .Returns(Expression.Constant(new[] { new UserEntity() }));
            var arguments = new Expression[]
            {
                Expression.Constant(values.Object, typeof(IValuesQuery<UserEntity>))
            };
            var expression = Expression.Call(
                Expression.Constant(instance, typeof(IDatabase)),
                _insertByValuesMethod,
                arguments);
            var context = new Mock<IVisitationContext>();
            var handler = new DatabaseInsertByValuesMethodCallHandler();
            handler.AttachContext(context.Object);
            var name = new ElementName("Name");
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetTableName(typeof(UserEntity)))
                .Returns(name);
            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);

            var result = handler.Handle(expression);

            var command = result.ShouldBeInstanceOf<InsertCommand>();
            var table = command.TableReference.ShouldBeInstanceOf<TableReference>();
            table.Name.ShouldBeTheSameInstance(name);
            table.ClrType.ShouldBeEqual(typeof(UserEntity));
        }
    }
}
