﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class LogicalConjunctionExpressionHandlerTests
    {
        private Mock<IVisitationContext> _context;

        [TestMethod]
        public void AndAlso()
        {
            var handler = new AndAlsoLogicalConjunctionExpressionHandler();
            handler.AttachContext(_context.Object);
            var expression = Expression.AndAlso(
                Expression.Equal(Expression.Constant(2), Expression.Constant(2)),
                Expression.Equal(Expression.Constant(2), Expression.Constant(2)));

            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<AndLogicalConjunctionExpression>();
            logicalExpression.Left.ShouldBeInstanceOf<ISqlLogicalExpression>();
            logicalExpression.Right.ShouldBeInstanceOf<ISqlLogicalExpression>();
        }

        [TestMethod]
        public void ConjunctionOnBooleanProperties()
        {
            var handler = new AndAlsoLogicalConjunctionExpressionHandler();
            handler.AttachContext(_context.Object);
            var expression = Expression.AndAlso(
                Expression.Constant(true),
                Expression.Constant(true));

            var sqlExpression = handler.Handle(expression);
            var conjunction = sqlExpression.ShouldBeInstanceOf<AndLogicalConjunctionExpression>();
            conjunction.Left.ShouldNotBeNull();
            conjunction.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ExclusiveOr()
        {
            var handler = new ExclusiveOrLogicalConjunctionExpressionHandler();
            handler.AttachContext(_context.Object);

            var expression = Expression.ExclusiveOr(
                Expression.Equal(Expression.Constant(2), Expression.Constant(2)),
                Expression.Equal(Expression.Constant(2), Expression.Constant(2)));

            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<ExclusiveOrLogicalConjunctionExpression>();
            logicalExpression.Left.ShouldBeInstanceOf<ISqlLogicalExpression>();
            logicalExpression.Right.ShouldBeInstanceOf<ISqlLogicalExpression>();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleLogicalExclusiveOrOperation()
        {
            var queryPart = Expression.ExclusiveOr(
                Expression.Constant(true),
                Expression.Constant(true));
            var handler = new ExclusiveOrLogicalConjunctionExpressionHandler();
            handler.AttachContext(_context.Object);

            handler.CanHandle(queryPart).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldNotBeAbleToHandleBitwiseExclusiveOrOperation()
        {
            var queryPart = Expression.ExclusiveOr(
                Expression.Constant(2),
                Expression.Constant(4));
            var handler = new ExclusiveOrLogicalConjunctionExpressionHandler();
            handler.AttachContext(_context.Object);

            handler.CanHandle(queryPart).ShouldBeFalse();
        }

        [TestMethod]
        public void OrElse()
        {
            var handler = new OrElseLogicalConjunctionExpressionHandler();
            handler.AttachContext(_context.Object);

            var expression = Expression.OrElse(
                Expression.Equal(Expression.Constant(2), Expression.Constant(2)),
                Expression.Equal(Expression.Constant(2), Expression.Constant(2)));

            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<OrLogicalConjunctionExpression>();
            logicalExpression.Left.ShouldBeInstanceOf<ISqlLogicalExpression>();
            logicalExpression.Right.ShouldBeInstanceOf<ISqlLogicalExpression>();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new Mock<IVisitationContext>();
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Equal)))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns(new LinqColumnReference(
                    new Mock<INamedSource>().Object,
                    ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                    typeof(int),
                    new Mock<ISqlType>().Object));
            _context
                .Setup(x => x.WrapInLogicalExpression(It.IsAny<LinqColumnReference>()))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            var typeManager = new Mock<ISqlTypeManager>();
            _context.Setup(x => x.TypeManager).Returns(typeManager.Object);
        }
    }
}