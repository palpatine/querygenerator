﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class OrderByHandlerTests
    {
        private Mock<IVisitationContext> _context;
        private OrderExpressionHandler _handler;
        private ISqlExpression _sourceParsed;

        [TestMethod]
        public void ShouldHandleAscendingOrderingOnSelectCommand()
        {
            var query = Query<UserEntity>()
                .OrderBy(x => x.Id);

            var sqlExpression = _handler.Handle(query.Expression);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Single().Value.ShouldBeInstanceOf<LinqColumnReference>();
            select.Order.Single().Ascending.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleDescendingOrderingOnSelectCommand()
        {
            var query = Query<UserEntity>()
                .OrderByDescending(x => x.Id);

            var sqlExpression = _handler.Handle(query.Expression);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Single().Value.ShouldBeInstanceOf<LinqColumnReference>();
            select.Order.Single().Ascending.ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldOverrideExistingOrderOnAscendingOrderingOnSelectCommand()
        {
            var firstOrder = new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, false);
            _sourceParsed = new SelectCommand(
                order: new[]
                {
                    firstOrder
                },
                source: new Mock<INamedSource>().Object);
            var query = Query<UserEntity>()
                .OrderBy(x => x.Id);

            var sqlExpression = _handler.Handle(query.Expression);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Single().Value.ShouldBeInstanceOf<LinqColumnReference>();
            select.Order.Single().Ascending.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldOverrideExistingOrderOnDescendingOrderingOnSelectCommand()
        {
            var firstOrder = new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, false);
            _sourceParsed = new SelectCommand(
                order: new[]
                {
                    firstOrder
                },
                source: new Mock<INamedSource>().Object);
            var query = Query<UserEntity>()
                .OrderByDescending(x => x.Id);

            var sqlExpression = _handler.Handle(query.Expression);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Single().Value.ShouldBeInstanceOf<LinqColumnReference>();
            select.Order.Single().Ascending.ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldWrapOtherSelectionsInSelectCommand()
        {
            _sourceParsed = new UnionSelectionCommand(new Mock<ISelectCommand>().Object, new Mock<ISelectCommand>().Object);
            var query = Query<UserEntity>()
                .OrderBy(x => x.Id);

            var sqlExpression = _handler.Handle(query.Expression);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Single().Value.ShouldBeInstanceOf<LinqColumnReference>();
            select.Order.Single().Ascending.ShouldBeTrue();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new Mock<IVisitationContext>();
            _sourceParsed = new SelectCommand(
                new TableReference(
                new ElementName("Users"),
                typeof(UserEntity)));
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression x) => _sourceParsed);
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.MemberAccess)))
                .Returns(
                    (Expression x) => new LinqColumnReference(
                        new Mock<INamedSource>().Object,
                        ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                        typeof(int),
                        new Mock<ISqlType>().Object));

            _handler = new OrderExpressionHandler();
            _handler.AttachContext(_context.Object);
        }

        [TestMethod]
        public void ShouldAddSecondaryAscendingOrdering()
        {
            var firstOrder = new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, false);
            _sourceParsed = new SelectCommand(
                order: new[]
                {
                    firstOrder
                },
                source: new Mock<INamedSource>().Object);
            Expression<Func<UserEntity, int>> property = x => x.Id;
            var method = ExpressionExtensions.GetMethod(() => OrderedTableQuery.ThanBy<UserEntity, int>(null, null));
            var arguments = new Expression[]
            {
                Expression.Constant(new Mock<IOrderedTableQuery<UserEntity>>().Object, typeof(IOrderedTableQuery<UserEntity>)),
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            var sqlExpression = _handler.Handle(expression);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Count().ShouldBeEqual(2);
            select.Order.First().ShouldBeTheSameInstance(firstOrder);
            select.Order.Last().Value.ShouldBeInstanceOf<LinqColumnReference>();
            select.Order.Last().Ascending.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldAddSecondaryDescendingOrdering()
        {
            var firstOrder = new OrderedValueExpression(new Mock<ISqlValueExpression>().Object, false);
            _sourceParsed = new SelectCommand(
                order: new[]
                {
                    firstOrder
                },
                source: new Mock<INamedSource>().Object);
            Expression<Func<UserEntity, int>> property = x => x.Id;
            var method = ExpressionExtensions.GetMethod(() => OrderedTableQuery.ThanByDescending<UserEntity, int>(null, null));
            var arguments = new Expression[]
            {
                Expression.Constant(new Mock<IOrderedTableQuery<UserEntity>>().Object, typeof(IOrderedTableQuery<UserEntity>)),
                Expression.Quote(property)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            var sqlExpression = _handler.Handle(expression);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Count().ShouldBeEqual(2);
            select.Order.First().ShouldBeTheSameInstance(firstOrder);
            select.Order.Last().Value.ShouldBeInstanceOf<LinqColumnReference>();
            select.Order.Last().Ascending.ShouldBeFalse();
        }

        private static ITableQuery<T> Query<T>()
        {
            var provider = new Mock<ITableQueryProvider>();
            provider.Setup(x => x.CreateOrderedQuery<UserEntity>(It.IsAny<Expression>()))
                .Returns(
                    (Expression x) =>
                    {
                        var innerMock = new Mock<IOrderedTableQuery<UserEntity>>();
                        innerMock.Setup(z => z.Expression).Returns(x);
                        innerMock.Setup(z => z.Provider).Returns(provider.Object);
                        return innerMock.Object;
                    });

            var mock = new Mock<ITableQuery<T>>();
            mock.Setup(x => x.Provider).Returns(provider.Object);
            mock.Setup(x => x.Expression)
                .Returns(Expression.Constant(mock.Object));
            return mock.Object;
        }
    }
}