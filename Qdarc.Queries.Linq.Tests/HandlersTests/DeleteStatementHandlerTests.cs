using System;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.ExpressionReplacer;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class DeleteStatementHandlerTests
    {
        private static readonly MethodInfo DeleteMethod
            = ExpressionExtensions.GetMethod(() => TableQueryDelete.Delete<UserEntity>(null));
        private static readonly MethodInfo DeleteOnTupleMethod
            = ExpressionExtensions.GetMethod(() => TableQueryDelete.Delete<UserEntity, ContactEntity, UserEntity>(null, null));

        [TestMethod]
        public void ShouldCreateDeleteCommand()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var expression = Expression.Call(
                null,
                DeleteMethod,
                baseQuery);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(new TableReference(new ElementName("Name")));

            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<DeleteCommand>();
        }

        [TestMethod]
        public void ShouldVisitBaseQuery()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var expression = Expression.Call(
                null,
                DeleteMethod,
                baseQuery);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(new TableReference(new ElementName("Name")));

            handler.Handle(expression);

            context.Verify(x => x.VisitExpression(baseQuery), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignQueryAsDeletionTarget()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var expression = Expression.Call(
                null,
                DeleteMethod,
                baseQuery);
            var deleteionTarget = new TableReference(new ElementName("Name"));
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(deleteionTarget);

            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<DeleteCommand>().
                Target.ShouldBeTheSameInstance(deleteionTarget);
        }

        [TestMethod]
        public void ShouldUseSelectCommandSourceAsTarget()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var expression = Expression.Call(
                null,
                DeleteMethod,
                baseQuery);
            var source = new Mock<INamedSource>();
            var deleteionTarget = new SelectCommand(source.Object);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(deleteionTarget);

            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<DeleteCommand>().
                Target.ShouldBeTheSameInstance(deleteionTarget.Source);
        }

        [TestMethod]
        public void ShouldUseFilterFromSelectCommand()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var expression = Expression.Call(
                null,
                DeleteMethod,
                baseQuery);
            var source = new Mock<INamedSource>();
            var filter = new Mock<ISqlLogicalExpression>().Object;
            var deleteionTarget = new SelectCommand(source.Object, filter: filter);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(deleteionTarget);

            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<DeleteCommand>().
                Filter.ShouldBeTheSameInstance(filter);
        }

        [TestMethod]
        public void ShouldReplaceAliasedSourceInFilterWithItsTarget()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var expression = Expression.Call(
                null,
                DeleteMethod,
                baseQuery);
            var filter = new Mock<ISqlLogicalExpression>().Object;
            var innerSource = new Mock<INamedSource>();
            var source = new AliasedSourceItem(innerSource.Object);
            var deleteionTarget = new SelectCommand(source, filter: filter);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(deleteionTarget);
            var replacedFilter = new Mock<ISqlLogicalExpression>();
            replacer.Setup(x => x.Replace(filter, source, innerSource.Object))
                .Returns(replacedFilter.Object);

            var result = handler.Handle(expression);

            replacer.Verify(x => x.Replace(filter, source, innerSource.Object), Times.Once());
            result.ShouldBeInstanceOf<DeleteCommand>()
                            .Filter.ShouldBeTheSameInstance(replacedFilter.Object);
        }

        [TestMethod]
        public void ShouldUseSelectCommandSourceAsTargetAfterRemovingAlias()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var expression = Expression.Call(
                null,
                DeleteMethod,
                baseQuery);
            var innerSource = new Mock<INamedSource>();
            var source = new AliasedSourceItem(innerSource.Object);
            var deleteionTarget = new SelectCommand(source);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(deleteionTarget);

            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<DeleteCommand>().
                Target.ShouldBeTheSameInstance(innerSource.Object);
        }

        [TestMethod]
        public void ShouldProcessTargetSelector()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<Tuple<UserEntity, ContactEntity>>>().Object, typeof(ITableQuery<Tuple<UserEntity, ContactEntity>>));
            Expression<Func<UserEntity, ContactEntity, UserEntity>> targetSelector = (u, c) => u;
            var expression = Expression.Call(
                null,
                DeleteOnTupleMethod,
                baseQuery,
                Expression.Quote(targetSelector));
            var source = new Mock<IJoinSource>();
            var target = new AliasedSourceItem(new Mock<ISource>().Object);
            var secondary = new AliasedSourceItem(new Mock<ISource>().Object);
            source.Setup(x => x.Sources)
                .Returns(
                    new[]
                    {
                        target,
                        secondary
                    });
            var deleteionTarget = new SelectCommand(source.Object);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(deleteionTarget);
            context.Setup(x => x.VisitExpression(targetSelector.Body))
                .Returns(target);
            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<DeleteCommand>().
                Target.ShouldBeTheSameInstance(target);
        }

        [TestMethod]
        public void ShouldUseSelectCommandSourceAsDeletionSource()
        {
            var context = new Mock<IVisitationContext>();
            var replacer = new Mock<IExpressionReplacer<INamedSource>>();
            var handler = new DeleteStatementHandler(replacer.Object);
            handler.AttachContext(context.Object);
            var baseQuery = Expression.Constant(new Mock<ITableQuery<Tuple<UserEntity, ContactEntity>>>().Object, typeof(ITableQuery<Tuple<UserEntity, ContactEntity>>));
            Expression<Func<UserEntity, ContactEntity, UserEntity>> targetSelector = (u, c) => u;
            var expression = Expression.Call(
                null,
                DeleteOnTupleMethod,
                baseQuery,
                Expression.Quote(targetSelector));
            var source = new Mock<IJoinSource>();
            var target = new AliasedSourceItem(new Mock<ISource>().Object);
            var secondary = new AliasedSourceItem(new Mock<ISource>().Object);
            source.Setup(x => x.Sources)
                .Returns(
                    new[]
                    {
                        target,
                        secondary
                    });
            var deleteionTarget = new SelectCommand(source.Object);
            context.Setup(x => x.VisitExpression(baseQuery))
                .Returns(deleteionTarget);
            context.Setup(x => x.VisitExpression(targetSelector.Body))
                .Returns(target);
            var result = handler.Handle(expression);

            result.ShouldBeInstanceOf<DeleteCommand>().
                Source.ShouldBeTheSameInstance(source.Object);
        }
    }
}