using System;
using System.Globalization;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class ToStringExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldExecuteParameterlessToStringOnConstantDate()
        {
            // ReSharper disable once SpecifyACultureInStringConversionExplicitly - checks if call without parameters is visited correctly
            Expression<Func<string>> expression = () => new DateTime(2016, 1, 1).ToString();
            var memberExpression = (MethodCallExpression)expression.Body;
            var stringType = new Mock<ISqlType>().Object;
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetNVarChar(0))
                .Returns(stringType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager)
                .Returns(typeManager.Object);
            var value = new Mock<ISqlValueExpression>();
            value.Setup(x => x.ClrType).Returns(typeof(DateTime));
            context
                .Setup(x => x.VisitExpression(memberExpression.Object))
                .Returns(value.Object);
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);

            var result = handler.Handle(memberExpression);

            var formatExpression = result.ShouldBeInstanceOf<SqlFormatExpression>();
            formatExpression.ClrType.ShouldBeEqual(typeof(string));
            formatExpression.SqlType.ShouldBeEqual(stringType);
            formatExpression.Expression.ShouldBeTheSameInstance(value.Object);
            formatExpression.FormatProvider.ShouldBeTheSameInstance(CultureInfo.CurrentCulture);
            formatExpression.Format.ShouldBeEqual("d");
        }

        [TestMethod]
        public void ShouldExecuteParameterlessToStringOnConstantNumber()
        {
            Expression<Func<string>> expression = () => 1.ToString();
            var memberExpression = (MethodCallExpression)expression.Body;
            var stringType = new Mock<ISqlType>().Object;
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetNVarChar(0))
                .Returns(stringType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager)
                .Returns(typeManager.Object);
            var value = new Mock<ISqlValueExpression>().Object;
            context
                .Setup(x => x.VisitExpression(memberExpression.Object))
                .Returns(value);
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var formatExpression = result.ShouldBeInstanceOf<SqlFormatExpression>();
            formatExpression.ClrType.ShouldBeEqual(typeof(string));
            formatExpression.SqlType.ShouldBeEqual(stringType);
            formatExpression.Expression.ShouldBeTheSameInstance(value);
            formatExpression.FormatProvider.ShouldBeTheSameInstance(CultureInfo.CurrentCulture);
            formatExpression.Format.ShouldBeEqual("G");
        }

        [TestMethod]
        public void ShouldExecuteToStringWithCultureOnConstantDate()
        {
            Expression<Func<string>> expression = () => new DateTime(2016, 1, 1).ToString(CultureInfo.InvariantCulture);
            var memberExpression = (MethodCallExpression)expression.Body;
            var stringType = new Mock<ISqlType>().Object;
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetNVarChar(0))
                .Returns(stringType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager)
                .Returns(typeManager.Object);
            var value = new Mock<ISqlValueExpression>();
            value.Setup(x => x.ClrType).Returns(typeof(DateTime));
            context
                 .Setup(x => x.VisitExpression(memberExpression.Object))
                .Returns(value.Object);
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var formatExpression = result.ShouldBeInstanceOf<SqlFormatExpression>();
            formatExpression.ClrType.ShouldBeEqual(typeof(string));
            formatExpression.SqlType.ShouldBeEqual(stringType);
            formatExpression.Expression.ShouldBeTheSameInstance(value.Object);
            formatExpression.FormatProvider.ShouldBeTheSameInstance(CultureInfo.InvariantCulture);
            formatExpression.Format.ShouldBeEqual("d");
        }

        [TestMethod]
        public void ShouldExecuteToStringWithFormatOnConstantDate()
        {
            Expression<Func<string>> expression = () => new DateTime(2016, 1, 1).ToString("F");
            var memberExpression = (MethodCallExpression)expression.Body;
            var stringType = new Mock<ISqlType>().Object;
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetNVarChar(0))
                .Returns(stringType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager)
                .Returns(typeManager.Object);
            var value = new Mock<ISqlValueExpression>().Object;
            context
                .Setup(x => x.VisitExpression(memberExpression.Object))
                .Returns(value);
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var formatExpression = result.ShouldBeInstanceOf<SqlFormatExpression>();
            formatExpression.ClrType.ShouldBeEqual(typeof(string));
            formatExpression.SqlType.ShouldBeEqual(stringType);
            formatExpression.Expression.ShouldBeTheSameInstance(value);
            formatExpression.FormatProvider.ShouldBeTheSameInstance(CultureInfo.CurrentCulture);
            formatExpression.Format.ShouldBeEqual("F");
        }

        [TestMethod]
        public void ShouldExecuteToStringWithFormatAndCultureOnConstantDate()
        {
            Expression<Func<string>> expression = () => new DateTime(2016, 1, 1).ToString("F", CultureInfo.InvariantCulture);
            var memberExpression = (MethodCallExpression)expression.Body;
            var stringType = new Mock<ISqlType>().Object;
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.GetNVarChar(0))
                .Returns(stringType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager)
                .Returns(typeManager.Object);
            var value = new Mock<ISqlValueExpression>().Object;
            context
                .Setup(x => x.VisitExpression(memberExpression.Object))
                .Returns(value);
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var formatExpression = result.ShouldBeInstanceOf<SqlFormatExpression>();
            formatExpression.ClrType.ShouldBeEqual(typeof(string));
            formatExpression.SqlType.ShouldBeEqual(stringType);
            formatExpression.Expression.ShouldBeTheSameInstance(value);
            formatExpression.FormatProvider.ShouldBeTheSameInstance(CultureInfo.InvariantCulture);
            formatExpression.Format.ShouldBeEqual("F");
        }

        [TestMethod]
        public void ShouldHandleToStringMethodCallOnConstant()
        {
            Expression<Func<string>> expression = () => 1.ToString();
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleToStringMethodCallOnVariable()
        {
            var value = 1.1;
            Expression<Func<string>> expression = () => value.ToString();
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleToStringMethodCallWithCulture()
        {
            var value = 1.1;
            Expression<Func<string>> expression = () => value.ToString(CultureInfo.InvariantCulture);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleToStringMethodCallWithFormat()
        {
            var value = 1.1;
            Expression<Func<string>> expression = () => value.ToString("C");
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleToStringMethodCallWithFormatAndCulture()
        {
            var value = 1.1;
            Expression<Func<string>> expression = () => value.ToString("d", CultureInfo.InvariantCulture);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ToStringExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }
    }
}