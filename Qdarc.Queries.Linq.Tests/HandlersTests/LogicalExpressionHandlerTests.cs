﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class LogicalExpressionHandlerTests
    {
        private Mock<IVisitationContext> _context;

        [TestMethod]
        public void ShouldDetermineOperandsTypeIfBothAreParameters()
        {
            var parameterType = new Mock<ISqlType>().Object;
            var columnType = new Mock<ISqlType>().Object;
            var parameterPromotedType = new Mock<ISqlType>().Object;

            var context = new Mock<IVisitationContext>();

            context
               .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Field)))
               .Returns((MemberExpression x) => new LinqParameterReference(
                   Expression.Constant(null, x.Type),
                   x.Type,
                   parameterType));
            context
               .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Property)))
               .Returns((MemberExpression x) => new LinqParameterReference(
                    Expression.Constant(null, x.Type),
                    x.Type,
                    columnType));

            var typeProvider = new Mock<ISqlTypeManager>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            typeProvider.Setup(x => x.GetSqlType(columnType, parameterType)).Returns(parameterPromotedType);
            var handler = new EqualLogicalExpressionHandler();
            handler.AttachContext(context.Object);
            var value = "a";
            Expression<Func<UserEntity, bool>> expression = x => x.FirstName == value;

            var sqlExpression = handler.Handle(expression.Body);

            var logicalExpression = sqlExpression.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
            logicalExpression.Right
                .ShouldBeInstanceOf<LinqParameterReference>()
                .SqlType.ShouldBeTheSameInstance(parameterPromotedType);
            logicalExpression.Left
                .ShouldBeInstanceOf<LinqParameterReference>()
                .SqlType.ShouldBeTheSameInstance(parameterPromotedType);
        }

        [TestMethod]
        public void ShouldDetermineOperandsTypeIfRightIsParameter()
        {
            var parameterType = new Mock<ISqlType>().Object;
            var columnType = new Mock<ISqlType>().Object;
            var parameterPromotedType = new Mock<ISqlType>().Object;

            var context = new Mock<IVisitationContext>();

            context
                .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Field)))
                .Returns((MemberExpression x) => new LinqParameterReference(
                    Expression.Constant(null, x.Type),
                    x.Type,
                    parameterType));
            context
                .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Property)))
                .Returns((MemberExpression x) => new LinqColumnReference(
                    new Mock<INamedSource>().Object,
                    ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                    x.Type,
                    columnType));

            var typeProvider = new Mock<ISqlTypeManager>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            typeProvider.Setup(x => x.GetSqlType(columnType, parameterType)).Returns(parameterPromotedType);
            var handler = new EqualLogicalExpressionHandler();
            handler.AttachContext(context.Object);
            var value = "a";
            Expression<Func<UserEntity, bool>> expression = x => x.FirstName == value;

            var sqlExpression = handler.Handle(expression.Body);

            var logicalExpression = sqlExpression.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
            logicalExpression.Right
                .ShouldBeInstanceOf<LinqParameterReference>()
                .SqlType.ShouldBeTheSameInstance(parameterPromotedType);
        }

        [TestMethod]
        public void ShouldDetermineOperandsTypeLeftIsParameter()
        {
            var parameterType = new Mock<ISqlType>().Object;
            var columnType = new Mock<ISqlType>().Object;
            var parameterPromotedType = new Mock<ISqlType>().Object;

            var context = new Mock<IVisitationContext>();

            context
                .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Field)))
                .Returns((MemberExpression x) => new LinqColumnReference(
                    new Mock<INamedSource>().Object,
                    ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                    x.Type,
                    parameterType));
            context
                .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Property)))
                .Returns((MemberExpression x) => new LinqParameterReference(
                    Expression.Constant(null, x.Type),
                    x.Type,
                    columnType));

            var typeProvider = new Mock<ISqlTypeManager>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            typeProvider.Setup(x => x.GetSqlType(columnType, parameterType)).Returns(parameterPromotedType);
            var handler = new EqualLogicalExpressionHandler();
            handler.AttachContext(context.Object);
            var value = "a";
            Expression<Func<UserEntity, bool>> expression = x => x.FirstName == value;

            var sqlExpression = handler.Handle(expression.Body);

            var logicalExpression = sqlExpression.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
            logicalExpression.Left
                .ShouldBeInstanceOf<LinqParameterReference>()
                .SqlType.ShouldBeTheSameInstance(parameterPromotedType);
        }

        /// <summary>
        /// This can occure if eg filtering on single selection query: (..).Select(x=>x.Id).Where(x>9).
        /// </summary>
        [TestMethod]
        public void ShouldAssumeSingleSelectionIfSelectCommandIsOperand()
        {
            var parameterType = new Mock<ISqlType>().Object;
            var columnType = new Mock<ISqlType>().Object;
            var parameterPromotedType = new Mock<ISqlType>().Object;

            var context = new Mock<IVisitationContext>();

            context
                .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Field)))
                .Returns((MemberExpression x) => new SelectCommand
                {
                    Selection = new SqlSelection(
                        new[]
                        {
                            new LinqColumnReference(
                                new Mock<INamedSource>().Object,
                                ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                                x.Type,
                                columnType)
                        })
                });
            context
                .Setup(x => x.VisitExpression(It.Is<MemberExpression>(z => z.Member.MemberType == MemberTypes.Property)))
                .Returns((MemberExpression x) => new LinqParameterReference(
                Expression.Constant(null, x.Type),
                x.Type,
                parameterType));

            var typeProvider = new Mock<ISqlTypeManager>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            typeProvider.Setup(x => x.GetSqlType(parameterType, columnType)).Returns(parameterPromotedType);
            var handler = new EqualLogicalExpressionHandler();
            handler.AttachContext(context.Object);
            var value = "a";
            Expression<Func<UserEntity, bool>> expression = x => x.FirstName == value;

            var sqlExpression = handler.Handle(expression.Body);

            var logicalExpression = sqlExpression.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
            logicalExpression.Left
                .ShouldBeInstanceOf<LinqParameterReference>()
                .SqlType.ShouldBeTheSameInstance(parameterPromotedType);
        }

        [TestMethod]
        public void ShouldVisitLogicalEqualityComparison()
        {
            var expression = Expression.Equal(Expression.Constant(2), Expression.Constant(2));
            var handler = new EqualLogicalExpressionHandler();
            handler.AttachContext(_context.Object);
            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<EqualLogicalExpression>();
            logicalExpression.Left.ShouldNotBeNull();
            logicalExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLogicalGreaterThanComparison()
        {
            var expression = Expression.GreaterThan(Expression.Constant(2), Expression.Constant(2));
            var handler = new GreaterThanLogicalExpressionHandler();
            handler.AttachContext(_context.Object);
            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<GreaterThanLogicalExpression>();
            logicalExpression.Left.ShouldNotBeNull();
            logicalExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLogicalGreaterThanOrEqualComparison()
        {
            var expression = Expression.GreaterThanOrEqual(Expression.Constant(2), Expression.Constant(2));
            var handler = new GreaterThanOrEqualLogicalExpressionHandler();
            handler.AttachContext(_context.Object);
            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<GreaterThanOrEqualLogicalExpression>();
            logicalExpression.Left.ShouldNotBeNull();
            logicalExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLogicalLessThanComparison()
        {
            var expression = Expression.LessThan(Expression.Constant(2), Expression.Constant(2));
            var handler = new LessThanLogicalExpressionHandler();
            handler.AttachContext(_context.Object);
            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<LessThanLogicalExpression>();
            logicalExpression.Left.ShouldNotBeNull();
            logicalExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLogicalLessThanOrEqualComparison()
        {
            var expression = Expression.LessThanOrEqual(Expression.Constant(2), Expression.Constant(2));
            var handler = new LessThanOrEqualLogicalExpressionHandler();
            handler.AttachContext(_context.Object);
            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<LessThanOrEqualLogicalExpression>();
            logicalExpression.Left.ShouldNotBeNull();
            logicalExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLogicalNotEqualityComparison()
        {
            var expression = Expression.NotEqual(Expression.Constant(2), Expression.Constant(2));
            var handler = new NotEqualLogicalExpressionHandler();
            handler.AttachContext(_context.Object);
            var sqlExpression = handler.Handle(expression);
            var logicalExpression = sqlExpression.ShouldBeInstanceOf<NotEqualLogicalExpression>();
            logicalExpression.Left.ShouldNotBeNull();
            logicalExpression.Right.ShouldNotBeNull();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new Mock<IVisitationContext>();
            _context
               .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
               .Returns((ConstantExpression x) =>
               {
                   var mock = new Mock<ISqlValueExpression>();
                   mock.Setup(y => y.ClrType).Returns(x.Type);
                   return mock.Object;
               });
            var typeProvider = new Mock<ISqlTypeManager>();
            _context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
        }
    }
}