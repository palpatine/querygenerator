﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class CountExpressionHandlerTests
    {
        [TestMethod]
        public void CompoundSelectionShouldBeWrappedAsSubQuery()
        {
            var context = new Mock<IVisitationContext>(MockBehavior.Strict);
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var tableQuery = new Mock<ITableQuery<int>>();
            Expression<Func<int>> expression = () => tableQuery.Object.Count();
            var call = (MethodCallExpression)expression.Body;
            var unionSelectionCommand = new UnionSelectionCommand(
                new SelectCommand(
                    selection: new SqlSelection(new[] { new Mock<ISelectionItem>().Object })),
                new SelectCommand(
                    selection: new SqlSelection(new[] { new Mock<ISelectionItem>().Object })));
            context.Setup(x => x.VisitExpression(call.Arguments[0])).Returns(unionSelectionCommand);
            context.Setup(x => x.EnsureSelection(unionSelectionCommand)).Returns(unionSelectionCommand);
            context.Setup(x => x.WrapInSelectCommand(unionSelectionCommand)).Returns(new SelectCommand());

            handler.Handle(call);

            // strict mock
        }

        [TestMethod]
        public void ShouldBeAbleToHandleCount()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<int>> expression = () => CountExtensions.Count<int>(null);
            handler.CanHandle(expression.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleCountDistinctWithInnerExpression()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<int>> expression = () => CountExtensions.CountDistinct<int, int>(null, null);
            handler.CanHandle(expression.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleCountWithInnerExpression()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<int>> expression = () => CountExtensions.Count<int, int>(null, null);
            handler.CanHandle(expression.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleCount()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            var query = new Mock<ITableQuery<int>>().Object;
            Expression<Func<int>> expression = () => query.Count();
            var call = (MethodCallExpression)expression.Body;
            context.Setup(x => x.VisitExpression(call.Arguments[0])).Returns(new SelectCommand());
            context.Setup(x => x.EnsureSelection(It.IsAny<SelectCommand>())).Returns(new SelectCommand());

            var result = handler.Handle(call);

            var selectCommand = result.ShouldBeInstanceOf<SelectCommand>();
            var selection = selectCommand.Selection.ShouldBeInstanceOf<SqlSelection>();
            var countExpression = selection.Elements.Single().ShouldBeInstanceOf<SqlAsteriskCountExpression>();
            countExpression.ClrType.ShouldBeEqual(typeof(int));
            countExpression.Order.ShouldBeNull();
            countExpression.Partition.ShouldBeNull();
            countExpression.SqlType.ShouldBeEqual(intType.Object);
        }

        [TestMethod]
        public void ShouldHandleCountDistinct()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            var innerExpression = new Mock<ISqlValueExpression>().Object;
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            var query = new Mock<ITableQuery<UserEntity>>().Object;
            Expression<Func<int>> expression = () => query.CountDistinct(x => x.FirstName);
            var call = (MethodCallExpression)expression.Body;
            var source = new Mock<INamedSource>();
            var baseQuery = new SelectCommand(source.Object);
            context.Setup(x => x.EnsureSelection(baseQuery)).Returns(baseQuery);
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>())).Returns(innerExpression);
            context.Setup(x => x.VisitExpression(call.Arguments[0])).Returns(baseQuery);

            var result = handler.Handle(call);

            var selectCommand = result.ShouldBeInstanceOf<SelectCommand>();
            var selection = selectCommand.Selection.ShouldBeInstanceOf<SqlSelection>();
            var countExpression = selection.Elements.Single().ShouldBeInstanceOf<SqlCountExpression>();
            countExpression.ClrType.ShouldBeEqual(typeof(int));
            countExpression.IsDistinct.ShouldBeTrue();
            countExpression.Order.ShouldBeNull();
            countExpression.Partition.ShouldBeNull();
            countExpression.Value.ShouldBeTheSameInstance(innerExpression);
            countExpression.SqlType.ShouldBeEqual(intType.Object);
        }

        [TestMethod]
        public void ShouldHandleCountWithInnerExpression()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            var innerExpression = new Mock<ISqlValueExpression>().Object;
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            var query = new Mock<ITableQuery<UserEntity>>().Object;
            Expression<Func<int>> expression = () => query.Count(x => x.LastName);
            var call = (MethodCallExpression)expression.Body;
            var source = new Mock<INamedSource>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>())).Returns(innerExpression);
            context.Setup(x => x.VisitExpression(call.Arguments[0])).Returns(new SelectCommand());
            context.Setup(x => x.EnsureSelection(It.IsAny<SelectCommand>())).Returns(new SelectCommand(source.Object));

            var result = handler.Handle(call);

            var selectCommand = result.ShouldBeInstanceOf<SelectCommand>();
            var selection = selectCommand.Selection.ShouldBeInstanceOf<SqlSelection>();
            var countExpression = selection.Elements.Single().ShouldBeInstanceOf<SqlCountExpression>();
            countExpression.ClrType.ShouldBeEqual(typeof(int));
            countExpression.IsDistinct.ShouldBeFalse();
            countExpression.Order.ShouldBeNull();
            countExpression.Partition.ShouldBeNull();
            countExpression.Value.ShouldBeTheSameInstance(innerExpression);
            countExpression.SqlType.ShouldBeEqual(intType.Object);
        }

        [TestMethod]
        public void ShouldVisitBaseQuery()
        {
            var context = new Mock<IVisitationContext>();
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            var tableQuery = new Mock<ITableQuery<int>>();
            Expression<Func<int>> expression = () => tableQuery.Object.Count();
            var call = (MethodCallExpression)expression.Body;
            context.Setup(x => x.VisitExpression(call.Arguments[0])).Returns(new SelectCommand());
            context.Setup(x => x.EnsureSelection(It.IsAny<SelectCommand>())).Returns(new SelectCommand());

            handler.Handle(call);

            context.Verify(x => x.VisitExpression(call.Arguments[0]), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitInnerExpressionBaseQuery()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CountExpressionHandler();
            handler.AttachContext(context.Object);
            var intType = new Mock<ISqlType>();
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.Int).Returns(intType.Object);
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var tableQuery = new Mock<ITableQuery<int>>();
            Expression<Func<int>> expression = () => tableQuery.Object.Count(x => x);
            var call = (MethodCallExpression)expression.Body;
            var source = new Mock<INamedSource>();
            context.Setup(x => x.VisitExpression(call.Arguments[0])).Returns(new SelectCommand());
            context.Setup(x => x.EnsureSelection(It.IsAny<SelectCommand>())).Returns(new SelectCommand(source.Object));
            context.Setup(x => x.VisitExpression(It.IsAny<ParameterExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);

            handler.Handle(call);

            context.Verify(x => x.VisitExpression(It.IsAny<ParameterExpression>()), Times.Once);
        }
    }
}