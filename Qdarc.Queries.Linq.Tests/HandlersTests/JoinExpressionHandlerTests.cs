﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class JoinExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldBeAbleToHandleFullJoinMethodCall()
        {
            var handler = new JoinEpressionHandler();
            var method = ExpressionExtensions.GetMethod(() => TableQueryFullJoin.FullJoin<UserEntity, UserContactRelation>(null, null, null));
            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.CanHandle(expression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleLeftJoinMethodCall()
        {
            var handler = new JoinEpressionHandler();
            var method = ExpressionExtensions.GetMethod(() => TableQueryLeftJoin.LeftJoin<UserEntity, UserContactRelation>(null, null, null));
            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.CanHandle(expression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleRightJoinMethodCall()
        {
            var handler = new JoinEpressionHandler();
            var method = ExpressionExtensions.GetMethod(() => TableQueryRightJoin.RightJoin<UserEntity, UserContactRelation>(null, null, null));
            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.CanHandle(expression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleJoinMethodCall()
        {
            var handler = new JoinEpressionHandler();
            var method = ExpressionExtensions.GetMethod(() => TableQueryInnerJoin.Join<UserEntity, UserContactRelation>(null, null, null));
            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.CanHandle(expression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldCreateFullJoinIfMethodNameIsFullJoin()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryFullJoin.FullJoin<UserEntity, UserContactRelation>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);
            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Source.ShouldBeInstanceOf<FullJoinSource>();
        }

        [TestMethod]
        public void ShouldCreateInnerJoinIfMethodNameIsJoin()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryInnerJoin.Join<UserEntity, UserContactRelation>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);
            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Source.ShouldBeInstanceOf<InnerJoinSource>();
        }

        [TestMethod]
        public void ShouldCreateLeftJoinIfMethodNameIsLeftJoin()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryLeftJoin.LeftJoin<UserEntity, UserContactRelation>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);
            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Source.ShouldBeInstanceOf<LeftJoinSource>();
        }

        [TestMethod]
        public void ShouldCreateRightJoinIfMethodNameIsRightJoin()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryRightJoin.RightJoin<UserEntity, UserContactRelation>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);
            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Source.ShouldBeInstanceOf<RightJoinSource>();
        }

        [TestMethod]
        public void ShouldCreateTupleGeneratingProjectionByConstructionIfNoProjectionIsProvided()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryInnerJoin.Join<UserEntity, UserContactRelation>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            var projection = select.Selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.ClrType.ShouldBeEqual(typeof(Tuple<UserEntity, UserContactRelation>));
        }

        [TestMethod]
        public void ShouldCreateTupleGeneratingProjectionByConstructionIfNoProjectionIsProvidedForConsequentJoinCall()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, ContactEntity, bool>> on = (u, uc, c) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryInnerJoin.Join<UserEntity, UserContactRelation, ContactEntity>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<Tuple<UserEntity, UserContactRelation>>>().Object, typeof(ITableQuery<Tuple<UserEntity, UserContactRelation>>));
            var right = Expression.Constant(new Mock<ITableQuery<ContactEntity>>().Object, typeof(ITableQuery<ContactEntity>));

            var leftSelectCommand = new SelectCommand(
                selection:
                    new ProjectionByConstructor(
                        typeof(Tuple<int, int>).GetConstructors().Single(),
                        new[] { new Mock<ISelectionItem>().Object, new Mock<ISelectionItem>().Object }),
                source: new CrossJoinSource(
                    new Mock<INamedSource>().Object,
                    new Mock<INamedSource>().Object));
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(leftSelectCommand);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            var projection = select.Selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.ClrType.ShouldBeEqual(typeof(Tuple<UserEntity, UserContactRelation, ContactEntity>));
        }

        [TestMethod]
        public void ShouldCreateTupleGeneratingProjectionByConstructionIfNoProjectionIsProvidedForThirdConsequentJoinCall()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, ContactEntity, UserAccountRelation, bool>> on = (u, uc, c, ua) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(
                () => TableQueryInnerJoin.Join<UserEntity, UserContactRelation, ContactEntity, UserAccountRelation>(null, null, null));
            var left = Expression.Constant(
                new Mock<ITableQuery<Tuple<UserEntity, UserContactRelation, ContactEntity>>>().Object,
                typeof(ITableQuery<Tuple<UserEntity, UserContactRelation, ContactEntity>>));
            var right = Expression.Constant(new Mock<ITableQuery<UserAccountRelation>>().Object, typeof(ITableQuery<UserAccountRelation>));

            var leftSelectCommand = new SelectCommand(
                selection:
                    new ProjectionByConstructor(
                        typeof(Tuple<UserEntity, UserContactRelation, ContactEntity>).GetConstructors().Single(),
                        new[] { new Mock<ISelectionItem>().Object, new Mock<ISelectionItem>().Object, new Mock<ISelectionItem>().Object }),
                source: new CrossJoinSource(
                    new CrossJoinSource(
                        new Mock<INamedSource>().Object,
                        new Mock<INamedSource>().Object),
                    new Mock<INamedSource>().Object));
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(leftSelectCommand);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            var projection = select.Selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.ClrType.ShouldBeEqual(typeof(Tuple<UserEntity, UserContactRelation, ContactEntity, UserAccountRelation>));
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryInnerJoin.Join<UserEntity, UserContactRelation>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.Handle(expression);

            context.Verify(x => x.VisitExpression(left), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitProjection()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            Expression<Func<UserEntity, UserContactRelation, int>> projection = (u, uc) => u.Id;
            var method = ExpressionExtensions.GetMethod(() => TableQueryInnerJoin.Join<UserEntity, UserContactRelation, int>(null, null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on),
                Expression.Quote(projection)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.Handle(expression);

            context.Verify(x => x.VisitExpression(projection.Body), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new JoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, bool>> on = (u, uc) => u.Id == uc.User;
            var method = ExpressionExtensions.GetMethod(() => TableQueryInnerJoin.Join<UserEntity, UserContactRelation>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(on)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(on.Body))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.Handle(expression);

            context.Verify(x => x.VisitExpression(right), Times.Once);
        }
    }
}