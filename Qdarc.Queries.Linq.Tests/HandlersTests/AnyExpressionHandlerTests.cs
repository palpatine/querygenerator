﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class AnyExpressionHandlerTests
    {
        private Mock<IVisitationContext> _context;
        private AnyExpressionHandler _handler;
        private ISqlExpression _sourceParsed;
        private ITableQuery<UserEntity> _sourceStub;

        [TestMethod]
        public void ShouldClaimAbilityToHandleAnyMethod()
        {
            var query = TableQuery.AnyInner(_sourceStub);
            _handler.CanHandle(query).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleCallToAnyMethodOnTableQuery()
        {
            var query = TableQuery.AnyInner(_sourceStub);

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            var inlineIf = castExpression.Expression.ShouldBeInstanceOf<SqlInlineIfExpression>();
            var exists = inlineIf.Condition.ShouldBeInstanceOf<ExistsExpression>();
            exists.IsNegated.ShouldBeFalse();
            exists.Select.ShouldBeInstanceOf<SelectCommand>();
            inlineIf.IfTrue.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(1);
            inlineIf.IfFalse.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(0);
        }

        [TestMethod]
        public void ShouldHandleCallToAnyOnTableReference()
        {
            _sourceParsed = new TableReference(
                new ElementName("Users"),
                typeof(UserEntity));
            var query = TableQuery.AnyInner(_sourceStub);
            _context.Setup(x => x.EnsureSelection(_sourceParsed))
                .Returns(new SelectCommand());

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            var inlineIf = castExpression.Expression.ShouldBeInstanceOf<SqlInlineIfExpression>();
            var exists = inlineIf.Condition.ShouldBeInstanceOf<ExistsExpression>();
            exists.IsNegated.ShouldBeFalse();
            inlineIf.IfTrue.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(1);
            inlineIf.IfFalse.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(0);
        }

        [TestMethod]
        public void ShouldFillSqlTypeForCastExpression()
        {
            _sourceParsed = new TableReference(
                new ElementName("Users"),
                typeof(UserEntity));
            var query = TableQuery.AnyInner(_sourceStub);
            _context.Setup(x => x.EnsureSelection(_sourceParsed))
                .Returns(new SelectCommand());

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            castExpression.SqlType.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldFillClrTypeForCastExpression()
        {
            _sourceParsed = new TableReference(
                new ElementName("Users"),
                typeof(UserEntity));
            var query = TableQuery.AnyInner(_sourceStub);
            _context.Setup(x => x.EnsureSelection(_sourceParsed))
                .Returns(new SelectCommand());

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            castExpression.ClrType.ShouldBeEqual(typeof(bool));
        }

        [TestMethod]
        public void ShouldForceSelectionToBeSingleConstantValue()
        {
            _sourceParsed = new TableReference(
                new ElementName("Users"),
                typeof(UserEntity));
            var query = TableQuery.AnyInner(_sourceStub);
            var selectCommand = new Mock<ISelectCommand>();
            _context.Setup(x => x.EnsureSelection(_sourceParsed))
                .Returns(selectCommand.Object);

            _handler.Handle(query);

            selectCommand.VerifySet(x => x.Selection = It.Is((ISqlSelection z) => VerifySelection(z)));
        }

        [TestMethod]
        public void ShouldHandleAnyOnSelectionSourceThatIsNotSelectCommand()
        {
            _sourceParsed = new UnionSelectionCommand(
                new Mock<ISelectCommand>().Object,
                new Mock<ISelectCommand>().Object);
            var query = TableQuery.AnyInner(_sourceStub);

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            var inlineIf = castExpression.Expression.ShouldBeInstanceOf<SqlInlineIfExpression>();
            var exists = inlineIf.Condition.ShouldBeInstanceOf<ExistsExpression>();
            exists.IsNegated.ShouldBeFalse();
            exists.Select.ShouldBeInstanceOf<UnionSelectionCommand>();
            inlineIf.IfTrue.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(1);
            inlineIf.IfFalse.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(0);
        }

        [TestMethod]
        public void ShouldHandleAnyOnBaseQueryThatIsNotSelectCommandWithFilter()
        {
            var aliasedSelectionItems = new[]
            {
                new AliasedSelectionItem(
                    new LinqColumnReference(
                        new Mock<INamedSource>().Object,
                        ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                        typeof(int),
                        new Mock<ISqlType>().Object))
                {
                    Alias = "Id"
                }
            };
            var union = new UnionSelectionCommand(
                new SelectCommand
                {
                    Selection = new SqlSelection(aliasedSelectionItems, typeof(int))
                },
                new Mock<ISelectCommand>().Object);
            _sourceParsed = union;
            var query = TableQuery.AnyInner(_sourceStub, x => x.Id == 3);
            _context.Setup(x => x.WrapInSelectCommand(_sourceParsed))
                .Returns(
                    new SelectCommand(
                        source: new AliasedSourceItem(union)));

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            var inlineIf = castExpression.Expression.ShouldBeInstanceOf<SqlInlineIfExpression>();
            var exists = inlineIf.Condition.ShouldBeInstanceOf<ExistsExpression>();
            exists.IsNegated.ShouldBeFalse();
            var innerSelect = exists.Select.ShouldBeInstanceOf<SelectCommand>();
            innerSelect.Filter.ShouldNotBeNull();
            inlineIf.IfTrue.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(1);
            inlineIf.IfFalse.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(0);
        }

        [TestMethod]
        public void ShouldHandleAnyWithFilter()
        {
            var query = TableQuery.AnyInner(_sourceStub, x => x.Id == 3);
            _context.Setup(x => x.CanApplyFilterDirectly((SelectCommand)_sourceParsed))
                .Returns(true);

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            var inlineIf = castExpression.Expression.ShouldBeInstanceOf<SqlInlineIfExpression>();
            var exists = inlineIf.Condition.ShouldBeInstanceOf<ExistsExpression>();
            exists.IsNegated.ShouldBeFalse();
            var innerSelect = exists.Select.ShouldBeInstanceOf<SelectCommand>();
            innerSelect.Filter.ShouldNotBeNull();
            inlineIf.IfTrue.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(1);
            inlineIf.IfFalse.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(0);
        }

        [TestMethod]
        public void ShouldHandleAnyWithFilterWhenDirectFilteringIsNotPossible()
        {
            var query = TableQuery.AnyInner(_sourceStub, x => x.Id == 3);
            _context.Setup(x => x.CanApplyFilterDirectly((SelectCommand)_sourceParsed))
                .Returns(false);
            _context.Setup(x => x.WrapInSelectCommand(_sourceParsed))
                .Returns(new SelectCommand());

            var sqlExpression = _handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var castExpression = select.Selection.Elements.Single().ShouldBeInstanceOf<SqlCastExpression>();
            var inlineIf = castExpression.Expression.ShouldBeInstanceOf<SqlInlineIfExpression>();
            var exists = inlineIf.Condition.ShouldBeInstanceOf<ExistsExpression>();
            exists.IsNegated.ShouldBeFalse();
            var innerSelect = exists.Select.ShouldBeInstanceOf<SelectCommand>();
            innerSelect.Filter.ShouldNotBeNull();
            inlineIf.IfTrue.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(1);
            inlineIf.IfFalse.ShouldBeInstanceOf<SqlConstantExpression>().Value.ShouldBeEqual(0);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var typeManager = new Mock<ISqlTypeManager>();
            typeManager.Setup(x => x.Bit).Returns(new Mock<ISqlType>().Object);
            _context = new Mock<IVisitationContext>();
            _context.Setup(x => x.TypeManager)
                .Returns(typeManager.Object);

            _context.Setup(x => x.ProcessSingleParameterFilter(It.IsAny<UnaryExpression>(), It.IsAny<ISelectCommand>()))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            _sourceParsed = new SelectCommand
            {
                Selection = new SqlSelection(
                    new[]
                    {
                        new AsteriskSelection()
                    })
            };
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((Expression x) => _sourceParsed);

            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Equal)))
                .Returns(new Mock<ISqlLogicalExpression>().Object);

            _handler = new AnyExpressionHandler();
            _handler.AttachContext(_context.Object);

            var mock = new Mock<ITableQuery<UserEntity>>();
            mock.Setup(x => x.Expression)
                .Returns(Expression.Constant(null, typeof(ITableQuery<UserEntity>)));
            _sourceStub = mock.Object;
        }

        private static bool VerifySelection(ISqlSelection sqlSelection)
        {
            sqlSelection.ClrType.ShouldNotBeNull();
            sqlSelection.Elements.Single().ShouldBeInstanceOf<SqlConstantExpression>();
            return true;
        }
    }
}