﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class TopExpressionHandlerTests
    {
        private Mock<IVisitationContext> _context;
        private TopExpressionHandler _handler;
        private ISqlExpression _parsedSource;

        [TestInitialize]
        public void TestInitialize()
        {
            _context = new Mock<IVisitationContext>();
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns(
                    (ConstantExpression e) => e.Value is int ? new Mock<ISqlValueExpression>().Object : _parsedSource);

            _handler = new TopExpressionHandler();
            _handler.AttachContext(_context.Object);
        }

        [TestMethod]
        public void TopOnSelectCommand()
        {
            _parsedSource = new SelectCommand();
            var query = CreateTopExpression(3);

            var sqlExpression = _handler.Handle(query);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.ShouldBeTheSameInstance(_parsedSource);
            select.Top.ShouldNotBeNull();
            select.Top.Value.ShouldNotBeNull();
        }

        [TestMethod]
        public void TopOnSelectionSourceThatIsNotSelectCommand()
        {
            _parsedSource = new UnionSelectionCommand(
                new SelectCommand
                {
                    Selection = new Mock<ISqlSelection>().Object
                },
                new Mock<ISelectCommand>().Object);
            var query = CreateTopExpression(3);

            var sqlExpression = _handler.Handle(query);
            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            select.Source.ShouldBeTheSameInstance(_parsedSource);
            select.Top.ShouldNotBeNull();
            select.Top.Value.ShouldNotBeNull();
        }

        private static Expression CreateTopExpression(int count)
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Top<UserEntity>(null, 0));
            var arguments = new[]
            {
                Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>)),
                Expression.Constant(count)
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            return expression;
        }
    }
}