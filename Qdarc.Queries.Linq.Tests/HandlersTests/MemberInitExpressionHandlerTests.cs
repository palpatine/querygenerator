﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class MemberInitExpressionHandlerTests
    {
        [TestMethod]
        public void Projection()
        {
            var context = new Mock<IVisitationContext>();
            context
               .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
               .Returns((ConstantExpression x) =>
                {
                    var mock = new Mock<ISqlValueExpression>();
                    mock.Setup(y => y.ClrType).Returns(x.Type);
                    return mock.Object;
                });
            var projectionByConstructor = new ProjectionByConstructor(
                typeof(Tuple<int>).GetConstructors().Single(),
                new[] { new Mock<ISelectionItem>().Object });
            context
              .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.New)))
              .Returns(projectionByConstructor);

            var handler = new MemberInitializationExpressionHandler();
            handler.AttachContext(context.Object);
            var constructorInfo = typeof(DtoWithMembers).GetConstructors().Single();
            var constructorCall = Expression.New(constructorInfo);
            var idProperty = typeof(DtoWithMembers).GetProperty("Id");
            var idBinding = Expression.Bind(idProperty, Expression.Constant(3));
            var nameProperty = typeof(DtoWithMembers).GetProperty("Name");
            var nameBinding = Expression.Bind(nameProperty, Expression.Constant("name"));
            var expression = Expression.MemberInit(constructorCall, idBinding, nameBinding);

            var sqlExpression = handler.Handle(expression);
            var projection = sqlExpression.ShouldBeInstanceOf<ProjectionByMembersAssignment>();
            projection.ConstructorCall.ShouldNotBeNull();
            projection.Bindings.Count().ShouldBeEqual(2);
            var first = projection.Bindings.First();
            first.Member.ShouldBeEqual(idProperty);
            first.Item.ShouldNotBeNull();
            var second = projection.Bindings.Last();
            second.Member.ShouldBeEqual(nameProperty);
            second.Item.ShouldNotBeNull();
        }
    }
}