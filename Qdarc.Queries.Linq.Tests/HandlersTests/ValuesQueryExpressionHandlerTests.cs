﻿using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class ValuesQueryExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldHandleConstantExpressionContainingValuesQuery()
        {
            var handler = new ValuesQueryConstantExpressionHandler();
            var query = Expression.Constant(null, typeof(ValuesQuery<int>));

            handler.CanHandle(query).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleConstantExpressionContainingValuesQueryByInterface()
        {
            var handler = new ValuesQueryConstantExpressionHandler();
            var query = Expression.Constant(null, typeof(IValuesQuery<int>));

            handler.CanHandle(query).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldProcessValuesQueryOfStrings()
        {
            var typeManager = new Mock<ISqlTypeManager>();
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var sqlType = new Mock<ISqlType>();
            typeManager.Setup(x => x.GetSqlType(typeof(string), "a"))
                .Returns(sqlType.Object);
            typeManager.Setup(x => x.GetSqlType(typeof(string), "b"))
                .Returns(sqlType.Object);
            var handler = new ValuesQueryConstantExpressionHandler();
            handler.AttachContext(context.Object);
            var valuesQuery = new Mock<IValuesQuery<string>>();
            valuesQuery.Setup(x => x.Values)
                .Returns(Expression.Constant(new[] { "a", "b" }));
            var query = Expression.Constant(valuesQuery.Object, typeof(IValuesQuery<string>));
            typeManager.Setup(x => x.GetSqlType(sqlType.Object, sqlType.Object))
                .Returns(sqlType.Object);

            var result = handler.Handle(query);

            var source = result.ShouldBeInstanceOf<AliasedSourceItem>();
            source.ColumnAliases.Single().Alias.ShouldBeEqual("Value");
            source.ColumnAliases.Single().ClrType.ShouldBeEqual(typeof(string));
            source.ColumnAliases.Single().SqlType.ShouldBeTheSameInstance(sqlType.Object);
            var values = source.Source.ShouldBeInstanceOf<ValuesSource>();
            values.ClrType.ShouldBeEqual(typeof(string));
            values.Rows.Count().ShouldBeEqual(2);
            var first = values.Rows.First().Values.Single()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();
            first.ClrType.ShouldBeEqual(typeof(string));
            first.SqlType.ShouldBeEqual(sqlType.Object);
            first.Value.ShouldBeEqual("a");
            var second = values.Rows.Last().Values.Single()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();
            second.ClrType.ShouldBeEqual(typeof(string));
            second.SqlType.ShouldBeEqual(sqlType.Object);
            second.Value.ShouldBeEqual("b");
        }

        [TestMethod]
        public void ShouldProcessValuesQueryOfIntegers()
        {
            var typeManager = new Mock<ISqlTypeManager>();
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var sqlType = new Mock<ISqlType>();
            typeManager.Setup(x => x.GetSqlType(typeof(int), 1))
                .Returns(sqlType.Object);
            typeManager.Setup(x => x.GetSqlType(typeof(int), 2))
                .Returns(sqlType.Object);
            typeManager.Setup(x => x.GetSqlType(sqlType.Object, sqlType.Object))
                .Returns(sqlType.Object);
            var handler = new ValuesQueryConstantExpressionHandler();
            handler.AttachContext(context.Object);
            var valuesQuery = new Mock<IValuesQuery<int>>();
            valuesQuery.Setup(x => x.Values)
                .Returns(Expression.Constant(new[] { 1, 2 }));
            var query = Expression.Constant(valuesQuery.Object, typeof(IValuesQuery<int>));

            var result = handler.Handle(query);

            var source = result.ShouldBeInstanceOf<AliasedSourceItem>();
            source.ColumnAliases.Single().Alias.ShouldBeEqual("Value");
            var values = source.Source.ShouldBeInstanceOf<ValuesSource>();
            values.ClrType.ShouldBeEqual(typeof(int));
            values.Rows.Count().ShouldBeEqual(2);
            var first = values.Rows.First().Values.Single()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();
            first.ClrType.ShouldBeEqual(typeof(int));
            first.SqlType.ShouldBeEqual(sqlType.Object);
            first.Value.ShouldBeEqual(1);
            var second = values.Rows.Last().Values.Single()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();
            second.ClrType.ShouldBeEqual(typeof(int));
            second.SqlType.ShouldBeEqual(sqlType.Object);
            second.Value.ShouldBeEqual(2);
        }

        [TestMethod]
        public void ShouldProcessValuesQueryOfComplexTypes()
        {
            var typeManager = new Mock<ISqlTypeManager>();
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeManager.Object);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableProperties(typeof(NamedEntity)))
                .Returns(typeof(NamedEntity).GetRuntimeProperties());
            var idProperty = ExpressionExtensions.GetProperty((NamedEntity z) => z.Id);
            var nameProperty = ExpressionExtensions.GetProperty((NamedEntity z) => z.Name);
            convention.Setup(x => x.GetColumnName(idProperty))
                .Returns("Id");
            convention.Setup(x => x.GetColumnName(nameProperty))
                .Returns("Name");
            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);
            var intSqlType = new Mock<ISqlType>();
            typeManager.Setup(x => x.GetSqlType(typeof(int), 1))
                .Returns(intSqlType.Object);
            typeManager.Setup(x => x.GetSqlType(typeof(int), 2))
                .Returns(intSqlType.Object);
            var stringSqlType = new Mock<ISqlType>();
            typeManager.Setup(x => x.GetSqlType(typeof(string), "a"))
                .Returns(stringSqlType.Object);
            typeManager.Setup(x => x.GetSqlType(typeof(string), "b"))
                .Returns(stringSqlType.Object);
            typeManager.Setup(x => x.GetSqlType(intSqlType.Object, intSqlType.Object))
                .Returns(intSqlType.Object);
            typeManager.Setup(x => x.GetSqlType(stringSqlType.Object, stringSqlType.Object))
                .Returns(stringSqlType.Object);

            var handler = new ValuesQueryConstantExpressionHandler();
            handler.AttachContext(context.Object);
            var item1 = new NamedEntity { Name = "a", Id = 2 };
            var item2 = new NamedEntity { Name = "b", Id = 1 };
            var valuesQuery = new Mock<IValuesQuery<NamedEntity>>();
            valuesQuery.Setup(x => x.Values)
                .Returns(Expression.Constant(new[] { item1, item2 }));
            var query = Expression.Constant(valuesQuery.Object, typeof(IValuesQuery<NamedEntity>));

            var result = handler.Handle(query);

            var source = result.ShouldBeInstanceOf<AliasedSourceItem>();
            source.ColumnAliases.Select(x => x.Alias).AllCorrespondingElementsShouldBeEqual(new[] { "Id", "Name" });
            var values = source.Source.ShouldBeInstanceOf<ValuesSource>();
            values.ClrType.ShouldBeEqual(typeof(NamedEntity));
            values.Rows.Count().ShouldBeEqual(2);
            var firstName = values.Rows.First().Values.Last()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();
            firstName.ClrType.ShouldBeEqual(typeof(string));
            firstName.SqlType.ShouldBeEqual(intSqlType.Object);
            firstName.Value.ShouldBeEqual("a");
            var firstId = values.Rows.First().Values.First()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();
            firstId.ClrType.ShouldBeEqual(typeof(int));
            firstId.SqlType.ShouldBeEqual(intSqlType.Object);
            firstId.Value.ShouldBeEqual(2);
            var secondId = values.Rows.Last().Values.First()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();
            var secondName = values.Rows.Last().Values.Last()
                .ShouldBeInstanceOf<SqlCastExpression>()
                .Expression
                .ShouldBeInstanceOf<SqlConstantExpression>();

            secondId.ClrType.ShouldBeEqual(typeof(int));
            secondId.SqlType.ShouldBeEqual(intSqlType.Object);
            secondId.Value.ShouldBeEqual(1);
            secondName.ClrType.ShouldBeEqual(typeof(string));
            secondName.SqlType.ShouldBeEqual(stringSqlType.Object);
            secondName.Value.ShouldBeEqual("b");
        }
    }
}