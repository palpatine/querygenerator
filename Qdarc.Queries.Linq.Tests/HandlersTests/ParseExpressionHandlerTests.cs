using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class ParseExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldHandleDateTimeParseMethodCall()
        {
            Expression<Func<DateTime>> expression = () => DateTime.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleInteger32ParseMethodCall()
        {
            Expression<Func<int>> expression = () => int.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleUnsignedInteger32ParseMethodCall()
        {
            Expression<Func<uint>> expression = () => uint.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleInteger16ParseMethodCall()
        {
            Expression<Func<short>> expression = () => short.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleUnsignedInteger16ParseMethodCall()
        {
            Expression<Func<ushort>> expression = () => ushort.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleByteParseMethodCall()
        {
            Expression<Func<byte>> expression = () => byte.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleInteger64ParseMethodCall()
        {
            Expression<Func<long>> expression = () => long.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleUnsignedInteger64ParseMethodCall()
        {
            Expression<Func<ulong>> expression = () => ulong.Parse(string.Empty);
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitInteger64ParseMethodCall()
        {
            Expression<Func<long>> expression = () => long.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typesManager.Setup(x => x.GetSqlType(typeof(long))).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.ClrType.ShouldBeEqual(typeof(long));
            convert.SqlType.ShouldBeTheSameInstance(sqlType);
        }

        [TestMethod]
        public void ShouldVisitUnsignedInteger32ParseMethodCall()
        {
            Expression<Func<uint>> expression = () => uint.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typesManager.Setup(x => x.GetSqlType(typeof(uint))).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.ClrType.ShouldBeEqual(typeof(uint));
            convert.SqlType.ShouldBeTheSameInstance(sqlType);
        }

        [TestMethod]
        public void ShouldVisitUnsignedInteger64ParseMethodCall()
        {
            Expression<Func<ulong>> expression = () => ulong.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typesManager.Setup(x => x.GetSqlType(typeof(ulong))).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.ClrType.ShouldBeEqual(typeof(ulong));
            convert.SqlType.ShouldBeTheSameInstance(sqlType);
        }

        [TestMethod]
        public void ShouldVisitInteger16ParseMethodCall()
        {
            Expression<Func<short>> expression = () => short.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typesManager.Setup(x => x.GetSqlType(typeof(short))).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.ClrType.ShouldBeEqual(typeof(short));
            convert.SqlType.ShouldBeTheSameInstance(sqlType);
        }

        [TestMethod]
        public void ShouldVisitUnsignedInteger16ParseMethodCall()
        {
            Expression<Func<ushort>> expression = () => ushort.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typesManager.Setup(x => x.GetSqlType(typeof(ushort))).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.ClrType.ShouldBeEqual(typeof(ushort));
            convert.SqlType.ShouldBeTheSameInstance(sqlType);
        }

        [TestMethod]
        public void ShouldVisitByteParseMethodCall()
        {
            Expression<Func<byte>> expression = () => byte.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typesManager.Setup(x => x.GetSqlType(typeof(byte))).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.ClrType.ShouldBeEqual(typeof(byte));
            convert.SqlType.ShouldBeTheSameInstance(sqlType);
        }

        [TestMethod]
        public void ShouldVisitInteger32ParseMethodCall()
        {
            Expression<Func<int>> expression = () => int.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var sqlType = new Mock<ISqlType>().Object;
            typesManager.Setup(x => x.GetSqlType(typeof(int))).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.ClrType.ShouldBeEqual(typeof(int));
            convert.SqlType.ShouldBeTheSameInstance(sqlType);
        }

        [TestMethod]
        public void ShouldVisitParsedValueAsExpressionUnderConvert()
        {
            Expression<Func<long>> expression = () => long.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(new Mock<ISqlValueExpression>().Object);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            handler.Handle(memberExpression);
            context.Verify(x => x.VisitExpression(It.IsAny<MemberExpression>()), Times.Once);
        }

        [TestMethod]
        public void ShouldSetExpressionUnderConvert()
        {
            Expression<Func<long>> expression = () => long.Parse(string.Empty);
            var memberExpression = expression.Body;
            var typesManager = new Mock<ISqlTypeManager>();
            var context = new Mock<IVisitationContext>();
            var expressionUderConvert = new Mock<ISqlValueExpression>().Object;
            context.Setup(x => x.VisitExpression(It.IsAny<MemberExpression>()))
                .Returns(expressionUderConvert);
            context.Setup(x => x.TypeManager).Returns(typesManager.Object);
            var handler = new ParseExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(memberExpression);
            var convert = result.ShouldBeInstanceOf<SqlConvertExpression>();
            convert.Expression.ShouldBeTheSameInstance(expressionUderConvert);
        }
    }
}