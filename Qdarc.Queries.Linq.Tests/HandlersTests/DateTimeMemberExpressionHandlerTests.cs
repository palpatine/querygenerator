using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class DateTimeMemberExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldHandleDateTimeStaticMemberExpression()
        {
            Expression<Func<DateTime>> expression = () => DateTime.Now;
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldNotHandleDateTimeNotStaticMemberExpression()
        {
            Expression<Func<DateTime, DateTime>> expression = c => c.Date;
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldNotHandleNotDateTimeStaticMemberExpression()
        {
            Expression<Func<string>> expression = () => string.Empty;
            var memberExpression = expression.Body;
            var context = new Mock<IVisitationContext>();
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            handler.CanHandle(memberExpression).ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldVisitDateTimeNow()
        {
            var typeProvider = new Mock<ISqlTypeManager>();
            var type = new Mock<ISqlType>();
            typeProvider.Setup(x => x.DateTime).Returns(type.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<DateTime>> expression = () => DateTime.Now;
            var memberExpression = expression.Body;
            var result = handler.Handle(memberExpression);
            var call = result.ShouldBeInstanceOf<CurrentDateAndTimeExpression>();
            call.ClrType.ShouldBeEqual(typeof(DateTime));
            call.SqlType.ShouldBeEqual(type.Object);
            call.IsUtc.ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldVisitDateTimeUtcNow()
        {
            var typeProvider = new Mock<ISqlTypeManager>();
            var type = new Mock<ISqlType>();
            typeProvider.Setup(x => x.DateTime).Returns(type.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<DateTime>> expression = () => DateTime.UtcNow;
            var memberExpression = expression.Body;
            var result = handler.Handle(memberExpression);
            var call = result.ShouldBeInstanceOf<CurrentDateAndTimeExpression>();
            call.ClrType.ShouldBeEqual(typeof(DateTime));
            call.SqlType.ShouldBeEqual(type.Object);
            call.IsUtc.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldVisitDateTimeToday()
        {
            var typeProvider = new Mock<ISqlTypeManager>();
            var type = new Mock<ISqlType>();
            typeProvider.Setup(x => x.Date).Returns(type.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<DateTime>> expression = () => DateTime.Today;
            var memberExpression = expression.Body;

            var result = handler.Handle(memberExpression);

            var call = result.ShouldBeInstanceOf<CurrentDateExpression>();
            call.ClrType.ShouldBeEqual(typeof(DateTime));
            call.SqlType.ShouldBeEqual(type.Object);
        }

        [TestMethod]
        public void ShouldVisitDateTimeMinValue()
        {
            var typeProvider = new Mock<ISqlTypeManager>();
            var type = new Mock<ISqlType>();
            typeProvider.Setup(x => x.DateTime).Returns(type.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<DateTime>> expression = () => DateTime.MinValue;
            var memberExpression = expression.Body;
            var result = handler.Handle(memberExpression);
            var call = result.ShouldBeInstanceOf<DateAndTimeMinValueExpression>();
            call.ClrType.ShouldBeEqual(typeof(DateTime));
            call.SqlType.ShouldBeEqual(type.Object);
        }

        [TestMethod]
        public void ShouldVisitDateTimeMaxValue()
        {
            var typeProvider = new Mock<ISqlTypeManager>();
            var type = new Mock<ISqlType>();
            typeProvider.Setup(x => x.DateTime).Returns(type.Object);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new DateTimeMemberExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<DateTime>> expression = () => DateTime.MaxValue;
            var memberExpression = expression.Body;
            var result = handler.Handle(memberExpression);
            var call = result.ShouldBeInstanceOf<DateAndTimeMaxValueExpression>();
            call.ClrType.ShouldBeEqual(typeof(DateTime));
            call.SqlType.ShouldBeEqual(type.Object);
        }
    }
}