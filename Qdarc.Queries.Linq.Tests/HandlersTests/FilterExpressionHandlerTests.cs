using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class FilterExpressionHandlerTests
    {
        private static UnaryExpression _predicateQuote;

        [TestMethod]
        public void ShouldAssignFilterIfBaseQueryDoesNotHaveFilter()
        {
            var selectCommand = new SelectCommand(
                selection: new SqlSelection(
                    new[]
                    {
                        new AsteriskSelection(null, null, null)
                    }),
                source: new AliasedSourceItem(
                    new TableReference(new ElementName("Name"), typeof(UserEntity))));
            var context = new Mock<IVisitationContext>();
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression e) => selectCommand);

            context.Setup(x => x.CanApplyFilterDirectly(selectCommand)).Returns(true);
            var filter = new Mock<ISqlLogicalExpression>().Object;
            context.Setup(x => x.ProcessSingleParameterFilter(It.IsAny<UnaryExpression>(), It.IsAny<ISelectCommand>()))
               .Returns(filter);
            var handler = new FilterExpressionHandler();
            var expression = CreateFilter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Filter.ShouldBeTheSameInstance(filter);
        }

        [TestMethod]
        public void ShouldWrapSelectionIfFilterCannotBeAppliedDirectly()
        {
            var selectCommand = new SelectCommand(
                selection: new SqlSelection(
                    new[]
                    {
                        new AsteriskSelection(null, null, null)
                    }),
                source: new AliasedSourceItem(new TableReference(new ElementName("Name"), typeof(UserEntity))));
            var context = new Mock<IVisitationContext>();
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression e) => selectCommand);

            context.Setup(x => x.CanApplyFilterDirectly(selectCommand)).Returns(false);
            context.Setup(x => x.WrapInSelectCommand(selectCommand))
                .Returns(new SelectCommand(selectCommand));
            var filter = new Mock<ISqlLogicalExpression>().Object;
            context.Setup(x => x.ProcessSingleParameterFilter(It.IsAny<UnaryExpression>(), It.IsAny<ISelectCommand>()))
                .Returns(filter);
            var handler = new FilterExpressionHandler();
            var expression = CreateFilter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Filter.ShouldBeTheSameInstance(filter);
        }

        [TestMethod]
        public void ShouldWrapNonSelectCommandAndApplyFilter()
        {
            var selectCommand = new UnionAllSelectionCommand(
            new SelectCommand(),
            new SelectCommand());
            var context = new Mock<IVisitationContext>();
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression e) => selectCommand);

            context.Setup(x => x.WrapInSelectCommand(selectCommand))
                .Returns(new SelectCommand(selectCommand));
            var filter = new Mock<ISqlLogicalExpression>().Object;
            context.Setup(x => x.ProcessSingleParameterFilter(It.IsAny<UnaryExpression>(), It.IsAny<ISelectCommand>()))
                .Returns(filter);
            var handler = new FilterExpressionHandler();
            var expression = CreateFilter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Filter.ShouldBeTheSameInstance(filter);
        }

        [TestMethod]
        public void ShouldBeAbleToHandleFiltering()
        {
            var handler = new FilterExpressionHandler();
            var expression = CreateFilter();
            handler.CanHandle(expression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldCreateConjunctionIfBaseQueryContainsFilter()
        {
            var sqlBinaryLogicalExpression = new Mock<ISqlLogicalExpression>().Object;
            var selectCommand = new SelectCommand(
                filter: sqlBinaryLogicalExpression);
            var context = new Mock<IVisitationContext>();
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression e) => selectCommand);
            context.Setup(x => x.CanApplyFilterDirectly(selectCommand)).Returns(true);

            var filter = new Mock<ISqlLogicalExpression>().Object;
            context.Setup(x => x.ProcessSingleParameterFilter(It.IsAny<UnaryExpression>(), It.IsAny<ISelectCommand>()))
               .Returns(filter);
            var handler = new FilterExpressionHandler();
            var expression = CreateFilter();
            handler.AttachContext(context.Object);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            var conjunction = select.Filter.ShouldBeInstanceOf<AndLogicalConjunctionExpression>();
            conjunction.Left.ShouldBeTheSameInstance(sqlBinaryLogicalExpression);
            conjunction.Right.ShouldBeTheSameInstance(filter);
        }

        [TestMethod]
        public void ShouldVisitBaseQuery()
        {
            var selectCommand = new SelectCommand(
                selection: new SqlSelection(new[] { new AsteriskSelection(null, null, null) }),
                source: new AliasedSourceItem(new TableReference(new ElementName("Name"), typeof(UserEntity))));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.CanApplyFilterDirectly(selectCommand)).Returns(true);
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression e) => selectCommand);
            var handler = new FilterExpressionHandler();
            var expression = CreateFilter();
            handler.AttachContext(context.Object);

            handler.Handle(expression);

            context.Verify(
                x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)),
                Times.Once);
        }

        [TestMethod]
        public void ShouldVisitFilterExpression()
        {
            SelectCommand selectCommand = new SelectCommand(
                selection: new SqlSelection(new[] { new AsteriskSelection(null, null, null) }),
                source: new AliasedSourceItem(new TableReference(new ElementName("Name"), typeof(UserEntity))));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.CanApplyFilterDirectly(selectCommand)).Returns(true);
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression e) => selectCommand);

            var handler = new FilterExpressionHandler();
            var expression = CreateFilter();
            handler.AttachContext(context.Object);

            handler.Handle(expression);

            context.Verify(
                x => x.ProcessSingleParameterFilter(_predicateQuote, It.IsAny<ISelectCommand>()),
                Times.Once);
        }

        private static MethodCallExpression CreateFilter()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Where<UserEntity>(null, null));
            Expression<Func<UserEntity, bool>> predicate = x => x.Id == 4;
            _predicateQuote = Expression.Quote(predicate);
            var arguments = new Expression[]
            {
                Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object),
                _predicateQuote
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);
            return expression;
        }
    }
}