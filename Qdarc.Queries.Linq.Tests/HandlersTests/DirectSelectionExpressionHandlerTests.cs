﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class DirectSelectionExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldHandleDirectSelectionExpression()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new DirectSelectionExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<int>> call = () => TableQuery.Select(null, x => 1);
            handler.CanHandle(call.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldNotHandleProjectionExpression()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new DirectSelectionExpressionHandler();
            handler.AttachContext(context.Object);
            var tableQuery = new Mock<ITableQuery<UserEntity>>();
            Expression<Func<ITableQuery<int>>> call = () => TableQuery.Select(tableQuery.Object, x => 1);
            handler.CanHandle(call.Body).ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldVisitDirectSelectionExpression()
        {
            Expression<Func<int>> call = () => TableQuery.Select(null, x => 1);
            var context = new Mock<IVisitationContext>();
            var parameter = new Mock<ISqlValueExpression>();
            context.Setup(x => x.VisitExpression(It.IsAny<ConstantExpression>()))
                .Returns(parameter.Object);
            var handler = new DirectSelectionExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call.Body);
            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Selection.ShouldNotBeNull();
            select.Selection.Elements.Single().ShouldBeTheSameInstance(parameter.Object);
        }
    }
}