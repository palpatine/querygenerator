﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class SingleExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldRecognizeCallToMethodSingle()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var methodCallExpression = Expression.Call(method, Expression.Constant(null, typeof(ITableQuery<int>)));
            var context = new Mock<IVisitationContext>();
            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.CanHandle(methodCallExpression);

            result.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldRecognizeCallToMethodSingleOrDefault()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.SingleOrDefault<int>(null));
            var methodCallExpression = Expression.Call(method, Expression.Constant(null, typeof(ITableQuery<int>)));
            var context = new Mock<IVisitationContext>();
            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.CanHandle(methodCallExpression);

            result.ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldNotRecognizeCallToOtherMethod()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Top<int>(null, 0));
            var context = new Mock<IVisitationContext>();
            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.CanHandle(Expression.Call(method, Expression.Constant(null, typeof(ITableQuery<int>)), Expression.Constant(10)));

            result.ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldReturnSelectCommand()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var methodCallExpression = Expression.Call(method, Expression.Constant(null, typeof(ITableQuery<int>)));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.ModelToSqlConvention).Returns(new Mock<IModelToSqlConvention>().Object);
            var linqTableReference = new TableReference(new ElementName("Name"), typeof(UserEntity));
            context.Setup(x => x.VisitExpression(It.IsAny<Expression>())).Returns(linqTableReference);
            var typeProvider = new Mock<ISqlTypeManager>();
            ISqlType type = new Mock<ISqlType>().Object;
            typeProvider.Setup(x => x.GetSqlType(It.Is<ConstantExpression>(z => z.Type == typeof(int)))).Returns(type);
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);

            var result = handler.Handle(methodCallExpression);

            result.ShouldBeInstanceOf<SelectCommand>();
        }

        [TestMethod]
        public void ShouldSetTopClauseOnSelectCommand()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var methodCallExpression = Expression.Call(method, Expression.Constant(null, typeof(ITableQuery<int>)));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.ModelToSqlConvention).Returns(new Mock<IModelToSqlConvention>().Object);
            var linqTableReference = new TableReference(new ElementName("Name"), typeof(UserEntity));
            context.Setup(x => x.VisitExpression(It.IsAny<Expression>())).Returns(linqTableReference);
            var typeProvider = new Mock<ISqlTypeManager>();
            ISqlType type = new Mock<ISqlType>().Object;
            typeProvider.Setup(x => x.GetSqlType(It.Is<ConstantExpression>(z => z.Type == typeof(int)))).Returns(type);
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);

            var result = handler.Handle(methodCallExpression);

            var command = result.ShouldBeInstanceOf<SelectCommand>();
            command.Top.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldSetTopParameterToValueTwo()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var methodCallExpression = Expression.Call(method, Expression.Constant(null, typeof(ITableQuery<int>)));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.ModelToSqlConvention).Returns(new Mock<IModelToSqlConvention>().Object);
            var linqTableReference = new TableReference(new ElementName("Name"), typeof(UserEntity));
            context.Setup(x => x.VisitExpression(It.IsAny<Expression>())).Returns(linqTableReference);
            var typeProvider = new Mock<ISqlTypeManager>();
            ISqlType type = new Mock<ISqlType>().Object;
            typeProvider.Setup(x => x.GetSqlType(It.Is<ConstantExpression>(z => z.Type == typeof(int)))).Returns(type);
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);

            var result = handler.Handle(methodCallExpression);

            var command = result.ShouldBeInstanceOf<SelectCommand>();
            var constant = command.Top.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            constant.Value.ShouldBeEqual(2);
            constant.SqlType.ShouldBeTheSameInstance(type);
        }

        [TestMethod]
        public void ShouldSetTopClauseToUseLiteralValue()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var methodCallExpression = Expression.Call(method, Expression.Constant(null, typeof(ITableQuery<int>)));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.ModelToSqlConvention).Returns(new Mock<IModelToSqlConvention>().Object);
            var linqTableReference = new TableReference(new ElementName("Name"), typeof(UserEntity));
            context.Setup(x => x.VisitExpression(It.IsAny<Expression>())).Returns(linqTableReference);
            var typeProvider = new Mock<ISqlTypeManager>();
            ISqlType type = new Mock<ISqlType>().Object;
            typeProvider.Setup(x => x.GetSqlType(It.Is<ConstantExpression>(z => z.Type == typeof(int)))).Returns(type);
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);
            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);

            var result = handler.Handle(methodCallExpression);

            var command = result.ShouldBeInstanceOf<SelectCommand>();
            command.Top.IsPercent.ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldVisitBaseExpression()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var baseExpression = Expression.Constant(null, typeof(ITableQuery<int>));
            var methodCallExpression = Expression.Call(method, baseExpression);
            var selectCommand = new SelectCommand(
                selection: new ProjectionByConstructor(ExpressionExtensions.GetConstructor(() => new UserEntity()), new List<ISelectionItem>()));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(baseExpression)).Returns(selectCommand);
            var typeProvider = new Mock<ISqlTypeManager>();
            ISqlType type = new Mock<ISqlType>().Object;
            typeProvider.Setup(x => x.GetSqlType(It.Is<ConstantExpression>(z => z.Type == typeof(int)))).Returns(type);
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);

            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);

            handler.Handle(methodCallExpression);

            context.VerifyAll();
        }

        [TestMethod]
        public void ShouldUseSelectCommandFormBaseExpression()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var baseExpression = Expression.Constant(null, typeof(ITableQuery<int>));
            var methodCallExpression = Expression.Call(method, baseExpression);
            var selectCommand = new SelectCommand();
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(baseExpression)).Returns(selectCommand);
            var typeProvider = new Mock<ISqlTypeManager>();
            ISqlType type = new Mock<ISqlType>().Object;
            typeProvider.Setup(x => x.GetSqlType(It.Is<ConstantExpression>(z => z.Type == typeof(int)))).Returns(type);
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);

            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(methodCallExpression);

            result.ShouldBeTheSameInstance(selectCommand);
        }

        [TestMethod]
        public void ShouldWrapTableReferenceInSqlCommand()
        {
            var method = ExpressionExtensions.GetMethod(() => TableQuery.Single<int>(null));
            var baseExpression = Expression.Constant(null, typeof(ITableQuery<int>));
            var methodCallExpression = Expression.Call(method, baseExpression);
            var tableReference = new TableReference(new ElementName("Name"), typeof(UserEntity));
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.ModelToSqlConvention).Returns(new Mock<IModelToSqlConvention>().Object);
            context.Setup(x => x.VisitExpression(baseExpression)).Returns(tableReference);
            var typeProvider = new Mock<ISqlTypeManager>();
            ISqlType type = new Mock<ISqlType>().Object;
            typeProvider.Setup(x => x.GetSqlType(It.Is<ConstantExpression>(z => z.Type == typeof(int)))).Returns(type);
            context.Setup(x => x.TypeManager).Returns(typeProvider.Object);

            var handler = new SingleExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(methodCallExpression);
            var command = result.ShouldBeInstanceOf<SelectCommand>();
            command.Source.ShouldBeTheSameInstance(tableReference);
        }
    }
}
