﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class NotExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldVisitOperand()
        {
            var notExpression = Expression.Not(Expression.Constant(true));
            var handler = new NotExpressionHandler();
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(notExpression.Operand))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            handler.AttachContext(context.Object);

            handler.Handle(notExpression);

            context.Verify(x => x.VisitExpression(notExpression.Operand), Times.Once);
        }

        [TestMethod]
        public void ShouldReturnNegateLogicalExpressionIfOperandIsLogicalExpression()
        {
            var notExpression = Expression.Not(Expression.Constant(true));
            var handler = new NotExpressionHandler();
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.VisitExpression(notExpression.Operand))
                .Returns(new Mock<ISqlLogicalExpression>().Object);
            handler.AttachContext(context.Object);

            handler.Handle(notExpression).ShouldBeInstanceOf<NegateLogicalExpression>();
        }

        [TestMethod]
        public void ShouldAssignOperandExpressionAsInnerExpression()
        {
            var notExpression = Expression.Not(Expression.Constant(true));
            var handler = new NotExpressionHandler();
            var context = new Mock<IVisitationContext>();
            var operandExpression = new Mock<ISqlLogicalExpression>().Object;
            context.Setup(x => x.VisitExpression(notExpression.Operand))
                .Returns(operandExpression);
            handler.AttachContext(context.Object);

            var negate = handler.Handle(notExpression).ShouldBeInstanceOf<NegateLogicalExpression>();
            negate.LogicalExpression.ShouldBeTheSameInstance(operandExpression);
        }

        [TestMethod]
        public void ShouldReturnNegateValueExpressionIfOperandIsValueExpression()
        {
            var notExpression = Expression.Not(Expression.Constant(true));
            var handler = new NotExpressionHandler();
            var context = new Mock<IVisitationContext>();
            var operandExpression = new Mock<ISqlValueExpression>().Object;
            context.Setup(x => x.VisitExpression(notExpression.Operand))
                .Returns(operandExpression);
            handler.AttachContext(context.Object);

            var negate = handler.Handle(notExpression).ShouldBeInstanceOf<NegateValueExpression>();
            negate.ValueExpression.ShouldBeTheSameInstance(operandExpression);
        }
    }
}
