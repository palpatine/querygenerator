﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class ScalarFunctionCallExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldBeAbleToHandleScalarFunctionCall()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new ScalarFunctionCallExpressionHandler();
            handler.AttachContext(context.Object);
            Expression<Func<IDatabase, string>> call = x => x.TestScalarFunction();
            handler.CanHandle(call.Body).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldHandleScalarFunctionCall()
        {
            Expression<Func<IDatabase, string>> call = x => x.TestScalarFunction();
            var sqlType = new Mock<ISqlType>().Object;
            var convention = new Mock<IModelToSqlConvention>();
            var method = call.GetMethod();
            convention.Setup(x => x.GetSqlType(method)).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);
            var handler = new ScalarFunctionCallExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call.Body);
            var scalar = result.ShouldBeInstanceOf<ScalarFunctionCall>();
            scalar.Name[0].ShouldBeEqual("function");
            scalar.SqlType.ShouldBeEqual(sqlType);
            context.Verify(x => x.VisitExpression(It.IsAny<Expression>()), Times.Never);
        }

        [TestMethod]
        public void ShouldHandleScalarFunctionCallWithParameter()
        {
            Expression<Func<IDatabase, string>> call = x => x.TestScalarFunction(5);
            var sqlType = new Mock<ISqlType>().Object;
            var convention = new Mock<IModelToSqlConvention>();
            var method = call.GetMethod();
            convention.Setup(x => x.GetSqlType(method)).Returns(sqlType);
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.ModelToSqlConvention).Returns(convention.Object);
            var parameter = new Mock<ISqlValueExpression>().Object;
            context.Setup(x => x.VisitExpression(It.IsAny<ConstantExpression>())).Returns(parameter);
            var handler = new ScalarFunctionCallExpressionHandler();
            handler.AttachContext(context.Object);
            var result = handler.Handle(call.Body);
            var scalar = result.ShouldBeInstanceOf<ScalarFunctionCall>();
            scalar.Name[0].ShouldBeEqual("function");
            scalar.Arguments.Single().ShouldBeTheSameInstance(parameter);
        }
    }
}
