﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class CrossJoinExpressionHandlerTests
    {
        [TestMethod]
        public void ShouldBeAbleToHandleCrossJoinCall()
        {
            var handler = new CrossJoinEpressionHandler();
            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation>(null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.CanHandle(expression).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldCreateCrossJoinIfMethodNameIsCrossJoin()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation>(null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            select.Source.ShouldBeInstanceOf<CrossJoinSource>();
        }

        [TestMethod]
        public void ShouldCreateTupleGeneratingProjectionByConstructionIfNoProjectionIsProvided()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation>(null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            var projection = select.Selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.ClrType.ShouldBeEqual(typeof(Tuple<UserEntity, UserContactRelation>));
        }

        [TestMethod]
        public void ShouldCreateTupleGeneratingProjectionByConstructionIfNoProjectionIsProvidedForConsequentCrossJoinCall()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation, ContactEntity>(null, null));
            var left = Expression.Constant(new Mock<ITableQuery<Tuple<UserEntity, UserContactRelation>>>().Object, typeof(ITableQuery<Tuple<UserEntity, UserContactRelation>>));
            var right = Expression.Constant(new Mock<ITableQuery<ContactEntity>>().Object, typeof(ITableQuery<ContactEntity>));

            var leftSelectCommand = new SelectCommand(
                selection:
                    new ProjectionByConstructor(
                        typeof(Tuple<int, int>).GetConstructors().Single(),
                        new[] { new Mock<ISelectionItem>().Object, new Mock<ISelectionItem>().Object }),
                source: new CrossJoinSource(
                    new Mock<INamedSource>().Object,
                    new Mock<INamedSource>().Object));

            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(leftSelectCommand);
            var arguments = new Expression[]
            {
                left,
                right,
            };
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            var projection = select.Selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.ClrType.ShouldBeEqual(typeof(Tuple<UserEntity, UserContactRelation, ContactEntity>));
        }

        [TestMethod]
        public void ShouldCreateTupleGeneratingProjectionByConstructionIfNoProjectionIsProvidedForThirdConsequentCrossJoinCall()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            var method = ExpressionExtensions.GetMethod(
                () => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation, ContactEntity, UserAccountRelation>(null, null));
            var left = Expression.Constant(
                new Mock<ITableQuery<Tuple<UserEntity, UserContactRelation, ContactEntity>>>().Object,
                typeof(ITableQuery<Tuple<UserEntity, UserContactRelation, ContactEntity>>));
            var right = Expression.Constant(new Mock<ITableQuery<UserAccountRelation>>().Object, typeof(ITableQuery<UserAccountRelation>));

            var leftSelectCommand = new SelectCommand(
                selection:
                    new ProjectionByConstructor(
                        typeof(Tuple<UserEntity, UserContactRelation, ContactEntity>).GetConstructors().Single(),
                        new[] { new Mock<ISelectionItem>().Object, new Mock<ISelectionItem>().Object, new Mock<ISelectionItem>().Object }),
                source: new CrossJoinSource(
                    new CrossJoinSource(new Mock<INamedSource>().Object, new Mock<INamedSource>().Object),
                    new Mock<INamedSource>().Object));

            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(leftSelectCommand);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var arguments = new Expression[]
            {
                left,
                right,
            };

            var expression = Expression.Call(
                null,
                method,
                arguments);

            var result = handler.Handle(expression);

            var select = result.ShouldBeInstanceOf<SelectCommand>();
            var projection = select.Selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            projection.ClrType.ShouldBeEqual(typeof(Tuple<UserEntity, UserContactRelation, ContactEntity, UserAccountRelation>));
        }

        [TestMethod]
        public void ShouldVisitLeft()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation>(null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.Handle(expression);

            context.Verify(x => x.VisitExpression(left), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitProjection()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            Expression<Func<UserEntity, UserContactRelation, int>> projection = (u, uc) => u.Id;
            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation, int>(null, null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
                Expression.Quote(projection)
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.Handle(expression);

            context.Verify(x => x.VisitExpression(projection.Body), Times.Once);
        }

        [TestMethod]
        public void TupleSelectionOnCrossJoin()
        {
            var context = new Mock<IVisitationContext>();
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns((ConstantExpression c) => new TableReference(new ElementName("Name"), c.Type.GenericTypeArguments[0]));

            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, ContactEntity>(null, null));
            var arguments = new[]
            {
                Expression.Constant(null, typeof(ITableQuery<UserEntity>)),
                Expression.Constant(null, typeof(ITableQuery<ContactEntity>)),
            };

            var query = Expression.Call(
                null,
                method,
                arguments);

            var sqlExpression = handler.Handle(query);

            var select = sqlExpression.ShouldBeInstanceOf<SelectCommand>();
            var rootSelection = select.Selection.ShouldBeInstanceOf<ProjectionByConstructor>();
            rootSelection.Elements.Count().ShouldBeEqual(2);

            var first = rootSelection.Elements.First().ShouldBeInstanceOf<PrefixedSelection>();
            first.Prefix.ShouldBeEqual("Item1");
            var second = rootSelection.Elements.Last().ShouldBeInstanceOf<PrefixedSelection>();
            second.Prefix.ShouldBeEqual("Item2");
        }

        [TestMethod]
        public void ShouldVisitRight()
        {
            var context = new Mock<IVisitationContext>();
            var handler = new CrossJoinEpressionHandler();
            handler.AttachContext(context.Object);

            var method = ExpressionExtensions.GetMethod(() => TableQueryCrossJoin.CrossJoin<UserEntity, UserContactRelation>(null, null));
            var left = Expression.Constant(new Mock<ITableQuery<UserEntity>>().Object, typeof(ITableQuery<UserEntity>));
            var right = Expression.Constant(new Mock<ITableQuery<UserContactRelation>>().Object, typeof(ITableQuery<UserContactRelation>));
            var arguments = new Expression[]
            {
                left,
                right,
            };
            context.Setup(x => x.GenerateExplicitSelectionList(It.IsAny<ISource>()))
                .Returns(new Mock<ISqlSelection>().Object);
            context.Setup(x => x.VisitExpression(left))
                .Returns(new Mock<ISource>().Object);
            context.Setup(x => x.VisitExpression(right))
                .Returns(new Mock<ISource>().Object);
            var expression = Expression.Call(
                null,
                method,
                arguments);

            handler.Handle(expression);

            context.Verify(x => x.VisitExpression(right), Times.Once);
        }
    }
}
