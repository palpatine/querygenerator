﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Handlers;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Queries.Linq.Tests.HandlersTests
{
    [TestClass]
    public class BinaryExpressionHandlerTests
    {
        private ConstantExpression _firstConstant;
        private ISqlType _firstType;
        private ConstantExpression _secondConstant;
        private ISqlType _secondType;
        private Mock<ISqlTypeManager> _typesProviderMock;
        private Mock<IVisitationContext> _context;

        [TestMethod]
        public void ShouldVisitLinqBitwiseAndOperation()
        {
            var queryPart = Expression.And(_firstConstant, _secondConstant);
            var handler = new BitwiseAndArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);

            var sqlExpression = parseResult.ShouldBeInstanceOf<BitwiseAndValueExpression>();
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldDetermineSqlTypeWhileVisitingBinaryOperation()
        {
            var queryPart = Expression.And(_firstConstant, _secondConstant);
            var handler = new BitwiseAndArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);

            var sqlExpression = parseResult.ShouldBeInstanceOf<SqlBinaryValueExpression>();
            _typesProviderMock.Verify(c => c.GetSqlType(_firstType, _secondType), Times.Once);
            sqlExpression.SqlType.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLinqBitwiseExclusiveOrOperation()
        {
            var queryPart = Expression.ExclusiveOr(_firstConstant, _secondConstant);
            var handler = new ExclusiveOrBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);

            var sqlExpression = parseResult.ShouldBeInstanceOf<BitwiseExclusiveOrValueExpression>();
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldBeAbleToHandleBitwiseExclusiveOrOperation()
        {
            var queryPart = Expression.ExclusiveOr(_firstConstant, _secondConstant);
            var handler = new ExclusiveOrBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            handler.CanHandle(queryPart).ShouldBeTrue();
        }

        [TestMethod]
        public void ShouldNotBeAbleToHandleLogicalExclusiveOrOperation()
        {
            var queryPart = Expression.ExclusiveOr(Expression.Constant(true), Expression.Constant(false));
            var handler = new ExclusiveOrBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            handler.CanHandle(queryPart).ShouldBeFalse();
        }

        [TestMethod]
        public void ShouldVisitLinqBitwiseOrOperation()
        {
            var queryPart = Expression.Or(_firstConstant, _secondConstant);
            var handler = new BitwiseOrArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<BitwiseOrValueExpression>();
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLinqDivisionOperation()
        {
            var queryPart = Expression.Divide(_firstConstant, _secondConstant);
            var handler = new DivideBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<DivideValueExpression>();
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLinqModuloOperation()
        {
            var queryPart = Expression.Modulo(_firstConstant, _secondConstant);
            var handler = new ModuloBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<ModuloValueExpression>();
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLinqMultiplicationOperation()
        {
            var queryPart = Expression.Multiply(_firstConstant, _secondConstant);
            var handler = new MultiplyBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<MultiplyValueExpression>();
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLinqSubtractionOperation()
        {
            var queryPart = Expression.Subtract(_firstConstant, _secondConstant);
            var handler = new SubtractBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<SubtractValueExpression>();
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldVisitLinqSumOperation()
        {
            var queryPart = Expression.Add(_firstConstant, _secondConstant);
            var handler = new AddBinaryArithmeticExpressionHandler();
            handler.AttachContext(_context.Object);

            var parseResult = handler.Handle(queryPart);
            var sqlExpression = parseResult.ShouldBeInstanceOf<AddValueExpression>();
            sqlExpression.ClrType.ShouldBeEqual(typeof(int));
            sqlExpression.Left.ShouldNotBeNull();
            sqlExpression.Right.ShouldNotBeNull();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _firstConstant = Expression.Constant(6);
            _secondConstant = Expression.Constant(54);
            _typesProviderMock = new Mock<ISqlTypeManager>();
            _firstType = new Mock<ISqlType>().Object;
            _secondType = new Mock<ISqlType>().Object;
            _typesProviderMock
                .Setup(x => x.GetSqlType(_firstType, _secondType))
                .Returns(new Mock<ISqlType>().Object);

            _context = new Mock<IVisitationContext>();
            _context.Setup(x => x.TypeManager).Returns(_typesProviderMock.Object);
            _context
                .Setup(x => x.VisitExpression(It.Is<Expression>(z => z.NodeType == ExpressionType.Constant)))
                .Returns(
                    (ConstantExpression x) =>
                    {
                        var mock = new Mock<ISqlValueExpression>();
                        mock.Setup(y => y.SqlType).Returns(x == _firstConstant ? _firstType : _secondType);
                        return mock.Object;
                    });
        }
    }
}