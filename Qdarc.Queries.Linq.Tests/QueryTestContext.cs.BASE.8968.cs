using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Moq;
using Qdarc.Sql.Connector;
using Qdarc.Sql.Formatter;
using Qdarc.Sql.Linq.Materialization;
using Qdarc.Sql.Linq.Queries;
using Qdarc.Sql.Model;
using Qdarc.Sql.Model.Linq.References;
using Qdarc.Sql.Model.Names;

namespace Qdarc.Sql.Linq.Tests
{
    internal sealed class QueryTestContext
    {
        private QueryTestContext(Func<IVisitationContext, IEnumerable<ILinqExpressionHandler>> handlers = null)
        {
            TypeProviderMock = new Mock<ISqlTypeManager>();
            ModelToSqlConventionMock = new Mock<IModelToSqlConvention>();
            ModelToSqlConventionMock = new Mock<IModelToSqlConvention>();
            DatabaseConnectionProviderMock = new Mock<IDatabaseConnectionProvider>();
            ObjectMapperMock = new Mock<IObjectMapperSelector>();
            FormatingContextMock = new Mock<IFormattingContext>();
            VisitationContext = VisitationContextCreator.CreateContext(handlers, TypeProviderMock, ModelToSqlConventionMock);
        }

        public Mock<IDatabaseConnectionProvider> DatabaseConnectionProviderMock { get; }

        public Mock<IFormattingContext> FormatingContextMock { get; }

        public Mock<IModelToSqlConvention> ModelToSqlConventionMock { get; }

        public Mock<IObjectMapperSelector> ObjectMapperMock { get; }

        public Mock<ISqlTypeManager> TypeProviderMock { get; }

        public IVisitationContext VisitationContext { get; }

        public static QueryTestContext Create(Func<IVisitationContext, IEnumerable<ILinqExpressionHandler>> handlers)
        {
            var context = new QueryTestContext(handlers);

            context.ModelToSqlConventionMock
               .Setup(x => x.GetTableName(It.IsAny<Type>()))
               .Returns((Type x) => new TableName(null, null, x.Name));

            context.ModelToSqlConventionMock
                .Setup(x => x.GetSelectableColumns(It.IsAny<Type>()))
                .Returns((Type x) => x.GetRuntimeProperties().Select(z => new LinqColumnReference { Property = z }));

            return context;
        }

        public ITableQuery<TEntity> Query<TEntity>()
        {
            var query = new TableQuery<TEntity>(CreateProvider());
            return query;
        }

        private TableQueryProvider CreateProvider()
        {
            return new TableQueryProvider(
                DatabaseConnectionProviderMock.Object,
                () => VisitationContext,
                () => FormatingContextMock.Object,
                ObjectMapperMock.Object,
                new Mock<IParametersBinder>().Object);
        }
    }
}