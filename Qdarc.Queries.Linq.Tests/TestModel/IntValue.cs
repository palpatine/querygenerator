﻿using Qdarc.Queries.Linq.Annotations;

namespace Qdarc.Queries.Linq.Tests.TestModel
{
    [UserDefinedTableTypeName("IntList", "dbo")]
    public class IntValue
    {
        public IntValue(int value)
        {
            Value = value;
        }

        public int Value { get; set; }
    }
}
