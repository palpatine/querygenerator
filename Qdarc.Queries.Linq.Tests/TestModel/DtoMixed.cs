﻿namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public class DtoMixed
    {
        public DtoMixed(int id)
        {
            Id = id;
        }

        public int Id { get; }

        public string Name { get; set; }
    }
}