﻿namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public class DtoWithConstructor
    {
        public DtoWithConstructor(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; }

        public string Name { get; }
    }
}