﻿using Qdarc.Queries.Linq.Annotations;

namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public static class TestScalarFunctionProvider
    {
        [ScalarFunction("function")]
        public static string TestScalarFunction(this IDatabase database)
        {
            return database.ScalarFunction(x => x.TestScalarFunction());
        }

        [ScalarFunction("function")]
        public static string TestScalarFunction(this IDatabase database, int parameter)
        {
            return database.ScalarFunction(x => x.TestScalarFunction(parameter));
        }
    }
}