﻿namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public interface IMarker
    {
        int Id { get; set; }
    }
}