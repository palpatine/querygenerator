﻿using System;

namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public class ContactEntity : IScopedEntity
    {
        public int? Account { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public int ParentId { get; set; }

        public int Scope { get; set; }

        public DateTime? VersionEndDate { get; set; }

        public DateTime VersionStartDate { get; set; }
    }
}