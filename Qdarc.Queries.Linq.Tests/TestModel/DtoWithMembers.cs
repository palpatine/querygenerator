﻿namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public class DtoWithMembers
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}