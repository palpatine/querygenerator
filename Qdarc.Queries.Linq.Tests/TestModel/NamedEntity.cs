namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public class NamedEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}