﻿using System;
using System.Runtime.Remoting.Contexts;

namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public class UserEntity : IUser
    {
        public IContributeServerContextSink Context { get; set; }

        public string FirstName { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        public string LastName { get; set; }

        public IUser Parent { get; set; }

        public int? ParentId { get; set; }

        public DateTime WorkEnd { get; set; }
    }
}