﻿namespace Qdarc.Queries.Linq.Tests.TestModel
{
    public interface IScopedEntity
    {
        int Scope { get; set; }
    }
}