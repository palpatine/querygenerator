﻿using System;

namespace Qdarc.Queries.Linq.Tests.TestModel
{
    [Flags]
    public enum Modes
    {
        None = 0,
        Value1 = 1,
        Value2 = 2,
        Value3 = 4,
    }
}