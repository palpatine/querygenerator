﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators.ExplicitSelectionListTransformationHandler;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Tests.ExplicitSelectionListGenerators
{
    [TestClass]
    public class AliasedSourceItemSelectionListResolverTests
    {
        [TestMethod]
        public void ShouldAssignWrapperSourceIfNotAlreadySet()
        {
            var handler = new AliasedSourceItemSelectionListResolver();
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var aliasedSource = new AliasedSourceItem(new Mock<ISource>().Object);

            handler.Handle(aliasedSource, transformer.Object);

            transformer.VerifySet(x => x.WrapperSource = aliasedSource, Times.Once);
        }

        [TestMethod]
        public void ShouldNotAssignWrapperSourceIfAlreadySet()
        {
            var handler = new AliasedSourceItemSelectionListResolver();
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var aliasedSource = new AliasedSourceItem(new Mock<ISource>().Object);
            transformer.SetupProperty(x => x.WrapperSource, new Mock<INamedSource>().Object);

            handler.Handle(aliasedSource, transformer.Object);

            transformer.VerifySet(x => x.WrapperSource = aliasedSource, Times.Never);
        }

        [TestMethod]
        public void ShouldTransformSource()
        {
            var handler = new AliasedSourceItemSelectionListResolver();
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var source = new Mock<ISource>();
            var aliasedSource = new AliasedSourceItem(source.Object);

            handler.Handle(aliasedSource, transformer.Object);

            source.Verify(x => x.Accept<ISqlSelection>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldCreateSqlSelectionForStringSelection()
        {
            var handler = new AliasedSourceItemSelectionListResolver();
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var source = new Mock<ISource>();
            source.Setup(x => x.ClrType).Returns(typeof(string));
            var aliasedSource = new AliasedSourceItem(
                source.Object,
                new[]
                {
                    new ColumnAlias(
                        "a",
                        typeof(string),
                        new Mock<ISqlType>().Object)
                });

            var result = handler.Handle(aliasedSource, transformer.Object);
            result.ShouldBeInstanceOf<SqlSelection>();
        }

        [TestMethod]
        public void ShouldCreateSqlSelectionForIntegerSelection()
        {
            var handler = new AliasedSourceItemSelectionListResolver();
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var source = new Mock<ISource>();
            source.Setup(x => x.ClrType).Returns(typeof(int));
            var aliasedSource = new AliasedSourceItem(
                source.Object,
                new[]
                {
                    new ColumnAlias(
                        "a",
                        typeof(string),
                        new Mock<ISqlType>().Object)
                });

            var result = handler.Handle(aliasedSource, transformer.Object);
            result.ShouldBeInstanceOf<SqlSelection>();
        }

        [TestMethod]
        public void ShouldCreateProjectionByConstructorForTypeThatDeclaresOneWithParameters()
        {
            var handler = new AliasedSourceItemSelectionListResolver();
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var source = new Mock<ISource>();
            source.Setup(x => x.ClrType).Returns(typeof(Tuple<int, string>));
            var aliasedSource = new AliasedSourceItem(
                source.Object,
                new[]
                {
                    new ColumnAlias(
                         "Item1",
                         typeof(int),
                         new Mock<ISqlType>().Object),
                    new ColumnAlias(
                        "Item2",
                        typeof(string),
                        new Mock<ISqlType>().Object)
                });

            var result = handler.Handle(aliasedSource, transformer.Object);
            result.ShouldBeInstanceOf<ProjectionByConstructor>();
        }

        [TestMethod]
        public void ShouldCreateSqlSelectionForTypeThatDoNotDeclareConstructorWithParameters()
        {
            var handler = new AliasedSourceItemSelectionListResolver();
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var source = new Mock<ISource>();
            source.Setup(x => x.ClrType).Returns(typeof(DtoWithMembers));
            var aliasedSource = new AliasedSourceItem(
                source.Object,
                new[]
                {
                    new ColumnAlias(
                        "Id",
                        typeof(int),
                        new Mock<ISqlType>().Object),
                    new ColumnAlias(
                        "Name",
                        typeof(string),
                        new Mock<ISqlType>().Object)
                });

            var result = handler.Handle(aliasedSource, transformer.Object);
            result.ShouldBeInstanceOf<SqlSelection>();
        }
    }
}
