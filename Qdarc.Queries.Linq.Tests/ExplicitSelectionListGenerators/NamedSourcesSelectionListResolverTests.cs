﻿using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators.ExplicitSelectionListTransformationHandler;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.ExplicitSelectionListGenerators
{
    [TestClass]
    public class NamedSourcesSelectionListResolverTests
    {
        [TestMethod]
        public void ShouldRetrieveColumnsBasedOnClrTypeForLinqTableParameterReference()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            var expression = new LinqTableParameterReference(Expression.Constant(4), type, new Mock<ISqlType>().Object);
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                    .Returns(
                        new[]
                        {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                        });
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            handler.Handle(expression, transformer.Object);

            convention.Verify(x => x.GetSelectableColumns(wrapperSource), Times.Once);
        }

        [TestMethod]
        public void ShouldRetrieveColumnsBasedOnClrTypeForTabularFunctionCall()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var expression = new TabularFunctionCall(
                new ElementName("Name"),
                Enumerable.Empty<ISqlValueExpression>(),
                type);
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            handler.Handle(expression, transformer.Object);

            convention.Verify(x => x.GetSelectableColumns(wrapperSource), Times.Once);
        }

        [TestMethod]
        public void ShouldRetrieveColumnsBasedOnClrTypeForTableReference()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var expression = new TableReference(new ElementName("Name"), type);
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            handler.Handle(expression, transformer.Object);

            convention.Verify(x => x.GetSelectableColumns(wrapperSource), Times.Once);
        }

        [TestMethod]
        public void ShouldAssignSourceFromTransformerIfItsNotNullForLinqTableParameterReference()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var expression = new LinqTableParameterReference(Expression.Constant(4), type, new Mock<ISqlType>().Object);
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ShouldBeInstanceOf<SqlSelection>()
                .Elements.Single()
                .ShouldBeInstanceOf<AliasedSelectionItem>()
                .Item.ShouldBeInstanceOf<LinqColumnReference>()
                .Source.ShouldBeTheSameInstance(transformer.Object.WrapperSource);
        }

        [TestMethod]
        public void ShouldAssignSourceFromTransformerIfItsNotNullForTabularFunctionCall()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var expression = new TabularFunctionCall(
                new ElementName("Name"),
                Enumerable.Empty<ISqlValueExpression>(),
                type);
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ShouldBeInstanceOf<SqlSelection>()
                .Elements.Single()
                .ShouldBeInstanceOf<AliasedSelectionItem>()
                .Item.ShouldBeInstanceOf<LinqColumnReference>()
                .Source.ShouldBeTheSameInstance(transformer.Object.WrapperSource);
        }

        [TestMethod]
        public void ShouldAssignSourceFromTransformerIfItsNotNullForLinqColumnReference()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var expression = new TableReference(new ElementName("Name"), type);
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ShouldBeInstanceOf<SqlSelection>()
                .Elements.Single()
                .ShouldBeInstanceOf<AliasedSelectionItem>()
                .Item.ShouldBeInstanceOf<LinqColumnReference>()
                .Source.ShouldBeTheSameInstance(transformer.Object.WrapperSource);
        }

        [TestMethod]
        public void ShouldAssignSourceAsExpressionIfITransformerDoesNotProvideOneForTabularFunctionCall()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            transformer.SetupProperty(x => x.WrapperSource, null);
            var type = typeof(UserEntity);
            var expression = new TabularFunctionCall(
                new ElementName("Name"),
                Enumerable.Empty<ISqlValueExpression>(),
                type);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(expression))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            expression,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ShouldBeInstanceOf<SqlSelection>()
                .Elements.Single()
                .ShouldBeInstanceOf<AliasedSelectionItem>()
                .Item.ShouldBeInstanceOf<LinqColumnReference>()
                .Source.ShouldBeTheSameInstance(expression);
        }

        [TestMethod]
        public void ShouldAssignSourceAsExpressionIfITransformerDoesNotProvideOneForLinqColumnReference()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            transformer.SetupProperty(x => x.WrapperSource, null);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            var expression = new TableReference(new ElementName("Name"), type);
            convention.Setup(x => x.GetSelectableColumns(expression))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            expression,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ShouldBeInstanceOf<SqlSelection>()
                .Elements.Single()
                .ShouldBeInstanceOf<AliasedSelectionItem>()
                .Item.ShouldBeInstanceOf<LinqColumnReference>()
                .Source.ShouldBeTheSameInstance(expression);
        }

        [TestMethod]
        public void ShouldReturnSqlSelectionForTypeNotDefiningParameterizedConstructor()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(UserEntity);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var expression = new TableReference(new ElementName("Name"), type);
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ShouldBeInstanceOf<SqlSelection>();
        }

        [TestMethod]
        public void ShouldReturnConstructorProjectionForTypeDefiningParameterizedConstructor()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var wrapperSource = new Mock<INamedSource>().Object;
            transformer.SetupProperty(x => x.WrapperSource, wrapperSource);
            var type = typeof(DtoWithConstructor);
            var convention = new Mock<IModelToSqlConvention>();
            convention.Setup(x => x.GetSelectableColumns(wrapperSource))
                .Returns(
                    new[]
                    {
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((DtoWithConstructor u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object),
                        new LinqColumnReference(
                            wrapperSource,
                            ExpressionExtensions.GetProperty((DtoWithConstructor u) => u.Id),
                            typeof(int),
                            new Mock<ISqlType>().Object)
                    });
            var expression = new TableReference(new ElementName("Name"), type);
            var handler = new NamedSourcesSelectionListResolver(convention.Object);

            var result = handler.Handle(expression, transformer.Object);

            result.ShouldBeInstanceOf<ProjectionByConstructor>();
        }
    }
}