﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators;
using Qdarc.Queries.Linq.ExplicitSelectionListGenerators.ExplicitSelectionListTransformationHandler;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Queries.Linq.Tests.ExplicitSelectionListGenerators
{
    [TestClass]
    public class SelectCommandsSelectionListResolverTests
    {
        [TestMethod]
        public void ShouldReturnSelectionFromSelectCommandIfWrapperSourceIsNull()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var handler = new SelectCommandsSelectionListResolver();
            var sqlSelection = new Mock<ISqlSelection>().Object;
            var command = new SelectCommand(
                selection: sqlSelection);

            var result = handler.Handle(command, transformer.Object);

            result.ShouldBeTheSameInstance(sqlSelection);
        }

        [TestMethod]
        public void ShouldReturnSelectionFromUnionSelectionCommandIfWrapperSourceIsNull()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var handler = new SelectCommandsSelectionListResolver();
            var sqlSelection = new Mock<ISqlSelection>().Object;
            var left = new Mock<ISelectCommand>();
            left.Setup(x => x.Selection).Returns(sqlSelection);
            var command = new UnionSelectionCommand(left.Object, new Mock<ISelectCommand>().Object);

            var result = handler.Handle(command, transformer.Object);

            result.ShouldBeTheSameInstance(sqlSelection);
        }

        [TestMethod]
        public void ShouldReturnSelectionFromUnionAllSelectionCommandIfWrapperSourceIsNull()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            var handler = new SelectCommandsSelectionListResolver();
            var sqlSelection = new Mock<ISqlSelection>().Object;
            var left = new Mock<ISelectCommand>();
            left.Setup(x => x.Selection).Returns(sqlSelection);
            var command = new UnionAllSelectionCommand(left.Object, new Mock<ISelectCommand>().Object);

            var result = handler.Handle(command, transformer.Object);

            result.ShouldBeTheSameInstance(sqlSelection);
        }

        [TestMethod]
        public void ShouldVisitSelectionFromSelectCommandIfWrapperSourceIsNotNull()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            transformer.SetupProperty(x => x.WrapperSource, new Mock<INamedSource>().Object);
            var handler = new SelectCommandsSelectionListResolver();
            var sqlSelection = new Mock<ISqlSelection>();
            var command = new SelectCommand
            {
                Selection = sqlSelection.Object
            };

            handler.Handle(command, transformer.Object);

            sqlSelection.Verify(x => x.Accept<ISqlSelection>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitReturnSelectionFromUnionSelectionCommandIfWrapperSourceIsNotNull()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            transformer.SetupProperty(x => x.WrapperSource, new Mock<INamedSource>().Object);
            var handler = new SelectCommandsSelectionListResolver();
            var sqlSelection = new Mock<ISqlSelection>();
            var left = new Mock<ISelectCommand>();
            left.Setup(x => x.Selection).Returns(sqlSelection.Object);
            var command = new UnionSelectionCommand(left.Object, new Mock<ISelectCommand>().Object);

            handler.Handle(command, transformer.Object);

            sqlSelection.Verify(x => x.Accept<ISqlSelection>(transformer.Object), Times.Once);
        }

        [TestMethod]
        public void ShouldVisitReturnSelectionFromUnionAllSelectionCommandIfWrapperSourceIsNotNull()
        {
            var transformer = new Mock<IExplicitSelectionListTransformer>();
            transformer.SetupProperty(x => x.WrapperSource, new Mock<INamedSource>().Object);
            var handler = new SelectCommandsSelectionListResolver();
            var sqlSelection = new Mock<ISqlSelection>();
            var left = new Mock<ISelectCommand>();
            left.Setup(x => x.Selection).Returns(sqlSelection.Object);
            var command = new UnionAllSelectionCommand(left.Object, new Mock<ISelectCommand>().Object);

            handler.Handle(command, transformer.Object);

            sqlSelection.Verify(x => x.Accept<ISqlSelection>(transformer.Object), Times.Once);
        }
    }
}
