﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Extensions;
using Qdarc.Queries.Linq.Tests.TestModel;

namespace Qdarc.Queries.Linq.Tests.Extensions
{
    [TestClass]
    public class QueryParameterValueConverterFactoryTests
    {
        [TestMethod]
        public void ShouldReturnNullIfNoConvertersRegisteredFroGivenType()
        {
            var factory = new QueryParameterValueConverterFactory(Enumerable.Empty<IQueryParameterValueConverter>());
            var converter = factory.Resolve(typeof(int));
            converter.ShouldBeNull();
        }

        [TestMethod]
        public void ShouldReturnConverterRegisteredFroGivenType()
        {
            var converterMock = new Mock<IQueryParameterValueConverter>();
            converterMock.Setup(x => x.HandledType).Returns(typeof(int));
            var factory = new QueryParameterValueConverterFactory(
                new[] { converterMock.Object });
            var converter = factory.Resolve(typeof(int));
            converter.ShouldBeTheSameInstance(converterMock.Object);
        }

        [TestMethod]
        public void ShouldReturnConverterThatMatchesInterfaceImplementedByRequestedType()
        {
            var converterMock = new Mock<IQueryParameterValueConverter>();
            converterMock.Setup(x => x.HandledType).Returns(typeof(IMarker));
            var factory = new QueryParameterValueConverterFactory(
                new[] { converterMock.Object });
            var converter = factory.Resolve(typeof(UserEntity));
            converter.ShouldBeTheSameInstance(converterMock.Object);
        }

        [TestMethod]
        public void ShouldReturnConverterThatMatchesInterfaceImplementedByRequestedTypeWithHighestPriority()
        {
            var markerConverterMock = new Mock<IQueryParameterValueConverter>();
            markerConverterMock.Setup(x => x.HandledType).Returns(typeof(IMarker));
            markerConverterMock.Setup(x => x.Priority).Returns(9);
            var userConverterMock = new Mock<IQueryParameterValueConverter>();
            userConverterMock.Setup(x => x.HandledType).Returns(typeof(IUser));
            userConverterMock.Setup(x => x.Priority).Returns(10);
            var factory = new QueryParameterValueConverterFactory(
                new[] { markerConverterMock.Object, userConverterMock.Object });
            var converter = factory.Resolve(typeof(UserEntity));
            converter.ShouldBeTheSameInstance(userConverterMock.Object);
        }

        [TestMethod]
        public void ShouldNotAllowRegistrationOfMultipleConvertersHandlingTheSameType()
        {
            var markerConverterMock = new Mock<IQueryParameterValueConverter>();
            markerConverterMock.Setup(x => x.HandledType).Returns(typeof(IMarker));
            var userConverterMock = new Mock<IQueryParameterValueConverter>();
            userConverterMock.Setup(x => x.HandledType).Returns(typeof(IMarker));
            QAssert.ShouldThrow<InvalidOperationException>(
                () => new QueryParameterValueConverterFactory(new[] { markerConverterMock.Object, userConverterMock.Object }));
        }
    }
}
