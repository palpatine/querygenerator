﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Queries.Linq.Tests.Queries
{
    [TestClass]
    public class FunctionQueryTests
    {
        [TestMethod]
        public void CanCreateFunctionQuery()
        {
            var provider = new Mock<ITableQueryProvider>();
            var name = new ElementName("name");
            var arguments = Enumerable.Empty<ConstantExpression>().ToArray();
            var functionQuery = new FunctionQuery<UserEntity>(
                provider.Object,
                name,
                arguments);

            var expression = functionQuery.Expression.ShouldBeInstanceOf<ConstantExpression>();
            expression.Value.ShouldBeTheSameInstance(functionQuery);
            functionQuery.FunctionName.ShouldBeTheSameInstance(name);
            functionQuery.Arguments.ShouldBeTheSameInstance(arguments);
        }

        [TestMethod]
        public void ShouldExecuteQueryOnEnumeration()
        {
            var provider = new Mock<ITableQueryProvider>();
            var expectedResult = new List<UserEntity> { new UserEntity() };
            provider.Setup(x => x.Execute(It.IsAny<ConstantExpression>()))
                .Returns(expectedResult);
            var name = new ElementName("name");
            var arguments = Enumerable.Empty<ConstantExpression>().ToArray();
            var functionQuery = new FunctionQuery<UserEntity>(
                provider.Object,
                name,
                arguments);

            var result = functionQuery.ToArray();
            result.AllCorrespondingElementsShouldBeEqual(expectedResult);
        }
    }
}