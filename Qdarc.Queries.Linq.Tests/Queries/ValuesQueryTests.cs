﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Queries;

namespace Qdarc.Queries.Linq.Tests.Queries
{
    [TestClass]
    public class ValuesQueryTests
    {
        [TestMethod]
        public void ShouldCreateValuesQuery()
        {
            var provider = new Mock<ITableQueryProvider>();
            var query = new ValuesQuery<int>(provider.Object, Expression.Constant(new[] { 1 }));
            query.Values.ShouldNotBeNull();
            query.Values.Value.ShouldBeInstanceOf<int[]>().Single().ShouldBeEqual(1);
            var constant = query.Expression.ShouldBeInstanceOf<ConstantExpression>();
            constant.Value.ShouldBeTheSameInstance(query);
            query.Provider.ShouldBeTheSameInstance(provider.Object);
            var looslyTypedQuery = (IValuesQuery)query;
            looslyTypedQuery.Values.ShouldBeTheSameInstance(query.Values);
        }

        [TestMethod]
        public void ProviderCannotBeNull()
        {
            QAssert.ShouldThrow<ArgumentNullException>(() => new ValuesQuery<int>(null, Expression.Constant(new[] { 1 })));
        }

        [TestMethod]
        public void ValuesListCannotBeNull()
        {
            var provider = new Mock<ITableQueryProvider>();
            QAssert.ShouldThrow<ArgumentNullException>(() => new ValuesQuery<int>(provider.Object, null));
        }
    }
}
