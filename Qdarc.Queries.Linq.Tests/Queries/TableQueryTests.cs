﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Queries.Linq.Queries;

namespace Qdarc.Queries.Linq.Tests.Queries
{
    [TestClass]
    public class TableQueryTests
    {
        [TestMethod]
        public void ShouldExecuteQueryWhenAnyMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Any();

            privider.Verify(x => x.Execute<bool>(It.Is<MethodCallExpression>(z => z.Method.Name == "Any" && z.Arguments.Count == 1)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenElementAtMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.ElementAt(10);

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "ElementAt" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenFirstMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.First();

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "First" && z.Arguments.Count == 1)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenFirstOrDefaultMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.FirstOrDefault();

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "FirstOrDefault" && z.Arguments.Count == 1)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenCountMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Count();

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "Count" && z.Arguments.Count == 1)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenAnyWithPredicateMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Any(x => x > 0);

            privider.Verify(x => x.Execute<bool>(It.Is<MethodCallExpression>(z => z.Method.Name == "Any" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldWrapInAsQueryableCall()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.AsQueryable();

            privider.Verify(x => x.CreateQuery<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "AsQueryable" && z.Arguments.Count == 1)));
        }

        [TestMethod]
        public void ShouldWrapInOrderByCall()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.OrderBy(x => x);

            privider.Verify(x => x.CreateOrderedQuery<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "OrderBy" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldWrapInOrderByDescendingCall()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.OrderByDescending(x => x);

            privider.Verify(x => x.CreateOrderedQuery<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "OrderByDescending" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldWrapInSelectCall()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Select(x => (int?)x);

            privider.Verify(x => x.CreateQuery<int?>(It.Is<MethodCallExpression>(z => z.Method.Name == "Select" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldWrapInTopCall()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Top(10);

            privider.Verify(x => x.CreateQuery<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "Top" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldWrapInWhereCall()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Where(x => x > 0);

            privider.Verify(x => x.CreateQuery<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "Where" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenSingleMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Single();

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "Single" && z.Arguments.Count == 1)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenSingleMethodIsCalledWithPredicate()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Single(x => x > 0);

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "Single" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenSingleOrDefaultMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.SingleOrDefault();

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "SingleOrDefault" && z.Arguments.Count == 1)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenSingleOrDefaultMethodIsCalledWithPredicate()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.SingleOrDefault(x => x > 0);

            privider.Verify(x => x.Execute<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "SingleOrDefault" && z.Arguments.Count == 2)));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenToListMethodIsCalledWithPredicate()
        {
            var query = new Mock<ITableQuery<int>>();
            var expression = Expression.Constant(null, typeof(ITableQuery<int>));
            query.Setup(x => x.Expression).Returns(expression);
            var privider = new Mock<ITableQueryProvider>();
            query.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.ToList();

            privider.Verify(x => x.Execute<List<int>>(expression));
        }

        [TestMethod]
        public void ShouldExecuteQueryWhenContainsMethodIsCalled()
        {
            var query = new Mock<ITableQuery<int>>();
            query.Setup(x => x.Expression).Returns(Expression.Constant(null, typeof(ITableQuery<int>)));
            var privider = new Mock<ITableQueryProvider>();
            var intermidiateQuery = new Mock<ITableQuery<int>>();
            var intermediateExpression = Expression.Constant(null, typeof(ITableQuery<int>));
            intermidiateQuery.Setup(x => x.Expression).Returns(intermediateExpression);
            privider.Setup(x => x.CreateQuery<int>(It.IsAny<MethodCallExpression>()))
                .Returns(intermidiateQuery.Object);
            query.Setup(x => x.Provider).Returns(privider.Object);
            intermidiateQuery.Setup(x => x.Provider).Returns(privider.Object);
            query.Object.Contains(10);

            privider.Verify(x => x.CreateQuery<int>(It.Is<MethodCallExpression>(z => z.Method.Name == "Contains" && z.Arguments.Count == 2)));
            privider.Verify(x => x.Execute<bool>(intermediateExpression));
        }
    }
}
