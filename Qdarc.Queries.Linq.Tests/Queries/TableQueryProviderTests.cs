﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Formatter;
using Qdarc.Queries.Linq.Finalizers;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Sql.Connector;
using ISqlCommand = Qdarc.Sql.Connector.ISqlCommand;

namespace Qdarc.Queries.Linq.Tests.Queries
{
    [TestClass]
    public class TableQueryProviderTests
    {
        private Mock<ISqlCommand> _command;

        [TestMethod]
        public void ShouldCreateOrderedQuery()
        {
            var databaseConnectionProvider = new Mock<IDatabaseConnectionProvider>();
            var linqQueryVisitationContextFactory = new Mock<IVisitationContext>();
            var queryFormattingContextFactory = new Mock<IFormattedQueryTransformer>();
            var mapperSelector = new Mock<IObjectMapperSelector>();

            var provider = new TableQueryProvider(
                databaseConnectionProvider.Object,
                () => linqQueryVisitationContextFactory.Object,
                () => queryFormattingContextFactory.Object,
                Enumerable.Empty<IResultFinalizer>(),
                mapperSelector.Object,
                new Mock<IParametersBinder>().Object);

            var query = provider.CreateOrderedQuery<int>(Expression.Constant(null, typeof(ITableQuery<int>)));
            query.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldCreateQuery()
        {
            var databaseConnectionProvider = new Mock<IDatabaseConnectionProvider>();
            var linqQueryVisitationContextFactory = new Mock<IVisitationContext>();
            var queryFormattingContextFactory = new Mock<IFormattedQueryTransformer>();
            var mapperSelector = new Mock<IObjectMapperSelector>();

            var provider = new TableQueryProvider(
                databaseConnectionProvider.Object,
                () => linqQueryVisitationContextFactory.Object,
                () => queryFormattingContextFactory.Object,
                Enumerable.Empty<IResultFinalizer>(),
                mapperSelector.Object,
                new Mock<IParametersBinder>().Object);

            var query = provider.CreateQuery<int>(Expression.Constant(null, typeof(ITableQuery<int>)));
            query.ShouldNotBeNull();
        }

        [TestMethod]
        public void ShouldExecuteListQuery()
        {
            var expression = Expression.Constant(null, typeof(ITableQuery<int>));
            var queryResult = new List<int> { 1, 2, 3 };
            var provider = CreateTableQueryProvider(queryResult);

            var query = provider.Execute(expression);

            query.ShouldNotBeNull();
            query.ShouldBeTheSameInstance(queryResult);
        }

        [TestMethod]
        public void ShouldExecuteTypedListQuery()
        {
            var expression = Expression.Constant(null, typeof(ITableQuery<int>));
            var queryResult = new List<int> { 1, 2, 3 };
            var provider = CreateTableQueryProvider(queryResult);

            var query = provider.Execute<List<int>>(expression);

            query.ShouldNotBeNull();
            query.ShouldBeTheSameInstance(queryResult);
        }

        [TestMethod]
        public void ShouldExecuteTypedListQueryUsingLessSpecificType()
        {
            var expression = Expression.Constant(null, typeof(ITableQuery<int>));
            var queryResult = new List<int> { 1, 2, 3 };
            var provider = CreateTableQueryProvider(queryResult);

            var query = provider.Execute<IEnumerable<int>>(expression);

            query.ShouldNotBeNull();
            query.ShouldBeTheSameInstance(queryResult);
        }

        private TableQueryProvider CreateTableQueryProvider(List<int> queryResult, FormattedQuery formattedQuery = null)
        {
            formattedQuery = formattedQuery ?? new FormattedQuery
            {
                Query = "query"
            };
            var reader = new Mock<ISqlDataReader>();
            _command = new Mock<ISqlCommand>();
            _command.Setup(x => x.ExecuteReader()).Returns(reader.Object);
            var connection = new Mock<ISqlConnection>();
            connection.Setup(x => x.Trace).Returns(false);
            connection.Setup(x => x.CreateSqlCommand(formattedQuery.Query)).Returns(_command.Object);
            var databaseConnectionProvider = new Mock<IDatabaseConnectionProvider>();
            databaseConnectionProvider.Setup(x => x.Connection).Returns(connection.Object);
            var linqQueryVisitationContext = new Mock<IVisitationContext>();
            var typesProvider = new Mock<ISqlTypeManager>();
            typesProvider.Setup(x => x.GetSqlType(It.IsAny<ISqlType>(), null))
                .Returns((ISqlType first, ISqlType second) => first);
            linqQueryVisitationContext.Setup(x => x.TypeManager).Returns(typesProvider.Object);
            linqQueryVisitationContext.Setup(x => x.FinalizeSelection(null))
                .Returns(new SqlScript(new[] { new Mock<Qdarc.Queries.Model.Commands.ISqlCommand>().Object }, null));
            var queryFormattingContext = new Mock<IFormattedQueryTransformer>();
            queryFormattingContext.Setup(x => x.Handle<ISqlExpression, FormattedQuery>(It.IsAny<ISqlExpression>())).Returns(formattedQuery);
            var mapperSelector = new Mock<IObjectMapperSelector>();
            var mapper = new Mock<IObjectsSetMapper>();
            mapper.Setup(x => x.ReadAll(It.IsAny<Type>(), reader.Object)).Returns(queryResult);
            mapperSelector.Setup(x => x.BuildSetMapper(It.IsAny<SqlScript>(), formattedQuery)).Returns(mapper.Object);
            var finalizer = new Mock<IResultFinalizer>();
            object result = 1;
            finalizer.Setup(x => x.TryFinalizeResult(
                It.IsAny<IList>(),
                It.IsAny<Expression>(),
                out result))
                .Returns(true);

            var provider = new TableQueryProvider(
                databaseConnectionProvider.Object,
                () => linqQueryVisitationContext.Object,
                () => queryFormattingContext.Object,
                new IResultFinalizer[] { finalizer.Object },
                mapperSelector.Object,
                new Mock<IParametersBinder>().Object);
            return provider;
        }
    }
}