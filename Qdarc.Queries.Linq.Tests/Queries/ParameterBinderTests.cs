﻿using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Tests.Queries
{
    [TestClass]
    public class ParameterBinderTests
    {
        [TestMethod]
        public void ShouldBindParametersToQuery()
        {
            var sqlType = new Mock<ISqlType>().Object;
            var parameter = new Mock<ISqlParameter>();
            var sqlTypeProvider = new Mock<ISqlTypeManager>();
            sqlTypeProvider.Setup(x => x.GetSqlType(sqlType, null)).Returns(sqlType);
            var binder = new ParametersBinder(
                sqlTypeProvider.Object,
                null);
            var sqlCommand = new Mock<ISqlCommand>();
            sqlCommand.Setup(x => x.AddParameter()).Returns(parameter.Object);
            var linqParameterReferences = new[]
            {
                new LinqParameterReference(
                    Expression.Constant(55),
                    typeof(int),
                    sqlType)
                {
                    Name = "@param1",
                }
            };

            binder.BindParameters(linqParameterReferences, sqlCommand.Object, true);

            parameter.VerifySet(x => x.Name = "@param1", Times.Once);
            parameter.VerifySet(x => x.Value = 55, Times.Once);
            parameter.VerifySet(x => x.Type = sqlType, Times.Once);
        }

        [TestMethod]
        public void ShouldBindTableParametersToQuery()
        {
            var sqlType = new Mock<ISqlType>().Object;
            var parameter = new Mock<ISqlParameter>();
            var sqlTypeProvider = new Mock<ISqlTypeManager>();
            sqlTypeProvider.Setup(x => x.GetSqlType(sqlType, null)).Returns(sqlType);
            var binder = new ParametersBinder(
                sqlTypeProvider.Object,
                null);
            var sqlCommand = new Mock<ISqlCommand>();
            sqlCommand.Setup(x => x.AddParameter()).Returns(parameter.Object);
            var value = new[] { 55 };
            var linqParameterReferences = new[]
            {
                new LinqTableParameterReference(
                    Expression.Constant(value),
                    typeof(int),
                    sqlType)
                {
                    Name = "@param1",
                }
            };

            binder.BindParameters(linqParameterReferences, sqlCommand.Object, true);

            parameter.VerifySet(x => x.Name = "@param1", Times.Once);
            parameter.VerifySet(x => x.Value = value, Times.Once);
            parameter.VerifySet(x => x.Type = sqlType, Times.Once);
        }
    }
}