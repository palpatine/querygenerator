﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Finalizers;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.Finalizers
{
    [TestClass]
    public class SingleResultFinalizerTests
    {
        private readonly MethodInfo _all = ExpressionExtensions.GetMethod(() => Queryable.All<int>(null, x => x > 0));
        private readonly MethodInfo _any = ExpressionExtensions.GetMethod(() => Queryable.Any<int>(null));
        private readonly MethodInfo _count = ExpressionExtensions.GetMethod(() => Queryable.Count<int>(null));
        private readonly MethodInfo _first = ExpressionExtensions.GetMethod(() => Queryable.First<int>(null));
        private readonly MethodInfo _firstOrDefault = ExpressionExtensions.GetMethod(() => Queryable.FirstOrDefault<int>(null));
        private readonly MethodInfo _single = ExpressionExtensions.GetMethod(() => Queryable.Single<int>(null));
        private readonly MethodInfo _singleOrDefault = ExpressionExtensions.GetMethod(() => Queryable.SingleOrDefault<int>(null));

        [TestMethod]
        public void ShouldExecuteAllResultQuery()
        {
            var predicate = Expression.Constant(null, typeof(Expression<Func<int, bool>>));
            var expression = Expression.Call(_all, Expression.Constant(null, typeof(IQueryable<int>)), predicate);
            var queryResult = new List<int> { 1 };
            var finalizer = new SingleResultFinalizer();
            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeEqual(queryResult[0]);
        }

        [TestMethod]
        public void ShouldExecuteAnyResultQuery()
        {
            var expression = Expression.Call(_any, Expression.Constant(null, typeof(IQueryable<int>)));
            var queryResult = new List<int> { 1 };
            var finalizer = new SingleResultFinalizer();

            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeEqual(queryResult[0]);
        }

        [TestMethod]
        public void ShouldExecuteCountResultQuery()
        {
            var expression = Expression.Call(_count, Expression.Constant(null, typeof(IQueryable<int>)));
            var queryResult = new List<int> { 1 };
            var finalizer = new SingleResultFinalizer();

            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeEqual(queryResult[0]);
        }

        [TestMethod]
        public void ShouldExecuteFirstOrDefaultQueryWithEmptyResult()
        {
            var expression = Expression.Call(_firstOrDefault, Expression.Constant(null, typeof(IQueryable<int>)));
            var queryResult = new List<int>();
            var finalizer = new SingleResultFinalizer();

            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeNull();
        }

        [TestMethod]
        public void ShouldExecuteFirstOrDefaultResultQuery()
        {
            var expression = Expression.Call(_firstOrDefault, Expression.Constant(null, typeof(IQueryable<int>)));
            var queryResult = new List<int> { 1, 2 };
            var finalizer = new SingleResultFinalizer();

            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeEqual(queryResult[0]);
        }

        [TestMethod]
        public void ShouldExecuteFirstResultQuery()
        {
            var expression = Expression.Call(_first, Expression.Constant(null, typeof(IQueryable<int>)));
            var queryResult = new List<int> { 1, 2 };
            var finalizer = new SingleResultFinalizer();

            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeEqual(queryResult[0]);
        }

        [TestMethod]
        public void ShouldExecuteSingleOrDefaultResultQuery()
        {
            var expression = Expression.Call(_singleOrDefault, Expression.Constant(null, typeof(IQueryable<int>)));
            var queryResult = new List<int> { 1 };
            var finalizer = new SingleResultFinalizer();

            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeEqual(queryResult[0]);
        }

        [TestMethod]
        public void ShouldExecuteSingleResultQuery()
        {
            var expression = Expression.Call(_single, Expression.Constant(null, typeof(IQueryable<int>)));
            var queryResult = new List<int> { 1 };
            var finalizer = new SingleResultFinalizer();

            object result;
            var isFinalized = finalizer.TryFinalizeResult(queryResult, expression, out result);

            isFinalized.ShouldBeTrue();
            result.ShouldBeEqual(queryResult[0]);
        }
    }
}
