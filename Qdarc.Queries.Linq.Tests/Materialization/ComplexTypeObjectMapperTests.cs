﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.Materialization
{
    [TestClass]
    public class ComplexTypeObjectMapperTests
    {
        [TestMethod]
        public void ShouldMapIntProperty()
        {
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((ComplexTypeObjectMapperTestSubject x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var mapping = new AliasedSelectionItem(
                linqColumnReference)
            {
                ResolvedAlias = "Id",
            };
            var selection = new SqlSelection(new[] { mapping }, typeof(ComplexTypeObjectMapperTestSubject));
            var mapper = new ComplexTypeObjectsSetMapper(selection);
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns(4);
            var result = mapper.ReadSingle(reader.Object);
            var testSubject = result.ShouldBeInstanceOf<ComplexTypeObjectMapperTestSubject>();
            testSubject.Id.ShouldBeEqual(4);
        }

        [TestMethod]
        public void ShouldThrowIfNullIsReturnedForIntProperty()
        {
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((ComplexTypeObjectMapperTestSubject x) => x.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var mapping = new AliasedSelectionItem(linqColumnReference)
            {
                ResolvedAlias = "Id",
            };
            var selection = new SqlSelection(new[] { mapping }, typeof(ComplexTypeObjectMapperTestSubject));
            var mapper = new ComplexTypeObjectsSetMapper(selection);
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns(null);

            QAssert.ShouldThrow<MappingException>(
                () => mapper.ReadSingle(reader.Object),
                x => x.Message == "Expected type Int32 but NULL was provided by reader for Id property of Qdarc.Queries.Linq.Tests.Materialization.ComplexTypeObjectMapperTestSubject.");
        }
    }
}
