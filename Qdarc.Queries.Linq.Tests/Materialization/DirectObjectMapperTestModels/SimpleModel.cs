﻿namespace Qdarc.Queries.Linq.Tests.Materialization.DirectObjectMapperTestModels
{
    internal class SimpleModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}