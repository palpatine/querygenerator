﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Linq.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.Materialization
{
    [TestClass]
    public class ProjectionByConstructorObjectMapperTests
    {
        [TestMethod]
        public void ShouldProvideValueForInt()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns(4);
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject(0));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var idColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Id",
                ResolvedAlias = "Id",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { idColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            var result = data.ShouldBeInstanceOf<ProjectionByConstructorTestSubject>();
            result.Id.ShouldBeEqual(4);
        }

        [TestMethod]
        public void ShouldThrowOnNullForInt()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns(null);
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject(0));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var idColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Id",
                ResolvedAlias = "Id",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { idColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            QAssert.ShouldThrow<MappingException>(
                () => mapper.ReadSingle(reader.Object),
                x => x.Message == "Expected type Int32 but NULL was provided by reader for id parameter of Qdarc.Queries.Linq.Tests.Materialization.ProjectionByConstructorTestSubject(System.Int32).");
        }

        [TestMethod]
        public void ShouldThrowOnTypeMismatchForInt()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns("value");
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject(0));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var idColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Id",
                ResolvedAlias = "Id",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { idColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            QAssert.ShouldThrow<MappingException>(
                () => mapper.ReadSingle(reader.Object),
                x => x.Message == "Expected type Int32 but String was provided by reader for id parameter of Qdarc.Queries.Linq.Tests.Materialization.ProjectionByConstructorTestSubject(System.Int32).");
        }

        [TestMethod]
        public void ShouldThrowOnTypeMismatchForNullableInt()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns("value");
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject((int?)0));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var idColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Id",
                ResolvedAlias = "Id",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { idColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            QAssert.ShouldThrow<MappingException>(
                () => mapper.ReadSingle(reader.Object),
                x => x.Message == "Expected type Int32 but String was provided by reader for id parameter of Qdarc.Queries.Linq.Tests.Materialization.ProjectionByConstructorTestSubject(System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]).");
        }

        [TestMethod]
        public void ShouldProvideValueForNullableInt()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns(4);
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject((int?)0));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var idColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Id",
                ResolvedAlias = "Id",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { idColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            var result = data.ShouldBeInstanceOf<ProjectionByConstructorTestSubject>();
            result.Id.ShouldBeEqual(4);
        }

        [TestMethod]
        public void ShouldProvideNullForNullableInt()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Id"]).Returns(null);
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject((int?)0));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.Id),
                typeof(int),
                new Mock<ISqlType>().Object);
            var idColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Id",
                ResolvedAlias = "Id",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { idColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            var result = data.ShouldBeInstanceOf<ProjectionByConstructorTestSubject>();
            result.Id.ShouldBeNull();
        }

        [TestMethod]
        public void ShouldProvideValueForString()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Name"]).Returns("Object name");
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject(string.Empty));
            var nameColumn = new AliasedSelectionItem(new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object))
            {
                Alias = "Name",
                ResolvedAlias = "Name",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { nameColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            var result = data.ShouldBeInstanceOf<ProjectionByConstructorTestSubject>();
            result.Name.ShouldBeEqual("Object name");
        }

        [TestMethod]
        public void ShouldProvideNullForString()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["Name"]).Returns(null);
            var constructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject(string.Empty));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            var nameColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Name",
                ResolvedAlias = "Name",
            };
            var selection = new ProjectionByConstructor(constructor, new[] { nameColumn });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var mapper = new ProjectionByConstructorObjectMapper(selection, selector.Object);
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            var result = data.ShouldBeInstanceOf<ProjectionByConstructorTestSubject>();
            result.Name.ShouldBeNull();
        }

        [TestMethod]
        public void ShouldProvideValueForPrefixedSelection()
        {
            var reader = new Mock<ISqlDataReader>(MockBehavior.Strict);
            var innerConstructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject(string.Empty));
            var linqColumnReference = new LinqColumnReference(
                new Mock<INamedSource>().Object,
                ExpressionExtensions.GetProperty((UserEntity u) => u.FirstName),
                typeof(string),
                new Mock<ISqlType>().Object);
            var nameColumn = new AliasedSelectionItem(linqColumnReference)
            {
                Alias = "Name",
                ResolvedAlias = "Name",
            };
            var selection = new ProjectionByConstructor(innerConstructor, new[] { nameColumn });
            var prefixedSelection = new PrefixedSelection("prefix", selection);
            var outerConstructor =
                ExpressionExtensions.GetConstructor(() => new ProjectionByConstructorTestSubject((ProjectionByConstructorTestSubject)null));

            var outerSelection = new ProjectionByConstructor(outerConstructor, new[] { prefixedSelection });
            var selector = new Mock<IObjectMapperSelector>(MockBehavior.Strict);
            var innerMapper = new Mock<ISingleObjectsMapper>(MockBehavior.Strict);
            innerMapper.Setup(x => x.ReadSingle(reader.Object)).Returns(new ProjectionByConstructorTestSubject("name"));
            selector.Setup(x => x.BuildSingleObjectMapper(prefixedSelection.Selection)).Returns(innerMapper.Object);
            var mapper = new ProjectionByConstructorObjectMapper(outerSelection, selector.Object);
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            var result = data.ShouldBeInstanceOf<ProjectionByConstructorTestSubject>();
            result.Name.ShouldBeNull();
        }
    }
}
