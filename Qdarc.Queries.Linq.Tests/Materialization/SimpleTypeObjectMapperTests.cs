﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Tests.Materialization
{
    [TestClass]
    public class SimpleTypeObjectMapperTests
    {
        [TestMethod]
        public void ShouldGetMatchingValueFromReader()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x[0]).Returns(4);

            var mapper = new SimpleTypeMapper(typeof(int));
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            data.ShouldBeEqual(4);
        }

        [TestMethod]
        public void ShouldGetMatchingValueFromReaderWhileExpectedIsNullableValue()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x[0]).Returns(4);

            var mapper = new SimpleTypeMapper(typeof(int?));
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldNotBeNull();
            data.ShouldBeEqual(4);
        }

        [TestMethod]
        public void ShouldGetNullValueFromReaderForNullableType()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x[0]).Returns(null);

            var mapper = new SimpleTypeMapper(typeof(int?));
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldBeNull();
        }

        [TestMethod]
        public void ShouldGetNullValueFromReaderForReferenceType()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x[0]).Returns(null);

            var mapper = new SimpleTypeMapper(typeof(string));
            var data = mapper.ReadSingle(reader.Object);

            data.ShouldBeNull();
        }

        [TestMethod]
        public void ShouldThrowIfNullIsProvidedForNotNullableValue()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x[0]).Returns(null);

            var mapper = new SimpleTypeMapper(typeof(int));

            QAssert.ShouldThrow<MappingException>(
                () => mapper.ReadSingle(reader.Object),
                x => x.Message == "Expected type Int32 but NULL was provided by reader.");
        }

        [TestMethod]
        public void ShouldThrowIfTypesMismatch()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x[0]).Returns("text");

            var mapper = new SimpleTypeMapper(typeof(int));

            QAssert.ShouldThrow<MappingException>(
                () => mapper.ReadSingle(reader.Object),
                x => x.Message == "Expected type Int32 but String was provided by reader.");
        }

        [TestMethod]
        public void ShouldThrowIfTypesMismatchWithNullableType()
        {
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x[0]).Returns("text");

            var mapper = new SimpleTypeMapper(typeof(int?));

            QAssert.ShouldThrow<MappingException>(
                () => mapper.ReadSingle(reader.Object),
                x => x.Message == "Expected type Int32 but String was provided by reader.");
        }
    }
}