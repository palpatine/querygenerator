﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Linq.Tests.Materialization.MultipleResultMapperTestModels;
using Qdarc.Sql.Connector;

namespace Qdarc.Queries.Linq.Tests.Materialization
{
    [TestClass]
    public class MultipleResultMapperTests
    {
        [TestMethod]
        public void ShouldCreateDataFormMultipleResults()
        {
            var objectMapperSelector = new Mock<IObjectMapperSelector>();
            var innerModelMapper = new Mock<IObjectsSetMapper>();
            var numberMapper = new Mock<IObjectsSetMapper>();
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x.NextResult()).Returns(true);
            var innerModels = new List<InnerModel>();
            innerModelMapper.Setup(x => x.ReadAll(typeof(InnerModel), reader.Object)).Returns(innerModels);
            var innerNumbers = new List<int>();
            numberMapper.Setup(x => x.ReadAll(typeof(int), reader.Object)).Returns(innerNumbers);
            objectMapperSelector.Setup(x => x.BuildDirectObjectsSetMapper(typeof(InnerModel)))
                .Returns(innerModelMapper.Object);
            objectMapperSelector.Setup(x => x.BuildDirectObjectsSetMapper(typeof(int)))
                .Returns(numberMapper.Object);
            var mapper = new MulitpleResultMapper(typeof(SimpleModel), objectMapperSelector.Object);
            var data = mapper.ReadAll(typeof(SimpleModel), reader.Object);
            data.Count.ShouldBeEqual(1);
            var first = data[0];
            var model = first.ShouldBeInstanceOf<SimpleModel>();
            model.InnerModels.ShouldBeTheSameInstance(innerModels);
            model.NuberCollection.ShouldBeTheSameInstance(innerNumbers);
        }

        [TestMethod]
        public void ShouldThrowIfThereIsMoreResultsExpectedThenProvidedByCommand()
        {
            var objectMapperSelector = new Mock<IObjectMapperSelector>();
            var innerModelMapper = new Mock<IObjectsSetMapper>();
            var numberMapper = new Mock<IObjectsSetMapper>();
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x.NextResult()).Returns(false);
            var innerModels = new List<InnerModel>();
            innerModelMapper.Setup(x => x.ReadAll(typeof(InnerModel), reader.Object)).Returns(innerModels);
            var innerNumbers = new List<int>();
            numberMapper.Setup(x => x.ReadAll(typeof(int), reader.Object)).Returns(innerNumbers);
            objectMapperSelector.Setup(x => x.BuildDirectObjectsSetMapper(typeof(InnerModel)))
                .Returns(innerModelMapper.Object);
            objectMapperSelector.Setup(x => x.BuildDirectObjectsSetMapper(typeof(int)))
                .Returns(numberMapper.Object);
            var mapper = new MulitpleResultMapper(typeof(SimpleModel), objectMapperSelector.Object);
            QAssert.ShouldThrow<MappingException>(() => mapper.ReadAll(typeof(SimpleModel), reader.Object));
        }

        [TestMethod]
        public void ShouldRequireAllModelPropertiesToBeCollections()
        {
            var objectMapperSelector = new Mock<IObjectMapperSelector>();
            var reader = new Mock<ISqlDataReader>();
            var mapper = new MulitpleResultMapper(typeof(InvalidModel), objectMapperSelector.Object);
            QAssert.ShouldThrow<MappingException>(() => mapper.ReadAll(typeof(InvalidModel), reader.Object));
        }
    }
}
