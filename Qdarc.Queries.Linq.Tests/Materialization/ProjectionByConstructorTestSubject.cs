namespace Qdarc.Queries.Linq.Tests.Materialization
{
    internal class ProjectionByConstructorTestSubject
    {
        public ProjectionByConstructorTestSubject(int? id)
        {
            Id = id;
        }

        public ProjectionByConstructorTestSubject(int id)
        {
            Id = id;
        }

        public ProjectionByConstructorTestSubject(string name)
        {
            Name = name;
        }

        public ProjectionByConstructorTestSubject(ProjectionByConstructorTestSubject template)
        {
            Template = template;
        }

        public ProjectionByConstructorTestSubject Template { get; set; }

        public int? Id { get; }

        public string Name { get; }
    }
}