﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Linq.Tests.Materialization.DirectObjectMapperTestModels;
using Qdarc.Queries.Model;
using Qdarc.Sql.Connector;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.Materialization
{
    [TestClass]
    public class DirectObjectMapperTests
    {
        [TestMethod]
        public void ShouldCreateInstanceOfGivenType()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var mapper = new DirectObjectsSetMapper(typeof(EmptyModel), convention.Object);
            var reader = new Mock<ISqlDataReader>();
            var instance = mapper.ReadSingle(reader.Object);
            instance.ShouldBeInstanceOf<EmptyModel>();
        }

        [TestMethod]
        public void ShouldPopulatePropertiesUsingModelToSqlConvention()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var idProperty = ExpressionExtensions.GetProperty((SimpleModel x) => x.Id);
            var nameProperty = ExpressionExtensions.GetProperty((SimpleModel x) => x.Name);
            convention.Setup(x => x.GetColumnName(idProperty)).Returns("ModelId");
            convention.Setup(x => x.GetColumnName(nameProperty)).Returns("ModelName");
            var mapper = new DirectObjectsSetMapper(typeof(SimpleModel), convention.Object);
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["ModelId"]).Returns(4);
            reader.Setup(x => x["ModelName"]).Returns("Name");
            var instance = mapper.ReadSingle(reader.Object);
            var model = instance.ShouldBeInstanceOf<SimpleModel>();
            model.Id.ShouldBeEqual(4);
            model.Name.ShouldBeEqual("Name");
        }

        [TestMethod]
        public void ShouldThrowIfTypesMismatch()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var idProperty = ExpressionExtensions.GetProperty((SimpleModel x) => x.Id);
            var nameProperty = ExpressionExtensions.GetProperty((SimpleModel x) => x.Name);
            convention.Setup(x => x.GetColumnName(idProperty)).Returns("ModelId");
            convention.Setup(x => x.GetColumnName(nameProperty)).Returns("ModelName");
            var mapper = new DirectObjectsSetMapper(typeof(SimpleModel), convention.Object);
            var reader = new Mock<ISqlDataReader>();
            reader.Setup(x => x["ModelId"]).Returns("value");
            QAssert.ShouldThrow<MappingException>(() => mapper.ReadSingle(reader.Object));
        }
    }
}
