﻿using System.Collections.Generic;
using Qdarc.Queries.Linq.Annotations;

namespace Qdarc.Queries.Linq.Tests.Materialization.MultipleResultMapperTestModels
{
    [MultipleResult]
    internal class SimpleModel
    {
        [SourceResult(1)]
        public IEnumerable<InnerModel> InnerModels { get; set; }

        [SourceResult(0)]
        public IEnumerable<int> NuberCollection { get; set; }
    }
}