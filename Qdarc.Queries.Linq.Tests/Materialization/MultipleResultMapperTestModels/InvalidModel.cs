﻿using Qdarc.Queries.Linq.Annotations;

namespace Qdarc.Queries.Linq.Tests.Materialization.MultipleResultMapperTestModels
{
    [MultipleResult]
    internal class InvalidModel
    {
        [SourceResult(0)]
        public InnerModel InnerModel { get; set; }
    }
}