﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Materialization;
using Qdarc.Queries.Linq.Tests.Materialization.MultipleResultMapperTestModels;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Linq.Projections;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.Selections;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests.Materialization
{
    [TestClass]
    public class ObjectMapperSelectorTests
    {
        [TestMethod]
        public void ShouldCreateComplexTypeMapperForSelectCommandWithComplexTypeProjection()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var command = new SelectCommand
            {
                Selection = new SqlSelection(Enumerable.Empty<ISelectionItem>(), typeof(SimpleModel))
            };
            var selector = new ObjectMapperSelector(convention.Object);
            var mapper = selector.BuildSetMapper(command, null);
            mapper.ShouldBeInstanceOf<ComplexTypeObjectsSetMapper>();
        }

        [TestMethod]
        public void ShouldCreateSimpleTypeMapperForSelectCommandWithStructureTypeProjection()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var command = new SelectCommand
            {
                Selection = new SqlSelection(Enumerable.Empty<ISelectionItem>(), typeof(int))
            };
            var selector = new ObjectMapperSelector(convention.Object);
            var mapper = selector.BuildSetMapper(command, null);
            mapper.ShouldBeInstanceOf<SimpleTypeMapper>();
        }

        [TestMethod]
        public void ShouldCreateSimpleTypeMapperForSelectCommandWithStringTypeProjection()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var command = new SelectCommand
            {
                Selection = new SqlSelection(Enumerable.Empty<ISelectionItem>(), typeof(string))
            };
            var selector = new ObjectMapperSelector(convention.Object);
            var mapper = selector.BuildSetMapper(command, null);
            mapper.ShouldBeInstanceOf<SimpleTypeMapper>();
        }

        [TestMethod]
        public void ShouldCreateProjectionByConstructorObjectMapperForSelectCommandWithProjectionByConstructor()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var command = new SelectCommand
            {
                Selection = new ProjectionByConstructor(
                    ExpressionExtensions.GetConstructor(() => new SimpleModel()),
                    Enumerable.Empty<ISelectionItem>())
            };
            var selector = new ObjectMapperSelector(convention.Object);
            var mapper = selector.BuildSetMapper(command, null);
            mapper.ShouldBeInstanceOf<ProjectionByConstructorObjectMapper>();
        }

        [TestMethod]
        public void ShouldCreateMultipleResultMapperForProcedureCallWithModelMarkedWithMultipleResultAttribute()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var command = new ProcedureCall(
                new ElementName("Name"),
                Enumerable.Empty<ISqlValueExpression>(),
                typeof(SimpleModel));
            var selector = new ObjectMapperSelector(convention.Object);
            var mapper = selector.BuildSetMapper(command, null);
            mapper.ShouldBeInstanceOf<MulitpleResultMapper>();
        }

        [TestMethod]
        public void ShouldCreateDirectObjectsSetMapperForProcedureCallWithModelNotMarkedWithMultipleResultAttribute()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var command = new ProcedureCall(
                new ElementName("Name"),
                Enumerable.Empty<ISqlValueExpression>(),
                typeof(InnerModel));
            var selector = new ObjectMapperSelector(convention.Object);
            var mapper = selector.BuildSetMapper(command, null);
            mapper.ShouldBeInstanceOf<DirectObjectsSetMapper>();
        }

        [TestMethod]
        public void ShouldCreateSimpleTypeMapperForForProcedureCallWithStructureAsAModel()
        {
            var convention = new Mock<IModelToSqlConvention>();
            var command = new ProcedureCall(
                new ElementName("Name"),
                Enumerable.Empty<ISqlValueExpression>(),
                typeof(int));
            var selector = new ObjectMapperSelector(convention.Object);
            var mapper = selector.BuildSetMapper(command, null);
            mapper.ShouldBeInstanceOf<SimpleTypeMapper>();
        }
    }
}
