﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Queries;
using Qdarc.Queries.Linq.Tests.TestModel;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tests
{
    [TestClass]
    public class DatabaseTests
    {
        private Mock<ITableQueryProvider> _tableQueryProviderMock;
        private Mock<IQueryProvider> _queryProviderMock;

        [TestInitialize]
        public void Initialize()
        {
            _tableQueryProviderMock = new Mock<ITableQueryProvider>();
            _queryProviderMock = new Mock<IQueryProvider>();
            _tableQueryProviderMock.Setup(x => x.AsQueryProvider())
                .Returns(_queryProviderMock.Object);
        }

        [TestMethod]
        public void ShouldCreateFunctionQueryWithName()
        {
            var parameter = Expression.Constant(3);
            var database = new Database(GetTabelQueryProvider);
            var result = database.Function<int>("name", parameter);
            var function = result.ShouldBeInstanceOf<FunctionQuery<int>>();
            function.FunctionName[0].ShouldBeEqual("name");
            function.Arguments.Single().ShouldBeTheSameInstance(parameter);
            function.ElementType.ShouldBeEqual(typeof(int));
            function.Provider.ShouldBeTheSameInstance(_tableQueryProviderMock.Object);
            ((IQueryable)function).Provider.ShouldBeTheSameInstance(_queryProviderMock.Object);
        }

        [TestMethod]
        public void ShouldCreateFunctionQueryWithNameAndSchema()
        {
            var parameter = Expression.Constant(3);
            var database = new Database(GetTabelQueryProvider);
            var result = database.Function<int>("name", "schema", parameter);
            var function = result.ShouldBeInstanceOf<FunctionQuery<int>>();
            function.FunctionName[0].ShouldBeEqual("schema");
            function.FunctionName[1].ShouldBeEqual("name");
            function.Arguments.Single().ShouldBeTheSameInstance(parameter);
        }

        [TestMethod]
        public void ShouldCreateFunctionQueryWithNameSchemaAndCatalog()
        {
            var parameter = Expression.Constant(3);
            var database = new Database(GetTabelQueryProvider);
            var result = database.Function<int>("name", "schema", "catalog", parameter);
            var function = result.ShouldBeInstanceOf<FunctionQuery<int>>();
            function.FunctionName[0].ShouldBeEqual("catalog");
            function.FunctionName[1].ShouldBeEqual("schema");
            function.FunctionName[2].ShouldBeEqual("name");
            function.Arguments.Single().ShouldBeTheSameInstance(parameter);
        }

        [TestMethod]
        public void ShouldCreateFunctionQueryWithElementName()
        {
            var parameter = Expression.Constant(3);
            var database = new Database(GetTabelQueryProvider);
            var name = new ElementName("Name");
            var result = database.Function<int>(name, parameter);
            var function = result.ShouldBeInstanceOf<FunctionQuery<int>>();
            function.FunctionName.ShouldBeTheSameInstance(name);
            function.Arguments.Single().ShouldBeTheSameInstance(parameter);
        }

        [TestMethod]
        public void ShouldCreateQuery()
        {
            var database = new Database(GetTabelQueryProvider);
            var result = database.Query<UserEntity>();
            result.ShouldBeInstanceOf<TableQuery<UserEntity>>();
        }

        [TestMethod]
        public void ShouldCreateQueryByAbstraction()
        {
            var database = new Database(GetTabelQueryProvider);
            var result = database.Query<IMarker>(typeof(UserEntity));
            result.ShouldBeInstanceOf<TableQuery<UserEntity>>();
        }

        [TestMethod]
        public void ShouldCreateTableParameter()
        {
            var database = new Database(GetTabelQueryProvider);
            var values = new[] { new IntValue(4) };
            var result = database.TableParameter(values);
            var tableParmaeter = result.ShouldBeInstanceOf<TableParameter<IntValue>>();
            tableParmaeter.ElementType.ShouldBeEqual(typeof(IntValue));
            tableParmaeter.Values.Value.ShouldBeTheSameInstance(values);
        }

        [TestMethod]
        public void ShouldCreateQueryLooselyTypedQuery()
        {
            var database = new Database(GetTabelQueryProvider);
            var result = database.Query(typeof(UserEntity));
            result.ShouldBeInstanceOf<TableQuery<UserEntity>>();
        }

        [TestMethod]
        public void ShouldCreateValuesQuery()
        {
            var database = new Database(GetTabelQueryProvider);
            var values = new[] { 1, 2, 3 };
            var result = database.Values(values);
            var valuesQuery = result.ShouldBeInstanceOf<ValuesQuery<int>>();
            valuesQuery.Values.Value.ShouldBeTheSameInstance(values);
        }

        [TestMethod]
        public void ShouldExecuteProcedureReturningValue()
        {
            var parameter = Expression.Constant(3);
            var expectedResult = new[] { 1, 2, 3 };
            _tableQueryProviderMock.Setup(x => x.Execute<IEnumerable<int>>(It.IsAny<ProcedureCall>()))
                .Returns(expectedResult);
            var visitedParameter = new Mock<ISqlValueExpression>();
            _tableQueryProviderMock.Setup(x => x.Visit(parameter))
                .Returns(visitedParameter.Object);
            var database = new Database(GetTabelQueryProvider);
            var name = new ElementName("Name");
            var result = database.Procedure<int>(name, parameter);
            result.ShouldBeTheSameInstance(expectedResult);
            _tableQueryProviderMock.Verify(
                x => x.Execute<IEnumerable<int>>(It.Is<ProcedureCall>(z => ValidateProcedureWithResult(z, name, visitedParameter.Object))));
        }

        [TestMethod]
        public void ShouldExecuteProcedureNotReturningValue()
        {
            var parameter = Expression.Constant(3);
            _tableQueryProviderMock.Setup(x => x.ExecuteNonQuery(It.IsAny<ProcedureCall>()));
            var visitedParameter = new Mock<ISqlValueExpression>();
            _tableQueryProviderMock.Setup(x => x.Visit(parameter))
                .Returns(visitedParameter.Object);
            var database = new Database(GetTabelQueryProvider);
            var name = new ElementName("Name");
            database.Procedure(name, parameter);
            _tableQueryProviderMock.Verify(
                x => x.ExecuteNonQuery(It.Is<ProcedureCall>(z => ValidateProcedureWithoutResult(z, name, visitedParameter.Object))));
        }

        [TestMethod]
        public void ShouldExecuteScalarFunction()
        {
            var database = new Database(GetTabelQueryProvider);
            _tableQueryProviderMock.Setup(x => x.Execute<string>(It.IsAny<MethodCallExpression>()))
                .Returns("result");
            var result = database.TestScalarFunction(4);
            result.ShouldBeEqual("result");

            _tableQueryProviderMock
                .Verify(x => x.Execute<string>(It.Is<MethodCallExpression>(z => VerifyScalarFunctionCall(z))));
        }

        [TestMethod]
        public void ShouldExecuteInsertWithEntitiesCollection()
        {
            var database = new Database(GetTabelQueryProvider);
            _tableQueryProviderMock.Setup(x => x.ExecuteNonQuery(It.IsAny<MethodCallExpression>()))
                .Returns(1);
            var entity = new UserEntity();

            var result = database.Insert(new[] { entity }.AsEnumerable());

            result.ShouldBeEqual(1);
            _tableQueryProviderMock
                .Verify(x => x.ExecuteNonQuery(It.Is<MethodCallExpression>(z => VerifyInsertCall(z, entity))));
        }

        [TestMethod]
        public void ShouldExecuteInsertWithEntity()
        {
            var database = new Database(GetTabelQueryProvider);
            _tableQueryProviderMock.Setup(x => x.ExecuteNonQuery(It.IsAny<MethodCallExpression>()))
                .Returns(1);
            var entity = new UserEntity();

            var result = database.Insert(entity);

            result.ShouldBeEqual(1);
            _tableQueryProviderMock
                .Verify(x => x.ExecuteNonQuery(It.Is<MethodCallExpression>(z => VerifyInsertCall(z, entity))));
        }

        private static bool VerifyInsertCall(MethodCallExpression call, UserEntity entity)
        {
            var insertMethod = ExpressionExtensions.GetMethod(
                (IDatabase d) => d.Insert((IValuesQuery<UserEntity>)null));
            call.Method.ShouldBeEqual(insertMethod);
            call.Object.ShouldNotBeNull();
            var entities = (IValuesQuery<UserEntity>)call.Arguments.Single()
                .ShouldBeInstanceOf<ConstantExpression>().Value;
            var userEntities = (IEnumerable<UserEntity>)entities.Values.Value;
            userEntities.Count().ShouldBeEqual(1);
            userEntities.Single().ShouldBeTheSameInstance(entity);
            return true;
        }

        private static bool VerifyScalarFunctionCall(
            MethodCallExpression call)
        {
            var testScalarMethod = ExpressionExtensions.GetMethod(() => TestScalarFunctionProvider.TestScalarFunction(null, 0));
            var select = ExpressionExtensions.GetMethod(() => TableQuery.Select<string>(null, null));
            call.Method.ShouldBeEqual(select);
            call.Object.ShouldBeNull();
            call.Arguments.First().ShouldBeInstanceOf<ConstantExpression>().Value.ShouldBeNull();
            var projection = call.Arguments.Last().ShouldBeInstanceOf<UnaryExpression>();
            var innerMethodCall = projection.Operand
                .ShouldBeInstanceOf<LambdaExpression>()
                .Body.ShouldBeInstanceOf<MethodCallExpression>();
            innerMethodCall.Method.ShouldBeTheSameInstance(testScalarMethod);
            var parameterProvider = innerMethodCall.Arguments.Last()
                .ShouldBeInstanceOf<MemberExpression>();
            Expression.Lambda<Func<int>>(parameterProvider).Compile()().ShouldBeEqual(4);
            return true;
        }

        private static bool ValidateProcedureWithResult(
            ProcedureCall call,
            ElementName name,
            ISqlValueExpression visitedParameter)
        {
            call.Arguments.Single().ShouldBeTheSameInstance(visitedParameter);
            call.Name.ShouldBeTheSameInstance(name);
            call.ClrType.ShouldBeEqual(typeof(int));

            return true;
        }

        private static bool ValidateProcedureWithoutResult(ProcedureCall call, ElementName name, ISqlValueExpression visitedParameter)
        {
            call.Arguments.Single().ShouldBeTheSameInstance(visitedParameter);
            call.Name.ShouldBeTheSameInstance(name);
            call.ClrType.ShouldBeNull();

            return true;
        }

        private ITableQueryProvider GetTabelQueryProvider()
        {
            return _tableQueryProviderMock.Object;
        }
    }
}
