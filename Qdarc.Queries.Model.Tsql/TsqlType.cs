using Qdarc.Queries.Model.Visitation;

namespace Qdarc.Queries.Model.Tsql
{
    public class TsqlType : ISqlType
    {
        public TsqlType(string name)
        {
            Name = name;
        }

        public TsqlType(string name, bool isNullable)
        {
            Name = name;
            IsNullable = isNullable;
        }

        public TsqlType(string name, bool isNullable, int capacity)
        {
            Name = name;
            Capacity = capacity;
            IsNullable = isNullable;
        }

        public TsqlType(string name, bool isNullable, byte precision, byte scale)
        {
            Name = name;
            Precision = precision;
            Scale = scale;
            IsNullable = isNullable;
        }

        public TsqlType(string name, int capacity)
        {
            Name = name;
            Capacity = capacity;
        }

        public TsqlType(string name, byte precision, byte scale)
        {
            Name = name;
            Precision = precision;
            Scale = scale;
        }

        public int? Capacity { get; }

        public bool IsNullable { get; }

        public string Name { get; }

        public byte? Precision { get; }

        public byte? Scale { get; }

        public override string ToString()
        {
            if (Precision != null)
            {
                return $"{Name}({Precision},{Scale})";
            }

            if (Capacity != null)
            {
                return $"{Name}({(Capacity == -1 ? "MAX" : Capacity.ToString())})";
            }

            return Name;
        }
    }
}