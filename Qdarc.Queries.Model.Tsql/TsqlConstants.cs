using System;
using System.Collections.Generic;

namespace Qdarc.Queries.Model.Tsql
{
    public static class TsqlConstants
    {
        public static List<string> TypePriorities { get; } = new List<string>
        {
            TsqlNativeTypesNames.DateTimeOffset,
            TsqlNativeTypesNames.DateTime2,
            TsqlNativeTypesNames.DateTime,
            TsqlNativeTypesNames.SmallDateTime,
            TsqlNativeTypesNames.Date,
            TsqlNativeTypesNames.Time,
            TsqlNativeTypesNames.Float,
            TsqlNativeTypesNames.Real,
            TsqlNativeTypesNames.Decimal,
            TsqlNativeTypesNames.Money,
            TsqlNativeTypesNames.SmallMoney,
            TsqlNativeTypesNames.BigInt,
            TsqlNativeTypesNames.Int,
            TsqlNativeTypesNames.SmallInt,
            TsqlNativeTypesNames.TinyInt,
            TsqlNativeTypesNames.Bit,
            TsqlNativeTypesNames.NText,
            TsqlNativeTypesNames.Text,
            TsqlNativeTypesNames.Image,
            TsqlNativeTypesNames.Timestamp,
            TsqlNativeTypesNames.UniqueIdentifier,
            TsqlNativeTypesNames.NVarChar,
            TsqlNativeTypesNames.NChar,
            TsqlNativeTypesNames.VarChar,
            TsqlNativeTypesNames.Char,
            TsqlNativeTypesNames.VarBinary,
            TsqlNativeTypesNames.Binary
        };

        public static Dictionary<Type, string> TypesNamesMap { get; } = new Dictionary<Type, string>
        {
            { typeof(long), TsqlNativeTypesNames.BigInt },
            { typeof(long?), TsqlNativeTypesNames.BigInt },
            { typeof(byte[]), TsqlNativeTypesNames.VarBinary },
            { typeof(bool), TsqlNativeTypesNames.Bit },
            { typeof(bool?), TsqlNativeTypesNames.Bit },
            { typeof(DateTime), TsqlNativeTypesNames.DateTime },
            { typeof(DateTime?), TsqlNativeTypesNames.DateTime },
            { typeof(DateTimeOffset), TsqlNativeTypesNames.DateTimeOffset },
            { typeof(DateTimeOffset?), TsqlNativeTypesNames.DateTimeOffset },
            { typeof(decimal), TsqlNativeTypesNames.Decimal },
            { typeof(decimal?), TsqlNativeTypesNames.Decimal },
            { typeof(double), TsqlNativeTypesNames.Float },
            { typeof(double?), TsqlNativeTypesNames.Float },
            { typeof(int), TsqlNativeTypesNames.Int },
            { typeof(int?), TsqlNativeTypesNames.Int },
            { typeof(string), TsqlNativeTypesNames.NVarChar },
            { typeof(float), TsqlNativeTypesNames.Real },
            { typeof(float?), TsqlNativeTypesNames.Real },
            { typeof(short), TsqlNativeTypesNames.SmallInt },
            { typeof(short?), TsqlNativeTypesNames.SmallInt },
            { typeof(TimeSpan), TsqlNativeTypesNames.Time },
            { typeof(TimeSpan?), TsqlNativeTypesNames.Time },
            { typeof(byte), TsqlNativeTypesNames.TinyInt },
            { typeof(byte?), TsqlNativeTypesNames.TinyInt },
            { typeof(Guid), TsqlNativeTypesNames.UniqueIdentifier },
            { typeof(Guid?), TsqlNativeTypesNames.UniqueIdentifier }
        };
    }
}