﻿using System.Collections.Generic;
using System.Linq;

namespace Qdarc.Queries.Model.Tsql
{
    public static class TsqlNativeTypesNames
    {
        public const string BigInt = "BIGINT";

        public const string Binary = "BINARY";

        public const string Bit = "BIT";

        public const string Char = "CHAR";

        public const string Date = "DATE";

        public const string DateTime = "DATETIME";

        public const string DateTime2 = "DATETIME2";

        public const string DateTimeOffset = "DATETIMEOFFSET";

        public const string Decimal = "DECIMAL";

        public const string Float = "FLOAT";

        public const string Geography = "GEOGRAPHY";
        public const string HierarchyId = "HIERARCHYID";
        public const string Image = "IMAGE";

        public const string Int = "INT";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "int", Justification = "This is intended to point to type.")]
        public const int IntMaxValue = 2147483647;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "int", Justification = "This is intended to point to type.")]
        public const int IntMinValue = -2147483648;

        public const string Money = "MONEY";
        public const string NChar = "NCHAR";
        public const string NText = "NTEXT";
        public const string Numeric = "NUMERIC";
        public const string NVarChar = "NVARCHAR";
        public const string Real = "REAL";
        public const string SmallDateTime = "SMALLDATETIME";
        public const string SmallInt = "SMALLINT";
        public const string SmallMoney = "SMALLMONEY";
        public const string Text = "TEXT";
        public const string Time = "TIME";
        public const string Timestamp = "TIMESTAMP";
        public const string TinyInt = "TINYINT";
        public const string UniqueIdentifier = "UNIQUEIDENTIFIER";
        public const string VarBinary = "VARBINARY";
        public const string VarChar = "VARCHAR";
        public const string Xml = "XML";

        private static readonly IEnumerable<string> _types = new[]
        {
            Image,
            Text,
            UniqueIdentifier,
            Date,
            Time,
            DateTime2,
            DateTimeOffset,
            TinyInt,
            SmallInt,
            Int,
            SmallDateTime,
            Real,
            Money,
            DateTime,
            Float,
            NText,
            Bit,
            Decimal,
            Numeric,
            SmallMoney,
            BigInt,
            VarBinary,
            VarChar,
            Binary,
            Char,
            Timestamp,
            NVarChar,
            NChar,
            Xml,
            HierarchyId,
            Geography
        };

        public static bool HasCapacity(string sqlTypeName)
        {
            switch (sqlTypeName)
            {
                case TsqlNativeTypesNames.VarBinary:
                case TsqlNativeTypesNames.VarChar:
                case TsqlNativeTypesNames.Binary:
                case TsqlNativeTypesNames.Char:
                case TsqlNativeTypesNames.NVarChar:
                case TsqlNativeTypesNames.NChar:
                    return true;

                default:
                    return false;
            }
        }

        public static bool HasPrecisionAndScale(string typeName)
        {
            switch (typeName)
            {
                case TsqlNativeTypesNames.Decimal:
                case TsqlNativeTypesNames.Numeric:
                    return true;
            }

            return false;
        }

        public static string NormalizeName(string name)
        {
            name = name.ToUpper();
            if (_types.Contains(name))
            {
                return name;
            }

            return null;
        }
    }
}