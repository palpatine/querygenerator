﻿namespace Qdarc.Queries.Model.Tsql
{
    public static class TsqlTypeProvider
    {
        public static TsqlType BigInt => new TsqlType(TsqlNativeTypesNames.BigInt);

        public static TsqlType BinaryMax => new TsqlType(TsqlNativeTypesNames.Binary, -1);

        public static TsqlType Bit => new TsqlType(TsqlNativeTypesNames.Bit);

        public static TsqlType CharMax => new TsqlType(TsqlNativeTypesNames.Char, -1);

        public static TsqlType Date => new TsqlType(TsqlNativeTypesNames.Date);

        public static TsqlType DateTime => new TsqlType(TsqlNativeTypesNames.DateTime);

        public static TsqlType DateTime2 => new TsqlType(TsqlNativeTypesNames.DateTime2);

        public static TsqlType DateTimeOffset => new TsqlType(TsqlNativeTypesNames.DateTimeOffset);

        public static TsqlType Float => new TsqlType(TsqlNativeTypesNames.Float);

        public static TsqlType Image => new TsqlType(TsqlNativeTypesNames.Image);

        public static TsqlType Int => new TsqlType(TsqlNativeTypesNames.Int);

        public static TsqlType Money => new TsqlType(TsqlNativeTypesNames.Money);

        public static TsqlType NCharMax => new TsqlType(TsqlNativeTypesNames.NChar, -1);

        public static TsqlType NText => new TsqlType(TsqlNativeTypesNames.NText);

        public static TsqlType NVarCharMax => new TsqlType(TsqlNativeTypesNames.NVarChar, -1);

        public static TsqlType Real => new TsqlType(TsqlNativeTypesNames.Real);

        public static TsqlType SmallDateTime => new TsqlType(TsqlNativeTypesNames.SmallDateTime);

        public static TsqlType SmallInt => new TsqlType(TsqlNativeTypesNames.SmallInt);

        public static TsqlType SmallMoney => new TsqlType(TsqlNativeTypesNames.SmallMoney);

        public static TsqlType Text => new TsqlType(TsqlNativeTypesNames.Text);

        public static TsqlType Time => new TsqlType(TsqlNativeTypesNames.Time);

        public static TsqlType Timestamp => new TsqlType(TsqlNativeTypesNames.Timestamp);

        public static TsqlType TinyInt => new TsqlType(TsqlNativeTypesNames.TinyInt);

        public static TsqlType UniqueIdentifier => new TsqlType(TsqlNativeTypesNames.UniqueIdentifier);

        public static TsqlType VarBinaryMax => new TsqlType(TsqlNativeTypesNames.VarBinary, -1);

        public static TsqlType VarCharMax => new TsqlType(TsqlNativeTypesNames.VarChar, -1);

        public static TsqlType Xml => new TsqlType(TsqlNativeTypesNames.Xml);

        public static TsqlType Binary(int capacity) => new TsqlType(TsqlNativeTypesNames.Binary, capacity);

        public static TsqlType Char(int capacity) => new TsqlType(TsqlNativeTypesNames.Char, capacity);

        public static TsqlType Decimal(byte precision, byte scale) => new TsqlType(TsqlNativeTypesNames.Decimal, precision, scale);

        public static TsqlType NChar(int capacity) => new TsqlType(TsqlNativeTypesNames.NChar, capacity);

        public static TsqlType Numeric(byte precision, byte scale) => new TsqlType(TsqlNativeTypesNames.Numeric, precision, scale);

        public static TsqlType NVarChar(int capacity) => new TsqlType(TsqlNativeTypesNames.NVarChar, capacity);

        public static TsqlType VarBinary(int capacity) => new TsqlType(TsqlNativeTypesNames.VarBinary, capacity);

        public static TsqlType VarChar(int capacity) => new TsqlType(TsqlNativeTypesNames.VarChar, capacity);
    }
}