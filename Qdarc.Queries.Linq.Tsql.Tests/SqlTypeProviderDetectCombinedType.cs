using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql.Tests
{
    [TestClass]
    public class SqlTypeProviderDetectCombinedType
    {
        [TestMethod]
        public void CombineSmallIntAndInt()
        {
            var provider = new TsqlTypeManager();
            var firstType = new TsqlType(TsqlNativeTypesNames.Int);
            var secondType = new TsqlType(TsqlNativeTypesNames.SmallInt);

            var combinedType = (TsqlType)provider.GetSqlType(firstType, secondType);

            combinedType.Name.ShouldBeEqual(TsqlNativeTypesNames.Int);
        }

        [TestMethod]
        public void CombineVarChar200VarChar300()
        {
            var provider = new TsqlTypeManager();
            var firstType = new TsqlType(TsqlNativeTypesNames.VarChar, 200);
            var secondType = new TsqlType(TsqlNativeTypesNames.VarChar, 300);

            var combinedType = (TsqlType)provider.GetSqlType(firstType, secondType);

            combinedType.Name.ShouldBeEqual(TsqlNativeTypesNames.VarChar);
            combinedType.Capacity.ShouldBeEqual(300);
        }
    }
}