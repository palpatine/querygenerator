﻿using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql.Tests
{
    [TestClass]
    public class SqlTypeProviderDetectTypeFormConstantTests
    {
        [TestMethod]
        public void GetSqlTypeBool()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant(true));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.Bit);
            type.Capacity.ShouldBeNull();
            type.Precision.ShouldBeNull();
            type.Scale.ShouldBeNull();
        }

        [TestMethod]
        public void GetSqlTypeByteArray()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant(new byte[] { 3, 5, 6 }));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.VarBinary);
            type.Capacity.ShouldBeEqual(3);
            type.Precision.ShouldBeNull();
            type.Scale.ShouldBeNull();
        }

        [TestMethod]
        public void GetSqlTypeDateTime()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant(DateTime.Today));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.DateTime);
            type.Capacity.ShouldBeNull();
            type.Precision.ShouldBeNull();
            type.Scale.ShouldBeNull();
        }

        [TestMethod]
        public void GetSqlTypeDecimal()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant(3.4M));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.Decimal);
            type.Capacity.ShouldBeNull();
            type.Precision.ShouldBeEqual((byte)2);
            type.Scale.ShouldBeEqual((byte)1);
        }

        [TestMethod]
        public void GetSqlTypeInt()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant(4));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.Int);
            type.Capacity.ShouldBeNull();
            type.Precision.ShouldBeNull();
            type.Scale.ShouldBeNull();
        }

        [TestMethod]
        public void GetSqlTypeIntNull()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant(null, typeof(int?)));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.Int);
            type.Capacity.ShouldBeNull();
            type.Precision.ShouldBeNull();
            type.Scale.ShouldBeNull();
        }

        [TestMethod]
        public void GetSqlTypeString()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant("abc"));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.NVarChar);
            type.Capacity.ShouldBeEqual(3);
            type.Precision.ShouldBeNull();
            type.Scale.ShouldBeNull();
        }

        [TestMethod]
        public void GetSqlTypeStringNull()
        {
            var provider = new TsqlTypeManager();
            var type = (TsqlType)provider.GetSqlType(Expression.Constant(null, typeof(string)));
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.NVarChar);
            type.Capacity.ShouldBeEqual(1);
            type.Precision.ShouldBeNull();
            type.Scale.ShouldBeNull();
        }
    }
}