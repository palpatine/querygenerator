﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qdarc.Asserts;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions;
using Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Utilities;

namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions
{
    [TestClass]
    public class AttributesAnnotationModelToSqlConventionTests
    {
        [TestMethod]
        public void ShouldGetSqlTypeFromAnnotationOnClassProperty()
        {
            var type = typeof(Document);
            var property = ExpressionExtensions.GetProperty((Document x) => x.Id);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var sqlType = convention.GetSqlType(type, property);
            sqlType.ShouldBeInstanceOf<TsqlType>().Name.ShouldBeEqual(TsqlNativeTypesNames.Int);
        }

        [TestMethod]
        public void ShouldGetSqlTypeFromAnnotationOnClassPropertyPointedByInterfaceImplicitImplementation()
        {
            var type = typeof(Document);
            var property = ExpressionExtensions.GetProperty((IOwned x) => x.Owner);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var sqlType = convention.GetSqlType(type, property);
            sqlType.ShouldBeInstanceOf<TsqlType>().Name.ShouldBeEqual(TsqlNativeTypesNames.Int);
        }

        [TestMethod]
        public void ShouldGetSqlTypeFromAnnotationOnClassPropertyPointedByInterfaceExplicitImplementation()
        {
            var type = typeof(Document);
            var property = ExpressionExtensions.GetProperty((IOwnedExplicit x) => x.Owner);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var sqlType = convention.GetSqlType(type, property);
            sqlType.ShouldBeInstanceOf<TsqlType>().Name.ShouldBeEqual(TsqlNativeTypesNames.BigInt);
        }

        [TestMethod]
        public void ShouldThrowWhileGettingSqlTypeFromPropertyIfAnnotationIsMissing()
        {
            var type = typeof(Document);
            var property = ExpressionExtensions.GetProperty((Document x) => x.NotAnnotated);
            var convention = new AttributesAnnotationModelToSqlConvention();
            QAssert.ShouldThrow<AnnotationException>(() => convention.GetSqlType(type, property));
        }

        [TestMethod]
        public void ShouldGetUserDefinedTableType()
        {
            var type = typeof(IntValue);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var sqlType = convention.GetUserDefinedTableType(type);
            var reference = sqlType.ShouldBeInstanceOf<CustomTypeReference>();
            reference.Name[0].ShouldBeEqual("dbo");
            reference.Name[1].ShouldBeEqual("IntList");
        }

        [TestMethod]
        public void ShouldThrowWhileGettingUserDefinedTableTypeIfTypeIsNotMarked()
        {
            var type = typeof(InvalidIntValue);
            var convention = new AttributesAnnotationModelToSqlConvention();
            QAssert.ShouldThrow<AnnotationException>(() => convention.GetUserDefinedTableType(type));
        }

        [TestMethod]
        public void ShouldGetSqlTypeFromAnnotatedMethod()
        {
            var method = ExpressionExtensions.GetMethod(() => ScalarFunctions.AnnotatedMethod());
            var convention = new AttributesAnnotationModelToSqlConvention();
            var sqlType = convention.GetSqlType(method);
            sqlType.ShouldBeInstanceOf<TsqlType>().Name.ShouldBeEqual(TsqlNativeTypesNames.Int);
        }

        [TestMethod]
        public void ShouldThrowWhileGettingSqlTypeFromNotAnnotatedMethod()
        {
            var method = ExpressionExtensions.GetMethod(() => ScalarFunctions.NotAnnotatedMethod());
            var convention = new AttributesAnnotationModelToSqlConvention();
            QAssert.ShouldThrow<AnnotationException>(() => convention.GetSqlType(method));
        }

        [TestMethod]
        public void ShouldGetTableNameFromAnnotatedType()
        {
            var type = typeof(User);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var tableName = convention.GetTableName(type);
            tableName[0].ShouldBeEqual("Users_tbl");
        }

        [TestMethod]
        public void ShouldGetTableNameFromNotAnnotatedType()
        {
            var type = typeof(Document);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var tableName = convention.GetTableName(type);
            tableName[0].ShouldBeEqual("Document");
        }

        [TestMethod]
        public void ShouldGetColumnNameFromAnnotatedProperty()
        {
            var property = ExpressionExtensions.GetProperty((Document x) => x.Id);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var name = convention.GetColumnName(property);
            name.ShouldBeEqual("Document_Id");
        }

        [TestMethod]
        public void ShouldGetColumnNameFromNotAnnotatedProperty()
        {
            var property = ExpressionExtensions.GetProperty((Document x) => x.Owner);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var name = convention.GetColumnName(property);
            name.ShouldBeEqual("Owner");
        }

        [TestMethod]
        public void ShouldGetColumnNameFromGivenSource()
        {
            var property = ExpressionExtensions.GetProperty((Document x) => x.Id);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var source = new Mock<INamedSource>();
            source.Setup(x => x.Name).Returns(new ElementName("source"));
            var columnName = convention.GetColumnName(source.Object, property);
            columnName[1].ShouldBeEqual("Document_Id");
            columnName[0].ShouldBeEqual("source");
        }

        [TestMethod]
        public void ForSelectablePropertiesShouldGetAllPublicAccessibleProperties()
        {
            var type = typeof(User);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetSelectableProperties(type);
            var properties = columns.ToArray();
            CollectionAssert.AreEquivalent(new[] { ExpressionExtensions.GetProperty((User x) => x.Id) }, properties);
        }

        [TestMethod]
        public void ForSelectablePropertiesShouldNotGetExplicitlyImplementedProperties()
        {
            var type = typeof(Document);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetSelectableProperties(type);
            var properties = columns.ToArray();
            var expected = new[]
            {
                ExpressionExtensions.GetProperty((Document x) => x.Id),
                ExpressionExtensions.GetProperty((Document x) => x.Owner),
                ExpressionExtensions.GetProperty((Document x) => x.NotAnnotated),
            };
            CollectionAssert.AreEquivalent(expected, properties);
        }

        [TestMethod]
        public void ForSelectablePropertiesShouldNotGetIgnoredProperties()
        {
            var type = typeof(Document);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetSelectableProperties(type);
            var properties = columns.ToArray();

            properties.Any(x => x.Name == ExpressionExtensions.GetPropertyName((Document z) => z.Ignored))
                .ShouldBeFalse();
        }

        [TestMethod]
        public void ForSelectablePropertiesShouldGetPropertiesMarkedAsComputedColumns()
        {
            var type = typeof(Contact);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetSelectableProperties(type);
            var properties = columns.ToArray();

            properties.Any(x => x.Name == ExpressionExtensions.GetPropertyName((Contact z) => z.Score))
                .ShouldBeTrue();
        }

        [TestMethod]
        public void ForInsertableColumnsShouldGetAllPublicAccessibleProperties()
        {
            var type = typeof(User);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetInsertableColumns(type);
            var properties = columns.Select(x => x.Property).ToArray();
            CollectionAssert.AreEquivalent(new[] { ExpressionExtensions.GetProperty((User x) => x.Id) }, properties);
        }

        [TestMethod]
        public void ForInsertableColumnsShouldNotGetExplicitlyImplementedProperties()
        {
            var type = typeof(Document);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetInsertableColumns(type);
            var properties = columns.Select(x => x.Property).ToArray();
            var expected = new[]
            {
                ExpressionExtensions.GetProperty((Document x) => x.Id),
                ExpressionExtensions.GetProperty((Document x) => x.Owner),
                ExpressionExtensions.GetProperty((Document x) => x.NotAnnotated),
            };
            CollectionAssert.AreEquivalent(expected, properties);
        }

        [TestMethod]
        public void ForInsertableColumnsShouldNotGetIgnoredProperties()
        {
            var type = typeof(Document);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetInsertableColumns(type);
            var properties = columns.Select(x => x.Property).ToArray();

            properties.Any(x => x.Name == ExpressionExtensions.GetPropertyName((Document z) => z.Ignored))
                .ShouldBeFalse();
        }

        [TestMethod]
        public void ForInsertableColumnsShouldNotGetPropertiesMarkedAsComputedColumns()
        {
            var type = typeof(Contact);
            var convention = new AttributesAnnotationModelToSqlConvention();
            var columns = convention.GetInsertableColumns(type);
            var properties = columns.Select(x => x.Property).ToArray();

            properties.Any(x => x.Name == ExpressionExtensions.GetPropertyName((Contact z) => z.Score))
                .ShouldBeFalse();
        }
    }
}