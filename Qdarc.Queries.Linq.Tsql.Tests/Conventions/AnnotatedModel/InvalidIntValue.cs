namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    public class InvalidIntValue
    {
        public InvalidIntValue(int value)
        {
            Value = value;
        }

        public int Value { get; set; }
    }
}