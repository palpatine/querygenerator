﻿using Qdarc.Queries.Linq.Annotations;

namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    [UserDefinedTableTypeName("IntList", "dbo")]
    public class IntValue
    {
        public IntValue(int value)
        {
            Value = value;
        }

        public int Value { get; set; }
    }
}