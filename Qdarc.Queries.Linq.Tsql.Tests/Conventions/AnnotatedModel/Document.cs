﻿using System;
using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    internal class Document : IOwned, IOwnedExplicit
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        [SqlColumnName("Document_Id")]
        public int Id { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int Owner { get; set; }

        public int NotAnnotated { get; set; }

        [SqlIgnore]
        public int Ignored { get; set; }

        [TsqlType(TsqlNativeTypesNames.BigInt, false)]
        long IOwnedExplicit.Owner
        {
            get => throw new NotImplementedException();

            set => throw new NotImplementedException();
        }
    }
}