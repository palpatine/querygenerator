﻿namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    internal interface IOwned
    {
        int Owner { get; set; }
    }
}