﻿using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    public class Contact
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int Id { get; set; }

        [TsqlType(TsqlNativeTypesNames.Int, false)]
        [SqlComputedColumn]
        public int Score { get; set; }
    }
}