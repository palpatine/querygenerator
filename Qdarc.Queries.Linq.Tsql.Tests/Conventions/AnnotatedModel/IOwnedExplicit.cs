﻿namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    internal interface IOwnedExplicit
    {
        long Owner { get; set; }
    }
}