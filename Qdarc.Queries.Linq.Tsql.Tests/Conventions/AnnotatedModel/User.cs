﻿using Qdarc.Queries.Linq.Annotations;
using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    [SqlTableName("Users_tbl")]
    public class User
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public int Id { get; set; }
    }
}
