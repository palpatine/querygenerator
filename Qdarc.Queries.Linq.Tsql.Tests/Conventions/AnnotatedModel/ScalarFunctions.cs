﻿using Qdarc.Queries.Linq.Tsql.Conventions.Annotations;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Queries.Linq.Tsql.Tests.Conventions.AnnotatedModel
{
    internal static class ScalarFunctions
    {
        [TsqlType(TsqlNativeTypesNames.Int, false)]
        public static int AnnotatedMethod()
        {
            return 0;
        }

        public static int NotAnnotatedMethod()
        {
            return 0;
        }
    }
}