﻿using System;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Parsers.Tsql.Handlers.Helpers;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    internal static class SqlLogicalExpressionHandlerHelper
    {
        public static Tuple<SqlLogicalExpressionsHandler, ISqlGrammarElementHandler[]> CreateHandler()
        {
            var termsHandler = new SqlTermsHandler();
            var literalsHandler = new SqlLiteralsHandler();
            ISqlGrammarElementHandler[] handlers = null;
            var provider = new SqlGrammarHandlersProvider(() => handlers);
            var valueExpressionHandler = new SqlValueExpressionsHandler(provider);
            var sqlIdentifiersHandler = new SqlIdentifiersHandler(provider);
            var aggregateFunctionsHandler = new SqlAggregateExpressionsHandler(provider);
            var orderByHandler = new SqlOrderByExpressionHandler(provider);
            var sqlSelectHandler = new SqlSelectExpressionHandler(provider);
            var sqlAssignmentExpressionHandler = new SqlAssignmentExpressionHandler(provider);
            var sqlLogicalExpressionsHandler = new SqlLogicalExpressionsHandler(provider);

            handlers = new ISqlGrammarElementHandler[]
            {
                termsHandler,
                literalsHandler,
                sqlIdentifiersHandler,
                aggregateFunctionsHandler,
                valueExpressionHandler,
                orderByHandler,
                sqlAssignmentExpressionHandler,
                sqlSelectHandler,
                sqlLogicalExpressionsHandler
            };
            return Tuple.Create(
                sqlLogicalExpressionsHandler,
                handlers);
        }
    }
}