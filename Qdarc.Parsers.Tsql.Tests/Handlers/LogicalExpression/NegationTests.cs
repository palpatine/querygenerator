﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    [TestClass]
    public class NegationTests
    {
        [TestMethod]
        public void Not()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("not Value = 5", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("not Value = 5", root, handler.Item2);
            var negation = tree.ShouldBeInstanceOf<SqlLogicalNegateExpression>();
            negation.Value.ShouldNotBeNull();
        }
    }
}