﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    [TestClass]
    public class InExpressionTests
    {
        [TestMethod]
        public void InSelect()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("Value in (select Value From [Source])", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void InSelectAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("Value in (select Value From [Source])", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlInLogicalExpression>();
            expression.IsNegated.ShouldBeFalse();
            expression.Selection.ShouldNotBeNull();
            expression.Set.ShouldBeNull();
        }

        [TestMethod]
        public void InSet()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("Value in (1,2,3)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void InSetAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("Value in (1,2,3)", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlInLogicalExpression>();
            expression.IsNegated.ShouldBeFalse();
            expression.Selection.ShouldBeNull();
            expression.Set.ShouldNotBeNull();
        }

        [TestMethod]
        public void NotInSelect()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("Value not in (select Value From [Source])", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotInSelectAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("Value not in (select Value From [Source])", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlInLogicalExpression>();
            expression.IsNegated.ShouldBeTrue();
            expression.Selection.ShouldNotBeNull();
            expression.Set.ShouldBeNull();
        }

        [TestMethod]
        public void NotInSet()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("Value not in (1,2,3)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotInSetAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("Value not in (1,2,3)", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlInLogicalExpression>();
            expression.IsNegated.ShouldBeTrue();
            expression.Selection.ShouldBeNull();
            expression.Set.ShouldNotBeNull();
        }
    }
}