﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    [TestClass]
    public class LogicalExpressionTests
    {
        [TestMethod]
        public void And()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 !> 3 AND 1 < 7", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AndAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 !> 3 AND 1 < 7", root, handler.Item2);
            tree.ShouldBeInstanceOf<AndLogicalConjunctionExpression>();
        }

        [TestMethod]
        public void Equal()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 = 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void GreaterThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 > 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void GreaterThanOrEqual()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 >= 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void LessThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 < 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void LessThanOrEqual()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 <= 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotEqual()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 != 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotEqualIso()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 <> 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotGreater()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 !> 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotLess()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 !< 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void Or()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 !> 3 OR 1 < 7", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void OrAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 !> 3 OR 1 < 7", root, handler.Item2);
            tree.ShouldBeInstanceOf<OrLogicalConjunctionExpression>();
        }
    }
}