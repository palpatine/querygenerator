﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    [TestClass]
    public class BetweenExpressionTests
    {
        [TestMethod]
        public void Between()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse(" Value Between 4 and MaximalValue", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BetweenAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree(" Value Between 4 and MaximalValue", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<BetweenLogicalExpression>();
            expression.IsNegated.ShouldBeFalse();
            expression.LowerBound.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.UpperBound.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void NotBetween()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse(" Value not Between 4 and MaximalValue", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotBetweenAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree(" Value not Between 4 and MaximalValue", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<BetweenLogicalExpression>();
            expression.IsNegated.ShouldBeTrue();
            expression.LowerBound.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.UpperBound.ShouldBeInstanceOf<SqlColumnReference>();
        }
    }
}