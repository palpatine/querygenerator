﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    [TestClass]
    public class IsNullExpressionTests
    {
        [TestMethod]
        public void IsNotNull()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("Value is not null", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void IsNotNullAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("Value is not null", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
            expression.Expression.ShouldBeInstanceOf<SqlColumnReference>();
            expression.IsNegated.ShouldBeTrue();
        }

        [TestMethod]
        public void IsNull()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("Value is null", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void IsNullAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("Value is null", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
            expression.Expression.ShouldBeInstanceOf<SqlColumnReference>();
            expression.IsNegated.ShouldBeFalse();
        }
    }
}