﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    [TestClass]
    public class BinaryLogicalExpressionTests
    {
        [TestMethod]
        public void EqualsThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 = 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void EqualsThanAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 = 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<EqualLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void GreaterOrEqualThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 >= 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void GreaterOrEqualThanAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 >= 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<GreaterThanOrEqualLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void GreaterThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 > 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void GreaterThanAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 > 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<GreaterThanLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void LessOrEqualThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 <= 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void LessOrEqualToAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 <= 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<LessThanOrEqualLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void LessThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 < 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void LessThanAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 < 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<LessThanLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void NotEqualsAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 != 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<NotEqualLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void NotEqualsIsoThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 != 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotEqualsIsoThanAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 <> 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<NotEqualIsoLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void NotEqualsThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 <> 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotGreaterThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 !> 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotGreaterThanAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 !> 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<NotGreaterThanLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void NotLessThan()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("4 !< 3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotLessThanAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("4 !< 3", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<NotLessThanLogicalExpression>();
            expression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Right.ShouldBeInstanceOf<SqlConstantExpression>();
        }
    }
}