﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression
{
    [TestClass]
    public class ExistsTests
    {
        [TestMethod]
        public void Exists()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("Exists(select * from [User])", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void ExistsAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("Exists(select * from [User])", root, handler.Item2);
            var exists = tree.ShouldBeInstanceOf<ExistsExpression>();
            exists.Select.ShouldNotBeNull();
            exists.IsNegated.ShouldBeFalse();
        }

        [TestMethod]
        public void NotExists()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            TestGrammarCreator.Parse("not Exists(select * from [User])", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NotExistsAstNode()
        {
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.LogicalExpression;
            var tree = TestGrammarCreator.CreateAstTree("not Exists(select * from [User])", root, handler.Item2);
            var negation = tree.ShouldBeInstanceOf<SqlLogicalNegateExpression>();
            var exists = negation.Value.ShouldBeInstanceOf<ExistsExpression>();
            exists.Select.ShouldNotBeNull();
            exists.IsNegated.ShouldBeFalse();
        }
    }
}