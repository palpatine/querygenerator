﻿using System;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Parsers.Tsql.Handlers.Helpers;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.OrderBy
{
    internal static class SqlOrderByExpressionHandlerHelper
    {
        public static Tuple<SqlOrderByExpressionHandler, ISqlGrammarElementHandler[]> CreateHandler()
        {
            var termsHandler = new SqlTermsHandler();
            var literalsHandler = new SqlLiteralsHandler();
            ISqlGrammarElementHandler[] handlers = null;
            var provider = new SqlGrammarHandlersProvider(() => handlers);

            var valueExpressionHandler = new SqlValueExpressionsHandler(provider);
            var handler = new SqlOrderByExpressionHandler(provider);
            var aggregateFunctionsHandler = new SqlAggregateExpressionsHandler(provider);
            var sqlIdentifiersHandler = new SqlIdentifiersHandler(provider);
            var sqlSelectHandler = new SqlSelectExpressionHandler(provider);
            var sqlAssignmentExpressionHandler = new SqlAssignmentExpressionHandler(provider);
            var sqlLogicalExpressionsHandler = new SqlLogicalExpressionsHandler(provider);

            handlers = new ISqlGrammarElementHandler[]
            {
                termsHandler,
                literalsHandler,
                sqlIdentifiersHandler,
                aggregateFunctionsHandler,
                valueExpressionHandler,
                handler,
                sqlSelectHandler,
                sqlAssignmentExpressionHandler,
                sqlLogicalExpressionsHandler
            };

            return Tuple.Create(
                handler,
                handlers);
        }
    }
}