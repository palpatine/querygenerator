﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.OrderBy
{
    [TestClass]
    public class OrderByTests
    {
        [TestMethod]
        public void OrderByIdentifierAscendingParsing()
        {
            var handler = SqlOrderByExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.OrderBy;
            TestGrammarCreator.Parse("Order By Name asc", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void OrderByIdentifierAstNode()
        {
            var handler = SqlOrderByExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.OrderBy;
            var tree = TestGrammarCreator.CreateAstTree("Order By Name", root, handler.Item2);
            var order = tree.ShouldBeInstanceOf<IEnumerable<OrderedValueExpression>>();
            order.Single().Ascending.ShouldBeTrue();
            order.Single().Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void OrderByIdentifierDescendingParsing()
        {
            var handler = SqlOrderByExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.OrderBy;
            TestGrammarCreator.Parse("Order By Name desc", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void OrderByIdentifierParsing()
        {
            var handler = SqlOrderByExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.OrderBy;
            TestGrammarCreator.Parse("Order By Name", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void OrderByManyIdentifiersAstNode()
        {
            var handler = SqlOrderByExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.OrderBy;
            var tree = TestGrammarCreator.CreateAstTree("Order By BirthDate, Name asc, Surname desc", root, handler.Item2);
            var order = tree.ShouldBeInstanceOf<IEnumerable<OrderedValueExpression>>();
            order.ElementAt(0).Ascending.ShouldBeTrue();
            order.ElementAt(0).Value.ShouldBeInstanceOf<SqlColumnReference>();

            order.ElementAt(1).Ascending.ShouldBeTrue();
            order.ElementAt(1).Value.ShouldBeInstanceOf<SqlColumnReference>();

            order.ElementAt(2).Ascending.ShouldBeFalse();
            order.ElementAt(2).Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void OrderByManyIdentifiersParsing()
        {
            var handler = SqlOrderByExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.OrderBy;
            TestGrammarCreator.Parse("Order By BirthDate, Name asc, Surname desc", root, handler.Item2).ShouldParse();
        }
    }
}