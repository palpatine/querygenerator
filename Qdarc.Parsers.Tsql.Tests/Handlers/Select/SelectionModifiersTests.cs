﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Select
{
    [TestClass]
    public class SelectionModifiersTests
    {
        [TestMethod]
        public void Distinct()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select distinct Name", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void DistinctAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select distinct Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.IsDistinct.ShouldBeTrue();
        }

        [TestMethod]
        public void NotDistinctAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.IsDistinct.ShouldBeFalse();
        }

        [TestMethod]
        public void NoTopAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Top.ShouldBeNull();
        }

        [TestMethod]
        public void Top()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select Top 1 Name", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void TopPercent()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select Top 1 Percent Name", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void TopPercentAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select Top 1 Percent Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Top.Value.ShouldBeInstanceOf<ISqlValueExpression>();
            select.Top.IsPercent.ShouldBeTrue();
        }

        [TestMethod]
        public void TopValueAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select Top 1 Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Top.Value.ShouldBeInstanceOf<ISqlValueExpression>();
            select.Top.IsPercent.ShouldBeFalse();
            select.Top.WithTies.ShouldBeFalse();
        }

        [TestMethod]
        public void TopValueExpression()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select Top (1+Value) Name", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void TopValueExpressionAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select Top (1+Value) Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Top.Value.ShouldBeInstanceOf<ISqlValueExpression>();
            select.Top.IsPercent.ShouldBeFalse();
        }

        [TestMethod]
        public void TopWithTies()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select Top 1 With Ties Name", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void TopWithTiesAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select Top 1 With Ties Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Top.Value.ShouldBeInstanceOf<ISqlValueExpression>();
            select.Top.WithTies.ShouldBeTrue();
        }
    }
}