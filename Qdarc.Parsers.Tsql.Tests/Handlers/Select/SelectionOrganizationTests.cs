﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Select
{
    [TestClass]
    public class SelectionOrganizationTests
    {
        [TestMethod]
        public void Group()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * From Name Order by Value", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void GroupAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select * From Name Group by Value", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Group.Values.Count().ShouldBeEqual(1);
            select.Group.Filter.ShouldBeNull();
        }

        [TestMethod]
        public void GroupWithHavingAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select * From Name Group by Value Having Sum(Value) > 0", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Group.Values.Count().ShouldBeEqual(1);
            select.Group.Filter.ShouldNotBeNull();
        }

        [TestMethod]
        public void NoOrganizationDefinedAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select * From Name", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Order.ShouldBeNull();
            select.Filter.ShouldBeNull();
            select.Group.ShouldBeNull();
        }

        [TestMethod]
        public void Order()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * From Name Order by Value", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void OrderAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select * From Name Order by Value", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Order.Count().ShouldBeEqual(1);
        }

        [TestMethod]
        public void Where()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * From Name Where Value > 0", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void WhereAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select * From Name Where Value > 0", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Filter.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }
    }
}