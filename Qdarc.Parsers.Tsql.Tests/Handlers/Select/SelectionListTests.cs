﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Select
{
    [TestClass]
    public class SelectionListTests
    {
        [TestMethod]
        public void Alias()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select 11 A", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AliasAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select 11 A", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var aliasedSelectionItem = select.Selection.Elements.Single().ShouldBeInstanceOf<AliasedSelectionItem>();
            aliasedSelectionItem.Alias.ShouldBeEqual("A");
        }

        [TestMethod]
        public void AliasByAssignment()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select A=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AliasByAssignmentAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select A=11", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var aliasedSelectionItem = select.Selection.Elements.Single().ShouldBeInstanceOf<AliasedSelectionItem>();
            aliasedSelectionItem.Alias.ShouldBeEqual("A");
        }

        [TestMethod]
        public void AliasWithAs()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select 11 as A", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AliasWithAsAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select 11 as A", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var aliasedSelectionItem = select.Selection.Elements.Single().ShouldBeInstanceOf<AliasedSelectionItem>();
            aliasedSelectionItem.Alias.ShouldBeEqual("A");
        }

        [TestMethod]
        public void Assignment()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select @A=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AssignmentAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select @A=11", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Selection.Elements.Single().ShouldBeInstanceOf<BaseAssignExpression>();
        }

        [TestMethod]
        public void Asterisk()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select *", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AsteriskAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select *", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var selection = select.Selection.Elements.Single().ShouldBeInstanceOf<AsteriskSelection>();
            selection.Source.ShouldBeNull();
        }

        [TestMethod]
        public void ElementName()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select [A].[B].C", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void ElementNameAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select [A].[B].C", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Selection.Elements.Single().ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void ManyElementsInSelectionList()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select 3+3, [A].C, [schema].[table].*, ROW_NUMBER() over (Partition By City, Name, Surname Order By Age), @A=1, A=0, 5 as ABC, 4 AA", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void PrefixedAsterisk()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select [schema].[table].*", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void PrefixedAsteriskAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select [schema].[table].*", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var asterisk = select.Selection.Elements.Single().ShouldBeInstanceOf<AsteriskSelection>();
            asterisk.Source.Name[1].ShouldBeEqual("table");
            asterisk.Source.Name[0].ShouldBeEqual("schema");
        }

        [TestMethod]
        public void RowNumber()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select ROW_NUMBER() over (Order By Id)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void RowNumberAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select ROW_NUMBER() over (Order By Id)", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var rowNumber = select.Selection.Elements.Single().ShouldBeInstanceOf<RowNumberSelection>();
            rowNumber.Partition.ShouldBeNull();
            rowNumber.Order.Single().Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void RowNumberPartitionByMany()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select ROW_NUMBER() over (Partition By City, Name, Surname Order By Age)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void RowNumberPartitionByManyAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select ROW_NUMBER() over (Partition By City, Name, Surname Order By Age)", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var rowNumber = select.Selection.Elements.Single().ShouldBeInstanceOf<RowNumberSelection>();
            rowNumber.Partition.ShouldNotBeNull();
            rowNumber.Partition.Count().ShouldBeEqual(3);
            rowNumber.Order.Single().Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void RowNumberPartitionByOne()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select ROW_NUMBER() over (Partition By Name Order By Age)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void RowNumberPartitionByOneAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select ROW_NUMBER() over (Partition By Name Order By Age)", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var rowNumber = select.Selection.Elements.Single().ShouldBeInstanceOf<RowNumberSelection>();
            rowNumber.Partition.ShouldNotBeNull();
            rowNumber.Partition.Count().ShouldBeEqual(1);
            rowNumber.Partition.First().ShouldBeInstanceOf<SqlColumnReference>();
            rowNumber.Order.Single().Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void ValueExpression()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select 3+3", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void ValueExpressionAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select 3+3", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.Selection.Elements.Single().ShouldBeInstanceOf<AddValueExpression>();
        }
    }
}