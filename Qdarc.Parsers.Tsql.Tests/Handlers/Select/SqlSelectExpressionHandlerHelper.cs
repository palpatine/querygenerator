﻿using System;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Parsers.Tsql.Handlers.Helpers;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Select
{
    internal static class SqlSelectExpressionHandlerHelper
    {
        public static Tuple<SqlSelectExpressionHandler, ISqlGrammarElementHandler[]> CreateHandler()
        {
            var termsHandler = new SqlTermsHandler();
            var literalsHandler = new SqlLiteralsHandler();
            ISqlGrammarElementHandler[] handlers = null;
            var provider = new SqlGrammarHandlersProvider(() => handlers);
            var valueExpressionHandler = new SqlValueExpressionsHandler(provider);
            var sqlIdentifiersHandler = new SqlIdentifiersHandler(provider);
            var aggregateFunctionsHandler = new SqlAggregateExpressionsHandler(provider);
            var orderByHandler = new SqlOrderByExpressionHandler(provider);
            var sqlLogicalExpressionsHandler = new SqlLogicalExpressionsHandler(provider);
            var sqlSelectExpressionsHandler = new SqlSelectExpressionHandler(provider);
            var sqlAssignmentExpressionHandler = new SqlAssignmentExpressionHandler(provider);

            handlers = new ISqlGrammarElementHandler[]
            {
                termsHandler,
                literalsHandler,
                sqlIdentifiersHandler,
                aggregateFunctionsHandler,
                valueExpressionHandler,
                orderByHandler,
                sqlLogicalExpressionsHandler,
                sqlSelectExpressionsHandler,
                sqlAssignmentExpressionHandler
            };

            return Tuple.Create(
                sqlSelectExpressionsHandler,
                handlers);
        }
    }
}