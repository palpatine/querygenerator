﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Select
{
    [TestClass]
    public class CompoundSelectTests
    {
        [TestMethod]
        public void Union()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select 11 union Select Id From [User]", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void UnionAll()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select 11 union all Select Id From [User]", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void UnionAllAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select 11 union all Select Id From [User]", root, handler.Item2);
            var unionAll = tree.ShouldBeInstanceOf<UnionAllSelectionCommand>();
            unionAll.Left.ShouldNotBeNull();
            unionAll.Right.ShouldNotBeNull();
        }

        [TestMethod]
        public void UnionAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("Select 11 union Select Id From [User]", root, handler.Item2);
            var union = tree.ShouldBeInstanceOf<UnionSelectionCommand>();
            union.Left.ShouldNotBeNull();
            union.Right.ShouldNotBeNull();
        }
    }
}