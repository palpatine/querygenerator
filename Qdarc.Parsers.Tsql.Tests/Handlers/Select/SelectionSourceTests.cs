﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Sources;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Select
{
    [TestClass]
    public class SelectionSourceTests
    {
        [TestMethod]
        public void AliasedSource()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * FROM SchemaName.Name() A", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AliasedSourceUsingAs()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * FROM SchemaName.Name as A", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AliasedSourceWithColumnList()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * FROM SchemaName.Name() A(Id,Name)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void Call()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * FROM SchemaName.Name()", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void ElementName()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * FROM SchemaName.Name", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SelectFromCrossJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        CROSS JOIN [Common].[SystemPermission]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<CrossJoinSource>();
            var left = join.Left.ShouldBeInstanceOf<TableReference>();
            left.Name[0].ShouldBeEqual("Common");
            left.Name[1].ShouldBeEqual("Permission");
            var right = join.Right.ShouldBeInstanceOf<TableReference>();
            right.Name[0].ShouldBeEqual("Common");
            right.Name[1].ShouldBeEqual("SystemPermission");
        }

        [TestMethod]
        public void SelectFromCrossJoinByCommaAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission],
        [Common].[SystemPermission]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<CrossJoinSource>();
            var left = join.Left.ShouldBeInstanceOf<TableReference>();
            left.Name[0].ShouldBeEqual("Common");
            left.Name[1].ShouldBeEqual("Permission");
            var right = join.Right.ShouldBeInstanceOf<TableReference>();
            right.Name[0].ShouldBeEqual("Common");
            right.Name[1].ShouldBeEqual("SystemPermission");
        }

        [TestMethod]
        public void SelectFromFullJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse(query, root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SelectFromFullJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<FullJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromFullOuterJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<FullJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromInnerJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        INNER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<InnerJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<InnerJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromJoinOn()
        {
            var query = @"SELECT *
        FROM [Common].[Permission] p
        JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse(query, root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SelectFromLeftJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        LEFT JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse(query, root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SelectFromLeftJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        Left JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<LeftJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromLeftOuterJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        Left OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<LeftJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromMany()
        {
            var query = @"SELECT *
        FROM [Common].[Permission],
         [Common].[SystemPermission]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse(query, root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SelectFromOuterJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse(query, root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SelectFromRightJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        RIGHT JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse(query, root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SelectFromRightJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        Right JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<RightJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromRightOuterJoinAstNode()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        RIGHT OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree(query, root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<RightJoinSource>();
            join.Left.ShouldBeInstanceOf<TableReference>();
            join.Right.ShouldBeInstanceOf<TableReference>();
            join.On.ShouldBeInstanceOf<SqlBinaryLogicalExpression>();
        }

        [TestMethod]
        public void SubSelect()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("Select * FROM (Select * FROM A)", root, handler.Item2).ShouldParse();
        }
    }
}