﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Commands;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Select
{
    [TestClass]
    public class WithTests
    {
        [TestMethod]
        public void With()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("With A AS (Select * FROM [User]) Select * FROM A", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void WithAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("With A AS (Select * FROM [User]) Select * FROM A", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.With.Single().ShouldNotBeNull();
            var with = select.With.Single();
            with.Columns.ShouldBeNull();
            with.Name.ShouldBeEqual("A");
            with.Select.ShouldNotBeNull();
        }

        [TestMethod]
        public void WithColumns()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            TestGrammarCreator.Parse("With A(Id) AS (Select * FROM [User]) Select * FROM A", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void WithColumnsAstNode()
        {
            var handler = SqlSelectExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Select;
            var tree = TestGrammarCreator.CreateAstTree("With A(Id) AS (Select * FROM [User]) Select * FROM A", root, handler.Item2);
            var select = tree.ShouldBeInstanceOf<SelectCommand>();
            select.With.Single().ShouldNotBeNull();
            var with = select.With.Single();
            with.Columns.ShouldNotBeNull();
            with.Columns.Single().ShouldBeEqual("Id");
            with.Name.ShouldBeEqual("A");
            with.Select.ShouldNotBeNull();
        }
    }
}