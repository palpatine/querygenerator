﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Assignments
{
    [TestClass]
    public class AssignmentsTests
    {
        [TestMethod]
        public void AddAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A+=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AddAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A+=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<AddAssign>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void Assign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void AssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<Assign>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void BitwiseAndAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A&=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BitwiseAndAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A&=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<BitwiseAdd>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void BitwiseOrAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A|=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BitwiseOrAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A|=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<BitwiseOr>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void BitwiseXorAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A^=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BitwiseXorAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A^=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<BitwiseExclusiveOr>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void DivideAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A/=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void DivideAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A/=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<DivideAssign>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void ModuloAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A%=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void ModuloAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A%=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<ModuloAssign>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void MultiplyAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A*=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void MultiplyAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A*=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<MultiplyAssign>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }

        [TestMethod]
        public void SubtractAssign()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            TestGrammarCreator.Parse("@A-=11", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SubtractAssignAstNode()
        {
            var handler = SqlAssignmentExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Assignment;
            var tree = TestGrammarCreator.CreateAstTree("@A-=11", root, handler.Item2);
            var assign = tree.ShouldBeInstanceOf<SubtractAssign>();
            assign.Value.ShouldBeInstanceOf<SqlConstantExpression>();
            assign.Assignable.ShouldNotBeNull();
        }
    }
}