﻿using System;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Parsers.Tsql.Handlers.Helpers;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Identifiers
{
    internal static class SqlIdentifierHandlerHelper
    {
        public static Tuple<SqlIdentifiersHandler, ISqlGrammarElementHandler[]> CreateHandler()
        {
            var termsHandler = new SqlTermsHandler();
            var sqlLiteralsHandler = new SqlLiteralsHandler();
            ISqlGrammarElementHandler[] handlers = null;
            var provider = new SqlGrammarHandlersProvider(() => handlers);

            var sqlIdentifiersHandler = new SqlIdentifiersHandler(provider);
            handlers = new ISqlGrammarElementHandler[]
            {
                termsHandler,
                sqlIdentifiersHandler,
                sqlLiteralsHandler
            };

            return Tuple.Create(
                sqlIdentifiersHandler,
                handlers);
        }
    }
}