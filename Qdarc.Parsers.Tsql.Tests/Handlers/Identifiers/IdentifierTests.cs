﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Identifiers
{
    [TestClass]
    public class IdentifierTests
    {
        [TestMethod]
        public void BracedIdentifier()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = new NonTerminal("N");
            root.Rule = handler.Item1.Identifier;
            TestGrammarCreator.Parse("[Name]", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BracedIdentifierCanContainSpaces()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = new NonTerminal("N");
            root.Rule = handler.Item1.Identifier;
            TestGrammarCreator.Parse("[Name Name]", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SimpleIdentifier()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = new NonTerminal("N");
            root.Rule = handler.Item1.Identifier;
            TestGrammarCreator.Parse("Name", root, handler.Item2).ShouldParse();
        }
    }
}