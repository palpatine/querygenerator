﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Names;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Identifiers
{
    [TestClass]
    public class ElementNameTests
    {
        [TestMethod]
        public void DoubleElementName()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = handler.Item1.ElementFullName;
            TestGrammarCreator.Parse("First.Second", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void DoubleElementNameAstNode()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = handler.Item1.ElementFullName;
            var node = TestGrammarCreator.CreateAstTree("First.Second", root, handler.Item2);
            var elementName = node.ShouldBeInstanceOf<ElementName>();
            elementName.Count.ShouldBeEqual(2);
            elementName[0].ShouldBeEqual("First");
            elementName[1].ShouldBeEqual("Second");
        }

        [TestMethod]
        public void SingleElementName()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = handler.Item1.ElementFullName;
            TestGrammarCreator.Parse("Element", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SingleElementNameAstNode()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = handler.Item1.ElementFullName;
            var node = TestGrammarCreator.CreateAstTree("Element", root, handler.Item2);
            var elementName = node.ShouldBeInstanceOf<ElementName>();
            elementName.Count.ShouldBeEqual(1);
            elementName[0].ShouldBeEqual("Element");
        }

        [TestMethod]
        public void TripleElementName()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = handler.Item1.ElementFullName;
            TestGrammarCreator.Parse("First.Second.Third", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void TripleElementNameAstNode()
        {
            var handler = SqlIdentifierHandlerHelper.CreateHandler();
            var root = handler.Item1.ElementFullName;
            var node = TestGrammarCreator.CreateAstTree("First.Second.Third", root, handler.Item2);
            var elementName = node.ShouldBeInstanceOf<ElementName>();
            elementName.Count.ShouldBeEqual(3);
            elementName[0].ShouldBeEqual("First");
            elementName[1].ShouldBeEqual("Second");
            elementName[2].ShouldBeEqual("Third");
        }
    }
}