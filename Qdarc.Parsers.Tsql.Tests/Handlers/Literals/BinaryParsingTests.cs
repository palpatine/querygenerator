﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Literals
{
    [TestClass]
    public class BinaryParsingTests
    {
        [TestMethod]
        public void BinaryValue()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Binary;
            var tree = TestGrammarCreator.Parse("0xA4829F1", root, literalsHandler);
            tree.ShouldParse();
            var value = tree.Root.ChildNodes[0].Token.Value.ShouldBeInstanceOf<byte[]>();
            value[0].ShouldBeEqual((byte)0x0A);
            value[1].ShouldBeEqual((byte)0x48);
            value[2].ShouldBeEqual((byte)0x29);
            value[3].ShouldBeEqual((byte)0xF1);
        }

        [TestMethod]
        public void VarBinaryAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.Binary;
            var tree = TestGrammarCreator.CreateAstTree("0xA4829F1", root, literalsHandler);
            var constant = tree.ShouldBeInstanceOf<SqlConstantExpression>();
            var value = constant.Value.ShouldBeInstanceOf<byte[]>();
            value[0].ShouldBeEqual((byte)0x0A);
            value[1].ShouldBeEqual((byte)0x48);
            value[2].ShouldBeEqual((byte)0x29);
            value[3].ShouldBeEqual((byte)0xF1);
            var type = constant.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.VarBinary);
            constant.ClrType.ShouldBeEqual(typeof(byte[]));
        }
    }
}