﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Literals
{
    [TestClass]
    public class StringParsingTests
    {
        [TestMethod]
        public void NVarCharAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.Text;
            var tree = TestGrammarCreator.CreateAstTree("N'abcd'", root, literalsHandler);
            var constant = tree.ShouldBeInstanceOf<SqlConstantExpression>();
            constant.Value.ShouldBeEqual("abcd");
            var type = constant.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.NVarChar);
            constant.ClrType.ShouldBeEqual(typeof(string));
        }

        [TestMethod]
        public void NVarCharText()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Text;
            var tree = TestGrammarCreator.Parse("N'asdf'", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual("asdf");
        }

        [TestMethod]
        public void UnQuotedText()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Text;
            var tree = TestGrammarCreator.Parse("asdf", root, literalsHandler);
            tree.ShouldNotParse("Invalid character: 'a'.");
        }

        [TestMethod]
        public void VarCharAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.Text;
            var tree = TestGrammarCreator.CreateAstTree("'abcd'", root, literalsHandler);
            var constant = tree.ShouldBeInstanceOf<SqlConstantExpression>();
            constant.Value.ShouldBeEqual("abcd");
            var type = constant.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.VarChar);
            constant.ClrType.ShouldBeEqual(typeof(string));
        }

        [TestMethod]
        public void VarCharText()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Text;
            var tree = TestGrammarCreator.Parse("'asdf'", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual("asdf");
        }
    }
}