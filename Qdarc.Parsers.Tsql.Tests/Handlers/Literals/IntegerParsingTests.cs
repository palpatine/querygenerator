﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Literals
{
    [TestClass]
    public class IntegerParsingTests
    {
        [TestMethod]
        public void DecimalNumberAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.Number;
            var tree = TestGrammarCreator.CreateAstTree("79228162514264337593543950335", root, literalsHandler);
            var constant = tree.ShouldBeInstanceOf<SqlConstantExpression>();
            constant.Value.ShouldBeEqual(79228162514264337593543950335M);
            var type = constant.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.Numeric);
            constant.ClrType.ShouldBeEqual(typeof(decimal));
        }

        [TestMethod]
        public void ExplicitPositiveNumber()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            TestGrammarCreator.Parse("+1", root, literalsHandler).ShouldParse();
        }

        [TestMethod]
        public void IntegerNumberAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.Number;
            var tree = TestGrammarCreator.CreateAstTree("1", root, literalsHandler);
            var constant = tree.ShouldBeInstanceOf<SqlConstantExpression>();
            constant.Value.ShouldBeEqual(1);
            var type = constant.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.Int);
            constant.ClrType.ShouldBeEqual(typeof(int));
        }

        [TestMethod]
        public void Letter()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            TestGrammarCreator.Parse("x", root, literalsHandler).ShouldNotParse("Invalid character: 'x'.");
        }

        [TestMethod]
        public void LetterAfterNumbers()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            TestGrammarCreator.Parse("333x", root, literalsHandler).ShouldNotParse("Number cannot be followed by a letter.");
        }

        [TestMethod]
        public void LetterBeforeNumbers()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            TestGrammarCreator.Parse("x333", root, literalsHandler).ShouldNotParse("Invalid character: 'x'.");
        }

        [TestMethod]
        public void LetterInsideNumbers()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            TestGrammarCreator.Parse("333x33", root, literalsHandler).ShouldNotParse("Number cannot be followed by a letter.");
        }

        [TestMethod]
        public void LongNumberAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.Number;
            var tree = TestGrammarCreator.CreateAstTree("2147483648", root, literalsHandler);
            var constant = tree.ShouldBeInstanceOf<SqlConstantExpression>();
            constant.Value.ShouldBeEqual(2147483648L);
            var type = constant.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypesNames.Numeric);
            constant.ClrType.ShouldBeEqual(typeof(long));
        }

        [TestMethod]
        public void MaximalCsNumber()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            var tree = TestGrammarCreator.Parse("79228162514264337593543950335", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual(79228162514264337593543950335M);
            tree.Root.ChildNodes[0].Token.Value.GetType().ShouldBeEqual(typeof(decimal));
        }

        [TestMethod]
        public void MinimalCsNumber()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            var tree = TestGrammarCreator.Parse("-79228162514264337593543950335", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual(-79228162514264337593543950335M);
            tree.Root.ChildNodes[0].Token.Value.GetType().ShouldBeEqual(typeof(decimal));
        }

        [TestMethod]
        public void NegativeNumber()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            var tree = TestGrammarCreator.Parse("-1", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual(-1);
            tree.Root.ChildNodes[0].Token.Value.GetType().ShouldBeEqual(typeof(int));
        }

        [TestMethod]
        public void SimpleNumber()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.Number;
            var tree = TestGrammarCreator.Parse("1", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual(1);
            tree.Root.ChildNodes[0].Token.Value.GetType().ShouldBeEqual(typeof(int));
        }
    }
}