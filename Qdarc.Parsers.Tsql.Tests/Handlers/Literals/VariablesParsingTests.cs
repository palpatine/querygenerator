﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Literals
{
    [TestClass]
    public class VariablesParsingTests
    {
        [TestMethod]
        public void SystemVariableAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.SystemVariableName;
            var tree = TestGrammarCreator.CreateAstTree("@@SystemVariable", root, literalsHandler);
            var variable = tree.ShouldBeInstanceOf<SqlVariable>();
            variable.Name.ShouldBeEqual("@@SystemVariable");
            variable.IsSystem.ShouldBeTrue();
        }

        [TestMethod]
        public void SystemVariableIsNotVariable()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.VariableName;
            var tree = TestGrammarCreator.Parse("@@Assignable", root, literalsHandler);
            tree.ShouldNotParse("Invalid character: '@'.");
        }

        [TestMethod]
        public void SystemVariableValue()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.SystemVariableName;
            var tree = TestGrammarCreator.Parse("@@SystemVariable", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual("@@SystemVariable");
        }

        [TestMethod]
        public void VariableAstNode()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N", (x, y) => y.AstNode = y.ChildNodes[0].AstNode);
            root.Rule = literalsHandler.VariableName;
            var tree = TestGrammarCreator.CreateAstTree("@Assignable", root, literalsHandler);
            var variable = tree.ShouldBeInstanceOf<SqlVariable>();
            variable.Name.ShouldBeEqual("@Assignable");
            variable.IsSystem.ShouldBeFalse();
        }

        [TestMethod]
        public void VariableValue()
        {
            var literalsHandler = new SqlLiteralsHandler();
            var root = new NonTerminal("N");
            root.Rule = literalsHandler.VariableName;
            var tree = TestGrammarCreator.Parse("@Assignable", root, literalsHandler);
            tree.ShouldParse();
            tree.Root.ChildNodes[0].Token.Value.ShouldBeEqual("@Assignable");
        }
    }
}