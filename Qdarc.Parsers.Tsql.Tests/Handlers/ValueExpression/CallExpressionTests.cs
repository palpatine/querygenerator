﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class CallExpressionTests
    {
        [TestMethod]
        public void CallMultipleParameters()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.CallExpression;
            TestGrammarCreator.Parse("FunctionName(5, 'abc', @Parameter)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CallSingleParameter()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.CallExpression;
            TestGrammarCreator.Parse("FunctionName(@Parameter)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CallWithoutParameters()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.CallExpression;
            TestGrammarCreator.Parse("FunctionName()", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CallWithoutParametersWithQualifiedName()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.CallExpression;
            TestGrammarCreator.Parse("SchemaName.FunctionName()", root, handler.Item2).ShouldParse();
        }
    }
}