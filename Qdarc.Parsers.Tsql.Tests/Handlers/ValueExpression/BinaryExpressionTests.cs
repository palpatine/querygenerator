﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class BinaryExpressionTests
    {
        [TestMethod]
        public void BitwiseAnd()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 & 4", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BitwiseExclusiveOr()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 ^ 4", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BitwiseOr()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 | 4", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void Division()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 / 4", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void Modulo()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 % 4", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void Multiplication()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 * 4", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void MultiplicationHasHigherPriorityThenSum()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;

            var firstTree = TestGrammarCreator.CreateAstTree("4 + 4 * 4", root, handler.Item2);
            var lastInFirst = firstTree.ShouldBeInstanceOf<AddValueExpression>();
            lastInFirst.Right.ShouldBeInstanceOf<MultiplyValueExpression>();

            var secondTree = TestGrammarCreator.CreateAstTree("4 * 4 + 4", root, handler.Item2);
            var lastInSecond = secondTree.ShouldBeInstanceOf<AddValueExpression>();
            lastInSecond.Left.ShouldBeInstanceOf<MultiplyValueExpression>();
        }

        [TestMethod]
        public void MultiplyDivideModuloHaveTheSamePriority()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;

            var firstTree = TestGrammarCreator.CreateAstTree("4 * 4 / 4 % 4", root, handler.Item2);
            var lastInFirst = firstTree.ShouldBeInstanceOf<ModuloValueExpression>();
            var beforeLastInFirst = lastInFirst.Left.ShouldBeInstanceOf<DivideValueExpression>();
            beforeLastInFirst.Left.ShouldBeInstanceOf<MultiplyValueExpression>();

            var secondTree = TestGrammarCreator.CreateAstTree("4 * 4 % 4 / 4", root, handler.Item2);
            var lastInSecond = secondTree.ShouldBeInstanceOf<DivideValueExpression>();
            var beforeLastInSecond = lastInSecond.Left.ShouldBeInstanceOf<ModuloValueExpression>();
            beforeLastInSecond.Left.ShouldBeInstanceOf<MultiplyValueExpression>();

            var thirdTree = TestGrammarCreator.CreateAstTree("4 / 4 % 4 * 4", root, handler.Item2);
            var lastInThird = thirdTree.ShouldBeInstanceOf<MultiplyValueExpression>();
            var beforeLastInThird = lastInThird.Left.ShouldBeInstanceOf<ModuloValueExpression>();
            beforeLastInThird.Left.ShouldBeInstanceOf<DivideValueExpression>();

            var fourthTree = TestGrammarCreator.CreateAstTree("4 / 4 * 4 % 4", root, handler.Item2);
            var lastInForth = fourthTree.ShouldBeInstanceOf<ModuloValueExpression>();
            var beforeLastInForth = lastInForth.Left.ShouldBeInstanceOf<MultiplyValueExpression>();
            beforeLastInForth.Left.ShouldBeInstanceOf<DivideValueExpression>();

            var fivethTree = TestGrammarCreator.CreateAstTree("4 % 4 * 4 / 4", root, handler.Item2);
            var lastInFiveth = fivethTree.ShouldBeInstanceOf<DivideValueExpression>();
            var beforeLastInFiveth = lastInFiveth.Left.ShouldBeInstanceOf<MultiplyValueExpression>();
            beforeLastInFiveth.Left.ShouldBeInstanceOf<ModuloValueExpression>();

            var sixthTree = TestGrammarCreator.CreateAstTree("4 % 4 / 4 * 4", root, handler.Item2);
            var lastInSixth = sixthTree.ShouldBeInstanceOf<MultiplyValueExpression>();
            var beforeLastInSixth = lastInSixth.Left.ShouldBeInstanceOf<DivideValueExpression>();
            beforeLastInSixth.Left.ShouldBeInstanceOf<ModuloValueExpression>();
        }

        [TestMethod]
        public void Subtraction()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 - 4", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void SubtractionAndSumHaveTheSamePriority()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;

            var firstTree = TestGrammarCreator.CreateAstTree("4 + 4 - 4", root, handler.Item2);
            var lastInFirst = firstTree.ShouldBeInstanceOf<SubtractValueExpression>();
            lastInFirst.Left.ShouldBeInstanceOf<AddValueExpression>();

            var secondTree = TestGrammarCreator.CreateAstTree("4 - 4 + 4", root, handler.Item2);
            var lastInSecond = secondTree.ShouldBeInstanceOf<AddValueExpression>();
            lastInSecond.Left.ShouldBeInstanceOf<SubtractValueExpression>();
        }

        [TestMethod]
        public void Sum()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("4 + 4", root, handler.Item2).ShouldParse();
        }
    }
}