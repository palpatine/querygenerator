﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class NullValueExpressionTests
    {
        [TestMethod]
        public void NullExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("null", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void NullExpressionAstNode()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("null", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlNullExpression>();
        }
    }
}