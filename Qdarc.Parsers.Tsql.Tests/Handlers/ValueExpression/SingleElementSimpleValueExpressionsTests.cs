﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class SingleElementSimpleValueExpressionsTests
    {
        [TestMethod]
        public void BinaryAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("'asdf'", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void ColumnValueByColumnNameAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("ColumnName", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void ColumnValueBySchemaNameTableNameColumnNameAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("SchemaName.Name.ColumnName", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void ColumnValueByTableNameColumnNameAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("Name.ColumnName", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void NumberAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("79228162514264337593543950335", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void SystemVariableAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("@Assignable", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlVariable>();
        }

        [TestMethod]
        public void TextAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("'asdf'", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void VariableAsValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("@Assignable", root, handler.Item2);
            tree.ShouldBeInstanceOf<SqlVariable>();
        }
    }
}