﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class InlineIfTests
    {
        [TestMethod]
        public void InlineIf()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("IIF(Value is null, First, Second)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void InlineIfAstNode()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("IIF(Value is null, 5, Second)", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlInlineIfExpression>();
            expression.Condition.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
            expression.IfTrue.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.IfFalse.ShouldBeInstanceOf<SqlColumnReference>();
        }
    }
}