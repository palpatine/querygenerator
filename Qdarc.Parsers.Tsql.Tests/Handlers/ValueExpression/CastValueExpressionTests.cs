﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class CastValueExpressionTests
    {
        [TestMethod]
        public void Cast()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("Cast(5 as bigint)", root, handler.Item2).ShouldParse();
        }
    }
}