﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class ConvertExpressionTests
    {
        [TestMethod]
        public void Convert()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("Convert(int, Value)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void ConvertAstNode()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("Convert(int, Value)", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlConvertExpression>();
            expression.Expression.ShouldBeInstanceOf<SqlColumnReference>();
            expression.SqlType.ShouldBeInstanceOf<TsqlType>();
        }
    }
}