﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class CoalesceExpressionTests
    {
        [TestMethod]
        public void Coalesce()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("Coalesce(Value, OtherValue, 0)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void ConvertAstNode()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("Coalesce(Value, OtherValue, 0)", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlCoalesceExpression>();
            expression.Expressions.Count().ShouldBeEqual(3);
        }
    }
}