﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class ValueExpressionTests
    {
        [TestMethod]
        public void CountIsASimpleValueExpression()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("Count(*)", root, handler.Item2).ShouldParse();
        }
    }
}