﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.ValueExpression
{
    [TestClass]
    public class SqlUnaryExpressionTests
    {
        [TestMethod]
        public void BitwiseNegation()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("~Value", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void BitwiseNegationAstNode()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("~Value", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<NegateValueExpression>();
            expression.ValueExpression.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void Minus()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("-Value", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void MinusAstNode()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("-Value", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlUnaryMinusExpression>();
            expression.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void Plus()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            TestGrammarCreator.Parse("+Value", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void PlusAstNode()
        {
            var handler = SqlValueExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.ValueExpression;
            var tree = TestGrammarCreator.CreateAstTree("+Value", root, handler.Item2);
            var expression = tree.ShouldBeInstanceOf<SqlUnaryPlusExpression>();
            expression.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }
    }
}