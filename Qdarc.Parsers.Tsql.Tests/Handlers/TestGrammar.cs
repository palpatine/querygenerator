﻿using Irony.Parsing;
using Qdarc.Parsers.Tsql.Tests.Handlers.LogicalExpression;

namespace Qdarc.Parsers.Tsql.Tests.Handlers
{
    public class TestGrammar : Grammar
    {
        public TestGrammar()
            : base(false)
        {
            LanguageFlags = LanguageFlags.CreateAst;
            var handler = SqlLogicalExpressionHandlerHelper.CreateHandler();
            foreach (var sqlGrammarElementHandler in handler.Item2)
            {
                sqlGrammarElementHandler.Initialize(this);
            }

            Root = handler.Item1.LogicalExpression;
        }
    }
}