﻿using System.Linq;
using System.Text.RegularExpressions;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Parsers.Tsql.Handlers.Helpers;

namespace Qdarc.Parsers.Tsql.Tests.Handlers
{
    internal static class TestGrammarCreator
    {
        public static object CreateAstTree(string query, NonTerminal root, params ISqlGrammarElementHandler[] handlers)
        {
            var grammar = CreateGrammar(LanguageFlags.CreateAst, root, handlers);
            var parser = CreateParser(grammar);
            parser.Parse(query);
            var parseTree = parser.Parse(query);
            parseTree.ShouldParse();
            return parseTree.Root.AstNode;
        }

        public static ParseTree Parse(string query, NonTerminal root, params ISqlGrammarElementHandler[] handlers)
        {
            var grammar = CreateGrammar(LanguageFlags.Default, root, handlers);
            var parser = CreateParser(grammar);
            parser.Parse(query);
            var parseTree = parser.Parse(query);
            return parseTree;
        }

        public static void ShouldMatch(this string value, string pattern, string errorMessage)
        {
            if (!Regex.IsMatch(value, pattern))
            {
                throw new AssertException($"Input does not match pattern. {value} ; {pattern} ; {errorMessage}");
            }
        }

        public static void ShouldNotParse(this ParseTree parseTree, string pattern)
        {
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Error, string.Join(",", parseTree.ParserMessages));
            parseTree.ParserMessages[0].Message.ShouldMatch(pattern, string.Join(",", parseTree.ParserMessages));
        }

        public static void ShouldParse(this ParseTree parseTree)
        {
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Grammar CreateGrammar(LanguageFlags flags, NonTerminal root, params ISqlGrammarElementHandler[] handlers)
        {
            var grammar = new Grammar(false);
            grammar.LanguageFlags = flags;

            foreach (var handler in handlers)
            {
                handler.Initialize(grammar);
            }

            grammar.Root = root;

            return grammar;
        }

        private static Parser CreateParser(Grammar grammar)
        {
            var language = new LanguageData(grammar);
            var parser = new Parser(language);
            parser.Context.TracingEnabled = true;
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive(string.Join(", ", parser.Language.Errors.Select(x => x.Message)));
            }

            return parser;
        }
    }
}