using System;
using Qdarc.Parsers.Tsql.Handlers;
using Qdarc.Parsers.Tsql.Handlers.Helpers;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Aggregate
{
    internal static class SqlAggregateExpressionHandlerHelper
    {
        public static Tuple<SqlAggregateExpressionsHandler, ISqlGrammarElementHandler[]> CreateHandler()
        {
            var termsHandler = new SqlTermsHandler();
            var literalsHandler = new SqlLiteralsHandler();

            ISqlGrammarElementHandler[] handlers = null;
            var provider = new SqlGrammarHandlersProvider(() => handlers);

            var sqlIdentifiersHandler = new SqlIdentifiersHandler(provider);
            var valueExpressionHandler = new SqlValueExpressionsHandler(provider);
            var orderByHandler = new SqlOrderByExpressionHandler(provider);
            var sqlSelectHandler = new SqlSelectExpressionHandler(provider);
            var sqlAssignmentExpressionHandler = new SqlAssignmentExpressionHandler(provider);
            var handler = new SqlAggregateExpressionsHandler(provider);
            var sqlLogicalExpressionsHandler = new SqlLogicalExpressionsHandler(provider);

            handlers = new ISqlGrammarElementHandler[]
            {
                termsHandler,
                literalsHandler,
                sqlIdentifiersHandler,
                valueExpressionHandler,
                orderByHandler,
                sqlSelectHandler,
                sqlAssignmentExpressionHandler,
                handler,
                sqlLogicalExpressionsHandler
            };

            return Tuple.Create(
                handler,
                handlers);
        }
    }
}