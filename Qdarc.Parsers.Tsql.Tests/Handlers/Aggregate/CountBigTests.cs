﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Tests.Handlers.Aggregate
{
    [TestClass]
    public class CountBigTests
    {
        [TestMethod]
        public void CountAllIdentifier()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            TestGrammarCreator.Parse("Count_Big(all Name)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CountAllIdentifierAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(all Name)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlCountBigExpression>();
            count.IsDistinct.ShouldBeFalse();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void CountAsterisk()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            TestGrammarCreator.Parse("Count_Big(*)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CountAsteriskAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(*)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlAsteriskCountBigExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
        }

        [TestMethod]
        public void CountAsteriskOverOrder()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            TestGrammarCreator.Parse("Count_Big(*) over(order by Name)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CountAsteriskOverOrderAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(*) over(order by Name)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlAsteriskCountBigExpression>();
            count.Order.ShouldNotBeNull();
            count.Partition.ShouldBeNull();
        }

        [TestMethod]
        public void CountAsteriskOverPartition()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            TestGrammarCreator.Parse("Count_Big(*) over(partition by Name)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CountAsteriskOverPartitionAndOrder()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            TestGrammarCreator.Parse("Count_Big(*) over(partition by BirthYear order by Name)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CountAsteriskOverPartitionAndOrderAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(*) over(partition by BirthYear order by Name)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlAsteriskCountBigExpression>();
            count.Order.ShouldNotBeNull();
            count.Partition.ShouldNotBeNull();
        }

        [TestMethod]
        public void CountAsteriskOverPartitionAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(*) over(partition by Name)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlAsteriskCountBigExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldNotBeNull();
        }

        [TestMethod]
        public void CountDistinctIdentifier()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            TestGrammarCreator.Parse("Count_Big(distinct Name)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CountDistinctIdentifierAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(distinct Name)", root, handler.Item2);

            var count = tree.ShouldBeInstanceOf<SqlCountBigExpression>();
            count.IsDistinct.ShouldBeTrue();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void CountIdentifier()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            TestGrammarCreator.Parse("Count_Big(Name)", root, handler.Item2).ShouldParse();
        }

        [TestMethod]
        public void CountIdentifierAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(Name)", root, handler.Item2);

            var count = tree.ShouldBeInstanceOf<SqlCountBigExpression>();
            count.IsDistinct.ShouldBeFalse();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void CountOverOrderAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(Name) over(order by Name)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlCountBigExpression>();
            count.IsDistinct.ShouldBeFalse();
            count.Order.ShouldNotBeNull();
            count.Partition.ShouldBeNull();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void CountOverPartitionAndOrderAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(Name) over(partition by BirthYear order by Name)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlCountBigExpression>();
            count.IsDistinct.ShouldBeFalse();
            count.Order.ShouldNotBeNull();
            count.Partition.ShouldNotBeNull();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void CountOverPartitionAstNode()
        {
            var handler = SqlAggregateExpressionHandlerHelper.CreateHandler();
            var root = handler.Item1.Count;
            var tree = TestGrammarCreator.CreateAstTree("Count_Big(Name) over(partition by Name)", root, handler.Item2);
            var count = tree.ShouldBeInstanceOf<SqlCountBigExpression>();
            count.IsDistinct.ShouldBeFalse();
            count.Order.ShouldBeNull();
            count.Partition.ShouldNotBeNull();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }
    }
}