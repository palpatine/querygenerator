﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class UserDefinedTypesTests
    {
        [TestMethod]
        public void CreateTableType()
        {
            var query = @"CREATE TYPE [Common].[AssignmentJobRoleList] AS TABLE(
	[Id] [int] NOT NULL,
	[ElementId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[JobTime] [decimal](28, 3) NULL,
	[JobHours] [int] NULL,
	[DateStart] [smalldatetime] NOT NULL,
	[DateEnd] [smalldatetime] NULL)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateTableTypeWithIndex()
        {
            var query = @"CREATE TYPE [Common].[AssignmentJobRoleList] AS TABLE(
	[Id] [int] NOT NULL,
	[ElementId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[JobTime] [decimal](28, 3) NULL,
	[JobHours] [int] NULL,
	[DateStart] [smalldatetime] NOT NULL,
	[DateEnd] [smalldatetime] NULL,
	UNIQUE NONCLUSTERED
(
	[Id] ASC
))";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}