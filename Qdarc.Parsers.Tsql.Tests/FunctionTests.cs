﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class FunctionTests
    {
        [TestMethod]
        [Ignore]
        public void AlterFunction()
        {
            var query = @"ALTER FUNCTION [Common].[GetUserAndOrganizationByLogin](
    @OrganizationName NVARCHAR(25)
  , @LoginName NVARCHAR(20))
RETURNS TABLE
    WITH SCHEMABINDING
AS
RETURN
(SELECT * FROM User)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateCalendarFunction()
        {
            var query = @"CREATE FUNCTION [Common].[Calendar](
    @Year INT
  , @Month INT)
RETURNS TABLE
    WITH SCHEMABINDING
AS
RETURN
    WITH
    [CalendarCTE] AS (
        SELECT
            DATEFROMPARTS(@Year, @Month, 1) AS [Date]
        UNION ALL
        SELECT
            DATEADD([dd], 1, [Date])
        FROM [CalendarCTE]
        WHERE [Date] < DATEADD([dd], -1, DATEADD([mm], 1, DATEFROMPARTS(@Year, @Month, 1)))
    )
    SELECT
        [c].[Date]
        , CAST(IIF(DATEPART([dw], [c].[Date]) IN(1, 7), 1, 0) AS BIT) AS [IsFreeDay]
    FROM [CalendarCTE] AS [c];
";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateFunction()
        {
            var query = @"Create FUNCTION [Common].[GetUserAndOrganizationByLogin]()
RETURNS INT
AS
BEGIN
RETURN (5)
END";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateFunctionReturnImplicitValueWithoutBlockIsInvalid()
        {
            var query = @"Create FUNCTION [Common].[GetUserAndOrganizationByLogin]()
RETURNS INT
AS
RETURN(5);
";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Error);
        }

        [TestMethod]
        public void CreateFunctionReturnsConcreteTable()
        {
            var query = @"Create FUNCTION [Numbers]()
RETURNS @Result TABLE ([Number] [INT])
AS
BEGIN
INSERT @Result SELECT 3
RETURN
END";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateFunctionWithBlockAndReturnImplicitValue()
        {
            var query = @"Create FUNCTION [Common].[GetUserAndOrganizationByLogin]()
RETURNS INT
AS
BEGIN
RETURN(5);
END";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}