﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class ProcedureTests
    {
        [TestMethod]
        public void CreateProcedure()
        {
            var query = @"CREATE PROCEDURE [Common].[Update_Organization_User]
(
    @ScopeId INT,
    @ChangerId INT,
    @MasterColumnName VARCHAR(50),
    @MasterId INT,
    @NewList [Common].[AssignmentRoleList] READONLY
)
AS
BEGIN
    SELECT * FROM UserEntity User
END";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}