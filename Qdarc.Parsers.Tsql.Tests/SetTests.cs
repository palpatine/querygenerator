﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class SetTests
    {
        [TestMethod]
        public void ScriptConfigurationSetting()
        {
            var query = "SET ANSI_NULLS ON";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ScriptConfigurationSettings()
        {
            var query = "SET ANSI_NULLS ON SET QUOTED_IDENTIFIER ON";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SetIdentityInsert()
        {
            var query = "SET IDENTITY_INSERT [Common].[Company] ON";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SetScriptVariableWithSystemVariable()
        {
            var query = "SET @MessageCode = @@Identity";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SetScriptVariableWithValue()
        {
            var query = "SET @MessageCode = 'NoLockError'";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SetScriptVariableWithVariable()
        {
            var query = "SET @MessageCode = @variable";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}