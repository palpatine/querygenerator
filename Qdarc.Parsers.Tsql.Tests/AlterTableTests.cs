﻿using System.IO;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class AlterTableTests
    {
        [TestMethod]
        public void AddCheck()
        {
            var query = @"ALTER TABLE [Common].[Module]
    ADD CONSTRAINT [CK_Module_Required]
    CHECK ([Common].[IsRequiredModulesExists]([Required]) = 1)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void AddForeignKeyColumnListUpdateDeleteNoAction()
        {
            var query = @"ALTER TABLE [Common].[AddressCompany]
ADD CONSTRAINT [FK_AddressCompany_AddressId] FOREIGN KEY ([AddressId])
  REFERENCES [Common].[Address] ([Id])
  ON UPDATE NO ACTION
  ON DELETE NO ACTION";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        [Ignore]
        public void Test()
        {
            var directory = @"C:\Repos\bss\BSSystem\Common\Database\Schema";
            var files = Directory.GetFiles(directory, "*.sql", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                var query = File.ReadAllText(file);
                var parser = CreateParser();

                var parseTree = parser.Parse(query);

                parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
            }
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}