﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class CreateTableTests
    {
        [TestMethod]
        public void ColumnConstraintCheck()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] CHECK ([UserId] > 4));";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintDefaultValue()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [Number] UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKey()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User]);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnDeleteCascade()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON DELETE CASCADE);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnDeleteNoAction()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON DELETE NO ACTION);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnDeleteSetDefault()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON DELETE SET DEFAULT);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnDeleteSetNull()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON DELETE SET NULL);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnUpdateCascade()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON UPDATE CASCADE);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnUpdateNoAction()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON UPDATE NO ACTION);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnUpdateOnDeleteSetDefault()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON UPDATE SET DEFAULT ON DELETE SET DEFAULT);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnUpdateSetDefault()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON UPDATE SET DEFAULT);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyOnUpdateSetNull()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User] ON UPDATE SET NULL);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintForeignKeyWithColumn()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [UserId] [int] FOREIGN KEY REFERENCES [Common].[User](Id));";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintNamedDefaultValue()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [Number] UNIQUEIDENTIFIER CONSTRAINT [DF_Address_Number] DEFAULT (newid()) NOT NULL);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ColumnConstraintPrimaryKey()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int] PRIMARY KEY);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ComputedColumn()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [IsDeleted]  AS 5);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ComputedColumnPersisted()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [IsDeleted]  AS (5) PERSISTED);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ComputedColumnPersistedNonNull()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
    [IsDeleted]  AS (5) PERSISTED NOT NULL);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateTable()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int]);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void IdentityColumn()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int] IDENTITY);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void IdentityColumnWithSeedAndIncrement()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int] IDENTITY(1,1));";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void NotNullColumn()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int] not null);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void NullColumn()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int] null);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void TableConstraintPrimaryKey()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
     CONSTRAINT [PK_Address] PRIMARY KEY([Id]));";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void TableConstraintPrimaryKeyClusteredAscending()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [int],
     CONSTRAINT [PK_Address] PRIMARY KEY CLUSTERED
(
    [Id] ASC
));";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);
            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}