﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Queries.Model;
using Qdarc.Tests.Common;

namespace Qdarc.Parsers.Tsql.Tests.AbstractSourceTree
{
    [TestClass]
    public class CreateSchemaTests
    {
        [TestMethod]
        public void CreateSchemaTest()
        {
            var query = @"CREATE SCHEMA [Common]
    AUTHORIZATION [dbo];
";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createSchema = script.Commands.Single().ShouldBeInstanceOf<CreateSchemaCommand>();

            createSchema.Name.ShouldBeEqual("Common");
        }

        private static object CreateAbstractSourceTree(string query)
        {
            var grammar = new TsqlGrammar();
            var language = new LanguageData(grammar);
            var parser = new Irony.Parsing.Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive(parser.Language.Errors.Aggregate(String.Empty, (x, y) => x + Environment.NewLine + y));
            }

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));

            return parseTree.Root.AstNode;
        }
    }
}
