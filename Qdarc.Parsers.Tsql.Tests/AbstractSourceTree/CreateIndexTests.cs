﻿using System;
using System.Linq;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Queries.Model;
using Qdarc.Tests.Common;

namespace Qdarc.Parsers.Tsql.Tests.AbstractSourceTree
{
    [TestClass]
    public class CreateIndexTests
    {
        [TestMethod]
        public void UniqueNonClusteredIndexOnManyColumnsWithRestriction()
        {
            var query = @"CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[TopicKind]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);
";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createIndex = script.Commands.Single().ShouldBeInstanceOf<CreateIndexCommand>();
            createIndex.Name.ShouldBeEqual("UQ_DisplayName");
            createIndex.IsUnique.ShouldBeTrue();
            createIndex.Kind.ShouldBeEqual(IndexKind.NonClustered);
            var tableReference = createIndex.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Schema.ShouldBeEqual("Wtt");
            tableReference.TableName.Name.ShouldBeEqual("TopicKind");
            var firstColumnReference = createIndex.Columns.First();
            firstColumnReference.ShouldNotBeNull();
            var firstColumn = firstColumnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumn.ColumnName.Name.ShouldBeEqual("ScopeId");
            var secondColumnReference = createIndex.Columns.Last();
            var secondColumn = secondColumnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumn.ColumnName.Name.ShouldBeEqual("DisplayName");
            createIndex.Where.ShouldNotBeNull();
        }

        [TestMethod]
        public void UniqueNonClusteredIndexOnManyColumns()
        {
            var query = @"CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[TopicKind]([ScopeId], [DisplayName])
";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createIndex = script.Commands.Single().ShouldBeInstanceOf<CreateIndexCommand>();
            createIndex.Name.ShouldBeEqual("UQ_DisplayName");
            createIndex.IsUnique.ShouldBeTrue();
            createIndex.Kind.ShouldBeEqual(IndexKind.NonClustered);
            var tableReference = createIndex.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Schema.ShouldBeEqual("Wtt");
            tableReference.TableName.Name.ShouldBeEqual("TopicKind");
            var firstColumnReference = createIndex.Columns.First();
            firstColumnReference.ShouldNotBeNull();
            var firstColumn = firstColumnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumn.ColumnName.Name.ShouldBeEqual("ScopeId");
            var secondColumnReference = createIndex.Columns.Last();
            var secondColumn = secondColumnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumn.ColumnName.Name.ShouldBeEqual("DisplayName");
            createIndex.Where.ShouldBeNull();
        }

        [TestMethod]
        public void UniqueNonClusteredIndex()
        {
            var query = @"CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[TopicKind]([ScopeId])
";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createIndex = script.Commands.Single().ShouldBeInstanceOf<CreateIndexCommand>();
            createIndex.Name.ShouldBeEqual("UQ_DisplayName");
            createIndex.IsUnique.ShouldBeTrue();
            createIndex.Kind.ShouldBeEqual(IndexKind.NonClustered);
            var tableReference = createIndex.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Schema.ShouldBeEqual("Wtt");
            tableReference.TableName.Name.ShouldBeEqual("TopicKind");
            var columnReference = createIndex.Columns.SingleOrDefault();
            columnReference.ShouldNotBeNull();
            var column = columnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("ScopeId");
            createIndex.Where.ShouldBeNull();
        }

        [TestMethod]
        public void NonClusteredIndex()
        {
            var query = @"CREATE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[TopicKind]([ScopeId])
";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createIndex = script.Commands.Single().ShouldBeInstanceOf<CreateIndexCommand>();
            createIndex.Name.ShouldBeEqual("UQ_DisplayName");
            createIndex.IsUnique.ShouldBeFalse();
            createIndex.Kind.ShouldBeEqual(IndexKind.NonClustered);
            var tableReference = createIndex.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Schema.ShouldBeEqual("Wtt");
            tableReference.TableName.Name.ShouldBeEqual("TopicKind");
            var columnReference = createIndex.Columns.SingleOrDefault();
            columnReference.ShouldNotBeNull();
            var column = columnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("ScopeId");
            createIndex.Where.ShouldBeNull();
        }

        [TestMethod]
        public void ClusteredIndex()
        {
            var query = @"CREATE CLUSTERED INDEX [UQ_DisplayName]
    ON [Wtt].[TopicKind]([ScopeId])
";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createIndex = script.Commands.Single().ShouldBeInstanceOf<CreateIndexCommand>();
            createIndex.Name.ShouldBeEqual("UQ_DisplayName");
            createIndex.IsUnique.ShouldBeFalse();
            createIndex.Kind.ShouldBeEqual(IndexKind.Clustered);
            var tableReference = createIndex.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Schema.ShouldBeEqual("Wtt");
            tableReference.TableName.Name.ShouldBeEqual("TopicKind");
            var columnReference = createIndex.Columns.SingleOrDefault();
            columnReference.ShouldNotBeNull();
            var column = columnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("ScopeId");
            createIndex.Where.ShouldBeNull();
        }

        [TestMethod]
        public void Index()
        {
            var query = @"CREATE INDEX [UQ_DisplayName]
    ON [Wtt].[TopicKind]([ScopeId])
";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createIndex = script.Commands.Single().ShouldBeInstanceOf<CreateIndexCommand>();
            createIndex.Name.ShouldBeEqual("UQ_DisplayName");
            createIndex.IsUnique.ShouldBeFalse();
            createIndex.Kind.ShouldBeEqual(IndexKind.Default);
            var tableReference = createIndex.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Schema.ShouldBeEqual("Wtt");
            tableReference.TableName.Name.ShouldBeEqual("TopicKind");
            var columnReference = createIndex.Columns.SingleOrDefault();
            columnReference.ShouldNotBeNull();
            var column = columnReference.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("ScopeId");
            createIndex.Where.ShouldBeNull();
        }

        private static object CreateAbstractSourceTree(string query)
        {
            var grammar = new TsqlGrammar();
            var language = new LanguageData(grammar);
            var parser = new Irony.Parsing.Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive(parser.Language.Errors.Aggregate(String.Empty, (x, y) => x + Environment.NewLine + y));
            }

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));

            return parseTree.Root.AstNode;
        }
    }
}