using System;
using System.IO;
using System.Linq;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Queries.Model;
using Qdarc.Tests.Common;

namespace Qdarc.Parsers.Tsql.Tests.AbstractSourceTree
{
    [TestClass]
    public class AlterTableTests
    {
        [TestMethod]
        public void AddCheck()
        {
            var query = @"ALTER TABLE [Common].[Module]
    ADD CONSTRAINT [CK_Module_Required]
    CHECK ([Common].[IsRequiredModulesExists]([Required]) = 1)";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var alterTable = script.Commands.Single().ShouldBeInstanceOf<AlterTableCommand>();
            var tableReference = alterTable.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Schema.ShouldBeEqual("Common");
            tableReference.TableName.Name.ShouldBeEqual("Module");
            var check = alterTable.AddedComponents.Single().ShouldBeInstanceOf<CheckConstraint>();
            check.Name.ShouldBeEqual("CK_Module_Required");
        }

        [TestMethod]
        public void AddForeignKeyColumnListUpdateDeleteNoAction()
        {
            var query = @"ALTER TABLE [Common].[AddressCompany]
ADD CONSTRAINT [FK_AddressCompany_AddressId] FOREIGN KEY ([AddressId])
  REFERENCES [Common].[Address] ([Id])
  ON UPDATE NO ACTION
  ON DELETE CASCADE";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var alterTable = script.Commands.Single().ShouldBeInstanceOf<AlterTableCommand>();
            var foreignKey = alterTable.AddedComponents.Single().ShouldBeInstanceOf<ForeignKeyConstraint>();
            foreignKey.Name.ShouldBeEqual("FK_AddressCompany_AddressId");
            var column = foreignKey.TargetColumnReferences.Single().ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("AddressId");
            var tableReference = foreignKey.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Name.ShouldBeEqual("Address");
            tableReference.TableName.Schema.ShouldBeEqual("Common");
            var referencedColumn = foreignKey.ReferencedTableColumns.Single().ShouldBeInstanceOf<SqlColumnReference>();
            referencedColumn.ColumnName.Name.ShouldBeEqual("Id");
            foreignKey.Actions.ShouldNotBeNull();
            foreignKey.Actions.First().ActionKind.ShouldBeEqual(ForeignKeyActionKind.NoAction);
            foreignKey.Actions.First().TriggerKind.ShouldBeEqual(ForeignKeyTriggerKind.OnUpdate);
            foreignKey.Actions.Last().ActionKind.ShouldBeEqual(ForeignKeyActionKind.Cascade);
            foreignKey.Actions.Last().TriggerKind.ShouldBeEqual(ForeignKeyTriggerKind.OnDelete);
        }

        [TestMethod]
        public void AddForeignKeyWithSpecificReferencedColumnList()
        {
            var query = @"ALTER TABLE [Common].[AddressCompany]
ADD CONSTRAINT [FK_AddressCompany_AddressId] FOREIGN KEY ([AddressId])
  REFERENCES [Common].[Address] ([Id])";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var alterTable = script.Commands.Single().ShouldBeInstanceOf<AlterTableCommand>();
            var foreignKey = alterTable.AddedComponents.Single().ShouldBeInstanceOf<ForeignKeyConstraint>();
            foreignKey.Name.ShouldBeEqual("FK_AddressCompany_AddressId");
            var column = foreignKey.TargetColumnReferences.Single().ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("AddressId");
            var tableReference = foreignKey.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Name.ShouldBeEqual("Address");
            tableReference.TableName.Schema.ShouldBeEqual("Common");
            var referencedColumn = foreignKey.ReferencedTableColumns.Single().ShouldBeInstanceOf<SqlColumnReference>();
            referencedColumn.ColumnName.Name.ShouldBeEqual("Id");
            foreignKey.Actions.ShouldBeNull();
        }

        [TestMethod]
        public void AddForeignKey()
        {
            var query = @"ALTER TABLE [Common].[AddressCompany]
ADD CONSTRAINT [FK_AddressCompany_AddressId] FOREIGN KEY ([AddressId])
  REFERENCES [Common].[Address]";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var alterTable = script.Commands.Single().ShouldBeInstanceOf<AlterTableCommand>();
            var foreignKey = alterTable.AddedComponents.Single().ShouldBeInstanceOf<ForeignKeyConstraint>();
            foreignKey.Name.ShouldBeEqual("FK_AddressCompany_AddressId");
            var column = foreignKey.TargetColumnReferences.Single().ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("AddressId");
            var tableReference = foreignKey.Table.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Name.ShouldBeEqual("Address");
            tableReference.TableName.Schema.ShouldBeEqual("Common");
            foreignKey.Actions.ShouldBeNull();
        }

        [TestMethod]
        public void Test()
        {
            var directory = @"C:\Repos\bss\BSSystem\Common\Database\Schema";
            var files = Directory.GetFiles(directory, "*.sql", SearchOption.AllDirectories);

            foreach (var file in files)
            {
                var query = File.ReadAllText(file);
                CreateAbstractSourceTree(query);
            }
        }

        private static object CreateAbstractSourceTree(string query)
        {
            var grammar = new TsqlGrammar();
            var language = new LanguageData(grammar);
            var parser = new Irony.Parsing.Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive(parser.Language.Errors.Aggregate(String.Empty, (x, y) => x + Environment.NewLine + y));
            }

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));

            return parseTree.Root.AstNode;
        }
    }
}