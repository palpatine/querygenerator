﻿using System;
using System.Linq;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Tests.Common;

namespace Qdarc.Parsers.Tsql.Tests.AbstractSourceTree
{
    [TestClass]
    public class CreateTableTests
    {
        [TestMethod]
        public void BasicCreateTable()
        {
            var query = @"CREATE TABLE [Common].[Address]([Id] [int])";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.Name.Catalog.ShouldBeNull();
            createTable.Name.Schema.ShouldBeEqual("Common");
            createTable.Name.Name.ShouldBeEqual("Address");
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.Name.ShouldBeEqual("Id");
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.Int);
        }

        [TestMethod]
        public void TwoColumns()
        {
            var query = @"CREATE TABLE [Common].[Address]([Id] [int], [Name] [nvarchar](40))";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.Name.Catalog.ShouldBeNull();
            createTable.Name.Schema.ShouldBeEqual("Common");
            createTable.Name.Name.ShouldBeEqual("Address");

            var firstColumn = createTable.ColumnDeclarations.First().ShouldBeInstanceOf<ColumnDeclaration>();
            firstColumn.Name.ShouldBeEqual("Id");
            var firstColumnType = firstColumn.SqlType.ShouldBeInstanceOf<TsqlType>();
            firstColumnType.Name.ShouldBeEqual(TsqlNativeTypes.Int);

            var secondColumn = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ColumnDeclaration>();
            secondColumn.Name.ShouldBeEqual("Name");
            var secondColumnType = secondColumn.SqlType.ShouldBeInstanceOf<TsqlType>();
            secondColumnType.Name.ShouldBeEqual(TsqlNativeTypes.NVarChar);
            secondColumnType.Capacity.ShouldBeEqual(40);
        }

        [TestMethod]
        public void CapacitySpecifyingColumnType()
        {
            var query = @"CREATE TABLE [Common].[Address]([Id] [varchar](20))";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.VarChar);
            type.Capacity.ShouldBeEqual(20);
        }

        [TestMethod]
        public void CustomTypeAsColumnType()
        {
            var query = @"CREATE TABLE [Common].[Address]([Id] [Common].[customType])";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<CustomTypeReference>();
            type.Name.Schema.ShouldBeEqual("Common");
            type.Name.Name.ShouldBeEqual("customType");
        }

        [TestMethod]
        public void MaxCapacitySpecifyingColumnType()
        {
            var query = @"CREATE TABLE [Common].[Address]([Id] [varchar](max))";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.VarChar);
            type.Capacity.ShouldBeEqual(-1);
        }

        [TestMethod]
        public void ComputedColumn()
        {
            var query = @"CREATE TABLE [Common].[Address] (
    [Id] [varchar](max),
    [IsDeleted] AS 5);";

            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.SqlType.ShouldBeNull();
            column.Name.ShouldBeEqual("IsDeleted");
            var expression = column.Expression.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Value.ShouldBeEqual(5);
        }
        [TestMethod]
        public void ComputedColumnPersisted()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [varchar](max),
    [IsDeleted]  AS (5) PERSISTED);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.SqlType.ShouldBeNull();
            column.Persisted.ShouldNotBeNull();
            column.Persisted.NullableValue.ShouldBeEqual(PersistedNullableValue.Default);
            column.Name.ShouldBeEqual("IsDeleted");
            var expression = column.Expression.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Value.ShouldBeEqual(5);
        }

        [TestMethod]
        public void ComputedColumnNotNull()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] [varchar](max),
    [IsDeleted] AS (5) PERSISTED NOT NULL);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.SqlType.ShouldBeNull();
            column.Persisted.ShouldNotBeNull();
            column.Persisted.NullableValue.ShouldBeEqual(PersistedNullableValue.NotNull);
            column.Name.ShouldBeEqual("IsDeleted");
            var expression = column.Expression.ShouldBeInstanceOf<SqlConstantExpression>();
            expression.Value.ShouldBeEqual(5);
        }

        [TestMethod]
        public void Identity()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int IDENTITY);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.Int);
            column.Name.ShouldBeEqual("Id");
            column.Identity.ShouldNotBeNull();
            column.Identity.Seed.ShouldBeNull();
            column.Identity.Increment.ShouldBeNull();
        }

        [TestMethod]
        public void IdentityWithSeedAndIncrement()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int IDENTITY (3, 1));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.Int);
            column.Name.ShouldBeEqual("Id");
            column.Identity.ShouldNotBeNull();
            column.Identity.Seed.ShouldBeEqual(3);
            column.Identity.Increment.ShouldBeEqual(1);
        }

        [TestMethod]
        public void NullableColumn()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int null);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.Int);
            column.Name.ShouldBeEqual("Id");
            column.NullableValue.ShouldBeEqual(NullableValue.Null);
        }

        [TestMethod]
        public void NotNullableColumn()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int not null);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.Int);
            column.Name.ShouldBeEqual("Id");
            column.NullableValue.ShouldBeEqual(NullableValue.NotNull);
        }

        [TestMethod]
        public void IdentityAndNotNull()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int IDENTITY not null);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            var type = column.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.Int);
            column.Name.ShouldBeEqual("Id");
            column.Identity.ShouldNotBeNull();
            column.NullableValue.ShouldBeEqual(NullableValue.NotNull);
        }

        [TestMethod]
        public void PrimaryKeyColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int PRIMARY KEY);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void PrimaryKeyClusteredColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int PRIMARY KEY CLUSTERED);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            column.PrimaryKey.Kind.ShouldBeEqual(IndexKind.Clustered);

        }

        [TestMethod]
        public void PrimaryKeyWithColumnNameColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int PRIMARY KEY (Id));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void PrimaryKeyWithOtherColumnNameColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int PRIMARY KEY ([Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
            column.PrimaryKey.ColumnReferences.Last().Ascending.ShouldBeNull();
        }

        [TestMethod]
        public void PrimaryKeyMultipleColumnsColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] int PRIMARY KEY ([Key] asc, [Id] desc));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var firstColumnReference = column.PrimaryKey.ColumnReferences.First().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumnReference.ColumnName.Name.ShouldBeEqual("Key");
            column.PrimaryKey.ColumnReferences.First().Ascending.Value.ShouldBeTrue();
            var secondColumnReference = column.PrimaryKey.ColumnReferences.Last().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.PrimaryKey.ColumnReferences.Last().Ascending.Value.ShouldBeFalse();
        }

        [TestMethod]
        public void NamedPrimaryKeyColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int CONSTRAINT pk PRIMARY KEY);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.PrimaryKey.Name.ShouldBeEqual("pk");
        }

        [TestMethod]
        public void UniqueColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int UNIQUE);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            column.Index.IsUnique.ShouldBeTrue();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void UniqueClusteredColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int UNIQUE CLUSTERED);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            column.Index.IsUnique.ShouldBeTrue();
            column.Index.Kind.ShouldBeEqual(IndexKind.Clustered);

        }

        [TestMethod]
        public void UniqueWithColumnNameColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int UNIQUE (Id));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            column.Index.IsUnique.ShouldBeTrue();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void UniqueWithOtherColumnNameColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int UNIQUE ([Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            column.Index.IsUnique.ShouldBeTrue();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
        }

        [TestMethod]
        public void NamedUniqueColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Id] int CONSTRAINT pk UNIQUE);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Single().ShouldBeInstanceOf<ColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            column.Index.IsUnique.ShouldBeTrue();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.Index.Name.ShouldBeEqual("pk");
        }


        [TestMethod]
        public void UniqueMultipleColumnsColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] int Unique ([Key] asc, [Id] desc));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            column.Index.IsUnique.ShouldBeTrue();
            var firstColumnReference = column.Index.ColumnReferences.First().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumnReference.ColumnName.Name.ShouldBeEqual("Key");
            column.Index.ColumnReferences.First().Ascending.Value.ShouldBeTrue();
            var secondColumnReference = column.Index.ColumnReferences.Last().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.Index.ColumnReferences.Last().Ascending.Value.ShouldBeFalse();
        }

        [TestMethod]
        public void PrimaryKeyComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 PRIMARY KEY);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void PrimaryKeyClusteredComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 PRIMARY KEY CLUSTERED);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            column.PrimaryKey.Kind.ShouldBeEqual(IndexKind.Clustered);
        }

        [TestMethod]
        public void PrimaryKeyWithColumnNameComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 PRIMARY KEY (Id));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void PrimaryKeyWithOtherColumnNameComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 PRIMARY KEY ([Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
            column.PrimaryKey.ColumnReferences.Last().Ascending.ShouldBeNull();
        }

        [TestMethod]
        public void PrimaryKeyMultipleColumnsComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 PRIMARY KEY ([Key] asc, [Id] desc));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var firstColumnReference = column.PrimaryKey.ColumnReferences.First().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumnReference.ColumnName.Name.ShouldBeEqual("Key");
            column.PrimaryKey.ColumnReferences.First().Ascending.Value.ShouldBeTrue();
            var secondColumnReference = column.PrimaryKey.ColumnReferences.Last().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.PrimaryKey.ColumnReferences.Last().Ascending.Value.ShouldBeFalse();
        }

        [TestMethod]
        public void NamedPrimaryKeyComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 CONSTRAINT pk PRIMARY KEY);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.PrimaryKey.ShouldNotBeNull();
            var columnReference = column.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.PrimaryKey.Name.ShouldBeEqual("pk");
        }

        [TestMethod]
        public void UniqueComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 UNIQUE);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void UniqueClusteredComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 UNIQUE CLUSTERED);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            column.Index.Kind.ShouldBeEqual(IndexKind.Clustered);
        }

        [TestMethod]
        public void UniqueWithColumnNameComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 UNIQUE (Id));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
        }

        [TestMethod]
        public void UniqueWithOtherColumnNameComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 UNIQUE ([Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
        }

        [TestMethod]
        public void NamedUniqueComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 CONSTRAINT pk UNIQUE);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            column.Index.ShouldNotBeNull();
            var columnReference = column.Index.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.Index.Name.ShouldBeEqual("pk");
        }

        [TestMethod]
        public void TableConstraintPrimaryKeyClusteredWithSingleOrderedColumnInColumnList()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
     CONSTRAINT [PK] PRIMARY KEY CLUSTERED ([Key] ASC));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.PrimaryKey.ShouldNotBeNull();
            var columnReference = createTable.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
            createTable.PrimaryKey.ColumnReferences.Single().Ascending.ShouldBeEqual(true);
            createTable.PrimaryKey.Kind.ShouldBeEqual(IndexKind.Clustered);
            createTable.PrimaryKey.Name.ShouldBeEqual("PK");
        }

        [TestMethod]
        public void TableConstraintPrimaryKeyClusteredWithSingleColumnInColumnList()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
     CONSTRAINT [PK] PRIMARY KEY CLUSTERED ([Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.PrimaryKey.ShouldNotBeNull();
            var columnReference = createTable.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
            createTable.PrimaryKey.ColumnReferences.Single().Ascending.ShouldBeNull();
            createTable.PrimaryKey.Kind.ShouldBeEqual(IndexKind.Clustered);
            createTable.PrimaryKey.Name.ShouldBeEqual("PK");
        }

        [TestMethod]
        public void TableConstraintPrimaryKeyNonClusteredWithSingleColumnInColumnList()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
     CONSTRAINT [PK] PRIMARY KEY NONCLUSTERED ([Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.PrimaryKey.ShouldNotBeNull();
            var columnReference = createTable.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
            createTable.PrimaryKey.ColumnReferences.Single().Ascending.ShouldBeNull();
            createTable.PrimaryKey.Kind.ShouldBeEqual(IndexKind.NonClustered);
            createTable.PrimaryKey.Name.ShouldBeEqual("PK");
        }

        [TestMethod]
        public void TableConstraintPrimaryKeyWithSingleColumnInColumnList()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
     CONSTRAINT [PK] PRIMARY KEY ([Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.PrimaryKey.ShouldNotBeNull();
            var columnReference = createTable.PrimaryKey.ColumnReferences.Single().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Key");
            createTable.PrimaryKey.ColumnReferences.Single().Ascending.ShouldBeNull();
            createTable.PrimaryKey.Kind.ShouldBeEqual(IndexKind.Default);
            createTable.PrimaryKey.Name.ShouldBeEqual("PK");
        }

        [TestMethod]
        public void TableConstraintPrimaryKeyWithMultipleColumnInColumnList()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] int,
     CONSTRAINT [PK] PRIMARY KEY ([Id],[Key]));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.PrimaryKey.ShouldNotBeNull();
            var firstColumnReference = createTable.PrimaryKey.ColumnReferences.First().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumnReference.ColumnName.Name.ShouldBeEqual("Id");
            createTable.PrimaryKey.ColumnReferences.First().Ascending.ShouldBeNull();
            var secondColumnReference = createTable.PrimaryKey.ColumnReferences.Last().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumnReference.ColumnName.Name.ShouldBeEqual("Key");
            createTable.PrimaryKey.ColumnReferences.Last().Ascending.ShouldBeNull();
            createTable.PrimaryKey.Kind.ShouldBeEqual(IndexKind.Default);
            createTable.PrimaryKey.Name.ShouldBeEqual("PK");
        }

        [TestMethod]
        public void TableConstraintPrimaryKeyWithMultipleColumnAndOrderInColumnList()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] int,
     CONSTRAINT [PK] PRIMARY KEY ([Id] asc,[Key] desc));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            createTable.PrimaryKey.ShouldNotBeNull();
            var firstColumnReference = createTable.PrimaryKey.ColumnReferences.First().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumnReference.ColumnName.Name.ShouldBeEqual("Id");
            createTable.PrimaryKey.ColumnReferences.First().Ascending.ShouldBeEqual(true);
            var secondColumnReference = createTable.PrimaryKey.ColumnReferences.Last().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumnReference.ColumnName.Name.ShouldBeEqual("Key");
            createTable.PrimaryKey.ColumnReferences.Last().Ascending.ShouldBeEqual(false);
            createTable.PrimaryKey.Kind.ShouldBeEqual(IndexKind.Default);
            createTable.PrimaryKey.Name.ShouldBeEqual("PK");
        }

        [TestMethod]
        public void UniqueMultipleColumnsComputedColumnConstraint()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Key] int,
    [Id] AS 5 Unique ([Key] asc, [Id] desc));";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var column = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();

            column.Index.ShouldNotBeNull();
            var firstColumnReference = column.Index.ColumnReferences.First().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            firstColumnReference.ColumnName.Name.ShouldBeEqual("Key");
            column.Index.ColumnReferences.First().Ascending.Value.ShouldBeTrue();
            var secondColumnReference = column.Index.ColumnReferences.Last().ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            secondColumnReference.ColumnName.Name.ShouldBeEqual("Id");
            column.Index.ColumnReferences.Last().Ascending.Value.ShouldBeFalse();
        }

        [TestMethod]
        public void DefaultValue()
        {
            var query = @"CREATE TABLE [Common].[Address](
    [Number] UNIQUEIDENTIFIER CONSTRAINT [DF_Address_Number] DEFAULT (newid()) NOT NULL);";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var constraint = createTable.ColumnDeclarations.Single().DefaultValue;
            constraint.ShouldNotBeNull();
            constraint.Name.ShouldBeEqual("DF_Address_Number");
            var columnReference = constraint.ColumnReference.ShouldBeInstanceOf<SqlColumnReference>();
            columnReference.ColumnName.Name.ShouldBeEqual("Number");
            var call = constraint.Value.ShouldBeInstanceOf<SqlCallExpression>();
            call.Name[0].ShouldBeEqual("newid");
            call.Arguments.ShouldBeNull();
        }

        [TestMethod]
        public void ConvertExpression()
        {
            var query = @"CREATE TABLE [Common].[User] (   
    [Id] int,
    [IsAlias] AS CONVERT([bit], 1) PERSISTED NOT NULL
);";

            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var computedColumn = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            var convert = computedColumn.Expression.ShouldBeInstanceOf<SqlConvertExpression>();
            var type = convert.SqlType.ShouldBeInstanceOf<TsqlType>();
            type.Name.ShouldBeEqual(TsqlNativeTypes.Bit);
            convert.Expression.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void IsNullValueExpression()
        {
            var query = @"CREATE TABLE [Common].[User] (   
    [Id] int,
    [IsAlias] AS ISNULL([Id],0) PERSISTED NOT NULL
);";

            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var computedColumn = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            computedColumn.Expression.ShouldNotBeNull();
        }

        [TestMethod]
        public void CaseWhen()
        {
            var query = @"CREATE TABLE [Common].[User] (   
    [Id] int,
    [IsAlias] AS case when [ParentId] IS NOT NULL then (1) else (0) end PERSISTED NOT NULL
);";

            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var computedColumn = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            var expression = computedColumn.Expression.ShouldBeInstanceOf<SqlCaseExpression>();
            var caseWhen = expression.Cases.Single();
            caseWhen.When.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
            caseWhen.Then.ShouldBeInstanceOf<SqlConstantExpression>();
        }

        [TestMethod]
        public void IsNotNullExpression()
        {
            var query = @"CREATE TABLE [Common].[User] (   
    [Id] int,
    [IsAlias] AS case when [ParentId] IS NOT NULL then (1) else (0) end PERSISTED NOT NULL
);";

            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var createTable = script.Commands.Single().ShouldBeInstanceOf<CreateTableCommand>();
            var computedColumn = createTable.ColumnDeclarations.Last().ShouldBeInstanceOf<ComputedColumnDeclaration>();
            var expression = computedColumn.Expression.ShouldBeInstanceOf<SqlCaseExpression>();
            var isNullExpression = expression.Cases.Single().When.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
            var columnValueExpression = isNullExpression.Expression.ShouldBeInstanceOf<SqlColumnReference>();
            columnValueExpression.ColumnName.Name.ShouldBeEqual("ParentId");
            isNullExpression.IsNegated.ShouldBeTrue();
        }

        [TestMethod]
        public void CreateMenuItemTable()
        {
            var query = @"CREATE TABLE [Common].[MenuItem] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [Key]		     NVARCHAR (50) NOT NULL,
    [MenuSectionId]	 INT           NOT NULL,
    [Order]          FLOAT         NOT NULL,
    [IconName]       NVARCHAR(MAX) NULL,
    [PermissionPath] VARCHAR (900) NULL
    CONSTRAINT [PK_MenuItem] PRIMARY KEY ([Id])
);;";
            var node = CreateAbstractSourceTree(query);
            node.ShouldNotBeNull();
        }

        private static object CreateAbstractSourceTree(string query)
        {
            var grammar = new TsqlGrammar();
            var language = new LanguageData(grammar);
            var parser = new Irony.Parsing.Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive(parser.Language.Errors.Aggregate(String.Empty, (x, y) => x + Environment.NewLine + y));
            }

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));

            return parseTree.Root.AstNode;
        }
    }
}
