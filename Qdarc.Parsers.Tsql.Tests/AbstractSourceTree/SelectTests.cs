﻿using System;
using System.Linq;
using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Tsql;
using Qdarc.Tests.Common;

namespace Qdarc.Parsers.Tsql.Tests.AbstractSourceTree
{
    [TestClass]
    public class SelectTests
    {
        [TestMethod]
        public void AliasByAssignment()
        {
            var query =
                @"SELECT Item = 4";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var aliastedItem = select.SelectionList.Single().ShouldBeInstanceOf<AliasedSelectionItem>();
            aliastedItem.Alias.ShouldBeEqual("Item");
            var value = aliastedItem.Item.ShouldBeInstanceOf<SqlConstantExpression>();
            value.Value.ShouldBeEqual(4);
        }

        [TestMethod]
        public void AsWithColumnList()
        {
            var query =
                @"SELECT * FROM (SELECT @TableName, @ColumnName, @ColumnValue) AS [source] (TableName, ColumnName, ColumnValue)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var source = select.Source.ShouldBeInstanceOf<AliasedSourceItem>();
            source.Alias.ShouldBeEqual("source");
            source.Item.ShouldBeInstanceOf<SelectCommand>();
            source.ColumnAliases.AllCorrespondingElementsShouldBeEqual(new[] { "TableName", "ColumnName", "ColumnValue" });
        }

        [TestMethod]
        public void Between()
        {
            var query = @"SELECT * FROM User Where Id BETWEEN 2 AND 4";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var between = select.Filter.ShouldBeInstanceOf<BetweenLogicalExpression>();
            var column = between.Value.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("Id");
            var lowerBound = between.LowerBound.ShouldBeInstanceOf<SqlConstantExpression>();
            lowerBound.Value.ShouldBeEqual(2);
            var upperBound = between.UpperBound.ShouldBeInstanceOf<SqlConstantExpression>();
            upperBound.Value.ShouldBeEqual(4);
        }

        [TestMethod]
        public void SelectFunctionCallWithVariablesAsParameters()
        {
            var query = @"SELECT DATEFROMPARTS(@Year, @Month, @@Day)";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var call = select.SelectionList.Single().ShouldBeInstanceOf<SqlCallExpression>();
            call.Arguments.Count().ShouldBeEqual(3);
            var firstArgument = call.Arguments.ElementAt(0).ShouldBeInstanceOf<SqlVariable>();
            firstArgument.Name.ShouldBeEqual("@Year");
            firstArgument.IsSystem.ShouldBeFalse();
            var lastArgument = call.Arguments.ElementAt(2).ShouldBeInstanceOf<SqlVariable>();
            lastArgument.Name.ShouldBeEqual("@@Day");
            lastArgument.IsSystem.ShouldBeTrue();
        }

        [TestMethod]
        public void BracedIdentifier()
        {
            var query = @"SELECT * FROM [User Entity]";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var source = select.Source.ShouldBeInstanceOf<SqlTableReference>();
            source.TableName.Name.ShouldBeEqual("User Entity");
        }

        [TestMethod]
        public void BracedLogicalExpression()
        {
            var query = @"SELECT * FROM [User Entity] spc WHERE ([spc].[Id] IS NULL)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            select.Filter.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
        }

        [TestMethod]
        public void CaseWhen()
        {
            var query =
                @"SELECT case when [TimeEnd] IS NULL then (0) else (1) end";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var caseWhen = select.SelectionList.Single().ShouldBeInstanceOf<SqlCaseExpression>();
            caseWhen.Cases.Single().When.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
            caseWhen.Cases.Single().Then.ShouldBeInstanceOf<SqlConstantExpression>();
            caseWhen.Else.ShouldBeInstanceOf<SqlConstantExpression>();

        }

        [TestMethod]
        public void Cast()
        {
            var query = @"SELECT CAST(NULL AS INT) AS [Id]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var aliasedSelectionItem = select.SelectionList.Single().ShouldBeInstanceOf<AliasedSelectionItem>();
            aliasedSelectionItem.Alias.ShouldBeEqual("Id");
            var cast = aliasedSelectionItem.Item.ShouldBeInstanceOf<SqlCastExpression>();
            cast.Expression.ShouldBeInstanceOf<SqlNullExpression>();
        }
        [TestMethod]
        public void Subtraction()
        {
            var query = @"SELECT 0 - @ModuleId";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var binaryExpression = select.SelectionList.Single().ShouldBeInstanceOf<SubtractExpression>();
            binaryExpression.Left.ShouldBeInstanceOf<SqlConstantExpression>();
            binaryExpression.Right.ShouldBeInstanceOf<SqlVariable>();
        }

        [TestMethod]
        public void Coalesce()
        {
            var query = @"SELECT COALESCE([pParent].[Id], 0 - [amc].[ModuleId], [pParent].[Id], 5)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var coalesce = select.SelectionList.Single().ShouldBeInstanceOf<SqlCoalesceExpression>();
            coalesce.Expressions.Count().ShouldBeEqual(4);
        }

        [TestMethod]
        public void ComplexIdentifier()
        {
            var query = "SELECT * FROM A.[C ommon].UserEntity";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var tableReference = select.Source.ShouldBeInstanceOf<SqlTableReference>();
            tableReference.TableName.Catalog.ShouldBeEqual("A");
            tableReference.TableName.Schema.ShouldBeEqual("C ommon");
            tableReference.TableName.Name.ShouldBeEqual("UserEntity");


        }

        [TestMethod]
        public void Convert()
        {
            var query =
                @"SELECT CONVERT([bit],1)";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            select.SelectionList.Single().ShouldBeInstanceOf<SqlConvertExpression>();
        }

        [TestMethod]
        public void DoubleWith()
        {
            var query = @"WITH
    TargetCTE AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ),TargetCTE2 AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ) Select * FROM TargetCTE UNION Select * FROM TargetCTE2";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void In()
        {
            var query = @"SELECT * FROM [User] Where Id in (2,4)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var inExpression = select.Filter.ShouldBeInstanceOf<SqlInLogicalExpression>();
            var column = inExpression.Value.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("Id");
            inExpression.Set.Count().ShouldBeEqual(2);
            inExpression.Set.First().ShouldBeInstanceOf<SqlConstantExpression>();
            inExpression.Set.Last().ShouldBeInstanceOf<SqlConstantExpression>();
            inExpression.Selection.ShouldBeNull();
            inExpression.IsNegated.ShouldBeFalse();

        }

        [TestMethod]
        public void InlineIf()
        {
            var query = @"SELECT IIF([pParent].[Id] IS NULL, 0 - [amc].[ModuleId], [pParent].[Id])";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var inlineIf = select.SelectionList.Single().ShouldBeInstanceOf<SqlInlineIfExpression>();
            inlineIf.Condition.ShouldBeInstanceOf<SqlIsNullLogicalExpression>();
            inlineIf.IfTrue.ShouldBeInstanceOf<SubtractExpression>();
            inlineIf.IfFalse.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void InSelect()
        {
            var query = @"SELECT * FROM User Where Id in (SELECT Id FROM User)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var inExpression = select.Filter.ShouldBeInstanceOf<SqlInLogicalExpression>();
            var column = inExpression.Value.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("Id");
            inExpression.Selection.ShouldBeInstanceOf<SelectCommand>();
            inExpression.Set.ShouldBeNull();
            inExpression.IsNegated.ShouldBeFalse();
        }

        [TestMethod]
        public void NotIn()
        {
            var query = @"SELECT * FROM User Where Id not in (2,4)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var inExpression = select.Filter.ShouldBeInstanceOf<SqlInLogicalExpression>();
            var column = inExpression.Value.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("Id");
            inExpression.Set.Count().ShouldBeEqual(2);
            inExpression.Set.First().ShouldBeInstanceOf<SqlConstantExpression>();
            inExpression.Set.Last().ShouldBeInstanceOf<SqlConstantExpression>();
            inExpression.Selection.ShouldBeNull();
            inExpression.IsNegated.ShouldBeTrue();
        }

        [TestMethod]
        public void NotInSelect()
        {
            var query = @"SELECT * FROM User Where Id not in (SELECT Id FROM User)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var inExpression = select.Filter.ShouldBeInstanceOf<SqlInLogicalExpression>();
            var column = inExpression.Value.ShouldBeInstanceOf<SqlColumnReference>();
            column.ColumnName.Name.ShouldBeEqual("Id");
            inExpression.Selection.ShouldBeInstanceOf<SelectCommand>();
            inExpression.Set.ShouldBeNull();
            inExpression.IsNegated.ShouldBeTrue();
        }

        [TestMethod]
        public void OrderBy()
        {
            var query = @"SELECT * FROM [User Entity] ORDER BY Name";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var order = select.Order.Single().ShouldBeInstanceOf<OrderedValueExpression>();
            order.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void PrefixedAsterisk()
        {
            var query = @"SELECT u.* FROM User u";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var asterisk = select.SelectionList.Single().ShouldBeInstanceOf<AsteriskSelection>();
            asterisk.Source.ShouldNotBeNull();
            asterisk.Source.Name[0].ShouldBeEqual("u");
        }

        [TestMethod]
        public void DoublePrefixedAsterisk()
        {
            var query = @"SELECT p.*, sp.*
        FROM [Common].[Permission] p
        FULL JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var firstAsterisk = select.SelectionList.First().ShouldBeInstanceOf<AsteriskSelection>();
            firstAsterisk.Source.ShouldNotBeNull();
            firstAsterisk.Source.Name[0].ShouldBeEqual("p");
            var secondAsterisk = select.SelectionList.Last().ShouldBeInstanceOf<AsteriskSelection>();
            secondAsterisk.Source.ShouldNotBeNull();
            secondAsterisk.Source.Name[0].ShouldBeEqual("sp");

        }

        [TestMethod]
        public void RowNumber()
        {
            var query = @"SELECT Row_Number() OVER (ORDER BY Name) FROM [User Entity]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var rowNumber = select.SelectionList.Single().ShouldBeInstanceOf<RowNumberSelection>();
            rowNumber.Order.ShouldNotBeNull();
            var orderBy = rowNumber.Order.Single();
            orderBy.Value.ShouldBeInstanceOf<SqlColumnReference>();
            rowNumber.Partition.ShouldBeNull();
        }

        [TestMethod]
        public void RowNumberPartition()
        {
            var query = @"SELECT Row_Number() OVER (PARTITION BY OrganizationId ORDER BY Name) FROM [User Entity]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var rowNumber = select.SelectionList.Single().ShouldBeInstanceOf<RowNumberSelection>();
            rowNumber.Order.ShouldNotBeNull();
            var orderBy = rowNumber.Order.Single();
            orderBy.Value.ShouldBeInstanceOf<SqlColumnReference>();
            rowNumber.Partition.ShouldNotBeNull();
            rowNumber.Partition.Single().ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void SelectAll()
        {
            var query = "SELECT * FROM UserEntity";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var asterisk = select.SelectionList.Single().ShouldBeInstanceOf<AsteriskSelection>();
            asterisk.Source.ShouldBeNull();
        }

        [TestMethod]
        public void SelectColumnWithAlias()
        {
            var query = "SELECT Name AS N, LastName ln FROM UserEntity User";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var firstAliastedItem = select.SelectionList.First().ShouldBeInstanceOf<AliasedSelectionItem>();
            firstAliastedItem.Alias.ShouldBeEqual("N");
            var secondAliastedItem = select.SelectionList.Last().ShouldBeInstanceOf<AliasedSelectionItem>();
            secondAliastedItem.Alias.ShouldBeEqual("ln");
        }

        [TestMethod]
        public void SelectCountAsterisk()
        {
            var query = @"SELECT COUNT(*) FROM [User]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var count = select.SelectionList.Single().ShouldBeInstanceOf<SqlAsteriskCountExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
        }

        [TestMethod]
        public void SelectFromInserted()
        {
            var query = "SELECT * FROM INSERTED";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var table = select.Source.ShouldBeInstanceOf<SpecialTableReference>();
            table.Kind.ShouldBeEqual(SpecialTable.Inserted);
        }

        [TestMethod]
        public void SelectCountExpression()
        {
            var query = @"select Count(e.Minutes + e.Hours * 60) FROM Wtt.[Entry] e";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var count = select.SelectionList.Single().ShouldBeInstanceOf<SqlCountExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
            count.IsDistinct.ShouldBeFalse();
            count.Value.ShouldBeInstanceOf<AddExpression>();
        }

        [TestMethod]
        public void SelectCountDistinct()
        {
            var query = @"select Count(DISTINCT e.Minutes) FROM Wtt.[Entry] e";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var count = select.SelectionList.Single().ShouldBeInstanceOf<SqlCountExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
            count.IsDistinct.ShouldBeTrue();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void SelectCountPartition()
        {
            var query = @"select Count(e.Minutes) over (partition by e.OrganizationId) FROM Wtt.[Entry] e";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var count = select.SelectionList.Single().ShouldBeInstanceOf<SqlCountExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldNotBeNull();
            count.Partition.Single().ShouldBeInstanceOf<SqlColumnReference>();
            count.IsDistinct.ShouldBeFalse();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void SelectCountOrder()
        {
            var query = @"select Count(e.Minutes) over (Order By e.UserId) FROM Wtt.[Entry] e";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var count = select.SelectionList.Single().ShouldBeInstanceOf<SqlCountExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldBeNull();
            count.IsDistinct.ShouldBeFalse();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }

        [TestMethod]
        public void SelectCountPartitionOrder()
        {
            var query = @"select Count(e.Minutes) over (partition by e.OrganizationId Order By e.UserId) FROM Wtt.[Entry] e";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var count = select.SelectionList.Single().ShouldBeInstanceOf<SqlCountExpression>();
            count.Order.ShouldBeNull();
            count.Partition.ShouldNotBeNull();
            count.Partition.Single().ShouldBeInstanceOf<SqlColumnReference>();
            count.IsDistinct.ShouldBeFalse();
            count.Value.ShouldBeInstanceOf<SqlColumnReference>();
        }


        [TestMethod]
        public void SelectDistinct()
        {
            var query = @"SELECT DISTINCT Name FROM [User Entity]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            select.IsDistinct.ShouldBeTrue();
        }

        [TestMethod]
        public void SelectFromCrossJoinByComma()
        {
            var query = @"SELECT *
        FROM [Common].[Permission],
        [Common].[SystemPermission]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<CrossJoinSelectionSource>();
            var left = join.Left.ShouldBeInstanceOf<SqlTableReference>();
            left.TableName.Schema.ShouldBeEqual("Common");
            left.TableName.Name.ShouldBeEqual("Permission");
            var right = join.Right.ShouldBeInstanceOf<SqlTableReference>();
            right.TableName.Schema.ShouldBeEqual("Common");
            right.TableName.Name.ShouldBeEqual("SystemPermission");
        }

        [TestMethod]
        public void SelectFromCrossJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        CROSS JOIN [Common].[SystemPermission]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<CrossJoinSelectionSource>();
            var left = join.Left.ShouldBeInstanceOf<SqlTableReference>();
            left.TableName.Schema.ShouldBeEqual("Common");
            left.TableName.Name.ShouldBeEqual("Permission");
            var right = join.Right.ShouldBeInstanceOf<SqlTableReference>();
            right.TableName.Schema.ShouldBeEqual("Common");
            right.TableName.Name.ShouldBeEqual("SystemPermission");
        }

        [TestMethod]
        public void SelectFromFullJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<FullJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }


        [TestMethod]
        public void SelectFromFullOuterJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<FullJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromLeftJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        Left JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<LeftJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromRightJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        Right JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<RightJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }


        [TestMethod]
        public void SelectFromRightOuterJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        RIGHT OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<RightJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromLeftOuterJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        Left OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<LeftJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<InnerJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromInnerJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        INNER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            var join = select.Source.ShouldBeInstanceOf<InnerJoinSelectionSource>();
            join.Left.ShouldBeInstanceOf<SqlTableReference>();
            join.Right.ShouldBeInstanceOf<SqlTableReference>();
            join.On.ShouldBeInstanceOf<EqualLogicalExpression>();
        }

        [TestMethod]
        public void SelectFromTableFunctionCall()
        {
            var query = @"SELECT *
                FROM [Common].[GetUserEffectivePermissions](@UserId)";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            select.Source.ShouldBeInstanceOf<SqlCallExpression>();
        }

        [TestMethod]
        public void Union()
        {
            var query = @"SELECT p.Id
        FROM [Common].[Permission] p
        JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]
        UNION
        SELECT p.Id
        FROM [Common].[Permission] p";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void UnionAll()
        {
            var query = @"SELECT p.Id
        FROM [Common].[Permission] p
        JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]
        UNION ALL
        SELECT p.Id
        FROM [Common].[Permission] p";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void SelectFunctionCall()
        {
            var query = @"SELECT [Common].[GetUserEffectivePermissions](@UserId)";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            select.SelectionList.Single().ShouldBeInstanceOf<SqlCallExpression>();
            select.Source.ShouldBeNull();
        }

        [TestMethod]
        public void SelectFunctionCallNoParameters()
        {
            var query = @"SELECT [Common].[GetUserEffectivePermissions]()";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
            select.SelectionList.Single().ShouldBeInstanceOf<SqlCallExpression>();
            select.Source.ShouldBeNull();
        }

        [TestMethod]
        public void SelectLiteralNumber()
        {
            var query = "SELECT 333, -32, 3e-3, 3.3";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();

            var first = select.SelectionList.First().ShouldBeInstanceOf<SqlConstantExpression>();
            first.Value.ShouldBeEqual(333);

            var second = select.SelectionList.ElementAt(1).ShouldBeInstanceOf<SqlConstantExpression>();
            second.Value.ShouldBeEqual(-32);

            var third = select.SelectionList.ElementAt(2).ShouldBeInstanceOf<SqlConstantExpression>();
            third.Value.ShouldBeEqual(0.003);

            var forth = select.SelectionList.ElementAt(3).ShouldBeInstanceOf<SqlConstantExpression>();
            forth.Value.ShouldBeEqual(3.3);
            select.Source.ShouldBeNull();
        }

        [TestMethod]
        public void SelectLiteralText()
        {
            var query = "SELECT 'aaa', N'swdes'";
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            var select = script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();

            var first = select.SelectionList.First().ShouldBeInstanceOf<SqlConstantExpression>();
            first.Value.ShouldBeEqual("aaa");

            var firstType = first.SqlType.ShouldBeInstanceOf<TsqlType>();
            firstType.Name.ShouldBeEqual(TsqlNativeTypes.VarChar);
            firstType.Capacity.ShouldBeEqual(3);

            var second = select.SelectionList.Last().ShouldBeInstanceOf<SqlConstantExpression>();
            second.Value.ShouldBeEqual("swdes");

            var secondType = second.SqlType.ShouldBeInstanceOf<TsqlType>();
            secondType.Name.ShouldBeEqual(TsqlNativeTypes.NVarChar);
            secondType.Capacity.ShouldBeEqual(5);
        }

        [TestMethod]
        public void SelectMultipleColumn()
        {
            var query = "SELECT Name, LastName FROM UserEntity User";
            CreateAbstractSourceTree(query);
            var node = CreateAbstractSourceTree(query);
            var script = node.ShouldBeInstanceOf<SqlScript>();
            script.Commands.Single().ShouldBeInstanceOf<SelectCommand>();
        }

        [TestMethod]
        public void SelectSingleColumn()
        {
            var query = "SELECT Name FROM UserEntity User";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void SelectTop()
        {
            var query = @"SELECT TOP 1 [Id] FROM CustomPermissions";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void SelectTopExpression()
        {
            var query = @"SELECT TOP (DATALENGTH(ISNULL(@List,1))) [Id] FROM CustomPermissions";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void SetScriptVariableUsingSelect()
        {
            var query = "SELECT @MessageCode = 'NoLockError', @MainTableName = 'User'";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void SubSelectionInFrom()
        {
            var query = @"SELECT * FROM (SELECT * FROM UserEntity AS User) AS User";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void SubSelectionInSelection()
        {
            var query = "SELECT (SELECT Id FROM UserEntity AS User) FROM UserEntity";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void TableAliasWithAsKeyword()
        {
            var query = "SELECT * FROM UserEntity AS User";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void TableAliasWithoutAsKeyword()
        {
            var query = "SELECT * FROM UserEntity User";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        [TestMethod]
        public void With()
        {
            var query = @"WITH
    TargetCTE AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ) Select * FROM TargetCTE";
            CreateAbstractSourceTree(query);
            Assert.Inconclusive();
        }

        private static object CreateAbstractSourceTree(string query)
        {
            var grammar = new TsqlGrammar();
            var language = new LanguageData(grammar);
            var parser = new Irony.Parsing.Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive(parser.Language.Errors.Aggregate(String.Empty, (x, y) => x + Environment.NewLine + y));
            }

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));

            return parseTree.Root.AstNode;
        }
    }
}