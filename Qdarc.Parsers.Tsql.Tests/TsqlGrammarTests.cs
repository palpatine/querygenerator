﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class TsqlGrammarTests
    {
        [TestMethod]
        public void CaseWhen()
        {
            var query = "If case when [TimeEnd] IS NULL then (0) else (1) end = 1 Throw 3, 'error', 4";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateIndex()
        {
            var query = @"CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[AddressCompany]([CompanyId], [DisplayName]);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CreateIndexWhere()
        {
            var query = @"CREATE UNIQUE NONCLUSTERED INDEX [UQ_DisplayName]
    ON [Common].[CustomFieldDefinition]([ScopeId], [DisplayName]) WHERE ([TimeEnd] IS NULL);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DeclareAndAssignScriptVariable()
        {
            var query = "DECLARE @ValidationResult INT = 7";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DeclareBinaryWithValue()
        {
            var query = "declare @a binary(4) = 0x3A33";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DeclareListOfScriptVariables()
        {
            var query = "DECLARE @ValidationResult INT, @MessageCode NVARCHAR(MAX)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DeclareScriptVariable()
        {
            var query = "DECLARE @ValidationResult INT";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DeclareScriptVariableWithComplexInitialization()
        {
            var query = "DECLARE @a INT = ((19 * (@Year % 19) + 24) % 30);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DeleteWhere()
        {
            var query = @"DELETE [Common].[Lock]
    WHERE
        [Lock].[OperationToken] = @OperationToken
    AND [Lock].[UserId] = @UserId
    AND [Lock].[SessionId] = @SessionId;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DropProcedure()
        {
            var query = @"DROP PROCEDURE [Common].[Update_Organization_User];";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Execute()
        {
            var query = @"EXEC [Common].[TryLock]
            @OperationToken,
            @UserId,
            @SessionId,
            @Name = '[Common].[User]',
            @RecordId = @AliasId";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Exists()
        {
            var query = "IF EXISTS (SELECT * FROM UserEntity) THROW 50000, 'ProcedureNullArgumentExecption', 4";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Go()
        {
            var query = "Go";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void If()
        {
            var query = @"IF @ChangerId IS NULL
    OR @OrganizationId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 4";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void IfColumnIsUpdated()
        {
            var query = @"IF(UPDATE([Number]))
    BEGIN
        RAISERROR('[Common].[Address]: Column [Number] cannot be changed.', 16, 1);
        ROLLBACK TRAN;
        RETURN;
    END;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void IfElse()
        {
            var query = @"IF @ChangerId IS NULL
    OR @OrganizationId IS NULL
        THROW 50000, 'ProcedureNullArgumentExecption', 4;
    ELSE
    BEGIN
        THROW 50000, 'ProcedureNullArgumentExecption', 4;
    END";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void IfParentheses()
        {
            var query = @"IF (@ChangerId IS NULL
    OR @OrganizationId IS NULL)
        THROW 50000, 'ProcedureNullArgumentExecption', 4";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InsertFromExecutionResult()
        {
            var query = @"INSERT @ValidationResult
    EXECUTE sp_executesql @Sql,
    N'@MessageCode NVARCHAR(MAX), @ColumnValue INT',
    @MessageCode,
    @ColumnValue";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InsertFromSelect()
        {
            var query = @"INSERT @ValidationResult SELECT Id FROM UserEntity";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InsertFromSelectInParentheses()
        {
            var query = @"INSERT @ValidationResult (Id) (SELECT Id FROM UserEntity)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InsertFromSelectWithColumnsList()
        {
            var query = @"INSERT @ValidationResult ([Id]) SELECT Id FROM UserEntity";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InsertInto()
        {
            var query = @"INSERT INTO @ValidationResult ([Id]) SELECT Id FROM UserEntity";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InsertValues()
        {
            var query = @"INSERT @Result
  VALUES
  (DATEFROMPARTS(@Year, 1, 1)),
  (DATEFROMPARTS(@Year, 1, 6))";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InsertValuesWithColumnsList()
        {
            var query = @"INSERT @Result ([Date])
  VALUES
  (DATEFROMPARTS(@Year, 1, 1)),
  (DATEFROMPARTS(@Year, 1, 6))";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ParseRaiseError()
        {
            var query = "RAISERROR('[Common].[Address]: Entries cannot be deleted.', 16, 1);";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ParserShouldBeAbleToParse()
        {
            var language = new LanguageData(new TsqlGrammar());
            var parser = new Parser(language);
            parser.Language.CanParse().ShouldBeTrue(string.Join(",", parser.Language.Errors));
            parser.Language.ErrorLevel.ShouldBeEqual(GrammarErrorLevel.NoError, string.Join(",", parser.Language.Errors));
        }

        [TestMethod]
        public void RollbackTransaction()
        {
            var query = "ROLLBACK TRAN";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SystemVariable()
        {
            var query = @"IF @@ROWCOUNT > 1
            THROW 50000, 'Multi alias detected', 1";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Throw()
        {
            var query = @"THROW 50000, 'ProcedureNullArgumentExecption', 4";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void UpdateWithComplexQuery()
        {
            var query = @"UPDATE [ua]
        SET
            [DateStart] = [u].[DateStart]
          , [DateEnd] = [u].[DateEnd]
          , [ChangerId] = @UserId
        FROM [Common].[User] AS [ua]
        JOIN [Common].[User] AS [u] ON [u].[Id] = [ua].[ParentId]
        WHERE [ua].[Id]=@AliasId
          AND ([ua].[DateStart] <> [u].[DateStart]
            OR ISNULL([ua].[DateEnd], @NotAllovedDateValue) <> ISNULL([u].[DateEnd], @NotAllovedDateValue))";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Use()
        {
            var query = "USE [Bss]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}