﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class SelectTests
    {
        [TestMethod]
        public void AliasByAssignment()
        {
            var query =
                @"SELECT Item = SUBSTRING(@List, s.N1, ISNULL(NULLIF(CHARINDEX(@Delimiter, @List, s.N1), 0) - s.N1, 8000))
    FROM cteStart s;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void AsWithColumnList()
        {
            var query =
                @"SELECT * FROM (SELECT @Name, @ColumnName, @ColumnValue) AS [source] (Name, ColumnName, ColumnValue)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Between()
        {
            var query = @"SELECT * FROM User Where Id BETWEEN 2 AND 4";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void BracedIdentifier()
        {
            var query = @"SELECT * FROM [User Entity]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void BracedLogicalExpression()
        {
            var query = @"SELECT * FROM [User Entity] spc WHERE ([spc].[Id] IS NULL)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void CaseWhen()
        {
            var query =
                @"SELECT case when [TimeEnd] IS NULL then (0) else (1) end";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Cast()
        {
            var query = @"SELECT CAST(NULL AS INT) AS [Id]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Coalesce()
        {
            var query = @"SELECT COALESCE([pParent].[Id], 0 - [amc].[ModuleId], [pParent].[Id], 5) AS [ParentId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void ComplexIdentifier()
        {
            var query = "SELECT * FROM A.[C ommon].UserEntity";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Convert()
        {
            var query =
                @"SELECT CONVERT([bit],1)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DoublePrefixedAsterisk()
        {
            var query = @"SELECT p.*, sp.*
        FROM [Common].[Permission] p
        FULL JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void DoubleWith()
        {
            var query = @"WITH
    TargetCTE AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ),TargetCTE2 AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ) Select * FROM TargetCTE UNION Select * FROM TargetCTE2";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void In()
        {
            var query = @"SELECT * FROM User Where Id in (2,4)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InlineIf()
        {
            var query = @"SELECT IIF([pParent].[Id] IS NULL, 0 - [amc].[ModuleId], [pParent].[Id]) AS [ParentId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void InSelect()
        {
            var query = @"SELECT * FROM User Where Id in (SELECT Id FROM User)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void NotIn()
        {
            var query = @"SELECT * FROM User Where Id not in (2,4)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void NotInSelect()
        {
            var query = @"SELECT * FROM User Where Id not in (SELECT Id FROM User)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void OrderBy()
        {
            var query = @"SELECT * FROM [User Entity] ORDER BY Name";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void PrefixedAsterisk()
        {
            var query = @"SELECT u.* FROM User u";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void RowNumber()
        {
            var query = @"SELECT Row_Number() OVER (ORDER BY Name) FROM [User Entity]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void RowNumberPartition()
        {
            var query = @"SELECT Row_Number() OVER (PARTITION BY OrganizationId ORDER BY Name) FROM [User Entity]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectAll()
        {
            var query = "SELECT * FROM UserEntity";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectColumnWithAlias()
        {
            var query = "SELECT Name AS N, LastName ln FROM UserEntity User";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectCountAsterisk()
        {
            var query = @"SELECT COUNT(*) FROM INSERTED";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectCountExpression()
        {
            var query = @"select Count(e.Minutes + e.Hours * 60) FROM Wtt.[Entry] e";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectDistinct()
        {
            var query = @"SELECT DISTINCT Name FROM [User Entity]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromFullJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromJoinOn()
        {
            var query = @"SELECT *
        FROM [Common].[Permission] p
        JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromLeftJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        LEFT JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromMany()
        {
            var query = @"SELECT *
        FROM [Common].[Permission],
         [Common].[SystemPermission]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromOuterJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        FULL OUTER JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromRightJoin()
        {
            var query = @"SELECT *
        FROM [Common].[Permission]
        RIGHT JOIN [Common].[SystemPermission] ON p.Id = [sp].[PermissionId]";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromTableFunctionCall()
        {
            var query = @"SELECT *
                FROM [Common].[GetUserEffectivePermissions](@UserId) AS guep";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromUnion()
        {
            var query = @"SELECT p.Id
        FROM [Common].[Permission] p
        JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]
        UNION
        SELECT p.Id
        FROM [Common].[Permission] p";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromUnionAll()
        {
            var query = @"SELECT p.Id
        FROM [Common].[Permission] p
        JOIN [Common].[SystemPermission] sp ON p.Id = [sp].[PermissionId]
        UNION ALL
        SELECT p.Id
        FROM [Common].[Permission] p";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFromWhere()
        {
            var query = @"SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFunctionCall()
        {
            var query = @"SELECT [Common].[GetUserEffectivePermissions](@UserId)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFunctionCallNoParameters()
        {
            var query = @"SELECT [Common].[GetUserEffectivePermissions]()";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectFunctionCallWithVariablesAsParameters()
        {
            var query = @"SELECT DATEFROMPARTS(@Year, @Month, @@Day)";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectLiteralNumber()
        {
            var query = "SELECT 333, -32, 3e-3, 3.3";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectLiteralText()
        {
            var query = "SELECT 'aaa', N'swdes'";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectMultipleColumn()
        {
            var query = "SELECT Name, LastName FROM UserEntity User";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectSingleColumn()
        {
            var query = "SELECT Name FROM UserEntity User";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectTop()
        {
            var query = @"SELECT TOP 1 [Id] FROM CustomPermissions";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SelectTopExpression()
        {
            var query = @"SELECT TOP (DATALENGTH(ISNULL(@List,1))) [Id] FROM CustomPermissions";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SetScriptVariableUsingSelect()
        {
            var query = "SELECT @MessageCode = 'NoLockError', @MainTableName = 'User'";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SubSelectionInFrom()
        {
            var query = @"SELECT * FROM (SELECT * FROM UserEntity AS User) AS User";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void SubSelectionInSelection()
        {
            var query = "SELECT (SELECT Id FROM UserEntity AS User) FROM UserEntity";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void TableAliasWithAsKeyword()
        {
            var query = "SELECT * FROM UserEntity AS User";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void TableAliasWithoutAsKeyword()
        {
            var query = "SELECT * FROM UserEntity User";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void With()
        {
            var query = @"WITH
    TargetCTE AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ) Select * FROM TargetCTE";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);
            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}