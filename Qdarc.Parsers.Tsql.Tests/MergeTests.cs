﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class MergeTests
    {
        [TestMethod]
        public void InsertValues()
        {
            var query = @"MERGE [Common].[JunctionLock] AS [target]
    USING (SELECT @Name, @ColumnName, @ColumnValue) AS [source] (Name, ColumnName, ColumnValue)
    ON [target].[Name] = [source].[Name]
    AND [target].[ColumnName] = [source].[ColumnName]
    AND [target].[ColumnValue] = [source].[ColumnValue]
    WHEN NOT MATCHED THEN
        INSERT ([OperationToken], [Name], [ColumnName], [ColumnValue], [SessionId], [UserId], [TimeStart])
        VALUES (@OperationToken, @Name, @ColumnName, @ColumnValue, @SessionId, @UserId, GETUTCDATE());";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Into()
        {
            var query = @"MERGE INTO common.[User] u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void MatchedWithAdditionalQuery()
        {
            var query = @"MERGE common.[User] u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED AND u.Name is not null THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void MergeWithoutSemicolonIsInvalid()
        {
            var query = @"MERGE common.[User] u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Error);
        }

        [TestMethod]
        public void TargetWithAsInAlias()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void TargetWithoutAlias()
        {
            var query = @"MERGE common.[User] USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void TargetWithoutAsInAlias()
        {
            var query = @"MERGE common.[User] u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void Top()
        {
            var query = @"MERGE TOP (4) common.[User] u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void TopPercent()
        {
            var query = @"MERGE TOP (4) PERCENT common.[User] u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenMatchedUpdate()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null, Name +='X';";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenNotMatchedAndInsertDefault()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN NOT MATCHED AND u.DateStart > getdate() THEN INSERT DEFAULT VALUES;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenNotMatchedBySourceAndUpdate()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN NOT MATCHED BY SOURCE AND u.DateStart > getdate() THEN UPDATE SET DisplayName = null, Name +='X';";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenNotMatchedBySourceDelete()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN NOT MATCHED BY SOURCE THEN DELETE;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenNotMatchedBySourceUpdate()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN NOT MATCHED BY SOURCE THEN UPDATE SET DisplayName = null, Name +='X';";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenNotMatchedByTargetAndInsertDefault()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN NOT MATCHED BY TARGET AND u.DateStart > getdate() THEN INSERT DEFAULT VALUES;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenNotMatchedByTargetInsertDefault()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN NOT MATCHED BY TARGET THEN INSERT DEFAULT VALUES;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void WhenNotMatchedInsertDefault()
        {
            var query = @"MERGE common.[User] AS u USING (Select * From Common.Organization) m ON u.Id = m.Id WHEN NOT MATCHED THEN INSERT DEFAULT VALUES;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        [TestMethod]
        public void With()
        {
            var query = @"WITH
    TargetCTE AS (
        SELECT
            [Id]
          , [ScopeId]
          , [SystemPermissionId]
          , [ChangerId]
          , [TimeEnd]
        FROM [Common].[Permission]
        WHERE [ScopeId] = @OrganizationId
          AND [CustomPermissionId] IS NULL
          AND [IsDeleted] = 0
    ) MERGE common.[User] u USING (Select * From TargetCTE) m ON u.Id = m.Id WHEN MATCHED THEN UPDATE SET DisplayName = null;";
            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}