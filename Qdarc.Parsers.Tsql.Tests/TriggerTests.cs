﻿using Irony.Parsing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Qdarc.Asserts;

namespace Qdarc.Parsers.Tsql.Tests
{
    [TestClass]
    public class TriggerTests
    {
        [TestMethod]
        public void CreateTrigger()
        {
            var query = @"CREATE TRIGGER [Common].[INST_TRD_Address] ON [Common].[Address]
    WITH EXECUTE AS CALLER
    INSTEAD OF DELETE
AS
BEGIN
    DECLARE @row_count INT = @@rowcount;
    IF(@row_count = 0)
    BEGIN
        RETURN
    END;
    RAISERROR('[Common].[Address]: Entries cannot be deleted.', 16, 1);
    ROLLBACK TRAN;
END";

            var parser = CreateParser();

            var parseTree = parser.Parse(query);

            parseTree.Status.ShouldBeEqual(ParseTreeStatus.Parsed, string.Join(",", parseTree.ParserMessages));
        }

        private static Parser CreateParser()
        {
            var grammar = new TsqlGrammar();
            grammar.LanguageFlags &= ~LanguageFlags.CreateAst;
            var language = new LanguageData(grammar);

            var parser = new Parser(language);
            if (!parser.Language.CanParse() || parser.Language.ErrorLevel != GrammarErrorLevel.NoError)
            {
                Assert.Inconclusive();
            }

            return parser;
        }
    }
}