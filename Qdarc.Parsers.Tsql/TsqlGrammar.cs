﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Irony;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Constraints;
using Qdarc.Queries.Model.Declarations;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Tsql;
using TypeCode = Irony.TypeCode;

namespace Qdarc.Parsers.Tsql
{
    public class TsqlGrammar : Grammar
    {
        private const string AggregateNonTerminalName = "Aggregate";
        private const string AliasedElementNonTerminalName = "AliasedElement";
        private const string AliasedSourceNonTerminalName = "AliasedSource";
        private const string AlterTableNonTerminalName = "AlterTable";
        private const string AssignmentNonTerminalName = "Assignment";
        private const string AssignOperatorNonTerminalName = "AssignOperator";
        private const string BetweenExpressionNonTerminalName = "BetweenExpression";
        private const string BinaryLiteralName = "Binary";
        private const string BlockCommandListNonTerminalName = "BlockCommandList";
        private const string BlockCommandNonTerminalName = "BlockCommand";
        private const string CallExpressionNonTerminalName = "CallExpression";
        private const string CaseWhenNonTerminalName = "CaseWhen";
        private const string CodeBlockNonTerminalName = "CodeBlock";
        private const string ColumnAssignmentListNonTerminalName = "ColumnAssignmentList";
        private const string ColumnCheckDeclarationNonTerminalName = "ColumnCheckDeclaration";
        private const string ColumnConstraintListNonTerminalName = "ColumnConstraintList";
        private const string ColumnConstraintNonTerminalName = "ColumnConstraint";
        private const string ColumnDeclarationNonTerminalName = "ColumnDeclaration";
        private const string ColumnDefaultValueNonTerminalName = "ColumnDefaultValue";
        private const string ColumnForeignKeyDeclartionNonTerminalName = "ColumnForeignKeyDeclartion";
        private const string ComputedColumnConstraintListNonTerminalName = "ComputedColumnConstraintList";
        private const string ComputedColumnConstraintNonTerminalName = "ComputedColumnConstraint";
        private const string ComputedColumnDeclarationNonTerminalName = "ComputedColumnDeclaration";
        private const string ComputedColumnForeignKeyActionsNonTerminalName = "ComputedColumnForeignKeyActions";
        private const string ComputedColumnForeignKeyDeclartionNonTerminalName = "ComputedColumnForeignKeyDeclartion";
        private const string CreateIndexNonTerminalName = "CreateIndex";
        private const string CreateSchemaNonTerminalName = "CreateSchema";
        private const string CreateTableNonTerminalName = "CreateTable";
        private const string DeleteNonTerminalName = "Delete";
        private const string ElementFullNameNonTerminalName = "ElementFullName";
        private const string ExecuteCommandNonTerminalName = "Execute";
        private const string ExecutionArgumentsListNonTerminalName = "ExecutionArgumentsList";
        private const string ExpressionNonTerminalName = "Expression";
        private const string ForeignKeyActionsNonTerminalName = "ForeignKeyActions";
        private const string FunctionNonTerminalName = "Function";
        private const string IdentifierListNonTerminalName = "IdentifierList";
        private const string IdentifierTerminalName = "Identifier";
        private const string IfNonTerminalName = "If";
        private const string InsertNonTerminlName = "Insert";
        private const string InsertValuesNonTerminalName = "InsertValues";
        private const string LogicalExpressionListNonTerminalName = "LogicalExpressionList";
        private const string LogicalExpressionNonTerminalName = "LogicalExpression";
        private const string MergeNonTerminalName = "Merge";
        private const string MergeOperationNonTerminalName = "MergeOperation";
        private const string MergeOperationsListNonTerminalName = "MergeOperationsList";
        private const string NumberLiteralName = "Number";
        private const string OrderByNonTerminalName = "OrderBy";
        private const string OrderedColumnNonTerminalName = "OrderedColumn";
        private const string OrderedColumnsListNonTerminalName = "OrderedColumnsList";
        private const string OrderedValueExpressionListNonTerminalName = "OrderedValueExpressionList";
        private const string ParameterDeclarationNonTerminalName = "ParameterDeclaration";
        private const string ParametersDeclarationListNonTerminalName = "ParametersDeclarationList";
        private const string ProcedureNonTerminalName = "Procedure";
        private const string QueryNonTerminalName = "Query";
        private const string RaiseErrorNonTerminalName = "RaiseError";
        private const string RowNumberNonTerminalName = "RowNumber";
        private const string ScriptConfigurationCommandNonTerminalName = "ScriptConfigurationCommand";
        private const string ScriptElementNonTerminalName = "ScriptElement";
        private const string ScriptNonTerminalName = "Script";
        private const string SelectionBodyNonTerminalName = "SelectionBody";
        private const string SelectionListNonTerminalName = "SelectionList";
        private const string SelectionSourceNonTerminalName = "SelectionSource";
        private const string SelectNonTerminalName = "Select";
        private const string TableComponentNonTerminalName = "TableComponent";
        private const string TableComponentsListNonTerminalName = "TableComponentsList";
        private const string TableConstraintNonTerminalName = "TableConstraint";
        private const string TableDeclarationNonTerminalName = "TableDeclaration";
        private const string TextLiteralName = "String";
        private const string ThrowNonTerminalName = "Throw";
        private const string TopNonTerminalName = "Top";
        private const string TransactionNonTerminalName = "Transaction";
        private const string TriggerNonTerminalName = "Trigger";
        private const string TriggerOperationList = "TriggerOperationList";
        private const string TypeNameNonTerminalName = "TypeName";
        private const string UniqueIndexDeclarationNonTerminalName = "UniqueIndexDeclaration";
        private const string UpdateNonTerminalName = "Update";
        private const string UserDefinedTypeNonTerminalName = "UserDefinedType";
        private const string ValueExpressionListNonTerminalName = "ValueExpressionList";
        private const string ValueExpressionNonTerminalName = "ValueExpression";
        private const string VariableDeclarationNonTerminalName = "VariableDeclaration";
        private const string VariablesAssignmentStatementNonTerminalName = "VariablesAssignmentStatement";
        private const string VariablesDeclarationListNonTerminalName = "VariablesDeclarationList";
        private const string VariablesDeclarationStatementNonTerminalName = "VariablesDeclarationStatement";
        private const string WhenSearchedListNonTerminalName = "WhenSearchedList";
        private const string WhenSearchedNonTerminalName = "WhenSearched";
        private const string WhenSimpleListNonTerminalName = "WhenSimpleList";
        private const string WhenSimpleNonTerminalName = "WhenSimple";
        private const string WithNonTerminalName = "With";
        private readonly NonTerminal _caseWhen;
        private readonly NonTerminal _codeBlock;
        private readonly NonTerminal _columnAssignmentList;
        private readonly NonTerminal _orderBy;
        private readonly NonTerminal _orderedValueExpressionList;
        private readonly NonTerminal _query;
        private readonly NonTerminal _select;
        private readonly NonTerminal _typeName;
        private KeyTerm _actionTerm;
        private KeyTerm _add;
        private KeyTerm _addAssign;
        private KeyTerm _addTerm;
        private KeyTerm _afterTerm;
        private NonTerminal _aliasedElement;
        private NonTerminal _aliasedSource;
        private KeyTerm _allTerm;
        private NonTerminal _alterTable;
        private KeyTerm _alterTerm;
        private KeyTerm _and;
        private KeyTerm _applyTerm;
        private KeyTerm _ascendingTerm;
        private KeyTerm _assign;
        private NonTerminal _assignment;
        private NonTerminal _assignOperator;
        private KeyTerm _asteriskTerm;
        private KeyTerm _asTerm;
        private KeyTerm _authorizationTerm;
        private KeyTerm _beginTerm;
        private NonTerminal _betweenExpression;
        private KeyTerm _betweenTerm;
        private BinaryLiteral _binary;
        private KeyTerm _bitwiseAnd;
        private KeyTerm _bitwiseAndAssign;
        private KeyTerm _bitwiseExclusiveOr;
        private KeyTerm _bitwiseExclusiveOrAssign;
        private KeyTerm _bitwiseNot;
        private KeyTerm _bitwiseOr;
        private KeyTerm _bitwiseOrAssign;
        private NonTerminal _blockCommand;
        private NonTerminal _blockCommandList;
        private KeyTerm _byTerm;
        private KeyTerm _callerTerm;
        private NonTerminal _callExpression;
        private KeyTerm _cascadeTerm;
        private KeyTerm _caseTerm;
        private KeyTerm _castTerm;
        private KeyTerm _checkTerm;
        private KeyTerm _clusteredTerm;
        private KeyTerm _coalesce;
        private NonTerminal _columnCheckDeclaration;
        private NonTerminal _columnConstraint;
        private NonTerminal _columnConstraintList;
        private NonTerminal _columnDeclaration;
        private NonTerminal _columnDefaultValueDeclaration;
        private NonTerminal _columnForeignKeyDeclaration;
        private KeyTerm _commitTerm;
        private NonTerminal _computedColumnConstraint;
        private NonTerminal _computedColumnConstraintList;
        private NonTerminal _computedColumnDeclaration;
        private NonTerminal _computedColumnForeignKeyActions;
        private NonTerminal _computedColumnForeignKeyDeclaration;
        private KeyTerm _constraintTerm;
        private KeyTerm _convert;
        private KeyTerm _countBigTerm;
        private KeyTerm _countTerm;
        private NonTerminal _createIndex;
        private NonTerminal _createSchema;
        private NonTerminal _createTable;

        ////  private KeyTerm _functionTerminal;
        private KeyTerm _createTerm;

        private KeyTerm _crossTerm;
        private KeyTerm _declareTerm;
        private KeyTerm _defaultTerm;
        private NonTerminal _delete;
        private KeyTerm _deleteTerm;
        private KeyTerm _descencingTerm;
        private KeyTerm _distinctTerm;
        private KeyTerm _divide;
        private KeyTerm _divideAssign;
        private KeyTerm _dropTerm;
        private NonTerminal _elementFullName;
        private KeyTerm _elseTerm;
        private KeyTerm _encriptionTerm;
        private KeyTerm _endTerm;
        private KeyTerm _equals;
        private NonTerminal _executeCommand;
        private KeyTerm _executeFullTerm;
        private KeyTerm _executeSqlTerm;
        private KeyTerm _executeTerm;
        private NonTerminal _executionArgumentsList;
        private KeyTerm _existsTerm;
        private NonTerminal _expression;
        private NonTerminal _foreignKeyActions;
        private KeyTerm _foreignTerm;
        private KeyTerm _forTerm;
        private KeyTerm _fromTerm;
        private KeyTerm _fullTerm;
        private NonTerminal _function;
        private KeyTerm _functionTerm;
        private KeyTerm _goTerm;
        private KeyTerm _greaterThan;
        private KeyTerm _greaterThanOrEqual;
        private KeyTerm _groupTerm;
        private KeyTerm _havingTerm;
        private IdentifierTerminal _identifier;
        private NonTerminal _identifierList;
        private KeyTerm _identityInsertTerm;
        private KeyTerm _identityTerm;
        private NonTerminal _if;
        private KeyTerm _ifTerm;
        private KeyTerm _indexTerm;
        private KeyTerm _inlineIfTerm;
        private KeyTerm _innerTerm;
        private NonTerminal _insert;
        private KeyTerm _insertTerm;
        private NonTerminal _insertValues;
        private KeyTerm _insteadTerm;
        private KeyTerm _inTerm;
        private KeyTerm _intoTerm;
        private KeyTerm _is;
        private KeyTerm _joinTerm;
        private KeyTerm _keyTerm;
        private KeyTerm _leftTerm;
        private KeyTerm _lessThan;
        private KeyTerm _lessThenOrEqual;
        private NonTerminal _logicalExpression;
        private NonTerminal _logicalExpressionList;
        private KeyTerm _matchedTerm;
        private KeyTerm _maxTerm;
        private NonTerminal _merge;
        private NonTerminal _mergeOperation;
        private NonTerminal _mergeOperationsList;
        private KeyTerm _mergeTerm;
        private KeyTerm _modulo;
        private KeyTerm _moduloAssign;
        private KeyTerm _multiply;
        private KeyTerm _multiplyAssign;
        private KeyTerm _nonclusteredTerm;
        private KeyTerm _not;
        private KeyTerm _notEqualTo;
        private KeyTerm _notEqualToIso;
        private KeyTerm _noTerm;
        private KeyTerm _notGreaterThen;
        private KeyTerm _notLessThen;
        private KeyTerm _null;
        private NumberLiteral _number;
        private KeyTerm _offTerm;
        private KeyTerm _ofTerm;
        private KeyTerm _onTerm;
        private KeyTerm _or;
        private NonTerminal _orderedColumn;
        private NonTerminal _orderedColumnsList;
        private KeyTerm _orderTerm;
        private KeyTerm _outerTerm;
        private KeyTerm _overTerm;
        private NonTerminal _parameterDeclaration;
        private NonTerminal _parametersDeclarationList;
        private KeyTerm _partitionTerm;
        private KeyTerm _percentTerm;
        private KeyTerm _persistedTerm;
        private KeyTerm _primaryTerm;
        private NonTerminal _procedure;
        private KeyTerm _procedureTerm;
        private NonTerminal _raiseError;
        private KeyTerm _raiseErrorTerm;
        private KeyTerm _readonlyTerm;
        private KeyTerm _referencesTerm;
        private KeyTerm _returnsTerm;
        private KeyTerm _returnTerm;
        private KeyTerm _rightTerm;
        private KeyTerm _rollbackTerm;
        private NonTerminal _rowNumber;
        private KeyTerm _rowNumberTerm;
        private KeyTerm _scheamTerm;
        private KeyTerm _schemabindingTerm;
        private NonTerminal _script;
        private NonTerminal _scriptConfigurationCommand;
        private NonTerminal _scriptElement;
        private NonTerminal _selectionBody;
        private NonTerminal _selectionList;
        private NonTerminal _selectionSource;
        private KeyTerm _selectTerm;
        private KeyTerm _selfTerm;
        private KeyTerm _setTerm;
        private KeyTerm _sourceTerm;
        private KeyTerm _substractAssign;
        private KeyTerm _subtract;
        private SystemVariableTerminal _systemVariableName;
        private NonTerminal _tableComponent;
        private NonTerminal _tableComponentsList;
        private NonTerminal _tableConstraint;
        private NonTerminal _tableDeclaration;
        private KeyTerm _tableTerm;
        private KeyTerm _targetTerm;
        private StringLiteral _text;
        private KeyTerm _thenTerm;
        private NonTerminal _throw;
        private KeyTerm _throwTerm;
        private KeyTerm _tiesTerm;
        private NonTerminal _top;
        private KeyTerm _topTerm;
        private NonTerminal _transaction;
        private KeyTerm _transactionFullTerm;
        private KeyTerm _transactionTerm;
        private NonTerminal _trigger;
        private NonTerminal _triggerOperationList;
        private KeyTerm _triggerTerm;
        private NonTerminal _type;
        private KeyTerm _typeTerm;
        private KeyTerm _unionTerm;
        private NonTerminal _uniqueIndexDeclaration;
        private KeyTerm _uniqueTerm;
        private NonTerminal _update;
        private KeyTerm _updateTerm;
        private KeyTerm _useTerm;
        private KeyTerm _usingTerm;
        private NonTerminal _valueExpression;
        private NonTerminal _valueExpressionList;
        private KeyTerm _valuesTerm;
        private NonTerminal _variableDeclaration;
        private VariableTerminal _variableName;
        private NonTerminal _variablesAssignmentStatement;
        private NonTerminal _variablesDeclarationList;
        private NonTerminal _variablesDeclarationStatement;
        private NonTerminal _whenList;
        private NonTerminal _whenSearched;
        private NonTerminal _whenSearchedList;
        private NonTerminal _whenSimple;
        private KeyTerm _whenTerm;
        private KeyTerm _whereTerm;
        private NonTerminal _with;
        private KeyTerm _withTerm;

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        public TsqlGrammar()
            : base(false)
        {
            LanguageFlags = LanguageFlags.CreateAst;

            CreateKeyTerminals();
            CreateComments();
            MarkPunctuation(",", "(", ")");
            MarkPunctuation(_asTerm, ToTerm(";"));
            CreateLiterals();
            CreateIdentifier();

            _query = new NonTerminal(QueryNonTerminalName);
            MarkTransient(_query);
            _codeBlock = new NonTerminal(CodeBlockNonTerminalName, CreateCodeBlockNode);
            _select = new NonTerminal(SelectNonTerminalName, CreateSelectNode);
            _orderBy = new NonTerminal(OrderByNonTerminalName, CreateOrderByNode);
            _typeName = new NonTerminal(TypeNameNonTerminalName, CreateTypeNameNode);
            _columnAssignmentList = new NonTerminal(ColumnAssignmentListNonTerminalName);
            _caseWhen = new NonTerminal(CaseWhenNonTerminalName, CreateCaseWhenNode);
            _orderedValueExpressionList = new NonTerminal(OrderedValueExpressionListNonTerminalName, CreateListNode<OrderedValueExpression>);

            ProcessArithmetic();
            ProcessCaseWhen();
            ProcessColumnsDeclaration();
            ProcessNamedElement();
            ProcessSelect();
            ProcessDelete();
            ProcessExecute();
            ProcessInsert();
            ProcessMerge();
            ProcessUpdate();
            ProcessThrow();
            ProcessIf();
            ProcessTransaction();
            _query.Rule = _select
                          | _insert
                          | _delete
                          | _update
                          | _merge
                          | _throw
                          | _raiseError
                          | _transaction
                          | _if;

            ProcessScriptConfiguration();
            ProcessTypeName();
            ProcessVariableManipulation();
            ProcessCodeBlock();

            ProcessTable();
            ProcessProcedure();
            ProcessFunction();
            ProcessTrigger();
            ProcessIndex();
            ProcessSchema();
            ProcessUserDefinedType();
            ProcessScript();

            Root = _script;
        }

        private static NumberLiteral CreateNumber(string name)
        {
            var term = new NumberLiteral(name);
            term.DefaultIntTypes = new[]
            {
                TypeCode.Int32,
                TypeCode.UInt32,
                TypeCode.Int64,
                TypeCode.UInt64
            };
            term.DefaultFloatType = TypeCode.Double;
            term.AddExponentSymbols("eE", TypeCode.Double);
            term.Flags = TermFlags.NoAstNode;
            return term;
        }

        private void CreateAliasByAssignmentNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new AliasedSelectionItem((ISelectionItem)parsenode.ChildNodes[2].AstNode)
            {
                Alias = parsenode.ChildNodes[0].Token.ValueString
            };
        }

        private void CreateAliasedElementNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 3)
            {
                parsenode.AstNode = new AliasedSelectionItem((ISelectionItem)parsenode.ChildNodes[0].AstNode)
                {
                    Alias = parsenode.ChildNodes[2].Token.ValueString
                };
            }
            else
            {
                parsenode.AstNode = (ISelectionItem)parsenode.ChildNodes[0].AstNode;
            }
        }

        private void CreateAliasedSourceNode(AstContext context, ParseTreeNode parsenode)
        {
            var item = parsenode.ChildNodes[0].AstNode as ISource;

            if (parsenode.ChildNodes.Count == 4)
            {
                IEnumerable<ColumnAlias> columnAliases = null;
                if (parsenode.ChildNodes[3].ChildNodes.Count == 1)
                {
                    var columnAliasesNames = (IEnumerable<string>)parsenode.ChildNodes[3].ChildNodes[0].AstNode;
                    columnAliases = columnAliasesNames.Select(
                        x => new ColumnAlias(x, null, null));
                }

                var source = new AliasedSourceItem(item, columnAliases)
                {
                    Alias = parsenode.ChildNodes[2].Token.ValueString
                };

                parsenode.AstNode = source;
            }
            else
            {
                parsenode.AstNode = item;
            }
        }

        private void CreateAlterTableNode(AstContext context, ParseTreeNode parsenode)
        {
            var table = new TableReference((ElementName)parsenode.ChildNodes[2].AstNode);
            IEnumerable<ITableComponent> addedComponents = null;
            if (ReferenceEquals(parsenode.ChildNodes[3].Term, _addTerm))
            {
                addedComponents = (IEnumerable<ITableComponent>)parsenode.ChildNodes[4].AstNode;
            }
            else
            {
                throw new NotImplementedException();
            }

            var node = new AlterTableCommand(table, addedComponents);
            parsenode.AstNode = node;
        }

        private void CreateArythmeticTerms()
        {
            _not = ToTerm("NOT");
            _not.Flags |= TermFlags.IsReservedWord;
            _and = ToTerm("AND");
            _and.Flags |= TermFlags.IsReservedWord;
            _or = ToTerm("OR");
            _or.Flags |= TermFlags.IsReservedWord;
            _add = ToTerm("+");
            _subtract = ToTerm("-");
            _multiply = ToTerm("*");
            _divide = ToTerm("/");
            _modulo = ToTerm("%");
            _bitwiseAnd = ToTerm("&");
            _bitwiseOr = ToTerm("|");
            _bitwiseExclusiveOr = ToTerm("^");
            _bitwiseNot = ToTerm("~");

            _assign = ToTerm("=");
            _addAssign = ToTerm("+=");
            _substractAssign = ToTerm("-=");
            _multiplyAssign = ToTerm("*=");
            _moduloAssign = ToTerm("%=");
            _divideAssign = ToTerm("/=");
            _bitwiseAndAssign = ToTerm("&=");
            _bitwiseOrAssign = ToTerm("|=");
            _bitwiseExclusiveOrAssign = ToTerm("^=");

            _equals = ToTerm("=");
            _greaterThan = ToTerm(">");
            _lessThan = ToTerm("<");
            _greaterThanOrEqual = ToTerm(">=");
            _lessThenOrEqual = ToTerm("<=");
            _notEqualToIso = ToTerm("<>");
            _notEqualTo = ToTerm("!=");
            _notLessThen = ToTerm("!<");
            _notGreaterThen = ToTerm("!>");
        }

        private void CreateBetweenExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var value = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
            var lowerBound = (ISqlValueExpression)parsenode.ChildNodes[3].AstNode;
            var upperBound = (ISqlValueExpression)parsenode.ChildNodes[5].AstNode;
            var isNegated = ReferenceEquals(parsenode.ChildNodes[2].Term, _not);
            parsenode.AstNode = new BetweenLogicalExpression(
                value,
                lowerBound,
                upperBound,
                isNegated);
        }

        private void CreateBinaryExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var left = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
            var right = (ISqlValueExpression)parsenode.ChildNodes[2].AstNode;

            if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _add))
            {
                parsenode.AstNode = new AddValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _subtract))
            {
                parsenode.AstNode = new SubtractValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _multiply))
            {
                parsenode.AstNode = new MultiplyValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _divide))
            {
                parsenode.AstNode = new DivideValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _modulo))
            {
                parsenode.AstNode = new ModuloValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _bitwiseAnd))
            {
                parsenode.AstNode = new BitwiseAndValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _bitwiseOr))
            {
                parsenode.AstNode = new BitwiseOrValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _bitwiseExclusiveOr))
            {
                parsenode.AstNode = new BitwiseExclusiveOrValueExpression(left, right, null, null);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        private void CreateBlockCommandNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 2)
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private void CreateCallExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            IEnumerable<ISqlValueExpression> arguments = null;
            if (parsenode.ChildNodes.Count == 2)
            {
                arguments = (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes?[1].AstNode;
            }

            var call = new SqlCallExpression(
                (ElementName)parsenode.ChildNodes[0].AstNode,
                arguments,
                null,
                null);
            parsenode.AstNode = call;
        }

        private void CreateCaseWhenNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[1].AstNode is ISqlValueExpression valueExpression)
            {
                ISqlValueExpression elseValue = null;

                if (parsenode.ChildNodes.Count == 6)
                {
                    elseValue = (ISqlValueExpression)parsenode.ChildNodes[4].AstNode;
                }

                var node = new SqlCaseSwitchExpression(
                    valueExpression,
                    (IEnumerable<SqlWhenThen>)parsenode.ChildNodes[2].AstNode,
                    elseValue,
                    null,
                    null);
                parsenode.AstNode = node;
            }
            else
            {
                ISqlValueExpression elseValue = null;

                if (parsenode.ChildNodes.Count == 5)
                {
                    elseValue = (ISqlValueExpression)parsenode.ChildNodes[3].AstNode;
                }

                var node = new SqlCaseExpression(
                    (IEnumerable<SqlLogicalWhenThen>)parsenode.ChildNodes[1].AstNode,
                    elseValue,
                    null,
                    null);
                parsenode.AstNode = node;
            }
        }

        private void CreateCastExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlCastExpression(
                (ISqlValueExpression)parsenode.ChildNodes[1].AstNode,
                null,
                (ISqlType)parsenode.ChildNodes[2].AstNode);
        }

        private void CreateCheckDeclarationNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = (Func<string, Constraint>)(x => new CheckConstraint(x, (ISqlLogicalExpression)parsenode.ChildNodes[1].AstNode));
        }

        private void CreateCoalesceExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var expressions = (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes[1].AstNode;
            parsenode.AstNode = new SqlCoalesceExpression(
                expressions,
                null,
                null);
        }

        private void CreateCodeBlockNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1)
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private void CreateColumnConstraintNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1 && ReferenceEquals(parsenode.ChildNodes[0].Term, _null))
            {
                var columnConstraint = new NullableConstraint();
                parsenode.AstNode = columnConstraint;
                columnConstraint.NullableValue = NullableValue.Null;
                return;
            }

            if (parsenode.ChildNodes.Count == 2
                && ReferenceEquals(parsenode.ChildNodes[0].Term, _not)
                && ReferenceEquals(parsenode.ChildNodes[1].Term, _null))
            {
                var columnConstraint = new NullableConstraint();
                parsenode.AstNode = columnConstraint;
                columnConstraint.NullableValue = NullableValue.NotNull;
                return;
            }

            if (ReferenceEquals(parsenode.ChildNodes[0].Term, _identityTerm))
            {
                var columnConstraint = new IdentityDeclaration();
                parsenode.AstNode = columnConstraint;
                if (parsenode.ChildNodes[1].ChildNodes.Count == 2)
                {
                    columnConstraint.Seed = (int?)parsenode.ChildNodes[1].ChildNodes[0].Token.Value;
                    columnConstraint.Increment = (int?)parsenode.ChildNodes[1].ChildNodes[1].Token.Value;
                }

                return;
            }

            if (parsenode.ChildNodes.Count == 2)
            {
                var constraint = (Func<string, Constraint>)parsenode.ChildNodes[1].AstNode;

                if (parsenode.ChildNodes[0].ChildNodes.Count == 2
                    && ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _constraintTerm))
                {
                    parsenode.AstNode = constraint(parsenode.ChildNodes[0].ChildNodes[1].Token.Value.ToString());
                }

                parsenode.AstNode = constraint(null);

                return;
            }

            throw new NotImplementedException();
        }

        private void CreateColumnDeclarationNode(AstContext context, ParseTreeNode parsenode)
        {
            var declaration = new ColumnDeclaration
            {
                Name = (string)parsenode.ChildNodes[0].Token.Value,
                SqlType = (ISqlType)parsenode.ChildNodes[1].AstNode
            };

            parsenode.AstNode = declaration;

            if (parsenode.ChildNodes.Count > 2)
            {
                var constraints = (List<IColumnConstraint>)parsenode.ChildNodes[2].AstNode;
                declaration.Identity =
                    constraints
                        .OfType<IdentityDeclaration>()
                        .SingleOrDefault();

                declaration.PrimaryKey =
                    constraints
                        .OfType<IIndexConstraint>()
                        .SingleOrDefault(x => x.IsPrimaryKey);

                declaration.DefaultValue =
                    constraints
                        .OfType<DefaultConstraint>()
                        .SingleOrDefault();

                declaration.Index =
                    constraints
                        .OfType<IIndexConstraint>()
                        .SingleOrDefault(x => !x.IsPrimaryKey);

                declaration.NullableValue =
                    constraints
                        .OfType<NullableConstraint>()
                        .Select(x => x.NullableValue)
                        .FirstOrDefault();
            }
        }

        private void CreateColumnDefaultValueNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode =
                (Func<string, Constraint>)(x => new DefaultConstraint(x, (ISqlValueExpression)parsenode.ChildNodes[1].AstNode));
        }

        private void CreateComments()
        {
            var comment = new CommentTerminal("comment", "/*", "*/");
            var lineComment = new CommentTerminal("line_comment", "--", "\n", "\r\n");
            NonGrammarTerminals.Add(comment);
            NonGrammarTerminals.Add(lineComment);
        }

        private void CreateComputedColumnConstraintNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count >= 1 && ReferenceEquals(parsenode.ChildNodes[0].Term, _persistedTerm))
            {
                var constraint = new PersistedConstraint();
                parsenode.AstNode = constraint;

                if (parsenode.ChildNodes[1].ChildNodes.Count == 2
                    && ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _not)
                    && ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[1].Term, _null))
                {
                    constraint.NullableValue = PersistedNullableValue.NotNull;
                }

                return;
            }

            if (parsenode.ChildNodes.Count == 2)
            {
                var index = (Func<string, Constraint>)parsenode.ChildNodes[1].AstNode;

                if (parsenode.ChildNodes[0].ChildNodes.Count == 2
                    && ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _constraintTerm))
                {
                    parsenode.AstNode = index(parsenode.ChildNodes[0].ChildNodes[1].Token.Value.ToString());
                }

                return;
            }

            throw new NotImplementedException();
        }

        private void CreateComputedColumnDeclarationNode(AstContext context, ParseTreeNode parsenode)
        {
            var declaration = new ComputedColumnDeclaration
            {
                Name = (string)parsenode.ChildNodes[0].Token.Value,
                Expression = (ISqlValueExpression)parsenode.ChildNodes[1].AstNode
            };
            parsenode.AstNode = declaration;
            if (parsenode.ChildNodes.Count > 2)
            {
                var constraints = (List<IColumnConstraint>)parsenode.ChildNodes[2].AstNode;

                declaration.PrimaryKey =
                    constraints
                        .OfType<IIndexConstraint>()
                        .SingleOrDefault(x => x.IsPrimaryKey);

                declaration.Index =
                    constraints
                        .OfType<IIndexConstraint>()
                        .SingleOrDefault(x => !x.IsPrimaryKey);

                declaration.Persisted =
                    constraints
                        .OfType<PersistedConstraint>()
                        .SingleOrDefault();
            }
        }

        private void CreateConvertNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlConvertExpression(
                (ISqlValueExpression)parsenode.ChildNodes[2].AstNode,
                null,
                (ISqlType)parsenode.ChildNodes[1].AstNode);
        }

        private void CreateCountExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            IEnumerable<OrderedValueExpression> order = null;
            IEnumerable<ISqlValueExpression> partition = null;

            if (parsenode.ChildNodes[2].ChildNodes.Any())
            {
                if (parsenode.ChildNodes[2].ChildNodes[2].ChildNodes.Any())
                {
                    order =
                        (IEnumerable<OrderedValueExpression>)parsenode.ChildNodes[2].ChildNodes[2].ChildNodes[0].AstNode;
                }

                if (parsenode.ChildNodes[2].ChildNodes[1].ChildNodes.Any())
                {
                    partition =
                        (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes[2].ChildNodes[1].ChildNodes[2].AstNode;
                }
            }

            if (parsenode.ChildNodes[1].ChildNodes.Count == 2)
            {
                var isDistinct = parsenode.ChildNodes[1].ChildNodes[0].ChildNodes.Any() && ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].ChildNodes[0].Term, _distinctTerm);
                var valueExpression = (ISqlValueExpression)parsenode.ChildNodes[1].ChildNodes[1].AstNode;

                if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _countTerm))
                {
                    parsenode.AstNode = new SqlCountExpression(
                        valueExpression,
                        isDistinct,
                        typeof(int),
                        TsqlTypeProvider.Int)
                    {
                        Order = order,
                        Partition = partition
                    };
                }
                else
                {
                    parsenode.AstNode = new SqlCountBigExpression(
                        valueExpression,
                        isDistinct,
                        typeof(int),
                        TsqlTypeProvider.Int)
                    {
                        Order = order,
                        Partition = partition
                    };
                }
            }
            else
            {
                if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _countTerm))
                {
                    parsenode.AstNode = new SqlAsteriskCountExpression(
                        typeof(int),
                        TsqlTypeProvider.Int)
                    {
                        Order = order,
                        Partition = partition
                    };
                }
                else
                {
                    parsenode.AstNode = new SqlAsteriskCountBigExpression(
                        typeof(int),
                        TsqlTypeProvider.Int)
                    {
                        Order = order,
                        Partition = partition
                    };
                }
            }
        }

        private void CreateCreateIndexNode(AstContext context, ParseTreeNode parsenode)
        {
            var table = new TableReference((ElementName)parsenode.ChildNodes[6].AstNode);
            var columns = (IEnumerable<OrderedColumnReference>)parsenode.ChildNodes[7].AstNode;
            foreach (var orderedColumnReference in columns)
            {
                var columnReference = (SqlColumnReference)orderedColumnReference.ColumnReference;
                columnReference.Source = table;
            }

            bool? isClustered = null;

            if (parsenode.ChildNodes[2].ChildNodes.Count == 1)
            {
                isClustered = ReferenceEquals(parsenode.ChildNodes[2].ChildNodes[0].Term, _clusteredTerm);
            }

            IIndexConstraint index;
            var indexName = parsenode.ChildNodes[4].Token.ValueString;
            var isIndexUnique = parsenode.ChildNodes[1].ChildNodes.Count == 1
                                && ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _uniqueTerm);

            if (isClustered.HasValue && isClustered.Value)
            {
                index = new ClusteredIndexConstraint(indexName, isIndexUnique, false);
            }
            else
            {
                index = new NonClusteredIndexConstraint(indexName, isIndexUnique, false);
            }

            var filter = (ISqlLogicalExpression)parsenode.ChildNodes[8].AstNode;
            var node = new CreateIndexCommand(index, table, filter);

            parsenode.AstNode = node;
        }

        private void CreateCreateSchemaNode(AstContext context, ParseTreeNode parsenode)
        {
            var node = new CreateSchemaCommand(
                parsenode.ChildNodes[2].Token.ValueString,
                (ElementName)parsenode.ChildNodes[3].AstNode);
            parsenode.AstNode = node;
        }

        private void CreateCreateTableNode(AstContext context, ParseTreeNode parsenode)
        {
            var name = (ElementName)parsenode.ChildNodes[2].AstNode;
            var components = (IEnumerable<ITableComponent>)parsenode.ChildNodes[3].AstNode;
            var columnDeclarations = components.OfType<ColumnDeclarationBase>().ToArray();
            var primaryKeys = new List<IIndexConstraint>();
            var indexes = new List<IIndexConstraint>();
            var checks = new List<CheckConstraint>();

            foreach (var tableComponent in components)
            {
                if (tableComponent is IIndexConstraint index)
                {
                    indexes.Add(index);
                    if (index.IsPrimaryKey)
                    {
                        primaryKeys.Add(index);
                    }
                }
                else if (tableComponent is CheckConstraint check)
                {
                    checks.Add(check);
                }
            }

            foreach (var x in columnDeclarations.Where(x => x.PrimaryKey != null))
            {
                if (!x.PrimaryKey.ColumnReferences.Any())
                {
                    x.PrimaryKey.SetColumnReference(new OrderedColumnReference(x, null));
                }
            }

            if (primaryKeys.Count > 1)
            {
                throw new InvalidOperationException("Multiple primary key definitions detected");
            }

            foreach (var x in columnDeclarations.Where(x => x.Index != null))
            {
                if (!x.Index.ColumnReferences.Any())
                {
                    x.Index.SetColumnReference(new OrderedColumnReference(x, null));
                }
            }

            foreach (var x in columnDeclarations.Where(x => x.Check != null))
            {
                if (!x.Index.ColumnReferences.Any())
                {
                    x.Index.SetColumnReference(new OrderedColumnReference(x, null));
                }
            }

            var node = new CreateTableCommand(
                name,
                primaryKeys.SingleOrDefault(),
                indexes,
                columnDeclarations,
                null,
                checks);
            parsenode.AstNode = node;
            ////foreach (var columnDeclarationBase in node.ColumnDeclarations)
            ////{
            ////    if (columnDeclarationBase.DefaultValue != null)
            ////    {
            ////        columnDeclarationBase.DefaultValue.ColumnReference = columnDeclarationBase;
            ////    }
            ////}
        }

        private void CreateElmentFullNameNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].AstNode is ElementName currentName)
            {
                currentName.AddPart(parsenode.ChildNodes[2].Token.Value.ToString());

                parsenode.AstNode = currentName;
            }
            else
            {
                parsenode.AstNode = new ElementName(parsenode.ChildNodes[0].Token.Value.ToString());
            }
        }

        private void CreateForeignKeyActionKindNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 2)
            {
                if (ReferenceEquals(parsenode.ChildNodes[0].Term, _noTerm) && ReferenceEquals(parsenode.ChildNodes[1].Term, _actionTerm))
                {
                    parsenode.AstNode = ForeignKeyActionKind.NoAction;
                }
                else if (ReferenceEquals(parsenode.ChildNodes[0].Term, _setTerm) && ReferenceEquals(parsenode.ChildNodes[1].Term, _null))
                {
                    parsenode.AstNode = ForeignKeyActionKind.SetNull;
                }
                else if (ReferenceEquals(parsenode.ChildNodes[0].Term, _setTerm) && ReferenceEquals(parsenode.ChildNodes[1].Term, _defaultTerm))
                {
                    parsenode.AstNode = ForeignKeyActionKind.SetDefault;
                }
            }
            else if (parsenode.ChildNodes.Count == 1 && ReferenceEquals(parsenode.ChildNodes[0].Term, _cascadeTerm))
            {
                parsenode.AstNode = ForeignKeyActionKind.Cascade;
            }
        }

        private void CreateForeignKeyActionNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new ForeignKeyAction
            {
                ActionKind = (ForeignKeyActionKind)parsenode.ChildNodes[2].AstNode,
                TriggerKind = ReferenceEquals(parsenode.ChildNodes[1].Term, _updateTerm) ? ForeignKeyTriggerKind.OnUpdate : ForeignKeyTriggerKind.OnDelete
            };
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void CreateIdentifier()
        {
            _identifier = new IdentifierTerminal(IdentifierTerminalName);
            _identifier.Flags = TermFlags.NoAstNode;
            var identifierTerm = new StringLiteral(IdentifierTerminalName + "_qouted");
            identifierTerm.AddStartEnd("[", "]", StringOptions.NoEscapes);
            identifierTerm.SetOutputTerminal(this, _identifier);

            _elementFullName = new NonTerminal(ElementFullNameNonTerminalName, CreateElmentFullNameNode);
            _elementFullName.Rule = _identifier
                                    | (_elementFullName + "." + _identifier);
            _variableName = new VariableTerminal();
            _systemVariableName = new SystemVariableTerminal();
            _identifierList = new NonTerminal(IdentifierListNonTerminalName, CreateIdentifierListNode);
            _identifierList.Rule = _identifier
                                   | (_identifierList + "," + _identifier);
        }

        private void CreateIdentifierListNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].AstNode is List<string> list)
            {
                var newList = new List<string>(list);
                newList.Add(parsenode.ChildNodes[1].Token.ValueString);
                parsenode.AstNode = newList;
                return;
            }

            parsenode.AstNode = new List<string>
            {
                parsenode.ChildNodes[0].Token.ValueString
            };
        }

        private void CreateInlineIfNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlInlineIfExpression(
                (ISqlLogicalExpression)parsenode.ChildNodes[1].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[2].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[3].AstNode,
                null,
                null);
        }

        private void CreateInNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[2].ChildNodes[0].AstNode is IEnumerable<ISqlValueExpression> set)
            {
                parsenode.AstNode = new SqlInLogicalExpression(
                    (ISqlValueExpression)parsenode.ChildNodes[0].AstNode,
                    parsenode.ChildNodes[1].ChildNodes.Count == 2,
                    set);
            }
            else
            {
                var command = (ISelectCommand)parsenode.ChildNodes[2].ChildNodes[0].AstNode;
                parsenode.AstNode = new SqlInLogicalExpression(
                    (ISqlValueExpression)parsenode.ChildNodes[0].AstNode,
                    parsenode.ChildNodes[1].ChildNodes.Count == 2,
                    command);
            }
        }

        private void CreateIsNullNode(AstContext context, ParseTreeNode parsenode)
        {
            var valueExpression = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
            var isNegated = ReferenceEquals(parsenode.ChildNodes[2].Term, _not);
            var expression = new SqlIsNullLogicalExpression(
                valueExpression,
                isNegated);
            parsenode.AstNode = expression;
        }

        [SuppressMessage(
            "Microsoft.Naming",
            "CA2204:Literals should be spelled correctly",
            MessageId = "spexecutesql",
            Justification = "Name of procedure.")]
        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void CreateKeyTerminals()
        {
            _scheamTerm = ToTerm("SCHEMA");
            _scheamTerm.Flags |= TermFlags.IsReservedWord;
            _authorizationTerm = ToTerm("AUTHORIZATION");
            _authorizationTerm.Flags |= TermFlags.IsReservedWord;
            _selectTerm = ToTerm("SELECT");
            _selectTerm.Flags |= TermFlags.IsReservedWord;
            _insertTerm = ToTerm("INSERT");
            _insertTerm.Flags |= TermFlags.IsReservedWord;
            _updateTerm = ToTerm("UPDATE");
            _updateTerm.Flags |= TermFlags.IsReservedWord;
            _deleteTerm = ToTerm("DELETE");
            _deleteTerm.Flags |= TermFlags.IsReservedWord;
            _asteriskTerm = ToTerm("*");
            _asteriskTerm.Flags |= TermFlags.IsReservedWord;
            _fromTerm = ToTerm("FROM");
            _fromTerm.Flags |= TermFlags.IsReservedWord;
            _joinTerm = ToTerm("JOIN");
            _joinTerm.Flags |= TermFlags.IsReservedWord;
            _leftTerm = ToTerm("LEFT");
            _leftTerm.Flags |= TermFlags.IsReservedWord;
            _outerTerm = ToTerm("OUTER");
            _outerTerm.Flags |= TermFlags.IsReservedWord;
            _rightTerm = ToTerm("RIGHT");
            _rightTerm.Flags |= TermFlags.IsReservedWord;
            _innerTerm = ToTerm("INNER");
            _innerTerm.Flags |= TermFlags.IsReservedWord;
            _fullTerm = ToTerm("FULL");
            _fullTerm.Flags |= TermFlags.IsReservedWord;
            _unionTerm = ToTerm("UNION");
            _unionTerm.Flags |= TermFlags.IsReservedWord;
            _allTerm = ToTerm("ALL");
            _allTerm.Flags |= TermFlags.IsReservedWord;
            _withTerm = ToTerm("WITH");
            _withTerm.Flags |= TermFlags.IsReservedWord;
            _asTerm = ToTerm("AS");
            _asTerm.Flags |= TermFlags.IsReservedWord;
            _setTerm = ToTerm("SET");
            _setTerm.Flags |= TermFlags.IsReservedWord;
            _declareTerm = ToTerm("DECLARE");
            _declareTerm.Flags |= TermFlags.IsReservedWord;
            _maxTerm = ToTerm("MAX");
            _maxTerm.Flags |= TermFlags.IsReservedWord;
            _onTerm = ToTerm("ON");
            _onTerm.Flags |= TermFlags.IsReservedWord;
            _offTerm = ToTerm("OFF");
            _offTerm.Flags |= TermFlags.IsReservedWord;
            _goTerm = ToTerm("GO");
            _goTerm.Flags |= TermFlags.IsReservedWord;
            _addTerm = ToTerm("Add");
            _addTerm.Flags |= TermFlags.IsReservedWord;
            _useTerm = ToTerm("USE");
            _useTerm.Flags |= TermFlags.IsReservedWord;
            _procedureTerm = ToTerm("PROCEDURE");
            _procedureTerm.Flags |= TermFlags.IsReservedWord;
            _existsTerm = ToTerm("EXISTS");
            _existsTerm.Flags |= TermFlags.IsReservedWord;
            _castTerm = ToTerm("CAST");
            _castTerm.Flags |= TermFlags.IsReservedWord;
            _topTerm = ToTerm("TOP");
            _topTerm.Flags |= TermFlags.IsReservedWord;
            _inTerm = ToTerm("IN");
            _inTerm.Flags |= TermFlags.IsReservedWord;
            _createTerm = ToTerm("CREATE");
            _createTerm.Flags |= TermFlags.IsReservedWord;
            _alterTerm = ToTerm("ALTER");
            _alterTerm.Flags |= TermFlags.IsReservedWord;
            _dropTerm = ToTerm("DROP");
            _dropTerm.Flags |= TermFlags.IsReservedWord;
            _beginTerm = ToTerm("BEGIN");
            _beginTerm.Flags |= TermFlags.IsReservedWord;
            _endTerm = ToTerm("END");
            _endTerm.Flags |= TermFlags.IsReservedWord;
            _betweenTerm = ToTerm("BETWEEN");
            _betweenTerm.Flags |= TermFlags.IsReservedWord;
            _readonlyTerm = ToTerm("READONLY");
            _readonlyTerm.Flags |= TermFlags.IsReservedWord;
            _throwTerm = ToTerm("THROW");
            _throwTerm.Flags |= TermFlags.IsReservedWord;
            _ifTerm = ToTerm("IF");
            _ifTerm.Flags |= TermFlags.IsReservedWord;
            _elseTerm = ToTerm("ELSE");
            _elseTerm.Flags |= TermFlags.IsReservedWord;
            _whereTerm = ToTerm("WHERE");
            _whereTerm.Flags |= TermFlags.IsReservedWord;
            _inlineIfTerm = ToTerm("IIF");
            _inlineIfTerm.Flags |= TermFlags.IsReservedWord;
            _coalesce = ToTerm("COALESCE");
            _coalesce.Flags |= TermFlags.IsReservedWord;
            _is = ToTerm("IS");
            _is.Flags |= TermFlags.IsReservedWord;
            _null = ToTerm("NULL");
            _null.Flags |= TermFlags.IsReservedWord;
            CreateArythmeticTerms();

            _defaultTerm = ToTerm("DEFAULT");
            _defaultTerm.Flags |= TermFlags.IsReservedWord;
            _functionTerm = ToTerm("FUNCTION");
            _functionTerm.Flags |= TermFlags.IsReservedWord;
            _returnsTerm = ToTerm("RETURNS");
            _returnsTerm.Flags |= TermFlags.IsReservedWord;
            _returnTerm = ToTerm("RETURN");
            _returnsTerm.Flags |= TermFlags.IsReservedWord;
            _tableTerm = ToTerm("TABLE");
            _tableTerm.Flags |= TermFlags.IsReservedWord;
            _encriptionTerm = ToTerm("ENCRIPTION");
            _encriptionTerm.Flags |= TermFlags.IsReservedWord;
            _schemabindingTerm = ToTerm("SCHEMABINDING");
            _schemabindingTerm.Flags |= TermFlags.IsReservedWord;
            _valuesTerm = ToTerm("VALUES");
            _valuesTerm.Flags |= TermFlags.IsReservedWord;
            _distinctTerm = ToTerm("DISTINCT");
            _distinctTerm.Flags |= TermFlags.IsReservedWord;

            _orderTerm = ToTerm("ORDER");
            _orderTerm.Flags |= TermFlags.IsReservedWord;
            _byTerm = ToTerm("BY");
            _byTerm.Flags |= TermFlags.IsReservedWord;
            _groupTerm = ToTerm("GROUP");
            _byTerm.Flags |= TermFlags.IsReservedWord;
            _havingTerm = ToTerm("HAVING");
            _byTerm.Flags |= TermFlags.IsReservedWord;
            _rowNumberTerm = ToTerm("ROW_NUMBER");
            _rowNumberTerm.Flags |= TermFlags.IsReservedWord;
            _partitionTerm = ToTerm("PARTITION");
            _partitionTerm.Flags |= TermFlags.IsReservedWord;
            _overTerm = ToTerm("OVER");
            _overTerm.Flags |= TermFlags.IsReservedWord;
            _executeTerm = ToTerm("EXEC");
            _executeTerm.Flags |= TermFlags.IsReservedWord;
            _executeFullTerm = ToTerm("EXECUTE");
            _executeFullTerm.Flags |= TermFlags.IsReservedWord;
            _executeSqlTerm = ToTerm("sp_executesql");
            _executeSqlTerm.Flags |= TermFlags.IsReservedWord;

            _thenTerm = ToTerm("THEN");
            _thenTerm.Flags |= TermFlags.IsReservedWord;
            _whenTerm = ToTerm("WHEN");
            _whenTerm.Flags |= TermFlags.IsReservedWord;
            _matchedTerm = ToTerm("MATCHED");
            _matchedTerm.Flags |= TermFlags.IsReservedWord;
            _mergeTerm = ToTerm("MERGE");
            _mergeTerm.Flags |= TermFlags.IsReservedWord;
            _intoTerm = ToTerm("INTO");
            _intoTerm.Flags |= TermFlags.IsReservedWord;
            _usingTerm = ToTerm("USING");
            _usingTerm.Flags |= TermFlags.IsReservedWord;
            _sourceTerm = ToTerm("SOURCE");
            _sourceTerm.Flags |= TermFlags.IsReservedWord;
            _targetTerm = ToTerm("TARGET");
            _targetTerm.Flags |= TermFlags.IsReservedWord;
            _percentTerm = ToTerm("PERCENT");
            _percentTerm.Flags |= TermFlags.IsReservedWord;
            _tiesTerm = ToTerm("TIES");
            _tiesTerm.Flags |= TermFlags.IsReservedWord;
            _crossTerm = ToTerm("CROSS");
            _createTerm.Flags |= TermFlags.IsReservedWord;
            _applyTerm = ToTerm("APPLY");
            _applyTerm.Flags |= TermFlags.IsReservedWord;
            _typeTerm = ToTerm("TYPE");
            _typeTerm.Flags |= TermFlags.IsReservedWord;

            _ascendingTerm = ToTerm("ASC");
            _ascendingTerm.Flags |= TermFlags.IsReservedWord;
            _descencingTerm = ToTerm("DESC");
            _ascendingTerm.Flags |= TermFlags.IsReservedWord;
            _primaryTerm = ToTerm("PRIMARY");
            _primaryTerm.Flags |= TermFlags.IsReservedWord;
            _keyTerm = ToTerm("KEY");
            _keyTerm.Flags |= TermFlags.IsReservedWord;
            _nonclusteredTerm = ToTerm("NONCLUSTERED");
            _nonclusteredTerm.Flags |= TermFlags.IsReservedWord;
            _uniqueTerm = ToTerm("UNIQUE");
            _uniqueTerm.Flags |= TermFlags.IsReservedWord;
            _clusteredTerm = ToTerm("CLUSTERED");
            _clusteredTerm.Flags |= TermFlags.IsReservedWord;

            _triggerTerm = ToTerm("TRIGGER");
            _triggerTerm.Flags |= TermFlags.IsReservedWord;
            _callerTerm = ToTerm("CALLER");
            _callerTerm.Flags |= TermFlags.IsReservedWord;
            _selfTerm = ToTerm("SELF");
            _selfTerm.Flags |= TermFlags.IsReservedWord;
            _forTerm = ToTerm("FROM");
            _forTerm.Flags |= TermFlags.IsReservedWord;
            _afterTerm = ToTerm("FOR");
            _afterTerm.Flags |= TermFlags.IsReservedWord;
            _ofTerm = ToTerm("OF");
            _ofTerm.Flags |= TermFlags.IsReservedWord;
            _insteadTerm = ToTerm("INSTEAD");
            _insteadTerm.Flags |= TermFlags.IsReservedWord;
            _raiseErrorTerm = ToTerm("RAISERROR");
            _raiseErrorTerm.Flags |= TermFlags.IsReservedWord;
            _transactionTerm = ToTerm("TRAN");
            _transactionTerm.Flags |= TermFlags.IsReservedWord;
            _transactionFullTerm = ToTerm("TRANSACTION");
            _transactionFullTerm.Flags |= TermFlags.IsReservedWord;
            _commitTerm = ToTerm("COMMIT");
            _commitTerm.Flags |= TermFlags.IsReservedWord;
            _rollbackTerm = ToTerm("ROLLBACK");
            _rollbackTerm.Flags |= TermFlags.IsReservedWord;
            _countTerm = ToTerm("COUNT");
            _countTerm.Flags |= TermFlags.IsReservedWord;
            _countBigTerm = ToTerm("COUNT_BIG");
            _countBigTerm.Flags |= TermFlags.IsReservedWord;
            _identityTerm = ToTerm("IDENTITY");
            _identityTerm.Flags |= TermFlags.IsReservedWord;
            _caseTerm = ToTerm("CASE");
            _caseTerm.Flags |= TermFlags.IsReservedWord;
            _persistedTerm = ToTerm("PERSISTED");
            _persistedTerm.Flags |= TermFlags.IsReservedWord;
            _constraintTerm = ToTerm("CONSTRAINT");
            _constraintTerm.Flags |= TermFlags.IsReservedWord;
            _noTerm = ToTerm("NO");
            _noTerm.Flags |= TermFlags.IsReservedWord;
            _actionTerm = ToTerm("ACTION");
            _actionTerm.Flags |= TermFlags.IsReservedWord;
            _cascadeTerm = ToTerm("CASCADE");
            _cascadeTerm.Flags |= TermFlags.IsReservedWord;
            _foreignTerm = ToTerm("FOREIGN");
            _foreignTerm.Flags |= TermFlags.IsReservedWord;
            _referencesTerm = ToTerm("REFERENCES");
            _referencesTerm.Flags |= TermFlags.IsReservedWord;
            _checkTerm = ToTerm("CHECK");
            _checkTerm.Flags |= TermFlags.IsReservedWord;
            _indexTerm = ToTerm("INDEX");
            _indexTerm.Flags |= TermFlags.IsReservedWord;
            _identityInsertTerm = ToTerm("IDENTITY_INSERT");
            _identityInsertTerm.Flags |= TermFlags.IsReservedWord;
            _convert = ToTerm("CONVERT");
            _convert.Flags |= TermFlags.IsReservedWord;
        }

        private void CreateListNode<TNode>(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].AstNode is List<TNode> list)
            {
                var newList = new List<TNode>(list);
                newList.Add((TNode)parsenode.ChildNodes[1].AstNode);
                parsenode.AstNode = newList;
                return;
            }

            parsenode.AstNode = new List<TNode>
            {
                (TNode)parsenode.ChildNodes[0].AstNode
            };
        }

        private void CreateLiterals()
        {
            _number = CreateNumber(NumberLiteralName);
            _binary = new BinaryLiteral(BinaryLiteralName, null);
            _binary.Flags = TermFlags.NoAstNode;
            _text = new StringLiteral(
                TextLiteralName,
                "\'",
                StringOptions.AllowsDoubledQuote | StringOptions.AllowsLineBreak);
            _text.AddPrefix("N", StringOptions.None);
            _text.Flags = TermFlags.NoAstNode;
        }

        private void CreateLogicalExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 3)
            {
                ProcessLogicalBinaryExpression(parsenode);
            }
            else if (parsenode.ChildNodes.Count == 1)
            {
                if (parsenode.ChildNodes[0].AstNode is ISqlLogicalExpression logicalExpression)
                {
                    parsenode.AstNode = logicalExpression;
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private void CreateOrderByNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = parsenode.ChildNodes[2].AstNode;
        }

        private void CreateOrderedColumnNode(AstContext context, ParseTreeNode parsenode)
        {
            var columnReference = new SqlColumnReference(
                new ElementName(parsenode.ChildNodes[0].Token.ValueString),
                null,
                null,
                null);
            var ascending = !parsenode.ChildNodes[1].ChildNodes.Any() ? null : (bool?)ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _ascendingTerm);
            var reference = new OrderedColumnReference(
                columnReference,
                ascending);

            parsenode.AstNode = reference;
        }

        private void CreateOrderedValueExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var ascending = !parsenode.ChildNodes[1].ChildNodes.Any()
                            || ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _ascendingTerm);
            parsenode.AstNode = new OrderedValueExpression(
                (ISqlValueExpression)parsenode.ChildNodes[0].AstNode,
                ascending);
        }

        private void CreateRowNumberNode(AstContext context, ParseTreeNode parsenode)
        {
            var orderedValueExpressions = (IEnumerable<OrderedValueExpression>)parsenode.ChildNodes[3].AstNode;

            IEnumerable<ISqlValueExpression> partition = null;
            if (parsenode.ChildNodes[2].ChildNodes.Any())
            {
                partition = (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes[2].ChildNodes[2].AstNode;
            }

            var node = new RowNumberSelection(orderedValueExpressions, partition, typeof(long), TsqlTypeProvider.BigInt);
            parsenode.AstNode = node;
        }

        private void CreateScriptNode(AstContext context, ParseTreeNode parsenode)
        {
            var commands = parsenode.ChildNodes.SelectMany(
                    x =>
                    {
                        if (x.AstNode is SqlScript innerScript)
                        {
                            return innerScript.Commands;
                        }

                        return new[]
                        {
                            (ISqlCommand)x.AstNode
                        };
                    })
                .ToArray();
            var node = new SqlScript(commands, null);
            parsenode.AstNode = node;
        }

        private void CreateSelectionBodyNode(AstContext context, ParseTreeNode parsenode)
        {
            ISource source = null;
            ISqlLogicalExpression filter = null;
            IEnumerable<OrderedValueExpression> order = null;

            if (parsenode.ChildNodes[4].ChildNodes.Any())
            {
                source = (ISource)parsenode.ChildNodes[4].ChildNodes[1].AstNode;
                var selectionOrganization = parsenode.ChildNodes[4].ChildNodes[2];
                if (selectionOrganization.ChildNodes[0].ChildNodes.Any())
                {
                    filter = (ISqlLogicalExpression)selectionOrganization.ChildNodes[0].ChildNodes[1].AstNode;
                }

                if (parsenode.ChildNodes[4].ChildNodes[3].ChildNodes.Any())
                {
                    order = (IEnumerable<OrderedValueExpression>)parsenode.ChildNodes[4].ChildNodes[3].ChildNodes[0].AstNode;
                }
            }

            var node = new SelectCommand(
                source,
                filter: filter,
                order: order,
                isDistinct: parsenode.ChildNodes[1].ChildNodes.Count == 1,
                top:
                (TopExpression)(parsenode.ChildNodes[2].ChildNodes.Count == 1 ? parsenode.ChildNodes[2].ChildNodes[0].AstNode : null),
                selection: new SqlSelection((IEnumerable<ISelectionItem>)parsenode.ChildNodes[3].AstNode, null));
            parsenode.AstNode = node;
        }

        private void CreateSelectionListItemNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 2)
            {
                parsenode.AstNode = new AsteriskSelection(
                    new TableReference((ElementName)parsenode.ChildNodes[0].AstNode),
                    null,
                    null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[0].Term, _asteriskTerm))
            {
                parsenode.AstNode = new AsteriskSelection(null, null, null);
            }
            else if (parsenode.ChildNodes.Count == 3 && ReferenceEquals(parsenode.ChildNodes[2].Term, _asteriskTerm))
            {
                parsenode.AstNode = new AsteriskSelection(
                    new TableReference((ElementName)parsenode.ChildNodes[0].AstNode),
                    null,
                    null);
            }
            else
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
        }

        private void CreateSelectionSourceNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1)
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
            else if (parsenode.ChildNodes.Count == 2)
            {
                parsenode.AstNode = new CrossJoinSource(
                    (ISource)parsenode.ChildNodes[0].AstNode,
                    (ISource)parsenode.ChildNodes[1].AstNode);
            }
            else if (parsenode.ChildNodes.Count == 4)
            {
                parsenode.AstNode = new CrossJoinSource(
                    (ISource)parsenode.ChildNodes[0].AstNode,
                    (ISource)parsenode.ChildNodes[3].AstNode);
            }
            else if (parsenode.ChildNodes.Count == 6)
            {
                parsenode.AstNode = new InnerJoinSource(
                    (ISource)parsenode.ChildNodes[0].AstNode,
                    (ISource)parsenode.ChildNodes[3].AstNode,
                    (ISqlLogicalExpression)parsenode.ChildNodes[5].AstNode);
            }
            else if (parsenode.ChildNodes.Count == 7)
            {
                var left = (ISource)parsenode.ChildNodes[0].AstNode;
                var right = (ISource)parsenode.ChildNodes[4].AstNode;
                var on = (ISqlLogicalExpression)parsenode.ChildNodes[6].AstNode;
                ConditionalJoinSource join;

                if (ReferenceEquals(parsenode.ChildNodes[1].Term, _fullTerm))
                {
                    join = new FullJoinSource(left, right, on);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _leftTerm))
                {
                    join = new LeftJoinSource(left, right, on);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _rightTerm))
                {
                    join = new RightJoinSource(left, right, on);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _innerTerm))
                {
                    join = new InnerJoinSource(left, right, on);
                }
                else
                {
                    throw new InvalidOperationException();
                }

                parsenode.AstNode = join;
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        private void CreateSelectNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1)
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private void CreateSourceItemNode(AstContext context, ParseTreeNode parsenode)
        {
            ISource item = null;
            if (parsenode.ChildNodes[0].Term.Equals(_elementFullName))
            {
                item = new TableReference((ElementName)parsenode.ChildNodes[0].AstNode);
            }

            if (item == null)
            {
                item = parsenode.ChildNodes[0].AstNode as ISource;
            }

            parsenode.AstNode = item;
        }

        private void CreateTableComponentsListNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = parsenode.ChildNodes.SelectMany(
                    x =>
                    {
                        if (x.AstNode is List<ITableComponent> listNode)
                        {
                            return listNode;
                        }

                        var node = x;
                        if (x.Term.Name != ColumnDeclarationNonTerminalName
                            && x.Term.Name != ComputedColumnDeclarationNonTerminalName
                            && x.Term.Name != TableConstraintNonTerminalName)
                        {
                            node = x.ChildNodes[0];
                        }

                        return new List<ITableComponent>
                        {
                            (ITableComponent)node.AstNode
                        };
                    })
                .ToList();
        }

        private void CreateTableForeignKeyDeclaration(AstContext context, ParseTreeNode parsenode)
        {
            var referencedTable = new TableReference((ElementName)parsenode.ChildNodes[4].AstNode);

            parsenode.AstNode = (Func<string, Constraint>)(constraintName =>
            {
                var node = new ForeignKeyConstraint(constraintName)
                {
                    Table = referencedTable
                };

                var targetTableColumnReferences = ((IEnumerable<string>)parsenode.ChildNodes[2].AstNode)
                    .Select(
                        x => new SqlColumnReference(new ElementName(x), null, null, null))
                    .ToArray();

                node.SetTargetColumns(targetTableColumnReferences);

                ParseTreeNode actionsNode = null;
                if (parsenode.ChildNodes.Count >= 6 && parsenode.ChildNodes[5].Term == _identifierList)
                {
                    var referencedTableColumns = ((IEnumerable<string>)parsenode.ChildNodes[5].AstNode)
                        .Select(
                            x => new SqlColumnReference(new ElementName(x), referencedTable, null, null))
                        .ToArray();

                    node.SetReferencedTableColumns(referencedTableColumns);

                    if (parsenode.ChildNodes.Count == 7)
                    {
                        actionsNode = parsenode.ChildNodes[6];
                    }
                }
                else if (parsenode.ChildNodes.Count == 6)
                {
                    actionsNode = parsenode.ChildNodes[5];
                }

                if (actionsNode != null)
                {
                    node.Actions = (IEnumerable<ForeignKeyAction>)actionsNode.AstNode;
                }

                return node;
            });
        }

        private void CreateTableUniqueIndexNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = (Func<string, Constraint>)(constraintName =>
            {
                ColumnsTableConstraint constraint = null;
                bool? isClustered = null;
                bool? isNonClustered = null;

                if (parsenode.ChildNodes[1].ChildNodes.Any())
                {
                    isClustered = ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _clusteredTerm);
                    isNonClustered = ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _nonclusteredTerm);
                }

                if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _primaryTerm)
                    && ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[1].Term, _keyTerm))
                {
                    if (isClustered.HasValue && isClustered.Value)
                    {
                        constraint = new ClusteredIndexConstraint(constraintName, true, true);
                    }

                    if (isNonClustered.HasValue && isNonClustered.Value)
                    {
                        constraint = new NonClusteredIndexConstraint(constraintName, true, true);
                    }
                }
                else if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _uniqueTerm))
                {
                    if (isClustered.HasValue && isClustered.Value)
                    {
                        constraint = new ClusteredIndexConstraint(constraintName, true, false);
                    }

                    if (isNonClustered.HasValue && isNonClustered.Value)
                    {
                        constraint = new ClusteredIndexConstraint(constraintName, true, false);
                    }
                }
                else
                {
                    throw new InvalidOperationException();
                }

                constraint.SetColumnReference((List<OrderedColumnReference>)parsenode.ChildNodes[2].AstNode);
                return constraint;
            });
        }

        private void CreateTypeNameNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1)
            {
                var name = (ElementName)parsenode.ChildNodes.Single().AstNode;
                if (name.Count == 1)
                {
                    var normalizedName = TsqlNativeTypesNames.NormalizeName(name[0]);
                    if (normalizedName != null)
                    {
                        parsenode.AstNode = new TsqlType(normalizedName);
                    }
                    else
                    {
                        parsenode.AstNode = new CustomTypeReference(name);
                    }
                }
                else
                {
                    parsenode.AstNode = new CustomTypeReference(name);
                }
            }
            else if (parsenode.ChildNodes.Count == 2)
            {
                var name = parsenode.ChildNodes[0].Token.Value.ToString();
                name = TsqlNativeTypesNames.NormalizeName(name);
                var capacity = parsenode.ChildNodes[1].Term == _maxTerm ? -1 : (int)parsenode.ChildNodes[1].Token.Value;
                parsenode.AstNode = new TsqlType(name, capacity);
            }
        }

        private void CreateUnaryValueExpression(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[1].AstNode is SqlConstantExpression)
            {
                throw new NotImplementedException();
            }

            throw new NotImplementedException();
        }

        private void CreateUniqueIndexDeclarationNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = (Func<string, Constraint>)(name =>
            {
                ColumnsTableConstraint constraint = null;
                bool? isClustered = null;
                bool? isNonClustered = null;

                if (parsenode.ChildNodes[1].ChildNodes.Any())
                {
                    isClustered = ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _clusteredTerm);
                    isNonClustered = ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _nonclusteredTerm);
                }

                if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _primaryTerm)
                    && ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[1].Term, _keyTerm))
                {
                    if (isClustered.HasValue && isClustered.Value)
                    {
                        constraint = new ClusteredIndexConstraint(name, true, true);
                    }

                    if (isNonClustered.HasValue && isNonClustered.Value)
                    {
                        constraint = new NonClusteredIndexConstraint(name, true, true);
                    }
                }
                else if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _uniqueTerm))
                {
                    if (isClustered.HasValue && isClustered.Value)
                    {
                        constraint = new ClusteredIndexConstraint(name, true, false);
                    }

                    if (isNonClustered.HasValue && isNonClustered.Value)
                    {
                        constraint = new NonClusteredIndexConstraint(name, true, false);
                    }
                }
                else
                {
                    throw new InvalidOperationException();
                }

                if (parsenode.ChildNodes.Count == 3 && parsenode.ChildNodes[2].AstNode != null)
                {
                    constraint.SetColumnReference((List<OrderedColumnReference>)parsenode.ChildNodes[2].AstNode);
                }

                return constraint;
            });
        }

        /// <summary>
        ///     Creates the value expression node.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="parsenode">The parsenode.</param>
        private void CreateValueExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].Term == _number
                || parsenode.ChildNodes[0].Term == _text
                || parsenode.ChildNodes[0].Term == _binary)
            {
                ISqlType type;
                var clrType = parsenode.ChildNodes[0].Token.Value.GetType();
                var value = parsenode.ChildNodes[0].Token.Value;

                if (parsenode.ChildNodes[0].Term == _number)
                {
                    var indexOfDot = parsenode.ChildNodes[0].Token.ValueString.IndexOf(".", StringComparison.Ordinal);
                    if (indexOfDot == -1)
                    {
                        var intValue = Convert.ToInt64(value);
                        if (intValue > TsqlNativeTypesNames.IntMinValue && intValue < TsqlNativeTypesNames.IntMaxValue)
                        {
                            type = new TsqlType(TsqlNativeTypesNames.Int);
                        }
                        else
                        {
                            type = new TsqlType(TsqlNativeTypesNames.Numeric, (byte)parsenode.ChildNodes[0].Token.ValueString.Length, 0);
                        }
                    }
                    else
                    {
                        var leadingZeroCount = 0;
                        for (var i = 0; i < indexOfDot; i++)
                        {
                            if (parsenode.ChildNodes[0].Token.ValueString[i] == '0')
                            {
                                leadingZeroCount++;
                            }
                            else
                            {
                                break;
                            }
                        }

                        var precision = parsenode.ChildNodes[0].Token.ValueString.Length - 1 - leadingZeroCount;
                        var scale = parsenode.ChildNodes[0].Token.ValueString.Length - indexOfDot - 1;
                        type = new TsqlType(TsqlNativeTypesNames.Numeric, (byte)precision, (byte)scale);
                    }
                }
                else if (parsenode.ChildNodes[0].Term == _text)
                {
                    if (parsenode.ChildNodes[0].Token.Text.StartsWith("N", StringComparison.Ordinal))
                    {
                        type = new TsqlType(TsqlNativeTypesNames.NVarChar, parsenode.ChildNodes[0].Token.ValueString.Length);
                    }
                    else
                    {
                        type = new TsqlType(TsqlNativeTypesNames.VarChar, parsenode.ChildNodes[0].Token.ValueString.Length);
                    }
                }
                else
                {
                    type = new TsqlType(TsqlNativeTypesNames.VarBinary, ((byte[])value).Length);
                }

                parsenode.AstNode = new SqlConstantExpression(
                    value,
                    clrType,
                    type);
            }
            else
            {
                if (parsenode.ChildNodes[0].AstNode is ISqlValueExpression valueExpression)
                {
                    parsenode.AstNode = valueExpression;
                    return;
                }

                if (parsenode.ChildNodes[0].AstNode is ElementName elementName)
                {
                    parsenode.AstNode = new SqlColumnReference(elementName, null, null, null);
                    return;
                }

                throw new NotImplementedException();
            }
        }

        private void CreateWhenSearchedNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlLogicalWhenThen(
                (ISqlLogicalExpression)parsenode.ChildNodes[1].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[3].AstNode);
        }

        private void CreateWhenSimpleNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlWhenThen(
                (ISqlValueExpression)parsenode.ChildNodes[1].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[3].AstNode);
        }

        private void CreeateTableConstraintNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = parsenode.ChildNodes[1].AstNode;

            var constraint = (Func<string, Constraint>)parsenode.ChildNodes[1].AstNode;

            if (parsenode.ChildNodes[0].ChildNodes.Count == 2
                && ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _constraintTerm))
            {
                parsenode.AstNode = constraint(parsenode.ChildNodes[0].ChildNodes[1].Token.Value.ToString());
            }
            else
            {
                parsenode.AstNode = constraint(string.Empty);
            }
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void ProcessArithmetic()
        {
            _logicalExpression = new NonTerminal(LogicalExpressionNonTerminalName, CreateLogicalExpressionNode);
            _valueExpression = new NonTerminal(ValueExpressionNonTerminalName, CreateValueExpressionNode);
            _expression = new NonTerminal(ExpressionNonTerminalName);
            _betweenExpression = new NonTerminal(BetweenExpressionNonTerminalName, CreateBetweenExpressionNode);
            _valueExpressionList = new NonTerminal(ValueExpressionListNonTerminalName, CreateListNode<ISqlValueExpression>);
            _assignment = new NonTerminal(AssignmentNonTerminalName);
            var count = new NonTerminal("CountExpression", CreateCountExpressionNode);
            count.Rule = (_countTerm | _countBigTerm)
                         + "("
                         + ((new AstNodeCreatingTerm(
                                 (c, n) =>
                                 {
                                 })
                             + (_allTerm | _distinctTerm | Empty)
                             + _valueExpression)
                            | _asteriskTerm)
                         + ")"
                         + ((new AstNodeCreatingTerm(
                                 (c, n) =>
                                 {
                                 })
                             + Empty)
                            | (_overTerm
                               + "("
                               + ((new AstNodeCreatingTerm(
                                       (c, n) =>
                                       {
                                       })
                                   + _partitionTerm
                                   + _byTerm
                                   + _valueExpressionList)
                                  | Empty)
                               + (_orderBy | Empty)
                               + ")"));

            var aggregate = new NonTerminal(AggregateNonTerminalName);
            MarkTransient(aggregate);
            aggregate.Rule = count;

            _valueExpressionList.Rule = _valueExpression
                                        | (_valueExpressionList + "," + _valueExpression);
            _logicalExpressionList = new NonTerminal(LogicalExpressionListNonTerminalName);
            _logicalExpressionList.Rule = _logicalExpression
                                          | (_logicalExpressionList + "," + _logicalExpression);

            _callExpression = new NonTerminal(CallExpressionNonTerminalName, CreateCallExpressionNode);
            _callExpression.Rule = _elementFullName + PreferShiftHere() + "(" + ((new TransientNode() + _valueExpressionList) | Empty) + ")";
            var convertExpression = new NonTerminal("ConvertExpression", CreateConvertNode);
            convertExpression.Rule = _convert + "(" + _typeName + "," + _valueExpression + ")";

            var castExpression = new NonTerminal("CastExpression", CreateCastExpressionNode);
            castExpression.Rule = _castTerm + "(" + _valueExpression + _asTerm + _typeName + ")";

            var nullExpression = new NonTerminal("NullExpression", (x, y) => y.AstNode = new SqlNullExpression(typeof(object), null));
            nullExpression.Rule = _null;

            var coalesceExpression = new NonTerminal("CoalesceExpression", CreateCoalesceExpressionNode);
            coalesceExpression.Rule = _coalesce + "(" + _valueExpressionList + ")";

            var binaryExpression = new NonTerminal("BinaryExpression", CreateBinaryExpressionNode);
            binaryExpression.Rule = _valueExpression
                                    + (_add
                                       | _subtract
                                       | _multiply
                                       | _divide
                                       | _modulo
                                       | _bitwiseAnd
                                       | _bitwiseOr
                                       | _bitwiseExclusiveOr)
                                    + _valueExpression;

            var inlineIf = new NonTerminal("InlineIf", CreateInlineIfNode);
            inlineIf.Rule = _inlineIfTerm + "(" + _logicalExpression + "," + _valueExpression + "," + _valueExpression + ")";

            var unaryValueExpression = new NonTerminal("UnaryValueExpression", CreateUnaryValueExpression);
            unaryValueExpression.Rule = (_add | _subtract | _bitwiseNot) + _valueExpression;

            _valueExpression.Rule = _number
                                    | _text
                                    | _binary
                                    | (ToTerm("(") + _query + ")")
                                    | _variableName
                                    | _systemVariableName
                                    | aggregate
                                    | _elementFullName
                                    | _callExpression
                                    | ("(" + _valueExpression + ")")
                                    | nullExpression
                                    | castExpression
                                    | unaryValueExpression
                                    | _caseWhen
                                    | convertExpression
                                    | inlineIf
                                    | coalesceExpression
                                    | binaryExpression;
            var inNonTerminal = new NonTerminal("In", CreateInNode);
            inNonTerminal.Rule = _valueExpression
                                 + ((_not + _inTerm) | _inTerm)
                                 + "("
                                 + ((new AstNodeCreatingTerm(
                                         (x, y) =>
                                         {
                                         })
                                     + _valueExpressionList)
                                    | _select)
                                 + ")";
            var isNullTerminal = new NonTerminal("IsNull", CreateIsNullNode);
            isNullTerminal.Rule = (_valueExpression + PreferShiftHere() + _is + _not + _null)
                                  | (_valueExpression + PreferShiftHere() + _is + _null);

            _logicalExpression.Rule = (_valueExpression
                                       + (_equals
                                          | _greaterThan
                                          | _lessThan
                                          | _greaterThanOrEqual
                                          | _lessThenOrEqual
                                          | _notEqualToIso
                                          | _notEqualTo
                                          | _notLessThen
                                          | _notGreaterThen)
                                       + _valueExpression)
                                      | (_logicalExpression + _and + _logicalExpression)
                                      | (_logicalExpression + _or + _logicalExpression)
                                      | ("(" + _logicalExpression + ")")
                                      | (_updateTerm + "(" + _elementFullName + ")") ////only in trigger
                                      | _betweenExpression
                                      | (_not + _logicalExpression)
                                      | isNullTerminal
                                      | (_existsTerm + "(" + _select + ")")
                                      | inNonTerminal;

            _expression.Rule = _valueExpression
                               | _logicalExpression;
            _betweenExpression.Rule = _valueExpression + (_not | Empty) + _betweenTerm + _valueExpression + _and + _valueExpression;
            _assignOperator = new NonTerminal(AssignOperatorNonTerminalName);
            MarkTransient(_assignOperator);
            _assignOperator.Rule = _assign
                                   | _addAssign
                                   | _substractAssign
                                   | _multiplyAssign
                                   | _divideAssign
                                   | _moduloAssign
                                   | _bitwiseAndAssign
                                   | _bitwiseOrAssign
                                   | _bitwiseExclusiveOrAssign;
            _assignment.Rule = _variableName + _assignOperator + _valueExpression;

            RegisterOperators(10, _multiply, _divide, _modulo);
            RegisterOperators(9, _add, _subtract);
            RegisterOperators(8, _equals, _greaterThan, _lessThan, _greaterThanOrEqual, _lessThenOrEqual);
            RegisterOperators(8, _notEqualToIso, _notEqualTo, _notLessThen, _notGreaterThen, _inTerm);
            RegisterOperators(7, _bitwiseExclusiveOr, _bitwiseAnd, _bitwiseOr);
            RegisterOperators(6, _not);
            RegisterOperators(5, _and);
            RegisterOperators(4, _or);
        }

        private void ProcessCaseWhen()
        {
            _whenSimple = new NonTerminal(WhenSimpleNonTerminalName, CreateWhenSimpleNode);
            _whenSimple.Rule = _whenTerm + _valueExpression + _thenTerm + _valueExpression;

            _whenSearched = new NonTerminal(WhenSearchedNonTerminalName, CreateWhenSearchedNode);
            _whenSearched.Rule = PreferShiftHere() + _whenTerm + _logicalExpression + _thenTerm + _valueExpression;

            _whenList = new NonTerminal(WhenSimpleListNonTerminalName, CreateListNode<SqlWhenThen>);
            _whenList.Rule = _whenSimple
                             | (_whenList + _whenSimple);

            _whenSearchedList = new NonTerminal(WhenSearchedListNonTerminalName, CreateListNode<SqlLogicalWhenThen>);
            _whenSearchedList.Rule = _whenSearched
                                     | (_whenSearchedList + _whenSearched);

            _caseWhen.Rule = (_caseTerm + _valueExpression + _whenList + _endTerm)
                             | (_caseTerm + _valueExpression + _whenList + PreferShiftHere() + _elseTerm + _valueExpression + _endTerm)
                             | (_caseTerm + _whenSearchedList)
                             | (_caseTerm + _whenSearchedList + PreferShiftHere() + _elseTerm + _valueExpression + _endTerm);
        }

        private void ProcessCodeBlock()
        {
            _blockCommand = new NonTerminal(BlockCommandNonTerminalName, CreateBlockCommandNode);
            _blockCommand.Rule = (_query + (Empty | (PreferShiftHere() + ";")))
                                 | (_executeCommand + (Empty | (PreferShiftHere() + ";")))
                                 | (_scriptConfigurationCommand + (Empty | (PreferShiftHere() + ";")))
                                 | (_variablesDeclarationStatement + (Empty | (PreferShiftHere() + ";")))
                                 | (_variablesAssignmentStatement + (Empty | (PreferShiftHere() + ";")))
                                 | (_returnTerm + _valueExpression + (Empty | (PreferShiftHere() + ";")))
                                 | (_returnTerm + (Empty | (PreferShiftHere() + ";")));

            _blockCommandList = new NonTerminal(BlockCommandListNonTerminalName);
            _blockCommandList.Rule = (_beginTerm + _codeBlock)
                                     | (_blockCommandList + _codeBlock);

            _codeBlock.Rule = (_blockCommandList + _endTerm + (Empty | (PreferShiftHere() + ";")))
                              | _blockCommand;
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void ProcessColumnsDeclaration()
        {
            _orderedColumn = new NonTerminal(OrderedColumnNonTerminalName, CreateOrderedColumnNode);
            _orderedColumn.Rule = _identifier + (_ascendingTerm | _descencingTerm | Empty);
            _orderedColumnsList = new NonTerminal(OrderedColumnsListNonTerminalName, CreateListNode<OrderedColumnReference>);
            _orderedColumnsList.Rule = _orderedColumn | (_orderedColumnsList + "," + _orderedColumn);
            var foreignKeyActionKind = new NonTerminal("ForeignKeyActionKind", CreateForeignKeyActionKindNode);
            foreignKeyActionKind.Rule = (_noTerm + _actionTerm) | _cascadeTerm | (_setTerm + _null) | (_setTerm + _defaultTerm);
            var foreignKeyAction = new NonTerminal("ForeignKeyAction", CreateForeignKeyActionNode);
            foreignKeyAction.Rule = _onTerm + ((new TransientNode() + _deleteTerm) | _updateTerm) + foreignKeyActionKind;
            _foreignKeyActions = new NonTerminal(ForeignKeyActionsNonTerminalName, CreateListNode<ForeignKeyAction>);
            _foreignKeyActions.Rule = foreignKeyAction
                                      | (_foreignKeyActions + foreignKeyAction);

            _computedColumnForeignKeyActions = new NonTerminal(ComputedColumnForeignKeyActionsNonTerminalName);
            _computedColumnForeignKeyActions.Rule = (_onTerm + _deleteTerm + ((_noTerm + _actionTerm) | _cascadeTerm))
                                                    | (_onTerm + _updateTerm + _noTerm + _actionTerm)
                                                    | (_computedColumnForeignKeyActions
                                                       + ((_onTerm + _deleteTerm + ((_noTerm + _actionTerm) | _cascadeTerm))
                                                          | (_onTerm + _updateTerm + _noTerm + _actionTerm)));

            _uniqueIndexDeclaration = new NonTerminal(UniqueIndexDeclarationNonTerminalName, CreateUniqueIndexDeclarationNode);
            _uniqueIndexDeclaration.Rule = ((_primaryTerm + _keyTerm) | _uniqueTerm)
                                           + (_clusteredTerm | _nonclusteredTerm | Empty)
                                           + ((new TransientNode() + PreferShiftHere() + ToTerm("(") + _orderedColumnsList + ")") | Empty);

            _columnDefaultValueDeclaration = new NonTerminal(ColumnDefaultValueNonTerminalName, CreateColumnDefaultValueNode);
            _columnDefaultValueDeclaration.Rule = _defaultTerm + _valueExpression;

            _columnForeignKeyDeclaration = new NonTerminal(ColumnForeignKeyDeclartionNonTerminalName);
            _columnForeignKeyDeclaration.Rule = ((_foreignTerm + _keyTerm) | Empty)
                                                + _referencesTerm
                                                + _elementFullName
                                                + ((PreferShiftHere() + ToTerm("(") + _identifier + ")") | Empty)
                                                + (Empty | _foreignKeyActions);

            _computedColumnForeignKeyDeclaration = new NonTerminal(ComputedColumnForeignKeyDeclartionNonTerminalName);
            _computedColumnForeignKeyDeclaration.Rule = ((_foreignTerm + _keyTerm) | Empty)
                                                        + _referencesTerm
                                                        + _elementFullName
                                                        + ((PreferShiftHere() + ToTerm("(") + _identifier + ")") | Empty)
                                                        + (Empty | _computedColumnForeignKeyActions);

            _columnCheckDeclaration = new NonTerminal(ColumnCheckDeclarationNonTerminalName, CreateCheckDeclarationNode);
            _columnCheckDeclaration.Rule = _checkTerm + "(" + _logicalExpression + ")";

            _columnConstraint = new NonTerminal(ColumnConstraintNonTerminalName, CreateColumnConstraintNode);
            _columnConstraint.Rule = _null
                                     | (_not + _null)
                                     | (_identityTerm + (Empty | ("(" + _number + "," + _number + ")")))
                                     | (((_constraintTerm + _identifier) | Empty)
                                        + ((new TransientNode()
                                            + _columnDefaultValueDeclaration)
                                           | _uniqueIndexDeclaration
                                           | _columnForeignKeyDeclaration
                                           | _columnCheckDeclaration));

            _computedColumnConstraint = new NonTerminal(ComputedColumnConstraintNonTerminalName, CreateComputedColumnConstraintNode);
            _computedColumnConstraint.Rule = (_persistedTerm + ((_not + _null) | Empty))
                                             | (((_constraintTerm + _identifier) | Empty)
                                                + ((new TransientNode()
                                                    + _uniqueIndexDeclaration)
                                                   | _computedColumnForeignKeyDeclaration
                                                   | _columnCheckDeclaration));

            _columnConstraintList = new NonTerminal(ColumnConstraintListNonTerminalName, CreateListNode<IColumnConstraint>);
            _columnConstraintList.Rule = _columnConstraint
                                         | (_columnConstraintList + _columnConstraint);

            _computedColumnConstraintList = new NonTerminal(ComputedColumnConstraintListNonTerminalName, CreateListNode<IColumnConstraint>);
            _computedColumnConstraintList.Rule = _computedColumnConstraint
                                                 | (_computedColumnConstraintList + _computedColumnConstraint);

            _columnDeclaration = new NonTerminal(ColumnDeclarationNonTerminalName, CreateColumnDeclarationNode);
            _columnDeclaration.Rule = _identifier + _typeName + ((new TransientNode() + _columnConstraintList) | Empty);

            _computedColumnDeclaration = new NonTerminal(ComputedColumnDeclarationNonTerminalName, CreateComputedColumnDeclarationNode);
            _computedColumnDeclaration.Rule = _identifier + _asTerm + _valueExpression + ((new TransientNode() + _computedColumnConstraintList) | Empty);
            _tableConstraint = new NonTerminal(TableConstraintNonTerminalName, CreeateTableConstraintNode);

            var tableUniqueIndexDeclaration = new NonTerminal("TablePrimaryKeyDeclaration", CreateTableUniqueIndexNode);
            tableUniqueIndexDeclaration.Rule = ((_primaryTerm + _keyTerm) | _uniqueTerm) + (_clusteredTerm | _nonclusteredTerm | Empty) + "(" + _orderedColumnsList + ")";
            var tableForeignKeyDeclaration = new NonTerminal("TableForeignKeyDeclaration", CreateTableForeignKeyDeclaration);
            tableForeignKeyDeclaration.Rule = _foreignTerm
                                              + _keyTerm
                                              + "("
                                              + _identifierList
                                              + ")"
                                              + _referencesTerm
                                              + _elementFullName
                                              + ((new TransientNode() + "(" + _identifierList + ")") | Empty)
                                              + ((new TransientNode() + Empty) | _foreignKeyActions);
            var tableCheckDeclaration = new NonTerminal("TableCheckDeclaration", CreateCheckDeclarationNode);
            tableCheckDeclaration.Rule = _checkTerm + "(" + _logicalExpression + ")";

            _tableConstraint.Rule = ((_constraintTerm + _identifier) | Empty)
                                    + ((new TransientNode() + tableUniqueIndexDeclaration)
                                       | tableForeignKeyDeclaration
                                       | tableCheckDeclaration);

            _tableComponent = new NonTerminal(TableComponentNonTerminalName);
            _tableComponent.Rule = _columnDeclaration | _computedColumnDeclaration | _tableConstraint;
            MarkTransient(_tableComponent);
            _tableComponentsList = new NonTerminal(TableComponentsListNonTerminalName, CreateTableComponentsListNode);
            _tableComponentsList.Rule = _columnDeclaration
                                        | (_tableComponentsList + "," + _tableComponent);

            _tableDeclaration = new NonTerminal(TableDeclarationNonTerminalName);
            _tableDeclaration.Rule = _tableTerm + "(" + _tableComponentsList + ")";
        }

        private void ProcessDelete()
        {
            _delete = new NonTerminal(DeleteNonTerminalName);
            _delete.Rule = _deleteTerm + _elementFullName + ((_whereTerm + _logicalExpression) | Empty);
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void ProcessExecute()
        {
            _executionArgumentsList = new NonTerminal(ExecutionArgumentsListNonTerminalName);
            _executionArgumentsList.Rule = _valueExpression
                                           | (_variableName + _assign + _valueExpression)
                                           | (_executionArgumentsList + "," + (_valueExpression | (_variableName + _assign + _valueExpression)));
            _executeCommand = new NonTerminal(ExecuteCommandNonTerminalName);
            _executeCommand.Rule = ((_executeTerm | _executeFullTerm) + _elementFullName + _executionArgumentsList)
                                   | ((_executeTerm | _executeFullTerm) + _executeSqlTerm + _expression + ((ToTerm(",") + _expression + "," + _executionArgumentsList) | Empty));
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void ProcessFunction()
        {
            ////todo: _blockCommandList is inadequet as it allows inserts, deletes
            _function = new NonTerminal(FunctionNonTerminalName);
            _function.Rule = ((_createTerm | _alterTerm)
                              + _functionTerm
                              + _elementFullName
                              + "("
                              + (_parametersDeclarationList | Empty)
                              + ")"
                              + ((_returnsTerm
                                  + _tableTerm
                                  + ((_withTerm + (_encriptionTerm | _schemabindingTerm)) | Empty)
                                  + _asTerm
                                  + ((_returnTerm + _select + PreferShiftHere()) | (_blockCommandList + _endTerm)))
                                 | (_returnsTerm
                                    + (_typeName | (_variableName + _tableDeclaration))
                                    + ((_withTerm + (_encriptionTerm | _schemabindingTerm)) | Empty)
                                    + _asTerm
                                    + _blockCommandList
                                    + _endTerm)))
                             | (_dropTerm + _functionTerm + _elementFullName);
        }

        private void ProcessIf()
        {
            _if = new NonTerminal(IfNonTerminalName);
            _if.Rule = (_ifTerm + _logicalExpression + _codeBlock)
                       | (_ifTerm + _logicalExpression + _codeBlock + PreferShiftHere() + _elseTerm + _codeBlock);
        }

        private void ProcessIndex()
        {
            _createIndex = new NonTerminal(CreateIndexNonTerminalName, CreateCreateIndexNode);
            _createIndex.Rule = _createTerm
                                + (_uniqueTerm | Empty)
                                + (_clusteredTerm | _nonclusteredTerm | Empty)
                                + _indexTerm
                                + _identifier
                                + _onTerm
                                + _elementFullName
                                + "("
                                + _orderedColumnsList
                                + ")"
                                + ((new AstNodeCreatingTerm(
                                        (c, p) => p.AstNode = p.ChildNodes.Count == 2 ? p.ChildNodes[1].AstNode : null)
                                    + _whereTerm
                                    + _logicalExpression)
                                   | Empty);
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void ProcessInsert()
        {
            _insertValues = new NonTerminal(InsertValuesNonTerminalName);
            _insertValues.Rule = (_valuesTerm + "(" + _valueExpressionList + ")")
                                 | (_insertValues + "," + "(" + _valueExpressionList + ")");

            _insert = new NonTerminal(InsertNonTerminlName);
            _insert.Rule =
                _insertTerm
                + (_intoTerm | Empty)
                + (_variableName | _elementFullName)
                + ((PreferShiftHere() + ToTerm("(") + _identifierList + ")" + (_select | (ToTerm("(") + _select + ")") | _insertValues | _executeCommand))
                   | (_select | _insertValues | _executeCommand));
        }

        private void ProcessLogicalBinaryExpression(ParseTreeNode parsenode)
        {
            var left = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
            var right = (ISqlValueExpression)parsenode.ChildNodes[2].AstNode;
            if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _equals))
            {
                parsenode.AstNode = new EqualLogicalExpression(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _greaterThan))
            {
                parsenode.AstNode = new GreaterThanLogicalExpression(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _lessThan))
            {
                parsenode.AstNode = new LessThanLogicalExpression(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _notEqualToIso))
            {
                parsenode.AstNode = new NotEqualIsoLogicalExpression(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _notEqualTo))
            {
                parsenode.AstNode = new NotEqualLogicalExpression(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _notLessThen))
            {
                parsenode.AstNode = new NotLessThanLogicalExpression(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _equals))
            {
                parsenode.AstNode = new EqualLogicalExpression(left, right);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void ProcessMerge()
        {
            _mergeOperation = new NonTerminal(MergeOperationNonTerminalName);
            _mergeOperation.Rule =
                (_whenTerm + _matchedTerm + ((_and + _logicalExpression) | Empty) + _thenTerm + ((_updateTerm + _setTerm + _columnAssignmentList) | _deleteTerm))
                | (_whenTerm + _not + _matchedTerm + _byTerm + _sourceTerm + ((_and + _logicalExpression) | Empty) + _thenTerm + ((_updateTerm + _setTerm + _columnAssignmentList) | _deleteTerm))
                | (_whenTerm
                   + _not
                   + _matchedTerm
                   + ((_byTerm + _targetTerm) | Empty)
                   + ((_and + _logicalExpression) | Empty)
                   + _thenTerm
                   + _insertTerm
                   + ((ToTerm("(") + _identifierList + ")") | Empty)
                   + (_columnAssignmentList | _insertValues | (_defaultTerm + _valuesTerm)));
            _mergeOperationsList = new NonTerminal(MergeOperationsListNonTerminalName);
            _mergeOperationsList.Rule = _mergeOperation
                                        | (_mergeOperationsList + _mergeOperation);

            _merge = new NonTerminal(MergeNonTerminalName);
            _merge.Rule = (_with | Empty)
                          + _mergeTerm
                          + (_top | Empty)
                          + (_intoTerm | Empty)
                          + _elementFullName
                          + (((_asTerm | Empty) + _identifier) | Empty)
                          + _usingTerm
                          + _selectionSource
                          + _onTerm
                          + _logicalExpression
                          + _mergeOperationsList
                          + ";";
        }

        private void ProcessNamedElement()
        {
            _aliasedElement = new NonTerminal(AliasedElementNonTerminalName, CreateAliasedElementNode);
            _aliasedElement.Rule = (_valueExpression + ((PreferShiftHere() + _asTerm) | Empty) + _identifier)
                                   | _valueExpression;

            var sourceItem = new NonTerminal("SourceItem", CreateSourceItemNode);
            sourceItem.Rule = _elementFullName | _callExpression | (ToTerm("(") + _select + ")");

            _aliasedSource = new NonTerminal(AliasedSourceNonTerminalName, CreateAliasedSourceNode);
            _aliasedSource.Rule = (sourceItem
                                   + ((PreferShiftHere() + _asTerm) | Empty)
                                   + _identifier
                                   + ((new AstNodeCreatingTerm(
                                           (z, y) =>
                                           {
                                           })
                                       + "("
                                       + _identifierList
                                       + ")")
                                      | Empty))
                                  | sourceItem;
        }

        private void ProcessProcedure()
        {
            _procedure = new NonTerminal(ProcedureNonTerminalName);
            _procedure.Rule = ((_createTerm | _alterTerm) + _procedureTerm + _elementFullName + "(" + (_parametersDeclarationList | Empty) + ")" + _asTerm + _codeBlock)
                              | (_dropTerm + _procedureTerm + _elementFullName);
        }

        private void ProcessSchema()
        {
            _createSchema = new NonTerminal(CreateSchemaNonTerminalName, CreateCreateSchemaNode);
            _createSchema.Rule = _createTerm
                                 + _scheamTerm
                                 + _identifier
                                 + ((new AstNodeCreatingTerm((c, p) => p.AstNode = p.ChildNodes.Count == 2 ? p.ChildNodes[1].AstNode : null)
                                     + _authorizationTerm
                                     + _elementFullName)
                                    | Empty);
        }

        private void ProcessScript()
        {
            _scriptElement = new NonTerminal(ScriptElementNonTerminalName);
            _scriptElement.Rule = _codeBlock
                                  | _createTable
                                  | _alterTable
                                  | _procedure
                                  | _function
                                  | _trigger
                                  | _createIndex
                                  | _createSchema
                                  | _type
                                  | ";";
            MarkTransient(_scriptElement);
            _script = new NonTerminal(ScriptNonTerminalName, CreateScriptNode);
            _script.Rule = _scriptElement
                           | (_script + _scriptElement);
        }

        private void ProcessScriptConfiguration()
        {
            _scriptConfigurationCommand = new NonTerminal(ScriptConfigurationCommandNonTerminalName);
            _scriptConfigurationCommand.Rule = (_setTerm + _identifier + _onTerm)
                                               | (_setTerm + _identifier + _offTerm)
                                               | (_setTerm + _identityInsertTerm + _elementFullName + (_offTerm | _onTerm))
                                               | _goTerm
                                               | (_useTerm + _identifier);
        }

        [SuppressMessage(
            "Microsoft.Globalization",
            "CA1303:Do not pass literals as localized parameters",
            MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
            Justification = "This is grammar there are no localizable elements here")]
        private void ProcessSelect()
        {
            var orderedValueExpression = new NonTerminal("OrderedValueExpression", CreateOrderedValueExpressionNode);
            orderedValueExpression.Rule = _valueExpression + (Empty | _ascendingTerm | _descencingTerm);

            _orderedValueExpressionList.Rule = orderedValueExpression
                                               | (_orderedValueExpressionList + "," + orderedValueExpression);
            _orderBy.Rule = _orderTerm + _byTerm + _orderedValueExpressionList;

            _rowNumber = new NonTerminal(RowNumberNonTerminalName, CreateRowNumberNode);
            _rowNumber.Rule = _rowNumberTerm
                              + "("
                              + ")"
                              + _overTerm
                              + "("
                              + ((new AstNodeCreatingTerm(
                                      (x, y) =>
                                      {
                                      })
                                  + _partitionTerm
                                  + _byTerm
                                  + _valueExpressionList)
                                 | Empty)
                              + _orderBy
                              + ")";

            var aliasByAssignment = new NonTerminal("AlliasByAssignment", CreateAliasByAssignmentNode);
            aliasByAssignment.Rule = _identifier + _assignOperator + _valueExpression;
            var selectionListItem = new NonTerminal("SelectionListItem", CreateSelectionListItemNode);
            selectionListItem.Rule = _aliasedElement
                                     | aliasByAssignment
                                     | _assignment
                                     | _rowNumber
                                     | (_elementFullName + "." + _asteriskTerm)
                                     | _asteriskTerm;

            _selectionList = new NonTerminal(SelectionListNonTerminalName, CreateListNode<ISelectionItem>);
            _selectionList.Rule = selectionListItem
                                  | (_selectionList + "," + selectionListItem);

            _selectionSource = new NonTerminal(SelectionSourceNonTerminalName, CreateSelectionSourceNode);
            _selectionSource.Rule = _aliasedSource
                                    | (_selectionSource + "," + _aliasedSource)
                                    | (_selectionSource + _crossTerm + _joinTerm + _aliasedSource)
                                    | (_selectionSource + (_innerTerm | Empty) + _joinTerm + _aliasedSource + _onTerm + _logicalExpression)
                                    | (_selectionSource + _leftTerm + (_outerTerm | Empty) + _joinTerm + _aliasedSource + _onTerm + _logicalExpression)
                                    | (_selectionSource + _rightTerm + (_outerTerm | Empty) + _joinTerm + _aliasedSource + _onTerm + _logicalExpression)
                                    | (_selectionSource + _fullTerm + (_outerTerm | Empty) + _joinTerm + _aliasedSource + _onTerm + _logicalExpression)
                                    | (_selectionSource + _crossTerm + _applyTerm + _aliasedSource)
                                    | (_selectionSource + _outerTerm + _applyTerm + _aliasedSource);

            _top = new NonTerminal(TopNonTerminalName);
            _top.Rule = _topTerm + (_number | ("(" + _valueExpression + ")")) + (_percentTerm | Empty);

            var selectonOrganization = new NonTerminal(
                "SelectionOrganization",
                (a, b) =>
                {
                });
            selectonOrganization.Rule = ((new AstNodeCreatingTerm(
                                              (x, y) =>
                                              {
                                              })
                                          + _whereTerm
                                          + _logicalExpression)
                                         | Empty)
                                        + ((new AstNodeCreatingTerm(
                                                (x, y) =>
                                                {
                                                })
                                            + _groupTerm
                                            + _byTerm
                                            + _identifierList
                                            + ((new AstNodeCreatingTerm(
                                                    (x, y) =>
                                                    {
                                                    })
                                                + _havingTerm
                                                + _logicalExpression)
                                               | Empty))
                                           | Empty);

            _selectionBody = new NonTerminal(SelectionBodyNonTerminalName, CreateSelectionBodyNode);
            _selectionBody.Rule =
                _selectTerm
                + (_distinctTerm | Empty)
                + (_top | Empty)
                + ((_withTerm + _tiesTerm) | Empty)
                + _selectionList
                + ((new AstNodeCreatingTerm(
                        (x, y) =>
                        {
                        })
                    + _fromTerm
                    + _selectionSource
                    + selectonOrganization
                    + ((new AstNodeCreatingTerm(
                            (x, y) =>
                            {
                            })
                        + _orderBy)
                       | Empty))
                   | Empty);

            _with = new NonTerminal(WithNonTerminalName);
            _with.Rule = (_withTerm + _identifier + ((ToTerm("(") + _identifierList + ")") | Empty) + _asTerm + "(" + _select + ")")
                         | (_with + "," + _identifier + ((ToTerm("(") + _identifierList + ")") | Empty) + _asTerm + "(" + _select + ")");

            // todo: mising operators
            _select.Rule =
                _selectionBody
                | (_with + _selectionBody)
                | (_select + _unionTerm + _selectionBody)
                | (_select + _unionTerm + _allTerm + _selectionBody);
        }

        private void ProcessTable()
        {
            _createTable = new NonTerminal(CreateTableNonTerminalName, CreateCreateTableNode);
            _createTable.Rule = _createTerm + _tableTerm + _elementFullName + "(" + _tableComponentsList + ")";

            var alterTableComponentsList = new NonTerminal("AlterTableComponentsList", CreateTableComponentsListNode);
            alterTableComponentsList.Rule = _tableComponent
                                            | (_tableComponentsList + "," + _tableComponent);
            _alterTable = new NonTerminal(AlterTableNonTerminalName, CreateAlterTableNode);
            _alterTable.Rule = _alterTerm + _tableTerm + _elementFullName + _addTerm + alterTableComponentsList;
        }

        private void ProcessThrow()
        {
            _throw = new NonTerminal(ThrowNonTerminalName);
            _throw.Rule = _throwTerm + _number + "," + _text + "," + _number;
            _raiseError = new NonTerminal(RaiseErrorNonTerminalName);
            _raiseError.Rule = _raiseErrorTerm + "(" + _text + "," + _number + "," + _number + ")";
        }

        private void ProcessTransaction()
        {
            _transaction = new NonTerminal(TransactionNonTerminalName);
            _transaction.Rule = (_beginTerm + (_transactionTerm | _transactionFullTerm))
                                | (_commitTerm + (_transactionTerm | _transactionFullTerm | Empty))
                                | (_rollbackTerm + (_transactionTerm | _transactionFullTerm));
        }

        private void ProcessTrigger()
        {
            _triggerOperationList = new NonTerminal(TriggerOperationList);
            _triggerOperationList.Rule = _insertTerm
                                         | _updateTerm
                                         | _deleteTerm
                                         | (_triggerOperationList + "," + (_insertTerm | _updateTerm | _deleteTerm));
            _trigger = new NonTerminal(TriggerNonTerminalName);
            _trigger.Rule = (_createTerm | _alterTerm)
                            + _triggerTerm
                            + _elementFullName
                            + _onTerm
                            + _elementFullName
                            + (Empty | (_withTerm + (_encriptionTerm | Empty) + (((_executeTerm | _executeFullTerm) + _asTerm + (_callerTerm | _selfTerm | _text)) | Empty)))
                            + (_forTerm | _afterTerm | (_insteadTerm + _ofTerm))
                            + _triggerOperationList
                            + _asTerm
                            + _codeBlock;
        }

        private void ProcessTypeName()
        {
            _typeName.Rule = _elementFullName
                             | (_identifier + "(" + _number + ")")
                             | (_identifier + "(" + _maxTerm + ")")
                             | (_identifier + "(" + _number + "," + _number + ")");
        }

        private void ProcessUpdate()
        {
            _columnAssignmentList.Rule = (((_variableName + _assign) | Empty) + _elementFullName + _assignOperator + _valueExpression)
                                         | (_columnAssignmentList + "," + ((_variableName + _assign) | Empty) + _elementFullName + _assignOperator + _valueExpression);

            _update = new NonTerminal(UpdateNonTerminalName);
            _update.Rule = _updateTerm + _elementFullName + _setTerm + _columnAssignmentList + _fromTerm + _selectionSource + ((_whereTerm + _logicalExpression) | Empty);
        }

        private void ProcessUserDefinedType()
        {
            _type = new NonTerminal(UserDefinedTypeNonTerminalName);
            _type.Rule = _createTerm + _typeTerm + _elementFullName + _asTerm + _tableDeclaration;
        }

        private void ProcessVariableManipulation()
        {
            _parameterDeclaration = new NonTerminal(ParameterDeclarationNonTerminalName);
            _parameterDeclaration.Rule = (_variableName + _typeName)
                                         | (_variableName + _typeName + _assign + (_number | _binary | _text | _defaultTerm))
                                         | (_variableName + _typeName + _readonlyTerm);

            _parametersDeclarationList = new NonTerminal(ParametersDeclarationListNonTerminalName);
            _parametersDeclarationList.Rule = _parameterDeclaration
                                              | (_parametersDeclarationList + "," + _parameterDeclaration);

            _variableDeclaration = new NonTerminal(VariableDeclarationNonTerminalName);
            _variableDeclaration.Rule = (_variableName + _typeName)
                                        | (_variableName + _typeName + _assign + _expression);

            _variablesDeclarationList = new NonTerminal(VariablesDeclarationListNonTerminalName);
            _variablesDeclarationList.Rule = _variableDeclaration
                                             | (_variablesDeclarationList + "," + _variableDeclaration);

            _variablesDeclarationStatement = new NonTerminal(VariablesDeclarationStatementNonTerminalName);
            _variablesDeclarationStatement.Rule = _declareTerm + _variablesDeclarationList;

            _variablesAssignmentStatement = new NonTerminal(VariablesAssignmentStatementNonTerminalName);
            _variablesAssignmentStatement.Rule = _setTerm + _variableName + _assignOperator + _valueExpression;
        }
    }
}