using System;
using System.Collections.Generic;
using Irony;
using Irony.Ast;
using Irony.Parsing;

namespace Qdarc.Parsers.Tsql
{
    public class BinaryLiteral : CompoundTerminalBase
    {
        public BinaryLiteral(string name, AstNodeCreator astNodeCreator)
            : base(name)
        {
            AstConfig.NodeCreator = astNodeCreator;
        }

        public override IList<string> GetFirsts()
        {
            StringList result = new StringList();
            result.Add("0x");
            return result;
        }

        public override void Init(GrammarData grammarData)
        {
            base.Init(grammarData);
            if (EditorInfo == null)
            {
                EditorInfo = new TokenEditorInfo(TokenType.Literal, TokenColor.Number, TokenTriggers.None);
            }
        }

        protected override bool ConvertValue(CompoundTokenDetails details)
        {
            if (string.IsNullOrEmpty(details.Body))
            {
                details.Error = Resources.ErrInvNumber;
                return false;
            }

            var data = details.Body.Substring(2);
            if (data.Length % 2 == 1)
            {
                data = "0" + data;
            }

            var converted = new byte[data.Length / 2];
            for (int i = 0; i < data.Length; i += 2)
            {
                var number = data.Substring(i, 2);
                converted[i / 2] = (byte)Convert.ToInt32(number, 16);
            }

            details.Value = converted;
            return true;
        }

        protected override bool ReadBody(ISourceStream source, CompoundTokenDetails details)
        {
            if (source.NextPreviewChar != 'x')
            {
                return false;
            }

            int start = source.PreviewPosition;
            string digits = Strings.HexDigits;

            bool foundDigits = false;

            source.PreviewPosition += 2;
            while (!source.EOF())
            {
                var current = source.PreviewChar;
                if (digits.IndexOf(current) >= 0)
                {
                    source.PreviewPosition++;
                    foundDigits = true;
                    continue;
                }

                break;
            }

            int end = source.PreviewPosition;
            if (!foundDigits)
            {
                return false;
            }

            details.Body = source.Text.Substring(start, end - start);
            return true;
        }
    }
}