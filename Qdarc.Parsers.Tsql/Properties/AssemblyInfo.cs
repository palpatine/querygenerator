﻿using System.Runtime.CompilerServices;

#if DEBUG
[assembly: InternalsVisibleTo("Qdarc.Parsers.Tsql.Tests")]
#endif