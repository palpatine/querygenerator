using System.Collections.Generic;
using System.Linq;
using Irony;
using Irony.Parsing;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql
{
    public class SystemVariableTerminal : Terminal
    {
        private char[] _stopChars;

        public SystemVariableTerminal()
            : base("SystemVariable")
        {
            Priority = TerminalPriority.High;
            AstConfig.NodeCreator = (x, y) =>
            {
                y.AstNode = new SqlVariable(y.Token.ValueString, true, null, null);
            };
        }

        private char[] StopChars
        {
            get
            {
                if (_stopChars == null)
                {
                    CreateStopCharacters();
                }

                return _stopChars;
            }
        }

        public override IList<string> GetFirsts()
        {
            return new[] { "@" };
        }

        public override Token TryMatch(ParsingContext context, ISourceStream source)
        {
            if (source.Text.Length <= source.Location.Position + 1 || source.Text[source.Location.Position + 1] != '@')
            {
                return null;
            }

            var stopIndex = source.Text.IndexOfAny(StopChars, source.Location.Position + 2);
            if (stopIndex == source.Location.Position)
            {
                return null;
            }

            if (stopIndex < 0)
            {
                stopIndex = source.Text.Length;
            }

            source.PreviewPosition = stopIndex;
            return source.CreateToken(OutputTerminal);
        }

        private void CreateStopCharacters()
        {
            var stopCharSet = new CharHashSet();
            foreach (var white in new[] { '\r', '\t', ' ', '\n', ')', ',' })
            {
                stopCharSet.Add(white);
            }

            foreach (var term in GrammarData.Terminals.Where(x => (x.Flags & (TermFlags.IsOperator | TermFlags.IsPunctuation)) != TermFlags.None))
            {
                var firsts = term.GetFirsts();
                if (firsts == null)
                {
                    continue;
                }

                foreach (var first in firsts)
                {
                    if (!string.IsNullOrEmpty(first))
                    {
                        stopCharSet.Add(first[0]);
                    }
                }
            }

            _stopChars = stopCharSet.Where(x => !char.IsLetterOrDigit(x)).ToArray();
        }
    }
}