﻿using System.Collections.Generic;
using System.Linq;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlIdentifiersHandler : ISqlGrammarElementHandler
    {
        private const string ElementFullNameNonTerminalName = "ElementFullName";
        private const string IdentifierListNonTerminalName = "IdentifierList";
        private const string IdentifierTerminalName = "Identifier";
        private const string TypeNameNonTerminalName = "TypeName";
        private readonly StringLiteral _identifierTerm;
        private readonly ISqlGrammarHandlersProvider _provider;

        public SqlIdentifiersHandler(ISqlGrammarHandlersProvider provider)
        {
            _provider = provider;
            ElementFullName = new NonTerminal(ElementFullNameNonTerminalName, CreateElmentFullNameNode);
            Identifier = new IdentifierTerminal(IdentifierTerminalName);
            Identifier.Flags = TermFlags.NoAstNode;
            _identifierTerm = new StringLiteral(IdentifierTerminalName + "_qouted");
            _identifierTerm.AddStartEnd("[", "]", StringOptions.NoEscapes);
            IdentifierList = new NonTerminal(IdentifierListNonTerminalName, CreateIdentifierListNode);
            TypeName = new NonTerminal(TypeNameNonTerminalName, CreateTypeNameNode);
        }

        public NonTerminal ElementFullName { get; }

        public IdentifierTerminal Identifier { get; }

        public NonTerminal IdentifierList { get; }

        public NonTerminal TypeName { get; }

        public void Initialize(Grammar grammar)
        {
            var sqlLiteralsHandler = _provider.GetHandler<SqlLiteralsHandler>();
            _identifierTerm.SetOutputTerminal(grammar, Identifier);
            ElementFullName.Rule = Identifier
                                    | ElementFullName + _provider.Terms.Dot + Identifier;
            IdentifierList.Rule = Identifier
                                   | IdentifierList + _provider.Terms.Comma + Identifier;

            TypeName.Rule = ElementFullName
                             | Identifier + _provider.Terms.OpenBrace + sqlLiteralsHandler.Number + _provider.Terms.CloseBrace
                             | Identifier + _provider.Terms.OpenBrace + _provider.Terms.MaxTerm + _provider.Terms.CloseBrace
                             | Identifier + _provider.Terms.OpenBrace + sqlLiteralsHandler.Number + _provider.Terms.Comma + sqlLiteralsHandler.Number + _provider.Terms.CloseBrace;
        }

        private void CreateElmentFullNameNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].AstNode is ElementName currentName)
            {
                currentName.AddPart(parsenode.ChildNodes[1].Token.Value.ToString());

                parsenode.AstNode = currentName;
            }
            else
            {
                parsenode.AstNode = new ElementName(parsenode.ChildNodes[0].Token.Value.ToString());
            }
        }

        private void CreateIdentifierListNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].AstNode is List<string> list)
            {
                var newList = new List<string>(list);
                newList.Add(parsenode.ChildNodes[1].Token.ValueString);
                parsenode.AstNode = newList;
                return;
            }

            parsenode.AstNode = new List<string> { parsenode.ChildNodes[0].Token.ValueString };
        }

        private void CreateTypeNameNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1)
            {
                var name = (ElementName)parsenode.ChildNodes.Single().AstNode;
                if (name.Count == 1)
                {
                    var normalizedName = TsqlNativeTypesNames.NormalizeName(name[0]);
                    if (normalizedName != null)
                    {
                        parsenode.AstNode = new TsqlType(normalizedName);
                    }
                    else
                    {
                        parsenode.AstNode = new CustomTypeReference(name);
                    }
                }
                else
                {
                    parsenode.AstNode = new CustomTypeReference(name);
                }
            }
            else if (parsenode.ChildNodes.Count == 2)
            {
                var name = parsenode.ChildNodes[0].Token.Value.ToString();
                name = TsqlNativeTypesNames.NormalizeName(name);
                var capacity = ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.MaxTerm) ? -1 : (int)parsenode.ChildNodes[1].Token.Value;
                parsenode.AstNode = new TsqlType(name, capacity);
            }
        }
    }
}