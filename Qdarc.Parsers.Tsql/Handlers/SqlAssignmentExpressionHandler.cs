﻿using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlAssignmentExpressionHandler : ISqlGrammarElementHandler
    {
        private const string AssignmentNonTerminalName = "Assignment";
        private const string AssignOperatorNonTerminalName = "AssignOperator";
        private readonly ISqlGrammarHandlersProvider _provider;

        public SqlAssignmentExpressionHandler(ISqlGrammarHandlersProvider provider)
        {
            _provider = provider;
            Assignment = new NonTerminal(AssignmentNonTerminalName, CreateAssignmetnNode);
            AssignOperator = new NonTerminal(AssignOperatorNonTerminalName);
        }

        public NonTerminal Assignment { get; }

        public NonTerminal AssignOperator { get; }

        public void Initialize(Grammar grammar)
        {
            grammar.MarkTransient(AssignOperator);
            AssignOperator.Rule = _provider.Terms.Assign
                                   | _provider.Terms.AddAssign
                                   | _provider.Terms.SubstractAssign
                                   | _provider.Terms.MultiplyAssign
                                   | _provider.Terms.DivideAssign
                                   | _provider.Terms.ModuloAssign
                                   | _provider.Terms.BitwiseAndAssign
                                   | _provider.Terms.BitwiseOrAssign
                                   | _provider.Terms.BitwiseExclusiveOrAssign;
            SqlLiteralsHandler sqlLiteralsHandler = _provider.GetHandler<SqlLiteralsHandler>();
            SqlValueExpressionsHandler sqlValueExpressionsHandler = _provider.GetHandler<SqlValueExpressionsHandler>();
            Assignment.Rule = sqlLiteralsHandler.VariableName + AssignOperator + sqlValueExpressionsHandler.ValueExpression;
        }

        private void CreateAssignmetnNode(AstContext context, ParseTreeNode parsenode)
        {
            var left = (SqlVariable)parsenode.ChildNodes[0].AstNode;
            var right = (ISqlValueExpression)parsenode.ChildNodes[2].AstNode;

            if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.Assign))
            {
                parsenode.AstNode = new Assign(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.AddAssign))
            {
                parsenode.AstNode = new AddAssign(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.SubstractAssign))
            {
                parsenode.AstNode = new SubtractAssign(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.MultiplyAssign))
            {
                parsenode.AstNode = new MultiplyAssign(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.DivideAssign))
            {
                parsenode.AstNode = new DivideAssign(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.ModuloAssign))
            {
                parsenode.AstNode = new ModuloAssign(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.BitwiseAndAssign))
            {
                parsenode.AstNode = new BitwiseAdd(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.BitwiseOrAssign))
            {
                parsenode.AstNode = new BitwiseOr(left, right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.BitwiseExclusiveOrAssign))
            {
                parsenode.AstNode = new BitwiseExclusiveOr(left, right);
            }
        }
    }
}