﻿using System.Collections.Generic;
using System.Linq;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlAggregateExpressionsHandler : ISqlGrammarElementHandler
    {
        private const string AggregateNonTerminalName = "Aggregate";
        private const string CountNonTerminalName = "CountExpression";
        private readonly ISqlGrammarHandlersProvider _provider;

        public SqlAggregateExpressionsHandler(ISqlGrammarHandlersProvider provider)
        {
            _provider = provider;
            Count = new NonTerminal(CountNonTerminalName, CreateCountExpressionNode);
            Aggregate = new NonTerminal(AggregateNonTerminalName);
        }

        public NonTerminal Aggregate { get; }

        public NonTerminal Count { get; }

        public void Initialize(Grammar grammar)
        {
            var valueExpressionsHandler = _provider.GetHandler<SqlValueExpressionsHandler>();
            var orderByHandler = _provider.GetHandler<SqlOrderByExpressionHandler>();

            // Count does not accept subqery and aggregate functions inside, but as far as grammar is
            // concerned it does this must be verified while creating ast node.
            Count.Rule = (_provider.Terms.CountTerm | _provider.Terms.CountBigTerm)
                + _provider.Terms.OpenBrace + (new AstNodeCreatingTerm((c, n) => { }) + (_provider.Terms.AllTerm | _provider.Terms.DistinctTerm | grammar.Empty)
                + valueExpressionsHandler.ValueExpression | _provider.Terms.AsteriskTerm) + _provider.Terms.CloseBrace
                + (new AstNodeCreatingTerm((c, n) => { }) + grammar.Empty | _provider.Terms.OverTerm + _provider.Terms.OpenBrace
                    + (new AstNodeCreatingTerm((c, n) => { }) + _provider.Terms.PartitionTerm + _provider.Terms.ByTerm + valueExpressionsHandler.ValueExpressionList | grammar.Empty)
                    + (new AstNodeCreatingTerm((c, n) => { }) + orderByHandler.OrderBy | grammar.Empty) + _provider.Terms.CloseBrace);

            grammar.MarkTransient(Aggregate);
            Aggregate.Rule = Count;
        }

        private void CreateCountExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            IEnumerable<OrderedValueExpression> order = null;
            IEnumerable<ISqlValueExpression> partition = null;

            if (parsenode.ChildNodes[2].ChildNodes.Any())
            {
                if (parsenode.ChildNodes[2].ChildNodes[2].ChildNodes.Any())
                {
                    order =
                        (IEnumerable<OrderedValueExpression>)parsenode.ChildNodes[2].ChildNodes[2].ChildNodes[0].AstNode;
                }

                if (parsenode.ChildNodes[2].ChildNodes[1].ChildNodes.Any())
                {
                    partition =
                       (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes[2].ChildNodes[1].ChildNodes[2].AstNode;
                }
            }

            // todo: verify if valueExpression contains subquery or aggregate function and add an error
            if (parsenode.ChildNodes[1].ChildNodes.Count == 2)
            {
                var isDistinct = parsenode.ChildNodes[1].ChildNodes[0].ChildNodes.Any() &&
                                 ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].ChildNodes[0].Term, _provider.Terms.DistinctTerm);
                var valueExpression = (ISqlValueExpression)parsenode.ChildNodes[1].ChildNodes[1].AstNode;

                if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _provider.Terms.CountTerm))
                {
                    parsenode.AstNode = new SqlCountExpression(
                        valueExpression,
                        isDistinct,
                        typeof(int),
                        TsqlTypeProvider.Int)
                        {
                            Order = order,
                            Partition = partition
                        };
                }
                else
                {
                    parsenode.AstNode = new SqlCountBigExpression(
                        valueExpression,
                        isDistinct,
                        typeof(int),
                        TsqlTypeProvider.Int)
                        {
                            Order = order,
                            Partition = partition
                        };
                }
            }
            else
            {
                if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _provider.Terms.CountTerm))
                {
                    parsenode.AstNode = new SqlAsteriskCountExpression(
                        typeof(int),
                        TsqlTypeProvider.Int)
                    {
                        Order = order,
                        Partition = partition
                    };
                }
                else
                {
                    parsenode.AstNode = new SqlAsteriskCountBigExpression(
                        typeof(int),
                        TsqlTypeProvider.Int)
                    {
                        Order = order,
                        Partition = partition
                    };
                }
            }
        }
    }
}