using System;
using Irony;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Tsql;
using TypeCode = Irony.TypeCode;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlLiteralsHandler : ISqlGrammarElementHandler
    {
        private const string BinaryLiteralName = "Binary";
        private const string NumberLiteralName = "Number";
        private const string TextLiteralName = "String";

        public SqlLiteralsHandler()
        {
            VariableName = new VariableTerminal();
            SystemVariableName = new SystemVariableTerminal();
            Number = new NumberLiteral(NumberLiteralName, NumberOptions.AllowSign, NumberNodeCreator);
            Number.DefaultIntTypes = new[] { TypeCode.Int32, TypeCode.Int64, TypeCode.Decimal };
            Number.DefaultFloatType = TypeCode.Double;
            Number.AddExponentSymbols("eE", TypeCode.Double);
            Binary = new BinaryLiteral(BinaryLiteralName, BinaryNodeCreator);
            Text = new StringLiteral(
                TextLiteralName,
                "\'",
                StringOptions.AllowsDoubledQuote | StringOptions.AllowsLineBreak,
                StringNodeCreator);
            Text.AddPrefix("N", StringOptions.None);
        }

        public BinaryLiteral Binary { get; }

        public NumberLiteral Number { get; }

        public SystemVariableTerminal SystemVariableName { get; }

        public StringLiteral Text { get; }

        public VariableTerminal VariableName { get; }

        public void Initialize(Grammar grammar)
        {
        }

        private static void NumberNodeCreator(AstContext context, ParseTreeNode parseNode)
        {
            ISqlType type;
            var clrType = parseNode.Token.Value.GetType();
            var value = parseNode.Token.Value;

            var indexOfDot = parseNode.Token.ValueString.IndexOf(".", StringComparison.Ordinal);
            if (indexOfDot == -1)
            {
                var intValue = Convert.ToDecimal(value);
                if (intValue > TsqlNativeTypesNames.IntMinValue && intValue < TsqlNativeTypesNames.IntMaxValue)
                {
                    type = new TsqlType(TsqlNativeTypesNames.Int);
                }
                else
                {
                    type = new TsqlType(TsqlNativeTypesNames.Numeric, (byte)parseNode.Token.ValueString.Length, 0);
                }
            }
            else
            {
                var leadingZeroCount = 0;
                for (var i = 0; i < indexOfDot; i++)
                {
                    if (parseNode.Token.ValueString[i] == '0')
                    {
                        leadingZeroCount++;
                    }
                    else
                    {
                        break;
                    }
                }

                var precision = parseNode.Token.ValueString.Length - 1 - leadingZeroCount;
                var scale = parseNode.Token.ValueString.Length - indexOfDot - 1;
                type = new TsqlType(TsqlNativeTypesNames.Numeric, (byte)precision, (byte)scale);
            }

            parseNode.AstNode = new SqlConstantExpression(
                value,
                clrType,
                type);
        }

        private void BinaryNodeCreator(AstContext context, ParseTreeNode parseNode)
        {
            var clrType = parseNode.Token.Value.GetType();
            var value = parseNode.Token.Value;
            var type = new TsqlType(TsqlNativeTypesNames.VarBinary, ((byte[])value).Length);

            parseNode.AstNode = new SqlConstantExpression(
                value,
                clrType,
                type);
        }

        private void StringNodeCreator(AstContext context, ParseTreeNode parseNode)
        {
            ISqlType type;
            var clrType = parseNode.Token.Value.GetType();
            var value = parseNode.Token.Value;

            if (parseNode.Token.Text.StartsWith("N", StringComparison.Ordinal))
            {
                type = new TsqlType(TsqlNativeTypesNames.NVarChar, parseNode.Token.ValueString.Length);
            }
            else
            {
                type = new TsqlType(TsqlNativeTypesNames.VarChar, parseNode.Token.ValueString.Length);
            }

            parseNode.AstNode = new SqlConstantExpression(
                value,
                clrType,
                type);
        }
    }
}