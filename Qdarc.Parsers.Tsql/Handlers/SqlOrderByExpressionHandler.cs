﻿using System.Linq;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model.Expressions;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlOrderByExpressionHandler : ISqlGrammarElementHandler
    {
        private const string OrderByNonTerminalName = "OrderBy";
        private const string OrderedValueExpressionListNonTerminalName = "OrderedValueExpressionList";
        private const string OrderedValueExpressionNonTerminalName = "OrderedValueExpression";
        private readonly ISqlGrammarHandlersProvider _provider;

        public SqlOrderByExpressionHandler(ISqlGrammarHandlersProvider provider)
        {
            _provider = provider;
            OrderBy = new NonTerminal(OrderByNonTerminalName, CreateOrderByNode);
        }

        public NonTerminal OrderBy { get; }

        public void Initialize(Grammar grammar)
        {
            var valueExpressionsHandler = _provider.GetHandler<SqlValueExpressionsHandler>();
            var orderedValueExpression = new NonTerminal(OrderedValueExpressionNonTerminalName, CreateOrderedValueExpressionNode);
            orderedValueExpression.Rule = valueExpressionsHandler.ValueExpression + (grammar.Empty | _provider.Terms.AscendingTerm | _provider.Terms.DescencingTerm);

            var orderedValueExpressionList = new NonTerminal(OrderedValueExpressionListNonTerminalName, AstNodeCreationHelper.CreateListNode<OrderedValueExpression>);
            orderedValueExpressionList.Rule = orderedValueExpression
                                            | orderedValueExpressionList + _provider.Terms.Comma + orderedValueExpression;
            OrderBy.Rule = _provider.Terms.OrderTerm + _provider.Terms.ByTerm + orderedValueExpressionList;
        }

        private void CreateOrderByNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = parsenode.ChildNodes[2].AstNode;
        }

        private void CreateOrderedValueExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var @ascending = !parsenode.ChildNodes[1].ChildNodes.Any() || ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _provider.Terms.AscendingTerm);
            parsenode.AstNode = new OrderedValueExpression(
                (ISqlValueExpression)parsenode.ChildNodes[0].AstNode,
                @ascending);
        }
    }
}