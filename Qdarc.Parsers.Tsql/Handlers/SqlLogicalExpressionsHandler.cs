﻿using System;
using System.Collections.Generic;
using System.Linq;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Selections;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlLogicalExpressionsHandler : ISqlGrammarElementHandler
    {
        private const string BetweenExpressionNonTerminalName = "BetweenExpression";
        private const string InExpressionNonTerminalName = "InExpression";
        private const string IsNullExpressionNonTerminalName = "IsNullExpression";
        private const string LogicalExpressionNonTerminalName = "LogicalExpression";
        private readonly ISqlGrammarHandlersProvider _provider;

        public SqlLogicalExpressionsHandler(ISqlGrammarHandlersProvider provider)
        {
            _provider = provider;
            LogicalExpression = new NonTerminal(LogicalExpressionNonTerminalName, CreateLogicalExpressionNode);
        }

        public NonTerminal LogicalExpression { get; }

        public void Initialize(Grammar grammar)
        {
            var sqlValueExpressionHandler = _provider.GetHandler<SqlValueExpressionsHandler>();
            var sqlIdentifiersHandler = _provider.GetHandler<SqlIdentifiersHandler>();
            var selectHandler = _provider.GetHandler<SqlSelectExpressionHandler>();
            var betweenExpression = new NonTerminal(BetweenExpressionNonTerminalName, CreateBetweenExpressionNode);
            betweenExpression.Rule = sqlValueExpressionHandler.ValueExpression + (_provider.Terms.Not | grammar.Empty)
                                        + _provider.Terms.BetweenTerm + sqlValueExpressionHandler.ValueExpression + _provider.Terms.And
                                        + sqlValueExpressionHandler.ValueExpression;
            var isNullExpression = new NonTerminal(IsNullExpressionNonTerminalName, CreateIsNullNode);
            isNullExpression.Rule = sqlValueExpressionHandler.ValueExpression + new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.Is + _provider.Terms.Not + _provider.Terms.Null
                                  | sqlValueExpressionHandler.ValueExpression + new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.Is + _provider.Terms.Null;
            var inExpression = new NonTerminal(InExpressionNonTerminalName, CreateInNode);
            inExpression.Rule = sqlValueExpressionHandler.ValueExpression + (_provider.Terms.Not + _provider.Terms.InTerm | _provider.Terms.InTerm)
                + _provider.Terms.OpenBrace + (new AstNodeCreatingTerm((x, y) => { }) + sqlValueExpressionHandler.ValueExpressionList | selectHandler.Select) + _provider.Terms.CloseBrace;

            LogicalExpression.Rule = sqlValueExpressionHandler.ValueExpression + (new TransientNode() + _provider.Terms.EqualsTerm
                                                        | _provider.Terms.GreaterThan
                                                        | _provider.Terms.LessThan
                                                        | _provider.Terms.GreaterThanOrEqual
                                                        | _provider.Terms.LessThenOrEqual
                                                        | _provider.Terms.NotEqualToIso
                                                        | _provider.Terms.NotEqualTo
                                                        | _provider.Terms.NotLessThen
                                                        | _provider.Terms.NotGreaterThen)
                                    + sqlValueExpressionHandler.ValueExpression
                                    | LogicalExpression + _provider.Terms.Or + LogicalExpression
                                    | LogicalExpression + _provider.Terms.And + LogicalExpression
                                    | _provider.Terms.OpenBrace + LogicalExpression + _provider.Terms.CloseBrace
                                    | _provider.Terms.UpdateTerm + _provider.Terms.OpenBrace + sqlIdentifiersHandler.ElementFullName + _provider.Terms.CloseBrace // todo: only in trigger
                                    | betweenExpression
                                    | _provider.Terms.Not + LogicalExpression
                                    | isNullExpression
                                    | _provider.Terms.ExistsTerm + _provider.Terms.OpenBrace + selectHandler.Select + _provider.Terms.CloseBrace
                                    | inExpression;
        }

        private void CreateBetweenExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var value = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
            var lowerBound = (ISqlValueExpression)parsenode.ChildNodes[3].AstNode;
            var upperBound = (ISqlValueExpression)parsenode.ChildNodes[5].AstNode;
            var isNegated = parsenode.ChildNodes[1].ChildNodes.Any() && ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _provider.Terms.Not);
            parsenode.AstNode = new BetweenLogicalExpression(
                value,
                lowerBound,
                upperBound,
                isNegated);
        }

        private void CreateInNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[2].ChildNodes[0].AstNode is IEnumerable<ISqlValueExpression> set)
            {
                parsenode.AstNode = new SqlInLogicalExpression(
                    (ISqlValueExpression)parsenode.ChildNodes[0].AstNode,
                    parsenode.ChildNodes[1].ChildNodes.Count == 2,
                    set);
            }
            else
            {
                var command = (ISelectCommand)parsenode.ChildNodes[2].ChildNodes[0].AstNode;
                parsenode.AstNode = new SqlInLogicalExpression(
                    (ISqlValueExpression)parsenode.ChildNodes[0].AstNode,
                    parsenode.ChildNodes[1].ChildNodes.Count == 2,
                    command);
            }
        }

        private void CreateIsNullNode(AstContext context, ParseTreeNode parsenode)
        {
            var valueExpression = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
            var isNegated = ReferenceEquals(parsenode.ChildNodes[2].Term, _provider.Terms.Not);
            var expression = new SqlIsNullLogicalExpression(
                valueExpression,
                isNegated);
            parsenode.AstNode = expression;
        }

        private void CreateLogicalExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 3)
            {
                if (ReferenceEquals(parsenode.ChildNodes[0].Term, _provider.Terms.Not)
                    && ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.ExistsTerm))
                {
                    parsenode.AstNode = new ExistsExpression(
                    (ISelectCommand)parsenode.ChildNodes[1].AstNode,
                    true);
                }

                ProcessLogicalBinaryExpression(parsenode);
            }
            else if (parsenode.ChildNodes.Count == 2)
            {
                if (ReferenceEquals(parsenode.ChildNodes[0].Term, _provider.Terms.ExistsTerm))
                {
                    parsenode.AstNode = new ExistsExpression(
                    (ISelectCommand)parsenode.ChildNodes[1].AstNode,
                    false);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[0].Term, _provider.Terms.Not))
                {
                    parsenode.AstNode = new SqlLogicalNegateExpression(
                        (ISqlLogicalExpression)parsenode.ChildNodes[1].AstNode);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            else if (parsenode.ChildNodes.Count == 1)
            {
                if (parsenode.ChildNodes[0].AstNode is ISqlLogicalExpression logicalExpression)
                {
                    parsenode.AstNode = logicalExpression;
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private void ProcessLogicalBinaryExpression(ParseTreeNode parsenode)
        {
            if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.And))
            {
                var left = (ISqlLogicalExpression)parsenode.ChildNodes[0].AstNode;
                var right = (ISqlLogicalExpression)parsenode.ChildNodes[2].AstNode;
                parsenode.AstNode = new AndLogicalConjunctionExpression(
                    left,
                    right);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.Or))
            {
                var left = (ISqlLogicalExpression)parsenode.ChildNodes[0].AstNode;
                var right = (ISqlLogicalExpression)parsenode.ChildNodes[2].AstNode;
                parsenode.AstNode = new OrLogicalConjunctionExpression(
                    left,
                    right);
            }
            else
            {
                var left = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
                var right = (ISqlValueExpression)parsenode.ChildNodes[2].AstNode;
                if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.EqualsTerm))
                {
                    parsenode.AstNode = new EqualLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.GreaterThan))
                {
                    parsenode.AstNode = new GreaterThanLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.GreaterThanOrEqual))
                {
                    parsenode.AstNode = new GreaterThanOrEqualLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.LessThan))
                {
                    parsenode.AstNode = new LessThanLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.LessThenOrEqual))
                {
                    parsenode.AstNode = new LessThanOrEqualLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.NotEqualToIso))
                {
                    parsenode.AstNode = new NotEqualIsoLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.NotEqualTo))
                {
                    parsenode.AstNode = new NotEqualLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.NotLessThen))
                {
                    parsenode.AstNode = new NotLessThanLogicalExpression(left, right);
                }
                else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.NotGreaterThen))
                {
                    parsenode.AstNode = new NotGreaterThanLogicalExpression(left, right);
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}