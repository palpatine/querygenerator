namespace Qdarc.Parsers.Tsql.Handlers.Helpers
{
    internal interface ISqlGrammarHandlersProvider
    {
        SqlTermsHandler Terms { get; }

        THandler GetHandler<THandler>()
                    where THandler : class, ISqlGrammarElementHandler;
    }
}