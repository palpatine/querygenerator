using System;
using System.Collections.Generic;
using System.Linq;

namespace Qdarc.Parsers.Tsql.Handlers.Helpers
{
    internal class SqlGrammarHandlersProvider : ISqlGrammarHandlersProvider
    {
        private readonly Func<IEnumerable<ISqlGrammarElementHandler>> _handlersFactory;
        private Dictionary<Type, ISqlGrammarElementHandler> _handlers;
        private SqlTermsHandler _terms;

        public SqlGrammarHandlersProvider(
            Func<IEnumerable<ISqlGrammarElementHandler>> handlersFactory)
        {
            _handlersFactory = handlersFactory;
        }

        public SqlTermsHandler Terms
        {
            get
            {
                if (_terms == null)
                {
                    _terms = GetHandler<SqlTermsHandler>();
                }

                return _terms;
            }
        }

        private Dictionary<Type, ISqlGrammarElementHandler> Handlers => _handlers ?? (_handlers = _handlersFactory().ToDictionary(x => x.GetType(), x => x));

        public THandler GetHandler<THandler>()
            where THandler : class, ISqlGrammarElementHandler
        {
            return (THandler)Handlers[typeof(THandler)];
        }
    }
}