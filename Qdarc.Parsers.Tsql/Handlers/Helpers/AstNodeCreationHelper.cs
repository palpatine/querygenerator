﻿using System.Collections.Generic;
using Irony.Ast;
using Irony.Parsing;

namespace Qdarc.Parsers.Tsql.Handlers.Helpers
{
    internal static class AstNodeCreationHelper
    {
        public static void CreateListNode<TNode>(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].AstNode is List<TNode> list)
            {
                var newList = new List<TNode>(list);
                newList.Add((TNode)parsenode.ChildNodes[1].AstNode);
                parsenode.AstNode = newList;
                return;
            }

            parsenode.AstNode = new List<TNode> { (TNode)parsenode.ChildNodes[0].AstNode };
        }
    }
}