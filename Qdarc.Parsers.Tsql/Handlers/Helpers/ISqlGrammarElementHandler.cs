using Irony.Parsing;

namespace Qdarc.Parsers.Tsql.Handlers.Helpers
{
    internal interface ISqlGrammarElementHandler
    {
        void Initialize(Grammar grammar);
    }
}