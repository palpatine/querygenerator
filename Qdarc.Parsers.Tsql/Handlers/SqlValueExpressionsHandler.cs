﻿using System;
using System.Collections.Generic;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlValueExpressionsHandler : ISqlGrammarElementHandler
    {
        private const string BinaryExpressionNonTerminalName = "BinaryExpression";
        private const string CallExpressionNonTerminalName = "CallExpression";
        private const string CaseWhenNonTerminalName = "CaseWhen";
        private const string CastExpressionNonTerminalName = "CastExpression";
        private const string CoalesceExpressionNonTerminalName = "CoalesceExpression";
        private const string ConvertExpressionNonTerminalName = "ConvertExpression";
        private const string InlineIfExpressionNonTerminalName = "InlineIfExpression";
        private const string NullExpressionNonTerminalName = "NullExpression";
        private const string UnaryValueExpressionNonTerminalName = "UnaryValueExpression";
        private const string ValueExpressionListNonTerminalName = "ValueExpressionList";
        private const string ValueExpressionNonTerminalName = "ValueExpression";
        private const string WhenSearchedListNonTerminalName = "WhenSearchedList";
        private const string WhenSearchedNonTerminalName = "WhenSearched";
        private const string WhenSimpleListNonTerminalName = "WhenSimpleList";
        private const string WhenSimpleNonTerminalName = "WhenSimple";
        private readonly ISqlGrammarHandlersProvider _provider;

        public SqlValueExpressionsHandler(ISqlGrammarHandlersProvider provider)
        {
            _provider = provider;
            ValueExpression = new NonTerminal(ValueExpressionNonTerminalName, CreateValueExpressionNode);
            ValueExpressionList = new NonTerminal(ValueExpressionListNonTerminalName, AstNodeCreationHelper.CreateListNode<ISqlValueExpression>);
            CallExpression = new NonTerminal(CallExpressionNonTerminalName, CreateCallExpressionNode);
            CaseWhen = new NonTerminal(CaseWhenNonTerminalName, CreateCaseWhenNode);
        }

        public NonTerminal CallExpression { get; private set; }

        public NonTerminal CaseWhen { get; }

        public NonTerminal ValueExpression { get; }

        public NonTerminal ValueExpressionList { get; }

        public void Initialize(Grammar grammar)
        {
            var sqlLiteralsHandler = _provider.GetHandler<SqlLiteralsHandler>();
            var sqlIdentifiersHandler = _provider.GetHandler<SqlIdentifiersHandler>();
            var aggregateFunctionsHandler = _provider.GetHandler<SqlAggregateExpressionsHandler>();
            var logicalExpressionHandler = _provider.GetHandler<SqlLogicalExpressionsHandler>();

            var nullExpression = new NonTerminal(NullExpressionNonTerminalName, (x, y) => y.AstNode = new SqlNullExpression(typeof(object), null));
            nullExpression.Rule = _provider.Terms.Null;

            CallExpression.Rule = sqlIdentifiersHandler.ElementFullName + new PreferredActionHint(PreferredActionType.Shift)
                + _provider.Terms.OpenBrace + (new TransientNode() + ValueExpressionList | grammar.Empty) + _provider.Terms.CloseBrace;
            var castExpression = new NonTerminal(CastExpressionNonTerminalName, CreateCastExpressionNode);
            castExpression.Rule = _provider.Terms.CastTerm + _provider.Terms.OpenBrace + ValueExpression + _provider.Terms.AsTerm + sqlIdentifiersHandler.TypeName + _provider.Terms.CloseBrace;
            var unaryValueExpression = new NonTerminal(UnaryValueExpressionNonTerminalName, CreateUnaryValueExpression);
            unaryValueExpression.Rule = (_provider.Terms.Add | _provider.Terms.Subtract | _provider.Terms.BitwiseNot) + ValueExpression;
            var convertExpression = new NonTerminal(ConvertExpressionNonTerminalName, CreateConvertNode);
            convertExpression.Rule = _provider.Terms.Convert + _provider.Terms.OpenBrace + sqlIdentifiersHandler.TypeName + _provider.Terms.Comma + ValueExpression + _provider.Terms.CloseBrace;
            var inlineIf = new NonTerminal(InlineIfExpressionNonTerminalName, CreateInlineIfNode);
            inlineIf.Rule = _provider.Terms.InlineIfTerm + _provider.Terms.OpenBrace + logicalExpressionHandler.LogicalExpression
                + _provider.Terms.Comma + ValueExpression + _provider.Terms.Comma + ValueExpression + _provider.Terms.CloseBrace;
            var coalesceExpression = new NonTerminal(CoalesceExpressionNonTerminalName, CreateCoalesceExpressionNode);
            coalesceExpression.Rule = _provider.Terms.Coalesce + _provider.Terms.OpenBrace + ValueExpressionList + _provider.Terms.CloseBrace;
            var binaryExpression = new NonTerminal(BinaryExpressionNonTerminalName, CreateBinaryExpressionNode);
            binaryExpression.Rule = ValueExpression + (new TransientNode() + _provider.Terms.Add
                                                        | _provider.Terms.Subtract
                                                        | _provider.Terms.Multiply
                                                        | _provider.Terms.Divide
                                                        | _provider.Terms.Modulo
                                                        | _provider.Terms.BitwiseAnd
                                                        | _provider.Terms.BitwiseOr
                                                        | _provider.Terms.BitwiseExclusiveOr) + ValueExpression;

            ValueExpression.Rule = sqlLiteralsHandler.Number
                                         | sqlLiteralsHandler.Text
                                         | sqlLiteralsHandler.Binary
                                         | sqlLiteralsHandler.VariableName
                                         | sqlLiteralsHandler.SystemVariableName
                                         | sqlIdentifiersHandler.ElementFullName
                                         | CallExpression
                                         | _provider.Terms.OpenBrace + ValueExpression + new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.CloseBrace
                                         | nullExpression
                                         | castExpression
                                         | unaryValueExpression
                                         | CaseWhen
                                         | convertExpression
                                         | inlineIf
                                         | coalesceExpression
                                         | binaryExpression
                | aggregateFunctionsHandler.Aggregate

                // todo: | ToTerm("(") + _query + ")"
                ;

            ValueExpressionList.Rule = ValueExpression
                                        | ValueExpressionList + _provider.Terms.Comma + ValueExpression;

            var whenSimple = new NonTerminal(WhenSimpleNonTerminalName, CreateWhenSimpleNode);
            whenSimple.Rule = _provider.Terms.WhenTerm + ValueExpression + _provider.Terms.ThenTerm + ValueExpression;

            var whenSearched = new NonTerminal(WhenSearchedNonTerminalName, CreateWhenSearchedNode);
            whenSearched.Rule = new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.WhenTerm + logicalExpressionHandler.LogicalExpression + _provider.Terms.ThenTerm + ValueExpression;

            var whenList = new NonTerminal(WhenSimpleListNonTerminalName, AstNodeCreationHelper.CreateListNode<SqlWhenThen>);
            whenList.Rule = whenSimple
                        | whenList + whenSimple;

            var whenSearchedList = new NonTerminal(WhenSearchedListNonTerminalName, AstNodeCreationHelper.CreateListNode<SqlLogicalWhenThen>);
            whenSearchedList.Rule = whenSearched
                        | whenSearchedList + whenSearched;

            CaseWhen.Rule = _provider.Terms.CaseTerm + ValueExpression + whenList + _provider.Terms.EndTerm
                       | _provider.Terms.CaseTerm + ValueExpression + whenList + new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.ElseTerm + ValueExpression + _provider.Terms.EndTerm
                       | _provider.Terms.CaseTerm + whenSearchedList
                       | _provider.Terms.CaseTerm + whenSearchedList + new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.ElseTerm + ValueExpression + _provider.Terms.EndTerm;
        }

        private void CreateBinaryExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var left = (ISqlValueExpression)parsenode.ChildNodes[0].AstNode;
            var right = (ISqlValueExpression)parsenode.ChildNodes[2].AstNode;

            if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.Add))
            {
                parsenode.AstNode = new AddValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.Subtract))
            {
                parsenode.AstNode = new SubtractValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.Multiply))
            {
                parsenode.AstNode = new MultiplyValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.Divide))
            {
                parsenode.AstNode = new DivideValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.Modulo))
            {
                parsenode.AstNode = new ModuloValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.BitwiseAnd))
            {
                parsenode.AstNode = new BitwiseAndValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.BitwiseOr))
            {
                parsenode.AstNode = new BitwiseOrValueExpression(left, right, null, null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.BitwiseExclusiveOr))
            {
                parsenode.AstNode = new BitwiseExclusiveOrValueExpression(left, right, null, null);
            }
            else
            {
                throw new NotSupportedException();
            }
        }

        private void CreateCallExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            IEnumerable<ISqlValueExpression> arguments = null;

            if (parsenode.ChildNodes.Count == 2)
            {
                arguments = (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes?[1].AstNode;
            }

            var call = new SqlCallExpression(
                (ElementName)parsenode.ChildNodes[0].AstNode,
                arguments,
                null,
                null);
            parsenode.AstNode = call;
        }

        private void CreateCaseWhenNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[1].AstNode is ISqlValueExpression valueExpression)
            {
                ISqlValueExpression elseValue = null;

                if (parsenode.ChildNodes.Count == 6)
                {
                    elseValue = (ISqlValueExpression)parsenode.ChildNodes[4].AstNode;
                }

                var node = new SqlCaseSwitchExpression(
                    valueExpression,
                    (IEnumerable<SqlWhenThen>)parsenode.ChildNodes[2].AstNode,
                    elseValue,
                    null,
                    null);
                parsenode.AstNode = node;
            }
            else
            {
                ISqlValueExpression elseValue = null;
                if (parsenode.ChildNodes.Count == 5)
                {
                    elseValue = (ISqlValueExpression)parsenode.ChildNodes[3].AstNode;
                }

                var node = new SqlCaseExpression(
                    (IEnumerable<SqlLogicalWhenThen>)parsenode.ChildNodes[1].AstNode,
                    elseValue,
                    null,
                    null);
                parsenode.AstNode = node;
            }
        }

        private void CreateCastExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlCastExpression(
                (ISqlValueExpression)parsenode.ChildNodes[1].AstNode,
                null,
                (ISqlType)parsenode.ChildNodes[2].AstNode);
        }

        private void CreateCoalesceExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var expressions = (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes[1].AstNode;
            parsenode.AstNode = new SqlCoalesceExpression(
                expressions,
                null,
                null);
        }

        private void CreateConvertNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlConvertExpression(
                (ISqlValueExpression)parsenode.ChildNodes[2].AstNode,
                null,
                (ISqlType)parsenode.ChildNodes[1].AstNode);
        }

        private void CreateInlineIfNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlInlineIfExpression(
                (ISqlLogicalExpression)parsenode.ChildNodes[1].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[2].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[3].AstNode,
                null,
                null);
        }

        private void CreateUnaryValueExpression(AstContext context, ParseTreeNode parsenode)
        {
            var value = (ISqlValueExpression)parsenode.ChildNodes[1].AstNode;

            if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _provider.Terms.Add))
            {
                parsenode.AstNode = new SqlUnaryPlusExpression(value, value.ClrType, value.SqlType);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _provider.Terms.Subtract))
            {
                parsenode.AstNode = new SqlUnaryMinusExpression(value, value.ClrType, value.SqlType);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[0].ChildNodes[0].Term, _provider.Terms.BitwiseNot))
            {
                parsenode.AstNode = new NegateValueExpression(value, value.ClrType, value.SqlType);
            }
            else
            {
                throw new InvalidOperationException();
            }
        }

        private void CreateValueExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes[0].AstNode is ISqlValueExpression valueExpression)
            {
                parsenode.AstNode = valueExpression;
                return;
            }

            if (parsenode.ChildNodes[0].AstNode is ElementName elementName)
            {
                parsenode.AstNode = new SqlColumnReference(elementName, null, null, null);
                return;
            }

            throw new NotImplementedException();
        }

        private void CreateWhenSearchedNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlLogicalWhenThen(
                (ISqlLogicalExpression)parsenode.ChildNodes[1].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[3].AstNode);
        }

        private void CreateWhenSimpleNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new SqlWhenThen(
                (ISqlValueExpression)parsenode.ChildNodes[1].AstNode,
                (ISqlValueExpression)parsenode.ChildNodes[3].AstNode);
        }
    }
}