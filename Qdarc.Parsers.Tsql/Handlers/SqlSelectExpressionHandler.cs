﻿using System;
using System.Collections.Generic;
using System.Linq;
using Irony.Ast;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;
using Qdarc.Queries.Model.Commands;
using Qdarc.Queries.Model.Expressions;
using Qdarc.Queries.Model.Names;
using Qdarc.Queries.Model.References;
using Qdarc.Queries.Model.Selections;
using Qdarc.Queries.Model.Sources;
using Qdarc.Queries.Model.Tsql;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlSelectExpressionHandler : ISqlGrammarElementHandler
    {
        private const string AliasedElementNonTerminalName = "AliasedElement";
        private const string AliasedSourceNonTerminalName = "AliasedSource";
        private const string AlliasByAssignmentNonTerminalName = "AlliasByAssignment";
        private const string OrderedValueExpressionNonTerminalName = "OrderedValueExpression";
        private const string RowNumberNonTerminalName = "RowNumber";
        private const string SelectionBodyNonTerminalName = "SelectionBody";
        private const string SelectionListNonTerminalName = "SelectionList";
        private const string SelectionSourceNonTerminalName = "SelectionSource";
        private const string SelectNonTerminalName = "Select";
        private const string TopNonTerminalName = "Top";
        private const string WithNonTerminalName = "With";
        private readonly ISqlGrammarHandlersProvider _provider;
        private NonTerminal _selectionBody;
        private NonTerminal _selectionList;
        private NonTerminal _selectionSource;

        public SqlSelectExpressionHandler(ISqlGrammarHandlersProvider provider)
        {
            _provider = provider;
            Select = new NonTerminal(SelectNonTerminalName, CreateSelectNode);
        }

        public NonTerminal Select { get; }

        public void Initialize(Grammar grammar)
        {
            var sqlAssignemtnsHandler = _provider.GetHandler<SqlAssignmentExpressionHandler>();
            var sqlValueExpressionHandler = _provider.GetHandler<SqlValueExpressionsHandler>();
            var sqlLiteralsHandler = _provider.GetHandler<SqlLiteralsHandler>();
            var sqlOrderByHandler = _provider.GetHandler<SqlOrderByExpressionHandler>();
            var sqlIdentifierHandler = _provider.GetHandler<SqlIdentifiersHandler>();
            var sqlLogicalExpressionHandler = _provider.GetHandler<SqlLogicalExpressionsHandler>();
            var orderedValueExpression = new NonTerminal(OrderedValueExpressionNonTerminalName, CreateOrderedValueExpressionNode);
            orderedValueExpression.Rule = sqlValueExpressionHandler.ValueExpression + (grammar.Empty | _provider.Terms.AscendingTerm | _provider.Terms.DescencingTerm);

            var aliasedElement = new NonTerminal(AliasedElementNonTerminalName, CreateAliasedElementNode);
            aliasedElement.Rule = (sqlValueExpressionHandler.ValueExpression
                                   + ((new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.AsTerm) | grammar.Empty)
                                   + sqlIdentifierHandler.Identifier)
                                  | sqlValueExpressionHandler.ValueExpression;

            var rowNumber = new NonTerminal(RowNumberNonTerminalName, CreateRowNumberNode);
            rowNumber.Rule = _provider.Terms.RowNumberTerm
                             + _provider.Terms.OpenBrace
                             + _provider.Terms.CloseBrace
                             + _provider.Terms.OverTerm
                             + _provider.Terms.OpenBrace
                             + ((new AstNodeCreatingTerm(
                                     (x, y) =>
                                     {
                                     })
                                 + _provider.Terms.PartitionTerm
                                 + _provider.Terms.ByTerm
                                 + sqlValueExpressionHandler.ValueExpressionList)
                                | grammar.Empty)
                             + sqlOrderByHandler.OrderBy
                             + _provider.Terms.CloseBrace;

            var sourceItem = new NonTerminal("SourceItem", CreateSourceItemNode);
            sourceItem.Rule = sqlIdentifierHandler.ElementFullName | sqlValueExpressionHandler.CallExpression | (_provider.Terms.OpenBrace + Select + _provider.Terms.CloseBrace);

            var aliasedSource = new NonTerminal(AliasedSourceNonTerminalName, CreateAliasedSourceNode);
            aliasedSource.Rule = (sourceItem
                                  + ((new PreferredActionHint(PreferredActionType.Shift) + _provider.Terms.AsTerm) | grammar.Empty)
                                  + sqlIdentifierHandler.Identifier
                                  + ((new AstNodeCreatingTerm(
                                          (z, y) =>
                                          {
                                          })
                                      + _provider.Terms.OpenBrace
                                      + sqlIdentifierHandler.IdentifierList
                                      + _provider.Terms.CloseBrace)
                                     | grammar.Empty))
                                 | sourceItem;

            _selectionSource = new NonTerminal(SelectionSourceNonTerminalName, CreateSelectionSourceNode);
            _selectionSource.Rule = aliasedSource
                                    | (_selectionSource + "," + aliasedSource)
                                    | (_selectionSource + _provider.Terms.CrossTerm + _provider.Terms.JoinTerm + aliasedSource)
                                    | (_selectionSource
                                       + (_provider.Terms.InnerTerm | grammar.Empty)
                                       + _provider.Terms.JoinTerm
                                       + aliasedSource
                                       + _provider.Terms.OnTerm
                                       + sqlLogicalExpressionHandler.LogicalExpression)
                                    | (_selectionSource
                                       + _provider.Terms.LeftTerm
                                       + (_provider.Terms.OuterTerm | grammar.Empty)
                                       + _provider.Terms.JoinTerm
                                       + aliasedSource
                                       + _provider.Terms.OnTerm
                                       + sqlLogicalExpressionHandler.LogicalExpression)
                                    | (_selectionSource
                                       + _provider.Terms.RightTerm
                                       + (_provider.Terms.OuterTerm | grammar.Empty)
                                       + _provider.Terms.JoinTerm
                                       + aliasedSource
                                       + _provider.Terms.OnTerm
                                       + sqlLogicalExpressionHandler.LogicalExpression)
                                    | (_selectionSource
                                       + _provider.Terms.FullTerm
                                       + (_provider.Terms.OuterTerm | grammar.Empty)
                                       + _provider.Terms.JoinTerm
                                       + aliasedSource
                                       + _provider.Terms.OnTerm
                                       + sqlLogicalExpressionHandler.LogicalExpression)
                                    | (_selectionSource + _provider.Terms.CrossTerm + _provider.Terms.ApplyTerm + aliasedSource)
                                    | (_selectionSource + _provider.Terms.OuterTerm + _provider.Terms.ApplyTerm + aliasedSource);

            var top = new NonTerminal(TopNonTerminalName, CreateTopNode);
            top.Rule = _provider.Terms.TopTerm
                       + ((new AstNodeCreatingTerm(
                               (x, y) =>
                               {
                               })
                           + sqlLiteralsHandler.Number)
                          | (_provider.Terms.OpenBrace + sqlValueExpressionHandler.ValueExpression + _provider.Terms.CloseBrace))
                       + (_provider.Terms.PercentTerm | grammar.Empty)
                       + ((_provider.Terms.WithTerm + _provider.Terms.TiesTerm) | grammar.Empty);

            var grouping = new NonTerminal("Grouping", CreateGroupingNode);
            grouping.Rule = _provider.Terms.GroupTerm
                            + _provider.Terms.ByTerm
                            + sqlValueExpressionHandler.ValueExpressionList
                            + ((new AstNodeCreatingTerm(
                                    (x, y) =>
                                    {
                                    })
                                + _provider.Terms.HavingTerm
                                + sqlLogicalExpressionHandler.LogicalExpression)
                               | grammar.Empty);
            var selectonOrganization = new NonTerminal(
                "SelectionOrganization",
                (a, b) =>
                {
                });
            selectonOrganization.Rule = ((new AstNodeCreatingTerm(
                                              (x, y) =>
                                              {
                                              })
                                          + _provider.Terms.WhereTerm
                                          + sqlLogicalExpressionHandler.LogicalExpression)
                                         | grammar.Empty)
                                        + ((new AstNodeCreatingTerm(
                                                (x, y) =>
                                                {
                                                })
                                            + grouping)
                                           | grammar.Empty);

            var aliasByAssignment = new NonTerminal(AlliasByAssignmentNonTerminalName, CreateAliasByAssignmentNode);
            aliasByAssignment.Rule = sqlIdentifierHandler.Identifier + sqlAssignemtnsHandler.AssignOperator + sqlValueExpressionHandler.ValueExpression;
            var selectionListItem = new NonTerminal("SelectionListItem", CreateSelectionListItemNode);
            selectionListItem.Rule = aliasedElement
                                     | aliasByAssignment
                                     | sqlAssignemtnsHandler.Assignment
                                     | rowNumber
                                     | (sqlIdentifierHandler.ElementFullName + "." + _provider.Terms.AsteriskTerm)
                                     | _provider.Terms.AsteriskTerm;

            _selectionList = new NonTerminal(SelectionListNonTerminalName, AstNodeCreationHelper.CreateListNode<ISelectionItem>);
            _selectionList.Rule = selectionListItem
                                  | (_selectionList + "," + selectionListItem);

            _selectionBody = new NonTerminal(SelectionBodyNonTerminalName, CreateSelectionBodyNode);
            _selectionBody.Rule =
                _provider.Terms.SelectTerm
                + (_provider.Terms.DistinctTerm | grammar.Empty)
                + ((new AstNodeCreatingTerm(
                        (x, y) =>
                        {
                        })
                    + top)
                   | grammar.Empty)
                + _selectionList
                + ((new AstNodeCreatingTerm(
                        (x, y) =>
                        {
                        })
                    + _provider.Terms.FromTerm
                    + _selectionSource
                    + selectonOrganization
                    + ((new AstNodeCreatingTerm(
                            (x, y) =>
                            {
                            })
                        + sqlOrderByHandler.OrderBy)
                       | grammar.Empty))
                   | grammar.Empty);

            var with = new NonTerminal(WithNonTerminalName, CreateWithNode);
            with.Rule = _provider.Terms.WithTerm
                        + sqlIdentifierHandler.Identifier
                        + ((new AstNodeCreatingTerm(
                                (x, y) =>
                                {
                                })
                            + _provider.Terms.OpenBrace
                            + sqlIdentifierHandler.IdentifierList
                            + _provider.Terms.CloseBrace)
                           | grammar.Empty)
                        + _provider.Terms.AsTerm
                        + _provider.Terms.OpenBrace
                        + Select
                        + _provider.Terms.CloseBrace;

            var withList = new NonTerminal("WithList", AstNodeCreationHelper.CreateListNode<WithExpression>);
            withList.Rule = with | (withList + "," + with);

            // todo: mising operators
            Select.Rule =
                _selectionBody
                | (withList + _selectionBody)
                | (Select + _provider.Terms.UnionTerm + _selectionBody)
                | (Select + _provider.Terms.UnionTerm + _provider.Terms.AllTerm + _selectionBody);
        }

        private void CreateAliasByAssignmentNode(AstContext context, ParseTreeNode parsenode)
        {
            parsenode.AstNode = new AliasedSelectionItem((ISelectionItem)parsenode.ChildNodes[2].AstNode)
            {
                Alias = parsenode.ChildNodes[0].Token.ValueString
            };
        }

        private void CreateAliasedElementNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 3)
            {
                parsenode.AstNode = new AliasedSelectionItem((ISelectionItem)parsenode.ChildNodes[0].AstNode)
                {
                    Alias = parsenode.ChildNodes[2].Token.ValueString
                };
            }
            else
            {
                parsenode.AstNode = (ISelectionItem)parsenode.ChildNodes[0].AstNode;
            }
        }

        private void CreateAliasedSourceNode(AstContext context, ParseTreeNode parsenode)
        {
            var item = parsenode.ChildNodes[0].AstNode as ISource;

            if (parsenode.ChildNodes.Count == 4)
            {
                IEnumerable<ColumnAlias> columnAliases = null;
                if (parsenode.ChildNodes[3].ChildNodes.Count == 1)
                {
                    var columnAliasesNames = (IEnumerable<string>)parsenode.ChildNodes[3].ChildNodes[0].AstNode;
                    columnAliases = columnAliasesNames.Select(
                        x => new ColumnAlias(x, null, null));
                }

                var source = new AliasedSourceItem(item, columnAliases)
                {
                    Alias = parsenode.ChildNodes[2].Token.ValueString
                };

                parsenode.AstNode = source;
            }
            else
            {
                parsenode.AstNode = item;
            }
        }

        private void CreateGroupingNode(AstContext context, ParseTreeNode parsenode)
        {
            var values = (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes[2].AstNode;

            ISqlLogicalExpression filter = null;

            if (parsenode.ChildNodes[3].ChildNodes.Any())
            {
                filter = (ISqlLogicalExpression)parsenode.ChildNodes[3].ChildNodes[1].AstNode;
            }

            var group = new GroupExpression(values, filter);
            parsenode.AstNode = group;
        }

        private void CreateOrderedValueExpressionNode(AstContext context, ParseTreeNode parsenode)
        {
            var @ascending = !parsenode.ChildNodes[1].ChildNodes.Any() || ReferenceEquals(parsenode.ChildNodes[1].ChildNodes[0].Term, _provider.Terms.AscendingTerm);
            parsenode.AstNode = new OrderedValueExpression(
                (ISqlValueExpression)parsenode.ChildNodes[0].AstNode,
                @ascending);
        }

        private void CreateRowNumberNode(AstContext context, ParseTreeNode parsenode)
        {
            var orderedValueExpressions = (IEnumerable<OrderedValueExpression>)parsenode.ChildNodes[3].AstNode;

            IEnumerable<ISqlValueExpression> partition = null;
            if (parsenode.ChildNodes[2].ChildNodes.Any())
            {
                partition = (IEnumerable<ISqlValueExpression>)parsenode.ChildNodes[2].ChildNodes[2].AstNode;
            }

            var node = new RowNumberSelection(orderedValueExpressions, partition, typeof(long), TsqlTypeProvider.BigInt);
            parsenode.AstNode = node;
        }

        private void CreateSelectionBodyNode(AstContext context, ParseTreeNode parsenode)
        {
            ISource source = null;
            ISqlLogicalExpression filter = null;
            GroupExpression group = null;
            IEnumerable<OrderedValueExpression> order = null;

            if (parsenode.ChildNodes[4].ChildNodes.Any())
            {
                source = (ISource)parsenode.ChildNodes[4].ChildNodes[1].AstNode;
                var selectionOrganization = parsenode.ChildNodes[4].ChildNodes[2];
                if (selectionOrganization.ChildNodes[0].ChildNodes.Any())
                {
                    filter = (ISqlLogicalExpression)selectionOrganization.ChildNodes[0].ChildNodes[1].AstNode;
                }

                if (selectionOrganization.ChildNodes[1].ChildNodes.Any())
                {
                    group = (GroupExpression)selectionOrganization.ChildNodes[1].ChildNodes[0].AstNode;
                }

                if (parsenode.ChildNodes[4].ChildNodes[3].ChildNodes.Any())
                {
                    order = (IEnumerable<OrderedValueExpression>)parsenode.ChildNodes[4].ChildNodes[3].ChildNodes[0].AstNode;
                }
            }

            var node = new SelectCommand(
                source,
                isDistinct: parsenode.ChildNodes[1].ChildNodes.Count == 1,
                top:
                (TopExpression)(parsenode.ChildNodes[2].ChildNodes.Count == 1 ? parsenode.ChildNodes[2].ChildNodes[0].AstNode : null),
                selection: new SqlSelection((IEnumerable<ISelectionItem>)parsenode.ChildNodes[3].AstNode, null),
                filter: filter,
                group: group,
                order: order);
            parsenode.AstNode = node;
        }

        private void CreateSelectionListItemNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 2)
            {
                parsenode.AstNode = new AsteriskSelection(
                    new TableReference((ElementName)parsenode.ChildNodes[0].AstNode),
                    null,
                    null);
            }
            else if (ReferenceEquals(parsenode.ChildNodes[0].Term, _provider.Terms.AsteriskTerm))
            {
                parsenode.AstNode = new AsteriskSelection(null, null, null);
            }
            else if (parsenode.ChildNodes.Count == 3 && ReferenceEquals(parsenode.ChildNodes[2].Term, _provider.Terms.AsteriskTerm))
            {
                parsenode.AstNode = new AsteriskSelection(
                    new TableReference((ElementName)parsenode.ChildNodes[0].AstNode),
                    null,
                    null);
            }
            else
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
        }

        private void CreateSelectionSourceNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1)
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
            else
            {
                var left = (ISource)parsenode.ChildNodes[0].AstNode;
                if (parsenode.ChildNodes.Count == 2)
                {
                    var right = (ISource)parsenode.ChildNodes[1].AstNode;
                    parsenode.AstNode = new CrossJoinSource(left, right);
                }
                else if (parsenode.ChildNodes.Count == 4)
                {
                    var right = (ISource)parsenode.ChildNodes[3].AstNode;
                    parsenode.AstNode = new CrossJoinSource(left, right);
                }
                else if (parsenode.ChildNodes.Count == 6)
                {
                    var right = (ISource)parsenode.ChildNodes[3].AstNode;
                    var on = (ISqlLogicalExpression)parsenode.ChildNodes[5].AstNode;
                    parsenode.AstNode = new InnerJoinSource(
                        left,
                        right,
                        on);
                }
                else if (parsenode.ChildNodes.Count == 7)
                {
                    var right = (ISource)parsenode.ChildNodes[4].AstNode;
                    var on = (ISqlLogicalExpression)parsenode.ChildNodes[6].AstNode;
                    ConditionalJoinSource join;

                    if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.FullTerm))
                    {
                        join = new FullJoinSource(left, right, on);
                    }
                    else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.LeftTerm))
                    {
                        join = new LeftJoinSource(left, right, on);
                    }
                    else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.RightTerm))
                    {
                        join = new RightJoinSource(left, right, on);
                    }
                    else if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.InnerTerm))
                    {
                        join = new InnerJoinSource(left, right, on);
                    }
                    else
                    {
                        throw new InvalidOperationException();
                    }

                    parsenode.AstNode = join;
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        private void CreateSelectNode(AstContext context, ParseTreeNode parsenode)
        {
            if (parsenode.ChildNodes.Count == 1)
            {
                parsenode.AstNode = parsenode.ChildNodes[0].AstNode;
            }
            else if (parsenode.ChildNodes.Count == 2)
            {
                parsenode.AstNode = parsenode.ChildNodes[1].AstNode;
                var selectCommand = (SelectCommand)parsenode.ChildNodes[1].AstNode;
                selectCommand.With = (IEnumerable<WithExpression>)parsenode.ChildNodes[0].AstNode;
            }
            else
            {
                var left = (ISelectCommand)parsenode.ChildNodes[0].AstNode;
                if (parsenode.ChildNodes.Count == 3)
                {
                    if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.UnionTerm))
                    {
                        var right = (ISelectCommand)parsenode.ChildNodes[2].AstNode;
                        parsenode.AstNode = new UnionSelectionCommand(left, right);
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
                else if (parsenode.ChildNodes.Count == 4)
                {
                    if (ReferenceEquals(parsenode.ChildNodes[1].Term, _provider.Terms.UnionTerm))
                    {
                        var right = (ISelectCommand)parsenode.ChildNodes[3].AstNode;
                        parsenode.AstNode = new UnionAllSelectionCommand(left, right);
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }
            }
        }

        private void CreateSourceItemNode(AstContext context, ParseTreeNode parsenode)
        {
            ISource item = null;
            var sqlIdentifiersHandler = _provider.GetHandler<SqlIdentifiersHandler>();
            if (parsenode.ChildNodes[0].Term.Equals(sqlIdentifiersHandler.ElementFullName))
            {
                item = new TableReference((ElementName)parsenode.ChildNodes[0].AstNode);
            }

            if (item == null)
            {
                item = parsenode.ChildNodes[0].AstNode as ISource;
            }

            parsenode.AstNode = item;
        }

        private void CreateTopNode(AstContext context, ParseTreeNode parsenode)
        {
            ISqlValueExpression value;
            if (parsenode.ChildNodes[1].ChildNodes.Count == 1)
            {
                value = (ISqlValueExpression)parsenode.ChildNodes[1].ChildNodes[0].AstNode;
            }
            else
            {
                value = (ISqlValueExpression)parsenode.ChildNodes[1].ChildNodes[1].AstNode;
            }

            var isPercent = parsenode.ChildNodes[2].ChildNodes.Any();
            var withTies = parsenode.ChildNodes[3].ChildNodes.Any();
            var top = new TopExpression(
                value,
                isPercent,
                withTies);
            parsenode.AstNode = top;
        }

        private void CreateWithNode(AstContext context, ParseTreeNode parsenode)
        {
            var name = parsenode.ChildNodes[1].Token.ValueString;
            var columns = (IEnumerable<string>)(parsenode.ChildNodes[2].ChildNodes.Any() ? parsenode.ChildNodes[2].ChildNodes[0].AstNode : null);
            var command = (ISelectCommand)parsenode.ChildNodes[4].AstNode;
            var with = new WithExpression(
                name,
                command,
                columns);

            parsenode.AstNode = with;
        }
    }
}