﻿using System.Diagnostics.CodeAnalysis;
using Irony.Parsing;
using Qdarc.Parsers.Tsql.Handlers.Helpers;

namespace Qdarc.Parsers.Tsql.Handlers
{
    internal class SqlTermsHandler : ISqlGrammarElementHandler
    {
        public KeyTerm ActionTerm { get; private set; }

        public KeyTerm Add { get; private set; }

        public KeyTerm AddAssign { get; private set; }

        public KeyTerm AddTerm { get; private set; }

        public KeyTerm AfterTerm { get; private set; }

        public KeyTerm AllTerm { get; private set; }

        public KeyTerm AlterTerm { get; private set; }

        public KeyTerm And { get; private set; }

        public KeyTerm ApplyTerm { get; private set; }

        public KeyTerm AscendingTerm { get; private set; }

        public KeyTerm Assign { get; private set; }

        public KeyTerm AsteriskTerm { get; private set; }

        public KeyTerm AsTerm { get; private set; }

        public KeyTerm AuthorizationTerm { get; private set; }

        public KeyTerm BeginTerm { get; private set; }

        public KeyTerm BetweenTerm { get; private set; }

        public KeyTerm BitwiseAnd { get; private set; }

        public KeyTerm BitwiseAndAssign { get; private set; }

        public KeyTerm BitwiseExclusiveOr { get; private set; }

        public KeyTerm BitwiseExclusiveOrAssign { get; private set; }

        public KeyTerm BitwiseNot { get; private set; }

        public KeyTerm BitwiseOr { get; private set; }

        public KeyTerm BitwiseOrAssign { get; private set; }

        public KeyTerm ByTerm { get; private set; }

        public KeyTerm CallerTerm { get; private set; }

        public KeyTerm CascadeTerm { get; private set; }

        public KeyTerm CaseTerm { get; private set; }

        public KeyTerm CastTerm { get; private set; }

        public KeyTerm CheckTerm { get; private set; }

        public KeyTerm CloseBrace { get; private set; }

        public KeyTerm ClusteredTerm { get; private set; }

        public KeyTerm Coalesce { get; private set; }

        public KeyTerm Comma { get; set; }

        public KeyTerm CommitTerm { get; private set; }

        public KeyTerm ConstraintTerm { get; private set; }

        public KeyTerm Convert { get; private set; }

        public KeyTerm CountBigTerm { get; private set; }

        public KeyTerm CountTerm { get; private set; }

        public KeyTerm CreateTerm { get; private set; }

        public KeyTerm CrossTerm { get; private set; }

        public KeyTerm DeclareTerm { get; private set; }

        public KeyTerm DefaultTerm { get; private set; }

        public KeyTerm DeleteTerm { get; private set; }

        public KeyTerm DescencingTerm { get; private set; }

        public KeyTerm DistinctTerm { get; private set; }

        public KeyTerm Divide { get; private set; }

        public KeyTerm DivideAssign { get; private set; }

        public KeyTerm Dot { get; set; }

        public KeyTerm DropTerm { get; private set; }

        public KeyTerm ElseTerm { get; private set; }

        public KeyTerm EncriptionTerm { get; private set; }

        public KeyTerm EndTerm { get; private set; }

        public KeyTerm EqualsTerm { get; private set; }

        public KeyTerm ExecuteFullTerm { get; private set; }

        public KeyTerm ExecuteSqlTerm { get; private set; }

        public KeyTerm ExecuteTerm { get; private set; }

        public KeyTerm ExistsTerm { get; private set; }

        public KeyTerm ForeignTerm { get; private set; }

        public KeyTerm ForTerm { get; private set; }

        public KeyTerm FromTerm { get; private set; }

        public KeyTerm FullTerm { get; private set; }

        public KeyTerm FunctionTerm { get; private set; }

        public KeyTerm GoTerm { get; private set; }

        public KeyTerm GreaterThan { get; private set; }

        public KeyTerm GreaterThanOrEqual { get; private set; }

        public KeyTerm GroupTerm { get; private set; }

        public KeyTerm HavingTerm { get; private set; }

        public KeyTerm IdentityInsertTerm { get; private set; }

        public KeyTerm IdentityTerm { get; private set; }

        public KeyTerm IfTerm { get; private set; }

        public KeyTerm IndexTerm { get; private set; }

        public KeyTerm InlineIfTerm { get; private set; }

        public KeyTerm InnerTerm { get; private set; }

        public KeyTerm InsertTerm { get; private set; }

        public KeyTerm InsteadTerm { get; private set; }

        public KeyTerm InTerm { get; private set; }

        public KeyTerm IntoTerm { get; private set; }

        public KeyTerm Is { get; private set; }

        public KeyTerm JoinTerm { get; private set; }

        public KeyTerm LeftTerm { get; private set; }

        public KeyTerm LessThan { get; private set; }

        public KeyTerm LessThenOrEqual { get; private set; }

        public KeyTerm MatchedTerm { get; private set; }

        public KeyTerm MaxTerm { get; private set; }

        public KeyTerm MergeTerm { get; private set; }

        public KeyTerm Modulo { get; private set; }

        public KeyTerm ModuloAssign { get; private set; }

        public KeyTerm Multiply { get; private set; }

        public KeyTerm MultiplyAssign { get; private set; }

        public KeyTerm NonclusteredTerm { get; private set; }

        public KeyTerm Not { get; private set; }

        public KeyTerm NotEqualTo { get; private set; }

        public KeyTerm NotEqualToIso { get; private set; }

        public KeyTerm NoTerm { get; private set; }

        public KeyTerm NotGreaterThen { get; private set; }

        public KeyTerm NotLessThen { get; private set; }

        public KeyTerm Null { get; private set; }

        public KeyTerm OffTerm { get; private set; }

        public KeyTerm OfTerm { get; private set; }

        public KeyTerm OnTerm { get; private set; }

        public KeyTerm OpenBrace { get; set; }

        public KeyTerm Or { get; private set; }

        public KeyTerm OrderTerm { get; private set; }

        public KeyTerm OuterTerm { get; private set; }

        public KeyTerm OverTerm { get; private set; }

        public KeyTerm PartitionTerm { get; private set; }

        public KeyTerm PercentTerm { get; private set; }

        public KeyTerm PersistedTerm { get; private set; }

        public KeyTerm PrimaryTerm { get; private set; }

        public KeyTerm ProcedureTerm { get; private set; }

        public KeyTerm RaiseErrorTerm { get; private set; }

        public KeyTerm ReadonlyTerm { get; private set; }

        public KeyTerm ReferencesTerm { get; private set; }

        public KeyTerm ReturnsTerm { get; private set; }

        public KeyTerm ReturnTerm { get; private set; }

        public KeyTerm RightTerm { get; private set; }

        public KeyTerm RollbackTerm { get; private set; }

        public KeyTerm RowNumberTerm { get; private set; }

        public KeyTerm ScheamTerm { get; private set; }

        public KeyTerm SchemabindingTerm { get; private set; }

        public KeyTerm SelectTerm { get; private set; }

        public KeyTerm SelfTerm { get; private set; }

        public KeyTerm SetTerm { get; private set; }

        public KeyTerm SourceTerm { get; private set; }

        public KeyTerm SubstractAssign { get; private set; }

        public KeyTerm Subtract { get; private set; }

        public KeyTerm TableTerm { get; private set; }

        public KeyTerm TargetTerm { get; private set; }

        public KeyTerm Term { get; private set; }

        public KeyTerm ThenTerm { get; private set; }

        public KeyTerm ThrowTerm { get; private set; }

        public KeyTerm TiesTerm { get; private set; }

        public KeyTerm TopTerm { get; private set; }

        public KeyTerm TransactionFullTerm { get; private set; }

        public KeyTerm TransactionTerm { get; private set; }

        public KeyTerm TriggerTerm { get; private set; }

        public KeyTerm TypeTerm { get; private set; }

        public KeyTerm UnionTerm { get; private set; }

        public KeyTerm UniqueTerm { get; private set; }

        public KeyTerm UpdateTerm { get; private set; }

        public KeyTerm UseTerm { get; private set; }

        public KeyTerm UsingTerm { get; private set; }

        public KeyTerm ValuesTerm { get; private set; }

        public KeyTerm WhenTerm { get; private set; }

        public KeyTerm WhereTerm { get; private set; }

        public KeyTerm WithTerm { get; private set; }

        // todo: try to split this method
        [SuppressMessage(
            "Microsoft.Maintainability",
            "CA1505:AvoidUnmaintainableCode",
            Justification = "Very repetitive code and hard to make token groups that make sense")]
        [SuppressMessage(
          "Microsoft.Naming",
          "CA2204:Literals should be spelled correctly",
          MessageId = "spexecutesql",
          Justification = "Name of procedure.")]
        [SuppressMessage(
          "Microsoft.Globalization",
          "CA1303:Do not pass literals as localized parameters",
          MessageId = "Irony.Parsing.Grammar.ToTerm(System.String)",
          Justification = "This is grammar there are no localizable elements here")]
        public void Initialize(Grammar grammar)
        {
            ScheamTerm = grammar.ToTerm("SCHEMA");
            ScheamTerm.Flags |= TermFlags.IsReservedWord;
            AuthorizationTerm = grammar.ToTerm("AUTHORIZATION");
            AuthorizationTerm.Flags |= TermFlags.IsReservedWord;
            InsertTerm = grammar.ToTerm("INSERT");
            InsertTerm.Flags |= TermFlags.IsReservedWord;
            UpdateTerm = grammar.ToTerm("UPDATE");
            UpdateTerm.Flags |= TermFlags.IsReservedWord;
            DeleteTerm = grammar.ToTerm("DELETE");
            DeleteTerm.Flags |= TermFlags.IsReservedWord;

            SelectTerm = grammar.ToTerm("SELECT");
            SelectTerm.Flags |= TermFlags.IsReservedWord;
            AsteriskTerm = grammar.ToTerm("*");
            FromTerm = grammar.ToTerm("FROM");
            FromTerm.Flags |= TermFlags.IsReservedWord;
            WithTerm = grammar.ToTerm("WITH");
            WithTerm.Flags |= TermFlags.IsReservedWord;
            AsTerm = grammar.ToTerm("AS");
            AsTerm.Flags |= TermFlags.IsReservedWord;
            SetTerm = grammar.ToTerm("SET");
            SetTerm.Flags |= TermFlags.IsReservedWord;
            DeclareTerm = grammar.ToTerm("DECLARE");
            DeclareTerm.Flags |= TermFlags.IsReservedWord;
            MaxTerm = grammar.ToTerm("MAX");
            MaxTerm.Flags |= TermFlags.IsReservedWord;
            OnTerm = grammar.ToTerm("ON");
            OnTerm.Flags |= TermFlags.IsReservedWord;
            OffTerm = grammar.ToTerm("OFF");
            OffTerm.Flags |= TermFlags.IsReservedWord;
            GoTerm = grammar.ToTerm("GO");
            GoTerm.Flags |= TermFlags.IsReservedWord;
            AddTerm = grammar.ToTerm("Add");
            AddTerm.Flags |= TermFlags.IsReservedWord;
            UseTerm = grammar.ToTerm("USE");
            UseTerm.Flags |= TermFlags.IsReservedWord;
            ProcedureTerm = grammar.ToTerm("PROCEDURE");
            ProcedureTerm.Flags |= TermFlags.IsReservedWord;
            ExistsTerm = grammar.ToTerm("EXISTS");
            ExistsTerm.Flags |= TermFlags.IsReservedWord;
            CastTerm = grammar.ToTerm("CAST");
            CastTerm.Flags |= TermFlags.IsReservedWord;
            TopTerm = grammar.ToTerm("TOP");
            TopTerm.Flags |= TermFlags.IsReservedWord;
            InTerm = grammar.ToTerm("IN");
            InTerm.Flags |= TermFlags.IsReservedWord;
            CreateTerm = grammar.ToTerm("CREATE");
            CreateTerm.Flags |= TermFlags.IsReservedWord;
            AlterTerm = grammar.ToTerm("ALTER");
            AlterTerm.Flags |= TermFlags.IsReservedWord;
            DropTerm = grammar.ToTerm("DROP");
            DropTerm.Flags |= TermFlags.IsReservedWord;
            BeginTerm = grammar.ToTerm("BEGIN");
            BeginTerm.Flags |= TermFlags.IsReservedWord;
            EndTerm = grammar.ToTerm("END");
            EndTerm.Flags |= TermFlags.IsReservedWord;
            BetweenTerm = grammar.ToTerm("BETWEEN");
            BetweenTerm.Flags |= TermFlags.IsReservedWord;
            ReadonlyTerm = grammar.ToTerm("READONLY");
            ReadonlyTerm.Flags |= TermFlags.IsReservedWord;
            ThrowTerm = grammar.ToTerm("THROW");
            ThrowTerm.Flags |= TermFlags.IsReservedWord;
            IfTerm = grammar.ToTerm("IF");
            IfTerm.Flags |= TermFlags.IsReservedWord;
            ElseTerm = grammar.ToTerm("ELSE");
            ElseTerm.Flags |= TermFlags.IsReservedWord;
            WhereTerm = grammar.ToTerm("WHERE");
            WhereTerm.Flags |= TermFlags.IsReservedWord;
            InlineIfTerm = grammar.ToTerm("IIF");
            InlineIfTerm.Flags |= TermFlags.IsReservedWord;
            Coalesce = grammar.ToTerm("COALESCE");
            Coalesce.Flags |= TermFlags.IsReservedWord;
            Is = grammar.ToTerm("IS");
            Is.Flags |= TermFlags.IsReservedWord;
            Null = grammar.ToTerm("NULL");
            Null.Flags |= TermFlags.IsReservedWord;
            DefaultTerm = grammar.ToTerm("DEFAULT");
            DefaultTerm.Flags |= TermFlags.IsReservedWord;
            FunctionTerm = grammar.ToTerm("FUNCTION");
            FunctionTerm.Flags |= TermFlags.IsReservedWord;
            ReturnsTerm = grammar.ToTerm("RETURNS");
            ReturnsTerm.Flags |= TermFlags.IsReservedWord;
            ReturnTerm = grammar.ToTerm("RETURN");
            ReturnTerm.Flags |= TermFlags.IsReservedWord;
            TableTerm = grammar.ToTerm("TABLE");
            TableTerm.Flags |= TermFlags.IsReservedWord;
            EncriptionTerm = grammar.ToTerm("ENCRIPTION");
            EncriptionTerm.Flags |= TermFlags.IsReservedWord;
            SchemabindingTerm = grammar.ToTerm("SCHEMABINDING");
            SchemabindingTerm.Flags |= TermFlags.IsReservedWord;
            ValuesTerm = grammar.ToTerm("VALUES");
            ValuesTerm.Flags |= TermFlags.IsReservedWord;
            DistinctTerm = grammar.ToTerm("DISTINCT");
            DistinctTerm.Flags |= TermFlags.IsReservedWord;
            OrderTerm = grammar.ToTerm("ORDER");
            OrderTerm.Flags |= TermFlags.IsReservedWord;
            ByTerm = grammar.ToTerm("BY");
            ByTerm.Flags |= TermFlags.IsReservedWord;
            GroupTerm = grammar.ToTerm("GROUP");
            GroupTerm.Flags |= TermFlags.IsReservedWord;
            HavingTerm = grammar.ToTerm("HAVING");
            HavingTerm.Flags |= TermFlags.IsReservedWord;
            RowNumberTerm = grammar.ToTerm("ROW_NUMBER");
            RowNumberTerm.Flags |= TermFlags.IsReservedWord;
            PartitionTerm = grammar.ToTerm("PARTITION");
            PartitionTerm.Flags |= TermFlags.IsReservedWord;
            OverTerm = grammar.ToTerm("OVER");
            OverTerm.Flags |= TermFlags.IsReservedWord;
            ExecuteTerm = grammar.ToTerm("EXEC");
            ExecuteTerm.Flags |= TermFlags.IsReservedWord;
            ExecuteFullTerm = grammar.ToTerm("EXECUTE");
            ExecuteFullTerm.Flags |= TermFlags.IsReservedWord;
            ExecuteSqlTerm = grammar.ToTerm("sp_executesql");
            ExecuteSqlTerm.Flags |= TermFlags.IsReservedWord;
            ThenTerm = grammar.ToTerm("THEN");
            ThenTerm.Flags |= TermFlags.IsReservedWord;
            WhenTerm = grammar.ToTerm("WHEN");
            WhenTerm.Flags |= TermFlags.IsReservedWord;
            MatchedTerm = grammar.ToTerm("MATCHED");
            MatchedTerm.Flags |= TermFlags.IsReservedWord;
            MergeTerm = grammar.ToTerm("MERGE");
            MergeTerm.Flags |= TermFlags.IsReservedWord;
            IntoTerm = grammar.ToTerm("INTO");
            IntoTerm.Flags |= TermFlags.IsReservedWord;
            UsingTerm = grammar.ToTerm("USING");
            UsingTerm.Flags |= TermFlags.IsReservedWord;
            SourceTerm = grammar.ToTerm("SOURCE");
            SourceTerm.Flags |= TermFlags.IsReservedWord;
            TargetTerm = grammar.ToTerm("TARGET");
            TargetTerm.Flags |= TermFlags.IsReservedWord;
            PercentTerm = grammar.ToTerm("PERCENT");
            PercentTerm.Flags |= TermFlags.IsReservedWord;
            TiesTerm = grammar.ToTerm("TIES");
            TiesTerm.Flags |= TermFlags.IsReservedWord;
            CrossTerm = grammar.ToTerm("CROSS");
            CrossTerm.Flags |= TermFlags.IsReservedWord;
            ApplyTerm = grammar.ToTerm("APPLY");
            ApplyTerm.Flags |= TermFlags.IsReservedWord;
            TypeTerm = grammar.ToTerm("TYPE");
            TypeTerm.Flags |= TermFlags.IsReservedWord;
            AscendingTerm = grammar.ToTerm("ASC");
            AscendingTerm.Flags |= TermFlags.IsReservedWord;
            DescencingTerm = grammar.ToTerm("DESC");
            DescencingTerm.Flags |= TermFlags.IsReservedWord;
            PrimaryTerm = grammar.ToTerm("PRIMARY");
            PrimaryTerm.Flags |= TermFlags.IsReservedWord;
            Term = grammar.ToTerm("KEY");
            Term.Flags |= TermFlags.IsReservedWord;
            NonclusteredTerm = grammar.ToTerm("NONCLUSTERED");
            NonclusteredTerm.Flags |= TermFlags.IsReservedWord;
            UniqueTerm = grammar.ToTerm("UNIQUE");
            UniqueTerm.Flags |= TermFlags.IsReservedWord;
            ClusteredTerm = grammar.ToTerm("CLUSTERED");
            ClusteredTerm.Flags |= TermFlags.IsReservedWord;
            TriggerTerm = grammar.ToTerm("TRIGGER");
            TriggerTerm.Flags |= TermFlags.IsReservedWord;
            CallerTerm = grammar.ToTerm("CALLER");
            CallerTerm.Flags |= TermFlags.IsReservedWord;
            SelfTerm = grammar.ToTerm("SELF");
            SelfTerm.Flags |= TermFlags.IsReservedWord;
            ForTerm = grammar.ToTerm("FROM");
            ForTerm.Flags |= TermFlags.IsReservedWord;
            AfterTerm = grammar.ToTerm("FOR");
            AfterTerm.Flags |= TermFlags.IsReservedWord;
            OfTerm = grammar.ToTerm("OF");
            OfTerm.Flags |= TermFlags.IsReservedWord;
            InsteadTerm = grammar.ToTerm("INSTEAD");
            InsteadTerm.Flags |= TermFlags.IsReservedWord;
            RaiseErrorTerm = grammar.ToTerm("RAISERROR");
            RaiseErrorTerm.Flags |= TermFlags.IsReservedWord;
            TransactionTerm = grammar.ToTerm("TRAN");
            TransactionTerm.Flags |= TermFlags.IsReservedWord;
            TransactionFullTerm = grammar.ToTerm("TRANSACTION");
            TransactionFullTerm.Flags |= TermFlags.IsReservedWord;
            CommitTerm = grammar.ToTerm("COMMIT");
            CommitTerm.Flags |= TermFlags.IsReservedWord;
            RollbackTerm = grammar.ToTerm("ROLLBACK");
            RollbackTerm.Flags |= TermFlags.IsReservedWord;
            CountTerm = grammar.ToTerm("COUNT");
            CountTerm.Flags |= TermFlags.IsReservedWord;
            CountBigTerm = grammar.ToTerm("COUNT_BIG");
            CountBigTerm.Flags |= TermFlags.IsReservedWord;
            IdentityTerm = grammar.ToTerm("IDENTITY");
            IdentityTerm.Flags |= TermFlags.IsReservedWord;
            CaseTerm = grammar.ToTerm("CASE");
            CaseTerm.Flags |= TermFlags.IsReservedWord;
            PersistedTerm = grammar.ToTerm("PERSISTED");
            PersistedTerm.Flags |= TermFlags.IsReservedWord;
            ConstraintTerm = grammar.ToTerm("CONSTRAINT");
            ConstraintTerm.Flags |= TermFlags.IsReservedWord;
            NoTerm = grammar.ToTerm("NO");
            NoTerm.Flags |= TermFlags.IsReservedWord;
            ActionTerm = grammar.ToTerm("ACTION");
            ActionTerm.Flags |= TermFlags.IsReservedWord;
            CascadeTerm = grammar.ToTerm("CASCADE");
            CascadeTerm.Flags |= TermFlags.IsReservedWord;
            ForeignTerm = grammar.ToTerm("FOREIGN");
            ForeignTerm.Flags |= TermFlags.IsReservedWord;
            ReferencesTerm = grammar.ToTerm("REFERENCES");
            ReferencesTerm.Flags |= TermFlags.IsReservedWord;
            CheckTerm = grammar.ToTerm("CHECK");
            CheckTerm.Flags |= TermFlags.IsReservedWord;
            IndexTerm = grammar.ToTerm("INDEX");
            IndexTerm.Flags |= TermFlags.IsReservedWord;
            IdentityInsertTerm = grammar.ToTerm("IDENTITY_INSERT");
            IdentityInsertTerm.Flags |= TermFlags.IsReservedWord;
            Convert = grammar.ToTerm("CONVERT");
            Convert.Flags |= TermFlags.IsReservedWord;

            CreateSetOperationsTokens(grammar);
            CreateArythmeticTerms(grammar);
            CreatePunctuationTerms(grammar);
            RegisterOperators(grammar);
        }

        private void CreateArythmeticTerms(Grammar grammar)
        {
            Not = grammar.ToTerm("NOT");
            Not.Flags |= TermFlags.IsReservedWord;
            And = grammar.ToTerm("AND");
            And.Flags |= TermFlags.IsReservedWord;
            Or = grammar.ToTerm("OR");
            Or.Flags |= TermFlags.IsReservedWord;
            Add = grammar.ToTerm("+");
            Subtract = grammar.ToTerm("-");
            Multiply = grammar.ToTerm("*");
            Divide = grammar.ToTerm("/");
            Modulo = grammar.ToTerm("%");
            BitwiseAnd = grammar.ToTerm("&");
            BitwiseOr = grammar.ToTerm("|");
            BitwiseExclusiveOr = grammar.ToTerm("^");
            BitwiseNot = grammar.ToTerm("~");

            Assign = grammar.ToTerm("=");
            AddAssign = grammar.ToTerm("+=");
            SubstractAssign = grammar.ToTerm("-=");
            MultiplyAssign = grammar.ToTerm("*=");
            ModuloAssign = grammar.ToTerm("%=");
            DivideAssign = grammar.ToTerm("/=");
            BitwiseAndAssign = grammar.ToTerm("&=");
            BitwiseOrAssign = grammar.ToTerm("|=");
            BitwiseExclusiveOrAssign = grammar.ToTerm("^=");

            EqualsTerm = grammar.ToTerm("=");
            GreaterThan = grammar.ToTerm(">");
            LessThan = grammar.ToTerm("<");
            GreaterThanOrEqual = grammar.ToTerm(">=");
            LessThenOrEqual = grammar.ToTerm("<=");
            NotEqualToIso = grammar.ToTerm("<>");
            NotEqualTo = grammar.ToTerm("!=");
            NotLessThen = grammar.ToTerm("!<");
            NotGreaterThen = grammar.ToTerm("!>");
        }

        private void CreatePunctuationTerms(Grammar grammar)
        {
            Comma = grammar.ToTerm(",");
            Dot = grammar.ToTerm(".");
            OpenBrace = grammar.ToTerm("(");
            CloseBrace = grammar.ToTerm(")");
            grammar.MarkPunctuation(Comma, Dot, OpenBrace, CloseBrace);
        }

        private void CreateSetOperationsTokens(Grammar grammar)
        {
            JoinTerm = grammar.ToTerm("JOIN");
            JoinTerm.Flags |= TermFlags.IsReservedWord;
            LeftTerm = grammar.ToTerm("LEFT");
            LeftTerm.Flags |= TermFlags.IsReservedWord;
            OuterTerm = grammar.ToTerm("OUTER");
            OuterTerm.Flags |= TermFlags.IsReservedWord;
            RightTerm = grammar.ToTerm("RIGHT");
            RightTerm.Flags |= TermFlags.IsReservedWord;
            InnerTerm = grammar.ToTerm("INNER");
            InnerTerm.Flags |= TermFlags.IsReservedWord;
            FullTerm = grammar.ToTerm("FULL");
            FullTerm.Flags |= TermFlags.IsReservedWord;
            UnionTerm = grammar.ToTerm("UNION");
            UnionTerm.Flags |= TermFlags.IsReservedWord;
            AllTerm = grammar.ToTerm("ALL");
            AllTerm.Flags |= TermFlags.IsReservedWord;
        }

        private void RegisterOperators(Grammar grammar)
        {
            grammar.RegisterOperators(10, Multiply, Divide, Modulo);
            grammar.RegisterOperators(9, Add, Subtract);
            grammar.RegisterOperators(8, EqualsTerm, GreaterThan, LessThan, GreaterThanOrEqual, LessThenOrEqual);
            grammar.RegisterOperators(8, NotEqualToIso, NotEqualTo, NotLessThen, NotGreaterThen, InTerm);
            grammar.RegisterOperators(7, BitwiseExclusiveOr, BitwiseAnd, BitwiseOr);
            grammar.RegisterOperators(6, Not);
            grammar.RegisterOperators(5, And);
            grammar.RegisterOperators(4, Or);
        }
    }
}